package com.sun.mdm.index.edm.presentation.handlers;

import com.sun.mdm.index.edm.services.configuration.ConfigManager;
import com.sun.mdm.index.edm.services.configuration.ValidationService;
import com.sun.mdm.index.edm.control.QwsController;
import com.sun.mdm.index.edm.util.DateUtil;
import com.sun.mdm.index.edm.util.QwsUtil;
import com.sun.mdm.index.edm.services.security.Logon;
import com.sun.mdm.index.edm.services.masterController.MasterControllerService;

import javax.servlet.http.HttpServlet;

import com.sun.mdm.index.edm.presentation.util.Localizer;
import com.sun.mdm.index.edm.presentation.util.Logger;
import net.java.hulp.i18n.LocalizationSupport;

public class InitializeServlet extends HttpServlet  {
    
    private transient static final Logger mLogger = Logger.getLogger("com.sun.mdm.index.edm.presentation.handlers.InitializeServlet");
    private static transient final Localizer mLocalizer = Localizer.get();    

     public void  init() {
     	int euidLength=10;
     	mLogger.info("####Starting Hot Deployment####");
        try {
            ConfigManager.init();
        } catch (Exception t) {
            mLogger.error(mLocalizer.x("ISV001: Error while initializing ConfigManager {0}",t.getMessage()));
            
        }

        // check if MasterController is available
        // To get to MasterController, ConfigManager is required.
        if (QwsController.getMasterController() == null || QwsController.getValidationService() == null) {
             try {
                // this piece of info in ConfigManager comes from MC
                ConfigManager.getInstance().setEuidLength(euidLength);
            } catch (Exception e) {
                 mLogger.error(mLocalizer.x("ISV002: Error while initializing QwsController : {0}",e.getMessage()),e);
                 
            }
 
             // initialize the date format in DateUtil for EDM to convert date/string back and forth
            try {
                DateUtil.init();
            } catch (Exception t) {
                mLogger.error(mLocalizer.x("ISV003: Error while initializing DateUtil : {0}",t.getMessage()),t);
               
            }
        } else {
            // Restart the ValidationService and DateUtil in case there were problems
            // with logging in the EDM or if the database connection was lost.  Otherwise,
            // the EDM will not display values for pull-down menus.
            try {
                ValidationService.init();
            } catch (Exception t) {
                mLogger.error(mLocalizer.x("ISV004: Error while initializing ValidationService : {0}",t.getMessage()),t);
                
            }
            
            // initialize the date format in DateUtil for EDM to convert date/string back and forth
            try {
                DateUtil.init();
            } catch (Exception t) {
                mLogger.error(mLocalizer.x("ISV005: Error while initializing ValidationService : {0}",t.getMessage()),t);
                
            }            
        }
        String errorMessage ="Login_Success";
        try {
            Logon.initializeConfigurationSecurity();            
        } catch(Exception ex) {            
            errorMessage = "login_user_login_init_load_message";
            mLogger.error(mLocalizer.x("ISV006: Error while initializing ConfigurationSecurity :{1}",errorMessage,ex.getMessage()),ex);            
        }
        
        try {
            Logon.initializeQWSControllerValidationDate();
        } catch (Exception e) {
            errorMessage = "login_user_login_init_load_message";
            mLogger.info(mLocalizer.x("ISV007: Error while initializing QWSControllerValidationDate :{1}",errorMessage,e.getMessage()));            
        }
        
        mLogger.info("####Ending Hot Deployment####");
         

      }
    
    
}
