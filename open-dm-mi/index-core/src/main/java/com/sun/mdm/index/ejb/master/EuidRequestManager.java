/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common
 * Development and Distribution License ("CDDL")(the "License"). You
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the
 * specific language governing permissions and limitations under the
 * License.
 *
 * When distributing the Covered Code, include this CDDL Header Notice
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the
 * fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.index.ejb.master;

import java.util.Queue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.locks.LockSupport;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Euid Request Queue Manager class.
 * @author chaye
 */
public class EuidRequestManager {
    private ConcurrentHashMap<String, EuidRequestQueue> waiters = new ConcurrentHashMap<String, EuidRequestQueue>();
    private static EuidRequestManager instance = null;

    protected EuidRequestManager(){
    }

    public static EuidRequestManager getInstance() {
        if (instance == null) {
            instance = new EuidRequestManager();
        }
        return instance;
    }

    public void lock(String euid) {
        if (euid != null) {
            EuidRequestQueue euidQueue = waiters.get(euid);
            if (euidQueue == null) {
                euidQueue = new EuidRequestQueue(euid);
                waiters.put(euid, euidQueue);
            }
            euidQueue.lock();
        }
    }

    public void unlock(String euid) {
        if (euid != null) {
            EuidRequestQueue euidQueue = waiters.get(euid);
            if (euidQueue != null) {
                euidQueue.unlock();
            }
            if (euidQueue != null &&
                euidQueue.isEmpty()) {
                waiters.remove(euid);
            }
        }
    }

    public EuidRequestQueue getEuidRequestQueue(String euid) {
        return waiters.get(euid);
    }

    public class EuidRequestQueue {
        private AtomicBoolean locked = new AtomicBoolean(false);
        private Queue<Thread> waiters = new ConcurrentLinkedQueue<Thread>();

        String euid;
        String revision;

        public EuidRequestQueue(String euid) {
            this.euid = euid;
        }

        public EuidRequestQueue(String euid, String revision) {
            this.euid = euid;
            this.revision = revision;
        }

        public void setEUID(String euid) {
            this.euid = euid;
        }

        public String getEUID() {
            return this.euid;
        }

        public void setRevision(String revision) {
            this.revision = revision;
        }

        public String getRevision() {
            return this.revision;
        }

        @Override
        public boolean equals (Object obj) {
            if (obj instanceof EuidRequestQueue) {
                EuidRequestQueue el = (EuidRequestQueue)obj;
                if (el.euid != null &&
                    el.euid.equals(this.euid)) {
                    return true;
                }
            }
            return false;
        }

        public void lock() {
            boolean wasInterrupted = false;
            Thread current = Thread.currentThread();
            waiters.add(current);

            // Block while not first in queue or cannot acquire lock
            while (waiters.peek() != current ||
                    !locked.compareAndSet(false, true)) {
                LockSupport.park();
                if (Thread.interrupted()) {
                  // ignore interrupts while waiting
                  wasInterrupted = true;
                }
            }
            
            waiters.remove();
            locked.set(true);
            if (wasInterrupted) {
                // reassert interrupt status on exit
                current.interrupt();
            }
        }

        public void unlock() {
            locked.set(false);
            LockSupport.unpark(waiters.peek());
        }

        public boolean isEmpty() {
            return waiters.isEmpty();
        }
    }
}
