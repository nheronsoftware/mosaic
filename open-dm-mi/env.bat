set NB_HOME=C:\NetGen.NB7\netbeans
set SOURCE_HOME=C:\NetGen.Mosaic\mural
set FULL_PATH_TO_ORACLE_JDBC_JAR=%SOURCE_HOME%\lib\ojdbc14.jar
set FULL_PATH_TO_SQLSERVER_JDBC_JAR=%SOURCE_HOME%\lib\sqljdbc.jar
set GF_HOME=C:\NetGen.NB7\glassfish
set MAVEN_HOME=C:\User\apache-maven-2.0.9
set JAVA_HOME=C:\Java\jdk1.6.0_29
set ANT_HOME=C:\User\apache-ant-1.8.0
set RUN_JUNIT=no

set OPEM_DM_MI_ROOT=%CD%
set FULL_PATH_TO_LOADER_RESOURCES_JAR=%OPEM_DM_MI_ROOT%\loader\test\config\resources.jar
set FULL_PATH_TO_LOADER_MASTER_INDEX_CLIENT_JAR=%OPEM_DM_MI_ROOT%\loader\test\config\master-index-client.jar

set PATH=%MAVEN_HOME%/bin;%ANT_HOME%/bin;%PATH%
set ANT_OPTS=-Xmx512m
set MAVEN_OPTS=-Xmx512m

call mvn install:install-file -DgroupId=oracle.jdbc -DartifactId=ojdbc14 -Dversion=10.1.0.2.0 -Dpackaging=jar -Dfile=%FULL_PATH_TO_ORACLE_JDBC_JAR%
call mvn install:install-file -DgroupId=msft.sqlserver.jdbc -DartifactId=sqljdbc -Dversion=1.0 -Dpackaging=jar -Dfile=%FULL_PATH_TO_SQLSERVER_JDBC_JAR%
call mvn install:install-file -DgroupId=javaee -DartifactId=javaee-api -Dversion=5 -Dpackaging=jar -Dfile=%SOURCE_HOME%/lib/javaee.jar

call mvn install:install-file -DgroupId=loader.test -DartifactId=resources -Dversion=6.0 -Dpackaging=jar -Dfile=%FULL_PATH_TO_LOADER_RESOURCES_JAR%
call mvn install:install-file -DgroupId=loader.test -DartifactId=master-index-client -Dversion=6.0 -Dpackaging=jar -Dfile=%FULL_PATH_TO_LOADER_MASTER_INDEX_CLIENT_JAR%

call mvn install:install-file -DgroupId=commons-beanutils -DartifactId=commons-beanutils -Dversion=1.7.0 -Dpackaging=jar -Dfile=%OPEM_DM_MI_ROOT%\..\lib\commons-beanutils.jar
call mvn install:install-file -DgroupId=xml-apis -DartifactId=xml-apis -Dversion=1.3.02 -Dpackaging=jar -Dfile=%OPEM_DM_MI_ROOT%\..\lib\xml-apis.jar
call mvn install:install-file -DgroupId=org.netbeans.modules -DartifactId=org-netbeans-modules-compapp-projects-base -Dversion=RELEASE67 -Dpackaging=jar -Dfile=%OPEM_DM_MI_ROOT%\..\lib\org-netbeans-modules-compapp-projects-base.jar
