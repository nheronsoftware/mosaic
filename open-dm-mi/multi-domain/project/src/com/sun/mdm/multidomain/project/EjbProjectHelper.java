/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sun.mdm.multidomain.project;

import java.util.Properties;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;

import com.sun.mdm.multidomain.project.generator.FileUtil;

/**
 * EjbProjectHelper class.
 * @author cye
 * @author jlu
 */
public class EjbProjectHelper {
  
    public static void deleteJarsFromEjbProject(File ejbDir, List<String> jars) {
        try {
            List<String> deletedLibs = new ArrayList<String>();
            File ejbProjectXmlFile = new File(ejbDir, "nbproject/project.xml");
            String ejbProjectXmlString = FileUtil.readFileToString(ejbProjectXmlFile);
            for (String libName : jars) {
                if (ejbProjectXmlString.indexOf(libName) >= 0) {
                    deletedLibs.add(libName);
                }
            }
            if (deletedLibs.size() == 0) {
                return;
            }

            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilder = docBuilderFactory.newDocumentBuilder();
            Document ejbProjectXmlDoc = docBuilder.parse(ejbProjectXmlFile);

            File ejbPropertyFile = new File(ejbDir, "nbproject/project.properties");
            Properties projectProperties = new Properties();
            projectProperties.load(new FileInputStream(ejbPropertyFile));

            String classpath = projectProperties.getProperty("javac.classpath").trim();

            File buildImplXmlFile = new File(ejbDir, "nbproject/build-impl.xml");
            docBuilderFactory = DocumentBuilderFactory.newInstance();
            docBuilder = docBuilderFactory.newDocumentBuilder();
            Document buildImplXmlDoc = docBuilder.parse(buildImplXmlFile);

            for (String deletedLibName : deletedLibs) {
                //for project.properties
                String fileReference = "file.reference." + deletedLibName;

                projectProperties.remove(fileReference);
                String fileReferenceClassPath = ":${" + fileReference + "}";
                int beginIndex = classpath.indexOf(fileReferenceClassPath);
                int endIndex = beginIndex + fileReferenceClassPath.length();
                String str1 = classpath.substring(0, beginIndex);
                String str2 = classpath.substring(endIndex);
                classpath = str1 + str2;
                projectProperties.setProperty("javac.classpath", classpath);

                //ejb project.xml
                NodeList includedLibraries = ejbProjectXmlDoc.getElementsByTagName("included-library");
                for (int i = 0; i < includedLibraries.getLength(); i++) {
                    Node includedLibrary = includedLibraries.item(i);
                    if (fileReference.equals(includedLibrary.getTextContent())) {
                        Node parent = includedLibrary.getParentNode();
                        parent.removeChild(includedLibrary);
                    }
                }

                //ejb build-impl.xml
                NodeList copyLibraries = buildImplXmlDoc.getElementsByTagName("copy");
                for (int i = 0; i < copyLibraries.getLength(); i++) {
                    Element copyLibrary = (Element) copyLibraries.item(i);
                    String attribute = copyLibrary.getAttribute("file");
                    if (("${" + fileReference + "}").equals(attribute)) {
                        Node parent = copyLibrary.getParentNode();
                        parent.removeChild(copyLibrary);
                    }
                }

                NodeList baseNames = buildImplXmlDoc.getElementsByTagName("basename");
                for (int i = 0; i < baseNames.getLength(); i++) {
                    Element baseName = (Element) baseNames.item(i);
                    String attribute = baseName.getAttribute("file");
                    if (("${" + fileReference + "}").equals(attribute)) {
                        Node parent = baseName.getParentNode();
                        parent.removeChild(baseName);
                    }
                }

                NodeList manifests = buildImplXmlDoc.getElementsByTagName("manifest");
                String expectedValue = "${" + fileReference + "} ";
                for (int i = 0; i < manifests.getLength(); i++) {
                    Node manifest = manifests.item(i);
                    NodeList attributes = manifest.getChildNodes();
                    for (int j = 0; j < attributes.getLength(); j++) {
                        Node attribute = attributes.item(j);
                        if ("attribute".equals(attribute.getNodeName())) {
                            String value = ((Element) attribute).getAttribute("value");
                            if (value.indexOf(expectedValue) >= 0) {
                                beginIndex = value.indexOf(expectedValue);
                                endIndex = beginIndex + expectedValue.length();
                                str1 = value.substring(0, beginIndex);
                                str2 = value.substring(endIndex);
                                value = str1 + str2;
                                ((Element) attribute).setAttribute("value", value);
                            }
                        }
                    }
                }
            }

            //write to project.properties
            projectProperties.store(new FileOutputStream(ejbPropertyFile), null);

            //write to project.xml
            FileUtil.updateFile(ejbProjectXmlFile, FileUtil.transformXMLtoString(ejbProjectXmlDoc));

            //write to build-impl.xml
            FileUtil.updateFile(buildImplXmlFile, FileUtil.transformXMLtoString(buildImplXmlDoc));
        } catch (Exception ignore) {
            ignore.printStackTrace();
        }
    }

    public static void addLibsToEjbProject(File ejbDir,
            List<String> libs,
            String relativeLocation)
            throws FileNotFoundException, IOException, Exception {

        List<String> newLibs = new ArrayList<String>();
        File ejbProjectXml = new File(ejbDir, "nbproject/project.xml");
        String ejbProjectXmlString = FileUtil.readFileToString(ejbProjectXml);
        for (String libName : libs) {
            if (ejbProjectXmlString.indexOf(libName) < 0) {
                newLibs.add(libName);
            }
        }
        if (newLibs.size() == 0) {
            return;
        }
        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docBuilder = docBuilderFactory.newDocumentBuilder();
        Document ejbProjectXmlDoc = docBuilder.parse(ejbProjectXml);
        List<Element> includedLibraryList = new ArrayList<Element>();

        File ejbPropertyFile = new File(ejbDir, "nbproject/project.properties");
        java.util.Properties properties = new java.util.Properties();
        properties.load(new FileInputStream(ejbPropertyFile));

        StringBuffer classpath;
        StringBuffer manifesClasspath = new StringBuffer();
        if (null == properties.getProperty("javac.classpath")) {
            classpath = new StringBuffer();
        } else {
            classpath = new StringBuffer(properties.getProperty("javac.classpath").trim());
        }

        File buildImplXml = new File(ejbDir, "nbproject/build-impl.xml");
        docBuilderFactory = DocumentBuilderFactory.newInstance();
        docBuilder = docBuilderFactory.newDocumentBuilder();
        Document buildImplXmlDoc = docBuilder.parse(buildImplXml);
        List<Element> archiveList = new ArrayList<Element>();
        List<Element> manifestList = new ArrayList<Element>();

        for (String newLibName : newLibs) {
            //for project.properties
            properties.setProperty("file.reference." + newLibName,
                    relativeLocation + "/" + newLibName);

            if (classpath.length() < 1) {
                classpath.append("${file.reference." + newLibName + "}");
            } else {
                classpath = classpath.append(":${file.reference." + newLibName + "}");
            }

            //ejb project.xml
            Element ejbProjectXmlElement = ejbProjectXmlDoc.createElement("included-library");
            ejbProjectXmlElement.setAttribute("files", "1");
            ejbProjectXmlElement.setTextContent("file.reference." + newLibName);
            includedLibraryList.add(ejbProjectXmlElement);

            //ejb build-impl.xml
            Element buildImplXmlElement = buildImplXmlDoc.createElement("copy");
            buildImplXmlElement.setAttribute("file", "${file.reference." + newLibName + "}");
            buildImplXmlElement.setAttribute("todir", "${build.classes.dir}");
            archiveList.add(buildImplXmlElement);

            buildImplXmlElement = buildImplXmlDoc.createElement("basename");
            buildImplXmlElement.setAttribute("file", "${file.reference." + newLibName + "}");
            buildImplXmlElement.setAttribute("property", "included.lib.file.reference." + newLibName);
            manifestList.add(buildImplXmlElement);
            buildImplXmlElement = buildImplXmlDoc.createElement("copy");
            buildImplXmlElement.setAttribute("file", "${file.reference." + newLibName + "}");
            buildImplXmlElement.setAttribute("todir", "${dist.ear.dir}");
            manifestList.add(buildImplXmlElement);

            manifesClasspath.append("${file.reference." + newLibName + "} ");

        }

        //write to project.properties
        properties.setProperty("javac.classpath", classpath.toString());
        properties.store(new FileOutputStream(ejbPropertyFile), null);

        //write to project.xml
        NodeList dataNodes = ejbProjectXmlDoc.getElementsByTagName("data");
        Element dataElement = (Element) dataNodes.item(0);
        for (Element includedLibrary : includedLibraryList) {
            dataElement.appendChild(includedLibrary);
        }
        FileUtil.updateFile(ejbProjectXml, FileUtil.transformXMLtoString(ejbProjectXmlDoc));

        //write to build-impl.xml
        NodeList nodes = buildImplXmlDoc.getElementsByTagName("target");
        for (int i = 0; i < nodes.getLength(); i++) {
            Element element = (Element) (nodes.item(i));
            if (element.hasAttribute("name") &&
                    element.getAttribute("name").equals("library-inclusion-in-archive")) {
                for (Element archive : archiveList) {
                    element.appendChild(archive);
                }
            }

            if (element.hasAttribute("name") &&
                    element.getAttribute("name").equals("library-inclusion-in-manifest")) {
                for (Element manifest : manifestList) {
                    element.insertBefore(manifest, element.getLastChild());
                }

                NodeList manifestNodes = element.getElementsByTagName("manifest");
                Element manifestElement = (Element) manifestNodes.item(0);
                Node attributeNode = manifestElement.getElementsByTagName("attribute").item(0);
                Element attributeElement = null;
                if (attributeNode == null) {
                    attributeElement = buildImplXmlDoc.createElement("attribute");
                    attributeElement.setAttribute("name", "Class-Path");
                    attributeElement.setAttribute("value", manifesClasspath.toString());
                    manifestElement.appendChild(attributeElement);
                } else {
                    attributeElement = (Element) attributeNode;
                    String value = attributeElement.getAttribute("value");
                    attributeElement.setAttribute("value", manifesClasspath.toString() + value);
                }
            }
        }
        FileUtil.updateFile(buildImplXml, FileUtil.transformXMLtoString(buildImplXmlDoc));
    }

}
