/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.multidomain.project.nodes;

import java.util.List;
import java.util.ArrayList;
import java.util.Enumeration;
import java.io.File;
import java.io.IOException;
import javax.swing.Action;

import org.openide.loaders.DataFolder;
import org.openide.util.actions.SystemAction;
import org.openide.filesystems.FileUtil;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileAttributeEvent;
import org.openide.filesystems.FileRenameEvent;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectManager;
import org.netbeans.spi.project.support.ant.AntProjectHelper;
import org.netbeans.spi.project.support.ant.EditableProperties;

import com.sun.mdm.multidomain.project.EjbProjectHelper;
import com.sun.mdm.multidomain.project.MultiDomainProjectProperties;
import com.sun.mdm.multidomain.util.Logger;
import com.sun.mdm.multidomain.project.editor.EditorMainApp;
import com.sun.mdm.multidomain.util.Helper;

/**
 * MultiDomainPlugInsFolderNode class.
 * @author cye
 */
public class MultiDomainPlugInsFolderNode extends MultiDomainFolderNode {
    private static final Logger mLog = Logger.getLogger(
            MultiDomainPlugInsFolderNode.class.getName()
        
        );
    private DataFolder folder;
    private FileObject pluginDir;
    private FileObject projectDir;
            
    public MultiDomainPlugInsFolderNode(String displayName, DataFolder folder) {
        super(displayName, folder);
        this.folder = folder;
        this.pluginDir = folder.getPrimaryFile();
        this.projectDir = FileUtil.toFileObject(FileUtil.toFile(pluginDir).getParentFile().getParentFile());
        this.pluginDir.addFileChangeListener(new pluginChangeListener());
        String appName = projectDir.getName();
        addPlugins(pluginDir, appName);
        this.getCookieSet().add(new MultiDomainPlugInsCookieImpl(this));
    }
    
    @Override
    public Action[] getActions( boolean context ) {
        return new Action[] {
            SystemAction.get(com.sun.mdm.multidomain.project.actions.ImportRelationshipPluginAction.class),
            null,
            SystemAction.get(org.openide.actions.FindAction.class),
            null,
            SystemAction.get(org.openide.actions.OpenLocalExplorerAction.class),
        };
    }
    
    @Override      
    public FileObject getFileObject() {
        return pluginDir;
    }
    
    private void addPlugins(FileObject pluginDir, String appName) {
       
        EditorMainApp editorMainApp = EditorMainApp.getInstance(appName);
        if (editorMainApp == null) {
            editorMainApp = EditorMainApp.createInstance(appName);
        };   
        FileObject[] files = pluginDir.getChildren();
        String coreFileName = FileUtil.toFile(pluginDir).getParentFile().getParentFile().getPath();
        coreFileName = coreFileName + File.separator + "lib" + File.separator + "multidomain-core.jar";
        for (FileObject file : files) {
            if (file.isData()) {
                List<EditorMainApp.Plugin> plugins = Helper.getPlugins(file.getPath(), coreFileName);
                editorMainApp.addPlugins(plugins);
            }
        } 
    }
            
    private void deleteFileFromEjbProject(String deletedFile) {
        File ejbProjectDir = null;
        try {
            Project mdProject = ProjectManager.getDefault().findProject(projectDir);
            AntProjectHelper mdProjectHelper = (AntProjectHelper) mdProject.getLookup().lookup(AntProjectHelper.class);
            EditableProperties mdProjectProperties = mdProjectHelper.getProperties(AntProjectHelper.PROJECT_PROPERTIES_PATH);
            String ejbProjectLocation = mdProjectProperties.getProperty(MultiDomainProjectProperties.EJB_DIR);
            ejbProjectDir = new File(projectDir.getPath() + File.separator + ejbProjectLocation); 
            List<String> deletedFiles = new ArrayList<String>();
            deletedFiles.add(deletedFile);
            EjbProjectHelper.deleteJarsFromEjbProject(ejbProjectDir, deletedFiles);
            
            String appName = projectDir.getName();
            EditorMainApp editorMainApp = EditorMainApp.getInstance(appName);
            if (editorMainApp == null) {
                editorMainApp = EditorMainApp.createInstance(appName);
            };
            List<EditorMainApp.Plugin> plugins = Helper.getPlugins(deletedFile, null);
            editorMainApp.removePlugins(plugins); 
        } catch(IOException ioex) {
        }
    }
    
    public class pluginChangeListener implements FileChangeListener {
        public void fileAttributeChanged(FileAttributeEvent fe) {
        }
        public void fileChanged(FileEvent fe) {    
        }
        public void fileDataCreated(FileEvent fe) {            
        }
        public void fileDeleted(FileEvent fe) {
            deleteFileFromEjbProject(fe.getFile().getNameExt());
        }
        public void fileFolderCreated(FileEvent fe) {
        }
        public void fileRenamed(FileRenameEvent fe) {
        }
    }
     
}