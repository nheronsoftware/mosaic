/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.multidomain.util;

import java.util.List;
import java.util.ArrayList;
import java.io.File;
import java.io.IOException;
import java.util.jar.JarFile;
import java.util.jar.JarEntry;
import  java.util.Enumeration; 
import java.net.URL;
import java.net.URLClassLoader;

import com.sun.mdm.multidomain.project.editor.EditorMainApp.Plugin;

/**
 * Helper class.
 * @author cye
 */
public class Helper {

    
    public static List<Plugin> getPlugins(String fileName, String coreFileName) {
        List<Plugin> plugins = new ArrayList<Plugin>();
        try {
            File jarFile = new File(fileName);
            URL[] jarURL = null;
            if (coreFileName == null) {
                jarURL = new URL[]{jarFile.toURL()};
            } else {
                jarURL = new URL[]{jarFile.toURL(), new File(coreFileName).toURL()};
            }
            URLClassLoader clzLoader = new URLClassLoader(jarURL); 
            JarFile jar = new JarFile(jarFile);
            Enumeration<JarEntry> entries = jar.entries();
            while(entries.hasMoreElements()) {
                JarEntry entry = entries.nextElement();
                if (entry.getName().endsWith(".class")) {
                    String clzName = entry.getName().replace('/', '.');
                    clzName = clzName.substring(0, clzName.length() - ".class".length());
                    try {
                        Class clz = clzLoader.loadClass(clzName);
                        Class[] interfaces = clz.getInterfaces();
                        if (interfaces != null) {
                            for (Class pluginClz : interfaces) {
                                if (pluginClz.getName().equals("com.sun.mdm.multidomain.relationship.RelationshipSync")) {
                                    Plugin  plugin = new Plugin(clzName, "relationship", clzName, jarFile.getName());
                                    plugins.add(plugin);
                                    break;
                                }
                                if (pluginClz.getName().equals("com.sun.mdm.multidomain.hierarchy.HierarchySync")) {
                                    Plugin  plugin = new Plugin(clzName, "hierarchy", clzName, jarFile.getName());
                                    plugins.add(plugin);
                                    break;
                                }                                
                            }
                        }
                    } catch (ClassNotFoundException ignore) {                      
                    } catch (NoClassDefFoundError ignore) {
                    }
                }
            }
        } catch(IOException ioex) {          
        }
        return plugins;
    }       
}
