/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2009 NetServices, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.multidomain.sync;

import java.util.EventObject;
import javax.jms.Message;

/**
 * SyncEvent class.
 * @author cye
 */
public class SyncEvent extends EventObject {

    private String domain;
    private String id;
    private String fromEuid;
    private String toEuid;
    private EventType type;
            
    public enum EventType {     
        ADD("ADD"),
        UPD("UPD"),
        MRG("MRG"),
        UNMRG("UNMRG"),
        DEA("DEA"),
        REA("REA"),
        UNKNOWN("UNKNOWN");
         
        private String type;
        
        EventType(String type) {
            this.type = type;
        }
        
        public static EventType[] types() {
            return EventType.values();
        }
    }
       
    public SyncEvent(Message message) {
        super(message);
    }

    public SyncEvent(String domain, Message message) {
        super(message);
        this.domain = domain;
    }
   
    public void setDomain(String domain){
        this.domain = domain;
    }

    public String getDomain(){
        return this.domain;
    }

    public String getId(){
        return this.id;
    }

    public void setId(String id){
        this.id = id;
    }

    public String getFromEuid(){
        return this.fromEuid;
    }
    
    public void setFromEuid(String fromEuid){
        this.fromEuid = fromEuid;
    }
    
    public String getToEuid(){
        return this.toEuid;
    }
    
    public void setToEuid(String toEuid){
        this.toEuid = toEuid;
    }    

    public EventType getType() {
        return this.type;
    }
    
    public void setType(EventType type) {
        this.type = type;
    }
}
