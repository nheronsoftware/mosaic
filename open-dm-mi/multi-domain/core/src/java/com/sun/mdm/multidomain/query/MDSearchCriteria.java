/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.multidomain.query;

import java.util.List;
import java.util.ArrayList;
import java.io.Serializable;
import com.sun.mdm.multidomain.attributes.Attribute;
import com.sun.mdm.multidomain.relationship.RelationshipDef;


/**
 * MultiDomainSearchCriteria class. This class is used to specify different 
 * search critera for relationships.
 * You can filter by Relationship field values, each domain system object field values.
 * The net filter is the union of all EUIDs that satisfy the Relationship and domain
 * search criterias.
 * @author cye
 */
public class MDSearchCriteria implements Serializable {
    /**
     * Relationship defines search relationship attributes. 
     */
    private RelationshipDef relationshipDef;
    private List<Attribute> attributes = null;
    /**
     * SearchCritera constructor.
     */
    public MDSearchCriteria() {
    	attributes = new ArrayList<Attribute>();
    }
    
    /**
     * Set relationtship instance. The non-values are used as filter criteria for 
     * search
     * @param relationship
     */
    public void setRelationshipDef(RelationshipDef relationshipDef) {
    	this.relationshipDef = relationshipDef;
    }

    /**
     * Get relationship instance
     * @return relationship
     */
    public RelationshipDef getRelationshipDef() {
        return relationshipDef;
    }    

    public void setAttributeValues(List<Attribute> attributes) {
    	this.attributes = attributes;
    }

    public List<Attribute> getAttributeValues() {
    	return this.attributes;
    }
    
    public void setAttributeValue(Attribute attribute) {
        if (attributes.isEmpty()) {
            attributes.add(attribute);
        } else if (attribute.getName() != null) {
            boolean isUpdated = false;
            for (Attribute attr : attributes) {
                if(attribute.getName().equals(attr.getName())) {
                    attr.setValue(attribute.getValue());
                    isUpdated = true;
                    break;
                }
            }
            if (isUpdated) {
                attributes.add(attribute);
            }
        }
    }

    public String getAttributeValue(Attribute attribute) {
        Object attributeValue = null;
        if (attribute != null &&
            attribute.getName() != null) {
            for (Attribute attr : attributes) {
                if(attribute.getName().equals(attr.getName())) {
                    attributeValue = attr.getValue();
                    break;
                }
            }
        }
        if (attributeValue != null) {
            return (String)attributeValue;
        } else {
            return null;
        }
    }
}
