package com.sun.mdm.multidomain.ejb.service;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;

public class MasterControllerProxy implements InvocationHandler {

    private ClassLoader domainClassLoader;
    private Object target;
 
    public MasterControllerProxy(Object target, ClassLoader ejbClassLoader, String domainJar) {
        this.target = target;
        this.domainClassLoader = new JarStreamClassLoader(ejbClassLoader, domainJar);
    }

    public Object invoke(Object proxy, Method method, Object[] args)
            throws Throwable {
        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        try {
            Thread.currentThread().setContextClassLoader(domainClassLoader);
            return method.invoke(target, args);
        } catch (IllegalAccessException iex) {
            throw iex;
        } catch (IllegalArgumentException iex) {
            throw iex;
        } catch (InvocationTargetException iex) {
            throw iex;
        } finally {
            Thread.currentThread().setContextClassLoader(contextClassLoader);
        }
    }
}
