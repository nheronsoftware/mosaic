/* *****************************************************************************
 *
 *  Copyright (c) 2011, NetGen Software Inc., All Rights Reserved
 *
 *  This program, and all the routines referenced herein, are the proprietary
 *  properties and trade secrets of NetGen Software Inc.
 *
 *  Except as provided for by license agreement, this program shall not be
 *  duplicated, used, or disclosed without written consent signed by an officer
 *  of NetGen Software Inc.
 *
 * ****************************************************************************/
package com.sun.mdm.multidomain.transaction;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.ResultSet;

/**
 * LockManager class
 * @author cye
 */
public class LockManager {

    private String  FOR_UPDATE = " FOR UPDATE ";

    private String LOCK_STATEMENT = "SELECT RELATIONSHIP_ID FROM RELATIONSHIP " + "WHERE RELATIONSHIP_DEF_ID = ? AND RELATIONSHIP_ID = ? " + FOR_UPDATE;

    public LockManager(){
    }

    public void lock(Connection con, long relDefId, long relId)
        throws LockException {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.prepareStatement(LOCK_STATEMENT);
            int index = 1;
            stmt.setLong(index++, relDefId);
            stmt.setLong(index++, relId);
            rs = stmt.executeQuery();
            int count = 0;
            while (rs.next()) {
                count++;
            }
            if (count != 1) {
                throw new LockException("relationship:[" + count + "] has been modified by another user");
            }
        } catch (SQLException sqlex) {
            throw new LockException(sqlex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch(SQLException ignore) {
                }
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch(SQLException ignore) {
                }
            }
        }
    }
}
