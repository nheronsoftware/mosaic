/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2009 NetGen Software, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.multidomain.sync.impl;

import java.util.List;
import java.util.ArrayList;
import com.sun.mdm.multidomain.relationship.Relationship;
import com.sun.mdm.multidomain.relationship.RelationshipPair;
import com.sun.mdm.multidomain.relationship.RelationshipSync;
import com.sun.mdm.multidomain.relationship.exceptions.RelationshipException;

/**
 * DefaultRelationshipSync class.
 * @author cye
 */
public class DefaultRelationshipSync implements RelationshipSync {

    public List<RelationshipPair> merge(String domain, String fromEUID, String toEUID, List<RelationshipPair> mergedRels)
        throws RelationshipException {
        List<RelationshipPair> solvedRels = mergedRels;
        //TODO
        return solvedRels;
    }
    
    public List<Relationship> unmerge(String domain, String fromEUID, String toEUID, List<Relationship> unmergedRels)
        throws RelationshipException {
        List<Relationship> solvedRels = unmergedRels;
        //TODO
        return solvedRels;
    } 
}
