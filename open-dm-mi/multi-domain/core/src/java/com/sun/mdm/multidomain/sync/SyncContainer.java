/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2009 NetServices, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.multidomain.sync;

import com.sun.mdm.multidomain.ejb.service.MultiDomainMetaService;
import com.sun.mdm.multidomain.ejb.service.MultiDomainService;

/**
 * SyncContainer interface.
 * @author cye
 */
public abstract class SyncContainer {
    
    private String domain;
    private MultiDomainMetaService mdMetaService;
    private MultiDomainService mdService;
    protected boolean initialized = false;
            
    public SyncContainer(String domain) {
        this.domain = domain;
    }
    
    public void setDomain(String domain) {
        this.domain = domain;
    }
    
    public String getDomain(){
        return this.domain;
    }
    
    public boolean isInitialized() {
        return initialized;
    }
    
    public void setMultiDomainMetaService(MultiDomainMetaService mdMetaService) {
        this.mdMetaService = mdMetaService;
    }
    
    public MultiDomainMetaService getMultiDomainMetaService(){
        return this.mdMetaService;
    }
 
    public void setMultiDomainService(MultiDomainService mdService) {
        this.mdService = mdService;
    }
    
    public MultiDomainService getMultiDomainService(){
        return this.mdService;
    }
    
    public abstract void start() throws SyncException;
      
    public abstract void deliverEvent(SyncEvent event) throws SyncException;
    
    public abstract void shutdown() throws SyncException;
    
}
