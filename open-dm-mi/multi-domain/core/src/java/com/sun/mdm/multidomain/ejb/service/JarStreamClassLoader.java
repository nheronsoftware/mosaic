package com.sun.mdm.multidomain.ejb.service;

import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.io.IOException;
import java.io.InputStream;
import java.io.ByteArrayOutputStream;
import java.net.URL;

public class JarStreamClassLoader extends ClassLoader {
  
    private String jarFileName;

    public JarStreamClassLoader(ClassLoader parent, String jarFileName) {
        super(parent);
        this.jarFileName = jarFileName;
    }

    @Override
    protected Class<?> findClass(String name)
        throws ClassNotFoundException {
        try {
            URL url = this.getParent().getResource(jarFileName);
            if (url == null) {
                throw new ClassNotFoundException(name);
            }
            ZipFile jarFile = new ZipFile(url.getPath());
            ZipEntry entry = jarFile.getEntry(name.replace('.', '/') + ".class");
            if (entry == null) {
                throw new ClassNotFoundException(name);
            }
            int size = (int) entry.getSize();
            byte[] array = new byte[size];
            InputStream in = jarFile.getInputStream(entry);
            ByteArrayOutputStream out = new ByteArrayOutputStream(array.length);
            int length = in.read(array);
            while (length > 0) {
                out.write(array, 0, length);
                length = in.read(array);
            }
            return defineClass(name, out.toByteArray(), 0, out.size());
        } catch (IOException exception) {
            throw new ClassNotFoundException(name, exception);
        }
    }

 
}
