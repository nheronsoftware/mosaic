/* *****************************************************************************
 *
 *  Copyright (c) 2011, NetGen Software Inc., All Rights Reserved
 *
 *  This program, and all the routines referenced herein, are the proprietary
 *  properties and trade secrets of NetGen Software Inc.
 *
 *  Except as provided for by license agreement, this program shall not be
 *  duplicated, used, or disclosed without written consent signed by an officer
 *  of NetGen Software Inc.
 *
 * ****************************************************************************/
package com.sun.mdm.multidomain.transaction;

import java.util.Date;
import java.io.Serializable;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.IOException;
import com.sun.mdm.multidomain.relationship.Relationship;

/**
 * TransactionLogDto class
 * @author cye
 */
public class TransactionLogDto implements Serializable {
    public static final String ADD_ACTION = "ADD";
    public static final String UPDATE_ACTION = "UPDATE";
    public static final String DELETE_ACTION = "DELETE";
    
    private long transactionId;
    private long relationshipDefId;
    private long relationshipId;
    private String action;
    private String systemUser;
    private Date timeStamp;
    private String delta;

    public void TransactionLog(){
    }

    public void TransactionLog(long transactionId,
                               long relationshipDefId,
                               long relationshipId,
                               String action,
                               String systemUser,
                               Date timeStamp,
                               String delta) {
        this.transactionId = transactionId;
        this.relationshipDefId = relationshipDefId;
        this.relationshipId = relationshipId;
        this.action = action;
        this.systemUser = systemUser;
        this.timeStamp = timeStamp;
        this.delta = delta;
    }

    public long getTransactionId() {
        return this.transactionId;
    }

    public long getRelationshipDefId() {
        return this.relationshipDefId;
    }

    public long getRelationshipId() {
        return this.relationshipId;
    }

    public String getAction() {
        return this.action;
    }

    public String getSystemUser(){
        return this.systemUser;
    }

    public Date getTimeStamp() {
        return this.timeStamp;
    }

    public String getDelta() {
        return this.delta;
    }

    public void setTransactionId(long transactionId) {
        this.transactionId = transactionId;
    }

    public void setRelationshipDefId(long relationshipDefId) {
        this.relationshipDefId = relationshipDefId;
    }

    public void setRelationshipId(long relationshipId) {
        this.relationshipId = relationshipId;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public void setSystemUser(String systemUser){
        this.systemUser = systemUser;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    public void setDelta(String delta) {
        this.delta = delta;
    }

    public static String toClob(Relationship rel)
        throws TransactionLogException {
        try {
            rel.setRelationshipDef(null);
            ByteArrayOutputStream buf = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(buf);
            out.writeObject(rel);
            out.close();
            return buf.toString("ISO-8859-1");
        } catch (IOException ioex) {
            throw new TransactionLogException("Could not serialize " +
                                              "transaction log to a String", ioex);
        }
    }

   public static Relationship fromClob(String clob)
        throws TransactionLogException {
        Relationship rel = null;
        try {
            ByteArrayInputStream buf = new ByteArrayInputStream(clob.getBytes("ISO-8859-1"));
            ObjectInputStream in = new ObjectInputStream(buf);
            rel = (Relationship) in.readObject();
        } catch (Exception ioex) {
            throw new TransactionLogException("Could not retrieve " +
                                              "transaction log from the Clob", ioex);
        }
        return rel;
    }
}