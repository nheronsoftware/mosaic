/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2009 NetServices, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.multidomain.sync;

import com.sun.mdm.multidomain.parser.Plugin;

/**
 * Registory class.
 * @author cye
 */
public class Registry {

    public static final String MULTIDOMAIN_MODEL_XML = "MultiDomainModel.xml";
    
    private static Registry instance;
    private RegistryStrategy<Plugin> strategy;
            
    private Registry(){
    }
    
    public static Registry getInstance() {
         if (instance == null) {
            instance = new Registry();
            instance.initialize();
        }
        return instance;
    }
    
    public void initialize(){
        strategy = new InMemoryRegistryStrategy();
        strategy.initialize();
    }
    
    public void addEntry(Plugin plugin) {
        strategy.addEntry(plugin);
    }
    
    public void updateEntry(Plugin plugin) {
        strategy.updateEntry(plugin);
    }
    
    public void removeEntry(String Id) {
        strategy.removeEntry(Id);
    }
    
    public Plugin getEntry(String Id){
        return strategy.getEntry(Id);
    }
 
    public boolean containsEntry(String Id) {
        return strategy.containsEntry(Id);
    }
    
}
