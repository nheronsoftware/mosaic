/* *****************************************************************************
 *
 *  Copyright (c) 2011, NetGen Software Inc., All Rights Reserved
 *
 *  This program, and all the routines referenced herein, are the proprietary
 *  properties and trade secrets of NetGen Software Inc.
 *
 *  Except as provided for by license agreement, this program shall not be
 *  duplicated, used, or disclosed without written consent signed by an officer
 *  of NetGen Software Inc.
 *
 * ****************************************************************************/
package com.sun.mdm.multidomain.query;

import java.util.List;
import java.util.ArrayList;
import com.sun.mdm.multidomain.relationship.Relationship;
import com.sun.mdm.multidomain.attributes.Attribute;

/**
 * QueryFilter class.
 * @author cye
 */
public class QueryFilter {

     public static List<Relationship> doFilter(List<Relationship> rels,
                                               List<Attribute> filters) {
        List<Relationship> relationships = new ArrayList<Relationship>();
        if (filters == null || filters.isEmpty()) {
            return rels;
        }
        for (Relationship rel : rels) {
            boolean filtered = false;
            for (Attribute filter : filters) {
                if (rel.isAttribute(filter)) {
                    String attributeValue = rel.getAttributeValue(filter);
                    if (attributeValue != null) {
                        if (!attributeValue.equals(filter.getValue())) {
                            filtered = true;
                            break;
                        }
                    } else if (filter.getValue() != null) {
                            filtered = true;
                            break;
                    }
                }
            }
            if (!filtered) {
                relationships.add(rel);
            }
        }
        return relationships;
    }
     public static Relationship doFilter(Relationship rel,
                                         List<Attribute> filters) {
     
         if (filters == null || filters.isEmpty()) {
             return rel;
         }
         boolean filtered = false;
         for (Attribute filter : filters) {
             if (rel.isAttribute(filter)) {
                 String attributeValue = rel.getAttributeValue(filter);
                 if (attributeValue != null) {
                     if (!attributeValue.equals(filter.getValue())) {
                         filtered = true;
                         break;
                     }
                 } else if (filter.getValue() != null) {
                     filtered = true;
                     break;
                 }
             }
         }
         if (filtered) {
             return null;
         } else {
             return rel;
         }
    }
}
