/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2009 NetServices, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.multidomain.sync;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import javax.jms.Message;
import javax.jms.TextMessage;
import javax.jms.JMSException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
        
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.DOMException;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.sun.mdm.multidomain.sync.SyncEvent.EventType;
import com.sun.mdm.multidomain.parser.MultiDomainModel;
import com.sun.mdm.multidomain.parser.Utils;
import com.sun.mdm.multidomain.parser.ParserException;

/**
 * SyncUtils class.
 * @author cye
 */
public class SyncUtils {
  
    private static DocumentBuilderFactory docFactory;
    private static DocumentBuilder docBuilder;
    static {
        try {
            docFactory = DocumentBuilderFactory.newInstance();
            docBuilder = docFactory.newDocumentBuilder();
        } catch (ParserConfigurationException pex) {
        }
    }
    public static final String TagOutMsg = "tns:OutMsg";
    public static final String TagEvent = "Event";
    public static final String TagId = "ID";
    public static final String TagSBR = "tns:SBR";
    public static final String TagEUID = "EUID";        
    public static final String MergeMessage = "MRG";
    public static final String UnmergeMessage = "UNMRG";
    public static final String UnknownMessage = "UNKNOWN";
      
    public static final SyncEvent pasrseMessage(Message message) 
        throws ParserException {
        if (!(message instanceof TextMessage)) {
            throw new ParserException("Invalid sync message type.");
        }
        SyncEvent event = new SyncEvent(message);
        try {
            String xmlData = ((TextMessage)message).getText();
            InputSource source = new InputSource(new StringReader(xmlData)); 
            Document doc = docBuilder.parse(source);
            Element root = doc.getDocumentElement(); 
            if (TagOutMsg.equals(root.getTagName())) {
                String type = getAttributeValue(root, TagEvent);
                String id = getAttributeValue(root, TagId);
                if (MergeMessage.equals(type) ||
                    UnmergeMessage.equals(type)) {
                    event.setType(EventType.valueOf(type));
                    event.setId(id);
                    NodeList children = root.getElementsByTagName(TagSBR);
                    if (children.getLength() < 2) {
                        throw new ParserException("Invalid MRG message."); 
                    } else {
                        Node nodeA = children.item(0);
                        String fromEuid = getAttributeValue(nodeA, TagEUID);
                        event.setFromEuid(fromEuid);
                        Node nodeB = children.item(1);
                        String toEuid = getAttributeValue(nodeB, TagEUID);
                        event.setToEuid(toEuid);
                    }
                } else {
                     event.setType(EventType.valueOf(UnknownMessage));
                }
            } else {
                throw new ParserException("Invalid sync message root.");
            }      
        } catch (JMSException jmsex) {
            throw new ParserException(jmsex);
        } catch (IOException ioex) {
             throw new ParserException(ioex);
        } catch (SAXException saxex) {
             throw new ParserException(saxex);
        }
        return event;
    }
    
    private static String getAttributeValue(Node node, String attrName) {
        String value = null;
        NamedNodeMap nnm = node.getAttributes();
        if (nnm != null) {
            Node attrNode = nnm.getNamedItem(attrName);
            if (attrNode != null) {
                try {
                    value = attrNode.getNodeValue();
                } catch (DOMException ex) {
                }
            }
        }
        return value;
    }
       
    public static final MultiDomainModel getMultiDomainModel() 
        throws ParserException {
        InputStream stream = null;
        stream = Registry.class.getClassLoader().getResourceAsStream(Registry.MULTIDOMAIN_MODEL_XML);
        InputSource source = new InputSource(stream);
        MultiDomainModel multiDomainModel = Utils.parseMultiDomainModel(source);
        return multiDomainModel;
    }

}
