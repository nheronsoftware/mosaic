/* *****************************************************************************
 *
 *  Copyright (c) 2011, NetGen Software Inc., All Rights Reserved
 *
 *  This program, and all the routines referenced herein, are the proprietary
 *  properties and trade secrets of NetGen Software Inc.
 *
 *  Except as provided for by license agreement, this program shall not be
 *  duplicated, used, or disclosed without written consent signed by an officer
 *  of NetGen Software Inc.
 *
 * ****************************************************************************/
package com.sun.mdm.multidomain.transaction;

/**
 * TransactionLogException class.
 * @author cye
 */
public class TransactionLogException extends Exception {

    public TransactionLogException() {
        super();
    }

    public TransactionLogException(String m) {
        super(m);
    }

    public TransactionLogException(String m, Throwable t) {
        super(m, t);
    }

    public TransactionLogException(Throwable t) {
        super(t);
    }
}
