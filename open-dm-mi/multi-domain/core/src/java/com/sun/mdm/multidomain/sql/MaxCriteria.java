/* *****************************************************************************
 *
 *  Copyright (c) 2011, NetGen Software Inc., All Rights Reserved
 *
 *  This program, and all the routines referenced herein, are the proprietary
 *  properties and trade secrets of NetGen Software Inc.
 *
 *  Except as provided for by license agreement, this program shall not be
 *  duplicated, used, or disclosed without written consent signed by an officer
 *  of NetGen Software Inc.
 *
 * ****************************************************************************/
package com.sun.mdm.multidomain.sql;

/**
 * MaxCriteria class.
 * @author cye
 */
public class MaxCriteria extends Criteria {

    private final String columnName;

    public MaxCriteria(String columnName) {
        this.columnName = columnName;
    }

    public String write() {
        StringBuffer sb = new StringBuffer();
        sb.append(" MAX");
        sb.append("(");
        sb.append(columnName);
        sb.append(")");
        return sb.toString();
    }
}
