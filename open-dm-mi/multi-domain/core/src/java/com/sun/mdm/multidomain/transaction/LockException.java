/* *****************************************************************************
 *
 *  Copyright (c) 2011, NetGen Software Inc., All Rights Reserved
 *
 *  This program, and all the routines referenced herein, are the proprietary
 *  properties and trade secrets of NetGen Software Inc.
 *
 *  Except as provided for by license agreement, this program shall not be
 *  duplicated, used, or disclosed without written consent signed by an officer
 *  of NetGen Software Inc.
 *
 * ****************************************************************************/
package com.sun.mdm.multidomain.transaction;

/**
 * LockException class.
 * @author cye
 */
public class LockException extends Exception {

    public LockException() {
        super();
    }

    public LockException(String m) {
        super(m);
    }

    public LockException(String m, Throwable t) {
        super(m, t);
    }

    public LockException(Throwable t) {
        super(t);
    }
}

