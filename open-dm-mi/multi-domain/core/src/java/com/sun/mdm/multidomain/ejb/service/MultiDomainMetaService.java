/* *****************************************************************************
 *
 *  Copyright (c) 2011, NetGen Software Inc., All Rights Reserved
 *
 *  This program, and all the routines referenced herein, are the proprietary
 *  properties and trade secrets of NetGen Software Inc.
 *
 *  Except as provided for by license agreement, this program shall not be
 *  duplicated, used, or disclosed without written consent signed by an officer
 *  of NetGen Software Inc.
 *
 * ****************************************************************************/
package com.sun.mdm.multidomain.ejb.service;

import java.util.List;
import com.sun.mdm.index.master.ProcessingException;
import com.sun.mdm.index.master.UserException;
import com.sun.mdm.multidomain.relationship.RelationshipDef;
        
/**
 * MultiDomainMetaService interface.
 * @author cye
 */
public interface MultiDomainMetaService {
    
    /**
     * Get all domain names.
     * @return List<String> An array of domain.
     * @exception ProcessingException Thrown if an error occurs during processing.
     */
    public List<String> getDomains()
        throws ProcessingException;
    
    /**
     * Get all relational Defs Defs for all domains.
     * @return List<RelationshipDef> An array of relationship Def.
     * @throws ProcessingException Thrown if an error occurs during processing.
     */
    public List<RelationshipDef> getRelationshipDefs()
        throws ProcessingException;
    
    /**
     * Get all relationship Defs of the given source domain and target domains.
     * @param sourceDomain Source domain name.
     * @param targetDomain Target domain name.
     * @return List<RelationshipDef> An array of relationship Def.
     * @throws ProcessingException Thrown if an error occurs during processing.
     * @throws UserException Thrown if an invalid source domain or target domain 
     * is passed as a parameter.
     */
    public List<RelationshipDef> getRelationshipDefsByDomain(String sourceDomain,
                                                             String targetDomain)
        throws ProcessingException, UserException;
    
    /**
     * Get a relationshipDef of the given name and source domain and target domains.
     * @param name Relationship name.
     * @param sourceDomain Source domain name.
     * @param targetDomain Target domain name.
     * @return RelationshipDef RelationshipDef.
     * @throws ProcessingException Thrown if an error occurs during processing.
     * @throws UserException Thrown if an invalid source domain or target domain 
     * is passed as a parameter.
     */
    public RelationshipDef getRelationshipDefByName(String name, 
                                                    String sourceDomain,
                                                    String targetDomain)
        throws ProcessingException, UserException;  
    
    /**
     * Get a relationshipDef of the given relationship Id.
     * @param relationshipDefId RelationshipDefId.
     * @return RelationshipDef  RelationshipDef.
     * @throws ProcessingException Thrown if an error occurs during processing.
     * @throws UserException Thrown if an invalid source domain or target domain 
     * is passed as a parameter.
     */
    public RelationshipDef getRelationshipDefById(long relationshipDefId) 
        throws ProcessingException, UserException;  
    
    
    /**
     * Create a relationship Def and persist the relationship Def in the database.
     * @param relationshipDef RelationshipDef.
     * @return long Relationship Def identifier which is newly created. 
     * @throws ProcessingException Thrown if an error occurs during processing.
     * @throws UserException Thrown if an invalid relationship Def is passed as a parameter.
     */
    public long createRelationshipDef(RelationshipDef relationshipDef)  
        throws ProcessingException, UserException;
    
    /**
     * Update an existing relationship Def and persist in the database.
     * @param relationshipDef RelationshipDef.
     * @throws ProcessingException Thrown if an error occurs during processing.
     * @throws UserException Thrown if an invalid relationship Def is passed as a parameter.
     */
    public void updateRelationshipDef(RelationshipDef relationshipDef)  
        throws ProcessingException, UserException;
    
    /**
     * Delete an existing relation Def from the database.
     * @param relationshipDef RelationshipDef.
     * @throws ProcessingException Thrown if an error occurs during processing.
     * @throws UserException Thrown if an invalid relationship Def is passed as a parameter.
     */
    public void deleteRelationshipDef(RelationshipDef relationshipDef)  
        throws ProcessingException, UserException;
}
