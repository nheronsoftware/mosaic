/* *****************************************************************************
 *
 *  Copyright (c) 2011, NetGen Software Inc., All Rights Reserved
 *
 *  This program, and all the routines referenced herein, are the proprietary
 *  properties and trade secrets of NetGen Software Inc.
 *
 *  Except as provided for by license agreement, this program shall not be
 *  duplicated, used, or disclosed without written consent signed by an officer
 *  of NetGen Software Inc.
 *
 * ****************************************************************************/
package com.sun.mdm.multidomain.transaction;

import java.util.List;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.io.StringReader;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.sql.Clob;
import java.sql.SQLException;
import com.sun.mdm.multidomain.sql.DBSchema.DBTable;
import com.sun.mdm.multidomain.sql.DBSchema.RELATIONSHIP_TRANSACTION;
import com.sun.mdm.multidomain.sql.AND;
import com.sun.mdm.multidomain.sql.Criteria;
import com.sun.mdm.multidomain.sql.MaxCriteria;
import com.sun.mdm.multidomain.sql.DeleteBuilder;
import com.sun.mdm.multidomain.sql.InCriteria;
import com.sun.mdm.multidomain.sql.InsertBuilder;
import com.sun.mdm.multidomain.sql.JoinCriteria;
import com.sun.mdm.multidomain.sql.MatchCriteria.OPERATOR;
import com.sun.mdm.multidomain.sql.MatchCriteria;
import com.sun.mdm.multidomain.sql.OR;
import com.sun.mdm.multidomain.sql.OrderBy;
import com.sun.mdm.multidomain.sql.Parameter;
import com.sun.mdm.multidomain.sql.SQLBuilder;
import com.sun.mdm.multidomain.sql.SelectBuilder;
import com.sun.mdm.multidomain.sql.UpdateBuilder;

/**
 * TransactionLogDao class
 * @author cye
 */
public class TransactionLogDao {

    static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    static final String DB_DATE_FORMAT = "yyyy-MM-dd hh24:MI:ss";

    private Connection conn;
  
    public TransactionLogDao(Connection conn){
        this.conn = conn;
    }

    public void createTransatcionLog(TransactionLogDto transLog)
        throws TransactionLogException {
        try {
            InsertBuilder insert = new InsertBuilder();
            insert.setTable(RELATIONSHIP_TRANSACTION.getTableName());
            for (RELATIONSHIP_TRANSACTION c : RELATIONSHIP_TRANSACTION.values()) {
                insert.addColumns(c.columnName);
            }
            String sqlStr = SQLBuilder.buildSQL(insert);
            int[] columns = {1};
            PreparedStatement stmt = conn.prepareStatement(sqlStr, columns);
            int index = 1;
            stmt.setNull(index++, java.sql.Types.NULL);
            stmt.setLong(index++, transLog.getRelationshipDefId());
            stmt.setLong(index++, transLog.getRelationshipId());
            stmt.setString(index++, transLog.getAction());
            stmt.setString(index++, transLog.getSystemUser());
            Date dt = transLog.getTimeStamp();
            stmt.setTimestamp(index++, dt != null ? new Timestamp(dt.getTime()) : null);
            String delta = transLog.getDelta();
            StringReader reader = new StringReader(delta);
            stmt.setClob(index++, reader, delta != null? delta.length() : 0);
            int rows = stmt.executeUpdate();
        } catch (SQLException sqlex) {
            throw new TransactionLogException(sqlex);
        }
    }

    public TransactionLogDto getTransactionLog(long relDefId, long relId)
      throws TransactionLogException {
      try {
            SelectBuilder selBuilder1 = new SelectBuilder();
            selBuilder1.setTable(RELATIONSHIP_TRANSACTION.getTableName());
            for (RELATIONSHIP_TRANSACTION c : RELATIONSHIP_TRANSACTION.values()) {
                selBuilder1.addColumns(c.columnName);
            }

            SelectBuilder selBuilder2 = new SelectBuilder();
            selBuilder2.setTable(RELATIONSHIP_TRANSACTION.getTableName());
            Criteria c1 = new MaxCriteria(RELATIONSHIP_TRANSACTION.TIMESTAMP.columnName);
            selBuilder2.addColumns(c1.write());
            Criteria c2 = new Parameter(RELATIONSHIP_TRANSACTION.RELATIONSHIP_DEF_ID.columnName);
            Criteria c3 = new Parameter(RELATIONSHIP_TRANSACTION.RELATIONSHIP_ID.columnName);
            selBuilder2.addCriteria(new AND(c2,c3));
            String sqlStr2 = SQLBuilder.buildSQL(selBuilder2);
            sqlStr2 = "(" + sqlStr2 + ")";
            
            Criteria c4 = new MatchCriteria(RELATIONSHIP_TRANSACTION.TIMESTAMP.columnName, MatchCriteria.OPERATOR.EQUALS, sqlStr2);
            String sqlStr1 = SQLBuilder.buildSQL(selBuilder1);
            
            PreparedStatement stmt = conn.prepareStatement(sqlStr1);

            int index = 1;
            stmt.setLong(index++, relDefId);
            stmt.setLong(index++, relId);

            ResultSet rs = stmt.executeQuery();
            List<TransactionLogDto> transLogs = fetchResults(rs);
            if (transLogs.isEmpty()) {
               return null;
            } else{
                return transLogs.get(0);
            }
        } catch (SQLException sqlex) {
            throw new TransactionLogException(sqlex);
        }
    }

    public TransactionLogDto getTransactionLog(long transactionId)
      throws TransactionLogException {
      try {
            SelectBuilder selBuilder = new SelectBuilder();
            selBuilder.setTable(RELATIONSHIP_TRANSACTION.getTableName());
            for (RELATIONSHIP_TRANSACTION c : RELATIONSHIP_TRANSACTION.values()) {
                selBuilder.addColumns(c.columnName);
            }
            Criteria c1 = new Parameter(RELATIONSHIP_TRANSACTION.TRANSACTION_ID.columnName);
            selBuilder.addCriteria(c1);
            String sqlStr = SQLBuilder.buildSQL(selBuilder);
            PreparedStatement stmt = conn.prepareStatement(sqlStr);

            int index = 1;
            stmt.setLong(index++, transactionId);

            ResultSet rs = stmt.executeQuery();
            List<TransactionLogDto> transLogs = fetchResults(rs);
            if (transLogs.isEmpty()) {
                return null;
            } else {
                return transLogs.get(0);
            }
        } catch (SQLException sqlex) {
            throw new TransactionLogException(sqlex);
        }
    }

    public List<TransactionLogDto> getTransactionLogs(long relDefId, long relId)
       throws TransactionLogException {
       try {
            SelectBuilder selBuilder = new SelectBuilder();
            selBuilder.setTable(RELATIONSHIP_TRANSACTION.getTableName());
            for (RELATIONSHIP_TRANSACTION c : RELATIONSHIP_TRANSACTION.values()) {
                selBuilder.addColumns(c.columnName);
            }
            Criteria c1 = new Parameter(RELATIONSHIP_TRANSACTION.RELATIONSHIP_DEF_ID.columnName);
            Criteria c2 = new Parameter(RELATIONSHIP_TRANSACTION.RELATIONSHIP_ID.columnName);
            selBuilder.addCriteria(new AND(c1,c2));

            String sqlStr = SQLBuilder.buildSQL(selBuilder);
            PreparedStatement stmt = conn.prepareStatement(sqlStr);

            int index = 1;
            stmt.setLong(index++, relDefId);
            stmt.setLong(index++, relId);
         
            ResultSet rs = stmt.executeQuery();
            List<TransactionLogDto> transLogs = fetchResults(rs);
            return transLogs;
        } catch (SQLException sqlex) {
            throw new TransactionLogException(sqlex);
        }
    }

    public List<TransactionLogDto> searchTransactionLogs(long relDefId, Date begin, Date end)
       throws TransactionLogException {
       try {
            SelectBuilder selBuilder = new SelectBuilder();
            selBuilder.setTable(RELATIONSHIP_TRANSACTION.getTableName());
            for (RELATIONSHIP_TRANSACTION c : RELATIONSHIP_TRANSACTION.values()) {
                selBuilder.addColumns(c.columnName);
            }
            Criteria c1 = new Parameter(RELATIONSHIP_TRANSACTION.RELATIONSHIP_DEF_ID.columnName);
            Criteria c2 = null;
            Criteria c3 = null;
            if (begin != null) {
                c2 = new MatchCriteria("timestamp",OPERATOR.GREATEREQUAL,"?");
            }
            if (end != null) {
                c3 = new MatchCriteria("timestamp",OPERATOR.LESSEQUAL, "?");
            }
            PreparedStatement stmt = null;
            if (c2 != null && c3 != null) {
                selBuilder.addCriteria(new AND(c1, c2, c3));
                String sqlStr = SQLBuilder.buildSQL(selBuilder);
                stmt = conn.prepareStatement(sqlStr);
                int index = 1;
                stmt.setLong(index++, relDefId);
                stmt.setTimestamp(index++, new Timestamp(begin.getTime()));
                stmt.setTimestamp(index++, new Timestamp(end.getTime()));
            } else if (c2 != null) {
                selBuilder.addCriteria(new AND(c1, c2));
                String sqlStr = SQLBuilder.buildSQL(selBuilder);
                stmt = conn.prepareStatement(sqlStr);
                int index = 1;
                stmt.setLong(index++, relDefId);
                stmt.setTimestamp(index++, new Timestamp(begin.getTime()));
            } else if (c3 != null) {
                selBuilder.addCriteria(new AND(c1, c3));
                String sqlStr = SQLBuilder.buildSQL(selBuilder);
                stmt = conn.prepareStatement(sqlStr);
                int index = 1;
                stmt.setLong(index++, relDefId);
                stmt.setTimestamp(index++, new Timestamp(end.getTime()));
            } else {
                selBuilder.addCriteria(c1);
                String sqlStr = SQLBuilder.buildSQL(selBuilder);
                stmt = conn.prepareStatement(sqlStr);
                int index = 1;
                stmt.setLong(index++, relDefId);
            }
            ResultSet rs = stmt.executeQuery();
            List<TransactionLogDto> transLogs = fetchResults(rs);
            return transLogs;
        } catch (SQLException sqlex) {
            throw new TransactionLogException(sqlex);
        }
    }

    private List<TransactionLogDto> fetchResults(ResultSet rs)
        throws SQLException {
        List<TransactionLogDto> transLogs = new ArrayList<TransactionLogDto>();
        while (rs.next()) {
            TransactionLogDto transLog = new TransactionLogDto();
            transLog.setTransactionId(rs.getLong(RELATIONSHIP_TRANSACTION.TRANSACTION_ID.columnName));
            transLog.setRelationshipDefId(rs.getLong(RELATIONSHIP_TRANSACTION.RELATIONSHIP_DEF_ID.columnName));
            transLog.setRelationshipId(rs.getLong(RELATIONSHIP_TRANSACTION.RELATIONSHIP_ID.columnName));
            transLog.setAction(rs.getString(RELATIONSHIP_TRANSACTION.ACTION.columnName));
            transLog.setSystemUser(rs.getString(RELATIONSHIP_TRANSACTION.SYSTTEMUSER.columnName));
            transLog.setTimeStamp(rs.getDate(RELATIONSHIP_TRANSACTION.TIMESTAMP.columnName));
            Clob cdelta = rs.getClob(RELATIONSHIP_TRANSACTION.DELTA.columnName);
            String delta = clobToString(cdelta);
            transLog.setDelta(delta);
            transLogs.add(transLog);
        }
        return transLogs;
    }

    private String clobToString(Clob cdelta)
        throws SQLException {
        try {
            if (cdelta == null) {
                return null;
            }
            StringBuffer sbuf = new StringBuffer();
            InputStream is = cdelta.getAsciiStream();
            if (is == null) {
                return null;
            } else {
                byte buffer[] = new byte[64];
                int c = is.read(buffer);
                while (c > 0) {
                    sbuf.append(new String(buffer, 0, c));
                    c = is.read(buffer);
                }
                return sbuf.toString();
            }
        } catch (IOException ioex) {
            throw new SQLException(ioex);
        }
    }

    private String toDateString(Date date) {
    	SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
    	return " to_date('"+ sdf.format(date) + "', '" + DB_DATE_FORMAT + "') ";
    }
}

  