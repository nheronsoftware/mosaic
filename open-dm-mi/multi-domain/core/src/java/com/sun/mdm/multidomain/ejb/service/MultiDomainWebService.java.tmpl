/* *****************************************************************************
 *
 *  Copyright (c) 2011, NetGen Software Inc., All Rights Reserved
 *
 *  This program, and all the routines referenced herein, are the proprietary
 *  properties and trade secrets of NetGen Software Inc.
 *
 *  Except as provided for by license agreement, this program shall not be
 *  duplicated, used, or disclosed without written consent signed by an officer
 *  of NetGen Software Inc.
 *
 * ****************************************************************************/
package com.sun.mdm.multidomain.ejb.service;

import java.util.List;
import java.util.Date;
import javax.annotation.security.RunAs;
import javax.ejb.EJB;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.jws.WebMethod;
import javax.jws.WebResult;
import com.sun.mdm.index.master.ProcessingException;
import com.sun.mdm.index.master.UserException;
import com.sun.mdm.multidomain.relationship.Relationship;
import com.sun.mdm.multidomain.relationship.LocalId;
import com.sun.mdm.multidomain.query.MDSearchCriteria;
import com.sun.mdm.index.objects.SystemObject;
import com.sun.mdm.index.objects.ObjectNode;
import com.sun.mdm.multidomain.transaction.TransactionLog;
import com.sun.mdm.index.objects.SOURCE_DOMAIN_TOKEN_Object;
import com.sun.mdm.index.objects.TARGET_DOMAIN_TOKEN_Object;
import com.sun.mdm.index.webservice.SOURCE_DOMAIN_TOKEN_Bean;
import com.sun.mdm.index.webservice.TARGET_DOMAIN_TOKEN_Bean;
import com.sun.mdm.index.webservice.SystemSOURCE_DOMAIN_TOKEN_;
import com.sun.mdm.index.webservice.SystemTARGET_DOMAIN_TOKEN_;

/**
 * MultiDomainWebService
 * @author cye
 */
@WebService()
@Stateless()
@TransactionManagement(TransactionManagementType.CONTAINER)
@RunAs("MasterIndex.Admin")

public class MultiDomainWebService {
    @EJB(beanInterface=MultiDomainServiceLocal.class)
    private MultiDomainServiceLocal target; 
    
    /**
     * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#createRelationship()
     */        
    @WebMethod
    @WebResult(name="relationshipId")
    public long createRelationship(@WebParam(name = "relationship") Relationship relationship)
    throws ProcessingException, UserException {
        return target.createRelationship(relationship);  
    }

    /**
     * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#createRelationship()
     */            
    @WebMethod
    @WebResult(name="relationshipId")
    public long createRelationshipByDomain(@WebParam(name = "sourceDomain") String sourceDomain,
                                           @WebParam(name = "sourceLID") LocalId sourceLID,
                                           @WebParam(name = "targetDomain") String targetDomain,
                                           @WebParam(name = "targetLID") LocalId targetLID,
                                           @WebParam(name = "relationship") Relationship relationship)
        throws ProcessingException, UserException {
        return target.createRelationshipByDomain(sourceDomain, sourceLID, targetDomain, targetLID, relationship);
    }
  
    /**
     * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#updateRelationship()
     */ 
    @WebMethod
    public void updateRelationship(@WebParam(name = "relationship") Relationship relationship)
       throws ProcessingException, UserException {
        target.updateRelationship(relationship);
    }
    
    /**
     * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#deleteRelationship()
     */   
    @WebMethod
    public void deleteRelationship(@WebParam(name = "relationship") Relationship relationship)
        throws ProcessingException, UserException {
        target.deleteRelationship(relationship);
    }

       /**
     * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#deleteRelationship()
     */
    @WebMethod
    public void deleteRelationshipById(@WebParam(name = "relationshipId") long relationshipId)
        throws ProcessingException, UserException {
        target.deleteRelationshipById(relationshipId);
    }

    /**
     * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#deleteRelationship()
     */
    @WebMethod
    public void deleteRelationshipByDomain(@WebParam(name = "sourceDomain") String sourceDomain,
                                           @WebParam(name = "sourceLID") LocalId sourceLID,
                                           @WebParam(name = "targetDomain") String targetDomain,
                                           @WebParam(name = "targetLID") LocalId targetLID,
                                           @WebParam(name = "relationship") Relationship relationship)
        throws ProcessingException, UserException {
        target.deleteRelationshipByDomain(sourceDomain, sourceLID, targetDomain, targetLID, relationship);
    }

   /**
    * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#getRelationship()
    */
    @WebMethod
    @WebResult(name="relationship")
    public  Relationship getRelationship(@WebParam(name = "relationship") Relationship relationship)
        throws ProcessingException, UserException {
        return target.getRelationship(relationship);
    }

   /**
    * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#getRelationshipByDomain()
    */
    @WebMethod
    @WebResult(name="relationship")
    public Relationship getRelationshipByDomain(@WebParam(name = "sourceDomain") String sourceDomain,
                                                @WebParam(name = "sourceLID") LocalId sourceLID,
                                                @WebParam(name = "targetDomain") String targetDomain,
                                                @WebParam(name = "targetLID") LocalId targetLID,
                                                @WebParam(name = "relationship") Relationship relationship)
        throws ProcessingException, UserException {
        return target.getRelationshipByDomain(sourceDomain, sourceLID, targetDomain, targetLID, relationship);
    }

   /**
    * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#searchRelationships()
    */
    @WebMethod
    @WebResult(name="relationships")
    public List<Relationship> searchRelationships(@WebParam(name = "domain") String domain,
                                                  @WebParam(name = "euid") String euid,
                                                  @WebParam(name = "mdSearchCriteria") MDSearchCriteria searchCriteria)
        throws ProcessingException, UserException {
        return target.searchRelationships(domain, euid, searchCriteria);
    }


   /**
    * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#searchRelationshipByDomain()
    */
    @WebMethod
    @WebResult(name="relationships")
    public List<Relationship> searchRelationshipsByDomain(@WebParam(name = "domain") String domain,
                                                          @WebParam(name = "localId") LocalId localId,
                                                          @WebParam(name = "mdSearchCriteria") MDSearchCriteria searchCriteria)
       throws ProcessingException, UserException {
       return target.searchRelationshipsByDomain(domain, localId, searchCriteria);
    }


   /**
    * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#searchRelationshipsByDomainObject()
    */
    @WebMethod
    @WebResult(name="relationships")
    public List<Relationship> searchRelationshipsBySOURCE_DOMAIN_TOKEN_Domain(@WebParam(name = "sourceDomainBean") SOURCE_DOMAIN_TOKEN_Bean sourceDomainBean,
                                                                              @WebParam(name = "mdSearchCriteria") MDSearchCriteria searchCriteria)
        throws ProcessingException, UserException {
        SystemSOURCE_DOMAIN_TOKEN_ systemSOURCE_DOMAIN_TOKEN_ = new SystemSOURCE_DOMAIN_TOKEN_();
        systemSOURCE_DOMAIN_TOKEN_.setSOURCE_DOMAIN_TOKEN_(sourceDomainBean);
        SystemObject sourceSystemObject = systemSOURCE_DOMAIN_TOKEN_.pGetSystemObject();
        return target.searchRelationshipsByDomainObject(sourceSystemObject, searchCriteria);
    }


  /**
    * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#searchRelationshipsByDomainObject()
    */
    @WebMethod
    @WebResult(name="relationships")
    public List<Relationship> searchRelationshipsByTARGET_DOMAIN_TOKEN_Domain(@WebParam(name = "targetDomainBean") TARGET_DOMAIN_TOKEN_Bean targetDomainBean,
                                                                              @WebParam(name = "mdSearchCriteria") MDSearchCriteria searchCriteria)
        throws ProcessingException, UserException {
        SystemTARGET_DOMAIN_TOKEN_ systemTARGET_DOMAIN_TOKEN_ = new SystemTARGET_DOMAIN_TOKEN_();
        systemTARGET_DOMAIN_TOKEN_.setTARGET_DOMAIN_TOKEN_(targetDomainBean);
        SystemObject targetSystemObject = systemTARGET_DOMAIN_TOKEN_.pGetSystemObject();
        return target.searchRelationshipsByDomainObject(targetSystemObject, searchCriteria);
   }


   /**
    * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#searchRelationshipsByDomains()
    */
    @WebMethod
    @WebResult(name="relationships")
    public List<Relationship> searchRelationshipsByDomains(@WebParam(name = "sourceDomainBean") SOURCE_DOMAIN_TOKEN_Bean sourceDomainBean,
                                                           @WebParam(name = "targetDomainBean") TARGET_DOMAIN_TOKEN_Bean targetDomainBean,
                                                           @WebParam(name = "mdSearchCriteria") MDSearchCriteria searchCriteria)
        throws ProcessingException, UserException {
        SystemSOURCE_DOMAIN_TOKEN_ systemSOURCE_DOMAIN_TOKEN_ = new SystemSOURCE_DOMAIN_TOKEN_();
        systemSOURCE_DOMAIN_TOKEN_.setSOURCE_DOMAIN_TOKEN_(sourceDomainBean);
        SystemObject sourceSystemObject = systemSOURCE_DOMAIN_TOKEN_.pGetSystemObject();

        SystemTARGET_DOMAIN_TOKEN_ systemTARGET_DOMAIN_TOKEN_ = new SystemTARGET_DOMAIN_TOKEN_();
        systemTARGET_DOMAIN_TOKEN_.setTARGET_DOMAIN_TOKEN_(targetDomainBean);
        SystemObject targetSystemObject = systemTARGET_DOMAIN_TOKEN_.pGetSystemObject();

        return target.searchRelationshipsByDomainObjects(sourceSystemObject, targetSystemObject, searchCriteria);
     }

    /**
     * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#getEnterpriseByDomain()
     */
    @WebMethod
    @WebResult(name="targetDomainBean")
    public TARGET_DOMAIN_TOKEN_Bean getEnterpriseByTARGET_DOMAIN_TOKEN_Domain(@WebParam(name = "localId") LocalId localId)
        throws ProcessingException, UserException {
        ObjectNode objectNode = target.getEnterpriseByDomain("TARGET_DOMAIN_TOKEN_", localId);
        if (objectNode != null) {
            TARGET_DOMAIN_TOKEN_Bean targetDomainBean = new TARGET_DOMAIN_TOKEN_Bean((TARGET_DOMAIN_TOKEN_Object)objectNode);
            return targetDomainBean;
        } else {
            return null;
        }
    }

   /**
     * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#getEnterpriseByDomain()
     */
    @WebMethod
    @WebResult(name="sourceDomainBean")
    public SOURCE_DOMAIN_TOKEN_Bean getEnterpriseBySOURCE_DOMAIN_TOKEN_Domain(@WebParam(name = "localId") LocalId localId)
        throws ProcessingException, UserException {
        ObjectNode objectNode = target.getEnterpriseByDomain("SOURCE_DOMAIN_TOKEN_", localId);
        if (objectNode != null) {
            SOURCE_DOMAIN_TOKEN_Bean sourceDomainBean = new SOURCE_DOMAIN_TOKEN_Bean((SOURCE_DOMAIN_TOKEN_Object)objectNode);
            return sourceDomainBean;
        } else {
            return null;
        }
    }

   /**
    * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#getEnterprise()
    */
    @WebMethod
    @WebResult(name="sourceDomainBean")
    public SOURCE_DOMAIN_TOKEN_Bean getSOURCE_DOMAIN_TOKEN_Enterprise(@WebParam(name = "euid") String euid)
       throws ProcessingException, UserException {
       ObjectNode objectNode = target.getEnterprise("SOURCE_DOMAIN_TOKEN_", euid);
       if (objectNode != null) {
            SOURCE_DOMAIN_TOKEN_Bean sourceDomainBean = new SOURCE_DOMAIN_TOKEN_Bean((SOURCE_DOMAIN_TOKEN_Object)objectNode);
            return sourceDomainBean;
       } else {
            return null;
       }
    }

 /**
    * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#getEnterprise()
    */
    @WebMethod
    @WebResult(name="targetDomainBean")
    public TARGET_DOMAIN_TOKEN_Bean getTARGET_DOMAIN_TOKEN_Enterprise(@WebParam(name = "euid") String euid)
       throws ProcessingException, UserException {
       ObjectNode objectNode = target.getEnterprise("TARGET_DOMAIN_TOKEN_", euid);
       if (objectNode != null) {
            TARGET_DOMAIN_TOKEN_Bean targetDomainBean = new TARGET_DOMAIN_TOKEN_Bean((TARGET_DOMAIN_TOKEN_Object)objectNode);
            return targetDomainBean;
       } else {
            return null;
       }
    }

  /**
   * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#getTransactionLog()
   */
   @WebMethod
   @WebResult(name="transactionLog")
   public TransactionLog getTransactionLog(@WebParam(name = "transactionId") long transactionId)
        throws ProcessingException, UserException {
        return target.getTransactionLog(transactionId);
   }

  /**
   * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#getTransactionLogs()
   */
   @WebMethod
   @WebResult(name="transactionLogs")
    public List<TransactionLog> getTransactionLogs(@WebParam(name = "relDefId") long relDefId, @WebParam(name = "relId") long relId)
        throws ProcessingException, UserException {
        return target.getTransactionLogs(relDefId, relId);
    }

  /**
   * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#searchTransactionLogs()
   */
   @WebMethod
   @WebResult(name="transactionLogs")
    public List<TransactionLog> searchTransactionLogs(@WebParam(name = "relDefId") long relDefId, @WebParam(name = "begin") Date begin, @WebParam(name = "end") Date end)
        throws ProcessingException, UserException {
        return target.searchTransactionLogs(relDefId, begin, end);
    }
}
