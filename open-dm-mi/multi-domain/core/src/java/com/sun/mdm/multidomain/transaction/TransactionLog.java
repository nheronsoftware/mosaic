/* *****************************************************************************
 *
 *  Copyright (c) 2011, NetGen Software Inc., All Rights Reserved
 *
 *  This program, and all the routines referenced herein, are the proprietary
 *  properties and trade secrets of NetGen Software Inc.
 *
 *  Except as provided for by license agreement, this program shall not be
 *  duplicated, used, or disclosed without written consent signed by an officer
 *  of NetGen Software Inc.
 *
 * ****************************************************************************/
package com.sun.mdm.multidomain.transaction;

import java.util.Date;
import java.io.Serializable;
import com.sun.mdm.multidomain.relationship.Relationship;

/**
 * TransactionLog class
 * @author cye
 */
public class TransactionLog implements Serializable {
    private Relationship relationship;
    private long transactionId;
    private String action;
    private String systemUser;
    private Date timeStamp;

    public TransactionLog(){
    }

    public TransactionLog(Relationship relationship,
                             long transactionId,
                             String action,
                             String systemUser,
                             Date timeStamp) {
        this.relationship = relationship;
        this.transactionId = transactionId;
        this.action = action;
        this.systemUser = systemUser;
        this.timeStamp = timeStamp;
    }

    public Relationship getRelationship(){
        return this.relationship;
    }

    public long getTransactionId(){
        return this.transactionId;
    }

    public String getAction(){
        return this.action;
    }

    public String getSystemUser() {
        return this.systemUser;
    }

    public Date getTimeStamp(){
        return this.timeStamp;
    }

    public void setRelationship(Relationship relationship){
        this.relationship = relationship;
    }

    public void setTransactionId(long transactionId){
        this.transactionId = transactionId;
    }

    public void setAction(String action){
        this.action = action;
    }

    public void setSystemUser(String systemUser) {
        this.systemUser = systemUser;
    }

    public void setTimestamp(Date timeStamp){
        this.timeStamp = timeStamp;
    }
}
