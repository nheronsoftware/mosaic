/* *****************************************************************************
 *
 *  Copyright (c) 2011, NetGen Software Inc., All Rights Reserved
 *
 *  This program, and all the routines referenced herein, are the proprietary
 *  properties and trade secrets of NetGen Software Inc.
 *
 *  Except as provided for by license agreement, this program shall not be
 *  duplicated, used, or disclosed without written consent signed by an officer
 *  of NetGen Software Inc.
 *
 * ****************************************************************************/
package com.sun.mdm.multidomain.ejb.service;

import java.util.List;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.annotation.security.DeclareRoles;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import java.sql.Connection;
import java.sql.SQLException;
import com.sun.mdm.multidomain.transaction.TransactionLog;
import com.sun.mdm.index.master.ProcessingException;
import com.sun.mdm.index.master.UserException;
import com.sun.mdm.index.master.search.enterprise.EOSearchCriteria;
import com.sun.mdm.index.master.search.enterprise.EOSearchOptions;
import com.sun.mdm.index.master.search.enterprise.EOSearchResultRecord;
import com.sun.mdm.index.objects.ObjectNode;
import com.sun.mdm.index.objects.SystemObject;
import com.sun.mdm.multidomain.query.PageIterator;
import com.sun.mdm.multidomain.relationship.Relationship;
import com.sun.mdm.multidomain.relationship.LocalId;
import com.sun.mdm.multidomain.query.MDSearchCriteria;

/**
 * The enterprise beans implementation of MultiDomainService that is exposed to the clients.
 * @author cye
 */
@Stateless(name = "MultiDomainServiceBean", mappedName = "ejb/MULTIDOMAIN_APPLICATION_TOKEN_MultiDomainService")
@Remote(MultiDomainServiceRemote.class)
@Local(MultiDomainServiceLocal.class)
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
@DeclareRoles({"MultiDomain.Admin", "MultiDomain.User"})
/*
@EJB will be decided.
@Resources will be decided.
 */
public class MultiDomainServiceBean implements MultiDomainServiceRemote, MultiDomainServiceLocal {

    private SessionContext sessionContext;
    @Resource(name = "jdbc/MULTIDOMAIN_DATASOURCE",
    type = javax.sql.DataSource.class,
    mappedName = "jdbc/MULTIDOMAIN_APPLICATION_TOKEN_DataSource")
    javax.sql.DataSource dataSource;
    private MultiDomainServiceFacade mdService;

    /**
     * Set SessionContext and called by the container when the bean is created.
     * @param sessionContext SessionContext.
     */
    @Resource
    public void setSessionContext(SessionContext sessionContext) {
        this.sessionContext = sessionContext;
    }

    /**
     * Initialize resources.And onInitialization invocation occurs before the first 
     * business method invocation.
     * @throws ProcessingException Thrown if an error occurs during processing.
     */
    @PostConstruct
    public void onInitialization()
        throws ProcessingException {
        mdService = new MultiDomainServiceFacade();
        mdService.setContext(sessionContext);
    }

    /**
     * Clean resources. And onTermination invocation occurs before the instance is 
     * removed by the container.
     */
    @PreDestroy
    public void onTermination() {
    }

    /**
     * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#createRelationship()
     */
    public long createRelationship(Relationship relationship)
            throws ProcessingException, UserException {
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
            return mdService.createRelationship(relationship, conn);
        } catch (SQLException sqlex) {
            throw new ProcessingException(sqlex);
        } finally {
            safeClose(conn);
        }
    }

    /**
     * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#createRelationship()
     */
    public long createRelationshipByDomain(String sourceDomain,
                                           LocalId sourceLID,
                                           String targetDomain,
                                           LocalId targetLID,
                                           Relationship relationship)
            throws ProcessingException, UserException {
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
            return mdService.createRelationshipByDomain(sourceDomain, sourceLID,
                                                        targetDomain, targetLID,
                                                        relationship, conn);
        } catch (SQLException sqlex) {
            throw new ProcessingException(sqlex);
        } finally {
            safeClose(conn);
        }
    }

    /**
     * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#updateRelationship()
     */
    public void updateRelationship(Relationship relationship)
            throws ProcessingException, UserException {
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
            mdService.updateRelationship(relationship, conn);
        } catch (SQLException sqlex) {
            throw new ProcessingException(sqlex);
        } finally {
            safeClose(conn);
        }
    }

       /**
     * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#updateRelationshipById()
     */
    public void updateRelationshipById(Relationship relationship)
            throws ProcessingException, UserException {
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
            mdService.updateRelationship(relationship, conn);
        } catch (SQLException sqlex) {
            throw new ProcessingException(sqlex);
        } finally {
            safeClose(conn);
        }
    }

    /**
     * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#deleteRelationship()
     */
    public void deleteRelationship(Relationship relationship)
            throws ProcessingException, UserException {
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
            mdService.deleteRelationship(relationship, conn);
        } catch (SQLException sqlex) {
            throw new ProcessingException(sqlex);
        } finally {
            safeClose(conn);
        }
    }

    /**
     * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#deleteRelationship()
     */
    public void deleteRelationshipById(long relId)
            throws ProcessingException, UserException {
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
            mdService.deleteRelationshipById(relId, conn);
        } catch (SQLException sqlex) {
            throw new ProcessingException(sqlex);
        } finally {
            safeClose(conn);
        }
    }

    /**
     * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#deleteRelationship()
     */
    public void deleteRelationshipByDomain(String sourceDomain,
                                           LocalId sourceLID,
                                           String targetDomain,
                                           LocalId targetLID,
                                           Relationship relationship)
            throws ProcessingException, UserException {
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
            mdService.deleteRelationshipByDomain(sourceDomain, sourceLID,
                                                 targetDomain, targetLID,
                                                 relationship, conn);
        } catch (SQLException sqlex) {
            throw new ProcessingException(sqlex);
        } finally {
            safeClose(conn);
        }
    }

    /**
     * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#getRelationship()
     */
    public Relationship getRelationship(Relationship relationship)
            throws ProcessingException, UserException {
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
            return mdService.getRelationship(relationship, conn);
        } catch (SQLException sqlex) {
            throw new ProcessingException(sqlex);
        } finally {
            safeClose(conn);
        }
    }

    /**
     * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#getRelationshipByDomain()
     */
    public Relationship getRelationshipByDomain(String sourceDomain,
                                                LocalId sourceLID,
                                                String targetDomain,
                                                LocalId targetLID,
                                                Relationship relationship)
            throws ProcessingException, UserException {
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
            return mdService.getRelationshipByDomain(sourceDomain, sourceLID,
                                                     targetDomain, targetLID,
                                                     relationship, conn);
        } catch (SQLException sqlex) {
            throw new ProcessingException(sqlex);
        } finally {
            safeClose(conn);
        }
    }

    /**
     * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#searchRelationships()
     */
    public List<Relationship> searchRelationships(String domain,
                                                  String euid,
                                                  MDSearchCriteria searchCriteria)
        throws ProcessingException, UserException {
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
            return mdService.searchRelationships(domain, euid, searchCriteria, conn);
        } catch (SQLException sqlex) {
            throw new ProcessingException(sqlex);
        } finally {
            safeClose(conn);
        }
    }

  /**
    * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#searchRelationshipsByDomain()
    */
    public List<Relationship> searchRelationshipsByDomain(String domain,
                                                          LocalId localId,
                                                          MDSearchCriteria searchCriteria)
        throws ProcessingException, UserException {
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
            return mdService.searchRelationshipsByDomain(domain, localId, searchCriteria, conn);
        } catch (SQLException sqlex) {
            throw new ProcessingException(sqlex);
        } finally {
            safeClose(conn);
        }
    }

   /**
    * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#searchRelationshipsByDomainObject()
    */
    public List<Relationship> searchRelationshipsByDomainObject(SystemObject domainObject,
                                                                MDSearchCriteria searchCriteria)
        throws ProcessingException, UserException {
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
            return mdService.searchRelationshipsByDomainObject(domainObject, searchCriteria, conn);
        } catch (SQLException sqlex) {
            throw new ProcessingException(sqlex);
        } finally {
            safeClose(conn);
        }
    }

   /**
    * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#searchRelationshipsByDomainObjects()
    */
    public List<Relationship> searchRelationshipsByDomainObjects(SystemObject sourceDomainObject,
                                                                 SystemObject targetDomainObject,
                                                                 MDSearchCriteria searchCriteria)
       throws ProcessingException, UserException {
       Connection conn = null;
        try {
            conn = dataSource.getConnection();
            return mdService.searchRelationshipsByDomainObjects(sourceDomainObject,
                                                                targetDomainObject,
                                                                searchCriteria, conn);
        } catch (SQLException sqlex) {
            throw new ProcessingException(sqlex);
        } finally {
            safeClose(conn);
        }
    }

    /**
     * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#getEnterprise()
     */
    public ObjectNode getEnterprise(String domain, String euid)
            throws ProcessingException, UserException {
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
            return mdService.getEnterprise(domain, euid, conn);
        } catch (SQLException sqlex) {
            throw new ProcessingException(sqlex);
        } finally {
            safeClose(conn);
        }
    }

   /**
     * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#getEnterpriseByDomain()
     */
    public ObjectNode getEnterpriseByDomain(String domain, LocalId localId)
        throws ProcessingException, UserException {
         Connection conn = null;
        try {
            conn = dataSource.getConnection();
            return mdService.getEnterpriseByDomain(domain, localId, conn);
        } catch (SQLException sqlex) {
            throw new ProcessingException(sqlex);
        } finally {
            safeClose(conn);
        }
    }

    /**
     * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#searchEnterprises()
     */
    public PageIterator<EOSearchResultRecord> searchEnterprises(String domain, 
                                                                EOSearchOptions searchOptions,
                                                                EOSearchCriteria searchCriteria)
            throws ProcessingException, UserException {
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
            return mdService.searchEnterprises(domain, searchOptions, searchCriteria, conn);
        } catch (SQLException sqlex) {
            throw new ProcessingException(sqlex);
        } finally {
            safeClose(conn);
        }
    }

   /**
    * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#getTransactionLog()
    */
    public TransactionLog getTransactionLog(long transactionId)
        throws ProcessingException, UserException {
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
            return mdService.getTransactionLog(transactionId, conn);
        } catch (SQLException sqlex) {
            throw new ProcessingException(sqlex);
        } finally {
            safeClose(conn);
        }
    }

   /**
    * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#getTransactionLog()
    */
    public TransactionLog getTransactionLog(long relDefId, long relId)
        throws ProcessingException, UserException {
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
            return mdService.getTransactionLog(relDefId, relId, conn);
        } catch (SQLException sqlex) {
            throw new ProcessingException(sqlex);
        } finally {
            safeClose(conn);
        }
    }

   /**
    * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#getTransactionLogs()
    */
    public List<TransactionLog> getTransactionLogs(long relDefId, long relId)
        throws ProcessingException, UserException {
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
            return mdService.getTransactionLogs(relDefId, relId, conn);
        } catch (SQLException sqlex) {
            throw new ProcessingException(sqlex);
        } finally {
            safeClose(conn);
        }
    }

   /**
    * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#searchTransactionLogs()
    */
    public List<TransactionLog> searchTransactionLogs(long relDefId, Date begin, Date end)
        throws ProcessingException, UserException {
       Connection conn = null;
        try {
            conn = dataSource.getConnection();
            return mdService.searchTransactionLogs(relDefId, begin, end, conn);
        } catch (SQLException sqlex) {
            throw new ProcessingException(sqlex);
        } finally {
            safeClose(conn);
        }
    }

    private void safeClose(Connection conn) {
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException sqlex) {
            }
        }
    }
}
