/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.multidomain.mdb.service;

import javax.ejb.EJB;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.ejb.MessageDrivenContext;
import com.sun.mdm.multidomain.ejb.service.MultiDomainServiceLocal;
import com.sun.mdm.multidomain.ejb.service.MultiDomainMetaServiceLocal;
import com.sun.mdm.multidomain.mdb.service.MDBServiceFacade;

/**
 * DomainMessageDrivenBean class.
 * @author cye
 */
@MessageDriven(mappedName = "jms/DOMAIN_TOKENTopic", activationConfig = {
    @ActivationConfigProperty(propertyName = "connectionFcatoryJndiName", propertyValue = "jms/DOMAIN_TOKENOutBoundSender"),
    @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Topic"),
    @ActivationConfigProperty(propertyName = "subscriptionDurability", propertyValue = "Durable"),
    @ActivationConfigProperty(propertyName = "clientId", propertyValue = "DOMAIN_TOKENSyncMessageDrivenBean"),
    @ActivationConfigProperty(propertyName = "subscriptionName", propertyValue = "DOMAIN_TOKENSyncMessageDrivenBean")
})
public class DOMAIN_TOKENMessageDrivenBean implements MessageListener {

    @EJB
    private MultiDomainMetaServiceLocal multiDomainMetaService;
    @EJB
    private MultiDomainServiceLocal multiDomainService;
    @Resource
    private MessageDrivenContext mdbContext;
    private MDBServiceFacade mdbService;
    private String domain = "DOMAIN_TOKEN";

    public DOMAIN_TOKENMessageDrivenBean() {
    }

    @PostConstruct
    public void mdbCreate() {
        mdbService = new MDBServiceFacade();
        mdbService.create(domain, mdbContext);
        mdbService.init(multiDomainMetaService, multiDomainService);
    }

    public void onMessage(Message message) {
        mdbService.handle(message);
    }

    @PreDestroy
    public void mdbRemove() {
        mdbService.remove();
    }
}
