/* *****************************************************************************
 *
 *  Copyright (c) 2011, NetGen Software Inc., All Rights Reserved
 *
 *  This program, and all the routines referenced herein, are the proprietary
 *  properties and trade secrets of NetGen Software Inc.
 *
 *  Except as provided for by license agreement, this program shall not be
 *  duplicated, used, or disclosed without written consent signed by an officer
 *  of NetGen Software Inc.
 *
 * ****************************************************************************/
package com.sun.mdm.multidomain.relationship;

import java.io.Serializable;

/**
 * LocalKey class.
 * @author cye
 */
public class LocalId implements Serializable {
    
    private String id;
    private String code;

    public LocalId(){
    }

    public LocalId(String code, String id) {
        this.code = code;
        this.id = id;
    }

    public void setId(String id){
        this.id = id;
    }

    public void setCode(String code){
        this.code = code;
    }

    public String getId(){
        return this.id;
    }

    public String getCode(){
        return this.code;
    }
}
