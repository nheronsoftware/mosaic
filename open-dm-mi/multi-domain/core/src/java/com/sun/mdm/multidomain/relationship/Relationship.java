/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common
 * Development and Distribution License ("CDDL")(the "License"). You
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the
 * specific language governing permissions and limitations under the
 * License.
 *
 * When distributing the Covered Code, include this CDDL Header Notice
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the
 * fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.multidomain.relationship;

import java.util.Date;
import java.util.Map;
import java.util.HashMap;
import com.sun.mdm.multidomain.attributes.Attribute;
import java.io.Serializable;

/**
 * Relationship class.
 * This represents a particular instance of a relationship.
 * Ex. Relationship between Patient "John Smith" and Doctor "Watkins".
 * The relationship is between by sourceEUID and targetEUID and uniquely represented by
 * relationshipID.
 * @author cye
 */
public class Relationship implements Serializable {
    private static final long serialVersionUID = -1;
    private long id;
    private String sourceEUID;
    private String targetEUID;
    private Date effectiveFromDate;
    private Date effectiveToDate;
    private Date purgeDate;
    private Map<Attribute, String> attributeValues = null;
    private RelationshipDef relationshipDef = null;

    /**
     * Create an instance of Relationship.
     */
    public Relationship() {
        attributeValues = new HashMap<Attribute, String>();
    }

    /**
     * Get relationship Id.
     * @return long Relationship Id.
     */

    public long getRelationshipId() {
    	return id;

    }

    /**
     * Set relationship Id.
     * @param relationshipID Relationship Id.
     */

    public void setRelationshipId(long id){
    	this.id = id;

    }

    /**
     * Get Source EUID
     * @return sourceEUID
     */
    public String getSourceEUID() {
        return sourceEUID;
    }

    /**
     *  set sourceEUID
     * @param sourceEUID
     */
    public void setSourceEUID(String sourceEUID) {
        this.sourceEUID = sourceEUID;
    }

    /**
     * Get Target EUID
     * @return targetEUID
     */
    public String getTargetEUID() {
        return targetEUID;
    }

    /**
     *  set targetEUID
     * @param targetEUID
     */
    public void setTargetEUID(String targetEUID) {
        this.targetEUID = targetEUID;
    }

    /**
     * Get start date attribute.
     * @return Date Start date attribute.
     */
    public Date getEffectiveFromDate() {
        return effectiveFromDate;
    }

    /**
     * Set Start date attribute.
     * @param effectiveFromDate Start date attribute.
     */
    public void setEffectiveFromDate(Date effectiveFromDate) {
        this.effectiveFromDate = effectiveFromDate;
    }

    /**
     * Get end date attribute.
     * @return Date End date attribute.
     */
    public Date getEffectiveToDate() {
        return effectiveToDate;
    }

    /**
     * Set End date attribute.
     * @param effectiveToDate End date attribute.
     */
    public void setEffectiveToDate(Date effectiveToDate) {
        this.effectiveToDate = effectiveToDate;
    }

    /**
     * Get Purge date attribute.
     * @return Date Purge date attribute.
     */
    public Date getPurgeDate() {
        return purgeDate;
    }

    /**
     * Set Purge date attribute.
     * @param purgeDate Purge date attribute.
     */
    public void setPurgeDate(Date purgeDate) {
        this.purgeDate = purgeDate;
    }

    public boolean isAttribute(Attribute attribute) {
        boolean isAttribute = attributeValues.containsKey(attribute);
        if (!isAttribute) {
            if ("effectiveFromDate".equals(attribute.getName()) ||
                "effectiveToDate".equals(attribute.getName()) ||
                "purgeDate".equals(attribute.getName())) {
                isAttribute = true;
            }
        }
        return isAttribute;
    }

    /**
     * Set attribute value.
     * @param attribute Attribute.
     * @param value Attribute value.
     */
    public void setAttributeValue(Attribute attribute, String value) {
        attributeValues.put(attribute, value);
    }

    /**
     * Get attribute value.
     * @param attribute Attribute.
     * @return String Attribute value.
     */
    public String getAttributeValue(Attribute attribute) {
        if ("effectiveFromDate".equals(attribute.getName())) {
            if (effectiveFromDate != null) {
                return effectiveFromDate.toString();
            } else {
                return null;
            }
        } else if("effectiveToDate".equals(attribute.getName())) {
            if (effectiveToDate != null) {
                return effectiveToDate.toString();
            } else {
                return null;
            }
        } else if("purgeDate".equals(attribute.getName())) {
            if (purgeDate != null) {
                return purgeDate.toString();
            } else {
                return null;
            }
        } else {
            return attributeValues.get(attribute);
        }
    }

    /**
     * Set attribute value.
     * @param attribute Attribute.
     * @param value Attribute value.
     */
    public void setAttributes(Map<Attribute, String> attributeValues) {
        this.attributeValues = attributeValues;
    }

    /**
     * Get attribute value.
     * @param attribute Attribute.
     * @return String Attribute value.
     */
    public Map<Attribute, String> getAttributes() {
        return attributeValues;
    }

    /**
     * Set RelationshipDef instance
     * @param relDef RelationshipDef.
     */
    public void setRelationshipDef(RelationshipDef relDef) {
        this.relationshipDef = relDef;
    }

    /**
     * Get RelationshipDef instance.
     * @return RelationshipDef RelationshipDef instance.
     */
    public RelationshipDef getRelationshipDef() {
        if (relationshipDef == null) {
            relationshipDef = new RelationshipDef();
        }
        return relationshipDef;
    }
}

