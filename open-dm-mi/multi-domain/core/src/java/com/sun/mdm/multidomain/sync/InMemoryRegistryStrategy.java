/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2009 NetServices, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.multidomain.sync;

import java.util.List;
import java.util.ArrayList;

import com.sun.mdm.multidomain.parser.Plugin;
import com.sun.mdm.multidomain.parser.Definition;
import com.sun.mdm.multidomain.parser.MultiDomainModel;
import com.sun.mdm.multidomain.parser.ParserException;

/**
 * InMemoryRegistryStrategy class.
 * @author cye
 */
public class InMemoryRegistryStrategy implements RegistryStrategy<Plugin> {

    private List<Plugin> entries = new ArrayList<Plugin>();
            
    public void initialize() {
        try {
            MultiDomainModel mdModel = SyncUtils.getMultiDomainModel(); 
            ArrayList<Definition>  definitions = mdModel.getAllDefinitions();
            for (Definition definition : definitions) {
                 if (Definition.TYPE_RELATIONSHIP.equals(definition.getType())) {
                     String name = definition.getPlugin();  
                     //ToDo
                     Plugin plugin = new Plugin();
                     plugin.setName(name);
                     if (name.startsWith("relationshipDefaultPlugin")) {
                         plugin.setClz("com.sun.mdm.multidomain.sync.impl.DefaultRelationshipSync");
                     } else {
                        plugin.setClz(name);
                     }
                     plugin.setType(Plugin.PluginType.RELATIONSHIP);
                     addEntry(plugin);
                 }
            }
            
        } catch (ParserException pex) {
        }
    }
    
    public void addEntry(Plugin plugin) {
        if (!entries.contains(plugin)) {
            entries.add(plugin);
        } else {
            updateEntry(plugin);
        }
    }
    
    public void updateEntry(Plugin plugin) {
        for (Plugin entry : entries) {
            if(entry.equals(plugin)) {
                entry.copy(plugin);
            }
        }
    }
    
    public Plugin getEntry(String name) {
        Plugin plugin = null;
        if(name != null) { 
            for (Plugin entry : entries) {
                if (name.equals(entry.getName())) {
                    plugin = entry;
                    break;
                }
            }
        }
        //ToDo
        if (plugin == null) {
            plugin = entries.get(0);
        }
        return plugin;
    }
    
    public void removeEntry(String name) {
        if(name != null) { 
          for (Plugin entry : entries) {
              if (name.equals(entry.getName())) {
                entries.remove(entry);
                break;
              }
          }  
        }      
    }
    
    public boolean containsEntry(String name){
        boolean contains = false;
        if (name != null) {
            for (Plugin entry : entries) {
                if(name.equals(entry.getName())) {
                    contains = true;
                    break;
                }
            }            
        }
        return contains;
    }
}
