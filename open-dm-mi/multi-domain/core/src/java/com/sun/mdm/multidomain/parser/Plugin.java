/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2009 NetServices, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.multidomain.parser;

import java.io.Serializable;

/**
 * Plugin class.
 * @author cye
 */
public class Plugin implements Serializable {

    private String name;
    private String clz;
    private String jar;
    private PluginType type;
    
    public Plugin() {
    }

    public Plugin(PluginType type, String name, String clz, String jar) {
        this.type = type;
        this.name = name;
        this.clz = clz;
        this.jar = jar;
    }

    public PluginType getType() {
        return this.type;
    }
    
    public void setType(PluginType type) {
        this.type = type;
    }
    
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClz() {
        return this.clz;
    }

    public void setClz(String clz) {
        this.clz = clz;
    }

    public String getJar() {
        return this.jar;
    }

    public void setJar(String jar) {
        this.jar = jar;
    }
    
    public void copy(Plugin plugin) {
        this.name = plugin.getName();
        this.clz = plugin.getClz();
        this.jar = plugin.getJar();
    }
    
    @Override
    public boolean equals(Object o) {
        if(o == this) {
            return true;
        } else if (!(o instanceof Plugin)) {
            return false;
        } else {
            Plugin plugin = (Plugin)o;
            if (this.name.equals(plugin.getName())) {
                return true;
            } else {
                return false;
            }
        }
    }
    
    @Override 
    public int hashCode() {
        return super.hashCode();
    }
    
    public enum PluginType {
        
        RELATIONSHIP('R') {
            @Override
            public String toString(){
                return "Relationship";
            }
        },
        HIERARCHY('H') {
            @Override
            public String toString(){
                return "Hierarchy";
            }         
        };
        
        private char type;
        
        PluginType(char type) {
            this.type = type;
        }
        
        public static PluginType[] types() {
            return PluginType.values();
        }
    }
}
