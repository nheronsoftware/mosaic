/* *****************************************************************************
 *
 *  Copyright (c) 2011, NetGen Software Inc., All Rights Reserved
 *
 *  This program, and all the routines referenced herein, are the proprietary
 *  properties and trade secrets of NetGen Software Inc.
 *
 *  Except as provided for by license agreement, this program shall not be
 *  duplicated, used, or disclosed without written consent signed by an officer
 *  of NetGen Software Inc.
 *
 * ****************************************************************************/
package com.sun.mdm.multidomain.ejb.service;

import java.util.List;
import java.util.Date;
import java.util.ArrayList;
import java.sql.Connection;
import javax.ejb.SessionContext;
import com.sun.mdm.index.master.ProcessingException;
import com.sun.mdm.index.master.UserException;
import com.sun.mdm.index.master.search.enterprise.EOSearchCriteria;
import com.sun.mdm.index.master.search.enterprise.EOSearchOptions;
import com.sun.mdm.index.master.search.enterprise.EOSearchResultRecord;
import com.sun.mdm.index.objects.ObjectNode;
import com.sun.mdm.index.objects.SystemObject;
import com.sun.mdm.multidomain.transaction.LockManager;
import com.sun.mdm.multidomain.transaction.TransactionLogDto;
import com.sun.mdm.multidomain.transaction.TransactionLog;
import com.sun.mdm.multidomain.transaction.TransactionLogManager;
import com.sun.mdm.multidomain.transaction.TransactionLogException;
import com.sun.mdm.multidomain.transaction.LockException;
import com.sun.mdm.multidomain.query.MDSearchCriteria;
import com.sun.mdm.multidomain.query.QueryFilter;
import com.sun.mdm.multidomain.query.PageIterator;
import com.sun.mdm.multidomain.attributes.Attribute;
import com.sun.mdm.multidomain.relationship.Relationship;
import com.sun.mdm.multidomain.relationship.LocalId;
import com.sun.mdm.multidomain.relationship.RelationshipDef;
import com.sun.mdm.multidomain.relationship.service.RelationshipService;
import com.sun.mdm.multidomain.query.MultiDomainQuery;
import com.sun.mdm.multidomain.relationship.service.RelationshipDefService;
import com.sun.mdm.multidomain.relationship.ops.exceptions.RelationshipDefDaoException;
import com.sun.mdm.multidomain.relationship.ops.exceptions.RelationshipDaoException;
import com.sun.mdm.multidomain.relationship.ops.exceptions.RelationshipEavDaoException;

/**
 * MultiDomainServiceFacade class
 * @author cye
 */
public class MultiDomainServiceFacade {

    private SessionContext context;

    private TransactionLogManager transactionLogManager;

    private LockManager lockManager;

    private String userId = "MultiDomain.Admin";

    public MultiDomainServiceFacade() {
        transactionLogManager = new TransactionLogManager();
        lockManager = new LockManager();
    }

    public void setContext(SessionContext context) {
        this.context = context;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#createRelationship()
     */
    public long createRelationship(Relationship relationship, Connection conn)
            throws ProcessingException, UserException {
        try {
            RelationshipDef relDef = relationship.getRelationshipDef();
            if (relationship.getRelationshipDef() == null) {
                throw new UserException("Invalid relationship definition.");
            } else if (relDef.getName() != null &&
                    relDef.getSourceDomain() != null &&
                    relDef.getTargetDomain() != null) {
                RelationshipDefService relDefService = new RelationshipDefService(conn);
                relDef = relDefService.getRelationshipDef(relDef.getName(),
                        relDef.getSourceDomain(),
                        relDef.getTargetDomain());
                relationship.setRelationshipDef(relDef);
            } else {
                RelationshipDefService relDefService = new RelationshipDefService(conn);
                relDef = relDefService.getRelationshipDef(relDef.getId());
            }
            if (relDef == null) {
                throw new UserException("Invalid relationship definition.");
            }

            RelationshipService relService = new RelationshipService(conn);
            if (relService.searchRelationShip(relationship.getSourceEUID(),
                    relationship.getTargetEUID(),
                    relDef.getId()) != null) {
                throw new UserException(relationship.getSourceEUID() + " and " +
                        relationship.getTargetEUID() + " relationship existing.");
            }
            long relId = relService.create(relationship);
            relationship.setRelationshipId(relId);
            logTransaction(relationship, TransactionLogDto.ADD_ACTION, conn);
            return relId;
        } catch (ProcessingException pex) {
            context.setRollbackOnly();
            throw pex;
        } catch (UserException uex) {
            context.setRollbackOnly();
            throw uex;
        } catch (RelationshipDefDaoException rdex) {
            context.setRollbackOnly();
            throw new ProcessingException(rdex);
        } catch (RelationshipDaoException rex) {
            context.setRollbackOnly();
            throw new ProcessingException(rex);
        } catch (RelationshipEavDaoException reex) {
            context.setRollbackOnly();
            throw new ProcessingException(reex);
        }
    }

    /**
     * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#createRelationship()
     */
    public long createRelationshipByDomain(String sourceDomain, LocalId sourceLID,
                                           String targetDomain, LocalId targetLID,
                                           Relationship relationship, Connection conn)
        throws ProcessingException, UserException {
        try {
            MultiDomainQuery mDomainQuery = new MultiDomainQuery();
            String sourceEUID = mDomainQuery.getEUID(sourceDomain, sourceLID, conn);
            if (sourceEUID == null) {
                throw new UserException("source domain:" + sourceDomain + " source domain local Id:" + sourceLID + " invalid.");
            }
            String targetEUID = mDomainQuery.getEUID(targetDomain, targetLID, conn);
            if (targetEUID == null) {
                throw new UserException("target domain:" + targetDomain + " target domain local Id:" + targetLID + " invalid.");
            }
            relationship.setSourceEUID(sourceEUID);
            relationship.setTargetEUID(targetEUID);
            long relId = createRelationship(relationship, conn);
            relationship.setRelationshipId(relId);
            logTransaction(relationship, TransactionLogDto.ADD_ACTION, conn);
            return relId;
        } catch (ProcessingException pex) {
            context.setRollbackOnly();
            throw pex;
        } catch (UserException uex) {
            context.setRollbackOnly();
            throw uex;
        }
    }

    /**
     * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#updateRelationship()
     */
    public void updateRelationship(Relationship relationship, Connection conn)
            throws ProcessingException, UserException {
        try {
            RelationshipService relService = new RelationshipService(conn);
            RelationshipDef relDef = relationship.getRelationshipDef();
            if (relDef != null) {
                if (relDef.getName() != null &&
                    relDef.getSourceDomain() != null &&
                    relDef.getTargetDomain() != null) {
                    RelationshipDefService relDefService = new RelationshipDefService(conn);
                    relDef = relDefService.getRelationshipDef(relDef.getName(),
                                                              relDef.getSourceDomain(),
                                                              relDef.getTargetDomain());
                }
            }
            if (relDef == null ) {
                throw new UserException("Invalid relationship def.");
            }
            if (relationship.getSourceEUID() != null &&
                relationship.getTargetEUID() != null) {
                Relationship rel = relService.searchRelationShip(relationship.getSourceEUID(),
                                                                 relationship.getTargetEUID(),
                                                                 relDef.getId());
                if (rel != null) {
                    relationship.setRelationshipId(rel.getRelationshipId());
                } else {
                    throw new UserException("Relationship does not exist.");
                }
            }
            lockManager.lock(conn, relDef.getId(), relationship.getRelationshipId());
            relService.update(relationship);
            logTransaction(relationship, TransactionLogDto.UPDATE_ACTION, conn);
        } catch (UserException uex) {
            context.setRollbackOnly();
            throw uex;
        } catch (RelationshipDefDaoException rdex) {
            context.setRollbackOnly();
            throw new ProcessingException(rdex);
        } catch (RelationshipDaoException rex) {
            context.setRollbackOnly();
            throw new ProcessingException(rex);
        } catch (LockException lex) {
            context.setRollbackOnly();
            throw new ProcessingException(lex);
        }
    }

   /**
     * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#updateRelationship()
     */
    public void updateRelationshipById(Relationship relationship, Connection conn)
            throws ProcessingException, UserException {
        try {
            RelationshipService relService = new RelationshipService(conn);
            RelationshipDef relDef = relationship.getRelationshipDef();
            if (relDef != null) {
                if (relDef.getName() != null &&
                    relDef.getSourceDomain() != null &&
                    relDef.getTargetDomain() != null) {
                    RelationshipDefService relDefService = new RelationshipDefService(conn);
                    relDef = relDefService.getRelationshipDef(relDef.getName(),
                                                              relDef.getSourceDomain(),
                                                              relDef.getTargetDomain());
                }
            }
            if (relDef == null ) {
                throw new UserException("Invalid relationship def.");
            }
            lockManager.lock(conn, relDef.getId(), relationship.getRelationshipId());
            relService.update(relationship);
            logTransaction(relationship, TransactionLogDto.UPDATE_ACTION, conn);
        } catch (UserException uex) {
            context.setRollbackOnly();
            throw uex;
        } catch (RelationshipDefDaoException rdex) {
            context.setRollbackOnly();
            throw new ProcessingException(rdex);
        } catch (RelationshipDaoException rex) {
            context.setRollbackOnly();
            throw new ProcessingException(rex);
        } catch (LockException lex) {
            context.setRollbackOnly();
            throw new ProcessingException(lex);
        }
    }

    /**
     * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#deleteRelationship()
     */
    public void deleteRelationship(Relationship relationship, Connection conn)
            throws ProcessingException, UserException {
        try {
            RelationshipService relService = new RelationshipService(conn);
            RelationshipDef relDef = relationship.getRelationshipDef();
            if (relDef != null) {
                if (relDef.getName() != null &&
                    relDef.getSourceDomain() != null &&
                    relDef.getTargetDomain() != null) {
                    RelationshipDefService relDefService = new RelationshipDefService(conn);
                    relDef = relDefService.getRelationshipDef(relDef.getName(),
                                                              relDef.getSourceDomain(),
                                                              relDef.getTargetDomain());
                }
            }
            if (relationship.getSourceEUID() != null &&
                relationship.getTargetEUID() != null &&
                relDef != null ) {
                relationship = relService.searchRelationShip(relationship.getSourceEUID(),
                                                             relationship.getTargetEUID(),
                                                             relDef.getId());
            }
            lockManager.lock(conn, relDef.getId(), relationship.getRelationshipId());
            relService.delete(relationship.getRelationshipId());
            logTransaction(relationship, TransactionLogDto.DELETE_ACTION, conn);
        } catch (LockException lex) {
            context.setRollbackOnly();
            throw new ProcessingException(lex);
        } catch (RelationshipDefDaoException rdex) {
            context.setRollbackOnly();
            throw new ProcessingException(rdex);
        } catch (RelationshipDaoException rex) {
            context.setRollbackOnly();
            throw new ProcessingException(rex);
        } 
    }

    /**
     * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#deleteRelationship()
     */
    public void deleteRelationshipById(long relId, Connection conn)
            throws ProcessingException, UserException {
        try {
            RelationshipService relService = new RelationshipService(conn);
            Relationship rel = new Relationship();
            rel.setRelationshipId(relId);
            relService.getRelationship(rel);
            relService.delete(relId);
            logTransaction(rel, TransactionLogDto.DELETE_ACTION, conn);
         } catch (RelationshipDaoException rex) {
            context.setRollbackOnly();
            throw new ProcessingException(rex);
        }
    }

    /**
     * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#deleteRelationship()
     */
    public void deleteRelationshipByDomain(String sourceDomain, LocalId sourceLID,
                                           String targetDomain, LocalId targetLID,
                                           Relationship relationship, Connection conn)
        throws ProcessingException, UserException {
        try {
            MultiDomainQuery mDomainQuery = new MultiDomainQuery();
            String sourceEUID = mDomainQuery.getEUID(sourceDomain, sourceLID, conn);
            if (sourceEUID == null) {
                throw new UserException("source domain:" + sourceDomain + " source domain local Id:" + sourceLID + " invalid.");
            }
            String targetEUID = mDomainQuery.getEUID(targetDomain, targetLID, conn);
            if (targetEUID == null) {
                throw new UserException("target domain:" + targetDomain + " target domain local Id:" + targetLID + " invalid.");
            }
            relationship.setSourceEUID(sourceEUID);
            relationship.setTargetEUID(targetEUID);
            deleteRelationship(relationship, conn);
            logTransaction(relationship, TransactionLogDto.DELETE_ACTION, conn);
        } catch (ProcessingException pex) {
            context.setRollbackOnly();
            throw pex;
        } catch (UserException uex) {
            context.setRollbackOnly();
            throw uex;
        }
    }

    /**
     * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#getRelationship()
     */
    public Relationship getRelationship(Relationship relationship, Connection conn)
            throws ProcessingException, UserException {
        try {
            RelationshipService relService = new RelationshipService(conn);
            RelationshipDef relDef = relationship.getRelationshipDef();
            if (relDef != null) {
                if (relDef.getName() != null &&
                    relDef.getSourceDomain() != null &&
                    relDef.getTargetDomain() != null) {
                        RelationshipDefService relDefService = new RelationshipDefService(conn);
                        relDef = relDefService.getRelationshipDef(relDef.getName(),
                                                                  relDef.getSourceDomain(),
                                                                  relDef.getTargetDomain());
                }
            }
            if (relationship.getSourceEUID() != null &&
                relationship.getTargetEUID() != null &&
                relDef != null) {
                relationship = relService.getRelationship(relationship.getSourceEUID(),
                                                          relationship.getTargetEUID(),
                                                          relDef.getId());
            } else {
                relationship = relService.getRelationship(relationship);
            }
            /*
            MultiDomainQuery mDomainQuery = new MultiDomainQuery();
            MultiObject multiObject = mDomainQuery.getRelationship(relationship, searchOptions, conn);
            RelationshipDef relDef = relationship.getRelationshipDef();
            RelationshipDomain relDomain = multiObject.getRelationshipDomain(relDef.getSourceDomain());
            List<RelationshipObject> relObjects = relDomain.getRelationshipObjects();
             */
            return relationship;
       } catch (RelationshipDefDaoException rdex) {
            throw new ProcessingException(rdex);
        } catch (RelationshipDaoException rex) {
            throw new ProcessingException(rex);
        }
    }

   /**
     * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#getRelationshipByDomain()
     */
    public Relationship getRelationshipByDomain(String sourceDomain, LocalId sourceLID,
                                                String targetDomain, LocalId targetLID,
                                                Relationship relationship, Connection conn)
            throws ProcessingException, UserException {
        try {
            MultiDomainQuery mDomainQuery = new MultiDomainQuery();
            String sourceEUID = mDomainQuery.getEUID(sourceDomain, sourceLID, conn);
            if (sourceEUID == null) {
                throw new UserException("source domain:" + sourceDomain + " source domain local Id:" + sourceLID + " invalid.");
            }
            String targetEUID = mDomainQuery.getEUID(targetDomain, targetLID, conn);
            if (targetEUID == null) {
                throw new UserException("target domain:" + targetDomain + " target domain local Id:" + targetLID + " invalid.");
            }
            relationship.setSourceEUID(sourceEUID);
            relationship.setTargetEUID(targetEUID);
            return getRelationship(relationship, conn);

        } catch (ProcessingException pex) {
           throw pex;
        }
    }

    /**
     * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#searchRelationships()
     */
     public List<Relationship> searchRelationships(String domain,
                                                   String euid,
                                                   MDSearchCriteria searchCriteria,
                                                   Connection conn)
        throws ProcessingException, UserException {
        try {
            RelationshipDef relDef = searchCriteria.getRelationshipDef();
            if (relDef != null) {
                RelationshipDefService relDefService = new RelationshipDefService(conn);
                if (relDef.getName() != null &&
                    relDef.getSourceDomain() != null &&
                    relDef.getTargetDomain() != null) {
                    relDef = relDefService.getRelationshipDef(relDef.getName(),
                                                              relDef.getSourceDomain(),
                                                              relDef.getTargetDomain());
                } else {
                    relDef = relDefService.getRelationshipDef(relDef.getId());
                }
            }
            if (relDef == null ) {
                throw new UserException("Invalid relationship def.");
            }
            MultiDomainQuery mDomainQuery = new MultiDomainQuery();
            return mDomainQuery.searchRelationships(domain,
                                                    euid,
                                                    relDef,
                                                    searchCriteria.getAttributeValues(),
                                                    conn);
        } catch(RelationshipDefDaoException rdex) {
           throw new ProcessingException(rdex);
        }
    }

  /**
    * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#searchRelationshipsByDomain()
    */
    public List<Relationship> searchRelationshipsByDomain(String domain,
                                                          LocalId localId,
                                                          MDSearchCriteria searchCriteria,
                                                          Connection conn)
        throws ProcessingException, UserException {
        MultiDomainQuery mDomainQuery = new MultiDomainQuery();
        String euid = mDomainQuery.getEUID(domain, localId, conn);
        if (euid == null) {
            context.setRollbackOnly();
            throw new UserException("domain:" + domain + " domain local Id:" + localId + " invalid.");
        }
        return searchRelationships(domain, euid, searchCriteria, conn);
    }

  /**
    * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#searchRelationshipsByDomainObject()
    */
    public List<Relationship> searchRelationshipsByDomainObject(SystemObject domainObject,
                                                                MDSearchCriteria searchCriteria,
                                                                Connection conn)
        throws ProcessingException, UserException {
        MultiDomainQuery mDomainQuery = new MultiDomainQuery();
        List<String> euids = mDomainQuery.searchEUIDsByDomainObject(domainObject,
                                                                    conn);
        String domain = domainObject.getChildType();
        List<Relationship> rrels = new ArrayList<Relationship>();
        for (String euid : euids) {
            List<Relationship> rels = searchRelationships(domain, euid, searchCriteria, conn);
            rrels.addAll(rels);
        }
        return rrels;
    }

   /**
    * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#searchRelationshipsByDomainObjects()
    */
    public List<Relationship> searchRelationshipsByDomainObjects(SystemObject sourceDomainObject,
                                                                 SystemObject targetDomainObject,
                                                                 MDSearchCriteria searchCriteria, 
                                                                 Connection conn)
                                                                 throws ProcessingException, UserException {
        try {
            MultiDomainQuery mDomainQuery = new MultiDomainQuery();
            String sourceDomain = sourceDomainObject.pGetTag();
            List<String> sourceEuids = mDomainQuery.searchEUIDsByDomainObject(sourceDomainObject,
                    conn);
            if (sourceEuids.isEmpty()) {
                throw new UserException("source domain:" + sourceDomain + " records not found");
            }
            String targetDomain = targetDomainObject.pGetTag();
            List<String> targetEuids = mDomainQuery.searchEUIDsByDomainObject(targetDomainObject,
                    conn);
            if (targetEuids.isEmpty()) {
                throw new UserException("target domain:" + targetDomain + " records not found");
            }

            List<Relationship> rrels = new ArrayList<Relationship>();
            RelationshipDef relDef = searchCriteria.getRelationshipDef();
            List<Attribute> filters = searchCriteria.getAttributeValues();

            relDef = getRelationshipDef(relDef, conn);
            if (relDef == null) {
                throw new UserException("Invalid relationship definition.");
            }

            RelationshipService relService = new RelationshipService(conn);
            for (String sourceEuid : sourceEuids) {
                for (String targetEuid : targetEuids) {
                    try {
                        Relationship rel = relService.getRelationship(sourceEuid,
                                targetEuid,
                                relDef.getId());
                        rel = QueryFilter.doFilter(rel, filters);
                        if (rel != null) {
                            rrels.add(rel);
                        }
                    } catch (RelationshipDaoException rex) {
                    }
                }
            }
            return rrels;
        } catch (UserException uex) {
            throw uex;
        }
    }

    /**
     * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#getEnterprise()
     */
    public ObjectNode getEnterprise(String domain, String euid, Connection conn)
            throws ProcessingException, UserException {
        MultiDomainQuery mDomainQuery = new MultiDomainQuery();
        return mDomainQuery.getEnterpriseObject(domain, euid, conn);
    }

    /**
     * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#getEnterpriseByDomain()
     */

    public ObjectNode getEnterpriseByDomain(String domain, LocalId localId, Connection conn)
            throws ProcessingException, UserException {
        MultiDomainQuery mDomainQuery = new MultiDomainQuery();
        String euid = mDomainQuery.getEUID(domain, localId, conn);
        return mDomainQuery.getEnterpriseObject(domain, euid, conn);
    }

    /**
     * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#searchEnterprises()
     */
    public PageIterator<EOSearchResultRecord> searchEnterprises(String domain, 
                                                                EOSearchOptions searchOptions,
                                                                EOSearchCriteria searchCriteria,
                                                                Connection conn)
            throws ProcessingException, UserException {
        MultiDomainQuery mDomainQuery = new MultiDomainQuery();
        PageIterator<EOSearchResultRecord> results = mDomainQuery.searchEnterprises(domain, searchOptions, searchCriteria, conn);
        return results;
    }

  /**
    * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#getTransactionLog()
    */
    public TransactionLog getTransactionLog(long transactionId, Connection conn)
        throws ProcessingException, UserException {
        try {
            return transactionLogManager.getTransactionLog(conn, transactionId);
        } catch (TransactionLogException tex){
            throw new ProcessingException(tex);
        }
    }

   /**
    * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#getTransactionLog()
    */
    public TransactionLog getTransactionLog(long relDefId, long relId, Connection conn)
        throws ProcessingException, UserException {
        try {
            return transactionLogManager.getTransactionLog(conn, relDefId, relId);
        } catch (TransactionLogException tex){
            throw new ProcessingException(tex);
        }
    }

   /**
    * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#getTransactionLogs()
    */
    public List<TransactionLog> getTransactionLogs(long relDefId, long relId, Connection conn)
        throws ProcessingException, UserException {
        try {
            return transactionLogManager.getTransactionLogs(conn, relDefId, relId);
        } catch (TransactionLogException tex){
            throw new ProcessingException(tex);
        }
    }

   /**
    * @see com.sun.mdm.multidomain.ejb.service.MultiDomainService#searchTransactionLogs()
    */
    public List<TransactionLog> searchTransactionLogs(long relDefId, Date begin, Date end, Connection conn)
        throws ProcessingException, UserException {
        try {
            return transactionLogManager.searchTransactionLogs(conn, relDefId, begin, end);
        } catch (TransactionLogException tex){
            throw new ProcessingException(tex);
        }
    }

    private void logTransaction(Relationship relationship, String action, Connection conn)
        throws ProcessingException {
        try {
            TransactionLog transLogObject = new TransactionLog();
            transLogObject.setAction(action);
            transLogObject.setRelationship(relationship);
            transLogObject.setTimestamp(new Date());
            transLogObject.setSystemUser(userId);
            transactionLogManager.addTransactionLog(conn, transLogObject);
        } catch (TransactionLogException tex){
            throw new ProcessingException(tex);
        }
    }

    private RelationshipDef getRelationshipDef(RelationshipDef relDef, Connection conn) {
        try {
            if (relDef != null) {
                if (relDef.getName() != null &&
                    relDef.getSourceDomain() != null &&
                    relDef.getTargetDomain() != null) {
                    RelationshipDefService relDefService = new RelationshipDefService(conn);
                    relDef = relDefService.getRelationshipDef(relDef.getName(),
                                                              relDef.getSourceDomain(),
                                                              relDef.getTargetDomain());
                }
            }
            return relDef;
        } catch (RelationshipDefDaoException rex) {
        } finally {
            return relDef;
        }
    }
}
