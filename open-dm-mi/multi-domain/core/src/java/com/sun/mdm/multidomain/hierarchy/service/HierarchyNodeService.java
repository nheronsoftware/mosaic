/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common
 * Development and Distribution License ("CDDL")(the "License"). You
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the
 * specific language governing permissions and limitations under the
 * License.
 *
 * When distributing the Covered Code, include this CDDL Header Notice
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the
 * fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.multidomain.hierarchy.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.sql.Connection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.rmi.RemoteException;
        
import com.sun.mdm.multidomain.hierarchy.ops.exceptions.HierarchyDaoException;
import com.sun.mdm.multidomain.hierarchy.ops.exceptions.HierarchyEavDaoException;
import com.sun.mdm.multidomain.hierarchy.ops.factory.AbstractDaoFactory;
import com.sun.mdm.multidomain.hierarchy.ops.factory.AbstractDaoFactory.DatabaseType;
import com.sun.mdm.multidomain.hierarchy.HierarchyNode;
import com.sun.mdm.multidomain.hierarchy.HierarchyDef;
import com.sun.mdm.multidomain.hierarchy.HierarchyTree;
import com.sun.mdm.multidomain.hierarchy.ops.impl.HierarchyNodeDaoImpl;
import com.sun.mdm.multidomain.hierarchy.ops.dao.HierarchyNodeDao;
import com.sun.mdm.multidomain.hierarchy.ops.dto.HierarchyNodeDto;
import com.sun.mdm.multidomain.hierarchy.ops.dto.HierarchyNodeEaDto;
import com.sun.mdm.multidomain.hierarchy.ops.dto.HierarchyDefDto;
import com.sun.mdm.multidomain.query.HierarchySearchCriteria;
import com.sun.mdm.multidomain.query.MultiDomainSearchOptions.DomainSearchOption;
import com.sun.mdm.multidomain.query.MultiDomainQuery;
import com.sun.mdm.multidomain.ejb.service.MasterIndexLocator;
import com.sun.mdm.multidomain.attributes.Attribute;
import com.sun.mdm.multidomain.attributes.AttributeType;

import com.sun.mdm.index.master.ProcessingException;
import com.sun.mdm.index.master.UserException;
import com.sun.mdm.index.ejb.master.MasterController;
import com.sun.mdm.index.objects.epath.EPathArrayList;
import com.sun.mdm.index.objects.epath.EPath;
import com.sun.mdm.index.objects.ObjectNode;
import com.sun.mdm.index.objects.EnterpriseObject;
import com.sun.mdm.index.objects.SBR;
import com.sun.mdm.index.master.search.enterprise.EOSearchCriteria;
import com.sun.mdm.index.master.search.enterprise.EOSearchOptions;
import com.sun.mdm.index.master.search.enterprise.EOSearchResultIterator;
import com.sun.mdm.index.master.search.enterprise.EOSearchResultRecord;

/**
 * @author cye
 * @author David Peh
 */
public class HierarchyNodeService implements Serializable {

    private DatabaseType mDbType = null;
    private HierarchyNodeDao mHierDef = null;
    private Connection mConn = null;
    private int maxElements = 1000;
    private static int MAX_IN_RECORDS = 1000;
   
    /**
     * Method 'HierarchyService'
     *
     */
    public HierarchyNodeService() {
        getDaoInstances();

    }

    public HierarchyNodeService(DatabaseType dbtype, Connection conn) {
        this.mDbType = dbtype;
        this.mConn = conn;
        getDaoInstances();

    }

    public HierarchyNodeService(Connection conn) {
        this.mConn = conn;
        getDaoInstances();

    }

    /**
     * Method 'getHierarchy'
     *
     */
    private void getDaoInstances() {
        if (mHierDef == null) {
            mHierDef = (HierarchyNodeDaoImpl) AbstractDaoFactory.getDoaFactory(mDbType).getHierarchyNodeDao();
        }

    }

    /**
     * Method 'addHierarchyNode'
     *
     */
    public long addHierarchyNode(HierarchyNode hierNode) 
        throws HierarchyDaoException, HierarchyEavDaoException {
        long hierID = 0;
        HierarchyNodeDaoImpl hierDao = new HierarchyNodeDaoImpl(mConn);
        try {
            hierID = hierDao.insert(hierNode);
        } catch (HierarchyDaoException ex) {
            Logger.getLogger(HierarchyNodeService.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
        return hierID;
    }

    public void addHiearchyNodes(long parentNodeId, HierarchyNode[] hierNodes) 
        throws HierarchyDaoException, HierarchyEavDaoException {
        for (HierarchyNode node : hierNodes) {
            node.setParentNodeID(parentNodeId);
            addHierarchyNode(node);
        }
    }

       
    public List<HierarchyNode> searchHierarchyNodes(DomainSearchOption searchOption, HierarchySearchCriteria searchCriteria) 
        throws HierarchyDaoException {
       
        String domain = searchOption.getDomain();
        String searchEUID = searchCriteria.getEUID();       
        if (searchOption.getMaxElements() > 0 ) {
            maxElements = searchOption.getMaxElements();
        }
        List<String> searchEUIDs = new ArrayList<String>();
        List<HierarchyNode> hierarchyNodes = new ArrayList<HierarchyNode>();  
        
        try {
            MasterController mController= new MasterIndexLocator().getMasterController(domain, mConn);
 
            if (searchEUID == null && searchCriteria != null) {  
                EPathArrayList epaths = new EPathArrayList();
                epaths.add("Enterprise.SystemSBR." + domain + ".EUID");
                EOSearchOptions eoSearchOption = new EOSearchOptions(searchOption.getSearchId(), epaths);
               eoSearchOption.setWeighted(true);
                EOSearchCriteria eoSearchCriteria = searchCriteria.getEOSearchCriteria();
                EOSearchResultIterator eoSearchResults = mController.searchEnterpriseObject(eoSearchCriteria, eoSearchOption);  
                while (eoSearchResults.hasNext()) {
                     String euid = eoSearchResults.next().getEUID();
                     searchEUIDs.add(euid);
                }
            }
            
            List<HierarchyNode> hNodes = null;           
            if (searchEUID == null) {
                hNodes = searchHierarchyNodes(domain, searchEUIDs, searchCriteria, mController);
            } else {
                searchEUIDs.add(searchEUID);
                hNodes = searchHierarchyNodes(domain, searchEUIDs, searchCriteria, mController);
            }
           
            Map<Attribute, String> attributes = null;
            if (searchCriteria.getHierarchyNode() != null) {
                attributes = searchCriteria.getHierarchyNode().getAttributes();
            }
            
            Set<Attribute> keys = attributes != null ? attributes.keySet() : null;
            if (keys != null) {
                for (HierarchyNode hNode : hNodes) {
                    boolean matched = true;
                    for (Attribute key : keys) {
                        String value = hNode.getAttributeValue(key);
                        String expected = attributes.get(key);
                        if (!expected.equals(value)) {
                            matched = false;
                            break;
                        }
                    }
                    if (matched) {
                        hierarchyNodes.add(hNode);
                    }
                }
            } else {
                hierarchyNodes.addAll(hNodes);
            }
            
        } catch (RemoteException rex) {
           new  HierarchyDaoException(rex.getMessage(), rex);
        } catch (ProcessingException pex) {
           new  HierarchyDaoException(pex.getMessage(), pex);
        } catch (UserException uex) {
           new  HierarchyDaoException(uex.getMessage(), uex);   
        }  
        return hierarchyNodes;
    }
    
    private List<HierarchyNode> searchHierarchyNodes(String domain, List<String> searchEUIDs, HierarchySearchCriteria searchCriteria, MasterController mController) 
        throws ProcessingException, UserException{       
        List<HierarchyNode> hierarchyNodes = new ArrayList<HierarchyNode>();
        for (String euid : searchEUIDs) {
            try {
                List<HierarchyNode> hNodes = search(euid);           
                for (HierarchyNode hNode : hNodes) {
                    SBR sbr = mController.getSBR(euid);
                    ObjectNode objNode =  sbr.getObject();
                    hNode.setObjectNode(objNode);
                    hierarchyNodes.add(hNode);
                }
            } catch (HierarchyDaoException hex) {
                throw new ProcessingException(hex);
            }
        }
        return hierarchyNodes;
    }
            
    public HierarchyTree getHierarchyTree(long nodeId) {
        HierarchyTree hierTree = new HierarchyTree();
        try {
 
            HierarchyNodeDaoImpl dao = new HierarchyNodeDaoImpl(mConn);
            HierarchyNode node = getHierarchyNode(nodeId);
            hierTree.setNode(node);
            
            List<HierarchyNode> children = getChildren(nodeId);
            hierTree.setChildren(children);

            List<HierarchyNode> ancestors = getAncestors(nodeId);
            hierTree.setAncestors(ancestors);
            
        } catch (HierarchyDaoException ex) {
            Logger.getLogger(HierarchyNodeService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return hierTree;
    }

    public List<HierarchyNode> getAncestors(long nodeId) throws HierarchyDaoException {
        List<HierarchyNode> ancestors = new ArrayList<HierarchyNode>();
        HierarchyNodeDaoImpl dao = new HierarchyNodeDaoImpl(mConn);
        List<HierarchyNodeDto> hierDtos = dao.findAncestors(nodeId);
        
        MasterController mController = null;
        for (HierarchyNodeDto hierDto : hierDtos) {
            try {
                HierarchyNode hierNode = new HierarchyNode();
                copyFromHierDto(hierNode, hierDto);
                if (mController == null) {
                    String domain = hierNode.getHierarchyDef().getDomain();
                    mController= new MasterIndexLocator().getMasterController(domain, mConn);              
                }
                SBR sbr = mController.getSBR(hierNode.getEUID());
                ObjectNode objNode =  sbr.getObject();
                hierNode.setObjectNode(objNode);
                ancestors.add(hierNode);
            } catch (ProcessingException pex) {
            } catch (UserException uex) {
            }            
        }
        return ancestors;
    }
    
    public List<HierarchyNode> getChildren(long nodeId) throws HierarchyDaoException {
        
       List<HierarchyNode> children = new ArrayList<HierarchyNode>();
       HierarchyNodeDaoImpl dao = new HierarchyNodeDaoImpl(mConn);
       List<HierarchyNodeDto> childrenDto = dao.findChildren(nodeId);
       
       MasterController mController = null;
       for (HierarchyNodeDto dto : childrenDto) {
            try {
                HierarchyNode node = new HierarchyNode();
                copyFromHierDto(node, dto);
                if (mController == null) {
                    String domain = node.getHierarchyDef().getDomain();
                    mController= new MasterIndexLocator().getMasterController(domain, mConn);              
                }
                SBR sbr = mController.getSBR(node.getEUID());
                ObjectNode objNode =  sbr.getObject();
                node.setObjectNode(objNode);                
                children.add(node);
           } catch (ProcessingException pex) {
           } catch (UserException uex) {
           }
       }
       
       return children;
    }
    
    /**
     * Method 'delete'
     * @throws HierarchyDaoException 
     *
     */
    public void deleteHierarchy(long nodeId) throws HierarchyDaoException {
        HierarchyNodeDaoImpl hierDao = new HierarchyNodeDaoImpl(mConn);
        hierDao.delete(nodeId);
    }
    
    /**
     * Deletes the node and moves children up to parent node
     * 
     * @param nodeId the id of the node to delete
     * @throws HierarchyDaoException
     */
    public void deleteHierarchyNode(long nodeId) throws HierarchyDaoException {
        List<HierarchyNodeDto> children = null;
        
        HierarchyNodeDaoImpl dao = new HierarchyNodeDaoImpl(mConn);
        children = dao.findChildren(nodeId);        
        HierarchyNodeDto node = dao.findNode(nodeId);
        
        //cannot delete root node and move children up
        if (node.getParentEuid() != null && node.getParentNodeId() > 0) {
            for (HierarchyNodeDto child : children) {
                HierarchyNode childNode = new HierarchyNode();
                copyFromHierDto(childNode, child);
                childNode.setParentEUID(node.getParentEuid());
                childNode.setParentNodeID(node.getParentNodeId());
                update(childNode);
            }
        }
        dao.delete(nodeId);
    }
    
    public HierarchyNode getHierarchyNode(long nodeId) throws HierarchyDaoException {
        HierarchyNodeDaoImpl dao = new HierarchyNodeDaoImpl(mConn);
        HierarchyNode node = new HierarchyNode();
        HierarchyNodeDto dtoNode = dao.findNode(nodeId);
       
        copyFromHierDto(node, dtoNode);
        try {
            String domain = node.getHierarchyDef().getDomain();
            MasterController mController= new MasterIndexLocator().getMasterController(domain, mConn);
            SBR sbr = mController.getSBR(node.getEUID());
            ObjectNode objNode =  sbr.getObject();
            node.setObjectNode(objNode);
            
            List<HierarchyNode> children = this.getChildren(nodeId);
            
            node.setChildren(children);
            
        } catch (UserException uex) {
            throw new HierarchyDaoException(uex.getMessage(), uex);
        } catch (ProcessingException pex) {
            throw new HierarchyDaoException(pex.getMessage(), pex);
        }
        return node;
    }
    
    public HierarchyNode getRootNode(long hierarchyDefId) throws HierarchyDaoException {
        
        HierarchyNodeDaoImpl dao = new HierarchyNodeDaoImpl(mConn);
        HierarchyNode node = new HierarchyNode();
        HierarchyNodeDto dtoNode;
        dtoNode = dao.getRootHierarchyNode(hierarchyDefId);
        
        if (dtoNode != null) {
            
            copyFromHierDto(node, dtoNode);            
            List<HierarchyNode> children = this.getChildren(node.getNodeID());
            node.setChildren(children);
            
            try {
                HierarchyDefDto hierDef = dtoNode.getHierarchyDef();
                MultiDomainQuery mDomainQuery = new MultiDomainQuery();
                String domain = hierDef.getDomain();
                String euid = node.getEUID();
                ObjectNode objNode = mDomainQuery.getEnterpriseObject(domain, euid, mConn);
                node.setObjectNode(objNode);
                
                for (HierarchyNode child : node.getChildren()) {
                    euid = child.getEUID();
                    objNode = mDomainQuery.getEnterpriseObject(domain, euid, mConn);
                    child.setObjectNode(objNode);                  
                }
                
            } catch (UserException uex) {
                new HierarchyDaoException(uex.getMessage(), uex);
            } catch (ProcessingException pex) {
                new HierarchyDaoException(pex.getMessage(), pex);
            }
            
            return node;
        } else {
            return null;
        } 
    }

    public void moveHierarchyNodes(List<HierarchyNode> nodes, HierarchyNode parentNode) throws HierarchyDaoException {
        for (HierarchyNode node : nodes) {
            node.setParentEUID(parentNode.getEUID());
            node.setParentNodeID(parentNode.getNodeID());
            update(node);
        }
    }

    /**
     * Method 'update'
     *
     */
    public void update(HierarchyNode hier) throws HierarchyDaoException {
        /* Hierarchy object */
        HierarchyNodeDaoImpl dao = new HierarchyNodeDaoImpl(mConn);
        dao.update(hier);
    }

    /**
     * Method 'search'
     *
     * @return HierarchyDto
     */
    public List<HierarchyNode> search(String euid) throws HierarchyDaoException {
        HierarchyNodeDaoImpl dao = new HierarchyNodeDaoImpl(mConn);
        ArrayList<HierarchyNode> hierList = new ArrayList<HierarchyNode>();
        for (HierarchyNodeDto hierDto : dao.search(euid)) {
            HierarchyNode hier = new HierarchyNode();
            copyFromHierDto(hier, hierDto);
            hierList.add(hier);
        }
        return hierList;
    }

    /**
     * Method 'copyTorelDto'
     *
     */
    private void copyToHierDto(HierarchyNode hier, HierarchyNodeDto dto) {
        dto.setHierarchyDefId(hier.getHierarchyDef().getId());
        dto.setParentNodeId(hier.getParentNodeID());
        dto.setEuid(hier.getEUID());
        dto.setParentEuid(hier.getParentEUID());
        dto.setEffectiveFromDate(hier.getEffectiveFromDate());
        dto.setEffectiveToDate(hier.getEffectiveToDate());
    }

    private HierarchyDef copyHierDefDto(HierarchyDefDto hierDefDto) {
        HierarchyDef hierDef = new HierarchyDef();
        hierDef.setDomain(hierDefDto.getDomain());
        hierDef.setName(hierDefDto.getHierarchyName());
        hierDef.setId(hierDefDto.getHierarchyDefId()); 
        return hierDef;
    }
    
    /**
     * Method 'copyFromrelDto'
     *
     */
    private void copyFromHierDto(HierarchyNode hier, HierarchyNodeDto hierDto) {
        hier.setNodeID(hierDto.getHierarchyNodeId());
        hier.setParentNodeID(hierDto.getParentNodeId());
        hier.setEUID(hierDto.getEuid());
        hier.setParentEUID(hierDto.getParentEuid());
        hier.setEffectiveFromDate(hierDto.getEffectiveFromDate());
        hier.setEffectiveToDate(hierDto.getEffectiveToDate());
        
        HierarchyDef hierDef = copyHierDefDto(hierDto.getHierarchyDef());
        hier.setHierarchyDef(hierDef);
        
        Iterator iter = hierDto.getHierarchyAttributes().keySet().iterator();
        while (iter.hasNext()) {
            HierarchyNodeEaDto eaDto = (HierarchyNodeEaDto) iter.next();
            String attrValue = hierDto.getHierarchyAttributes().get(eaDto);
            Attribute attr = new Attribute();
            attr.setId(eaDto.getEaId());
            attr.setColumnName(eaDto.getColumnName());
            attr.setType(AttributeType.valueOf(eaDto.getColumnType()));
            attr.setIsRequired(eaDto.getIsRequired());
            attr.setIsSearchable(eaDto.getIsSearchable());
            attr.setName(eaDto.getAttributeName());
            hier.setAttributeValue(attr, attrValue);
        }
    }
}
