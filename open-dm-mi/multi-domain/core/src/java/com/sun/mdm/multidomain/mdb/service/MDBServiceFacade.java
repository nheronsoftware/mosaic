/* *****************************************************************************
 *
 *  Copyright (c) 2011, NetGen Software Inc., All Rights Reserved
 *
 *  This program, and all the routines referenced herein, are the proprietary
 *  properties and trade secrets of NetGen Software Inc.
 *
 *  Except as provided for by license agreement, this program shall not be
 *  duplicated, used, or disclosed without written consent signed by an officer
 *  of NetGen Software Inc.
 *
 * ****************************************************************************/
package com.sun.mdm.multidomain.mdb.service;

import javax.jms.Message;
import javax.ejb.MessageDrivenContext;
import com.sun.mdm.multidomain.ejb.service.MultiDomainService;
import com.sun.mdm.multidomain.ejb.service.MultiDomainMetaService;
import com.sun.mdm.multidomain.sync.SyncUtils;
import com.sun.mdm.multidomain.sync.SyncEvent;
import com.sun.mdm.multidomain.sync.SyncException;
import com.sun.mdm.multidomain.sync.SyncContainer;
import com.sun.mdm.multidomain.sync.SyncManager;
import com.sun.mdm.multidomain.parser.Plugin;
import com.sun.mdm.multidomain.parser.ParserException;

/**
 * MDBServiceFacade class.
 * @author cye
 */
public class MDBServiceFacade {

    private MessageDrivenContext context;
    private SyncContainer relationshipSyncContainer;
    private MultiDomainMetaService multiDomainMetaService;
    private MultiDomainService multiDomainService;
    private String domain;

    public MDBServiceFacade(){
    }

    public void create(String domain, MessageDrivenContext context) {
        this.domain = domain;
        this.context = context;
        relationshipSyncContainer = SyncManager.getInstance().getSyncContainer(domain, Plugin.PluginType.RELATIONSHIP);
    }

    public void init(MultiDomainMetaService multiDomainMetaService, MultiDomainService multiDomainService) {
        this.multiDomainMetaService = multiDomainMetaService;
        this.multiDomainService = multiDomainService;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public void handle(Message message) {
        try {
            SyncEvent event = SyncUtils.pasrseMessage(message);
            event.setDomain(domain);

            if (!relationshipSyncContainer.isInitialized()) {
                relationshipSyncContainer.setMultiDomainService(multiDomainService);
                relationshipSyncContainer.setMultiDomainMetaService(multiDomainMetaService);
                relationshipSyncContainer.start();
            }
            relationshipSyncContainer.deliverEvent(event);
        } catch (ParserException pex) {
            context.setRollbackOnly();
        } catch (SyncException syncex) {
            context.setRollbackOnly();
        }
    }

    public void remove() {
        try {
            relationshipSyncContainer.shutdown();
            SyncManager.getInstance().removeSyncContainer(domain, Plugin.PluginType.RELATIONSHIP);
        } catch (SyncException syncex) {
        }
    }
}
