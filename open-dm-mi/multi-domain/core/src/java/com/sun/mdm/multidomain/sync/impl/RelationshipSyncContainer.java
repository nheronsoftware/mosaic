/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2009 NetServices, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.multidomain.sync.impl;

import java.util.List;
import java.util.ArrayList;
import net.java.hulp.i18n.Logger;
import com.sun.mdm.multidomain.util.Localizer;
import com.sun.mdm.index.master.ProcessingException;
import com.sun.mdm.index.master.UserException;
import com.sun.mdm.multidomain.relationship.RelationshipDef;
import com.sun.mdm.multidomain.parser.Plugin;
import com.sun.mdm.multidomain.relationship.RelationshipSync;
import com.sun.mdm.multidomain.sync.SyncContainer;
import com.sun.mdm.multidomain.sync.SyncEvent;
import com.sun.mdm.multidomain.sync.SyncEvent.EventType;
import com.sun.mdm.multidomain.sync.Registry;
import com.sun.mdm.multidomain.sync.SyncException;
import com.sun.mdm.multidomain.relationship.exceptions.RelationshipException;
import com.sun.mdm.multidomain.relationship.Relationship;
import com.sun.mdm.multidomain.relationship.RelationshipSyncRule;
import com.sun.mdm.multidomain.relationship.RelationshipPair;
import com.sun.mdm.multidomain.ejb.service.MultiDomainMetaService;
import com.sun.mdm.multidomain.ejb.service.MultiDomainService;
import com.sun.mdm.multidomain.query.MDSearchCriteria;

/**
 * RelationshipSyncContainer class.
 * @author cye
 */
public class RelationshipSyncContainer extends SyncContainer {
    
    private static Logger logger = Logger.getLogger("com.sun.mdm.multidomain.sync.impl.RelationshipSyncContainer");
    private static Localizer localizer = Localizer.getInstance();
    
    private List<RelationshipSyncHandler> relationshipSyncHandlers;
      
    public RelationshipSyncContainer(String domain){
        super(domain);
        relationshipSyncHandlers = new ArrayList<RelationshipSyncHandler>();
    }
            
    public synchronized void addRelationshipSync(RelationshipSyncRule syncRule) 
        throws SyncException {
        boolean founded = false;
        for (RelationshipSyncHandler relSyncHandler : relationshipSyncHandlers) {
            if(relSyncHandler.getSyncRule().equals(syncRule)) {
                founded = true;
                break;
            }
        }
        if(!founded) {
            RelationshipSyncHandler relSyncHandler = createRelationshipSyncHandler(syncRule);
            relationshipSyncHandlers.add(relSyncHandler);
        }
    }
 
    public synchronized void removeRelationshipSync(RelationshipSyncRule syncRule) {
        for (RelationshipSyncHandler relSyncHandler : relationshipSyncHandlers) {
            if(relSyncHandler.getSyncRule().equals(syncRule)) {
                relationshipSyncHandlers.remove(relSyncHandler);
                break;
            }
        }   
    }
    
    private synchronized RelationshipSyncHandler createRelationshipSyncHandler(RelationshipSyncRule syncRule) 
        throws SyncException {
        RelationshipSyncHandler relSyncHandler = new RelationshipSyncHandler();
        try {
            Plugin plugin = syncRule.getPlugin();
            Class relSyncClz = Class.forName(plugin.getClz());
            RelationshipSync relSync = (RelationshipSync)relSyncClz.newInstance();
            relSyncHandler.setRelationshipSync(relSync);
            relSyncHandler.setSynRule(syncRule);
        } catch (ClassNotFoundException cex) {
            throw new SyncException(cex);
        } catch (InstantiationException iex) {
             throw new SyncException(iex);
        } catch (IllegalAccessException iex) {
             throw new SyncException(iex);
        } 
        return relSyncHandler;
    }
    
    public synchronized void start() 
        throws SyncException {
         try {
            MultiDomainMetaService mdMetaService = getMultiDomainMetaService();
            List<RelationshipDef> relDefs = mdMetaService.getRelationshipDefs();
            for (RelationshipDef relDef : relDefs) {
                Plugin plugin = Registry.getInstance().getEntry(relDef.getPlugin());
                if (getDomain().equals(relDef.getSourceDomain())) {
                    RelationshipSyncRule syncRule = new RelationshipSyncRule();
                    syncRule.setPlugin(plugin);
                    syncRule.setRelationshipDef(relDef);
                    addRelationshipSync(syncRule);
                }
                if (getDomain().equals(relDef.getTargetDomain())) {
                    RelationshipSyncRule syncRule = new RelationshipSyncRule();
                    syncRule.setPlugin(plugin);
                    syncRule.setRelationshipDef(relDef);
                    addRelationshipSync(syncRule);                   
                }               
            }
            initialized = true;
        } catch (ProcessingException pex) {
            throw new SyncException(pex);
        }     
    }
    
    public synchronized void deliverEvent(SyncEvent event)  
        throws SyncException {
            
            for (RelationshipSyncHandler relSyncHandler : relationshipSyncHandlers) {
                if (EventType.MRG.equals(event.getType())) {
                    processMerge(event, relSyncHandler);
                } else if (EventType.UNMRG.equals(event.getType())) {
                    processUnmerge(event, relSyncHandler);
                } else {
                    logger.warn(localizer.x("MDS001: event: " + event.getType().toString() + " from domain:" + getDomain() + " is not supported."));  
                }
            }
            logger.info(localizer.x("MDS001: event: " + event.getType().toString() + " from domain:" + getDomain() + " has been processed."));  
    }
    
    private void processMerge(SyncEvent event, RelationshipSyncHandler relSyncHanlder) 
        throws SyncException {
        try {
            MultiDomainService mdService = getMultiDomainService();
            String domain = event.getDomain();
            RelationshipSyncRule syncRule = (RelationshipSyncRule)relSyncHanlder.getSyncRule();
            RelationshipDef relDef = syncRule.getRelationshipDef();
            RelationshipSync relSync = relSyncHanlder.getRelationshipSync();

            MDSearchCriteria mdSearchCriteria = new MDSearchCriteria();
            mdSearchCriteria.setRelationshipDef(relDef);
            String fromEuid = event.getFromEuid();
            List<Relationship> fromRels = mdService.searchRelationships(domain, fromEuid, mdSearchCriteria);   
            String toEuid = event.getToEuid();
            List<Relationship> toRels = mdService.searchRelationships(domain, toEuid, mdSearchCriteria);

            List<RelationshipPair> conflictRels = new ArrayList<RelationshipPair>();
            List<Relationship> newRels = new ArrayList<Relationship>();

            if (domain.equals(relDef.getSourceDomain())) {
                // source domain
                for (Relationship fromRel : fromRels) {
                    fromRel.setSourceEUID(toEuid);
                }
            } else if (domain.equals(relDef.getTargetDomain())) {
               // target domain
                for (Relationship fromRel : fromRels) {
                    fromRel.setTargetEUID(toEuid);
                }
            }

            for (Relationship fromRel : fromRels) {
                boolean conflicted = false;
                for (Relationship toRel : toRels) {
                    if (isRelationshipConflict(fromRel,toRel)) {
                        conflictRels.add(new RelationshipPair(fromRel,toRel));
                        conflicted = true;
                    } 
                }
                if (!conflicted) {
                    newRels.add(fromRel);
                }
            }
            
            List<RelationshipPair> solvedRels = new ArrayList<RelationshipPair>();
            if (!conflictRels.isEmpty()) {
                solvedRels = relSync.merge(domain,fromEuid,toEuid,conflictRels);
            }

            for (RelationshipPair relPair : solvedRels) {
                mdService.updateRelationship(relPair.getToRel());
                mdService.deleteRelationshipById(relPair.getFromRel().getRelationshipId());
            }

            for (Relationship rel : newRels) {
                mdService.updateRelationship(rel);
            }
            
        } catch (RelationshipException rex) {
            throw new SyncException(rex);
        } catch (UserException uex) {
            throw new SyncException(uex);
        } catch (ProcessingException pex) {
            throw new SyncException(pex);
        }
    }
    
    private boolean isRelationshipConflict(Relationship fromRel, Relationship toRel) {
        if (toRel.getSourceEUID().equals(fromRel.getSourceEUID()) &&
            toRel.getTargetEUID().equals(fromRel.getTargetEUID())) {
            return true;
        } else {
            return false;
        }
    }
            
    private void processUnmerge(SyncEvent event, RelationshipSyncHandler relSyncHanlder) 
        throws SyncException {
        try {
            MultiDomainService mdService = getMultiDomainService();
            String domain = event.getDomain();
            RelationshipSyncRule syncRule = (RelationshipSyncRule)relSyncHanlder.getSyncRule();
            RelationshipDef relDef = syncRule.getRelationshipDef();
            RelationshipSync relSync = relSyncHanlder.getRelationshipSync();

            MDSearchCriteria mdSearchCriteria = new MDSearchCriteria();
            mdSearchCriteria.setRelationshipDef(relDef);
            String toEuid = event.getToEuid();
            List<Relationship> toRels = mdService.searchRelationships(domain, toEuid, mdSearchCriteria);
            String fromEuid = event.getFromEuid();

            if (!toRels.isEmpty()) {
                List<Relationship> newRels = relSync.unmerge(domain, fromEuid, toEuid, toRels);
                for (Relationship rel : newRels) {
                     mdService.updateRelationship(rel);
                }
            }    
        } catch(RelationshipException relex) {
            throw new SyncException(relex);
        } catch (UserException uex) {
            throw new SyncException(uex);
        } catch (ProcessingException pex) {
            throw new SyncException(pex);
        }
    }        
    public synchronized void shutdown()
        throws SyncException {
        //ToDo
    }
    
}
