/* *****************************************************************************
 *
 *  Copyright (c) 2011, NetGen Software Inc., All Rights Reserved
 *
 *  This program, and all the routines referenced herein, are the proprietary
 *  properties and trade secrets of NetGen Software Inc.
 *
 *  Except as provided for by license agreement, this program shall not be
 *  duplicated, used, or disclosed without written consent signed by an officer
 *  of NetGen Software Inc.
 *
 * ****************************************************************************/
package com.sun.mdm.multidomain.ejb.service;

import java.util.List;
import java.util.Date;
import com.sun.mdm.index.master.ProcessingException;
import com.sun.mdm.index.master.UserException;
import com.sun.mdm.index.master.search.enterprise.EOSearchCriteria;
import com.sun.mdm.index.master.search.enterprise.EOSearchOptions;
import com.sun.mdm.index.master.search.enterprise.EOSearchResultRecord;
import com.sun.mdm.index.objects.ObjectNode;
import com.sun.mdm.index.objects.SystemObject;
import com.sun.mdm.multidomain.query.PageIterator;
import com.sun.mdm.multidomain.relationship.Relationship;
import com.sun.mdm.multidomain.relationship.LocalId;
import com.sun.mdm.multidomain.query.MDSearchCriteria;
import com.sun.mdm.multidomain.transaction.TransactionLog;

/**
 * MultiDomainService interface.
 * @author cye
 */
public interface MultiDomainService {

    /***
     * Create a relationship that associates two EUIDs in the database.     
     * @param relationshp Relationship including predefined and extended attributes associates two EUIDs.
     * @return long Relationship identifier if the new relationship is created.
     * @throws ProcessingException Thrown if an error occurs during processing.
     * @throws UserException Thrown if an invalid source EUID or target EUID or relationship
     * is passed as a parameter.
     */
    public long createRelationship(Relationship relationship)
        throws ProcessingException, UserException;
    
    /**
     * Create a relationship that associates two local system records when EUID is unknown to the invoker.
     * @param sourceDomain Source system code.
     * @param sourceLID Source system record local Identifier.
     * @param targetDomain Target system code.
     * @param targetLID Target system record local Identifier.
     * @param relationship Relationship attributes values including predefined and
     *                     extended attributes associates two source system records.
     * @return long Relationship identifier if the new relationship is created.
     * @throws ProcessingException Thrown if an error occurs during processing.
     * @throws UserException Thrown if an invalid source EUID or target EUID or relationship
     * is passed as a parameter.
     */
    public long createRelationshipByDomain(String sourceDomain,
                                           LocalId sourceLID,
                                           String targetDomain,
                                           LocalId targetLID,
                                           Relationship relationship)
        throws ProcessingException, UserException;

    /**
     * Update an existing relationship.
     * @param relationship Relationship including predefined and extended attributes associates two EUIDs.
     * @throws ProcessingException Thrown if an error occurs during processing.
     * @throws UserException Thrown if an invalid source EUID and target EUID or relationship is passed as a parameter.
     */
    public void updateRelationship(Relationship relationship) 
        throws ProcessingException, UserException;

   /**
     * Update an existing relationship by relationship identifier.
     * @param relationship Relationship including predefined and extended attributes associates two EUIDs.
     * @throws ProcessingException Thrown if an error occurs during processing.
     * @throws UserException Thrown if an invalid source EUID and target EUID or relationship is passed as a parameter.
     */
    public void updateRelationshipById(Relationship relationship)
        throws ProcessingException, UserException;
    
    /**
     * Delete an existing relationship by its identifier.
     * @param relationshpId Relationship Identifier
     * @throws ProcessingException Thrown if an error occurs during processing.
     * @throws UserException Thrown if an invalid relationshipid is passed as a parameter.
     */
    public void deleteRelationshipById(long relationshipId)
        throws ProcessingException, UserException;

    /**
     * Delete an existing relationship by its attributes.
     * @param relationshp Relationship including fixed and extended attributes that associates two EUIDs
     * @throws ProcessingException Thrown if an error occurs during processing.
     * @throws UserException Thrown if an invalid relationshipid is passed as a parameter.
     */
    public void deleteRelationship(Relationship relationship)
        throws ProcessingException, UserException;
            
    /**
     * Delete an existing relationship by the given source system LID and target system LID.
     * @param sourceDomain Source system code.
     * @param sourceLID Source system entity LID.
     * @param targetDomain Target system code.
     * @param targetLID Target system entity LID.
     * @param relationship Relationship including relationship definition.
     * @throws ProcessingException Thrown if an error occurs during processing.
     * @throws UserException Thrown if an invalid source system code and 
     * target system code or relationship is passed as a parameter.
     */
    public void deleteRelationshipByDomain(String sourceDomain,
                                           LocalId sourceLID,
                                           String targetDomain,
                                           LocalId targetLID,
                                           Relationship relationship)
        throws ProcessingException, UserException;

    /**
     * Get detailed relations object for the given relationship identifier or its attributes.
     * @param relationship Relationship includeing relationship definition.
     * @return Relationship Detailed relationship object.
     * @throws ProcessingException Thrown if an error occurs during processing.
     * @throws UserException Thrown if an invalid searchOptions or searchCriteria is passed as a parameter.
     */
    public  Relationship getRelationship(Relationship relationship)
        throws ProcessingException, UserException;

  /**
     * Get detailed relations object for the given relationship id defined in Relationship.
     * @param relationship Relationship.
     * @param MultiDomainSearchOptions searchOptions.
     * @return Relationship Detailed relationship object.
     * @throws ProcessingException Thrown if an error occurs during processing.
     * @throws UserException Thrown if an invalid searchOptions or searchCriteria is passed as a parameter.
     */
    public Relationship getRelationshipByDomain(String sourceDomain,
                                                LocalId sourceLID,
                                                String targetDomain,
                                                LocalId targetLID,
                                                Relationship relationship)
        throws ProcessingException, UserException;

     /**
     * Search relationship tables to retrieve relationship records that qualifies search criteria
     * and search domain and euid.
     * @param domain domain to search for relationhips
     * @param euid euid to search for relationships
     * @param searchCriteria MultiDomainSearchCriteria.
     * @return List<Relationship> A array of relationships associated with the given euid from the given domain.
     * @throws ProcessingException Thrown if an error occurs during processing.
     * @throws UserException Thrown if an invalid searchOptions or searchCriteria is passed as a parameter.
     */
    public List<Relationship> searchRelationships(String domain, 
                                                  String euid,
                                                  MDSearchCriteria searchCriteria)
        throws ProcessingException, UserException;

    /**
     * Get a list of relationships.
     * @param domain domain to search for relationships.
     * @param localId localid to search for relationships.
     * @param searchCriteria MultiDomainSearchCriteria.
     * @return Relationship A array of relationships
     * @throws ProcessingException Thrown if an error occurs during processing.
     * @throws UserException Thrown if an invalid searchOptions or searchCriteria is passed as a parameter.
     */
    public List<Relationship> searchRelationshipsByDomain(String domain, 
                                                          LocalId localId,
                                                          MDSearchCriteria searchCriteria)
       throws ProcessingException, UserException;
      
    /**
     * Get a list of relationships.
     * @param domainObject domain SystemObject to search for relationships.
     * @param searchCriteria MultiDomainSearchCriteria.
     * @return Relationship A array of relationships
     * @throws ProcessingException Thrown if an error occurs during processing.
     * @throws UserException Thrown if an invalid searchOptions or searchCriteria is passed as a parameter.
     */
    public List<Relationship> searchRelationshipsByDomainObject(SystemObject domainObject,
                                                                MDSearchCriteria searchCriteria)
        throws ProcessingException, UserException;


   /**
     * Get a list of relationships.
     * @param domain domain to search for relationships.
     * @param domainObject source Domain SystemObject to search for relationships.
     * @param domainObject target Domain SystemObject to search for relationships.
     * @param searchCriteria MultiDomainSearchCriteria.
     * @return Relationship A array of relationships
     * @throws ProcessingException Thrown if an error occurs during processing.
     * @throws UserException Thrown if an invalid searchOptions or searchCriteria is passed as a parameter.
     */
    public List<Relationship> searchRelationshipsByDomainObjects(SystemObject sourceDomainObject,
                                                                 SystemObject targetDomainObject,
                                                                 MDSearchCriteria searchCriteria)
        throws ProcessingException, UserException;

    /**
     * Get ObjectNode for the given domain and EUID.
     * @param domain Domain name.
     * @param euid EUID.
     * @return ObjectNode ObjectNode.
     * @throws ProcessingException Thrown if an error occurs during processing.
     * @throws UserException Thrown if an invalid searchOptions or searchCriteria is passed as a parameter.
     */
    public ObjectNode getEnterprise(String domain, String euid)
        throws ProcessingException, UserException;

    /**
     * Get ObjectNode for the given domain and localId.
     * @param domain Domain name.
     * @param localId Domain local Identifier.
     * @return ObjectNode ObjectNode.
     * @throws ProcessingException Thrown if an error occurs during processing.
     * @throws UserException Thrown if an invalid searchOptions or searchCriteria is passed as a parameter.
     */
    public ObjectNode getEnterpriseByDomain(String domain, LocalId localId)
        throws ProcessingException, UserException;

    /**
     * Search domain master index and get master index records that qualify the search criteria. 
     * The method does not search or retrieve the relationship tables.
     * @param domain master index domain to search 
     * @param searchOptions Master Index SearchOptions.
     * @param searchCriteria Master Index SearchCriteria.
     * @return PageSingleIterator ObjectNode Iterator.
     * @throws ProcessingException Thrown if an error occurs during processing.
     * @throws UserException Thrown if an invalid searchOptions or searchCriteria is passed as a parameter.
     */
    public PageIterator<EOSearchResultRecord> searchEnterprises(String domain, 
                                                                EOSearchOptions searchOptions,
                                                                EOSearchCriteria searchCriteria)
        throws ProcessingException, UserException;

    /**
     * Get the latest transaction log object for the specific transaction identifier.
     * @param conn data source connection.
     * @param transactionId transaction identifier.
     * @return TransactionLog transaction log object.
     * @throws ProcessingException Thrown if an error occurs during processing.
     * @throws UserException Thrown if an invalid searchOptions or searchCriteria is passed as a parameter.
     */
    public TransactionLog getTransactionLog(long transactionId)
        throws ProcessingException, UserException;

    /**
     * Get the latest transaction log object for the specific relationship type and relationhip identifier.
     * @param conn data source connection.
     * @param relDefId relationship type identifier.
     * @param relId relationship identifier.
     * @return TransactionLog transaction log object.
     * @throws ProcessingException Thrown if an error occurs during processing.
     * @throws UserException Thrown if an invalid searchOptions or searchCriteria is passed as a parameter.
     */
    public TransactionLog getTransactionLog(long relDefId, long relId)
        throws ProcessingException, UserException;

     /**
     * Get a hitory of the transaction log object for the specific relationship type and relationhip identifier.
     * @param conn data source connection.
     * @param relDefId relationship type identifier.
     * @param relId relationship identifier.
     * @return List<TransactionLog> a list of transaction log object in timestamp ASC.
     * @throws ProcessingException Thrown if an error occurs during processing.
     * @throws UserException Thrown if an invalid searchOptions or searchCriteria is passed as a parameter.
     */
    public List<TransactionLog> getTransactionLogs(long relDefId, long relId)
        throws ProcessingException, UserException;

    /**
     * Search all the transaction log objects for the specified relationship type between the specific period.
     * @param conn data source connection.
     * @param relDefId relationship type identifier.
     * @param begin search begin date.
     * @param end search end date.
     * @return List<TransactionLog> a list of transaction log object in timestamp ASC.
     * @throws ProcessingException Thrown if an error occurs during processing.
     * @throws UserException Thrown if an invalid searchOptions or searchCriteria is passed as a parameter.
     */
    public List<TransactionLog> searchTransactionLogs(long relDefId, Date begin, Date end)
        throws ProcessingException, UserException;
}
