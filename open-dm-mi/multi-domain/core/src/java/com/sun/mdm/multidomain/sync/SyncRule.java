/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2009 NetServices, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.multidomain.sync;

import com.sun.mdm.multidomain.parser.Plugin;

/**
 * SyncRule class.
 * @author cye
 */
public abstract class SyncRule {
    private Plugin plugin;
   
    public SyncRule(){    
    }
    
    public SyncRule(Plugin plugin){   
        this.plugin = plugin;
    }
    
    public void setPlugin(Plugin plugin) {
        this.plugin = plugin;
    }
    
    public Plugin getPlugin() {
        return this.plugin;
    }
    
    public void copy(SyncRule syncRule) {
        this.plugin = syncRule.getPlugin();
    }
     
    @Override
    public boolean equals(Object o) {
        if(o == this) {
            return true;
        } else if (!(o instanceof SyncRule)) {
            return false;
        } else {
            SyncRule syncRule = (SyncRule)o;
            if (this.getId().equals(syncRule.getId())) {
                return true;
            } else {
                return false;
            }
        }
    }
    
    @Override 
    public int hashCode() {
        return super.hashCode();
    }
        
    public abstract String getId();
    
}
