/* *****************************************************************************
 *
 *  Copyright (c) 2011, NetGen Software Inc., All Rights Reserved
 *
 *  This program, and all the routines referenced herein, are the proprietary
 *  properties and trade secrets of NetGen Software Inc.
 *
 *  Except as provided for by license agreement, this program shall not be
 *  duplicated, used, or disclosed without written consent signed by an officer
 *  of NetGen Software Inc.
 *
 * ****************************************************************************/
package com.sun.mdm.multidomain.transaction;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Logger;
import java.sql.Connection;
import com.sun.mdm.multidomain.relationship.Relationship;
import com.sun.mdm.multidomain.relationship.RelationshipDef;

/**
 * TransactionLogManager class.
 * @author cye
 */
public class TransactionLogManager {

    private static Logger logger = Logger.getLogger(TransactionLogManager.class.getName());

    private TransactionLogDao transactionLogDao;

    public TransactionLogManager(){
    }
    
    /**
     * Create a transaction log object for adding, updating and deleting a relationship.
     * @param conn data source connection.
     * @param transLogObject transaction log object.
     */
    public void addTransactionLog(Connection conn, TransactionLog transLogObject)
        throws TransactionLogException {
        transactionLogDao = new TransactionLogDao(conn);
        TransactionLogDto transLog = new TransactionLogDto();
        Relationship rel = transLogObject.getRelationship();
        transLog.setAction(transLogObject.getAction());
        transLog.setTimeStamp(transLogObject.getTimeStamp());
        transLog.setSystemUser(transLogObject.getSystemUser());
        transLog.setRelationshipDefId(rel.getRelationshipDef().getId());
        transLog.setRelationshipId(rel.getRelationshipId());
        String delta = TransactionLogDto.toClob(transLogObject.getRelationship());
        transLog.setDelta(delta);
        transactionLogDao.createTransatcionLog(transLog);
    }

    /**
     * Get the latest transaction log object for the specific transaction identifier.
     * @param conn data source connection.
     * @param transactionId transaction identifier.
     * @return TransactionLog transaction log object.
     * @throws TransactionLogException
     */
    public TransactionLog getTransactionLog(Connection conn, long transactionId)
        throws TransactionLogException {
        transactionLogDao = new TransactionLogDao(conn);
        TransactionLogDto transLog = transactionLogDao.getTransactionLog(transactionId);
        TransactionLog transLogObject = new TransactionLog();
        transLogObject.setAction(transLog.getAction());
        transLogObject.setSystemUser(transLog.getSystemUser());
        transLogObject.setTimestamp(transLog.getTimeStamp());
        transLogObject.setTransactionId(transLog.getTransactionId());
        Relationship rel = TransactionLogDto.fromClob(transLog.getDelta());
        RelationshipDef relDef = new RelationshipDef();
        relDef.setId(transLog.getRelationshipId());
        rel.setRelationshipDef(relDef);
        transLogObject.setRelationship(rel);
        return transLogObject;
    }

    /**
     * Get the latest transaction log object for the specific relationship type and relationhip identifier.
     * @param conn data source connection.
     * @param relDefId relationship type identifier.
     * @param relId relationship identifier.
     * @return TransactionLog transaction log object.
     * @throws TransactionLogException
     */
    public TransactionLog getTransactionLog(Connection conn, long relDefId, long relId)
        throws TransactionLogException {
        transactionLogDao = new TransactionLogDao(conn);
        TransactionLogDto transLog = transactionLogDao.getTransactionLog(relDefId, relId);
        TransactionLog transLogObject = new TransactionLog();
        transLogObject.setAction(transLog.getAction());
        transLogObject.setSystemUser(transLog.getSystemUser());
        transLogObject.setTimestamp(transLog.getTimeStamp());
        transLogObject.setTransactionId(transLog.getTransactionId());
        Relationship rel = TransactionLogDto.fromClob(transLog.getDelta());
        RelationshipDef relDef = new RelationshipDef();
        relDef.setId(transLog.getRelationshipId());
        rel.setRelationshipDef(relDef);
        transLogObject.setRelationship(rel);
        return transLogObject;
    }
    
     /**
     * Get a hitory of the transaction log object for the specific relationship type and relationhip identifier.
     * @param conn data source connection.
     * @param relDefId relationship type identifier.
     * @param relId relationship identifier.
     * @return List<TransactionLog> a list of transaction log object in timestamp ASC.
     * @throws TransactionLogException
     */
    public List<TransactionLog> getTransactionLogs(Connection conn, long relDefId, long relId)
        throws TransactionLogException {
        transactionLogDao = new TransactionLogDao(conn);
        List<TransactionLogDto> transLogs = transactionLogDao.getTransactionLogs(relDefId, relId);
        List<TransactionLog> transLogObjects = new ArrayList<TransactionLog>();
        for (TransactionLogDto transLog : transLogs) {
            TransactionLog transLogObject = new TransactionLog();
            transLogObject.setAction(transLog.getAction());
            transLogObject.setSystemUser(transLog.getSystemUser());
            transLogObject.setTimestamp(transLog.getTimeStamp());
            transLogObject.setTransactionId(transLog.getTransactionId());
            Relationship rel = TransactionLogDto.fromClob(transLog.getDelta());
            RelationshipDef relDef = new RelationshipDef();
            relDef.setId(transLog.getRelationshipId());
            rel.setRelationshipDef(relDef);
            transLogObject.setRelationship(rel);
            transLogObjects.add(transLogObject);
        }
        return transLogObjects;
    }

    /**
     * Search all the transaction log objects for the specified relationship type between the specific period.
     * @param conn data source connection.
     * @param relDefId relationship type identifier.
     * @param begin search begin date.
     * @param end search end date.
     * @return List<TransactionLog> a list of transaction log object in timestamp ASC.
     * @throws TransactionLogException
     */
    public List<TransactionLog> searchTransactionLogs(Connection conn, long relDefId, Date begin, Date end)
        throws TransactionLogException  {
        transactionLogDao = new TransactionLogDao(conn);
        List<TransactionLogDto> transLogs = transactionLogDao.searchTransactionLogs(relDefId, begin, end);
        List<TransactionLog> transLogObjects = new ArrayList<TransactionLog>();
        for (TransactionLogDto transLog : transLogs) {
            TransactionLog transLogObject = new TransactionLog();
            transLogObject.setAction(transLog.getAction());
            transLogObject.setSystemUser(transLog.getSystemUser());
            transLogObject.setTimestamp(transLog.getTimeStamp());
            transLogObject.setTransactionId(transLog.getTransactionId());
            Relationship rel = TransactionLogDto.fromClob(transLog.getDelta());
            RelationshipDef relDef = new RelationshipDef();
            relDef.setId(transLog.getRelationshipId());
            rel.setRelationshipDef(relDef);
            transLogObject.setRelationship(rel);
            transLogObjects.add(transLogObject);
        }
        return transLogObjects;
    }
}
