/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2009 NetServices, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.multidomain.sync;

import java.util.Map;
import java.util.HashMap;
import java.util.Collections;

import com.sun.mdm.multidomain.relationship.RelationshipDef;
import com.sun.mdm.multidomain.parser.Plugin;
import com.sun.mdm.multidomain.sync.impl.RelationshipSyncContainerFactory;
import com.sun.mdm.multidomain.relationship.RelationshipSyncRule;
        
/**
 * SyncManager class.
 * @author cye
 */
public class SyncManager {
    private static Object lock = new Object();
    private static SyncManager instance;
    private Map<String, SyncContainer> domainsSyncContainers;
    private Registry registry;
    private RelationshipSyncContainerFactory relSyncContainerFactory;
    
    private SyncManager(){
    }
    
    public static SyncManager getInstance() {
        synchronized (lock) {
            if (instance == null) {
                instance = new SyncManager();
                instance.initialize();
            }
            return instance;
        }
    }
    
    public void initialize(){
        domainsSyncContainers = Collections.synchronizedMap(new HashMap<String, SyncContainer>());
        registry = Registry.getInstance();
        relSyncContainerFactory = new RelationshipSyncContainerFactory();
    }

    public SyncRule getSyncRule(RelationshipDef relDef) {
        Plugin plugin = registry.getEntry(relDef.getPlugin());
        RelationshipSyncRule syncRule = new RelationshipSyncRule();
        syncRule.setPlugin(plugin);
        syncRule.setRelationshipDef(relDef);
        return syncRule;
    }
            
    public void removeSyncContainer(String domain, Plugin.PluginType type) {
        SyncContainer syncContainer = domainsSyncContainers.get(domain);
        if (syncContainer != null && type.compareTo(Plugin.PluginType.RELATIONSHIP) == 0) {
            try {
                syncContainer.shutdown();
                domainsSyncContainers.remove(domain);
            } catch(SyncException syncex) {
            }
        }
    }
      
    public SyncContainer getSyncContainer(String domain, Plugin.PluginType type) {
        SyncContainer syncContainer = domainsSyncContainers.get(domain);
        if (syncContainer == null && type.compareTo(Plugin.PluginType.RELATIONSHIP) == 0) { 
            try {
                syncContainer = relSyncContainerFactory.createSyncContainer(domain);
                domainsSyncContainers.put(domain, syncContainer);
            } catch(SyncException syncex){
            }
        }
        return syncContainer;
    }  
}
