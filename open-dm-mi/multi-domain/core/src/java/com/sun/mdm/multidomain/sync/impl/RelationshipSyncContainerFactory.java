/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sun.mdm.multidomain.sync.impl;

import com.sun.mdm.multidomain.sync.SyncContainer;
import com.sun.mdm.multidomain.sync.SyncException;
import com.sun.mdm.multidomain.sync.SyncContainerFactory;

/**
 * RelationshipSyncContainerFactory class.
 * @author cye
 */
public class RelationshipSyncContainerFactory implements SyncContainerFactory {

    public SyncContainer createSyncContainer(String domain) 
        throws SyncException {
        RelationshipSyncContainer syncContainer = new RelationshipSyncContainer(domain);
        return syncContainer;
    }
}
