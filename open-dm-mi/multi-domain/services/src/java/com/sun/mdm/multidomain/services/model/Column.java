/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2009 NetServices, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.multidomain.services.model;

import com.sun.mdm.multidomain.attributes.AttributeType;

/**
 * Column class.
 * @author cye
 */
public class Column {
    
    public enum Name {
        CHAR(0), STRING(1), FLOAT(2), INT(3), DATE(4), BOOLEAN(5);
        private int index;
        
        private Name(int index) {
            this.index = index;
        }
        
        public int index() {
            return this.index;
        }
    };
    
    private int[] counts = new int[6];
            
    public Column(){
        for(int i = 0; i < counts.length; i++ ) {
            counts[i] = 1;
        }
    }
    
    public void toCount(AttributeType type) {
        switch (type) {
            case CHAR:
                counts[Name.CHAR.index()]++;
            break;
            case STRING:
                counts[Name.STRING.index()]++;
            break;
            case FLOAT:
                counts[Name.FLOAT.index()]++;                
            break;
            case INT:
                counts[Name.INT.index()]++;                
            break;
            case DATE:
                counts[Name.DATE.index()]++;                
            break;
            case BOOLEAN:
                counts[Name.BOOLEAN.index()]++;                
            break;    
        }
    }
    
    public String toName(AttributeType type) {
        String name = null;
        switch (type) {
            case CHAR:
                name = new String("CHAR_" + counts[Name.CHAR.index()]);
                counts[Name.CHAR.index()]++;
            break;
            case STRING:
                name = new String("STRING_" + counts[Name.STRING.index()]);
                counts[Name.STRING.index()]++;
            break;
            case FLOAT:
                name = new String("FLOAT_" + counts[Name.FLOAT.index()]);
                counts[Name.FLOAT.index()]++;                
            break;
            case INT:
                name = new String("INT_" + counts[Name.INT.index()]);
                counts[Name.INT.index()]++;                
            break;
            case DATE:
                name = new String("DATE_" + counts[Name.DATE.index()]);
                counts[Name.DATE.index()]++;                
            break;
            case BOOLEAN:
                name = new String("BOOLEAN_" + counts[Name.BOOLEAN.index()]);
                counts[Name.BOOLEAN.index()]++;                
            break;    
        }
        return name;
    }
}
