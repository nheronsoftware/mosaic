/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.multidomain.services.core;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Date;
import java.util.HashMap;
import java.text.SimpleDateFormat;
import java.text.ParseException;

import net.java.hulp.i18n.Logger;

import com.sun.mdm.index.objects.epath.EPathArrayList;
import com.sun.mdm.index.objects.epath.EPathException;
import com.sun.mdm.index.objects.ObjectNode;
import com.sun.mdm.index.objects.ObjectField;
import com.sun.mdm.index.objects.SystemObject;
import com.sun.mdm.index.objects.exception.ObjectException;

import com.sun.mdm.index.master.search.enterprise.EOSearchCriteria;
import com.sun.mdm.index.master.search.enterprise.EOSearchOptions;
import com.sun.mdm.index.configurator.ConfigurationException;

import com.sun.mdm.multidomain.services.configuration.SearchResultsConfig;
import com.sun.mdm.multidomain.services.configuration.SearchScreenConfig;
import com.sun.mdm.multidomain.services.configuration.DomainScreenConfig;
import com.sun.mdm.multidomain.services.configuration.FieldConfigGroup;
import com.sun.mdm.multidomain.services.configuration.FieldConfig;
import com.sun.mdm.multidomain.services.configuration.ScreenObject;

import com.sun.mdm.multidomain.query.MultiDomainSearchCriteria;
import com.sun.mdm.multidomain.query.MultiDomainSearchOptions;
import com.sun.mdm.multidomain.query.MultiDomainSearchOptions.DomainSearchOption;
import com.sun.mdm.multidomain.relationship.Relationship;
import com.sun.mdm.multidomain.relationship.RelationshipDef;
import com.sun.mdm.multidomain.hierarchy.HierarchyNode;
import com.sun.mdm.multidomain.hierarchy.HierarchyDef;
import com.sun.mdm.multidomain.query.HierarchySearchCriteria;
import com.sun.mdm.multidomain.attributes.AttributeType;
        
import com.sun.mdm.multidomain.services.model.Attribute;
import com.sun.mdm.multidomain.services.model.DomainSearch;
import com.sun.mdm.multidomain.services.relationship.RelationshipSearch;
import com.sun.mdm.multidomain.services.relationship.RelationshipRecord;
import com.sun.mdm.multidomain.services.configuration.MDConfigManager;
import com.sun.mdm.multidomain.services.control.HierarchyManager;
import com.sun.mdm.multidomain.services.util.Localizer;
import com.sun.mdm.multidomain.services.hierarchy.HierarchyNodeRecord;
import com.sun.mdm.multidomain.services.hierarchy.HierarchySearch;
import com.sun.mdm.multidomain.services.model.ObjectRecord;

/**
 * QueryBuilder class.
 * @author cye
 */
public class QueryBuilder {
    private static final String HIERARCHY_ROOT = "rootHierarchyID";
    private static final int ROOT_NODE_ID = -1;    
    private static final String EUID_NAME = "EUID";
    private static final String SYSTEM_CODE_NAME = "SystemCode";
    private static final String LOCAL_ID_NAME = "LID";
    private static Logger logger = Logger.getLogger("com.sun.mdm.multidomain.services.core.QueryBuilder");
    private static Localizer localizer = Localizer.getInstance();
    private static HierarchyManager hierarchyManager;
    
    public QueryBuilder() throws ServiceException { 
        hierarchyManager = ServiceManagerFactory.Instance().createHierarchyManager();
    }
    /**
     * Build MultiDomainSearchOptions.
     * @param sourceDomainSearch SourceDomainSearch
     * @param targetDomainSearch TargetDomainSearch
     * @return MultiDomainSearchOptions.
     * @throws ConfigException Thrown if an error occurs during processing.
     */
    public static MultiDomainSearchOptions buildMultiDomainSearchOptions(DomainSearch sourceDomainSearch, DomainSearch targetDomainSearch)
        throws ConfigException {        
         MDConfigManager configManager =  MDConfigManager.getInstance();        
         MultiDomainSearchOptions mdSearchOptions = new MultiDomainSearchOptions();

         DomainSearchOption mdSearchOption1 = buildMultiDomainSearchOption(sourceDomainSearch);
         DomainSearchOption mdSearchOption2 = buildMultiDomainSearchOption(targetDomainSearch);
         
         ScreenObject manageRelationshipScreen = configManager.getScreenByName("manage/manage_relationship");
         mdSearchOptions.setPrimaryDomain(sourceDomainSearch.getName());
         mdSearchOptions.setPageSize(manageRelationshipScreen.getPageSize());
         mdSearchOptions.setMaxElements(manageRelationshipScreen.getMaxRecords());
         mdSearchOptions.setOptions(sourceDomainSearch.getName(), mdSearchOption1);
         mdSearchOptions.setOptions(targetDomainSearch.getName(), mdSearchOption2);
             
         return mdSearchOptions;
    }
    
    public static DomainSearchOption buildMultiDomainSearchOption(String domain)
        throws ConfigException {
        DomainSearchOption domainSearchOption = new DomainSearchOption();
        try {
            MDConfigManager configManager =  MDConfigManager.getInstance(); 
            DomainScreenConfig domainConfig = configManager.getDomainScreenConfig(domain);
            List<FieldConfig> fieldConfigs = domainConfig.getSearchResultDetailsFieldConfigs();
            EPathArrayList resultFields = new EPathArrayList();
            for(FieldConfig fieldConfig : fieldConfigs) {
                resultFields.add(fieldConfig.getName());
            }
            domainSearchOption.setDomain(domain);
            domainSearchOption.setSearchId("BLOCKER-SEARCH");
            domainSearchOption.setIsWeighted(true); 
            domainSearchOption.setOptions(resultFields);
        } catch(EPathException eex) {
            throw new ConfigException(eex);            
        }            
        return domainSearchOption; 
    }

    /**
     * Build DomainSearchOption.
     * @param domainSearch DomainSearch.
     * @return DomainSearchOption.
     * @throws ConfigException Thrown if an error occurs during processing.
     */
    public static DomainSearchOption buildMultiDomainSearchOption(DomainSearch domainSearch) 
        throws ConfigException {
        MDConfigManager configManager =  MDConfigManager.getInstance(); 
        DomainScreenConfig domainConfig = configManager.getDomainScreenConfig(domainSearch.getName());      
        DomainSearchOption mdSearchOption = new DomainSearchOption();
        
        try {
            
            String EUID = domainSearch.getAttributeValue(EUID_NAME);
            String localId = domainSearch.getAttributeValue(LOCAL_ID_NAME);
            String systemCode = domainSearch.getAttributeValue(SYSTEM_CODE_NAME);    
            if (EUID != null) {
                // TBD: do EUID search. how to pass it back-end.
            } else if (systemCode != null && localId != null) {
                // TBD: systemcode and localId search. how to pass it back-end.
            } else if (systemCode == null && localId != null) {
                new ConfigException(localizer.t("QRY001: system code for simple search is not defined for localId {0}", localId));
            } else if (systemCode != null && localId == null) {
                new ConfigException(localizer.t("QRY002: local Id for simple search is not defined for system code {0}", systemCode));
            } 
                    
            // search result page based record-id
            List<FieldConfig> fieldConfigs = domainConfig.getSummaryLabel().getFieldConfigs();
            EPathArrayList resultFields = new EPathArrayList();
            for(FieldConfig fieldConfig : fieldConfigs) {
                resultFields.add(fieldConfig.getName());
            }
            
            // search page
            List<SearchScreenConfig> searchPages = domainConfig.getSearchScreenConfigs();
            Iterator searchPageIterator = searchPages.iterator();
            String queryBuilder = null;
            boolean isWeighted = false;
            for (SearchScreenConfig searchPage : searchPages) {
                if (searchPage.getScreenTitle().equalsIgnoreCase(domainSearch.getType())) {
                    queryBuilder = searchPage.getOptions().getQueryBuilder();
                    if (searchPage.getOptions().getIsWeighted()) {
                        isWeighted = true;
                    }
                    break;
                }             
            }
             
            mdSearchOption.setDomain(domainSearch.getName());
            mdSearchOption.setOptions(resultFields);
            mdSearchOption.setSearchId(queryBuilder);
            mdSearchOption.setIsWeighted(isWeighted);   
            
        } catch(EPathException eex) {
            throw new ConfigException(eex);            
        }        
        return mdSearchOption;
    } 
 
    /**
     * Build MultiDomainSearchCriteria.
     * @param sourceDomainSearch SourceDomainSearch.
     * @param targetDomainSearch TargetDomainSearch.
     * @param relationshipSearch RelationshipSearch.
     * @return MultiDomainSearchCriteria.
     * @throws ConfigException Thrown if an error occurs during processing.
     */
    public static MultiDomainSearchCriteria buildMultiDomainSearchCriteria(DomainSearch sourceDomainSearch, DomainSearch targetDomainSearch, RelationshipSearch relationshipSearch)
        throws ConfigException {        
        MDConfigManager configManager =  MDConfigManager.getInstance();                
        MultiDomainSearchCriteria mdSearchSearchCriteria = new MultiDomainSearchCriteria();
        Relationship relationship = buildRelationship(relationshipSearch);
        EOSearchCriteria sourceEOSearchCriteria = buildEOSearchCriteria(sourceDomainSearch);
        EOSearchCriteria targetEOSearchCriteria = buildEOSearchCriteria(targetDomainSearch);
        
        mdSearchSearchCriteria.setRelationshipDef(relationship.getRelationshipDef());
        mdSearchSearchCriteria.setDomainSearchCriteria(sourceDomainSearch.getName(), sourceEOSearchCriteria);
        mdSearchSearchCriteria.setDomainSearchCriteria(targetDomainSearch.getName(), targetEOSearchCriteria);
    
        return mdSearchSearchCriteria;
    } 

    /**
     * Build system objects for the search.
     * @param domainSearch DomainSearch.
     * @return SystemObject[] Range SystemObject.
     * @throws ConfigException Thrown if an error occurs during processing.
     */
    public static SystemObject[] buildSystemObjects(DomainSearch domainSearch)
        throws ConfigException {                
        MDConfigManager configManager =  MDConfigManager.getInstance(); 
        DomainScreenConfig domainConfig = configManager.getDomainScreenConfig(domainSearch.getName());
        
        Map<String, String> searchCriteria = new HashMap<String, String>();
        Map<String, String> searchCriteriaFrom = new HashMap<String, String>();
        Map<String, String> searchCriteriaTo = new HashMap<String, String>();
            
        // search screen cofnig
        List<SearchScreenConfig> searchPages = domainConfig.getSearchScreenConfigs();
        Iterator searchPageIterator = searchPages.iterator();
        SearchScreenConfig searchPage = null;
        //TDB: a simple configuration API should be provied to get the specific searchPage for the given the search type.
        while (searchPageIterator.hasNext()) {
            searchPage = (SearchScreenConfig) searchPageIterator.next();                   
            if (searchPage.getScreenTitle().equalsIgnoreCase(domainSearch.getType())) {
                break;
            }
        }             
        
        //TDB: a simple configuration API should be provied to get a list of search field config.
        List<FieldConfigGroup> searchFieldConfigGroups = searchPage.getFieldConfigGroups();
        List<FieldConfig> searchFieldConfigs = new ArrayList<FieldConfig>();
        for (FieldConfigGroup fieldConfigGorup : searchFieldConfigGroups) {
            List<FieldConfig> fieldConfigs = fieldConfigGorup.getFieldConfigs();
            searchFieldConfigs.addAll(fieldConfigs);
        }        

        String objectRef = null;
        for (FieldConfig fieldConfig : searchFieldConfigs) {
            objectRef = fieldConfig.getRootObj();
            String fieldValue = null;                
            //Get the field value for each field config range type.
            if (fieldConfig.isRange()) {
                fieldValue = domainSearch.getAttributeValue(fieldConfig.getDisplayName());
            } else {
                fieldValue = domainSearch.getAttributeValue(fieldConfig.getDisplayName());                    
            }                
            if (fieldValue != null && fieldValue.trim().length() > 0) {
                //Remove all masking fields from the field valued if any like SSN,LID...etc
                fieldValue = fieldConfig.demask(fieldValue);                        
                if (fieldConfig.isRange() && fieldConfig.getDisplayName().endsWith("From")) {
                    searchCriteriaFrom.put(fieldConfig.getName(), fieldValue);
                } else if (fieldConfig.isRange() && fieldConfig.getDisplayName().endsWith("To")) {
                    searchCriteriaTo.put(fieldConfig.getName(), fieldValue);
                } else {
                    searchCriteria.put(fieldConfig.getName(), fieldValue);
                }
            }                         
        }
         
        SystemObject[] systemObjects = new SystemObject[3];             
        systemObjects[0] = buildSystemObject(objectRef, searchCriteria);   
        // systemObjectFrom
        if (!searchCriteriaFrom.isEmpty()) {
            systemObjects[1] = buildSystemObject(objectRef, searchCriteriaFrom);
        }
        // systemObjectTo
        if (!searchCriteriaTo.isEmpty()) {
            systemObjects[2] = buildSystemObject(objectRef, searchCriteriaTo);
        }     
        return systemObjects;
    }
    
    public static SystemObject buildDetailedSystemObject(DomainSearch domainSearch)
        throws ConfigException {                
  
        DomainScreenConfig domainConfig = MDConfigManager.getDomainScreenConfig(domainSearch.getName());      
        Map<String, String> searchCriteria = new HashMap<String, String>();
          
        domainSearch.setType("Advanced " + domainSearch.getName() + " Lookup (Phonetic)");
        // search screen cofnig
        List<SearchScreenConfig> searchPages = domainConfig.getSearchScreenConfigs();
        Iterator searchPageIterator = searchPages.iterator();
        SearchScreenConfig searchPage = null;
        //TDB: a simple configuration API should be provied to get the specific searchPage for the given the search type.
        while (searchPageIterator.hasNext()) {
            searchPage = (SearchScreenConfig) searchPageIterator.next();                   
            if (searchPage.getScreenTitle().equalsIgnoreCase(domainSearch.getType())) {
                break;
            }
        }             
        
        //TDB: a simple configuration API should be provied to get a list of search field config.
        List<FieldConfigGroup> searchFieldConfigGroups = searchPage.getFieldConfigGroups();
        List<FieldConfig> searchFieldConfigs = new ArrayList<FieldConfig>();
        for (FieldConfigGroup fieldConfigGorup : searchFieldConfigGroups) {
            List<FieldConfig> fieldConfigs = fieldConfigGorup.getFieldConfigs();
            searchFieldConfigs.addAll(fieldConfigs);
        }        

        String objectRef = null;
        for (FieldConfig fieldConfig : searchFieldConfigs) {
            objectRef = fieldConfig.getRootObj();
            String fieldValue = domainSearch.getAttributeValue(fieldConfig.getDisplayName());                
            if (fieldValue != null && fieldValue.trim().length() > 0) {
                //Remove all masking fields from the field valued if any like SSN,LID...etc
                fieldValue = fieldConfig.demask(fieldValue);                          
                searchCriteria.put(fieldConfig.getName(), fieldValue);
            }                         
        }
        SystemObject systemObject = buildSystemObject(objectRef, searchCriteria);      
        return systemObject;
    }
    
    /**
     * Build Relationship for the given relationshipSearch.
     * @param relationshipSearch RelationshipSearch.
     * @return Relationship Relationship.
     */
    public static Relationship buildRelationship(RelationshipSearch relationshipSearch)
        throws ConfigException {        
        SimpleDateFormat dateFormat = new SimpleDateFormat(MDConfigManager.getInstance().getDateFormatForMultiDomain());
        Relationship relationship = new Relationship();        
        try {
            if (relationshipSearch.getStartDate() != null) {
                relationship.setEffectiveFromDate(dateFormat.parse(relationshipSearch.getStartDate()));
            }
            if (relationshipSearch.getEndDate() != null) {
                relationship.setEffectiveToDate(dateFormat.parse(relationshipSearch.getEndDate()));
            }
            if (relationshipSearch.getPurgeDate() != null) {
                relationship.setPurgeDate(dateFormat.parse(relationshipSearch.getPurgeDate()));   
            }        
            while(relationshipSearch.hasNext()) {
                Attribute field = relationshipSearch.next();
                com.sun.mdm.multidomain.attributes.Attribute attribute = new com.sun.mdm.multidomain.attributes.Attribute();
                attribute.setName(field.getName());
                relationship.setAttributeValue(attribute, field.getValue());
            }    
        } catch (ParseException pex) {            
            throw new ConfigException(pex);
        }
        return relationship;
    }
        
    /**
     * Build EOSearchOptions.
     * @param domainSearch DomainSearch.
     * @return EOSearchOptions.
     * @throws ConfigException Thrown if an error occurs during processing.
     */
    public static EOSearchOptions buildEOSearchOptions(DomainSearch domainSearch) 
        throws ConfigException {               
        EOSearchOptions eoSearchOptions = null;
        try {
            DomainScreenConfig domainConfig = MDConfigManager.getDomainScreenConfig(domainSearch.getName());  
            // build EOSearchOptions using SearchResultsConfig
            List<SearchResultsConfig> searchResultPages = domainConfig.getSearchResultsConfigs();
            Iterator searchResultPageIterator = searchResultPages.iterator();      
            EPathArrayList searchResultFields = new EPathArrayList();
            String objectRef = null;
            int pageSize = 0;
            int maxElements = 0;        
            while(searchResultPageIterator.hasNext()) {
                SearchResultsConfig searchResultPage = (SearchResultsConfig)searchResultPageIterator.next();
                List fieldEpaths = searchResultPage.getEPaths();
                Iterator fieldEpathsIterator = fieldEpaths.iterator();
                while(fieldEpathsIterator.hasNext()) {
                    String fieldEPath = (String) fieldEpathsIterator.next();
                    searchResultFields.add("Enterprise.SystemSBR." + fieldEPath);
                    if (objectRef == null) {
                        objectRef = fieldEPath.substring(0, fieldEPath.indexOf("."));
                    }      
                }
            }   
            searchResultFields.add("Enterprise.SystemSBR." + objectRef + ".EUID");
         
            // search screen config 
            List<SearchScreenConfig> searchPages = domainConfig.getSearchScreenConfigs();
            Iterator searchPageIterator = searchPages.iterator();
            String queryBuilder = null;
            boolean isWeighted = false;
            SearchScreenConfig searchPage = null;
            //TDB: a simple API should be provied to get the specific searchPage for the given the search type.
            while (searchPageIterator.hasNext()) {
                searchPage = (SearchScreenConfig) searchPageIterator.next();                   
                if (searchPage.getScreenTitle().equalsIgnoreCase(domainSearch.getType())) {
                    queryBuilder = searchPage.getOptions().getQueryBuilder();
                    if (searchPage.getOptions().getIsWeighted()) {
                        isWeighted = true;
                    }
                    break;
                }
            }                           
            eoSearchOptions = new EOSearchOptions(queryBuilder, searchResultFields);
            eoSearchOptions.setWeighted(isWeighted);
        } catch (EPathException eex) {
            throw new ConfigException(eex);
        } catch (ConfigurationException cex) {
            throw new ConfigException(cex);
        }
        return eoSearchOptions;
    }
 
    public static EOSearchOptions buildDetailedEOSearchOptions(DomainSearch domainSearch) 
        throws ConfigException {
        EOSearchOptions eoSearchOptions = null;
        try {
            DomainScreenConfig domainConfig = MDConfigManager.getDomainScreenConfig(domainSearch.getName());  
            // build EOSearchOptions using SearchResultsConfig
            List<SearchResultsConfig> searchResultPages = domainConfig.getSearchResultsConfigs();
            Iterator searchResultPageIterator = searchResultPages.iterator();      
            EPathArrayList searchResultFields = new EPathArrayList();
            String objectRef = null;
            int pageSize = 0;
            int maxElements = 0;        
            while(searchResultPageIterator.hasNext()) {
                SearchResultsConfig searchResultPage = (SearchResultsConfig)searchResultPageIterator.next();
                List fieldEpaths = searchResultPage.getEPaths();
                Iterator fieldEpathsIterator = fieldEpaths.iterator();
                while(fieldEpathsIterator.hasNext()) {
                    String fieldEPath = (String) fieldEpathsIterator.next();
                    searchResultFields.add("Enterprise.SystemSBR." + fieldEPath);
                    if (objectRef == null) {
                        objectRef = fieldEPath.substring(0, fieldEPath.indexOf("."));
                    }      
                }
            }   
            searchResultFields.add("Enterprise.SystemSBR.EUID");
            String queryBuilder = domainSearch.getType();
            boolean isWeighted = false;
                        
            eoSearchOptions = new EOSearchOptions(queryBuilder, searchResultFields);
            eoSearchOptions.setWeighted(isWeighted);
        } catch (EPathException eex) {
            throw new ConfigException(eex);
        } catch (ConfigurationException cex) {
            throw new ConfigException(cex);
        }
        return eoSearchOptions;
    }
        
    public static EOSearchOptions xbuildDetailedEOSearchOptions(DomainSearch domainSearch) 
        throws ConfigException {
        MDConfigManager configManager =  MDConfigManager.getInstance();                
        EOSearchOptions eoSearchOptions = null;
        domainSearch.setType("Advanced " + domainSearch.getName() + " Lookup (Phonetic)");
        try {
            DomainScreenConfig domainConfig = MDConfigManager.getDomainScreenConfig(domainSearch.getName());  
            // build EOSearchOptions using SearchResultsConfig                     
            List<FieldConfig> fieldConfigs = domainConfig.getSearchResultDetailsFieldConfigs();
            String objectRef = null;
            EPathArrayList searchResultFields = new EPathArrayList();
            for(FieldConfig fieldConfig : fieldConfigs) {
                searchResultFields.add("Enterprise.SystemSBR." + fieldConfig.getName());
                if (objectRef == null) {
                    objectRef = fieldConfig.getName().substring(0, fieldConfig.getName().indexOf("."));
                }      
            }
            searchResultFields.add("Enterprise.SystemSBR." + objectRef +".EUID");
          
            String queryBuilder = "BLOCKER-SEARCH";
            boolean isWeighted = true;
                        
            eoSearchOptions = new EOSearchOptions(queryBuilder, searchResultFields);
            eoSearchOptions.setWeighted(isWeighted);
        } catch (EPathException eex) {
            throw new ConfigException(eex);
        } catch (ConfigurationException cex) {
            throw new ConfigException(cex);
        }
        return eoSearchOptions;
    }
 
    /**
     * Build EOSearchCriteria.
     * @param domainSearch
     * @return EOSearchCriteria.
     * @throws ConfigException Thrown if an error occurs during processing.
     */
    public static EOSearchCriteria buildEOSearchCriteria(DomainSearch domainSearch)
        throws ConfigException {
        EOSearchCriteria eoSearchCriteria = new EOSearchCriteria();
        SystemObject[] systemObjects = buildSystemObjects(domainSearch);
        
        /* range search supported */
        if (systemObjects[0] != null) {
            eoSearchCriteria.setSystemObject(systemObjects[0]); 
        } else {
            throw new ConfigException("Invalid systemObject for EOSearchCriteria");
        }
        if (systemObjects[1] != null) {
            eoSearchCriteria.setSystemObject2(systemObjects[1]);
        }
        if (systemObjects[2] != null) {
            eoSearchCriteria.setSystemObject3(systemObjects[2]);
        }
        return eoSearchCriteria;
    }
    
    public static EOSearchCriteria buildDetailedEOSearchCriteria(DomainSearch domainSearch)
        throws ConfigException {
       
        EOSearchCriteria eoSearchCriteria = new EOSearchCriteria();
        SystemObject systemObject = buildDetailedSystemObject(domainSearch);
        
        /* range search supported */
        if (systemObject != null) {
            eoSearchCriteria.setSystemObject(systemObject); 
        } else {
            throw new ConfigException("Invalid systemObject for EOSearchCriteria");
        }
        return eoSearchCriteria;
    }
    
    /**
     * Build relationship for the given relastionshipRecord.
     * @param relRecord RelationshipRecord.
     * @param relDef RelationshipDef.
     * @return relastionship.
     */
    public static Relationship buildRelationship(RelationshipRecord relRecord, RelationshipDef relDef)
        throws ConfigException {
        MDConfigManager configManager = MDConfigManager.getInstance(); 
        SimpleDateFormat dateFormat = new SimpleDateFormat(configManager.getDateFormatForMultiDomain());
        Relationship relationship = new Relationship();     
        
        try {
            if (relRecord.getId() == null ) {
                throw new ConfigException(localizer.t("QRY505: RelationshipDef Id cannot be null"));
            }
            relationship.setRelationshipId(Long.parseLong(relRecord.getId()));
            relationship.setSourceEUID(relRecord.getSourceEUID());
            relationship.setTargetEUID(relRecord.getTargetEUID());
            if (relRecord.getStartDate() != null) {
                relationship.setEffectiveFromDate(dateFormat.parse(relRecord.getStartDate()));
            }
            if (relRecord.getEndDate() != null) {
                relationship.setEffectiveToDate(dateFormat.parse(relRecord.getEndDate()));
            }
            if (relRecord.getPurgeDate() != null) {
                relationship.setPurgeDate(dateFormat.parse(relRecord.getPurgeDate()));
            }                          
            relationship.setRelationshipDef(relDef);
            
            /*
            com.sun.mdm.multidomain.services.model.Attribute attribute1 = null;
            while(relRecord.hasNext()) {
                attribute1 = relRecord.next();
                com.sun.mdm.multidomain.attributes.Attribute attribute2 = relDef.getAttribute(attribute1.getName());        
                attribute2.setType(relDef.getAttribute(attribute1.getName()).getType());
                if (AttributeType.DATE == attribute2.getType()) {
                    long timeVal = dateFormat.parse(attribute1.getValue()).getTime();
                    relationship.setAttributeValue(attribute2, Long.toString(timeVal));
                } else {
                    relationship.setAttributeValue(attribute2, attribute1.getValue());
                }
            }
            */          
            List<com.sun.mdm.multidomain.attributes.Attribute> attributes = relDef.getAttributes();        
            for (com.sun.mdm.multidomain.attributes.Attribute attribute : attributes) {
                String value = relRecord.getAttributeValue(attribute.getName());
                 if (AttributeType.DATE == attribute.getType() && value != null) { 
                    long timeVal = dateFormat.parse(value).getTime();
                    relationship.setAttributeValue(attribute, Long.toString(timeVal));            
                 } else {
                     relationship.setAttributeValue(attribute, value);
                 }
                
            }
                 
        } catch(ParseException pex) {            
            throw new ConfigException(pex);
        }
        return relationship;
    } 
    
    public static SystemObject buildSystemObject(String objectName, Map<String, String> searchCriteria) 
        throws ConfigException {	
	SystemObject so = null;
	try {
            ObjectFactory objectFactory = ObjectFactoryRegistry.lookup(objectName);
            ObjectNode topNode = objectFactory.create(objectName);
            
            Iterator<String> keys = searchCriteria.keySet().iterator();        
            while (keys.hasNext()) {
                String key = (String) keys.next();
		String value = (String) searchCriteria.get(key);
		if (value != null && value.trim().length() > 0) {
                    
                    int index = key.indexOf(".");
                    if (index > -1) {                      
                        String nodeName = key.substring(0, index);
			String fieldName = key.substring(index + 1);
                        
                        index = fieldName.indexOf(".");
                        if (index > -1) {
                            nodeName = fieldName.substring(0, index);
                            fieldName = fieldName.substring(index + 1);         
                        }
                        
			if (nodeName.equalsIgnoreCase(objectName)) {
                            setObjectNodeFieldValue(topNode, fieldName, value);
			} else {
                            ArrayList<ObjectNode> children = topNode.pGetChildren(nodeName);
                            ObjectNode childNode = null;
                            if (children == null) {
                                childNode = objectFactory.create(nodeName);
				topNode.addChild(childNode);
                            } else {
                                childNode = (ObjectNode) children.get(0);
                            }                        
                            setObjectNodeFieldValue(childNode, fieldName, value);
                        }
                    }
                 }            
            }        
            so = new SystemObject();
            so.setValue("ChildType", objectName);
            so.setObject(topNode);        
	} catch(ObjectException oe) {
            throw new ConfigException(oe);
	}
        return so;		
    }   

    public static ObjectNode buildObjectNode(String objectName, Map<String, String> searchCriteria) 
        throws ConfigException {		
	ObjectNode objectNode = null;
	try {
            ObjectFactory objectFactory = ObjectFactoryRegistry.lookup(objectName);
            objectNode = objectFactory.create(objectName);
            Iterator<String> keys = searchCriteria.keySet().iterator();        
            while (keys.hasNext()) {
                String key = (String) keys.next();
                String value = (String) searchCriteria.get(key);
        
                if ((value != null) && (value.trim().length() > 0)) {
                    int index = key.indexOf(".");
                    if (index > -1) {
                        String tmpRef = key.substring(0, index);
			String fieldName = key.substring(index + 1);
			if (tmpRef.equalsIgnoreCase(objectName)) {
                            setObjectNodeFieldValue(objectNode, fieldName, value);
			} else {
                            ArrayList<ObjectNode> childNodes = objectNode.pGetChildren(tmpRef);
                            ObjectNode node = null;
                            if (childNodes == null) {
                                node = objectFactory.create(tmpRef);
				objectNode.addChild(node);
                            } else {
                                node = (ObjectNode) childNodes.get(0);
                            }                        
                            setObjectNodeFieldValue(node, fieldName, value);
                        }
                    }
                 }            
            }        
	} catch(ObjectException oe) {
            throw new ConfigException(oe);
	}
        return objectNode;
    }
	
    public static void setObjectNodeFieldValue(ObjectNode node, String field, String value)
        throws ObjectException, ConfigException {
        MDConfigManager configManager =  MDConfigManager.getInstance();                
        if (value == null) {
            if (node.isNullable(field)) {
                node.setValue(field, null);
            	return;
            } else {
                //ObjectNodeConfig config = configManager.getInstance().getObjectNodeConfig(node.pGetType());
                //String fieldDisplayName = config.getFieldConfig(field).getDisplayName(); 
                throw new ObjectException("Field [" + field + "] is required");
            }
        }
        int type = node.pGetType(field);
        try {
            switch (type) {
            case ObjectField.OBJECTMETA_DATE_TYPE:
            case ObjectField.OBJECTMETA_TIMESTAMP_TYPE:
                node.setValue(field, (Object) new SimpleDateFormat("mm/dd/yyyy").parse(value));
            	break;
            case ObjectField.OBJECTMETA_INT_TYPE:
            	node.setValue(field, (Object) Integer.valueOf(value));
            	break;
            case ObjectField.OBJECTMETA_BOOL_TYPE:
            	node.setValue(field, (Object) Boolean.valueOf(value));
            	break;
            case ObjectField.OBJECTMETA_BYTE_TYPE:
            	node.setValue(field, (Object) Byte.valueOf(value));
            	break;
            case ObjectField.OBJECTMETA_CHAR_TYPE:
            	node.setValue(field, (Object) new Character(value.charAt(0)));
            	break;
            case ObjectField.OBJECTMETA_LONG_TYPE:
            	node.setValue(field, (Object) Long.valueOf(value));
            	break;
            case ObjectField.OBJECTMETA_FLOAT_TYPE:
            	node.setValue(field, (Object) Float.valueOf(value));
            	break;
            case ObjectField.OBJECTMETA_STRING_TYPE:
            default:
            	node.setValue(field, (Object) value);
		break;
            }
        } catch (ParseException pex) {
            throw new ConfigException(pex);
        } catch (NumberFormatException nex) {
            //ObjectNodeConfig config = configManager.getInstance().getObjectNodeConfig(node.pGetType());
            //String fieldDisplayName = config.getFieldConfig(field).getDisplayName(); 
            throw new ObjectException("Invalid value [" + value + "] for field [" + field + "]"); 
        }
   }
    
   /**
    * Build HierarchyNode for the given HierarchyNodeRecord.  Only the immediate children
    * will be instantiated.
    * @param hNodeRecord HierarchyNodeRecord.
    * @return HierarchyNode HierarchyNode.
    * @throws ServiceException if an error occurs during processing.
    */ 
    public static HierarchyNode buildHierarchyNode(HierarchyNodeRecord hNodeRecord, HierarchyDef hDef) 
            throws ServiceException{
        return buildHierarchyNode(hNodeRecord, hDef, false);
        
    }
   /**
    * Build HierarchyNode for the given HierarchyNodeRecord.
    * @param hNodeRecord HierarchyNodeRecord.
    * @param instantiateChildren  Set to true if immediate children are to be instantiated,
    * false otherwise.
    * @return HierarchyNode HierarchyNode.
    * @throws ServiceException if an error occurs during processing.
    */ 
    public static HierarchyNode buildHierarchyNode(HierarchyNodeRecord hNodeRecord, HierarchyDef hDef,
                                                   boolean instantiateChildren) 
        throws ServiceException {
        if (hNodeRecord == null) {
            throw new ServiceException(localizer.t("QRY503: The hNodeRecord parameter cannot be null"));
        }
        HierarchyNode hNode = new HierarchyNode();
        int nodeId = -1;     
        try {
            MDConfigManager configManager =  MDConfigManager.getInstance(); 
            SimpleDateFormat dateFormat = new SimpleDateFormat(configManager.getDateFormatForMultiDomain());        
              
            String id = hNodeRecord.getId();
            if (id != null) {
                nodeId = Integer.parseInt(id);
            }
            hNode.setNodeID(nodeId);
            hNode.setEUID(hNodeRecord.getEUID());
            
            if (HIERARCHY_ROOT.equals(hNodeRecord.getParentId())) {
                hNode.setParentNodeID(ROOT_NODE_ID);
                hNode.setParentEUID(null);
            } else {
                hNode.setParentNodeID(Integer.parseInt(hNodeRecord.getParentId()));
                hNode.setParentEUID(hNodeRecord.getParentEUID());
            }
            if (hNodeRecord.getStartDate() != null) {
                hNode.setEffectiveFromDate(dateFormat.parse(hNodeRecord.getStartDate()));
            }
            if (hNodeRecord.getEndDate() != null) {
                hNode.setEffectiveToDate(dateFormat.parse(hNodeRecord.getEndDate()));
            }
            if (hNodeRecord.getPurgeDate() != null) {
                hNode.setPurgeDate(dateFormat.parse(hNodeRecord.getPurgeDate()));
            }

            /*
            List<Attribute> attributeList = hNodeRecord.getAttributes();
            for (Attribute attribute1 : attributeList) {  
                com.sun.mdm.multidomain.attributes.Attribute attribute2 = hDef.getAttribute(attribute1.getName());
                if (AttributeType.DATE == attribute2.getType()) {
                    long timeVal = dateFormat.parse(attribute1.getValue()).getTime();
                    hNode.setAttributeValue(attribute2, Long.toString(timeVal));
                } else {       
                    hNode.setAttributeValue(attribute2, attribute1.getValue());
                }
            }
            */
            
            List<com.sun.mdm.multidomain.attributes.Attribute> attributeList = hDef.getAttributes();
            for (com.sun.mdm.multidomain.attributes.Attribute attribute : attributeList) {
                String value = hNodeRecord.getAttributeValue(attribute.getName());
                if (AttributeType.DATE == attribute.getType() && value != null) {
                    long timeVal = dateFormat.parse(value).getTime();
                    hNode.setAttributeValue(attribute, Long.toString(timeVal));
                } else {
                    hNode.setAttributeValue(attribute, value);
                }
            }

            hNode.setHierarchyDef(hDef);
            ObjectRecord objectRecord = hNodeRecord.getObjectRecord();
            if (objectRecord != null) {
                String objectName = objectRecord.getName();
                List<Attribute> objectRecordAttributeList = objectRecord.getAttributes();
                HashMap<String, String> searchCriteria = new HashMap<String, String>();
                for (Attribute attr : objectRecordAttributeList) {
                    String attrName = attr.getName();
                    String value = attr.getValue();
                    searchCriteria.put(attrName, value);
                }       
                ObjectNode objectNode = buildObjectNode(objectName, searchCriteria);
                hNode.setObjectNode(objectNode);
            }
            
        } catch (ConfigException cex) {   
            throw new ServiceException(cex);
        } catch(ParseException pex) {
            throw new ServiceException(pex);
        }
             
        // The HierarchyNode for the parent is not needed.  It should be handled
        // by the backend APIs.

        // We don't necessarily need to instantiate all children--just
        // the immediate children.
        if (instantiateChildren == true && hNode.getParentNodeID() != ROOT_NODE_ID) {    
            ArrayList<HierarchyNode> children = new ArrayList();
            List<HierarchyNodeRecord> hNodeRecordChildren = hierarchyManager.getHierarchyNodeChildren(nodeId);
            for (HierarchyNodeRecord child : hNodeRecordChildren) {
                HierarchyNode hierarchyNode = buildHierarchyNode(child, hDef, false);
                children.add(hierarchyNode);
            }
            hNode.setChildren(children);
        }
        return hNode;
   } 
   
   /**  Build HierarchySearchCriteria instance for the given HierarchySearch instance.
    * 
    * @param hNodeSearch HierarchySearch instance.
    * @return HierarchyNode HierarchySearchCriteria.
    * @throws ServiceException if an error occurs during processing.
    */ 
   public static HierarchySearchCriteria buildHierarchySearchCriteria(DomainSearch domainSearch, HierarchySearch hNodeSearch) 
          throws ServiceException{
        
        if (hNodeSearch == null) {
            throw new ServiceException(localizer.t("QRY504: The hNodeSearch parameter cannot be null"));
        }            
        HierarchySearchCriteria hSearchCriteria = new HierarchySearchCriteria();
        HierarchyNode hNode = new HierarchyNode();
        
        try {
            MDConfigManager configManager =  MDConfigManager.getInstance(); 
            SimpleDateFormat dateFormat = new SimpleDateFormat(configManager.getDateFormatForMultiDomain());        
  
            if (hNodeSearch.getStartDate() != null) {
                hNode.setEffectiveFromDate(dateFormat.parse(hNodeSearch.getStartDate()));
            }
            if (hNodeSearch.getEndDate() != null) {
                hNode.setEffectiveToDate(dateFormat.parse(hNodeSearch.getEndDate()));
            }
            if (hNodeSearch.getPurgeDate() != null) {
                hNode.setPurgeDate(dateFormat.parse(hNodeSearch.getPurgeDate()));   
            }
        
            hSearchCriteria.setHierarchyNode(hNode);
    
            while (hNodeSearch.hasNext()) {
                Attribute field = hNodeSearch.next();
                com.sun.mdm.multidomain.attributes.Attribute attribute = new com.sun.mdm.multidomain.attributes.Attribute();
                attribute.setName(field.getName());
                hNode.setAttributeValue(attribute, field.getValue());
            }
            EOSearchCriteria eSearchCriteria = buildEOSearchCriteria(domainSearch);
            hSearchCriteria.setEOSearchCriteria(eSearchCriteria);
                   
        } catch (ConfigException cex) {
            throw new ServiceException(cex);            
        } catch(ParseException pex) {
            throw new ServiceException(pex);
        }
        
        return hSearchCriteria;
    }
}
