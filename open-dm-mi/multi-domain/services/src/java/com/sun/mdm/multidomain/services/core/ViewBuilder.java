/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.multidomain.services.core;

import java.util.Map;
import java.util.List;
import java.util.Set;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.ResourceBundle; 
import java.util.Locale;

import java.text.SimpleDateFormat;

import com.sun.mdm.index.objects.ObjectNode;
import com.sun.mdm.index.objects.exception.ObjectException;

import com.sun.mdm.multidomain.services.configuration.MDConfigManager;

import com.sun.mdm.multidomain.relationship.MultiObject;
import com.sun.mdm.multidomain.query.PageIterator;
import com.sun.mdm.multidomain.hierarchy.HierarchyDef;
import com.sun.mdm.multidomain.hierarchy.HierarchyNode;
import com.sun.mdm.multidomain.relationship.Relationship;
import com.sun.mdm.multidomain.relationship.MultiObject.RelationshipObject;
import com.sun.mdm.multidomain.relationship.RelationshipDef;
import com.sun.mdm.multidomain.attributes.Attribute;
import com.sun.mdm.multidomain.attributes.AttributeType;
import com.sun.mdm.multidomain.relationship.MultiObject.RelationshipDomain;
        
import com.sun.mdm.multidomain.services.model.ObjectView;
import com.sun.mdm.multidomain.services.model.ObjectRecord;
import com.sun.mdm.multidomain.services.model.AttributeDefExt;
import com.sun.mdm.multidomain.services.hierarchy.HierarchyDefExt;
import com.sun.mdm.multidomain.services.hierarchy.HierarchyNodeRecord;
import com.sun.mdm.multidomain.services.hierarchy.HierarchyNodeView;
import com.sun.mdm.multidomain.services.hierarchy.HierarchyTreeView;
import com.sun.mdm.multidomain.services.relationship.RelationshipDefExt;
import com.sun.mdm.multidomain.services.relationship.RelationshipView;
import com.sun.mdm.multidomain.services.relationship.RelationshipDefView;
import com.sun.mdm.multidomain.services.relationship.RelationshipComposite;
import com.sun.mdm.multidomain.services.relationship.RelationshipRecord;
import com.sun.mdm.multidomain.services.relationship.DomainRelationshipsObject;
import com.sun.mdm.multidomain.services.util.Helper;
        
import com.sun.mdm.index.objects.epath.EPathArrayList;
import com.sun.mdm.index.objects.epath.EPathException;
import com.sun.mdm.index.objects.epath.EPath;
import com.sun.mdm.index.objects.epath.EPathAPI;
import com.sun.mdm.index.master.search.enterprise.EOSearchResultRecord;

import com.sun.mdm.index.edm.services.configuration.ValidationService;
import com.sun.mdm.multidomain.services.configuration.FieldConfig;
import com.sun.mdm.multidomain.services.configuration.SummaryLabel;
import com.sun.mdm.multidomain.services.configuration.RelationshipScreenConfig;
import com.sun.mdm.multidomain.services.configuration.SearchResultsConfig;
import com.sun.mdm.multidomain.services.configuration.SearchScreenConfig;
import com.sun.mdm.multidomain.services.configuration.DomainScreenConfig;
import com.sun.mdm.multidomain.services.configuration.FieldConfigGroup;
import com.sun.mdm.index.util.ObjectSensitivePlugIn;
import com.sun.mdm.multidomain.services.model.Column;
         
/**
 * ViewHelper class.
 * @author cye
 */
public class ViewBuilder {
    public static final String RECORD_ID_DELIMITER = " ";
    private static ResourceBundle resourceBundle = ResourceBundle.getBundle("com.sun.mdm.multidomain.services.resources.mdwm", Locale.getDefault());
    
    public static HierarchyTreeView buildHierarchyTreeView(HierarchyNode hNode, List<HierarchyNode> ancestors, List<HierarchyNode> children) 
        throws ServiceException {
        HierarchyTreeView hTreeView = new HierarchyTreeView(); 
        if (hNode != null) {
            HierarchyNodeView hNodeView = buildHierarchyNodeView(hNode);
            hTreeView.setNode(hNodeView);
        }
        if (ancestors == null || ancestors.isEmpty()) {
            hTreeView.setParent(null);
        } else {
            HierarchyNodeView hParentView = buildHierarchyNodeView(ancestors.get(0));
            hTreeView.setParent(hParentView);
            for (HierarchyNode ancestor : ancestors) {
                HierarchyNodeView hAncestorView = buildHierarchyNodeView(ancestor);
                hTreeView.addAncestor(hAncestorView);   
            }          
        }
        if (children == null || children.isEmpty()) {
            hTreeView.setChildren(null);
        } else {
            for (HierarchyNode child : children) {
                HierarchyNodeView hChildView = buildHierarchyNodeView(child);
                hTreeView.addChild(hChildView);
            } 
        }
        return hTreeView;
    }

    public static HierarchyNodeView buildHierarchyNodeView(HierarchyNode hNode) 
        throws ServiceException {
        HierarchyNodeView hNodeView = new HierarchyNodeView();
        // String id;
        // String EUID;
        // String highLight;
        // ObjectRecord objectRecord;
        try {
            hNodeView.setId(Long.toString(hNode.getNodeID()));
            hNodeView.setEUID(hNode.getEUID());
            ObjectNode objectNode = hNode.getObjectNode();
            if (objectNode != null) {
                String domain = objectNode.pGetTag();
                MDConfigManager configManager = MDConfigManager.getInstance();
                DomainScreenConfig domainConfig = configManager.getDomainScreenConfig(domain);
                SummaryLabel domainSummaryLabel = domainConfig.getSummaryLabel();
                List<FieldConfig> recordIdConfigFields = domainSummaryLabel.getFieldConfigs();
                EPathArrayList recordIdEPathFields = Helper.toEPathArrayList(recordIdConfigFields);
                String highLight = buildHighLight(domain, recordIdConfigFields, recordIdEPathFields, objectNode);
                hNodeView.sethighLight(highLight);
                ObjectRecord objRecord = buildObjectRecord(domain, hNode.getEUID(), objectNode);
                hNodeView.setObjectRecord(objRecord);
            } else {
                hNodeView.sethighLight("record not found");
            }
        } catch (ConfigException cex) {
            throw new ServiceException(cex);
        }
        return hNodeView;
    }
    
    public static HierarchyNodeRecord buildHierarchyNodeRecord(HierarchyNode hNode) 
        throws ConfigException {
        String domain = hNode.getHierarchyDef().getDomain();
        SimpleDateFormat dateFormat = new SimpleDateFormat(MDConfigManager.getInstance().getDateFormatForDomain(domain));        
        HierarchyNodeRecord hNodeRecord = new HierarchyNodeRecord();  
        
        hNodeRecord.setId(Long.toString(hNode.getNodeID()));
        hNodeRecord.setEUID(hNode.getEUID());
        hNodeRecord.setDomain(hNode.getHierarchyDef().getDomain());
        hNodeRecord.setName(hNode.getHierarchyDef().getName());
        long parentId = hNode.getParentNodeID();
        hNodeRecord.setParentId(Long.toString(parentId));
        hNodeRecord.setParentEUID(hNode.getParentEUID());        
        if (hNode.getEffectiveFromDate() != null)  {
            hNodeRecord.setStartDate(dateFormat.format(hNode.getEffectiveFromDate()));
        }
        if (hNode.getEffectiveToDate() != null) {
            hNodeRecord.setEndDate(dateFormat.format(hNode.getEffectiveToDate()));
        }
        if (hNode.getPurgeDate() != null) {
            hNodeRecord.setPurgeDate(dateFormat.format(hNode.getPurgeDate()));
        }    
        ObjectNode objectNode = hNode.getObjectNode();    
        ObjectRecord objectRecord = buildObjectRecord(hNode.getHierarchyDef().getDomain(), hNode.getEUID(), objectNode);
        hNodeRecord.setObjectRecord(objectRecord);        
        Map<Attribute, String> extAttrs = hNode.getAttributes();
        Iterator<Attribute> keys = extAttrs.keySet().iterator();
        while(keys.hasNext()) {
            Attribute key = keys.next();
            String value = extAttrs.get(key);
            hNodeRecord.setAttributeValue(key.getName(), value);
        }
        return hNodeRecord;
    }
     
    public static HierarchyDef buildHierarchyDef(HierarchyDefExt hDefExt) {
        HierarchyDef hDef = new HierarchyDef();
        hDef.setName(hDefExt.getName());
        if (hDefExt.getId() != null) {
            hDef.setId(Long.parseLong(hDefExt.getId()));
        } else {
            hDef.setId(0);
        }
        hDef.setDomain(hDefExt.getDomain());
        //TBD need to fix core HierarchyDef
        //hDef.setPluginInfo(); 
        hDef.setDescription(hDefExt.getDescription());
        hDef.setPlugin(hDefExt.getPlugin());       
        hDef.setEffectiveFrom("true".equalsIgnoreCase(hDefExt.getStartDate()));
        hDef.setEffectiveTo("true".equalsIgnoreCase(hDefExt.getEndDate()));
        hDef.setPurgeDate("true".equalsIgnoreCase(hDefExt.getPurgeDate()));        
        hDef.setEffectiveFromRequired("true".equalsIgnoreCase(hDefExt.getStartDateRequired()) ? true : false);
        hDef.setEffectiveToRequired("true".equalsIgnoreCase(hDefExt.getEndDateRequired()) ? true : false);        
        hDef.setPurgeDateRequired("true".equalsIgnoreCase(hDefExt.getPurgeDateRequired()) ? true : false);
        
        List<AttributeDefExt> attributes = hDefExt.getExtendedAttributes();
        Column column = new Column();
        for (AttributeDefExt aDefExt : attributes) {
            Attribute a = new Attribute();            
            a.setName(aDefExt.getName());
            if (aDefExt.getId() != null) { 
                a.setId(Long.parseLong(aDefExt.getId()));
            }
            a.setType(AttributeType.valueOf(aDefExt.getDataType().toUpperCase()));    
            if (aDefExt.getColumnName() != null) {
                a.setColumnName(aDefExt.getColumnName());
                column.toCount(a.getType());
            } else {
                a.setColumnName(column.toName(a.getType()));
            }
            
            a.setValue(aDefExt.getDefaultValue());
            a.setIsRequired("true".equalsIgnoreCase(aDefExt.getIsRequired()));
            a.setIsSearchable("true".equalsIgnoreCase(aDefExt.getSearchable()));
            hDef.setAttribute(a);
        }                
        return hDef;
    }
    
    public static HierarchyDefExt buildHierarchyDefExt(HierarchyDef hDef) {
        HierarchyDefExt hDefExt = new HierarchyDefExt();
        hDefExt.setName(hDef.getName());
        hDefExt.setId(Long.toString(hDef.getId()));
        hDefExt.setDomain(hDef.getDomain());
        //TBD need to fix core HierarchyDef
        //hDefExt.setPluginInfo(); 
        hDefExt.setDescription(hDef.getDescription());
        hDefExt.setPlugin(hDef.getPlugin());       
        hDefExt.setStartDate(hDef.getEffectiveFrom() ? "true" : "false");
        hDefExt.setEndDate(hDef.getEffectiveTo() ? "true" : "false");
        hDefExt.setPurgeDate(hDef.getPurgeDate() ? "true" : "false");        
        hDefExt.setStartDateRequired(hDef.getEffectiveFromRequired() ? "true" : "false");
        hDefExt.setEndDateRequired(hDef.getEffectiveToRequired() ? "true" : "false");
        hDefExt.setPurgeDateRequired(hDef.getPurgeDateRequired() ? "true" : "false");
        
        List<Attribute> attributes = hDef.getAttributes();
        for (Attribute a : attributes) {
            AttributeDefExt aExt = new AttributeDefExt();
            aExt.setName(a.getName());
            aExt.setColumnName(a.getColumnName());
            aExt.setSearchable(a.getIsSearchable() ? "true" : "false");
            aExt.setIsRequired(a.getIsRequired() ? "true" : "false");
            aExt.setDataType(a.getType().toString().toLowerCase());
            aExt.setDefaultValue(a.getValue());
            hDefExt.setExtendedAttribute(aExt);
        }                
        return hDefExt;
    }
    
    public static RelationshipDefExt buildRelationshipDefExt(RelationshipDef rDef) {
        RelationshipDefExt rDefExt = new RelationshipDefExt();
        rDefExt.setName(rDef.getName());
        rDefExt.setId(Long.toString(rDef.getId()));
        rDefExt.setSourceDomain(rDef.getSourceDomain());
        rDefExt.setTargetDomain(rDef.getTargetDomain());        
        rDefExt.setBiDirection(rDef.getDirection() == RelationshipDef.DirectionMode.BIDIRECTIONAL ? "true" : "false");
        //TBD need to fix core RelationshipDef
        //rDefExt.setPluginInfo();      
        rDefExt.setDescription(rDef.getDescription());
        rDefExt.setPlugin(rDef.getPlugin());       
        rDefExt.setStartDate(rDef.getEffectiveFrom() ? "true" : "false");
        rDefExt.setEndDate(rDef.getEffectiveTo() ? "true" : "false");
        rDefExt.setPurgeDate(rDef.getPurgeDate() ? "true" : "false"); 
        rDefExt.setStartDateRequired(rDef.getEffectiveFromRequired() ? "true" : "false");
        rDefExt.setEndDateRequired(rDef.getEffectiveToRequired() ? "true" : "false");
        rDefExt.setPurgeDateRequired(rDef.getPurgeDateRequired() ? "true" : "false");
        
        List<Attribute> attributes = rDef.getAttributes();
        for (Attribute a : attributes) {
            AttributeDefExt aExt = new AttributeDefExt();
            aExt.setName(a.getName());
            aExt.setColumnName(a.getColumnName());
            aExt.setSearchable(a.getIsSearchable() ? "true" : "false");
            aExt.setIsRequired(a.getIsRequired() ? "true" : "false");
            aExt.setDataType(a.getType().toString().toLowerCase());
            aExt.setDefaultValue(a.getValue());
            rDefExt.setExtendedAttribute(aExt);
        }        
        return rDefExt;
    }
    
    public static RelationshipDef buildRelationshipDef(RelationshipDefExt rDefExt) {
        RelationshipDef rDef = new RelationshipDef();        
        rDef.setName(rDefExt.getName());
        if(rDefExt.getId() != null) {
            rDef.setId(Long.parseLong(rDefExt.getId()));
        }
        rDef.setSourceDomain(rDefExt.getSourceDomain());
        rDef.setTargetDomain(rDefExt.getTargetDomain());        
        rDef.setDirection("true".equalsIgnoreCase(rDefExt.getBiDirection()) ? 
                          RelationshipDef.DirectionMode.BIDIRECTIONAL : RelationshipDef.DirectionMode.UNIDIRECTIONAL);
        //TBD need to fix core RelationshipDef
        //rDef.setPluginInfo();   
        rDef.setDescription(rDefExt.getDescription());
        rDef.setPlugin(rDefExt.getPlugin());
        rDef.setEffectiveFrom("true".equalsIgnoreCase(rDefExt.getStartDate()) ? true : false);
        rDef.setEffectiveTo("true".equalsIgnoreCase(rDefExt.getEndDate()) ? true : false);
        rDef.setPurgeDate("true".equalsIgnoreCase(rDefExt.getPurgeDate()) ? true : false);
        rDef.setEffectiveFromRequired("true".equalsIgnoreCase(rDefExt.getStartDateRequired()) ? true : false);
        rDef.setEffectiveToRequired("true".equalsIgnoreCase(rDefExt.getEndDateRequired()) ? true : false);        
        rDef.setPurgeDateRequired("true".equalsIgnoreCase(rDefExt.getPurgeDateRequired()) ? true : false);
        
        List<AttributeDefExt> attributes = rDefExt.getExtendedAttributes();
        Column column = new Column();
        for (AttributeDefExt aDefExt : attributes) {
            Attribute a = new Attribute();            
            a.setName(aDefExt.getName());
            if (aDefExt.getId() != null) {
                a.setId(Long.parseLong(aDefExt.getId()));
            }
             a.setType(AttributeType.valueOf(aDefExt.getDataType().toUpperCase()));
             
            if (aDefExt.getColumnName() != null) {
                a.setColumnName(aDefExt.getColumnName());
                column.toCount(a.getType());
            } else {
                a.setColumnName(column.toName(a.getType()));
            }
            a.setValue(aDefExt.getDefaultValue());
            a.setIsRequired("true".equalsIgnoreCase(aDefExt.getIsRequired()));
            a.setIsSearchable("true".equalsIgnoreCase(aDefExt.getSearchable()));
            rDef.setAttribute(a);
        }        
        return rDef;        
    }
            
    public static DomainRelationshipsObject buildRelationshipView(PageIterator<MultiObject> pages, String primaryDomain) throws ConfigException {
        MDConfigManager configManager =  MDConfigManager.getInstance();        
        DomainRelationshipsObject domainRelationshipsObject  = new DomainRelationshipsObject();
        domainRelationshipsObject.setDomain(primaryDomain);                
    
        if (pages.hasNext()) {
            MultiObject multiObject = pages.first();
            ObjectNode sourceObject = multiObject.getSourceObject();
            DomainScreenConfig primaryDomainConfig = configManager.getDomainScreenConfig(primaryDomain);
            SummaryLabel primaryDomainSummaryLabel = primaryDomainConfig.getSummaryLabel();
            List<FieldConfig> primaryRecordIdConfigFields = primaryDomainSummaryLabel.getFieldConfigs();
            EPathArrayList primaryRecordIdEPathFields = Helper.toEPathArrayList(primaryRecordIdConfigFields);
            String primaryHighLight = buildHighLight(primaryDomain, primaryRecordIdConfigFields, primaryRecordIdEPathFields, sourceObject);
            ObjectView primaryObject = new ObjectView();
            primaryObject.setName(sourceObject.pGetTag());
            primaryObject.setHighLight(primaryHighLight);
            primaryObject.setEUID(multiObject.getSourceEuid());
            domainRelationshipsObject.setPrimaryObject(primaryObject);
      
            List<RelationshipDomain> relationshipDomains = multiObject.getRelationshipDomains();
            List<RelationshipObject> relationshipObjects = relationshipDomains.get(0).getRelationshipObjects();
            String targetDomain = relationshipDomains.get(0).getDomain();

            DomainScreenConfig targetDomainConfig = configManager.getDomainScreenConfig(targetDomain);
            SummaryLabel targetDomainSummaryLabel = targetDomainConfig.getSummaryLabel();
            List<FieldConfig> targetRecordIdConfigFields = targetDomainSummaryLabel.getFieldConfigs();
            EPathArrayList targetRecordIdEPathFields = Helper.toEPathArrayList(targetRecordIdConfigFields);

            for (RelationshipObject relationshipObject : relationshipObjects) {
                ObjectNode targetObject = relationshipObject.getTargetObject();
                Relationship relationship = relationshipObject.getRelationship();
                String targetHighLight = buildHighLight(targetDomain, targetRecordIdConfigFields, targetRecordIdEPathFields, targetObject);
                RelationshipDefView relationshipDef = buildRelationshipDefView(relationship);
                RelationshipView relationshipView = null;
                if (primaryDomain.equals(relationshipDef.getSourceDomain())) {
                    relationshipView = buildRelationshipView(relationship, primaryHighLight, targetHighLight);
                } else {
                    relationshipView = buildRelationshipView(relationship, targetHighLight, primaryHighLight);
                }
                domainRelationshipsObject.addRelationshipView(relationshipDef, relationshipView);
            }
            
        }
        return domainRelationshipsObject;
    }

    public static List<RelationshipView> buildRelationshipView(PageIterator<MultiObject> pages, String sourceDomain, String targetDomain, String relationshipName) 
        throws ConfigException {
        
        MDConfigManager configManager =  MDConfigManager.getInstance();
        DomainScreenConfig sourceDomainConfig = configManager.getDomainScreenConfig(sourceDomain);
        DomainScreenConfig targetDomainConfig = configManager.getDomainScreenConfig(targetDomain); 
        SummaryLabel sourceDomainSummaryLabel = sourceDomainConfig.getSummaryLabel();
        SummaryLabel targetDomainSummaryLabel = targetDomainConfig.getSummaryLabel();
         
        List<FieldConfig> sourceRecordIdConfigFields = sourceDomainSummaryLabel.getFieldConfigs();
        EPathArrayList sourceRecordIdEPathFields = Helper.toEPathArrayList(sourceRecordIdConfigFields);
   
        List<FieldConfig> targetRecordIdConfigFields = targetDomainSummaryLabel.getFieldConfigs();
        EPathArrayList targetRecordIdEPathFields = Helper.toEPathArrayList(targetRecordIdConfigFields);
            
        List<RelationshipView> relationships = new ArrayList<RelationshipView>();
        // base on Record-Id configuration
        while(pages.hasNext()) {
            MultiObject multiObject =  pages.next();
            ObjectNode sourceObject = multiObject.getSourceObject();               
            String sourceHighLight = buildHighLight(sourceDomain, sourceRecordIdConfigFields, sourceRecordIdEPathFields, sourceObject);
           
            //TBD: should go through each one
            List<RelationshipDomain>  relationshipDomains = multiObject.getRelationshipDomains();
            //TBD need to revist because of back-end changes
            List<RelationshipObject> relationshipObjects = relationshipDomains.get(0).getRelationshipObjects();
        
            ObjectNode targetObject =  relationshipObjects.get(0).getTargetObject();
            String targetHighLight = buildHighLight(targetDomain, targetRecordIdConfigFields, targetRecordIdEPathFields, targetObject); 
               
            Relationship relationship = relationshipObjects.get(0).getRelationship();     
            RelationshipView relationshipView =  buildRelationshipView(relationship, sourceHighLight, targetHighLight);
            relationships.add(relationshipView);
        }        
        return relationships;
    }
    
    public static String buildHighLight(String domain, ObjectNode objectNode)
        throws ConfigException {
        MDConfigManager configManager =  MDConfigManager.getInstance();
        DomainScreenConfig domainConfig = configManager.getDomainScreenConfig(domain);
        SummaryLabel domainSummaryLabel = domainConfig.getSummaryLabel();
        List<FieldConfig> recordIdConfigFields = domainSummaryLabel.getFieldConfigs();
        EPathArrayList recordIdEPathFields = Helper.toEPathArrayList(recordIdConfigFields);
        String highLight = buildHighLight(domain, recordIdConfigFields, recordIdEPathFields, objectNode);
        return highLight;    
    }
        
    public static String buildHighLight(String domain, List<FieldConfig> recordIdConfigFields, EPathArrayList recordIdEPathFields, ObjectNode objectNode) 
        throws ConfigException {
        MDConfigManager configManager =  MDConfigManager.getInstance();                
        String highLight = "";       
        SimpleDateFormat dateFormat = new SimpleDateFormat(configManager.getDateFormatForDomain(domain));
        boolean hasSensitiveData = false;
        boolean getSensitiveField = true;
        try {
            hasSensitiveData = configManager.getObjectSensitivePlugIn() != null ? 
                               configManager.getObjectSensitivePlugIn().isDataSensitive(objectNode) : true;
        } catch (Exception ignore) {
        }
        try {
            for(int i = 0; i < recordIdEPathFields.size(); i++) {
                EPath ePath = recordIdEPathFields.get(i);
                FieldConfig fieldConfig  = recordIdConfigFields.get(i);   
                Object value = null;
                value = EPathAPI.getFieldValue(ePath, objectNode);                        
                if(value instanceof java.util.Date) {
                    String dateField = dateFormat.format(value);               
                    if (value != null && hasSensitiveData && fieldConfig.isSensitive() && !getSensitiveField) { 
                        highLight = highLight + RECORD_ID_DELIMITER + resourceBundle.getString("SENSITIVE_FIELD_MASKING");
                    } else {
                        highLight = highLight + RECORD_ID_DELIMITER + dateField;
                    }                               
                } else {
                    if (value != null && hasSensitiveData && fieldConfig.isSensitive() && !getSensitiveField) { 
                        highLight = highLight + RECORD_ID_DELIMITER + resourceBundle.getString("SENSITIVE_FIELD_MASKING");
                    } else {
                        if ((fieldConfig.getValueList() != null && fieldConfig.getValueList().length() > 0) && value != null) {
                            value = ValidationService.getInstance().getDescription(fieldConfig.getValueList(), value.toString());
                            highLight = highLight + RECORD_ID_DELIMITER + value;             
                        } else if ((fieldConfig.getInputMask() != null && fieldConfig.getInputMask().length() > 0) && value != null) {                                    
                            value = fieldConfig.mask(value.toString());
                            highLight = highLight + RECORD_ID_DELIMITER + value;
                        } else if ((fieldConfig.getUserCode() != null && fieldConfig.getUserCode().length() > 0) && value != null) {                               
                            value = ValidationService.getInstance().getUserCodeDescription(fieldConfig.getUserCode(), value.toString());
                            highLight = highLight + RECORD_ID_DELIMITER + value;                                
                        } else {
                            highLight = highLight + RECORD_ID_DELIMITER + value;
                        }
                    }
                }     
            }
        } catch(EPathException eex) { 
            throw new ConfigException(eex);
        } catch(ObjectException oex) {
            throw new ConfigException(oex);
        }
        return highLight;
    }

    public static RelationshipDefView buildRelationshipDefView(Relationship rel) {   
        RelationshipDefView relationshipDefView = new RelationshipDefView();        
        RelationshipDef type = rel.getRelationshipDef();
        relationshipDefView.setName(type.getName());
        relationshipDefView.setId(Long.toString(type.getId()));
        relationshipDefView.setSourceDomain(type.getSourceDomain());
        relationshipDefView.setTargetDomain(type.getTargetDomain());
        relationshipDefView.setBiDirection(type.getDirection()== RelationshipDef.DirectionMode.BIDIRECTIONAL? true : false);        
        return relationshipDefView;
    }
    
    public static RelationshipDefView buildRelationshipDefView(RelationshipDef relDef) {   
        RelationshipDefView relationshipDefView = new RelationshipDefView();        
        relationshipDefView.setName(relDef.getName());
        relationshipDefView.setId(Long.toString(relDef.getId()));
        relationshipDefView.setSourceDomain(relDef.getSourceDomain());
        relationshipDefView.setTargetDomain(relDef.getTargetDomain());
        relationshipDefView.setBiDirection(relDef.getDirection()== RelationshipDef.DirectionMode.BIDIRECTIONAL? true : false);        
        return relationshipDefView;
    }
    
    public static RelationshipView buildRelationshipView(Relationship relationship, String sourceHighLight, String targetHighLight) {   
        RelationshipView relationshipView = new RelationshipView();        
        relationshipView.setId(Long.toString(relationship.getRelationshipId()));
        relationshipView.setName(relationship.getRelationshipDef().getName());
        relationshipView.setSourceDomain(relationship.getRelationshipDef().getSourceDomain());
        relationshipView.setTargetDomain(relationship.getRelationshipDef().getTargetDomain());
        relationshipView.setSourceEUID(relationship.getSourceEUID());
        relationshipView.setTargetEUID(relationship.getTargetEUID());
        relationshipView.setSourceHighLight(sourceHighLight);
        relationshipView.setTargetHighLight(targetHighLight);           
        return relationshipView;
    }
            
    public static RelationshipComposite buildRelationshipComposite(MultiObject multiObject) 
        throws ConfigException {
        RelationshipComposite relationshipComposite = new RelationshipComposite();
        try {
            MDConfigManager configManager = MDConfigManager.getInstance();

            ObjectNode sourceObject = multiObject.getSourceObject();
            //TBD: should go through each one
            List<RelationshipDomain> relationshipDomains = multiObject.getRelationshipDomains();
            //TBD need to revist because of back-end changes
            List<RelationshipObject> relationshipObjects = relationshipDomains.get(0).getRelationshipObjects();

            ObjectNode targetObject = relationshipObjects.get(0).getTargetObject();
            Relationship relationship = relationshipObjects.get(0).getRelationship();

            DomainScreenConfig domainConfig = configManager.getDomainScreenConfig(sourceObject.pGetTag());
            List<FieldConfig> fieldConfigs = domainConfig.getSearchResultDetailsFieldConfigs();
            EPathArrayList fieldEPaths = new EPathArrayList();
            for (FieldConfig fieldConfig : fieldConfigs) {
                fieldEPaths.add(fieldConfig.getName());
            }
            ObjectRecord sourceRecord = buildObjectRecord(sourceObject.pGetTag(), sourceObject, fieldConfigs, fieldEPaths);
            sourceRecord.setEUID(relationship.getSourceEUID());
            relationshipComposite.setSourceRecord(sourceRecord);

            domainConfig = configManager.getDomainScreenConfig(targetObject.pGetTag());
            fieldConfigs = domainConfig.getSearchResultDetailsFieldConfigs();
            fieldEPaths = new EPathArrayList();
            for (FieldConfig fieldConfig : fieldConfigs) {
                fieldEPaths.add(fieldConfig.getName());
            }
            ObjectRecord targetRecord = buildObjectRecord(targetObject.pGetTag(), targetObject, fieldConfigs, fieldEPaths);
            targetRecord.setEUID(relationship.getTargetEUID());
            relationshipComposite.setTargetRecord(targetRecord);

            RelationshipRecord relationshipRecord = buildRelationshipRecord(relationship);
            relationshipComposite.setRelationshipRecord(relationshipRecord);
        } catch (EPathException eex) {
            throw new ConfigException(eex);
        }    
        return relationshipComposite;
    }

    public static List<ObjectView> buildObjectViews(String domain, PageIterator<EOSearchResultRecord> pages)
        throws ConfigException {
        MDConfigManager configManager =  MDConfigManager.getInstance();         
        List<ObjectView> records = new ArrayList<ObjectView>();
        DomainScreenConfig domainConfig = configManager.getDomainScreenConfig(domain);
        SummaryLabel domainSummaryLabel = domainConfig.getSummaryLabel();
        List<FieldConfig> recordIdConfigFields = domainSummaryLabel.getFieldConfigs();
        EPathArrayList recordIdEPathFields = Helper.toEPathArrayList(recordIdConfigFields);
        
        while(pages.hasNext()) {
            EOSearchResultRecord pageRecord = pages.next(); 
            ObjectNode objectNode = pageRecord.getObject();
            ArrayList<ObjectNode> objectNodes = objectNode.pGetChildren(domain);
            if (!objectNodes.isEmpty()) { 
                String highLight = buildHighLight(domain, recordIdConfigFields, recordIdEPathFields, objectNodes.get(0));
                ObjectView record = new ObjectView();
                record.setName(domain);
                record.setHighLight(highLight);
                record.setEUID(pageRecord.getEUID());
                records.add(record);
            }
        }
        return records;
    }
    
    public static ObjectRecord buildObjectRecord(String domain, ObjectNode objectNode, List<FieldConfig> fieldConfigs, EPathArrayList fieldEPaths) 
        throws ConfigException {
        
        MDConfigManager configManager =  MDConfigManager.getInstance(); 
        SimpleDateFormat dateFormat = new SimpleDateFormat(configManager.getDateFormatForDomain(domain));
        ObjectSensitivePlugIn plugin = configManager.getObjectSensitivePlugIn();
        boolean getSensitiveField = true;
        boolean isSensitiveData = plugin != null ? plugin.equals(objectNode) : true;
        
        ObjectRecord record = new ObjectRecord();    
        for (int i = 0; i < fieldEPaths.size(); i++) {
            FieldConfig fieldConfig  = fieldConfigs.get(i);
            EPath ePath = fieldEPaths.get(i);
            try {
                Object objectValue = EPathAPI.getFieldValue(ePath, objectNode);
                if (objectValue != null) {
                    if (objectValue instanceof java.util.Date) {
                        String dateField = dateFormat.format(objectValue);
                        if (isSensitiveData && fieldConfig.isSensitive() && !getSensitiveField) {
                            record.setAttributeValue(fieldConfig.getName(), resourceBundle.getString("SENSITIVE_FIELD_MASKING"));
                        } else {
                            record.setAttributeValue(fieldConfig.getName(), dateField);
                        }
                    } else {
                        if (isSensitiveData && fieldConfig.isSensitive() && !getSensitiveField) {
                            record.setAttributeValue(fieldConfig.getName(), resourceBundle.getString("SENSITIVE_FIELD_MASKING"));
                        } else {
                            if ((fieldConfig.getValueList() != null && fieldConfig.getValueList().length() > 0)) {
                                //value for the fields with VALUE LIST
                                // fix me String stringValue = ValidationService.getInstance().getDescription(fieldConfig.getValueList(), objectValue.toString());
                                String stringValue = objectValue.toString();
                                record.setAttributeValue(fieldConfig.getName(), stringValue);
                            } else if (fieldConfig.getInputMask() != null && fieldConfig.getInputMask().length() > 0) {
                                //Mask the field values accordingly
                                String stringValue = fieldConfig.mask(objectValue.toString());
                                record.setAttributeValue(fieldConfig.getName(), stringValue);
                            } else if (fieldConfig.getUserCode() != null && fieldConfig.getUserCode().length() > 0) {
                                //Get the value if the user code is present for the fields
                                // fix me String stringValue = ValidationService.getInstance().getUserCodeDescription(fieldConfig.getUserCode(), objectValue.toString());
                                String stringValue = objectValue.toString();
                                record.setAttributeValue(fieldConfig.getName(), stringValue);
                            } else {
                                record.setAttributeValue(fieldConfig.getName(), objectValue.toString());
                            }
                        }
                    }
                }
              } catch (Exception npe) {
                    npe.printStackTrace();
              }                
        }
        record.setName(domain);
        return record;
    }
            
    public static List<ObjectRecord> buildObjectRecords(String domain, PageIterator<EOSearchResultRecord> pages, boolean isWeighted)
        throws ConfigException {
        MDConfigManager configManager =  MDConfigManager.getInstance(); 
        boolean getSensitiveField = true;
        List<ObjectRecord> records = new ArrayList<ObjectRecord>();
        
        SimpleDateFormat dateFormat = new SimpleDateFormat(configManager.getDateFormatForDomain(domain));
        DomainScreenConfig domainConfig = configManager.getDomainScreenConfig(domain);
        List<SearchResultsConfig> searchResultsConfigs = domainConfig.getSearchResultsConfigs();
          
        EPathArrayList searchResultsFieldEPaths = Helper.toSearchResultsFieldEPaths(searchResultsConfigs);
        List<FieldConfig> searchResultsConfigFields = Helper.toSearchResultsFieldConfigs(searchResultsConfigs);
        
        while(pages.hasNext()) {
            EOSearchResultRecord  pageRecord = pages.next();
            ObjectNode objectNode = pageRecord.getObject();
            ObjectSensitivePlugIn plugin = configManager.getObjectSensitivePlugIn();
            boolean isSensitiveData = plugin != null ? plugin.equals(objectNode) : true;
            ObjectRecord record = new ObjectRecord();
            //Set the comparision score
            if (isWeighted) {
                //TBD: EOSearchResultRecord.getComparisonScore()
                //     EOSearchResultRecord.getEUID();
                //     EOSearchResultRecord.getObjectNode(); 
                record.setAttributeValue("Weight", "0");
            }         
            for (int i = 0; i < searchResultsFieldEPaths.size(); i++) {
                FieldConfig fieldConfig  = searchResultsConfigFields.get(i);
                EPath ePath = searchResultsFieldEPaths.get(i);
                try {
                    Object objectValue = EPathAPI.getFieldValue(ePath, objectNode);
                    if (objectValue != null) {
                        if(objectValue instanceof java.util.Date) {
                            String dateField = dateFormat.format(objectValue);          
                            if (isSensitiveData && fieldConfig.isSensitive() && !getSensitiveField) { 
                                record.setAttributeValue(fieldConfig.getName(), resourceBundle.getString("SENSITIVE_FIELD_MASKING"));
                            } else {
                                record.setAttributeValue(fieldConfig.getName(), dateField);
                            }        
                        } else {
                            if (isSensitiveData && fieldConfig.isSensitive() && !getSensitiveField) { 
                                record.setAttributeValue(fieldConfig.getName(), resourceBundle.getString("SENSITIVE_FIELD_MASKING"));
                            } else {
                                if ((fieldConfig.getValueList() != null && fieldConfig.getValueList().length() > 0)) {
                                    //value for the fields with VALUE LIST
                                    // fix me String stringValue = ValidationService.getInstance().getDescription(fieldConfig.getValueList(), objectValue.toString());
                                    String stringValue = objectValue.toString();
                                    record.setAttributeValue(fieldConfig.getName(), stringValue);
                                } else if (fieldConfig.getInputMask() != null && fieldConfig.getInputMask().length() > 0) {
                                    //Mask the field values accordingly
                                    String stringValue = fieldConfig.mask(objectValue.toString());
                                    record.setAttributeValue(fieldConfig.getName(), stringValue);
                                } else if (fieldConfig.getUserCode() != null && fieldConfig.getUserCode().length() > 0) {
                                    //Get the value if the user code is present for the fields
                                    // fix me String stringValue = ValidationService.getInstance().getUserCodeDescription(fieldConfig.getUserCode(), objectValue.toString());
                                    String stringValue = objectValue.toString();
                                    record.setAttributeValue(fieldConfig.getName(), stringValue);             
                                } else {
                                    record.setAttributeValue(fieldConfig.getName(), objectValue.toString());
                                }
                            }                                         
                        }
                      }
                   } catch (Exception npe) {
		          npe.printStackTrace();
                   }                
            }
            record.setName(domain);
            record.setEUID(pageRecord.getEUID()); 
            records.add(record);
        }
        return records;
    } 
    
    public static RelationshipRecord buildRelationshipRecord(Relationship relationship) 
        throws ConfigException {
        
        SimpleDateFormat dateFormat = new SimpleDateFormat(MDConfigManager.getInstance().getDateFormatForMultiDomain());    
        
        RelationshipRecord relationshipRecord = new RelationshipRecord();        
        RelationshipDef type = (RelationshipDef)relationship.getRelationshipDef();
        relationshipRecord.setId(Long.toString(relationship.getRelationshipId()));
        relationshipRecord.setSourceEUID(relationship.getSourceEUID());
        relationshipRecord.setTargetEUID(relationship.getTargetEUID());
        relationshipRecord.setName(type.getName());
        if(relationship.getEffectiveFromDate()!= null) {
            relationshipRecord.setStartDate(dateFormat.format(relationship.getEffectiveFromDate()));
        }
        if(relationship.getEffectiveToDate()!= null) {
            relationshipRecord.setEndDate(dateFormat.format(relationship.getEffectiveToDate()));
        }
        if(relationship.getPurgeDate()!= null) {
            relationshipRecord.setPurgeDate(dateFormat.format(relationship.getPurgeDate()));
        }         
        
        Map<com.sun.mdm.multidomain.attributes.Attribute, String> attributes = relationship.getAttributes();
        Iterator<com.sun.mdm.multidomain.attributes.Attribute> keys = attributes.keySet().iterator();
        while (keys.hasNext()) {
            com.sun.mdm.multidomain.attributes.Attribute key = keys.next();
            String value = attributes.get(key);
            relationshipRecord.setAttributeValue(key.getName(), value);
        }
        
        return relationshipRecord;
    }
    
    public static ObjectRecord buildObjectRecord(String domain, String EUID, ObjectNode objectNode) 
        throws ConfigException {
        MDConfigManager configManager =  MDConfigManager.getInstance();        
        SimpleDateFormat dateFormat = new SimpleDateFormat(configManager.getDateFormatForDomain(domain));
        boolean hasSensitiveData = false;
        boolean getSensitiveField = true;
        try {
            hasSensitiveData = configManager.getObjectSensitivePlugIn() != null ? 
                               configManager.getObjectSensitivePlugIn().isDataSensitive(objectNode) : true;
        } catch (Exception ignore) {
        }
        DomainScreenConfig domainConfig = configManager.getDomainScreenConfig(domain);
        
        List<FieldConfig> searchResultsConfigFields = domainConfig.getSearchResultDetailsFieldConfigs();
        EPathArrayList searchResultsFieldEPaths = new EPathArrayList();
        try {
            for (FieldConfig fieldConfig : searchResultsConfigFields) {
                searchResultsFieldEPaths.add(fieldConfig.getName());
            } 
        } catch (EPathException eex) {
        }
        
        ObjectRecord objectRecord = new ObjectRecord();
        objectRecord.setEUID(EUID);
        objectRecord.setName(objectNode.pGetTag());
        
        for (int i = 0; i < searchResultsFieldEPaths.size(); i++) {
            FieldConfig fieldConfig  = searchResultsConfigFields.get(i);
            EPath ePath = searchResultsFieldEPaths.get(i);
            try {
                Object objectValue = EPathAPI.getFieldValue(ePath, objectNode);
                String stringValue = null;
                com.sun.mdm.multidomain.services.model.Attribute attribute = 
                            new com.sun.mdm.multidomain.services.model.Attribute(); 
                if (objectValue != null) {
                    if(objectValue instanceof java.util.Date) {
                        String dateField = dateFormat.format(objectValue);          
                        if (hasSensitiveData && fieldConfig.isSensitive() && !getSensitiveField) { 
                            attribute.setName(fieldConfig.getName());
                            attribute.setValue(resourceBundle.getString("SENSITIVE_FIELD_MASKING"));
                        } else {
                            attribute.setName(fieldConfig.getName());
                            attribute.setValue(dateField);
                        }        
                    } else {
                        if (hasSensitiveData && fieldConfig.isSensitive() && !getSensitiveField) { 
                            attribute.setName(fieldConfig.getName());
                            attribute.setValue(resourceBundle.getString("SENSITIVE_FIELD_MASKING"));
                        } else {
                            if (fieldConfig.getValueList() != null && fieldConfig.getValueList().length() > 0) {
                                //value for the fields with VALUE LIST
                                //stringValue = ValidationService.getInstance().getDescription(fieldConfig.getValueList(), objectValue.toString());
                                stringValue = (String)objectValue;
                                attribute.setName(fieldConfig.getName());
                                attribute.setValue(stringValue);
                            } else if (fieldConfig.getInputMask() != null && fieldConfig.getInputMask().length() > 0) {
                                //Mask the field values accordingly
                                stringValue = fieldConfig.mask(objectValue.toString());
                                attribute.setName(fieldConfig.getName());
                                attribute.setValue(stringValue);
                            } else if (fieldConfig.getUserCode() != null && fieldConfig.getUserCode().length() > 0) {
                                //Get the value if the user code is present for the fields
                                //stringValue = ValidationService.getInstance().getUserCodeDescription(fieldConfig.getUserCode(), objectValue.toString());
                                stringValue = (String)objectValue;
                                attribute.setName(fieldConfig.getName());
                                attribute.setValue(stringValue);             
                            } else {
                                attribute.setName(fieldConfig.getName());
                                attribute.setValue(objectValue.toString());
                            }
                        }                                         
                    }
                } else {
                    attribute.setName(fieldConfig.getName());
                    attribute.setValue(null);       
                }
                objectRecord.add(attribute);
            } catch (EPathException eex) {
                throw new ConfigException(eex);
            } catch(ObjectException oex) {
               throw new ConfigException(oex); 
            }
       }
       return objectRecord;
    }
}
