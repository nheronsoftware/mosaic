// initialize icons
var deleteButtonEnabled = new Image();
deleteButtonEnabled.src = "images/icons/delete_button.png";
var deleteButtonDisabled = new Image();
deleteButtonDisabled.src = "images/icons/delete_button_faded.png";

var selectAllButtonEnabled = new Image();
selectAllButtonEnabled.src = "images/icons/select_multiple.png";
var selectAllButtonDisabled = new Image();
selectAllButtonDisabled.src = "images/icons/select_multiple_faded.png";

var deselectAllButtonEnabled = new Image();
deselectAllButtonEnabled.src = "images/icons/deselect_multiple.png";
var deselectAllButtonDisabled = new Image();
deselectAllButtonDisabled.src = "images/icons/deselect_multiple_faded.png";   

var addButtonEnabled = new Image();
addButtonEnabled.src = "images/icons/add_button.png";
var addButtonDisabled = new Image();
addButtonDisabled.src = "images/icons/add_button_faded.png";   

var findButtonEnabled = new Image();
findButtonEnabled.src = "images/icons/find_button.png";
var findButtonDisabled = new Image();
findButtonDisabled.src = "images/icons/find_button_faded.png";   

var moveRightButtonEnabled = new Image();
moveRightButtonEnabled.src = "images/icons/move-right_button.png";
var moveRightButtonDisabled = new Image();
moveRightButtonDisabled.src = "images/icons/move-right_button_faded.png";  

var moveLeftButtonEnabled = new Image();
moveLeftButtonEnabled.src = "images/icons/move-left_button.png";
var moveLeftButtonDisabled = new Image();
moveLeftButtonDisabled.src = "images/icons/move-left_button_faded.png";  

var isByRelSelectDialogLoaded = false; // Global flag to find out if Select dialog is already loaded for ByRelationship screen
var isByRelAddDialogLoaded = false; // Global flag to find out if Add dialog is already loaded for ByRelationship screen

var isByRecordSelectDialogLoaded = false;
var isByRecordAddDialogLoaded = false;

var currentDomainByRecord = null;

function testResults(){
    
    DomainScreenHandler.getSearchResultFields("Company",testResultsBack);
}

function testResultsBack(data){
    alert(data);
    for (var fieldGrp in data)  {
        for (var fields in data[fieldGrp])  {
          alert("Search Results"+fieldGrp + " : "+data[fieldGrp][fields].name);
        }
     }
    
}

function test() {
    DomainScreenHandler.getSummaryFields("Person",testCB);
}
function testCB(data) {
    alert(data);
     for (var fieldGrp in data)  {
        for (var fields in data[fieldGrp])  {
          alert(fieldGrp + " : "+data[fieldGrp][fields].name);
        }
     }
}

// General functions for Manage Relationship Screens

// function to get HTML view of the relationship def name, along with direction icon.
function getRelationshipDefDirectionIcon (isBiDirection) {
    var strHTML = "";
    if(getBoolean (isBiDirection) ) {
        strHTML += "<img src='images/icons/relationship-both.png' hspace='3'>";
    } else {
        strHTML += "<img src='images/icons/relationship-right.png' hspace='3'>";
    }
    return strHTML;
}

// Manage screen related scripts

 function showRecordFullDetails(summaryId, detailsId, showDetailsFlag){
     if(! showDetailsFlag){
        displayDiv (detailsId, false);
        displayDiv (summaryId, true);
     }else{  
        displayDiv (detailsId, true);
        displayDiv (summaryId, false);        
     }
 }

var byRelationshipMainPage = "m_byrelationship_main.htm";
var byRecordMainPage = "m_byrecord_main.htm";
var currentView = "by_rel";
function changeViewToByRelationship(contentPaneId) {
    var contentPaneObj = dijit.byId(contentPaneId);
    _deleteChildWidgets(contentPaneId);
	var selectDialog = dijit.byId("byrecord_select");
	var addDialog = dijit.byId("byrecord_add");
	if(selectDialog != null) _deleteChildWidgets(selectDialog);
	if(addDialog != null) _deleteChildWidgets(addDialog);
    contentPaneObj.setHref (byRelationshipMainPage);
    currentView = "by_rel";    
    initializeByRelationshipScreen ();
}
function changeViewToByRecord(contentPaneId) {
    var contentPaneObj = dijit.byId(contentPaneId);
    _deleteChildWidgets(contentPaneId);
	var selectDialog = dijit.byId("byrel_select");
	var addDialog = dijit.byId("byrel_add");
	if(selectDialog != null) _deleteChildWidgets(selectDialog);
	if(addDialog != null) _deleteChildWidgets(addDialog);
    contentPaneObj.setHref (byRecordMainPage);
    currentView = "by_rec";
    initializeByRecordScreen ();
}

function onLoadManageRelationships() {
    if (currentView == "by_rec") {
        loadDomainsForRecord ();
    }
}

function initializeByRecordScreen() {  
    isByRecordSelectDialogLoaded = false;
    isByRecordAddDialogLoaded = false;
	isRearrangeTreeShown = false;
}
function initializeByRelationshipScreen() {
    currentSelectedSourceDomain = null; 
    currentSelectedTargetDomain = null; 
    currentSelectedRelationshipDef = null; 
    isByRelSelectDialogLoaded = false; 
    isByRelAddDialogLoaded = false; 
}
function _deleteChildWidgets( parentId) {
    var parentObj = dijit.byId(parentId);
    if(parentObj == null) return;
    var childWidgets = parentObj.getDescendants();
    for (i=0; i<childWidgets.length; i++) {
        childWidgets[i].destroyRecursive(false);
    }
}

function loadRelationshipDefsOverview() {
	var sourceDomain=document.getElementById("select_record_by_domain").value;
  var targetDomain=document.getElementById("select_relationship_def_overview").value;
  RelationshipDefHandler.getRelationshipDefs(sourceDomain, targetDomain, loadRelationshipDefsOverviewCB);
}

var relationshipDefsOverviewFuncs = [
  function(data) { return data.name; },
  function(data) { var desc =  data.description; 
            			 if(desc == null) return "";
            			 if(desc.length > 30) {
                			desc = firstNCharacters (desc, 30,true);
                			desc += "...";
            			 }
            			 var descHTML = "<span title='"+data.description+"'> "+ desc +"</span>"
            			 return descHTML;  
        					},
  function(data) { if(getBoolean(data.biDirection)) {return "<img src='images/icons/relationship-both.png'>"; }else {return "<img src='images/icons/relationship-right.png'>" ;}  },
  function(data) { var fixedAttributesCount = 0; 
            			 if(getBoolean(data.startDate)) fixedAttributesCount ++;
            			 if(getBoolean(data.endDate)) fixedAttributesCount ++;
            		   if(getBoolean(data.purgeDate)) fixedAttributesCount ++;
            			 var output = "";
            			 output += fixedAttributesCount + " Predefined | " + data.extendedAttributes.length + " Custom"; 
            			 return output; //return data.attributes; 
  },           
];
    
function loadRelationshipDefsOverviewCB(data) {
	dwr.util.removeAllRows("relationshipDefsOverviewList");
  if(data == null || data.length<=0) {
  	dwr.util.addRows("relationshipDefsOverviewList", [''], [function(data){return getMessageForI18N("no_relationship_definition_found");}], {
    		cellCreator:function(options) {
      		var td = document.createElement("td");
      		td.colSpan="7"; td.align="center";
      		return td;
      	},
      	escapeHtml:false
      });
    }
    
    dwr.util.addRows("relationshipDefsOverviewList", data, relationshipDefsOverviewFuncs, 
   				{ rowCreator:function(options) {
            	var row = document.createElement("tr");
              return row;
            },
            cellCreator:function(options) {
            	var td = document.createElement("td");
              if(options.cellNum==0) td.align="center";// alert(options.cellNum);
              return td;
            },
            escapeHtml:false
          });	
}

function showOverviewDialog() {
	var overviewDialog = dijit.byId("byrecord_relationship_def_overview");
	if (overviewDialog != null) {
		var sourceDomain=document.getElementById("select_record_by_domain").value;
  	var targetDomain=document.getElementById("select_relationship_def_overview").value;
  	document.getElementById("relationship_def_overview_source").innerHTML = "Source Domain: " + sourceDomain;
  	document.getElementById("relationship_def_overview_target").innerHTML = "Target Domain: " + targetDomain;
	overviewDialog.show();
		loadRelationshipDefsOverview();
	}	
}

function hideOverviewDialog() {
	var overviewDialog = dijit.byId("byrecord_relationship_def_overview");
	if (overviewDialog != null) {
		overviewDialog.hide();
	}	
}

function showSelectDialog() {
    if(currentView != null && currentView == "by_rel")
      showByRelSelectDialog ();
    else showByRecordSelectDialog();
}

function showByRelSelectDialog() {
    var selectDialog = dijit.byId("byrel_select");
    if(selectDialog != null)
      selectDialog.show();
  
    if(isByRelSelectDialogLoaded) loadDomainsForSearch();
}

function hideByRelSelectDialog () {
    dijit.byId('byrel_select').hide();
}

function showByRecordSelectDialog() {
    currentDomainByRecord = document.getElementById("select_record_by_domain").value;
    var selectDialog = dijit.byId("byrecord_select");
    if(selectDialog != null)
      selectDialog.show();
    if(isByRecordSelectDialogLoaded) initializeByRecordSelectDialog();
}
function hideByRecordSelectDialog () {
    dijit.byId('byrecord_select').hide();
}
function initializeByRecordSelectDialog() {
    isByRecordSelectDialogLoaded = true;
    byRecord_prepareSelect();
}

function showByRecordAddDialog() {
    var addDialog = dijit.byId("byrecord_add");
    if(addDialog != null)
        addDialog.show();
    if(isByRecordAddDialogLoaded) initializeByRecordAddDialog();    
}
function hideByRecordAddDialog () {
    dijit.byId('byrecord_add').hide();
}
function initializeByRecordAddDialog() {
    isByRecordAddDialogLoaded = true;
    byRecord_prepareAdd();
}

function showByRelAddDialog(){
    if(currentSelectedSourceDomain == null || currentSelectedTargetDomain==null || currentSelectedRelationshipDef==null)
        return;
    var addDialog = dijit.byId("byrel_add");
    addDialog.show();
    if(isByRelAddDialogLoaded) initializeAddDialog();
}

function initializeAddDialog() {
    isByRelAddDialogLoaded = true;
    //alert("add dialog ")
    var selectedSourceDomain = currentSelectedSourceDomain; //document.getElementById("select_sourceDomain").value;
    var selectedTargetDomain = currentSelectedTargetDomain; //document.getElementById("select_targetDomain").value;
    var selectedRelationshipDef = currentSelectedRelationshipDef; //document.getElementById("select_relationshipDefs").value;

    document.getElementById("byrel_addSourceDomain").innerHTML= selectedSourceDomain;
    document.getElementById("byrel_addTargetDomain").innerHTML= selectedTargetDomain;
    document.getElementById("relationship_add_RelationshipDefName").innerHTML= selectedRelationshipDef;
    
    RelationshipDefHandler.getRelationshipDefByName(selectedRelationshipDef, selectedSourceDomain, selectedTargetDomain, populateAddRelationshipDefAttributes);
    // Populate search type drop down 
    loadAddSearchCriteria();
    //RelationshipDefHandler.getDomainSearchCriteria(selectedSourceDomain, addSourceDomainCriteria);
    //RelationshipDefHandler.getDomainSearchCriteria(selectedTargetDomain, addTargetDomainCriteria);
}

function populateAddRelationshipDefAttributes(data){
    var startDate =(getBoolean(data.startDate));
    var endDate =(getBoolean(data.endDate));
    var purgeDate =(getBoolean(data.purgeDate));
    var CustomrowCount = 0;
    var PredefinedrowCount = 0;  
   // document.getElementById('relationship_add_customAttributes').innerHTML="";
   // document.getElementById('relationship_add_predefinedAttributes').innerHTML="";
    //dwr.util.removeAllRows("relationship_add_customAttributes");
    dwr.util.removeAllRows("relationship_add_predefinedAttributes");
       if(data.extendedAttributes.length>0 ){
           createCustomAttributesSection ("relationship_add_customAttributes", data.extendedAttributes, "add_custom", true,false);
           displayDiv ("add_Relationship_CustomAtrributes", true);
       }else{
           displayDiv ("add_Relationship_CustomAtrributes", false);
       }
       if(startDate==true || endDate==true || purgeDate == true ){ 
          createPredefinedAttributesSection ("relationship_add_predefinedAttributes", data, "add_predefined", true);
          displayDiv ("add_Relationship_PredefinedAtrributes", true);
      } else{
          displayDiv ("add_Relationship_PredefinedAtrributes", false);
      }    
}
function hideByRelAddDialog () {
    dijit.byId('byrel_add').hide();
}


// Method to refresh record listing table in "By Relationship""
function selectRecordRow (objSelRow) {
    // highlight the selected row
    var tempTBody = document.getElementById("relationshipsListing");
    for(i=0; i<tempTBody.rows.length; i++) {
        if(tempTBody.rows[i] == objSelRow) 
            objSelRow.style.backgroundColor='#EBF5FF'; // change to CSS class later
        else
            tempTBody.rows[i].style.backgroundColor='#FFFFFF'; // change to CSS class later
    }
}

// Function to create the UI for custom attributes section.
// Can be used for Add, select & edit relationship attributes screens   
function createCustomAttributesSection (tableId, attributesArray, prefixToUse, showRequiredSymbol, showByRange ) {
    //alert("createCustomAttributesSection => " + tableId);
     var editCustomAttrFuncs = [
      function(data) { return data; },
      function(data) { return data; },
      function(data) { return data; },
      function(data) { return data; }
    ];
    if(showRequiredSymbol == null) showRequiredSymbol = true;
    if(showByRange == null) showByRange = false;
    // Destroy previously created widgets.
    if(attributesArray != null && attributesArray.length > 0) {
        for(i=0; i<attributesArray.length; i++) {            
            var fName =  prefixToUse + "_" + attributesArray[i].name;
            if(dijit.byId(fName) != undefined && dijit.byId(fName) != null ) {
               dijit.byId(fName).destroy();
            }
            var fName_To =  prefixToUse + "_" + attributesArray[i].name + "_TO";
            if(dijit.byId(fName_To) != undefined && dijit.byId(fName_To) != null ) {
                dijit.byId(fName_To).destroy();
            }
        }
    }
    
    dwr.util.removeAllRows(tableId);
    if(attributesArray != null && attributesArray.length > 0) {
      dwr.util.addRows(tableId, attributesArray, editCustomAttrFuncs, {
          rowCreator:function(options) {
            var row = document.createElement("tr");
            return row;
          },
          cellCreator:function(options) {
            var td = document.createElement("td");
            var tempData = options.data;
            var byRangeDiv = null;
            var byRangeCheckbox = null;
            var labelByRange = null;
            var field_To = null;
            if(showByRange) {
                byRangeDiv = document.createElement("span");
                byRangeDiv.id = prefixToUse + "_" + tempData.name + "_TO_DIV";
                byRangeDiv.style.display = "none";
                var labelTo = document.createElement ("label");
                labelTo.id = "label_TO"; labelTo.innerHTML ="&nbsp; to ";
                byRangeDiv.appendChild (labelTo);
                field_To = document.createElement("input");
                field_To.type ="text";
                field_To.name = prefixToUse + "_" + tempData.name + "_TO";
                field_To.id = prefixToUse + "_" + tempData.name + "_TO";
                byRangeDiv.appendChild (field_To);

                byRangeCheckbox = document.createElement("input");
                byRangeCheckbox.type = "checkbox";
                byRangeCheckbox.name = "showbyRangeChk";
                byRangeCheckbox.value = byRangeDiv.id;
                byRangeCheckbox.onclick = function () {
                    displayDiv(this.value, this.checked);
                }
                labelByRange = document.createElement ("label");
                labelByRange.innerHTML = "By Range";
            }
            if(options.cellNum == 1) {
                if(tempData.dataType=="date"){
                    var datefield = document.createElement("input");
                    // This should be date field...
                    datefield.type = "text";
                    datefield.name = prefixToUse + "_" + tempData.name;
                    datefield.id = prefixToUse + "_" + tempData.name;
                    datefield.size = 5; 
                    datefield.style.width = "100px";
                    td.appendChild(datefield);
                    
                    var props = {
                          name: "custom_date_attr",
                          promptMessage: getMDWMDateFormat(),
                          invalidMessage: getMessageForI18N("invalid_date"),
                          selector: "date", 
                          datePattern: getMDWMDateFormat(),
                          //constraints: "{selector:'date', datePattern:'mm/dd/yyyy'}",
                          width:"150px"
                    }; 
                    datefield = new dijit.form.DateTextBox(props, datefield);
                }else if(tempData.dataType == "boolean"){
                    var booleanField = document.createElement("select");
                    var tOption = document.createElement ("option");
                    tOption.text = "true"; 
                    tOption.value = "true";
                    var fOption = document.createElement ("option");
                    fOption.text = "false"; 
                    fOption.value = "false";
                    booleanField.options[0] = tOption;
                    booleanField.options[1] = fOption;
                    booleanField.name = prefixToUse + "_" + tempData.name;
                    booleanField.id = prefixToUse + "_" + tempData.name;
                    td.appendChild(booleanField);
               } else {
                    var field = document.createElement("input");
                    field.type ="text";
                    field.name = prefixToUse + "_" + tempData.name;
                    field.id = prefixToUse + "_" + tempData.name;
                    td.appendChild (field);
               }
               td.className = "label";
               options.data = null;
            } else if(options.cellNum == 0) {
                if(getBoolean(tempData.isRequired) == true && showRequiredSymbol){
                   options.data = "<span class='label'>" + tempData.name + getMessageForI18N("mandatorySymbol") +  "</span>"; 
                } else {
                   options.data = "<span class='label'>" + tempData.name +  "</span>";
                }
                td.innerHTML = options.data;
            } else if(options.cellNum == 2) {
                // create by Range field.
                if(showByRange) { 
                    if(tempData.dataType=="date"){
                      field_To.type = "text";
                      field_To.name = prefixToUse + "_" + tempData.name + "_TO";
                      field_To.id = prefixToUse + "_" + tempData.name + "_TO";
                      field_To.size = 5; 
                      field_To.style.width = "100px";
                      var dateProps = {
                          name: "custom_date_attr",
                          promptMessage: getMDWMDateFormat(),
                          invalidMessage: getMessageForI18N("invalid_date"),
                          selector: "date", 
                          datePattern: getMDWMDateFormat(),
                          width:"150px"
                      }; 
                      field_To = new dijit.form.DateTextBox(dateProps, field_To);
                      td.appendChild(byRangeDiv);
                    } else if(tempData.dataType == "boolean"){
                    } else {
                      td.appendChild(byRangeDiv);  
                    }
                }
                options.data = null;
                td.className = "label";
            } else if(options.cellNum == 3) {
                if(showByRange) {                 
                  if(tempData.dataType == "boolean") {
                  } else {
                    td.appendChild(byRangeCheckbox);
                    td.appendChild(labelByRange);
                  }
                }
                options.data = null;
                td.className = "label";
            }
            else {}
            return td;
          },
          escapeHtml:false
        });
    } else {
    }
   
}

// Function to populate data in custom attributes section, used in Edit relationship attributes section.
function populateCustomAttributesValues (attributesArray, attributesValuesArray, prefixToUse) {
  //alert("POPULATING data for custom attributes");
  for(i=0; i<attributesValuesArray.length; i++) {
      //alert(attributesValuesArray[i].name + " : " + attributesValuesArray[i].value);
      var fieldObj = document.getElementById(prefixToUse + "_" + attributesValuesArray[i].name );
      if(fieldObj != null) {
          fieldObj.value = attributesValuesArray[i].value;
      }
  }
}
// Function to create Predefined attributes section
// Used in Add, Select & Edit Relationship Attributes screens
function createPredefinedAttributesSection (tableId, dataObj, prefixToUse, showRequiredSymbol) {
    
    //alert("CREATING Predefined attributes section : " + tableId);
    if(showRequiredSymbol == null) showRequiredSymbol = true;
    var startDate =(getBoolean(dataObj.startDate));
    var endDate =(getBoolean(dataObj.endDate));
    var purgeDate =(getBoolean(dataObj.purgeDate));
    var startDateRequired = (getBoolean(dataObj.startDateRequired));
    var endDateRequired = (getBoolean(dataObj.endDateRequired));
    var purgeDateRequired = (getBoolean(dataObj.purgeDateRequired));
    
    //alert(startDate + "," + endDate +", " + purgeDate);

    // Destroy the datetextbox widgets if already registered
    if(dijit.byId(prefixToUse + "_startDate") != undefined && dijit.byId(prefixToUse + "_startDate") != null ) {
      dijit.byId(prefixToUse + "_startDate").destroy();
    }
    if(dijit.byId(prefixToUse + "_endDate") != undefined && dijit.byId(prefixToUse + "_endDate") != null ) {
      dijit.byId(prefixToUse + "_endDate").destroy();
    }
    if(dijit.byId(prefixToUse + "_purgeDate") != undefined && dijit.byId(prefixToUse + "_purgeDate") != null ) {
      dijit.byId(prefixToUse + "_purgeDate").destroy();
    }    

    dwr.util.removeAllRows(tableId);
    PredefinedrowCount = 0;
    if(startDate==true&& endDate==true){ // condition for Start and End dates of Predefined Attributes
        var SecondRow = document.getElementById(tableId).insertRow(PredefinedrowCount ++);
        SecondRow.insertCell(0);
        SecondRow.insertCell(1);
        SecondRow.insertCell(2);
        SecondRow.insertCell(3);
        SecondRow.insertCell(4);
        SecondRow.cells[0].innerHTML=getMessageForI18N("effective");
        if(startDateRequired == true && showRequiredSymbol){
          SecondRow.cells[1].innerHTML=getMessageForI18N("from") + " " + getMessageForI18N("mandatorySymbol");   
        }else{
          SecondRow.cells[1].innerHTML=getMessageForI18N("from");      
        }
        var startDate_textBox = document.createElement("input");
        startDate_textBox.type = "text";
        startDate_textBox.name = prefixToUse + "_startDate";
        startDate_textBox.id = prefixToUse + "_startDate";
        startDate_textBox.size = "5";
        startDate_textBox.style.width="100px";
        SecondRow.cells[2].appendChild(startDate_textBox);
        //alert("fine till here");
        var startProps = {
            name: "start_date_textbox",
            promptMessage: getMDWMDateFormat(),
            invalidMessage: getMessageForI18N("invalid_date"),
            selector: "date", 
            datePattern: getMDWMDateFormat(),
            width:"100px"
        }
        startDate_textBox = new dijit.form.DateTextBox(startProps, startDate_textBox);
        if(endDateRequired == true && showRequiredSymbol){
          SecondRow.cells[3].innerHTML=getMessageForI18N("to") + " " + getMessageForI18N("mandatorySymbol");   
        }else{
          SecondRow.cells[3].innerHTML=getMessageForI18N("to");      
        }
        
        var endDate_textBox = document.createElement("input");
        endDate_textBox.type="text";
        endDate_textBox.name = prefixToUse + "_endDate";
        endDate_textBox.id = prefixToUse + "_endDate";
        endDate_textBox.size="5";
        endDate_textBox.style.width="100px";
        SecondRow.cells[4].appendChild(endDate_textBox);
        var endProps = {
            name: "end_date_textbox",
            promptMessage: getMDWMDateFormat(),
            invalidMessage: getMessageForI18N("invalid_date"),
            selector: "date", 
            datePattern: getMDWMDateFormat(),
            width:"100px"
        }
        endDate_textBox = new dijit.form.DateTextBox(endProps, endDate_textBox); 
    } else if (startDate == true) { // condition for Start Date of Predefined Attributes
        var SecondRowStart = document.getElementById(tableId).insertRow(PredefinedrowCount ++);
        SecondRowStart.insertCell(0);
        SecondRowStart.insertCell(1);
        SecondRowStart.insertCell(2);
        
        SecondRowStart.cells[0].innerHTML=getMessageForI18N("effective");
        if(startDateRequired == true && showRequiredSymbol){
          SecondRowStart.cells[1].innerHTML=getMessageForI18N("from") + " " + getMessageForI18N("mandatorySymbol");   
        }else{
          SecondRowStart.cells[1].innerHTML=getMessageForI18N("from");      
        }
        var start_date = document.createElement("input");
        start_date.type="text";
        start_date.name = prefixToUse + "_startDate";
        start_date.id = prefixToUse + "_startDate";
        start_date.size="5";
        start_date.style.width="100px";
        SecondRowStart.cells[2].appendChild(start_date);
        var start_date_Props = {
            name: "start_date",
            promptMessage: getMDWMDateFormat(),
            invalidMessage: getMessageForI18N("invalid_date"),
            selector: "date", 
            datePattern: getMDWMDateFormat(),
            width:"100px"
        }
        start_date = new dijit.form.DateTextBox(start_date_Props, start_date);
        
    } else if(endDate == true) { // condition for End Date of Predefined Attributes
        
        var SecondRowEnd = document.getElementById(tableId).insertRow(PredefinedrowCount ++);
        SecondRowEnd.insertCell(0);
        SecondRowEnd.insertCell(1);
        SecondRowEnd.insertCell(2);
        
        SecondRowEnd.cells[0].innerHTML=getMessageForI18N("effective");
        if(endDateRequired == true && showRequiredSymbol){
          SecondRowEnd.cells[1].innerHTML=getMessageForI18N("to") + " " + getMessageForI18N("mandatorySymbol");      
        }else{
          SecondRowEnd.cells[1].innerHTML=getMessageForI18N("to");      
        }
        var end_date = document.createElement("input");
        end_date.type="text";
        end_date.name = prefixToUse + "_endDate";
        end_date.id = prefixToUse + "_endDate";
        end_date.size="5";
        end_date.style.width="100px";
        SecondRowEnd.cells[2].appendChild(end_date);
        var end_date_Props = {
            name: "end_date",
            promptMessage: getMDWMDateFormat(),
            invalidMessage: getMessageForI18N("invalid_date"),
            selector: "date", 
            datePattern: getMDWMDateFormat(),
            width:"100px"
        }
        end_date = new dijit.form.DateTextBox(end_date_Props, end_date);
        
    }
    if(purgeDate==true ){ // condition for purge date of Predefined Attributes
        
        var ThirdRow = document.getElementById(tableId).insertRow(PredefinedrowCount ++);
        ThirdRow.insertCell(0);
        ThirdRow.insertCell(1);
        ThirdRow.insertCell(2);
        if(purgeDateRequired == true && showRequiredSymbol){
          ThirdRow.cells[0].innerHTML=getMessageForI18N("purgeDate") + " " + getMessageForI18N("mandatorySymbol");
        }else{
          ThirdRow.cells[0].innerHTML=getMessageForI18N("purgeDate");
        }
        ThirdRow.cells[1].innerHTML=" ";
        var Purge_date= document.createElement("input");
        Purge_date.type="text";
        Purge_date.name = prefixToUse + "_purgeDate";
        Purge_date.id = prefixToUse + "_purgeDate";
        Purge_date.value = "true";
        Purge_date.size="5";
        Purge_date.style.width="100px";
        ThirdRow.cells[2].appendChild(Purge_date);
        var purge_date_Props = {
            name: "purge_date",
            promptMessage: getMDWMDateFormat(),
            invalidMessage: getMessageForI18N("invalid_date"),
            selector: "date", 
            datePattern: getMDWMDateFormat(),
            width:"100px"
        }
        Purge_date = new dijit.form.DateTextBox(purge_date_Props, Purge_date);
    }
}


function populatePredefinedAttributesValues(dataObj, dataObjValues, prefixToUse) {
    if(getBoolean(dataObj.startDate)) {
      var startDateObj = document.getElementById(prefixToUse + "_startDate" );
      if(startDateObj != null) {
          startDateObj.value = dataObjValues.startDate;
      }
    }
    if(getBoolean(dataObj.endDate)) {
      var endDateObj = document.getElementById(prefixToUse + "_endDate" );
      if(endDateObj != null) {
          endDateObj.value = dataObjValues.endDate;
      }
    }
    if(getBoolean(dataObj.purgeDate)) {
      var purgeDateObj = document.getElementById(prefixToUse + "_purgeDate" );
      if(purgeDateObj!=null) {
          purgeDateObj.value = dataObjValues.purgeDate;
      }
    }    
}



function selectAllRelationships () {
    var chkboxes = document.getElementsByName("relationship_id");
    for(i=0;i<chkboxes.length; i++) {
        chkboxes[i].checked = true;
    }
    refreshRelationshipsListingButtonsPalette();
}
function deselectAllRelationships () {
    var chkboxes = document.getElementsByName("relationship_id");
    for(i=0;i<chkboxes.length; i++) {
        chkboxes[i].checked = false;
    }
    refreshRelationshipsListingButtonsPalette();
}

function refreshRelationshipsListingButtonsPalette () {
    var selectedChoices = 0;
    
    var chkboxes = document.getElementsByName("relationship_id");
    for(i=0;i<chkboxes.length; i++) {
        if(chkboxes[i].checked )
            selectedChoices ++;
    }

    var imgDeleteButtonObj = dojo.byId("imgDeleteRelationshipListing");
    if(imgDeleteButtonObj != null) {
        if(selectedChoices > 0)
            imgDeleteButtonObj.src =   deleteButtonEnabled.src;
        else
            imgDeleteButtonObj.src =   deleteButtonDisabled.src;
    }
    var imgSelectAllButtonObj = dojo.byId("imgSelectAllRelationshipListing");
    if(imgSelectAllButtonObj != null ) {
        if(selectedChoices != chkboxes.length)
            imgSelectAllButtonObj.src =   selectAllButtonEnabled.src;
        else
            imgSelectAllButtonObj.src =   selectAllButtonDisabled.src;
    }
    var imgDeSelectAllButtonObj = dojo.byId("imgDeselectAllRelationshipListing");
    if(imgDeSelectAllButtonObj != null ) {
        if(selectedChoices > 0)
            imgDeSelectAllButtonObj.src =   deselectAllButtonEnabled.src;
        else
            imgDeSelectAllButtonObj.src =   deselectAllButtonDisabled.src;
    }

    var imgAddButtonObj = dojo.byId("imgAddRelationshipListing");
    if(imgAddButtonObj != null) {
        if(currentSelectedSourceDomain != null && currentSelectedTargetDomain!=null && currentSelectedRelationshipDef!=null) {
            imgAddButtonObj.src =   addButtonEnabled.src;
        } else {
            imgAddButtonObj.src =   addButtonDisabled.src;
        }
    }
}


// *****   start hierarchy related functions ******

function onLoadManageHierarchy() {
    loadSelectHierarchyDomains();
}


// *****   end  hierarchy related functions  *******


// *****   start masking related functions  ******



var global_released=true;
var global_old_value="";
var global_mask="";
var global_alerting=false;
var global_errored=false;
var global_nchar_added=0;
var global_field=0;

function qws_field_on_key_down(field, mask) {
    global_mask=mask;
    global_field=field;
    if(mask == null || mask.length == 0) return;
    if(! global_released) {
      return;
    }
    global_released=false;
    global_old_value= field.value;
    global_alerting=false;
}

var global_count=0;

function qws_field_on_key_up(field) {
    if(field != global_field) return;
    if(global_mask == null || global_mask.length == 0) return;
    global_released=true;
    if(field.value == global_old_value) {
      return;
    }
    var oldvalue=global_old_value;
    if (field.value.length == 0 )
    {
        oldvalue=""; 
    } 
    var pos1=getNewCharPosL(field.value, oldvalue);
    // if too many chars entered before releasing key; take the first char that's different
    // from old value and place the others on queue
    var queue="";
    if(field.value.length > 1+oldvalue.length) {

        add_if_needed(field, global_mask)==true;
                if(field.value.length>global_mask.length) {
          field.value=field.value.substring(0, global_mask.length); // enforce the max length
                }
        return;
    }

    global_errored=false;
    global_count=0;
    global_nchar_added=0;
    apply_mask(field, oldvalue);


}


function apply_mask(field, oldvalue) {
    var etype=getEventType(field, oldvalue);
    var mask=global_mask;
    var pos1=getNewCharPosL(field.value, oldvalue);
    var pos2=getNewCharPosR(field.value, oldvalue);
    var cur_char=field.value.charAt(pos1);
    var cur_type=mask.charAt(pos1);
    var next_type=mask.charAt(pos1+1);
    switch(etype) {
      case 0:  // delete at end
      case 1:  // delete at other locations
        if(pos1+pos2< field.value.length) { // added 1 char after deleting selected chars
          if(isMatch(cur_char, cur_type)) {
            break;
          } else {
            global_alerting=true;
            //alert(describe(cur_type)+" required"); //Commented on 11/20/2007
            global_errored=true;
            global_alerting=false;
            field.value=oldvalue;
            break;
          }
        }
        break;
      case 2:  // replace 1 char
        if(isMatch(cur_char, cur_type)) { // new char is valid
          break;
        } else {
          global_alerting=true;
          //alert(describe(cur_type)+" required");
          global_errored=true;
          global_alerting=false;
          field.value=oldvalue;
          break;
        }
        break;
      case 3:  // append 1 char
        if(field.value.length>mask.length) {
          field.value=field.value.substring(0, mask.length); // enforce the max length
          break;
        }
        if(isMatch(cur_char, cur_type)) { // new char is valid
          if(field.value.length<mask.length && isLiteral(next_type)) {
            field.value=field.value + next_type;  // add the next literal
            global_nchar_added++;
            global_count++;
          }
          break;
        } else { // new char invalid
          if(isLiteral(cur_type)) { // if literal missing, insert it and check again
            field.value = oldvalue + cur_type;
            var oldvalue= field.value;
            field.value += cur_char;
            global_nchar_added++;
            apply_mask(field, oldvalue);
            break;
          } else {
            global_alerting=true;
            //alert(describe(cur_type)+" required");
            global_errored=true;
            global_alerting=false;
            field.value=oldvalue;
            break;
          }
        }
        break;
      case 4:  // insert 1 char in front
      case 5:  // insert 1 char at other locations
      default:
        var head=oldvalue.substring(0, pos1);
        var tail=oldvalue.substring(pos1+1);
        if(isMatch(cur_char, cur_type)) {
          if(field.value.length>mask.length) {
            field.value = head + cur_char + tail;
          }
          break;
        } else {
            global_alerting=true;
            //alert(describe(cur_type)+" required");
            global_errored=true;
            global_alerting=false;
            field.value=oldvalue;
            break;
        }
        break;
    }
    return;
}

// return the # of chars from the left that are the same
function getNewCharPosL(value, oldvalue) {
    var i=0;
    for(i=0; i<value.length && i<oldvalue.length; i++) {
      if(value.charAt(i) != oldvalue.charAt(i)) {
        break;
      }
    }
    return i;
}

// return the # of chars from the right that are the same
function getNewCharPosR(value, oldvalue) {
    var i=value.length-1;
    var j=oldvalue.length-1;
    var k=0;
    for(k=0; i>=0 && j>=0; i--, j--, k++) {
      if(value.charAt(i) != oldvalue.charAt(j)) {
        break;
      }
    }
    return k;
}

function getEventType(field, oldvalue) {
    var value=field.value;
    pos1=getNewCharPosL(value, oldvalue);
    pos2=getNewCharPosR(value, oldvalue);

    if(value.length > 1+oldvalue.length) {
        alert("java script error");
        return;
    } else if(value.length < oldvalue.length) { // char delete; possible replacement with 1 char
        if(pos1+pos2 > oldvalue.length) {
          pos2= oldvalue.length-pos1;
        }
        if(pos1==value.length) {
          return 0; // delete at the end
        } else {
          return 1; // other delete
        }
    } else if(value.length == oldvalue.length) { // replacement of 1 char
        return 2;
    } else if(value.length == (1+oldvalue.length)) { // insert/append 1 char
        if(pos1==oldvalue.length) {
          return 3; // append 1 char
        } else if(pos1==0) {
          return 4; // insert in front 1 char
        } else {
          return 5; // other insert
        }
    }
}

function isMatch(ch, type) {
    switch(type) {
      case 'A':
        if(!(ch>='A' && ch<='Z' || ch>='a' && ch<='z' || ch>='0' && ch<='9')) {
      return false;
        }
        break;
      case 'L':
        if(!(ch>='A' && ch<='Z' || ch>='a' && ch<='z')) {
      return false;
        }
        break;
      case 'D':
        if(!(ch>='0' && ch<='9')) {
      return false;
        }
        break;
      default:
        if(ch!=type) {
      return false;
        }
        break;
    }
    return true;
}

function isLiteral(type) {
    return !(type=='A' || type=='L' || type=='D');
}

function qws_field_on_blur(field, mask) {
    if(mask == null || mask.length == 0) return;
    if(global_alerting || field.value.length==0) {
          return;
    }
    var i=findMismatch(field.value, mask);
    if(i<mask.length || i<field.value.length) {
      alert("Field value does not match input mask <" + mask + ">");
      field.select();
      field.focus();
    }
}

function findMismatch(value, mask) {
        var valid=true;
        for(i=0; i<value.length && i<mask.length; i++) {
            if(! isMatch(value.charAt(i), mask.charAt(i))) {
              valid=false;
              break;
            }
        }
        return i;
}

function describe(type) {
  switch(type) {
      case 'A': return "alphanumeric";
      case 'L': return "letter";
      case 'D': return "number";
      default:  return ""+type;
  }
}

function add_if_needed(field, mask) {
        var idx=findMismatch(field.value, mask);
        if(idx<field.value.length) { // find mismatch, return the index
          if(isLiteral(mask.charAt(i))) {
              field.value=field.value.substring(0, idx) + mask.charAt(idx) + field.value.substring(idx);
              if(field.value.length<=mask.length && add_if_needed(field, mask)) {
                  return true;
              } else {
                  return false;
              }
          } else {
              return false;
          }
        } else {
          return true;
        }
}
function setchanged(checkFieldName) {
       if (checkFieldName.length > 0) {
          var e = document.getElementById(checkFieldName + "id");
          e.checked = "true";
       }
       document.AuditSearchForm.fieldchanged.value = "true";
}

function validate_Integer_fields(field,  label,valueType) {
    if (field.value == "")  // if nothing entered, do not validate
  return;

   var val ;

    var s = field.value;
    var startPos = 0;

   if ( (s.charAt(0) == "-") || (s.charAt(0) == "+") )
            startPos = 1;

   if(valueType == '0') 
      {
     var avalue = s.substring(startPos, s.length);

        if (!isInteger(s.substring(startPos, s.length))) 
     { 
        alert("Please enter a Integer value for the '" + label + "' field.");
        field.focus();  // go to current field
        return;
      }

    }
    else if(valueType == '4'){

     if (!isInteger(s.substring(startPos, s.length))) 
     { 
        alert("Please enter a long value for the '" + label + "' field.");
        field.focus();  // go to current field
        return;
      }

    }
    else if( valueType == '7' ){
        
        if(!isFloat(s.substring(startPos, s.length))){
         alert("Please enter float value for the '" + label + "' field.");
        field.focus();  // go to current field
        return;
     }



    }

}


   
    function isInteger (s)
   {
      var i;

      if (isEmpty(s))
      if (isInteger.arguments.length == 1) return 0;
      else return (isInteger.arguments[1] == true);

      for (i = 0; i < s.length; i++)
      {
       
         var c = s.charAt(i);

         if (!isDigit(c)) return false;
      }

      return true;
   }



function isFloat(s)
 
 {
    

   var str = s.indexOf('.');

 var startPos = 0;
//   if(str != -1){
 
   var s1 = s.substring(startPos, str);
   var s2 = s.substring(str+1, s.length);


  if(s1 .length !=0){

      for (i = 0; i < s1.length; i++)
      {
    
      var c = s1.charAt(i);
    
      if (!isDigit(c)) return false;

      }
   }

 if(s2 .length !=0){
       for (i = 0; i < s2.length; i++)
      {
    
      var c = s2.charAt(i);
    
      if (!isDigit(c)) return false;

      }
   }
   
//   }else
//  {
//   return false;
//   }
      return true;
   }
   




      function isEmpty(s)
   {
      return ((s == null) || (s.length == 0))
   }

     
     
     function isDigit (c)
   {
      return ((c >= "0") && (c <= "9"))
   }


function clear_masking_on_focus() {
    global_field="";
    global_old_value= "";
}


// *******   end masking related functions     *******

