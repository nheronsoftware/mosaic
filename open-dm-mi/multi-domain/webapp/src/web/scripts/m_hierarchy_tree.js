/*
 * JavaScript code related to Manage Hierarchy Trees
 */


var hierarchyMainTree_Data = { identifier: 'id',
    label: 'name',
    items: []};
var hierarchyMainTree_Store = null;

var hierarchyRearrangeTree_Data = { identifier: 'id',
    label: 'name',
    items: []};
var hierarchyRearrangeTree_Store = null;

var hierarchyEmpty = true;

dojo.addOnLoad(function(){
    hierarchyMainTree_Store = new dojo.data.ItemFileWriteStore({data: hierarchyMainTree_Data});
    hierarchyRearrangeTree_Store = new dojo.data.ItemFileWriteStore({data: hierarchyRearrangeTree_Data});
});

var item_types = {HIERARCHY:"hierarchy", DOMAIN:"domain", RECORD:"record"};

var isHierarchyRearrangeTreeShown = false;
var hierarchyRootRecordItem = {};
var selectedHierarchyNodeEUID = null;
var selectedHierarchyNodeId = null;
var hierarchy_Selected_Record = null;
var hierarchy_rearrange_Selected_Record = null;

var hierarchy_last_hovered_item = null;

function hierarchy_initMainTree(data) {
    
    // 1. Populate store for tree
    // delete existing data items from store.
    hierarchyMainTree_Store.fetch ({query:{},onComplete: hierarchyDeleteItemsFromStore, queryOptions: {deep:true}});
    
    // Create root element
    var rootHierarchyItem = {};
    rootHierarchyItem.id = "rootHierarchyID";
    rootHierarchyItem.name = currentHierarchy;
    rootHierarchyItem.type = item_types.HIERARCHY;
    rootHierarchyItem.isRoot = false;   
    var rootHierarchy = hierarchyMainTree_Store.newItem( rootHierarchyItem , null);
    
    // Create node for the main record. eg., George Karlin.
    if (data != null) {
        if (data.node != null) {
    	hierarchyEmpty = false;
            
            var curHierarchyParentRecord = rootHierarchy;
            if (data.ancestors != null) {
                for(j=data.ancestors.length - 1; j>=0; j--) {
                    var currentAncestorItem = {};
                    currentAncestorItem.id = data.ancestors[j].id;
                    currentAncestorItem.EUID = data.ancestors[j].EUID;
                    currentAncestorItem.name = data.ancestors[j].highLight;
                    currentAncestorItem.type = item_types.RECORD;
                    currentAncestorItem.isRoot = false;
                    curHierarchyParentRecord = hierarchyMainTree_Store.newItem(currentAncestorItem, {parent: curHierarchyParentRecord, attribute:"children"});
                }
            }
            
            var nodeRecordItem = {};
            nodeRecordItem.id = data.node.id;
            nodeRecordItem.EUID = data.node.EUID;
            nodeRecordItem.name = data.node.highLight;
            nodeRecordItem.type = item_types.RECORD;
            nodeRecordItem.isRoot = true;
            var nodeRecord = hierarchyMainTree_Store.newItem(nodeRecordItem, {parent: curHierarchyParentRecord, attribute:"children"});
    
      if (data.children != null) {
    		for(i=0; i<data.children.length; i++) {
        	var curChildRecordItem = {};
        	curChildRecordItem.id = data.children[i].id;
        	curChildRecordItem.EUID = data.children[i].EUID;
        	curChildRecordItem.name = data.children[i].highLight;
        	curChildRecordItem.type = item_types.RECORD;
        	curChildRecordItem.isRoot = false;
        	curChildRecordItem.isStub = true;    
                    var curChildRecord = hierarchyMainTree_Store.newItem(curChildRecordItem, {parent: nodeRecord, attribute:"children"});
                }
    		}
    	}
  	} else {
  		hierarchyEmpty = true;
  	}
    // 2. Save the items of store
    hierarchyMainTree_Store.save();
    
    // 3. Create tree now
    hierarchy_createMainTree();
    displayDiv("hierarchyMainTreeContainer", true);
    
    // 4. Reset button pallete (enable/disable add,delete, etc., buttons)
    hierarchy_resetView ();
    hierarchy_refreshMainTreeButtonsPallete();
    hierarchy_refreshRearrangeTreeButtonsPallete();
    return;
}


// Function to create the main tree. Store should be populated before calling this.
function hierarchy_createMainTree() {
    var newModel  = new dijit.tree.TreeStoreModel(
        { store: hierarchyMainTree_Store, query: {id:'rootHierarchyID'},
        childrenAttrs: ["children"], mayHaveChildren: customHierarchyMainTreeMayHaveChildren }
    );
    var mainTreeObj = dijit.byId("hierarchyTree");
    if (dijit.byId("hierarchyTree")) {
        dijit.byId("hierarchyTree").destroy();
    }
    
    mainTreeObj = new dijit.TreeCustom({
        id: "hierarchyTree",
        model: newModel, 
        customOnClick: hierarchyMainTreeClicked,
        getIconClass: hierarchyMainTreeGetIconClass, 
        lazyLoadItems: hierarchyMainTreeLazyLoad,
        dndController:"dijit._tree.dndSource", 
        checkAcceptance: hierarchyMainTreeDnDCheckAcceptance,
        checkItemAcceptance: hierarchyMainTreeDnDCheckItemAcceptance, 
        onDndDrop: hierarchyMainTreeDragAndDrop,
        dragThreshold: 8
    }, document.createElement("div"));
    mainTreeObj.startup();
    dojo.byId("hierarchyMainTreeContainer").appendChild(mainTreeObj.domNode);
}

function hierarchyMainTreeDragAndDrop(source, nodes, copy, target) {
    
    if (nodes != null && nodes.length > 0) {
        var selectedItem = dijit.getEnclosingWidget(nodes[0]).item;
        
        var targetId = hierarchyMainTree_Store.getValue(hierarchy_last_hovered_item, "id");
        var targetEUID = hierarchyMainTree_Store.getValue(hierarchy_last_hovered_item, "EUID");
    
        if (this == source) {
            // DnD from within the mainTree
            var sourceEUID = hierarchyMainTree_Store.getValue(selectedItem, "EUID");
            if (sourceEUID == targetEUID) {
                return;
            }
            
            var sourceNodeIds = [hierarchyMainTree_Store.getValue(selectedItem, "id")];
            
            HierarchyHandler.moveHierarchyNodes(sourceNodeIds, targetId, hierarchy_moveHierarchyNodes_CB);
        
        } else {
            // Dnd from rearrangeTree to mainTree
            var sourceEUID = hierarchyRearrangeTree_Store.getValue(selectedItem, "EUID");
            var hierarchyDefObj = hierarchyDefsCache[currentHierarchy + currentDomain_Hierarchy];
            var nodeRecord = {"id":"0", "EUID":sourceEUID, "parentId":targetId, "parentEUID":targetEUID, "hierarchyDefId":hierarchyDefObj.id, "name":currentHierarchy, "domain":currentDomain_Hierarchy};
            nodeRecord.startDate = null;
            nodeRecord.endDate = null;
            nodeRecord.purgeDate = null;
            nodeRecord.attributes = [];
            
            HierarchyHandler.addHierarchyNode(nodeRecord, hierarchy_rearrangeTree_move_CB);
    }
}
}

function hierarchy_moveHierarchyNodes_CB(data) {
    HierarchyHandler.getRootHierarchyTree(currentHierarchy, currentDomain_Hierarchy, { callback:function(dataFromServer) {
        hierarchy_initMainTree(dataFromServer); }
    });
}
    
function customHierarchyMainTreeMayHaveChildren(item) {
    return true;
}

function hierarchyMainTreeDnDCheckAcceptance(source, nodes)  {
    return true;
}

function hierarchyMainTreeDnDCheckItemAcceptance(node, source) {
    hierarchy_last_hovered_item = dijit.getEnclosingWidget(node).item;
    
    if (hierarchy_last_hovered_item.id == selectedHierarchyNodeId) {
        return false;
    }
    
    if (hierarchy_last_hovered_item.type == item_types.RECORD) {
    return true;
    } else {
        return false;
    }
    //TBD: Check if the items can be dropped on this node. based on Move Rules.
}

// Function called when tree (MAIN Tree) is trying load the childrens for expanded Record.
function hierarchyMainTreeLazyLoad(node, callback_function) {
    var parentItem = node.item;
    if(parentItem == null ) {
        callback_function();
        return;
    }
    
    var isStillStub = hierarchyMainTree_Store.getValue (parentItem, "isStub");
    var parentName = hierarchyMainTree_Store.getValue (parentItem, "name");
    var parentEUID = hierarchyMainTree_Store.getValue (parentItem, "EUID");
    var parentId = hierarchyMainTree_Store.getValue (parentItem, "id");
    
    if(isStillStub ) {
        HierarchyHandler.getHierarchyTree(parentId, parentEUID, {
            callback:function(dataFromServer) {
                hierarchMainTreeLazyLoadCB(dataFromServer, node, callback_function);
            }
        });
        
        // make the stub marking as false.
        hierarchyMainTree_Store.setValue(parentItem, "isStub", false);
    }
}

// To load next level of relationships. Loaded only when user expands the record. 
function hierarchMainTreeLazyLoadCB(data, node, callback_function) {
    if(data == null || data.children == null) {
        callback_function();
        return;
    }
    var parentItem = node.item;
    
    for(i=0; i<data.children.length; i++) {
        var curChildRecordItem = {};
        curChildRecordItem.id = data.children[i].id;
        curChildRecordItem.EUID = data.children[i].EUID;
        curChildRecordItem.name = data.children[i].highLight;
        curChildRecordItem.type = item_types.RECORD;
        curChildRecordItem.isRoot = false;
        curChildRecordItem.isStub = true;
        
        var curChildRecord = hierarchyMainTree_Store.newItem(curChildRecordItem, {parent: parentItem, attribute:"children"});
    }
    
    callback_function();
}

// function to delete items from main tree store.
function hierarchyDeleteItemsFromStore(items, req) {
    for(i=0; i<items.length; i++) {
        var status = hierarchyMainTree_Store.deleteItem(items[i]);
        hierarchyMainTree_Store.save();
    }
}

var hierarchyMainTree_isAddPossible = true;
var hierarchyMainTree_isDeletePossible = false;
var hierarchyMainTree_isFindPossible = false;
var hierarchyMainTree_isMovePossible = false;

// Main tree click event is captured by this function
function hierarchyMainTreeClicked(item, node, allSelectedItems ) {
    
    if (allSelectedItems.length == 0) {
        return;
    }
    
    var selectedItem = node.item;
    var itemType = hierarchyMainTree_Store.getValue(selectedItem, "type");
    var itemName = hierarchyMainTree_Store.getValue(selectedItem, "name");   
    var isRoot = hierarchyMainTree_Store.getValue(selectedItem, "isRoot");
        
    switch (itemType) {
        case item_types.RECORD:
        	selectedHierarchyNodeEUID = hierarchyMainTree_Store.getValue(selectedItem, "EUID");
          selectedHierarchyNodeId = hierarchyMainTree_Store.getValue(selectedItem, "id");
          hierarchy_Selected_Record = {};
          hierarchy_Selected_Record.EUID = hierarchyMainTree_Store.getValue(selectedItem, "EUID");
          hierarchy_Selected_Record.name = hierarchyMainTree_Store.getValue(selectedItem, "name");
            hierarchy_Selected_Record.id = hierarchyMainTree_Store.getValue(selectedItem, "id");
            
            hierarchyMainTree_isDeletePossible = true;
        break;
        case item_types.HIERARCHY:
          selectedHierarchyNodeId = hierarchyMainTree_Store.getValue(selectedItem, "id");
            selectedHierarchyNodeEUID = hierarchyMainTree_Store.getValue(selectedItem, "EUID");
            
            hierarchyMainTree_isDeletePossible = false;
        break;
    }
    
    hierarchy_refreshMainTreeButtonsPallete();
    hierarchy_refreshRearrangeTreeButtonsPallete();
    
    // Show details for selected record/relationship (main tree).
    hierarchy_ShowDetails();
    return;
}

// Custom icons for our main tree (different icons for domain, relationship & record)
function hierarchyMainTreeGetIconClass(item, opened) {
    if(item != null) {
        var itemType = hierarchyMainTree_Store.getValue(item, "type");
        if(itemType == item_types.HIERARCHY) {
            return "hierarchyIcon";
        } else {
            return "recordIcon";
        };
    }
    return "recordIcon";
}

// Custom icons for our Rearrange tree (different icons for domain, relationship & record)
function hierarchyRearrangeTreeGetIconClass(item, opened) {
	if(item != null) {
  	var itemType = hierarchyRearrangeTree_Store.getValue(item, "type");
    if(itemType == item_types.DOMAIN) {
    	return "domainIcon";
    } else {
      return "recordIcon";
    };
	}	
  return "recordIcon";    
}

// Function to get the data for rearrange tree.
function hierarchy_initRearrangeTree () {
    var selectedDomain = currentDomain_Hierarchy;
    var domainSearchObj = {name:selectedDomain};
    DomainHandler.getEnterprises(domainSearchObj, hierarchy_initRearrangeTree_CB); 
}

// Callback function: for getting data for re-arrange tree
function hierarchy_initRearrangeTree_CB(data) {
    // 1. Populate store for tree
    // 1a. delete existing data items from store.
    hierarchyRearrangeTree_Store.fetch ({query:{},onComplete: hierarchyDeleteItemsFromRearrangeTreeStore, queryOptions: {deep:true}});

    
    // 1b. Create root element, i.e., Domain eg., Person, UK Patient.
    var rootDomainItem = {};
    rootDomainItem.id = "rootDom";
    rootDomainItem.name = currentDomain_Hierarchy;
    rootDomainItem.type = item_types.DOMAIN;
    rootDomainItem.isRoot = true;

    var rootDomain = hierarchyRearrangeTree_Store.newItem( rootDomainItem , null);   
    var domainRecords = data;

    for(i=0; i<domainRecords.length; i++) {
        var tempRec = domainRecords[i];
        var recordNode = {};
        recordNode.id = i + "_" + tempRec.EUID;
        recordNode.EUID = tempRec.EUID;
        recordNode.name = tempRec.highLight;
        recordNode.type = item_types.RECORD;
        recordNode.parentDomain = rootDomainItem.name;
        recordNode.isStub = false;
        var recordNodeItem = hierarchyRearrangeTree_Store.newItem(recordNode, {parent: rootDomain, attribute:"children"} );
    }
    
    // 2. Save the items of store
    hierarchyRearrangeTree_Store.save();

    // 3. Create tree now
    hierarchy_createRearrangeTree();
    displayDiv("hierarchyRearrangeTreeContainer", true);
    
    // 4. Reset button pallete (enable/disable add,delete, etc., buttons)
    hierarchy_refreshMainTreeButtonsPallete();
    hierarchy_refreshRearrangeTreeButtonsPallete();
}


// Function to create the Rearrange tree. Store should be populated before calling this.
function hierarchy_createRearrangeTree() {
    var rearrangeTreeModel  = new dijit.tree.TreeStoreModel({
        store: hierarchyRearrangeTree_Store,
        query: {id:'rootDom'},
        childrenAttrs: ["children"],
        mayHaveChildren: hierarchyCustomRearrangeTreeMayHaveChildren
    });
                                                                 
    var rearrangeTreeObj = dijit.byId("hierarchyRearrangeTree");
    if (dijit.byId("hierarchyRearrangeTree")) {
        dijit.byId("hierarchyRearrangeTree").destroy();
    };

    rearrangeTreeObj = new dijit.TreeCustom({
        id: "hierarchyRearrangeTree",
        model: rearrangeTreeModel,
        customOnClick: hierarchyRearrangeTreeClicked,
        getIconClass: hierarchyRearrangeTreeGetIconClass,
        lazyLoadItems: hierarchyLazyLoadRearrangeTreeRelationships,
        dndController:"dijit._tree.dndSource",
        checkAcceptance: hierarchyRearrangeTreeDnDCheckAcceptance,
        checkItemAcceptance: hierarchyRearrangeTreeDnDCheckItemAcceptance,
        dragThreshold: 8
    }, document.createElement("div"));
    rearrangeTreeObj.startup();
    dojo.byId("hierarchyRearrangeTreeContainer").appendChild(rearrangeTreeObj.domNode);
}

// Custom function, for deciding if a node may have children or not. (For Rearrange tree)
function hierarchyCustomRearrangeTreeMayHaveChildren(item) {
    return false;
}

function hierarchyRearrangeTreeDnDCheckAcceptance(source,nodes) {
    return false;
}

function hierarchyRearrangeTreeDnDCheckItemAcceptance(node, source ){
    return false;
}

// function to delete items from rearrange tree store.
function hierarchyDeleteItemsFromRearrangeTreeStore(items, req) {
    for(i=0; i<items.length; i++) {
        var status = hierarchyRearrangeTree_Store.deleteItem(items[i]);
        hierarchyRearrangeTree_Store.save();
    }
}

// Function called when tree (rearrange Tree) is trying load the childrens for expanded node.
function hierarchyLazyLoadRearrangeTreeRelationships(node, callback_function) {
    
}

var hierarchyRearrangeTree_isAddPossible = false;
var hierarchyRearrangeTree_isDeletePossible = false;
var hierarchyRearrangeTree_isFindPossible = false;
var hierarchyRearrangeTree_isMovePossible = false;

function hierarchyRearrangeTreeClicked(item, node, allSelectedItems ) {
    
    if (allSelectedItems.length == 0) {
        return;
    }
        
    var selectedItem = node.item;

    var itemType = hierarchyRearrangeTree_Store.getValue(selectedItem, "type");
    var itemName = hierarchyRearrangeTree_Store.getValue(selectedItem, "name");
    var isRoot = hierarchyRearrangeTree_Store.getValue(selectedItem, "isRoot");

    switch (itemType) {
        case item_types.DOMAIN:
            hierarchyRearrangeTree_isMovePossible = false;
            break;
        case item_types.RECORD:
            hierarchy_rearrange_Selected_Record = {};
            hierarchy_rearrange_Selected_Record.EUID = hierarchyRearrangeTree_Store.getValue(selectedItem, "EUID");
            hierarchy_rearrange_Selected_Record.name = hierarchyRearrangeTree_Store.getValue(selectedItem, "name");           
            
            
            hierarchyRearrangeTree_isMovePossible = true;
            break;
    }
            
    hierarchy_refreshMainTreeButtonsPallete();
    hierarchy_refreshRearrangeTreeButtonsPallete();
    
    // Show details section for the selected record/relationship (rearrange tree)
    hierarchy_rearrangeTree_ShowDetails();
}


// Reset the view. reset flags used for enabling/disabling buttons pallete, for both Main Tree & rearrange Tree
// clear details section (both main & rearrange details)
function hierarchy_resetView () {
    
    hierarchyMainTree_isAddPossible = true;
    hierarchyMainTree_isDeletePossible = false;
    hierarchyMainTree_isFindPossible = false;
    hierarchyMainTree_isMovePossible = false;
    
    hierarchyRearrangeTree_isAddPossible = false;
    hierarchyRearrangeTree_isDeletePossible = false;
    hierarchyRearrangeTree_isFindPossible = false;
    hierarchyRearrangeTree_isMovePossible = false;
    
    hierarchy_clearDetailsSection();
    hierarchy_Rearrange_clearDetailsSection();
}


function hierarchyShowRearrangeTree(rearrangeButtonObj) {
    if(isHierarchyRearrangeTreeShown) {
        isHierarchyRearrangeTreeShown = false;
        rearrangeButtonObj.value = "Rearrange >>";
        rearrangeButtonObj.title = "Show Rearrange tree";
        document.getElementById("hierarchyTree").style.width = "100%";
        displayDiv("hierarchyRearrangeTree", false);
    } else {
        isHierarchyRearrangeTreeShown = true;
        rearrangeButtonObj.value = "<< Rearrange";
        rearrangeButtonObj.title = "Hide Rearrange tree";
        document.getElementById("hierarchyTree").style.width = "50%";
        displayDiv("hierarchyRearrangeTree", true);
    }
    hierarchy_refreshMainTreeButtonsPallete();
}

function hierarchy_refreshMainTreeButtonsPallete() {
	var imgAddButtonObj = dojo.byId("imgHierarchyMainTreeAddButton");
  if(imgAddButtonObj != null) {
  	if(hierarchyMainTree_isAddPossible) {
  		imgAddButtonObj.src =   addButtonEnabled.src;
 	  } else {
  		imgAddButtonObj.src =   addButtonDisabled.src;
  	}    
  }
    var imgDeleteButtonObj = dojo.byId("imgHierarchyMainTreeDeleteButton");
    if (imgDeleteButtonObj != null) {
        if(hierarchyMainTree_isDeletePossible) {
            imgDeleteButtonObj.src = deleteButtonEnabled.src;
        } else {
            imgDeleteButtonObj.src = deleteButtonDisabled.src;
        }
    }
}
 
function hierarchy_refreshRearrangeTreeButtonsPallete () {
    var imgMoveLeftButtonObj = dojo.byId("imgHierarchyRearrangeTreeMoveButton");
    if(imgMoveLeftButtonObj != null) {
        if(hierarchyMainTree_isDeletePossible && hierarchyRearrangeTree_isMovePossible) {
            imgMoveLeftButtonObj.src = moveLeftButtonEnabled.src;
        } else {
            imgMoveLeftButtonObj.src = moveLeftButtonDisabled.src;
        }
    }
}
 
function hierarchy_mainTree_addOperation() {
	
	 if (selectedHierarchyNodeEUID == null &&
	 		 selectedHierarchyNodeId == null) {
	 		 alert(getMessageForI18N("select_hierarchy_node_from_hierarchy_tree"));
	 } else {
	 	 //alert("selectedHierarchyNodeId => " + selectedHierarchyNodeId + " selectedHierarchyNodeEUID => " + selectedHierarchyNodeEUID);
   	 showAddHierarchyNodeDialog();
  }
}
	 
function hierarchy_rearrangeTree_moveOperation() {
    if(!hierarchyRearrangeTree_isMovePossible) return;
    
    var hierarchyMainTreeObj = dijit.byId("hierarchyTree");
    if(hierarchyMainTreeObj == null) return;
    
    var mainTreeStore = hierarchyMainTreeObj.model.store;
    var mainTreeModel = hierarchyMainTreeObj.model;
    
    var mainTreeSelectedNodes = hierarchyMainTreeObj.getSelectedNodes();
    var mainTreeSelectedItems = hierarchyMainTreeObj.getSelectedItems();
    
    if(mainTreeSelectedItems == null || mainTreeSelectedItems.length <= 0) {
        return;
    }
    
    var mainTreeSelectedItem = mainTreeSelectedItems [0];
    var mainTreeSelectedItem_Id = mainTreeStore.getValue(mainTreeSelectedItem, "id");
    var mainTreeSelectedItem_EUID = mainTreeStore.getValue(mainTreeSelectedItem, "EUID");
    
    
    var hierarchyRearrangeTreeObj = dijit.byId("hierarchyRearrangeTree");
    if(hierarchyRearrangeTreeObj == null) return;
    
    var rearrangeTreeStore = hierarchyRearrangeTreeObj.model.store;
    var rearrangeTreeModel = hierarchyRearrangeTreeObj.model;
    
    var rearrangeTreeSelectedNodes = hierarchyRearrangeTreeObj.getSelectedNodes();
    var rearrangeTreeSelectedItems = hierarchyRearrangeTreeObj.getSelectedItems();
    
    if(rearrangeTreeSelectedItems == null || rearrangeTreeSelectedItems.length <= 0) {
        return;
    }
    
    var rearrangeTreeSelectedItem = rearrangeTreeSelectedItems [0];
    var rearrangeTreeSelectedItems_EUID = rearrangeTreeStore.getValue(rearrangeTreeSelectedItem, "EUID");
    
    
    var hierarchyDefObj = hierarchyDefsCache[currentHierarchy + currentDomain_Hierarchy];
  
    //HierarchyNodeRecord: id,EUID,parentId,parentEUID,startDate,endDate,purgeDate, attributes
    var hierarchyNodeRecord = {"id":"0", "EUID":rearrangeTreeSelectedItems_EUID, "parentId":mainTreeSelectedItem_Id, "parentEUID":mainTreeSelectedItem_EUID, "hierarchyDefId":hierarchyDefObj.id, "name":currentHierarchy, "domain":currentDomain_Hierarchy};
    hierarchyNodeRecord.startDate = null;
    hierarchyNodeRecord.endDate = null;
    hierarchyNodeRecord.purgeDate = null;
    hierarchyNodeRecord.attributes = [];

    //alert("adding HierarchyNodeRecord: " + hierarchyNodeRecord.EUID);
    HierarchyHandler.addHierarchyNode(hierarchyNodeRecord, hierarchy_rearrangeTree_move_CB);
}

function hierarchy_rearrangeTree_move_CB(data) {
    HierarchyHandler.getRootHierarchyTree(currentHierarchy, currentDomain_Hierarchy, { callback:function(dataFromServer) {
        hierarchy_initMainTree(dataFromServer); }
    });
}


function hierarchy_mainTree_deleteOperation() {
    if(!hierarchyMainTree_isDeletePossible) return;
    
    var hierarchyMainTreeObj = dijit.byId("hierarchyTree");
    if(hierarchyMainTreeObj == null) return;
    
    var treeStore = hierarchyMainTreeObj.model.store;
    var treeModel = hierarchyMainTreeObj.model;
    
    var treeSelectedNodes = hierarchyMainTreeObj.getSelectedNodes();
    var treeSelectedItems = hierarchyMainTreeObj.getSelectedItems();
    
    if(treeSelectedItems == null || treeSelectedItems.length <= 0) {
        return;
    }
    
    var tempItem = treeSelectedItems [0];
    var tempItemType = treeStore.getValue(tempItem, "type");
    var tempItemName = treeStore.getValue(tempItem, "name");
    var tempItemId = null;
    switch (tempItemType) {
        case item_types.HIERARCHY:
            break;
        case item_types.RECORD:
            tempItemId = treeStore.getValue(tempItem, "id");
            break;
    }
    
    if (tempItemId == null) {
        return;
    }
    
    var confirmation = confirm("Are you sure to delete " + tempItemName + "?");
    if(confirmation) {
        //alert("deleting HierarchyNodeRecord: " + tempItemId);
        HierarchyHandler.deleteHierarchyNode(tempItemId, hierarchy_mainTree_delete_CB);
    }
    
}

function hierarchy_mainTree_delete_CB(data) {
    HierarchyHandler.getRootHierarchyTree(currentHierarchy, currentDomain_Hierarchy, { callback:function(dataFromServer) {
        hierarchy_initMainTree(dataFromServer); }
    });
}

function hierarchy_mainTree_findOperation() {
}
	 