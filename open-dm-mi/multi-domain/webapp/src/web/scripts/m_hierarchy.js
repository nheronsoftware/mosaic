
/*
 * API integration scripts for Manage - Hierarchy Screens.
 */ 

var currentDomain_Hierarchy;
var currentHierarchy;
var currentHierarchyDefId;
var hierarchyDefsCache = {};
var currentHierarchyNodeRecord = null;
var isSelectDomainCriteriaApplied = false;
var isSelectHierarchyDefAttributeCriteriaApplied = false;
var isHierarchySelectDialogLoaded = false;
var isAddHierarchyNodeDialogLoaded = false;
var selectedRootOrSpecificNode = "selectedHierarchyTreeRootNode";

var summaryFieldsCache_hierarchy = {};
var detailFieldsCache_hierarchy = {};

function setCurrentDomainHierarchy(id) {
    currentDomain_Hierarchy = document.getElementById(id).value;
    loadSelectHierarchyDefs();
}
function loadSelectHierarchyDomains() {
    DomainHandler.getDomains(updateDomains);
}
function updateDomains(data) {
    dwr.util.removeAllOptions("select_hierarchy_domain");
    dwr.util.addOptions("select_hierarchy_domain", data, "name");        
    dwr.util.setValue("select_hierarchy_domain", data[0].name);     
    currentDomain_Hierarchy = document.getElementById("select_hierarchy_domain").value;
    //loadSelectHierarchyDefs();
	  //loadSelectHierarchySearch();
}
function loadSelectHierarchyDefs() {
    HierarchyDefHandler.getHierarchyDefs(currentDomain_Hierarchy, updateSelectHierarchyDefs);
}
function updateSelectHierarchyDefs (data) {
    dwr.util.removeAllOptions("load_select_hierarchies");
    if (data != null && data.length > 0) {
    	dwr.util.addOptions("load_select_hierarchies", data, "name");      
    	dwr.util.setValue("load_select_hierarchies", data[0].name);
    } 
    //refreshHierarchyDefsButtonsPalette();
}
function loadSelectHierarchySearch(){
    RelationshipDefHandler.getDomainSearchCriteria(currentDomain_Hierarchy, loadSelectHierarchySearchTypes);
}
function loadSelectHierarchySearchTypes(data){
	dwr.util.removeAllOptions("select_hierarchy_searchtypes"); 
    for (var searchType in data)  {
    	var opt =  document.createElement("option");
      opt.text = searchType;
      opt.value = searchType;
			opt.title = searchType;
      document.getElementById('select_hierarchy_searchtypes').options.add(opt);
     }
     selectHierarchySearchFields('select_hierarchy_searchtypes');
}
function selectHierarchySearchFields(searchTypeId){
    var searchType = document.getElementById(searchTypeId).value;
    RelationshipDefHandler.getSearchTypeCriteria(currentDomain_Hierarchy, searchType,showSelectHierarchySearchFields);
}
function showSelectHierarchySearchFields(data){
		dwr.util.removeAllRows("select_hierarchy_search_fields");
  	var count = 0;
		var guiTypeStr;
  	// create a hidden field, put the querybuilder value in it. hidden field name=selectHierarchyQueryBuilder.
  	hiddenField = document.createElement("input");
  	hiddenField.type = "hidden";
    hiddenField.id = "selectHierarchyQueryBuilder";
    hiddenField.name = "selectHierarchyQueryBuilder";
    hiddenField.value = data.queryBuilder;
    document.getElementById('select_hierarchy_search_fields').appendChild(hiddenField);
    var fieldGroups = data.fieldGroups;
    for (var fieldGrp in fieldGroups)  {
			var descriptionRow = document.getElementById('select_hierarchy_search_fields').insertRow(count++);
			descriptionRow.insertCell(0);
			descriptionRow.cells[0].innerHTML = "<b>"+ fieldGrp + "</b>";
    	for(i=0; i<fieldGroups[fieldGrp].length; i++) {
      	fieldCfg = fieldGroups[fieldGrp][i];
        var row = document.getElementById('select_hierarchy_search_fields').insertRow(count++);
        row.insertCell(0);
        row.insertCell(1);
			 	guiTypeStr = fieldCfg.guiType;
			 	if(guiTypeStr.toLowerCase() == "textbox"){
        	var field;
          try{
          	field = document.createElement('<input type="text" name="selectHierarchySearchFieldName" />');
          }catch(err){
          	field = document.createElement("input");
          }
          field.type="text";
          field.size = "20";
			 		field.maxlength = fieldCfg.maxLength;
          field.name="selectHierarchySearchFieldName";
			 		field.title =  fieldCfg.displayName;
          field.domainFieldName = fieldCfg.displayName;
          field.fieldName = fieldCfg.name;          
          row.cells[0].innerHTML = fieldCfg.displayName;
          row.cells[1].appendChild(field);
			  }
      }
     } 	
 		currentHierarchy = document.getElementById("load_select_hierarchies").value;
 		var hierarchyDefObj = hierarchyDefsCache[currentHierarchy + currentDomain_Hierarchy];
	  document.getElementById("selected_hierarchydef").innerHTML = currentHierarchy;  
    loadSelectedHierarchyDefAttributes(currentHierarchy, currentDomain_Hierarchy);
}
function m_hierarchy_showSelectDialog () {
   var selectDialog = dijit.byId("select_hierarchy");
   if(selectDialog != null) {
      selectDialog.show();
    }
   if(isHierarchySelectDialogLoaded) {
   	initializeSelectHierarchy();
   }
}
function initializeSelectHierarchy() {
    isHierarchySelectDialogLoaded = true;
    loadSelectHierarchySearch();
} 
function hideSelectHierarchyDialog () {
    dijit.byId('select_hierarchy').hide();
}

function searchHierarchyNodes() {
	isSelectDomainCriteriaApplied = false;
	isSelectHierarchyDefAttributeCriteriaApplied = false;
	
	var domain = currentDomain_Hierarchy;
  var hierarchyDef = currentHierarchy;
  if (domain == null || hierarchyDef == null) {
  	alert("failed to search hierarchy nodes and domain:" + domain + " or hierarchy:" + hierarchyDef + " invalid.");
  }   
 
  var hierarchyDefObj = hierarchyDefsCache[hierarchyDef + domain];
  var domainSearchFieldNames = document.getElementsByName('selectHierarchySearchFieldName');
    
  //alert("searchHierarchyNodes - extended attributes - " + hierarchyDefObj.extendedAttributes.length);  
  for(i = 0; i < hierarchyDefObj.extendedAttributes.length; i++) {
  	var attributeName = hierarchyDefObj.extendedAttributes[i].name;
    var attributeId = document.getElementById("select_custom_" + attributeName);
    if (attributeId == null) {
    	continue;	
    }
    var attributeValue =  document.getElementById("select_custom_" + attributeName).value;
    var attributeType = hierarchyDefObj.extendedAttributes[i].dataType;
    if (!isValidCustomAttribute(attributeType, attributeValue)) {
    	alert(attributeValue + " " + getMessageForI18N("isnotavalidvaluefor")+ " " + attributeName + " " + getMessageForI18N("attribute")+ " " +getMessageForI18N("attributeTypeFor")+ " " +tempAttr.name  + " " +getMessageForI18N("is")+ " " +"'"+attributeType+"'");
      attributeId.focus(); 
      return;
    }
    var byRangeObj = document.getElementById("select_custom_" + attributeName + "_TO");
    var byRangeName = "";
    var byRangeValue = "";
    if (byRangeObj != null){
    	byRangeName = byRangeObj.name;
      byRangeValue = byRangeObj.value; 
      if (!isValidCustomAttribute(attributeType, byRangeObj.value)) {
      	alert(byRangeValue + " " + getMessageForI18N("isnotavalidvaluefor")+ " " + byRangeName + " " + getMessageForI18N("attribute")+ " " +getMessageForI18N("attributeTypeFor")+ " " +byRangeName + " " +getMessageForI18N("is")+ " " +"'"+attributeType+"'");
        byRangeObj.focus(); 
        return;
      }
    }         
  }
   
  //alert("searchHierarchyNodes - domain - " + domain + ": hierarchy - " + hierarchyDef);  
  var domainSearch = {"name":domain}; 
  var hierarchySearch = {"name": hierarchyDef, "domain": domain};
  var domainQueryBuilder = document.getElementById("select_hierarchy_searchtypes").value;
  domainSearch["type"] = domainQueryBuilder;
  var domainAttributes = [];
  for(i=0; i<domainSearchFieldNames.length; i++){
  	var domainTempFieldName = domainSearchFieldNames[i].domainFieldName;
    var domainTempFieldValue = domainSearchFieldNames[i].value ;
    var domainTempMap = {} ;
    domainTempMap["name"]= domainTempFieldName;
    domainTempMap["value"]= domainTempFieldValue;
    domainAttributes.push(domainTempMap );
    if (!isEmpty(domainTempFieldValue)) {
    	isSelectDomainCriteriaApplied = true;
    }
  }
  domainSearch.attributes = domainAttributes;
 
  //alert("searchHierarchyNodes - search custom attributes"); 
  var searchCustomAttributes = []; 
  for(i=0; i <hierarchyDefObj.extendedAttributes.length; i++) {
      var customAttributeName = hierarchyDefObj.extendedAttributes[i].name;
      var customAttributeId = document.getElementById("select_custom_" + customAttributeName);
      if(customAttributeId == null) {
      	continue;
      }
      var customAttributeValue =  document.getElementById("select_custom_" + customAttributeName).value;
      var customAttributeType = hierarchyDefObj.extendedAttributes[i].dataType;
      if (!isValidCustomAttribute(customAttributeType, customAttributeValue) ) {
          alert(customAttributeValue + " " + getMessageForI18N("isnotavalidvaluefor") + " " + customAttributeName + " " + getMessageForI18N("attribute")
           			+ " " + getMessageForI18N("attributeTypeFor") + " " + customAttributeName + " " + getMessageForI18N("is") + " " + "'" + customAttributeType + "'");
          customAttributeId.focus();
          return;
      }
      
      if(!isEmpty(customAttributeValue)) {
      	isSelectHierarchyDefAttributeCriteriaApplied = true;
      }
      searchCustomAttributes.push({"name":customAttributeName, "value":customAttributeValue});
  }
  
  //alert("searchHierarchyNodes - search predefined attributes");
  var startDateField = document.getElementById('select_predefined_startDate');
  var endDateField = document.getElementById('select_predefined_endDate');
  var purgeDateField = document.getElementById('select_predefined_purgeDate');
  var startDate,endDate,purgeDate;
  if(startDateField != null){
  	startDate =  document.getElementById('select_predefined_startDate').value;
    if(!isEmpty(startDate)) isSelectHierarchyDefAttributeCriteriaApplied = true;
  }
  if(endDateField != null) {
  	endDate =  document.getElementById('select_predefined_endDate').value;
    if(!isEmpty(endDate)) isSelectHierarchyDefAttributeCriteriaApplied = true;
  }
  if(purgeDateField != null) {
  	purgeDate =  document.getElementById('select_predefined_purgeDate').value;
    if(!isEmpty(purgeDate)) isSelectHierarchyDefAttributeCriteriaApplied = true;
  }
  
  hierarchySearch.startDate = startDate;
  hierarchySearch.endDate = endDate;
  hierarchySearch.purgeDate = purgeDate;
  hierarchySearch.attributes = searchCustomAttributes; 
  
  //alert("searchHierarchyNodes - searchHierarchyNodes");
  HierarchyHandler.searchHierarchyNodes(domainSearch, hierarchySearch, searchHierarchyNodesCB);
}

var searchHierarchyNodesCache = null;
var hierarchySearchResultsRowsPerPage = 10;
var hierarchySearchResultsLength = null;

function searchHierarchyNodesCB(data) {
	
  searchHierarchyNodesCache = [];
  for(i=0; i<data.length; i++) {
  	searchHierarchyNodesCache.push(data[i]);
  }
  if(data == null){ 
  	displayDiv("hierarchy_search_results_failure", true);
    displayDiv("hierarchy_search_results_success", false);
    displayDiv("hierarchy_search_results_div", false);
  } else {
  	displayDiv("hierarchy_search_results_success", true);
    displayDiv("hierarchy_search_results_failure", false);
    displayDiv("hierarchy_search_results_div", true);    
    displayHierarchySearchResults(1, hierarchySearchResultsRowsPerPage); 
    var hierarchySearchResultsPaginator = dijit.byId("hierarchy_search_results_paginator");
    hierarchySearchResultsPaginator.currentPage = 1;
    hierarchySearchResultsPaginator.totalPages = Math.ceil(searchHierarchyNodesCache.length / hierarchySearchResultsRowsPerPage) ;
    hierarchySearchResultsPaginator.refresh();
    hierarchySearchResultsLength = document.getElementsByName("hierarchySearchResultsRadio").length;        
  }
  enableSelectHiearchySearchResultsButton();
}

function displayHierarchySearchResults(currentPage,itemsPerPage) {
	
    data = searchHierarchyNodesCache;
    selectedDomain = currentDomain_Hierarchy;
    dwr.util.removeAllRows("hierarchy_search_results_tableid");
	
	  var domainSearchFieldNames = document.getElementsByName('selectHierarchySearchFieldName');
    var resultsPage = new Array();
    var itemsFrom = 0, itemsTo = 10;
    itemsFrom = (currentPage-1) * itemsPerPage;
    if(itemsFrom > data.length) {
    	itemsFrom = 0;
    }
    itemsTo = itemsFrom + itemsPerPage;
    
    if(itemsTo > data.length) {
    	itemsTo = data.length;
    }
  
    for (i=itemsFrom; i<itemsTo; i++) {
    	resultsPage.push(data[i]);
    }
    
    var column = 0;
    for(i =0; i<resultsPage.length; i++) {
    	if (i==0) {
      	column = 0;
        var header = document.getElementById('hierarchy_search_results_tableid').insertRow(i);
        header.className = "header";
        header.insertCell(column++);
        header.insertCell(column);
        header.cells[column].className = "label";
        header.cells[column].innerHTML  = "id";
        column++;  
        header.insertCell(column);
        header.cells[column].className = "label";
        header.cells[column].innerHTML  = "highLight";
        column++;  
        var record = resultsPage[i].objectRecord;
        for(j=0; j<record.attributes.length; j++) {
        	var fieldName = record.attributes[j].name;
        	var displayName = containsFieldName(fieldName, domainSearchFieldNames);
        	//var displayName = getDisplayNameForField(resultsPage[i].objectRecord.name, resultsPage[i].objectRecord.attributes[j].name);
          if (displayName == null) {
          	continue;
          } else {
          	header.insertCell(column);
          	header.cells[column].className = "label";
          	header.cells[column].innerHTML  = displayName;
          	column++;
        	}
        }
      }
     
      column = 0;
      var dataRow = document.getElementById('hierarchy_search_results_tableid').insertRow(i+1);
      dataRow.insertCell(column);
      var chkbox ; 
      try{
          chkbox = document.createElement('<input type="radio" name="hierarchySearchResultsRadio" />');
      }catch(err){
          chkbox = document.createElement("input");
      }
      chkbox.type = "radio";
      chkbox.name = "hierarchySearchResultsRadio";
      chkbox.value = resultsPage[i].id + ":" + resultsPage[i].EUID;
      dataRow.cells[column].appendChild (chkbox);
      column++;
      dataRow.insertCell(column);
      dataRow.cells[column].innerHTML = resultsPage[i].id;
      column++;      
      dataRow.insertCell(column);
      dataRow.cells[column].innerHTML = resultsPage[i].highLight;
      column++;            
      for(j=0; j<resultsPage[i].objectRecord.attributes.length; j++) {
      	var fieldName = resultsPage[i].objectRecord.attributes[j].name;
      	var displayName = containsFieldName(fieldName, domainSearchFieldNames);
      	if (displayName == null) {
      		 continue;
      	} else {
        	dataRow.insertCell(column);
        	dataRow.cells[column].innerHTML = resultsPage[i].objectRecord.attributes[j].value;
        	column++;
      	}
      }
    }
}

function containsFieldName(name, names) {
	var contained = null;
	for(k=0; k<names.length; k++) {
		if(name == names[k].fieldName) {
			contained = names[k].domainFieldName;
			break;
		}
	}
	return contained;
}

function hierarchySearchResultsDisplayRefresh(currentPage){
	displayHierarchySearchResults(currentPage, hierarchySearchResultsRowsPerPage);
}

function enableSelectHiearchySearchResultsButton(){
	if(hierarchySearchResultsLength > 0 ){
  	document.getElementById("select_hierarchy_search_results_button").disabled = false;
  }
}

function selectHierarchySearchResults() {
    var selectedDomain = currentDomain_Hierarchy;
    var selectedRecordEUID = null;
    var selectedNodeId = null;
    var selected = false;
    
    var chkBoxes = document.getElementsByName("hierarchySearchResultsRadio");
    for(i=0; i<chkBoxes.length; i++) {
    	if(chkBoxes[i].checked) {
    		var ids = chkBoxes[i].value;
    		var ids = ids.split(":");
    		selectedNodeId = ids[0];
    		selectedRecordEUID = ids[1]; 
				selected = true;
				break;
      }
    }
    
  	var chkBoxes = document.getElementsByName("select_hierarchytree_root_or_specificnode");
  	for(i=0; i<chkBoxes.length; i++) {
  		if(chkBoxes[i].checked) {
    		selectedRootOrSpecificNode = chkBoxes[i].value;
				break;
    	}
  	}    	
  	
  	if (selectedRootOrSpecificNode == "selectedHierarchyTreeRootNode") {
 			hideSelectHierarchyDialog();
 			//alert("currentHierarchy => " + currentHierarchy + " currentDomain_Hierarchy => " + currentDomain_Hierarchy);
			HierarchyHandler.getRootHierarchyTree(currentHierarchy, currentDomain_Hierarchy, selectHierarchySearchResultsCB); 		
  	} else if (selectedRootOrSpecificNode == "selectedSpecificHierarchyTreeNode") {
    	if(!selected) {
    		alert(getMessageForI18N("select_one_record_from_the_results"));
    	} 	else{
				hideSelectHierarchyDialog();
				HierarchyHandler.getHierarchyTree(selectedNodeId, selectedRecordEUID, selectHierarchySearchResultsCB);
    	}
	  }
}
	
function selectHierarchySearchResultsCB(data) {
	 //alert("display hierarchy tree ...");
    hierarchy_initMainTree(data);
    hierarchy_initRearrangeTree();
}
	
function loadSelectedHierarchyDefAttributes(name, domain) {
	HierarchyDefHandler.getHierarchyDefByName(name, domain, loadSelectedHierarchyDefAttributesCB);
}

function loadSelectedHierarchyDefAttributesCB(data) {
	var startDate =(getBoolean(data.startDate));
  var endDate =(getBoolean(data.endDate));
  var purgeDate =(getBoolean(data.purgeDate));
  var CustomrowCount = 0;
  var PredefinedrowCount = 0;  
  hierarchyDefsCache[data.name + data.domain] = data;
  dwr.util.removeAllRows("hierarchy_select_custom_attributes");
  dwr.util.removeAllRows("hierarchy_select_predefined_attributes");
  if (data.extendedAttributes.length>0 ){
  	var searchableCustomAttributes = [] ;
    for(i=0; i<data.extendedAttributes.length; i++) {
    	if( getBoolean(data.extendedAttributes[i].searchable) ) {
       	searchableCustomAttributes.push(data.extendedAttributes[i]);
      }
    }
    createCustomAttributesSection("hierarchy_select_custom_attributes", searchableCustomAttributes, "select_custom", false, true);
    displayDiv("select_hierarchy_custom_atrributes", true);
  } else {
  	displayDiv("select_hierarchy_custom_atrributes", false);
  }
       
  if (startDate==true || endDate==true || purgeDate == true ){ 
  	createPredefinedAttributesSection ("hierarchy_select_predefined_attributes", data, "select_predefined", false);
    displayDiv("select_hierarchy_predefined_atrributes", true);
  } else {
  	displayDiv("select_hierarchy_predefined_atrributes", false);
  }	
}

function initializeAddHierarchyNode() {
	isAddHierarchyNodeDialogLoaded = true;
	var selectedDomain = currentDomain_Hierarchy;
	var selectedHierarchy = currentHierarchy;
	RelationshipDefHandler.getDomainSearchCriteria(selectedDomain, initializeAddHierarchyNodeCB);
}

function initializeAddHierarchyNodeCB(data) {
  dwr.util.removeAllOptions("add_hierarchy_node_searchtypes"); 
  for (var searchType in data)  {
    var opt =  document.createElement("option");
    opt.text = searchType;
    opt.value = searchType;
     opt.title = searchType;
    document.getElementById('add_hierarchy_node_searchtypes').options.add(opt);
  }	
  addHierarchyNodeSearchFields('add_hierarchy_node_searchtypes');
}

function addHierarchyNode() {
	var selectedDomain = currentDomain_Hierarchy;
	var selectedHierarchyDef = currentHierarchy;
  var selectedRecordEUID = null;
  var selected = 0;
  var chkBoxes = document.getElementsByName("addHierarchyNodeDomainSearchResultsRadio");
  for(i=0; i<chkBoxes.length; i++) {
  	if(chkBoxes[i].checked) {
    	selectedRecordEUID = chkBoxes[i].value;
			selected++;
    }
  }    
  var selectedSearchType = document.getElementById("add_hierarchy_node_searchtypes").value;
  var hierarchyDefObj = hierarchyDefsCache[selectedHierarchyDef + selectedDomain];
  currentHierarchyDefId = hierarchyDefObj.id;
  
  for(i = 0; i < hierarchyDefObj.extendedAttributes.length; i++) {
  	var attributeName = hierarchyDefObj.extendedAttributes[i].name;
    var attributeId = document.getElementById("add_hierarchy_node_select_custom_" + attributeName).value;
    if (attributeId == null) {
    	continue;	
    }
    var attributeValue =  document.getElementById("add_hierarchy_node_select_custom_" + attributeName).value;
    var attributeType = hierarchyDefObj.extendedAttributes[i].dataType;
    if (!isValidCustomAttribute(attributeType, attributeValue)) {
    	alert(attributeValue + " " + getMessageForI18N("isnotavalidvaluefor")+ " " + attributeName + " " + getMessageForI18N("attribute")+ " " +getMessageForI18N("attributeTypeFor")+ " " +tempAttr.name  + " " +getMessageForI18N("is")+ " " +"'"+attributeType+"'");
      attributeId.focus(); 
      return;
    }
    var byRangeObj = document.getElementById("select_custom_" + attributeName + "_TO");
    var byRangeName = "";
    var byRangeValue = "";
    if (byRangeObj != null){
    	byRangeName = byRangeObj.name;
      byRangeValue = byRangeObj.value; 
      if (!isValidCustomAttribute(attributeType, byRangeObj.value)) {
      	alert(byRangeValue + " " + getMessageForI18N("isnotavalidvaluefor")+ " " + byRangeName + " " + getMessageForI18N("attribute")+ " " +getMessageForI18N("attributeTypeFor")+ " " +byRangeName + " " +getMessageForI18N("is")+ " " +"'"+attributeType+"'");
        byRangeObj.focus(); 
        return;
      }
    }         
  }   
  
  var searchCustomAttributes = []; 
  for(i=0; i <hierarchyDefObj.extendedAttributes.length; i++) {
      var customAttributeName = hierarchyDefObj.extendedAttributes[i].name;
      var customAttributeId = document.getElementById("add_hierarchy_node_select_custom_" + customAttributeName);
      if(customAttributeId == null) {
      	continue;
      }
      var customAttributeValue =  document.getElementById("add_hierarchy_node_select_custom_" + customAttributeName).value;
      var customAttributeType = hierarchyDefObj.extendedAttributes[i].dataType;
      if (!isValidCustomAttribute(customAttributeType, customAttributeValue) ) {
          alert(customAttributeValue + " " + getMessageForI18N("isnotavalidvaluefor") + " " + customAttributeName + " " + getMessageForI18N("attribute")
           			+ " " + getMessageForI18N("attributeTypeFor") + " " + customAttributeName + " " + getMessageForI18N("is") + " " + "'" + customAttributeType + "'");
          customAttributeId.focus();
          return;
      }
      if(!isEmpty(customAttributeValue)) {
      	isSelectHierarchyDefAttributeCriteriaApplied = true;
      }
      searchCustomAttributes.push({"name":customAttributeName, "value":customAttributeValue});
  }
    
  //alert("searchHierarchyNodes - search predefined attributes");
  var startDateField = document.getElementById('add_hierarchy_node_select_predefined_startDate');
  var endDateField = document.getElementById('add_hierarchy_node_select_predefined_endDate');
  var purgeDateField = document.getElementById('add_hierarchy_node_select_predefined_purgeDate');
  var startDate,endDate,purgeDate;
  if(startDateField != null){
  	startDate =  document.getElementById('add_hierarchy_node_select_predefined_startDate').value;
    if(!isEmpty(startDate)) isSelectHierarchyDefAttributeCriteriaApplied = true;
  }
  if(endDateField != null) {
  	endDate =  document.getElementById('add_hierarchy_node_select_predefined_endDate').value;
    if(!isEmpty(endDate)) isSelectHierarchyDefAttributeCriteriaApplied = true;
  }
  if(purgeDateField != null) {
  	purgeDate =  document.getElementById('add_hierarchy_node_select_predefined_purgeDate').value;
    if(!isEmpty(purgeDate)) isSelectHierarchyDefAttributeCriteriaApplied = true;
  }
  
  //HierarchyNodeRecord: id,EUID,parentId,parentEUID,startDate,endDate,purgeDate, attributes
  var hierarchyNodeRecord = {"id":"0", "EUID":selectedRecordEUID, "parentId":selectedHierarchyNodeId, "parentEUID":selectedHierarchyNodeEUID, "hierarchyDefId":currentHierarchyDefId, "name":currentHierarchy, "domain":currentDomain_Hierarchy};
  hierarchyNodeRecord.startDate = startDate;
  hierarchyNodeRecord.endDate = endDate;
  hierarchyNodeRecord.purgeDate = purgeDate;
  hierarchyNodeRecord.attributes = searchCustomAttributes;
 
  if(selected == 0) {
  	alert(getMessageForI18N("select_one_record_from_the_results"));
  } else {
    //alert("addHierarchyNode => selectedDomai-" + selectedDomain + " selectedRecordEUID-" + selectedRecordEUID + " selectedHierarchyNodeEUID-" + selectedHierarchyNodeEUID);
  	HierarchyHandler.addHierarchyNode(hierarchyNodeRecord, addHierarchyNodeCB);
		hideAddHierarchyNodeDialog();
  }
}

function addHierarchyNodeCB(data) {
	//refresh the hierarchy tree
	alert("hierarchy node " + data + " added");	
  if (selectedRootOrSpecificNode == "selectedHierarchyTreeRootNode") {
 		//alert("currentHierarchy => " + currentHierarchy + " currentDomain_Hierarchy => " + currentDomain_Hierarchy);
		HierarchyHandler.getRootHierarchyTree(currentHierarchy, currentDomain_Hierarchy, selectHierarchySearchResultsCB); 		
  } else if (selectedRootOrSpecificNode == "selectedSpecificHierarchyTreeNode") {
  	if (selectedNodeId != null && selectedRecordEUID != null) {
			HierarchyHandler.getHierarchyTree(selectedNodeId, selectedRecordEUID, selectHierarchySearchResultsCB);
		}
	}	
}

function hideAddHierarchyNodeDialog () {
		var addHierarchyNode = dijit.byId('add_hierarchy_node');
		if (addHierarchyNode != null) {
    	addHierarchyNode.hide();
  	}
}

function showAddHierarchyNodeDialog () {
		var addHierarchyNode = dijit.byId('add_hierarchy_node');
		if (addHierarchyNode != null) {
    	addHierarchyNode.show();
  	}
  	if (isAddHierarchyNodeDialogLoaded) {
  		initializeAddHierarchyNode();
  	}
}

function addHierarchyNodeSearchFields(searchTypeId){
    var searchType = document.getElementById(searchTypeId).value;
    RelationshipDefHandler.getSearchTypeCriteria(currentDomain_Hierarchy, searchType, showAddHierarchyNodeSearchFields);
}

function showAddHierarchyNodeSearchFields(data){
		dwr.util.removeAllRows("add_hierarchy_node_searchfields");
  	var count = 0;
		var guiTypeStr;
  	// create a hidden field, put the querybuilder value in it. hidden field name=selectHierarchyQueryBuilder.
  	hiddenField = document.createElement("input");
  	hiddenField.type = "hidden";
    hiddenField.id = "addHierarchyNodeQueryBuilder";
    hiddenField.name = "addHierarchyNodeQueryBuilder";
    hiddenField.value = data.queryBuilder;
    document.getElementById('add_hierarchy_node_searchfields').appendChild(hiddenField);
    var fieldGroups = data.fieldGroups;
    for (var fieldGrp in fieldGroups)  {
			var descriptionRow = document.getElementById('add_hierarchy_node_searchfields').insertRow(count++);
			descriptionRow.insertCell(0);
			descriptionRow.cells[0].innerHTML = "<b>"+ fieldGrp + "</b>";
    	for(i=0; i<fieldGroups[fieldGrp].length; i++) {
      	fieldCfg = fieldGroups[fieldGrp][i];
        var row = document.getElementById('add_hierarchy_node_searchfields').insertRow(count++);
        row.insertCell(0);
        row.insertCell(1);
			 	guiTypeStr = fieldCfg.guiType;
			 	if(guiTypeStr.toLowerCase() == "textbox"){
        	var field;
          try{
          	field = document.createElement('<input type="text" name="addHierarchyNodeSearchFieldName" />');
          }catch(err){
          	field = document.createElement("input");
          }
          field.type="text";
          field.size = "20";
			 		field.maxlength = fieldCfg.maxLength;
          field.name="addHierarchyNodeSearchFieldName";
			 		field.title =  fieldCfg.displayName;
          field.domainFieldName = fieldCfg.displayName;
          field.fieldName = fieldCfg.name;          
          row.cells[0].innerHTML = fieldCfg.displayName;
          row.cells[1].appendChild(field);
			  }
      }
     } 	
	   document.getElementById("add_hierarchy_node_hierarchydef").innerHTML = currentHierarchy;      
     loadAddHierarchyDefAttributes(currentHierarchy, currentDomain_Hierarchy);
}

function loadAddHierarchyDefAttributes(name, domain) {
	HierarchyDefHandler.getHierarchyDefByName(name, domain, loadAddHierarchyDefAttributesCB);
}

function loadAddHierarchyDefAttributesCB(data) {
	var startDate =(getBoolean(data.startDate));
  var endDate =(getBoolean(data.endDate));
  var purgeDate =(getBoolean(data.purgeDate));
  var CustomrowCount = 0;
  var PredefinedrowCount = 0;  
 
  hierarchyDefsCache[data.name + data.domain] = data;
  dwr.util.removeAllRows("hierarchy_add_custom_attributes");
  dwr.util.removeAllRows("hierarchy_add_predefined_attributes");
  if (data.extendedAttributes.length>0 ){
  	var searchableCustomAttributes = [] ;
    for(i=0; i<data.extendedAttributes.length; i++) {
    	if( getBoolean(data.extendedAttributes[i].searchable) ) {
       	searchableCustomAttributes.push(data.extendedAttributes[i]);
      }
    }
    createCustomAttributesSection("add_hierarchy_node_custom_attributes_table", searchableCustomAttributes, "add_hierarchy_node_select_custom", false, true);
    displayDiv("add_hierarchy_node_custom_atrributes", true);
  } else {
  	displayDiv("add_hierarchy_node_custom_atrributes", false);
  }
       
  if (startDate==true || endDate==true || purgeDate == true ){ 
  	createPredefinedAttributesSection ("add_hierarchy_node_predefined_attributes_table", data, "add_hierarchy_node_select_predefined", false);
    displayDiv("add_hierarchy_node_predefined_atrributes", true);
  } else {
  	displayDiv("add_hierarchy_node_predefined_atrributes", false);
  }	
}

function addHierarchyNodeDomainSearch(){
  var selectedDomain = currentDomain_Hierarchy;
  var domainSearch = {name:selectedDomain}; 
  var addHierarchyNodeSearchFieldNames = document.getElementsByName('addHierarchyNodeSearchFieldName');
  domainSearch["type"] =  document.getElementById("add_hierarchy_node_searchtypes").value;
  var domainAttributes = [];
  for(i=0; i<addHierarchyNodeSearchFieldNames.length; i++){
  	var tempFieldName = addHierarchyNodeSearchFieldNames[i].domainFieldName;
    var tempFieldValue = addHierarchyNodeSearchFieldNames[i].value ;
    var tempMap = {} ;
    tempMap["name"] = tempFieldName;
    tempMap["value"] = tempFieldValue;
    domainAttributes.push( tempMap );
  }

	domainSearch.attributes = domainAttributes;
  DomainHandler.searchEnterprises (domainSearch, addHierarchyNodeDomainSearchCB);	
}

var addHierarchyNodeDomainSearchResultsCache = null; 
var addHierarchyNodeDomainSearchResultsLength = null;

function addHierarchyNodeDomainSearchCB(data){
    addHierarchyNodeDomainSearchResultsCache = []; // clear the cache array.
    for(i=0; i<data.length; i++) {
        addHierarchyNodeDomainSearchResultsCache.push(data[i]);
    }
    if(data == null) { 
       displayDiv("add_hierarchynode_domainsearch_results_failure", true);
       displayDiv("add_hierarchynode_domainsearch_results_success", false);
       displayDiv("add_hierarchynode_domainsearch_results_div", false);
    } else {
       displayDiv("add_hierarchynode_domainsearch_results_success", true);
       displayDiv("add_hierarchynode_domainsearch_results_failure", false);
       displayDiv("add_hierarchynode_domainsearch_results_div", true);
       
       addHierarchyNodeSearchResultsDisplay(1, intDomainSearchResultsItemsPerPage); 
       var addHierarchyNodePaginator = dijit.byId("add_hierarchynode_domainsearch_results_paginator");
       addHierarchyNodePaginator.currentPage = 1;
       addHierarchyNodePaginator.totalPages = Math.ceil(addHierarchyNodeDomainSearchResultsCache.length / intDomainSearchResultsItemsPerPage) ;
       addHierarchyNodePaginator.refresh(); 
       addHierarchyNodeDomainSearchResultsLength = document.getElementsByName("addHierarchyNodeDomainSearchResultsRadio").length;        
   }
   enableAddHierarchyNodeButton();
}

function enableAddHierarchyNodeButton(){
	if(addHierarchyNodeDomainSearchResultsLength > 0 ){
  	document.getElementById('addHierarchNodeButton').disabled = false;
  }
}

function addHierarchyNodeSearchResultsDisplayRefresh(currentPage){
	addHierarchyNodeSearchResultsDisplay(currentPage,intDomainSearchResultsItemsPerPage);
}

function addHierarchyNodeSearchResultsDisplay(currentPage,itemsPerPage){
	data = addHierarchyNodeDomainSearchResultsCache;
  selectedDomain = currentDomain_Hierarchy;
  dwr.util.removeAllRows("add_hierarchynode_domainsearch_results_table");
	
  var domainSearchFieldNames = document.getElementsByName('addHierarchyNodeSearchFieldName');
    
  //show only records that should go in current page.
  var resultsToShow = new Array();
  var itemsFrom = 0, itemsTo = 10;
  itemsFrom = (currentPage-1) * itemsPerPage;
  if(itemsFrom > data.length) {
  	itemsFrom = 0;
  }
  itemsTo = itemsFrom + itemsPerPage;
    
  if(itemsTo > data.length) {
  	itemsTo = data.length;
  }
  
  for(i=itemsFrom; i<itemsTo; i++) {
  	resultsToShow.push(data[i]);
  }
  
  var columnCount = 0;
  for(i =0; i<resultsToShow.length; i++) {
    if(i==0){
      columnCount = 0;
      var header = document.getElementById('add_hierarchynode_domainsearch_results_table').insertRow(i);
      header.className = "header";
      header.insertCell(columnCount++);
      for(j=0; j<resultsToShow[i].attributes.length; j++) {
      	var fieldName = resultsToShow[i].attributes[j].name;
        var displayName = containsFieldName(fieldName, domainSearchFieldNames);
      	if (displayName == null) {
      		continue;
      	}	
      	header.insertCell(columnCount);
      	header.cells[columnCount].className = "label";
      	header.cells[columnCount].innerHTML  = displayName;
      	columnCount++;
     	}
    }
    columnCount = 0;
    var dataRow = document.getElementById('add_hierarchynode_domainsearch_results_table').insertRow(i+1);
    dataRow.insertCell(columnCount);
    var chkbox ; 
    try{
    	chkbox = document.createElement('<input type="radio" name="addHierarchyNodeDomainSearchResultsRadio" />');
    }catch(err){
    	chkbox = document.createElement("input");
    }
    chkbox.type = "radio";
    chkbox.name = "addHierarchyNodeDomainSearchResultsRadio";
    chkbox.value = resultsToShow[i].EUID;
    dataRow.cells[columnCount].appendChild (chkbox);
    columnCount++;
    for(j=0; j<resultsToShow[i].attributes.length; j++) {
      var fieldName = resultsToShow[i].attributes[j].name;
      var displayName = containsFieldName(fieldName, domainSearchFieldNames);   	
    	if(displayName == null) {
      	continue;
      }
      dataRow.insertCell(columnCount);
      dataRow.cells[columnCount].innerHTML = resultsToShow[i].attributes[j].value;
      columnCount++;
    }
	}
}



// function to show right section details...
function hierarchy_ShowDetails() {

    var mainTree_targetRecordDetailsPrefix = "target_hierarchy";
        
    if (hierarchy_Selected_Record != null) {
        var hierarchyDomain = currentDomain_Hierarchy;
        var targetPane = dijit.byId(mainTree_targetRecordDetailsPrefix+"RecordDetailsTitlePane"); 
        targetPane.attr("title", hierarchy_Selected_Record.name);

        if(addHierarchyNodeDomainSearchResultsCache != null) {
           targetPane.toggleSummaryIcon(); // revert back the view to summary
        }
        
        if (summaryFieldsCache_hierarchy[hierarchyDomain] == null) {
            DomainScreenHandler.getSummaryFields(hierarchyDomain, { callback:function(dataFromServer) {
                summaryFieldsCache_hierarchy[hierarchyDomain] = dataFromServer; }
            });
        }
     
        // Load fields to display record details for source 
        if (detailFieldsCache_hierarchy[hierarchyDomain] == null) {
            DomainScreenHandler.getDetailFields(hierarchyDomain, { callback:function(dataFromServer) {
                detailFieldsCache_hierarchy[hierarchyDomain] = dataFromServer; }
            });
        }

        // To get record derails for the source domain
        var recordSummary = { name:hierarchyDomain, EUID:hierarchy_Selected_Record.EUID}; 
        DomainHandler.getEnterprise(recordSummary, { callback:function(dataFromServer) {
            polulateHierarchySourceDetailsCB(dataFromServer, mainTree_targetRecordDetailsPrefix); }
        });
        
        // Get hierarchy attribute
        HierarchyHandler.getHierarchyNode(hierarchy_Selected_Record.id, { callback:function(dataFromServer) {
            populate_hierarchy_attribute_CB(dataFromServer, "mainTree"); }
        });
        
        displayDiv("hierarchy_TargetRecordDetails", true);
        displayDiv("hierarchy_editAttributes", true);
    }
    
}

function hierarchy_rearrangeTree_ShowDetails() {

    var rearrange_sourceRecordDetailsPrefix = "rearrangeSource_hierarchy";
        
    if (hierarchy_rearrange_Selected_Record != null) {
        var hierarchyDomain = currentDomain_Hierarchy;
        var sourcePane = dijit.byId(rearrange_sourceRecordDetailsPrefix+"RecordDetailsTitlePane");
        sourcePane.attr("title", hierarchy_rearrange_Selected_Record.name);
        
        if(addHierarchyNodeDomainSearchResultsCache != null) {
           sourcePane.toggleSummaryIcon(); // revert back the view to summary
        }
        
        if (summaryFieldsCache_hierarchy[hierarchyDomain] == null) {
            DomainScreenHandler.getSummaryFields(hierarchyDomain, { callback:function(dataFromServer) {
                summaryFieldsCache_hierarchy[hierarchyDomain] = dataFromServer; }
            });
        }
     
        // Load fields to display record details for source 
        if (detailFieldsCache_hierarchy[hierarchyDomain] == null) {
            DomainScreenHandler.getDetailFields(hierarchyDomain, { callback:function(dataFromServer) {
                detailFieldsCache_hierarchy[hierarchyDomain] = dataFromServer; }
            });
        }
        
        displayDiv("hierarchy_Rearrange_SourceRecordDetails", true);
        
        // To get record derails for the source domain
        var recordSummary = { name:hierarchyDomain, EUID:hierarchy_rearrange_Selected_Record.EUID}; 
        DomainHandler.getEnterprise(recordSummary, { callback:function(dataFromServer) {
            polulateHierarchySourceDetailsCB(dataFromServer, rearrange_sourceRecordDetailsPrefix); }
        });
    }
    
}

function polulateHierarchySourceDetailsCB(data, sourceDetailsPrefix) {
    var summaryFieldCount = 0;
    var fieldName, fieldValue;
    var recordFieldRow,isSummaryField;
    // Populate source record details
    var sourceRecordDetails =  data.attributes;
    dwr.util.removeAllRows(sourceDetailsPrefix+"RecordInSummary");    
    dwr.util.removeAllRows(sourceDetailsPrefix+"RecordInDetail");
    summaryFieldCount = 0;

    for(i=0; i<sourceRecordDetails.length; i++) {
        fieldName = sourceRecordDetails[i].name;
        fieldValue = sourceRecordDetails[i].value;
        
        var displayName = getDisplayNameForField_hierarchy(data.name, fieldName);
        
        //alert("fieldName  "+fieldName +"   fieldValue  "+fieldValue +"    displayName  "+displayName);
        var sourceSummaryTable = document.getElementById(sourceDetailsPrefix+'RecordInSummary');
        var sourceDetailTable = document.getElementById(sourceDetailsPrefix+'RecordInDetail');

        recordFieldRow = sourceDetailTable.insertRow(i);
        recordFieldRow.insertCell(0);recordFieldRow.cells[0].className = "label";
        recordFieldRow.insertCell(1);recordFieldRow.cells[1].className = "data";
        recordFieldRow.cells[0].innerHTML = displayName+ ": ";
        recordFieldRow.cells[1].innerHTML = fieldValue;

        isSummaryField = isSummaryField_hierarchy(data.name, fieldName);
        if( isSummaryField ) {
          recordFieldRow= sourceSummaryTable.insertRow(summaryFieldCount);
          recordFieldRow.insertCell(0);recordFieldRow.cells[0].className = "label";
          recordFieldRow.insertCell(1);recordFieldRow.cells[1].className = "data";
          
          recordFieldRow.cells[0].innerHTML = displayName + ": ";
          recordFieldRow.cells[1].innerHTML = fieldValue;
          summaryFieldCount ++;
        }
    }
}

function getDisplayNameForField_hierarchy(domainName, fieldName) {
    var fieldGroups = detailFieldsCache_hierarchy[domainName].detailFieldGroups;
    var field = null;
    for (fg in fieldGroups) {
        var fieldGroup = fieldGroups[fg];
        if (fieldGroup[fieldName] != null) {
            field = fieldGroup[fieldName];
            break;
        }
    }
    
    if (field.displayName != null) {
        return field.displayName;
    }
    return fieldName;
}

function isSummaryField_hierarchy(domainName, fieldName) {
    var fieldGroups = summaryFieldsCache_hierarchy[domainName].summaryFieldGroups;
    for (fg in fieldGroups) {
        var fieldGroup = fieldGroups[fg];
        if (fieldGroup[fieldName] != null) {
            return true;
        }
    }

    return false;
}

function hierarchy_clearDetailsSection() {
    displayDiv("hierarchy_SourceRecordDetails", false);
    displayDiv("hierarchy_TargetRecordDetails", false);
    displayDiv("hierarchy_editAttributes", false);
}

function hierarchy_Rearrange_clearDetailsSection() {
    displayDiv("hierarchy_Rearrange_SourceRecordDetails", false);
}

function selectHirarchyRootOrSpecificNode() {
  var chkBoxes = document.getElementsByName("select_hierarchytree_root_or_specificnode");
  for(i=0; i<chkBoxes.length; i++) {
  	if(chkBoxes[i].checked) {
    	selectedRootOrSpecificNode = chkBoxes[i].value;
			break;
    }
  }    	
  if (selectedRootOrSpecificNode != null && selectedRootOrSpecificNode == "selectedHierarchyTreeRootNode") {
  	displayDiv("select_hierarchy_node_search_criteria", false);
  	document.getElementById("select_hierarchy_search_results_button").disabled = false;
  } else if (selectedRootOrSpecificNode != null && selectedRootOrSpecificNode == "selectedSpecificHierarchyTreeNode") {
  	displayDiv("select_hierarchy_node_search_criteria", true);
  }

}


function updateHierarchyAtttributes(hierarchyEditAttributesPrefix) {
    
    var updatedHierarchyNodeRecord = currentHierarchyNodeRecord;
    var updatedHierarchyNodeRecordCustomAttributes = [];
    
    var startDateField = document.getElementById(hierarchyEditAttributesPrefix+'_edit_predefined_startDate');
    var endDateField = document.getElementById(hierarchyEditAttributesPrefix+'_edit_predefined_endDate');
    var purgeDateField = document.getElementById(hierarchyEditAttributesPrefix+'_edit_predefined_purgeDate');
    var startDate,endDate,purgeDate;
    
    var hierarchyDefObj = hierarchyDefsCache[currentHierarchy + currentDomain_Hierarchy];
    if(hierarchyDefObj != null) {
        var hierarchyDefAttributes = hierarchyDefObj.extendedAttributes;
        // Get Predefined Required Fileds
        var startDateRequire = (getBoolean(hierarchyDefObj.startDateRequired));
        var endDateRequire = (getBoolean(hierarchyDefObj.endDateRequired));
        var purgeDateRequire = (getBoolean(hierarchyDefObj.purgeDateRequired));

        for(i =0; i < hierarchyDefAttributes.length; i++) {
          var attributeName = hierarchyDefAttributes[i].name;
          var attributeId = document.getElementById(hierarchyEditAttributesPrefix+"_edit_custom_" + hierarchyDefAttributes[i].name);
          var attributeValue =  document.getElementById(hierarchyEditAttributesPrefix+"_edit_custom_" + hierarchyDefAttributes[i].name).value;
          var attributeType = hierarchyDefAttributes[i].dataType;
          if(getBoolean(hierarchyDefAttributes[i].isRequired)) {
              if( isEmpty (attributeValue) ) {
                  alert(getMessageForI18N("enter_ValueFor") + " " + attributeName +getMessageForI18N("period"));
                  attributeId.focus();
                  return ;
              }
          }
          if( ! isValidCustomAttribute( attributeType, attributeValue) ) {
              alert(attributeValue + " " +getMessageForI18N("isnotavalidvaluefor")+ " " +attributeName + " " +getMessageForI18N("attribute")+ " " +getMessageForI18N("attributeTypeFor")+ " " +attributeName + " " +getMessageForI18N("is")+ " " +"'"+attributeType+"'");
              attributeId.focus();
              return;
          }

          updatedHierarchyNodeRecordCustomAttributes.push( {"name":attributeName, "value":attributeValue} );
         }


        if(startDateField != null)
         {   
               startDate =  document.getElementById(hierarchyEditAttributesPrefix+'_edit_predefined_startDate').value;
               if(startDateRequire == true){
                   if( isEmpty (startDate) ) {
                  alert(getMessageForI18N("enterValueFor_effectiveFrom"));
                  startDateField.focus();
                  return ;
               }
             }    
         }
         if(endDateField != null)
         {   
               endDate =  document.getElementById(hierarchyEditAttributesPrefix+'_edit_predefined_endDate').value;
               if(endDateRequire == true){
                   if( isEmpty (endDate) ) {
                  alert(getMessageForI18N("enterValueFor_effectiveTo"));
                  endDateField.focus();
                  return ;
                }
             }      
         }
         if(purgeDateField != null)
         {
               purgeDate =  document.getElementById(hierarchyEditAttributesPrefix+'_edit_predefined_purgeDate').value;
               if(purgeDateRequire == true){
                   if( isEmpty (purgeDate) ) {
                  alert(getMessageForI18N("enterValueFor_purgeDate"));
                  purgeDateField.focus();
                  return ;
                }
              }      
         }
    }
    
    updatedHierarchyNodeRecord.objectRecord = null;  
    updatedHierarchyNodeRecord.startDate = startDate;
    updatedHierarchyNodeRecord.endDate = endDate;
    updatedHierarchyNodeRecord.purgeDate = purgeDate;
    updatedHierarchyNodeRecord.attributes = updatedHierarchyNodeRecordCustomAttributes;
    HierarchyHandler.updateHierarchyNode(updatedHierarchyNodeRecord, updateHierarchyAtttributesCB);
}

function updateHierarchyAtttributesCB(data) {
    alert("Hierarchy Attributes Updated.");
}

function populate_hierarchy_attribute_CB(data, hierarchyEditAttributesPrefix) {
    currentHierarchyNodeRecord = data;
    populateHierarchyAttribute(hierarchyEditAttributesPrefix);
}

function populateHierarchyAttribute(hierarchyEditAttributesPrefix) {
    var hierarchyDefObj = hierarchyDefsCache[currentHierarchy + currentDomain_Hierarchy];
    
    if(hierarchyDefObj != null) {
        var startDate = getBoolean(hierarchyDefObj.startDate);
        var endDate = getBoolean(hierarchyDefObj.endDate);
        var purgeDate = getBoolean(hierarchyDefObj.purgeDate);
        var customAttributes = hierarchyDefObj.extendedAttributes;
        var recordCustomAttributes = currentHierarchyNodeRecord.attributes;
        var blnShowEditAttributesSection = false;
        if(recordCustomAttributes != null && recordCustomAttributes.length > 0) {
            createCustomAttributesSection (hierarchyEditAttributesPrefix+"_hierarchyEditCustomAttributesTable", customAttributes, hierarchyEditAttributesPrefix+"_edit_custom", true,false);
            populateCustomAttributesValues (customAttributes, recordCustomAttributes, hierarchyEditAttributesPrefix+"_edit_custom");
            displayDiv(hierarchyEditAttributesPrefix+"_hierarchyEditCustomAttributesDiv", true);
            blnShowEditAttributesSection = true;
        } else {
            displayDiv(hierarchyEditAttributesPrefix+"_hierarchyEditCustomAttributesDiv", false);
        }
        if(startDate==true || endDate==true || purgeDate == true ){ 
            createPredefinedAttributesSection (hierarchyEditAttributesPrefix+"_hierarchyEditPredefinedAttributesTable", hierarchyDefObj, hierarchyEditAttributesPrefix+"_edit_predefined", true);
            populatePredefinedAttributesValues (hierarchyDefObj, currentHierarchyNodeRecord, hierarchyEditAttributesPrefix+"_edit_predefined");
            displayDiv(hierarchyEditAttributesPrefix+"_hierarchyEditPredefinedAttributesDiv", true);
            blnShowEditAttributesSection = true;
        } else{
            displayDiv(hierarchyEditAttributesPrefix+"_hierarchyEditPredefinedAttributesDiv", false);
        }
        
        if(blnShowEditAttributesSection) {
            displayDiv(hierarchyEditAttributesPrefix+"_hierarchyEditAttributesDiv", true);
        }
        else displayDiv(hierarchyEditAttributesPrefix+"_hierarchyEditAttributesDiv", false);
    }
}


