<%-- 
    Document   : domain_criteria
    Created on : Jan 15, 2009, 11:10:45 AM
    Author     : cye
--%>

<div class="MainBox">
	<div class="Content">
  <table border="0"  width="100%">
	<tr>
  	<td class="generalTextBold"><f:message key="select_the_search_type" /></td>
  </tr>
  <tr>
  	<td>
    <select id="add_hierarchy_node_searchtypes" name="<f:message key='select_the_search_type' />" title="<f:message key='select_the_search_type' />" onchange="addHierarchyNodeSearchFields(this.id);">
    </select>
    </td>
  </tr>
  <tr><td>
  	<div>
		<form name="addHierarchyNodeSearchFieldsForm">
    	<table border="0">
      <tbody id="add_hierarchy_node_searchfields" class="DomainSearchField"></tbody>
      </table>
    </form>
    </div>
	</td></tr>
	<tr><td><img src="images/spacer.gif" height="5" width="1"></td></tr>
  <tr><td align="left" colspan="2">
  	<input type="button" onclick="addHierarchyNodeDomainSearch()" title="<f:message key='search_text' />" value="<f:message key='search_text' />"/> 
    <input type="button" onclick="document.addHierarchyNodeSearchFieldsForm.reset();" title="<f:message key='clear_text' />" value="<f:message key='clear_text' />" />
  </td></tr>	
  </table>
  <table>
  	<tr>
    <td class="generalText"><div id="add_hierarchynode_domainsearch_results_success" style="display:none;"><f:message key="select_record_text" /></div></td>
    <td class="generalText"><div id="add_hierarchynode_domainsearch_results_failure" style="display:none;"><f:message key='no_records_found' /></div></td>
    </tr>
    <tr><td>
    	<div id="add_hierarchynode_domainsearch_results_div" style="display:none;">
      <table border="0" class="DomainSearchResults" >
      <tbody id="add_hierarchynode_domainsearch_results_table"></tbody>
      </table>
      <div dojoType="dijit.Paginator" id="add_hierarchynode_domainsearch_results_paginator"
           totalPages="0" currentPage="0" navigationFunction="addHierarchyNodeSearchResultsDisplayRefresh"></div>
      </div>
    </td></tr>
  </table>  
  </div>
</div>
