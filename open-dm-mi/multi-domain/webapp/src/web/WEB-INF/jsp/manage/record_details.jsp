<%-- 
    Document   : record_details
    Created on : Nov 19, 2008, 10:10:45 AM
    Author     : Harish
--%>
<%@ include file="/WEB-INF/jsp/include.jsp" %>
<%@ page import="java.util.Date" %>
<link rel="stylesheet" type="text/css" href="css/manage.css" media="screen"/>
<%
String prefix = (String) request.getParameter("prefix");
Date d = new Date();

if(prefix == null) prefix = "pre" + d.getTime();
%>
<style type="text/css">
            @import "scripts/dijit/themes/mdwm/mdwm.css";
</style>
<script type='text/javascript' src='scripts/manage.js'></script>
<script type="text/javascript" src="scripts/dojo/dojo.js" djConfig="parseOnLoad:true, isDebug: false"></script>
<script type="text/javascript">
    dojo.require("dijit.Dialog");
    dojo.require("dijit.layout.ContentPane");
    dojo.require("dijit.dijit");
    dojo.require("dijit.TitlePane");
    dojo.require("dojo.parser");
    dojo.require("dijit.RecordDetailsTitlePane");
     
</script>

<div dojoType="dijit.RecordDetailsTitlePane" title="<f:message key="record_details" />" id ="<%=prefix%>RecordDetailsTitlePane" class="MainBox" 
        onSummaryClick="showRecordFullDetails('<%=prefix%>RecordSummaryDiv','<%=prefix%>RecordDetailDiv',false);" onDetailsClick="showRecordFullDetails('<%=prefix%>RecordSummaryDiv','<%=prefix%>RecordDetailDiv',true);" parseOnLoad="true">

    <!-- sumarry content -->
    <div class="Content" style="padding-left:5px;">
        <div id="<%=prefix%>RecordSummaryDiv" cellpadding="2">
            <table border="0" class="RecordDetails" id="<%=prefix%>RecordInSummary">
                <tr>
                    <td class="label">&nbsp;</td>
                    <td class="data">&nbsp;</td>
                </tr>
          </table>
      </div>
      <div id="<%=prefix%>RecordDetailDiv" class="RecordDetails" style="display:none;">
              <table border="0" class="RecordDetails" id="<%=prefix%>RecordInDetail">
              <tr>
                    <td class="label">&nbsp;</td>
                    <td class="data">&nbsp;</td>
                </tr>
           </table>
        </div>
    </div>
</div>
   
  
<script>

</script>  