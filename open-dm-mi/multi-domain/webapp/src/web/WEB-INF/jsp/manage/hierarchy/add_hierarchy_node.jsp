<%-- 
    Document   : add_hierarchy_node
    Created on : May 02, 2009, 10:10:45 AM
    Author     : cye
--%>

<%@ include file="/WEB-INF/jsp/include.jsp" %>
<link rel="stylesheet" type="text/css" href="css/manage.css" media="screen"/>
<link rel="stylesheet" type="text/css" href="css/style.css" media="screen"/>

<style type="text/css">
	@import "scripts/dijit/themes/mdwm/mdwm.css";
</style>

<script type='text/javascript' src='scripts/mdwm.js'></script>
<script type="text/javascript" src="scripts/dojo/dojo.js" djConfig="parseOnLoad:true, isDebug: false">
</script>
<script type="text/javascript">
  dojo.require("dijit.Dialog");
	dojo.require("dijit.layout.ContentPane");
  dojo.require("dijit.dijit");
  dojo.require("dijit.TitlePane");
  dojo.require("dijit.form.TextBox");
  dojo.require("dijit.form.DateTextBox");
  dojo.require("dijit.GenericTitlePane");
  dojo.require("dojo.parser");
</script>

<body class="mdwm">
	<table border="0" width="100%">
  <tr><td>
  	<div class="MainBox">
    <div class="TitleBar"><f:message key="search_for_records" /></div>
    <div class="Content">
			<%@ include file="/WEB-INF/jsp/manage/hierarchy/add_node_domain_criteria.jsp" %>
    </div>
    </div>    
	</td></tr>
  <tr><td>
  	<%@ include file="/WEB-INF/jsp/manage/hierarchy/add_node_attributes_criteria.jsp" %>
  </td></tr>
  <tr><td align="right">
  	<input type="button" id="addHierarchNodeButton" disabled = "true" onclick="addHierarchyNode()" title="<f:message key="add_text" />" value="<f:message key="add_text" />"/>
    <input type="button" onclick="hideAddHierarchyNodeDialog()" title="<f:message key="cancel_text" />" value="<f:message key="cancel_text" />" />
  </td></tr>
  </table>
</body>