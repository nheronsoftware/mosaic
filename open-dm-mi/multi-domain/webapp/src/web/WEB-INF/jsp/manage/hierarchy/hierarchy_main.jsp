<%-- 
    Document   : manage hierarchy main page.
    Created on : Jan 15, 2009
    Author     : Harish
--%>


<%@ include file="/WEB-INF/jsp/include.jsp" %>

<div dojoType="dijit.layout.BorderContainer" splitter="true" style="width:100%; height:100%;padding:0px;border:0px;">
    <div dojoType="dijit.layout.BorderContainer" region="top" gutters="false" splitter="true" 
            minSize="50"  style="height:4%;border:0px;background-color:#F9F9F0;" >
           <div dojoType="dijit.layout.ContentPane" region="left" style="width:100%;" class="generalText">
               <table>
                   <tr>
                        <td>
                            <select id="select_hierarchy_domain" name="Domain" title="<f:message key='domain_text'/>" onchange="setCurrentDomainHierarchy(this.id)">
                            </select>
                        </td>
                        <td>
                            <select id="load_select_hierarchies" name="Hierarchy" title="<f:message key='hierarchy_tab_text'/>"  style="width:150px">
                            </select>
                        </td>
                       <td><input type="button" value="<f:message key='select_text' />..." title="<f:message key='select_text' />" onclick="m_hierarchy_showSelectDialog();"></td>
                   </tr>
               </table>
           </div>
    </div>
    
           <hr>

    <div dojoType="dijit.layout.BorderContainer" region="center" gutters="false" splitter="true" 
            minSize="50" style="height:90%;border:0px;background-color:#F9F9F0;" >
       <div dojoType="dijit.layout.ContentPane" region="left" style="width:40%;padding:4px;" href="m_hierarchy_tree.htm" parseOnLoad="true" 
       			onLoad="hierarchy_resetView();">
            tree section
       </div>
    
       <div dojoType="dijit.layout.ContentPane" region="center" href="m_hierarchy_details_section.htm" parseOnLoad="true"   
            onLoad="hierarchy_clearDetailsSection();hierarchy_Rearrange_clearDetailsSection();" >
            details section
       </div>
    </div>
    
</div>


<div id="select_hierarchy" dojoType="dijit.Dialog" title="<f:message key='select_hierarchy_text' />" style="display:none;width:750px;height:500px;padding:0px;"
        href="m_hierarchy_select_hierarchy.htm" parseOnLoad="true" onLoad="initializeSelectHierarchy()">
</div>

<div id="add_hierarchy_node" dojoType="dijit.Dialog" title="<f:message key='add_hierarchy_node_text' />" style="display:none;width:750px;height:500px;padding:0px;"
     href="m_hierarchy_add_hierarchy_node.htm" parseOnLoad="true" onLoad="initializeAddHierarchyNode()">
</div>