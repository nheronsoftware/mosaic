
<%@ include file="/WEB-INF/jsp/include.jsp" %>


<div dojoType="dijit.layout.BorderContainer">

<div class="MainBox" dojoType="dijit.layout.ContentPane">
    <div class="TitleBar">Hierarchy Tree</div>
    <div class="Content" style="padding-left:10px;padding-right:5px;">

        <table cellspacing="0" cellpadding="0" border="0" width="100%">
            <tr>
                <div class="generalTextBold">Select a Record to view details </div>
            </tr>
            <tr>
              <td valign="top" id="hierarchyTree" width="100%">
                <!-- main tree -->
                <table width="100%">
                    <tr><td width="100%" align="left"  valign="top">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%" style="height:25px;">
                          <tr>
                            <td><a href="javascript:void(0);" onclick="hierarchy_mainTree_addOperation();" title="<f:message key="add_text" />..."><img id="imgHierarchyMainTreeAddButton" src="images/icons/add_button_faded.png" border="0"></a></td>
                            
                            <td><img src="images/spacer.gif" height="1" width="6"></td>
                            
                            <td><a href="javascript:void(0);" onclick="hierarchy_mainTree_moveOperation();" title="Move"><img id="imgHierarchyMainTreeMoveButton" src="images/icons/move-right_button_faded.png" border="0"></a></td>
                            
                            <td><img src="images/spacer.gif" height="1" width="6"></td>
                            
                            <td><a href="javascript:void(0);" onclick="hierarchy_mainTree_deleteOperation();" title="<f:message key="delete_text" />"><img id="imgHierarchyMainTreeDeleteButton" src="images/icons/delete_button_faded.png" border="0"></a></td>
                            
                            <td><img src="images/spacer.gif" height="1" width="6"></td>
                            
                            <td><a href="javascript:void(0);" onclick="hierarchy_mainTree_findOperation();" title="Find"><img id="imgHierarchyMainTreeFindButton" src="images/icons/find_button_faded.png" border="0"></a></td>
                            
                            <td width="100%" align="right"><input type="button" value="Rearrange &gt;&gt;" title="Show Rearrange tree" onclick="hierarchyShowRearrangeTree(this);" style="height:25px;"></td>
                          </tr>
                      </table>
                    </td></tr>
                    <tr><td width="100%" valign="top">
                        <div id="hierarchyMainTreeContainer" dojoType="dijit.layout.ContentPane"
                        style="height:500px;border:1px solid #000000;background-color:#ffffff;"></div>
                    </td></tr>
                </table>
                
              </td>
              <td>&nbsp;</td>
              <td valign="top" id="hierarchyRearrangeTree" style="display:none;" width="100%">
                <!-- rearrange tree -->
                <table width="100%" border="0">
                    <tr><td width="100%" align="left"  valign="top">
                        <table cellspacing="0" cellpadding="0" border="0" style="height:25px;">
                           <tr>
                            <td><a href="javascript:void(0);" onclick="hierarchy_rearrangeTree_moveOperation();" title="Move"><img id="imgHierarchyRearrangeTreeMoveButton" src="images/icons/move-left_button_faded.png" border="0"></a></td>
                            
                            <td><img src="images/spacer.gif" height="1" width="6"></td>
                            
                            <td><a href="javascript:void(0);" onclick="hierarchy_rearrangeTree_findOperation();" title="Find"><img id="imgHierarchyRearrangeTreeFindButton" src="images/icons/find_button_faded.png" border="0"></a></td>
                            
                          </tr>
                        </table>
                    </td></tr>
                    <tr><td width="100%" valign="top">
                        <div id="hierarchyRearrangeTreeContainer" dojoType="dijit.layout.ContentPane"
                        style="height:500px;border:1px solid #000000;background-color:#ffffff;"></div>
                    </td></tr>
                </table>                
                
              </td>
            </tr>
        </table>
        <br><br>
    </div>
    
    
</div>

</div>
