
<%-- 
    Document   : Main screen for Manage - By Record 
    Created on : Nov 18, 2008, 10:10:45 AM
    Author     : Harish
--%>
<%@ include file="/WEB-INF/jsp/include.jsp" %>

<div dojoType="dijit.layout.BorderContainer" splitter="true" style="width:100%; height:100%;padding:0px;border:0px;">
    <div dojoType="dijit.layout.BorderContainer" region="top" gutters="false" splitter="true" 
            minSize="50"  style="height:7%;border:0px;background-color:#F9F9F0;" >
           <div dojoType="dijit.layout.ContentPane" region="left" style="width:30%;" class="generalText">
               <table>
                   <tr>
                       <td>
                           <input type="radio" value="<f:message key='by_relationship' />" title="<f:message key='by_relationship' />" name="m_relationship"  onclick="changeViewToByRelationship('mRelatioshipTab');" > By Relationship</input>
                       </td>
                       <td>
                       </td>
                       <td>
                       </td>
                   </tr>
                   <tr>
                        <td>
                            <input type="radio" value="<f:message key='by_record' />" title="<f:message key='by_record' />" name="m_relationship" checked> By Record</input>
                        </td>
                        <td class="generalTextBold">
                            <select id="select_record_by_domain" name="select_record_by_domain" title="<f:message key="select_domain_text" />" onchange="loadDomainsForOverview();">
                            </select>
                        </td>
                        <td><input type="button" value="<f:message key='select_text' />..." title="<f:message key='select_text' />" onclick="showSelectDialog();"></td>
                   </tr>
               </table>
           </div>
           <div dojoType="dijit.layout.ContentPane" region="center" >
           	<div class="MainBox">
           		<div class="TitleBar" style="height:20px;"> Relationship Overview</div>	               
               	<table>
               	<tr>	
               	<td>	
               	<div id="relationship_def_overview" style="padding-bottom:10px;padding-left:40px;font-size:12px;font-weight:bold;">
               	</div>
                </td> 
  							<img class="ImgBounce" src="images/icons/relationship-both.png"/>
               	<td>
                </td>                
                <td>
                <div style="padding-top:10px;padding-left:30px;padding-bottom:10px">          			
              	  <select id="select_relationship_def_overview" name="select_relationship_def_overview">
                  </select>
                </div>
                </td>
                <td><input type="button" value="<f:message key='overview_text' />..." title="<f:message key='overview_text' />" onclick="showOverviewDialog();"></td>                
                </tr> 
                </table>
             </div>
           </div>

    </div>
    
    <hr>

    <div dojoType="dijit.layout.BorderContainer" region="center" gutters="false" splitter="true" 
            minSize="50" style="height:85%;border:0px;background-color:#F9F9F0;" >
        
       <div dojoType="dijit.layout.ContentPane" region="left" style="width:40%;padding:4px;" href="m_byrecord_tree_section.htm"  parseOnLoad="true" onLoad="byRecord_resetView();">
                   tree section

       </div>   
        
        <div dojoType="dijit.layout.ContentPane" region="center" href="m_byrecord_details_section.htm" parseOnLoad="true"   
            onLoad="byRecord_clearDetailsSection();byRecord_Rearrange_clearDetailsSection();" >
            details section

       </div>
    </div>
    
</div>


<div id="byrecord_select" dojoType="dijit.Dialog" title="<f:message key='select_record_text' />" style="display:none;width:750px;height:500px;padding:0px;"
        href="m_byrecord_select_record.htm" parseOnLoad="true"
        onLoad="initializeByRecordSelectDialog();">
</div>

<div id="byrecord_add" dojoType="dijit.Dialog" title="<f:message key='add_records_text' />"  style="display:none;width:750px;height:500px;padding:0px;"
        href="m_byrecord_add_relationship.htm" parseOnLoad="true" onLoad="initializeByRecordAddDialog();">
</div>

<div id="byrecord_relationship_def_overview" dojoType="dijit.Dialog" title="<f:message key='overview_relationships_text' />" style="display:none;width:400px;height:200px;padding:0px;">
  <table cellpadding="0" cellspacing="0" border="0">
  <tr>
  	<td><img src="images/spacer.gif" height="1" width="30"></td>
  	<td id="relationship_def_overview_source" class="mainLabel" style="font-weight: bold;"><f:message key="source_domain_text" /><f:message key="colon_symbol" />&nbsp;<f:message key="mandatory_symbol" />&nbsp;</td> 
  	<td><img src="images/spacer.gif" height="1" width="20"></td>
  	<td id="relationship_def_overview_target" class="mainLabel" style="font-weight: bold;"><f:message key="target_domain_text" /><f:message key="colon_symbol" />&nbsp;<f:message key="mandatory_symbol" />&nbsp;</td>
  </tr>  
  </table>
	<table width="100%">
	<tr>
  	<td>
  	<table cellspacing="0" cellpadding="0" border="0" width="100%" class="DefsListing"  >
    	<thead class="header">
      	<tr>                                      
          <th width="20%" valign="bottom" class="label"><f:message key="name_text" /></th>
          <th width="20%" valign="bottom" class="label"><f:message key="desctription_text" /></th>                                        
          <th width="10%" valign="bottom" class="label"><f:message key="direction_text" /></th>                                        
          <th width="25%" valign="bottom" class="label"><f:message key="attributes_text" /></th>
          <th width="10%">&nbsp;</th>                                        
        </tr>
      </thead>
      <tbody id="relationshipDefsOverviewList">
    </table>	
		</td>			
  </tr>		
  </table>	
</div>