<%-- 
    Document   : node_attributes_criteria
    Created on : Jan 16, 2008, 10:10:45 AM
    Author     : Harish, Narahari
--%>

<div class="MainBox">
  <div class="TitleBar"><f:message key="node_attribute_text" /></div>
  <div class="Content">
  <table border="0" width="100%">
    <tr><td class="generalTextBold" colspan="3">
    	<table border="0">
      	<tr><td class="generalTextBold">&nbsp;<f:message key="selected_hierarchydef_text" /><f:message key="colon_symbol" />
            </td>
            <td class="generalTextBold"><div id="selected_hierarchydef"></div>
      		  </td>
      	</tr>
      </table>
    </td></tr>
    <tr><td><img src="images/spacer.gif" height="3" width="1"></td></tr>
    <tr><td>
    	<!--        	
    	<div dojoType="dijit.GenericTitlePane"  title="<f:message key='search_criteria_for_hierarchy_attributes' />" class="MainBox" jsId="pane1">
      -->
      <div class="Content">
      	<table border="0" width="100%">
        <tr><td>
            <div class="HierarchyAttributes" id="select_hierarchy_custom_atrributes" style="display:none;">
            	<table border="0" width="100%" >
              	<tr><td class="Heading" colspan="5"><f:message key="custom_attributes" /></td></tr>
                <tr><td class="Heading" colspan="5">
                	<table border="0"><tbody id="hierarchy_select_custom_attributes"></tbody></table>
                </td></tr>
              </table>
            </div>
        </td></tr>
        <tr><td>
            <div class="HierarchyAttributes" id="select_hierarchy_predefined_atrributes" style="display:none;">
            <table border="0" width="100%">
							<tr><td class="Heading"><f:message key="predefined_attributes" /></td></tr>
              <tr><td class="label">
              	<table border="0" >
                <tbody id="hierarchy_select_predefined_attributes"></tbody>
                </table>
              </td></tr>
            </table>
            </div>
        </td></tr>
      	</table>
      <!--	
      </div>
      -->
    	</div>        	
    </td></tr>    	        
	</table>
</div>
</div>
