
<div dojoType="dijit.layout.BorderContainer">

  <!-- Details pane -->
  <div dojoType="dijit.layout.ContentPane" region="top" splitter="true" style="width:98%; height:70%;padding:0px;border:0px;">

        <div dojoType="dijit.layout.BorderContainer" splitter="false" style="width:100%; height:100%;padding:0px;border: 1px solid #D1C8A9;">

           <div dojoType="dijit.layout.ContentPane" region="top" minSize="30" maxSize="30" style="height:24px;padding:0px;border:0px;" >
                <div dojoType="dijit.layout.ContentPane"  class="DetailsTitleBar">&nbsp;Hierarchy Record Details</div>
           </div>

           <!-- Record Details -->
           <div dojoType="dijit.layout.ContentPane" region="center" gutters="true" minSize="100" splitter="false" style="width:98%;height:100%;border:0px;background-color:#F9F9F0;">
                <div dojoType="dijit.layout.ContentPane" id="hierarchy_SourceRecordDetails" href="m_record_details.htm?prefix=source_hierarchy" parseOnLoad="true" >
                </div>
                <div style="padding:4px;"></div>
                <div dojoType="dijit.layout.ContentPane" id="hierarchy_TargetRecordDetails" href="m_record_details.htm?prefix=target_hierarchy" parseOnLoad="true" >
                </div>
                <div style="padding:4px;"></div>
                <div dojoType="dijit.layout.ContentPane" id="hierarchy_editAttributes" href="m_hierarchy_edit_attributes.htm?prefix=mainTree" parseOnLoad="true" >
                </div>
           </div>
         
                </div>

                </div>

  <!-- Details-Rearrange pane -->
  <div dojoType="dijit.layout.ContentPane" region="center" splitter="true" style="width:98%; height:30%;padding:0px;border:0px;">

    <div dojoType="dijit.layout.BorderContainer" splitter="false" style="width:100%; height:100%;padding:0px;border: 1px solid #D1C8A9;">

       <div dojoType="dijit.layout.ContentPane" region="top" minSize="30" maxSize="30" style="height:24px;padding:0px;border:0px;" >
            <div dojoType="dijit.layout.ContentPane" class="DetailsTitleBar">&nbsp;Domain Details</div>
           </div>           

       <div dojoType="dijit.layout.ContentPane" region="center" gutters="true" minSize="100" splitter="false" style="width:98%;height:100%;border:0px;background-color:#F9F9F0;">
            <div dojoType="dijit.layout.ContentPane" id="hierarchy_Rearrange_SourceRecordDetails" href="m_record_details.htm?prefix=rearrangeSource_hierarchy" parseOnLoad="true" >
               </div>
           </div>

        </div> 

  </div>

</div>