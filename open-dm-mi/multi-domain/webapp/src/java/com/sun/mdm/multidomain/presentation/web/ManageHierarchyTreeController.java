package com.sun.mdm.multidomain.presentation.web;

import java.io.IOException;
        
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;        
import javax.servlet.ServletException;

import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.ModelAndView;
        
public class ManageHierarchyTreeController implements Controller {

    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) 
        throws ServletException, IOException {    
        String value = "m_hierarchy_tree";
        return new ModelAndView("manage/hierarchy/hierarchy_tree", "m_hierarchy_tree", value);
    }    
}