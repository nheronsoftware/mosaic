/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.multidomain.mdb.service;

import javax.ejb.EJB;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.Message;
import javax.jms.MessageListener;

import com.sun.mdm.multidomain.ejb.service.MultiDomainServiceLocal;
import com.sun.mdm.multidomain.ejb.service.MultiDomainMetaServiceLocal;
import com.sun.mdm.multidomain.sync.SyncUtils;
import com.sun.mdm.multidomain.sync.SyncEvent;
import com.sun.mdm.multidomain.sync.SyncException;
import com.sun.mdm.multidomain.sync.SyncContainer;
import com.sun.mdm.multidomain.sync.SyncManager;
import com.sun.mdm.multidomain.parser.Plugin;
import com.sun.mdm.multidomain.parser.ParserException;

/**
 * DomainMessageDrivenBean class.
 * @author cye
 */
@MessageDriven(mappedName = "jms/PersonTopic", activationConfig =  {
        @ActivationConfigProperty(propertyName = "connectionFcatoryJndiName", propertyValue = "jms/PersonOutBoundSender"), 
        @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Topic"),
        @ActivationConfigProperty(propertyName = "subscriptionDurability", propertyValue = "Durable"),
        @ActivationConfigProperty(propertyName = "clientId", propertyValue = "PersonSyncMessageDrivenBean"),
        @ActivationConfigProperty(propertyName = "subscriptionName", propertyValue = "PersonSyncMessageDrivenBean")
    })
public class PersonMessageDrivenBean implements MessageListener {
    
    @EJB
    private MultiDomainMetaServiceLocal multiDomainMetaService;
   
    @EJB
    private MultiDomainServiceLocal multiDomainService;
   
    @Resource javax.ejb.MessageDrivenContext mdbContext;

    private SyncContainer relationshipSyncContainer;
    private String domain = "Person";
            
    public PersonMessageDrivenBean() {
    }

    @PostConstruct
    public void mdbCreate() {
        //relationshipSyncContainer = SyncManager.getInstance().getSyncContainer(domain, Plugin.PluginType.RELATIONSHIP);
    }
    
    public void onMessage(Message message) {
        //try {
            //SyncEvent event = SyncUtils.pasrseMessage(message);
            //event.setDomain(domain);
            //if (!relationshipSyncContainer.isInitialized()) {
            //    relationshipSyncContainer.setMultiDomainService(multiDomainService);
            //    relationshipSyncContainer.setMultiDomainMetaService(multiDomainMetaService);
            //    relationshipSyncContainer.start();
            //}
            //relationshipSyncContainer.deliverEvent(event);
        //} catch (ParserException pex) {
             //mdbContext.setRollbackOnly();
        //} catch (SyncException syncex) {
        //     mdbContext.setRollbackOnly();
        //}
    }
   
    @PreDestroy
    public void mdbRemove(){
        //try {
        //    relationshipSyncContainer.shutdown();
        //    SyncManager.getInstance().removeSyncContainer(domain, Plugin.PluginType.RELATIONSHIP);
        //} catch (SyncException syncex) {
        //}
    }
}
