#!/bin/bash
export BASE_DIR=/space/ohmpi
export NB_HOME=$BASE_DIR/netbeans-6.1
export FULL_PATH_TO_ORACLE_JDBC_JAR=$BASE_DIR/drivers/ojdbc14.jar
export FULL_PATH_TO_SQLSERVER_JDBC_JAR=$BASE_DIR/drivers/sqljdbc.jar
export GF_HOME=/space/ohmpi/glassfish
export MAVEN_HOME=/opt/apache-maven-2.0.9
export ANT_HOME=/opt/apache-ant-1.8.0
export RUN_JUNIT=
export HOME=/space/ohmpi
export JAVA_HOME=/usr/java
export OPEN_DM_MI_ROOT=$BASE_DIR/hudson_home/jobs/OHMPI-MDM/workspace/ohmpi/open-dm-mi
export FULL_PATH_TO_LOADER_RESOURCES_JAR=$OPEN_DM_MI_ROOT/loader/test/config/resources.jar
export FULL_PATH_TO_LOADER_MASTER_INDEX_CLIENT_JAR=$OPEN_DM_MI_ROOT/loader/test/config/master-index-client.jar

export PATH=$MAVEN_HOME/bin:$PATH
export ANT_OPTS=-Xmx512m
export MAVEN_OPTS=-Xmx512m

mvn install:install-file -DgroupId=oracle.jdbc -DartifactId=ojdbc14 -Dversion=10.1.0.2.0 -Dpackaging=jar -Dfile=$FULL_PATH_TO_ORACLE_JDBC_JAR
mvn install:install-file -DgroupId=msft.sqlserver.jdbc -DartifactId=sqljdbc -Dversion=1.0 -Dpackaging=jar -Dfile=$FULL_PATH_TO_SQLSERVER_JDBC_JAR
mvn install:install-file -DgroupId=javaee -DartifactId=javaee-api -Dversion=5 -Dpackaging=jar -Dfile=$GF_HOME/lib/javaee.jar

mvn install:install-file -DgroupId=loader.test -DartifactId=resources -Dversion=6.0 -Dpackaging=jar -Dfile=$FULL_PATH_TO_LOADER_RESOURCES_JAR
mvn install:install-file -DgroupId=loader.test -DartifactId=master-index-client -Dversion=6.0 -Dpackaging=jar -Dfile=$FULL_PATH_TO_LOADER_MASTER_INDEX_CLIENT_JAR
