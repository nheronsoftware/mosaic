#!/bin/bash
export BASE_DIR=/space/ohmpi
export NB_HOME=$BASE_DIR/netbeans-6.1
export FULL_PATH_TO_ORACLE_JDBC_JAR=$BASE_DIR/drivers/ojdbc14.jar
export FULL_PATH_TO_SQLSERVER_JDBC_JAR=$BASE_DIR/drivers/sqljdbc.jar
export GF_HOME=/space/ohmpi/glassfish
export MAVEN_HOME=/opt/apache-maven-2.0.9
export ANT_HOME=/opt/apache-ant-1.8.0
export RUN_JUNIT=
export HOME=/space/ohmpi
export JAVA_HOME=/usr/java

export PATH=$MAVEN_HOME/bin:$ANT_HOME/bin:$PATH
export ANT_OPTS=-Xmx512m
export MAVEN_OPTS=-Xmx512m

mvn clean install -Dmaven.test.skip=true
ant -f build-mural-nbm.xml make-ohmpi-nbm
cp ../ohmpi-nbm/*.nbm ../../builds
