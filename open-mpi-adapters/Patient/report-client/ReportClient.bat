REM NetGen Report Generator 
set JAVA_HOME=C:\Java\jdk1.6.0_24
set DEBUG=-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=5555
ECHO on
%JAVA_HOME%\bin\java -Dorg.omg.CORBA.ORBInitialHost=localhost -Dorg.omg.CORBA.ORBInitialPort=3100 -cp .\lib\*; com.sun.mdm.index.report.client.ReportClient %1 %2 %3 %4
