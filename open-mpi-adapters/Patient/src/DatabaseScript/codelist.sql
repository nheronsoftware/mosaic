
set sql_mode = '';
set autocommit = 0;
drop temporary table if exists tCodelist;
drop procedure if exists codelist;
delimiter $
create procedure codelist()
modifies SQL data
codelist: begin 
    declare v_objtype       varchar(1);
    declare v_code          varchar(40);
    declare v_descr         varchar(50);
    declare v_appl_id	    int;
    declare v_header_seq    int;
    declare v_detail_seq    int;
    declare v_date	        datetime;
    declare v_user          varchar(20);
    declare v_idx           int;
    declare v_no_row_found  int default 0;

    declare cur1 cursor for select * from tCodelist;

    declare continue handler for not found set v_no_row_found =1;
    declare exit handler for sqlexception 
    begin
	    select 'An SQLEXCEPTION error has occurred.' as Error;
	    rollback;
    end;

    create temporary table tCodelist (
	objtype varchar(1) not null, 
	code varchar(40) not null,
	descr varchar(50) not null);

    -- ****   ADDRTYPE   ****
    insert into tCodelist values('L', 'ADDRTYPE', 'Address Type');
    insert into tCodelist values('V', 'code', 'code description');

    -- ****   CITIZEN   ****
    insert into tCodelist values('L', 'CITIZEN', 'Citizenship');
    insert into tCodelist values('V', 'code', 'code description');

    -- ****   ETHNIC   ****
    insert into tCodelist values('L', 'ETHNIC', 'Ethnic');
    insert into tCodelist values('V', 'code', 'code description');

    -- ****   GENDER   ****
    insert into tCodelist values('L', 'GENDER', 'Gender');
    insert into tCodelist values('V', 'code', 'code description');

    -- ****   LANGUAGE   ****
    insert into tCodelist values('L', 'LANGUAGE', 'Language');
    insert into tCodelist values('V', 'code', 'code description');

    -- ****   MSTATUS   ****
    insert into tCodelist values('L', 'MSTATUS', 'Marital Status');
    insert into tCodelist values('V', 'code', 'code description');

    -- ****   NATIONAL   ****
    insert into tCodelist values('L', 'NATIONAL', 'Nationality');
    insert into tCodelist values('V', 'code', 'code description');

    -- ****   PHONTYPE   ****
    insert into tCodelist values('L', 'PHONTYPE', 'Phone Type');
    insert into tCodelist values('V', 'code', 'code description');

    -- ****   RACE   ****
    insert into tCodelist values('L', 'RACE', 'Race');
    insert into tCodelist values('V', 'code', 'code description');

    -- ****   RELIGION   ****
    insert into tCodelist values('L', 'RELIGION', 'Religion');
    insert into tCodelist values('V', 'code', 'code description');

    -- ****   SUFFIX   ****
    insert into tCodelist values('L', 'SUFFIX', 'Suffix');
    insert into tCodelist values('V', 'code', 'code description');

    -- ****   TITLE   ****
    insert into tCodelist values('L', 'TITLE', 'Title');
    insert into tCodelist values('V', 'code', 'code description');

    select now() into v_date;
    select user() into v_user;

    select APPL_ID into v_appl_id  from SBYN_APPL where CODE = 'NM';
    if v_no_row_found > 0 then
	    select 'ERROR: appl_id not found for NM.' as Error;
	    rollback;
	leave codelist;
    end if;

    delete from SBYN_COMMON_DETAIL where COMMON_HEADER_ID in 
        (select COMMON_HEADER_ID from SBYN_COMMON_HEADER
        where appl_id = v_appl_id);
		
    update SBYN_SEQ_TABLE set SEQ_COUNT = SEQ_COUNT + 1 
	where SEQ_NAME = 'SBYN_COMMON_HEADER';

    select SEQ_COUNT - 1 into v_header_seq from SBYN_SEQ_TABLE 
	where SEQ_NAME = 'SBYN_COMMON_HEADER';
	
	update SBYN_SEQ_TABLE set SEQ_COUNT = SEQ_COUNT + 1 
    where SEQ_NAME = 'SBYN_COMMON_DETAIL';

    select SEQ_COUNT - 1 into v_detail_seq from SBYN_SEQ_TABLE 
    where SEQ_NAME = 'SBYN_COMMON_DETAIL';

    open cur1;
    fetch cur1 into v_objtype, v_code, v_descr;
    while v_no_row_found = 0 do
        if v_objtype = 'L' then
            insert into SBYN_COMMON_HEADER (COMMON_HEADER_ID, APPL_ID, CODE, DESCR, READ_ONLY, MAX_INPUT_LEN, TYP_TABLE_CODE, CREATE_DATE, CREATE_USERID)
            values (v_header_seq, v_appl_id, v_code, v_descr , 'Y', 8, 'XX', v_date, v_user );
            set v_header_seq = v_header_seq + 1;
	    else
            insert into SBYN_COMMON_DETAIL (COMMON_DETAIL_ID, COMMON_HEADER_ID, CODE, DESCR, READ_ONLY, CREATE_DATE, CREATE_USERID)
            values (v_detail_seq, v_header_seq-1, v_code , v_descr, 'N', v_date, v_user );
            set v_detail_seq = v_detail_seq + 1;
        end if;
        fetch cur1 into v_objtype, v_code, v_descr;
    end while;
    close cur1;
    drop temporary table tCodelist;
    update SBYN_SEQ_TABLE set SEQ_COUNT = v_header_seq
    where SEQ_NAME = 'SBYN_COMMON_HEADER';
    update SBYN_SEQ_TABLE set SEQ_COUNT = v_detail_seq
    where SEQ_NAME = 'SBYN_COMMON_DETAIL';
    commit;
end; 
$
delimiter ;
call codelist();
drop procedure if exists codelist;
