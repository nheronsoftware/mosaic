/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */

package com.sun.mdm.index.webservice;

import java.util.*;
import com.sun.mdm.index.objects.exception.*;
import com.sun.mdm.index.objects.*;
import com.sun.mdm.index.objects.metadata.MetaDataService;
import java.text.SimpleDateFormat;
import java.text.ParsePosition;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public final class AddressBean implements ObjectBean
{
    public static final int version = 1;

    private AddressObject mAddressObject;
    private ClearFieldObject mClearFieldObj;
    private static java.text.SimpleDateFormat mDateFormat = new SimpleDateFormat(MetaDataService.getDateFormat());

    /**
     * Creates a new AddressBean instance.
     * @throws  ObjectException If creation fails. 
     */ 
    public AddressBean() throws ObjectException
    { 
       mAddressObject = new AddressObject();
    }
    
    /**
     * Creates a new AddressBean instance from a ClearFieldObject.
     */ 
    public AddressBean(ClearFieldObject clearFieldObj) throws ObjectException
    { 
       mAddressObject = new AddressObject();
       mClearFieldObj = clearFieldObj;
    }

    /**
     * Creates a new AddressBean instance from a AddressObject.
     */
    public AddressBean(AddressObject aAddressObject) throws ObjectException
    { 
       mAddressObject = aAddressObject;
    }
    
    /**
     * Creates a new AddressBean instance from 
     * a AddressObject and a ClearFieldObject.
     */
    public AddressBean(AddressObject aAddressObject,
      ClearFieldObject clearFieldObj) throws ObjectException
    { 
       mAddressObject = aAddressObject;
       mClearFieldObj = clearFieldObj;
    }
    
    /**
     * Getter for AddressId
     * @return a string value of AddressId
     */    
    public String getAddressId() throws ObjectException
    {
        try
        {
            int type = mAddressObject.pGetType("AddressId");
            Object value = mAddressObject.getValue("AddressId");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for AddressType
     * @return a string value of AddressType
     */    
    public String getAddressType() throws ObjectException
    {
        try
        {
            int type = mAddressObject.pGetType("AddressType");
            Object value = mAddressObject.getValue("AddressType");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for AddressLine1
     * @return a string value of AddressLine1
     */    
    public String getAddressLine1() throws ObjectException
    {
        try
        {
            int type = mAddressObject.pGetType("AddressLine1");
            Object value = mAddressObject.getValue("AddressLine1");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for AddressLine1_HouseNo
     * @return a string value of AddressLine1_HouseNo
     */    
    public String getAddressLine1_HouseNo() throws ObjectException
    {
        try
        {
            int type = mAddressObject.pGetType("AddressLine1_HouseNo");
            Object value = mAddressObject.getValue("AddressLine1_HouseNo");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for AddressLine1_StDir
     * @return a string value of AddressLine1_StDir
     */    
    public String getAddressLine1_StDir() throws ObjectException
    {
        try
        {
            int type = mAddressObject.pGetType("AddressLine1_StDir");
            Object value = mAddressObject.getValue("AddressLine1_StDir");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for AddressLine1_StName
     * @return a string value of AddressLine1_StName
     */    
    public String getAddressLine1_StName() throws ObjectException
    {
        try
        {
            int type = mAddressObject.pGetType("AddressLine1_StName");
            Object value = mAddressObject.getValue("AddressLine1_StName");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for AddressLine1_StPhon
     * @return a string value of AddressLine1_StPhon
     */    
    public String getAddressLine1_StPhon() throws ObjectException
    {
        try
        {
            int type = mAddressObject.pGetType("AddressLine1_StPhon");
            Object value = mAddressObject.getValue("AddressLine1_StPhon");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for AddressLine1_StType
     * @return a string value of AddressLine1_StType
     */    
    public String getAddressLine1_StType() throws ObjectException
    {
        try
        {
            int type = mAddressObject.pGetType("AddressLine1_StType");
            Object value = mAddressObject.getValue("AddressLine1_StType");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for AddressLine2
     * @return a string value of AddressLine2
     */    
    public String getAddressLine2() throws ObjectException
    {
        try
        {
            int type = mAddressObject.pGetType("AddressLine2");
            Object value = mAddressObject.getValue("AddressLine2");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for City
     * @return a string value of City
     */    
    public String getCity() throws ObjectException
    {
        try
        {
            int type = mAddressObject.pGetType("City");
            Object value = mAddressObject.getValue("City");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for State
     * @return a string value of State
     */    
    public String getState() throws ObjectException
    {
        try
        {
            int type = mAddressObject.pGetType("State");
            Object value = mAddressObject.getValue("State");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for County
     * @return a string value of County
     */    
    public String getCounty() throws ObjectException
    {
        try
        {
            int type = mAddressObject.pGetType("County");
            Object value = mAddressObject.getValue("County");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for Country
     * @return a string value of Country
     */    
    public String getCountry() throws ObjectException
    {
        try
        {
            int type = mAddressObject.pGetType("Country");
            Object value = mAddressObject.getValue("Country");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for Zip
     * @return a string value of Zip
     */    
    public String getZip() throws ObjectException
    {
        try
        {
            int type = mAddressObject.pGetType("Zip");
            Object value = mAddressObject.getValue("Zip");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for ZipExt
     * @return a string value of ZipExt
     */    
    public String getZipExt() throws ObjectException
    {
        try
        {
            int type = mAddressObject.pGetType("ZipExt");
            Object value = mAddressObject.getValue("ZipExt");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Setter for AddressId
     * @param string value of AddressId
     */ 
    public void setAddressId(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mAddressObject.isNullable("AddressId")) {
               mAddressObject.clearField("AddressId");
            } else {
               int type = mAddressObject.pGetType("AddressId");
               Object val = strToObj(value, type, "AddressId");
          
               mAddressObject.setValue("AddressId", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for AddressType
     * @param string value of AddressType
     */ 
    public void setAddressType(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mAddressObject.isNullable("AddressType")) {
               mAddressObject.clearField("AddressType");
            } else {
               int type = mAddressObject.pGetType("AddressType");
               Object val = strToObj(value, type, "AddressType");
          
               mAddressObject.setValue("AddressType", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for AddressLine1
     * @param string value of AddressLine1
     */ 
    public void setAddressLine1(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mAddressObject.isNullable("AddressLine1")) {
               mAddressObject.clearField("AddressLine1");
            } else {
               int type = mAddressObject.pGetType("AddressLine1");
               Object val = strToObj(value, type, "AddressLine1");
          
               mAddressObject.setValue("AddressLine1", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for AddressLine1_HouseNo
     * @param string value of AddressLine1_HouseNo
     */ 
    public void setAddressLine1_HouseNo(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mAddressObject.isNullable("AddressLine1_HouseNo")) {
               mAddressObject.clearField("AddressLine1_HouseNo");
            } else {
               int type = mAddressObject.pGetType("AddressLine1_HouseNo");
               Object val = strToObj(value, type, "AddressLine1_HouseNo");
          
               mAddressObject.setValue("AddressLine1_HouseNo", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for AddressLine1_StDir
     * @param string value of AddressLine1_StDir
     */ 
    public void setAddressLine1_StDir(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mAddressObject.isNullable("AddressLine1_StDir")) {
               mAddressObject.clearField("AddressLine1_StDir");
            } else {
               int type = mAddressObject.pGetType("AddressLine1_StDir");
               Object val = strToObj(value, type, "AddressLine1_StDir");
          
               mAddressObject.setValue("AddressLine1_StDir", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for AddressLine1_StName
     * @param string value of AddressLine1_StName
     */ 
    public void setAddressLine1_StName(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mAddressObject.isNullable("AddressLine1_StName")) {
               mAddressObject.clearField("AddressLine1_StName");
            } else {
               int type = mAddressObject.pGetType("AddressLine1_StName");
               Object val = strToObj(value, type, "AddressLine1_StName");
          
               mAddressObject.setValue("AddressLine1_StName", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for AddressLine1_StPhon
     * @param string value of AddressLine1_StPhon
     */ 
    public void setAddressLine1_StPhon(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mAddressObject.isNullable("AddressLine1_StPhon")) {
               mAddressObject.clearField("AddressLine1_StPhon");
            } else {
               int type = mAddressObject.pGetType("AddressLine1_StPhon");
               Object val = strToObj(value, type, "AddressLine1_StPhon");
          
               mAddressObject.setValue("AddressLine1_StPhon", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for AddressLine1_StType
     * @param string value of AddressLine1_StType
     */ 
    public void setAddressLine1_StType(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mAddressObject.isNullable("AddressLine1_StType")) {
               mAddressObject.clearField("AddressLine1_StType");
            } else {
               int type = mAddressObject.pGetType("AddressLine1_StType");
               Object val = strToObj(value, type, "AddressLine1_StType");
          
               mAddressObject.setValue("AddressLine1_StType", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for AddressLine2
     * @param string value of AddressLine2
     */ 
    public void setAddressLine2(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mAddressObject.isNullable("AddressLine2")) {
               mAddressObject.clearField("AddressLine2");
            } else {
               int type = mAddressObject.pGetType("AddressLine2");
               Object val = strToObj(value, type, "AddressLine2");
          
               mAddressObject.setValue("AddressLine2", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for City
     * @param string value of City
     */ 
    public void setCity(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mAddressObject.isNullable("City")) {
               mAddressObject.clearField("City");
            } else {
               int type = mAddressObject.pGetType("City");
               Object val = strToObj(value, type, "City");
          
               mAddressObject.setValue("City", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for State
     * @param string value of State
     */ 
    public void setState(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mAddressObject.isNullable("State")) {
               mAddressObject.clearField("State");
            } else {
               int type = mAddressObject.pGetType("State");
               Object val = strToObj(value, type, "State");
          
               mAddressObject.setValue("State", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for County
     * @param string value of County
     */ 
    public void setCounty(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mAddressObject.isNullable("County")) {
               mAddressObject.clearField("County");
            } else {
               int type = mAddressObject.pGetType("County");
               Object val = strToObj(value, type, "County");
          
               mAddressObject.setValue("County", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for Country
     * @param string value of Country
     */ 
    public void setCountry(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mAddressObject.isNullable("Country")) {
               mAddressObject.clearField("Country");
            } else {
               int type = mAddressObject.pGetType("Country");
               Object val = strToObj(value, type, "Country");
          
               mAddressObject.setValue("Country", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for Zip
     * @param string value of Zip
     */ 
    public void setZip(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mAddressObject.isNullable("Zip")) {
               mAddressObject.clearField("Zip");
            } else {
               int type = mAddressObject.pGetType("Zip");
               Object val = strToObj(value, type, "Zip");
          
               mAddressObject.setValue("Zip", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for ZipExt
     * @param string value of ZipExt
     */ 
    public void setZipExt(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mAddressObject.isNullable("ZipExt")) {
               mAddressObject.clearField("ZipExt");
            } else {
               int type = mAddressObject.pGetType("ZipExt");
               Object val = strToObj(value, type, "ZipExt");
          
               mAddressObject.setValue("ZipExt", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    
    public ObjectNode getObjectNode() {
        return mAddressObject;
    }

    /** 
     * Return AddressObject
     * @return AddressObject
     */ 
    public ObjectNode pGetObject() {
        return mAddressObject;
    }

    /** 
     * Getter for all children nodes
     * @return null because there is no child at the leaf
     */            
    public Collection pGetChildren() {            
         return null;
    }
    
    /** 
     * Getter for children of a specified type
     * @param type Type of children to retrieve
     * @return null because there is no child at the leaf
     */
    public Collection pGetChildren(String type) {
        return null;
    }

    /** 
     * Getter for child types
     * @return null because there is no child at the leaf
     */
    public ArrayList pGetChildTypes() {
        return null;
    }    

    /**
     * Count of all children
     * @return number of children
     */
    public int countChildren() {
        int count = 0;
        return count;
    }

    /**
     * Count of children of specified type
     * @param type of children to count
     * @return number of children of specified type
     */
    public int countChildren(String type) {
        int count = 0;
        return count;
    }
    
    /**
     * Delete itself from the parent and persist
     */
    public void delete() throws ObjectException {
        ObjectNode parent = mAddressObject.getParent();
        parent.deleteChild("Address", mAddressObject.pGetSuperKey()); 
    }
        
    // Find parent which is SystemObject    
    private SystemObject getParentSO() {
        ObjectNode obj = mAddressObject.getParent();
        
        while (obj != null) {
           if (obj instanceof SystemObject) {
              return (SystemObject) obj;
           } else {
              obj = obj.getParent();
           }
        }
        return (SystemObject) obj;
    }    
            
    static String objToString(Object value, int type) throws ObjectException {
        if (value == null) {
            return null;
        } else {
            if ( type == ObjectField.OBJECTMETA_STRING_TYPE) {
                return (String) value;
            }
            else if (type == ObjectField.OBJECTMETA_DATE_TYPE) {               
               return mDateFormat.format(value);              
            } else {
                return value.toString();
            }
        }
    }
    
    static Object strToObj(String str, int type, String fieldName) throws ObjectException {
        if (str == null || str.trim().length() == 0) {
            return null;
        } else if ( type == ObjectField.OBJECTMETA_STRING_TYPE) {
            return  str;
        } else if (type == ObjectField.OBJECTMETA_DATE_TYPE) {
            ParsePosition pos = new ParsePosition(0);
             Object ret = mDateFormat.parse(str, pos);   
            if ( ret == null) {
               throw new ObjectException("Invalid Date format of" + fieldName + ",value:" + str);
            }           
            return ret;             
        } else if (type == ObjectField.OBJECTMETA_INT_TYPE) {                
            return Integer.valueOf(str);              
        } else if (type == ObjectField.OBJECTMETA_FLOAT_TYPE) {                
            return Float.valueOf(str);              
        } else if (type == ObjectField.OBJECTMETA_LONG_TYPE) {                
            return Long.valueOf(str);             
        } else if (type == ObjectField.OBJECTMETA_BOOL_TYPE) {                
            return Boolean.valueOf(str);              
        } else if (type == ObjectField.OBJECTMETA_CHAR_TYPE) {                
            return (new Character(str.charAt(0)));                          
        } else {
            throw new ObjectException("Invalid type of" + fieldName + ",value:" + str);
        }
    }
}
