/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */

package com.sun.mdm.index.webservice;

import java.util.*;
import com.sun.mdm.index.objects.exception.*;
import com.sun.mdm.index.objects.*;
import com.sun.mdm.index.objects.metadata.MetaDataService;
import java.text.SimpleDateFormat;
import java.text.ParsePosition;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public final class MotherNameBean implements ObjectBean
{
    public static final int version = 1;

    private MotherNameObject mMotherNameObject;
    private ClearFieldObject mClearFieldObj;
    private static java.text.SimpleDateFormat mDateFormat = new SimpleDateFormat(MetaDataService.getDateFormat());

    /**
     * Creates a new MotherNameBean instance.
     * @throws  ObjectException If creation fails. 
     */ 
    public MotherNameBean() throws ObjectException
    { 
       mMotherNameObject = new MotherNameObject();
    }
    
    /**
     * Creates a new MotherNameBean instance from a ClearFieldObject.
     */ 
    public MotherNameBean(ClearFieldObject clearFieldObj) throws ObjectException
    { 
       mMotherNameObject = new MotherNameObject();
       mClearFieldObj = clearFieldObj;
    }

    /**
     * Creates a new MotherNameBean instance from a MotherNameObject.
     */
    public MotherNameBean(MotherNameObject aMotherNameObject) throws ObjectException
    { 
       mMotherNameObject = aMotherNameObject;
    }
    
    /**
     * Creates a new MotherNameBean instance from 
     * a MotherNameObject and a ClearFieldObject.
     */
    public MotherNameBean(MotherNameObject aMotherNameObject,
      ClearFieldObject clearFieldObj) throws ObjectException
    { 
       mMotherNameObject = aMotherNameObject;
       mClearFieldObj = clearFieldObj;
    }
    
    /**
     * Getter for MotherNameId
     * @return a string value of MotherNameId
     */    
    public String getMotherNameId() throws ObjectException
    {
        try
        {
            int type = mMotherNameObject.pGetType("MotherNameId");
            Object value = mMotherNameObject.getValue("MotherNameId");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for FirstName
     * @return a string value of FirstName
     */    
    public String getFirstName() throws ObjectException
    {
        try
        {
            int type = mMotherNameObject.pGetType("FirstName");
            Object value = mMotherNameObject.getValue("FirstName");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for FirstName_Std
     * @return a string value of FirstName_Std
     */    
    public String getFirstName_Std() throws ObjectException
    {
        try
        {
            int type = mMotherNameObject.pGetType("FirstName_Std");
            Object value = mMotherNameObject.getValue("FirstName_Std");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for FirstName_Phon
     * @return a string value of FirstName_Phon
     */    
    public String getFirstName_Phon() throws ObjectException
    {
        try
        {
            int type = mMotherNameObject.pGetType("FirstName_Phon");
            Object value = mMotherNameObject.getValue("FirstName_Phon");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for MiddleName
     * @return a string value of MiddleName
     */    
    public String getMiddleName() throws ObjectException
    {
        try
        {
            int type = mMotherNameObject.pGetType("MiddleName");
            Object value = mMotherNameObject.getValue("MiddleName");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for LastName
     * @return a string value of LastName
     */    
    public String getLastName() throws ObjectException
    {
        try
        {
            int type = mMotherNameObject.pGetType("LastName");
            Object value = mMotherNameObject.getValue("LastName");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for LastName_Phon
     * @return a string value of LastName_Phon
     */    
    public String getLastName_Phon() throws ObjectException
    {
        try
        {
            int type = mMotherNameObject.pGetType("LastName_Phon");
            Object value = mMotherNameObject.getValue("LastName_Phon");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for LastName_Std
     * @return a string value of LastName_Std
     */    
    public String getLastName_Std() throws ObjectException
    {
        try
        {
            int type = mMotherNameObject.pGetType("LastName_Std");
            Object value = mMotherNameObject.getValue("LastName_Std");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for Suffix
     * @return a string value of Suffix
     */    
    public String getSuffix() throws ObjectException
    {
        try
        {
            int type = mMotherNameObject.pGetType("Suffix");
            Object value = mMotherNameObject.getValue("Suffix");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for Prefix
     * @return a string value of Prefix
     */    
    public String getPrefix() throws ObjectException
    {
        try
        {
            int type = mMotherNameObject.pGetType("Prefix");
            Object value = mMotherNameObject.getValue("Prefix");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for Degree
     * @return a string value of Degree
     */    
    public String getDegree() throws ObjectException
    {
        try
        {
            int type = mMotherNameObject.pGetType("Degree");
            Object value = mMotherNameObject.getValue("Degree");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Setter for MotherNameId
     * @param string value of MotherNameId
     */ 
    public void setMotherNameId(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mMotherNameObject.isNullable("MotherNameId")) {
               mMotherNameObject.clearField("MotherNameId");
            } else {
               int type = mMotherNameObject.pGetType("MotherNameId");
               Object val = strToObj(value, type, "MotherNameId");
          
               mMotherNameObject.setValue("MotherNameId", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for FirstName
     * @param string value of FirstName
     */ 
    public void setFirstName(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mMotherNameObject.isNullable("FirstName")) {
               mMotherNameObject.clearField("FirstName");
            } else {
               int type = mMotherNameObject.pGetType("FirstName");
               Object val = strToObj(value, type, "FirstName");
          
               mMotherNameObject.setValue("FirstName", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for FirstName_Std
     * @param string value of FirstName_Std
     */ 
    public void setFirstName_Std(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mMotherNameObject.isNullable("FirstName_Std")) {
               mMotherNameObject.clearField("FirstName_Std");
            } else {
               int type = mMotherNameObject.pGetType("FirstName_Std");
               Object val = strToObj(value, type, "FirstName_Std");
          
               mMotherNameObject.setValue("FirstName_Std", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for FirstName_Phon
     * @param string value of FirstName_Phon
     */ 
    public void setFirstName_Phon(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mMotherNameObject.isNullable("FirstName_Phon")) {
               mMotherNameObject.clearField("FirstName_Phon");
            } else {
               int type = mMotherNameObject.pGetType("FirstName_Phon");
               Object val = strToObj(value, type, "FirstName_Phon");
          
               mMotherNameObject.setValue("FirstName_Phon", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for MiddleName
     * @param string value of MiddleName
     */ 
    public void setMiddleName(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mMotherNameObject.isNullable("MiddleName")) {
               mMotherNameObject.clearField("MiddleName");
            } else {
               int type = mMotherNameObject.pGetType("MiddleName");
               Object val = strToObj(value, type, "MiddleName");
          
               mMotherNameObject.setValue("MiddleName", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for LastName
     * @param string value of LastName
     */ 
    public void setLastName(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mMotherNameObject.isNullable("LastName")) {
               mMotherNameObject.clearField("LastName");
            } else {
               int type = mMotherNameObject.pGetType("LastName");
               Object val = strToObj(value, type, "LastName");
          
               mMotherNameObject.setValue("LastName", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for LastName_Phon
     * @param string value of LastName_Phon
     */ 
    public void setLastName_Phon(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mMotherNameObject.isNullable("LastName_Phon")) {
               mMotherNameObject.clearField("LastName_Phon");
            } else {
               int type = mMotherNameObject.pGetType("LastName_Phon");
               Object val = strToObj(value, type, "LastName_Phon");
          
               mMotherNameObject.setValue("LastName_Phon", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for LastName_Std
     * @param string value of LastName_Std
     */ 
    public void setLastName_Std(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mMotherNameObject.isNullable("LastName_Std")) {
               mMotherNameObject.clearField("LastName_Std");
            } else {
               int type = mMotherNameObject.pGetType("LastName_Std");
               Object val = strToObj(value, type, "LastName_Std");
          
               mMotherNameObject.setValue("LastName_Std", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for Suffix
     * @param string value of Suffix
     */ 
    public void setSuffix(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mMotherNameObject.isNullable("Suffix")) {
               mMotherNameObject.clearField("Suffix");
            } else {
               int type = mMotherNameObject.pGetType("Suffix");
               Object val = strToObj(value, type, "Suffix");
          
               mMotherNameObject.setValue("Suffix", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for Prefix
     * @param string value of Prefix
     */ 
    public void setPrefix(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mMotherNameObject.isNullable("Prefix")) {
               mMotherNameObject.clearField("Prefix");
            } else {
               int type = mMotherNameObject.pGetType("Prefix");
               Object val = strToObj(value, type, "Prefix");
          
               mMotherNameObject.setValue("Prefix", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for Degree
     * @param string value of Degree
     */ 
    public void setDegree(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mMotherNameObject.isNullable("Degree")) {
               mMotherNameObject.clearField("Degree");
            } else {
               int type = mMotherNameObject.pGetType("Degree");
               Object val = strToObj(value, type, "Degree");
          
               mMotherNameObject.setValue("Degree", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    
    public ObjectNode getObjectNode() {
        return mMotherNameObject;
    }

    /** 
     * Return MotherNameObject
     * @return MotherNameObject
     */ 
    public ObjectNode pGetObject() {
        return mMotherNameObject;
    }

    /** 
     * Getter for all children nodes
     * @return null because there is no child at the leaf
     */            
    public Collection pGetChildren() {            
         return null;
    }
    
    /** 
     * Getter for children of a specified type
     * @param type Type of children to retrieve
     * @return null because there is no child at the leaf
     */
    public Collection pGetChildren(String type) {
        return null;
    }

    /** 
     * Getter for child types
     * @return null because there is no child at the leaf
     */
    public ArrayList pGetChildTypes() {
        return null;
    }    

    /**
     * Count of all children
     * @return number of children
     */
    public int countChildren() {
        int count = 0;
        return count;
    }

    /**
     * Count of children of specified type
     * @param type of children to count
     * @return number of children of specified type
     */
    public int countChildren(String type) {
        int count = 0;
        return count;
    }
    
    /**
     * Delete itself from the parent and persist
     */
    public void delete() throws ObjectException {
        ObjectNode parent = mMotherNameObject.getParent();
        parent.deleteChild("MotherName", mMotherNameObject.pGetSuperKey()); 
    }
        
    // Find parent which is SystemObject    
    private SystemObject getParentSO() {
        ObjectNode obj = mMotherNameObject.getParent();
        
        while (obj != null) {
           if (obj instanceof SystemObject) {
              return (SystemObject) obj;
           } else {
              obj = obj.getParent();
           }
        }
        return (SystemObject) obj;
    }    
            
    static String objToString(Object value, int type) throws ObjectException {
        if (value == null) {
            return null;
        } else {
            if ( type == ObjectField.OBJECTMETA_STRING_TYPE) {
                return (String) value;
            }
            else if (type == ObjectField.OBJECTMETA_DATE_TYPE) {               
               return mDateFormat.format(value);              
            } else {
                return value.toString();
            }
        }
    }
    
    static Object strToObj(String str, int type, String fieldName) throws ObjectException {
        if (str == null || str.trim().length() == 0) {
            return null;
        } else if ( type == ObjectField.OBJECTMETA_STRING_TYPE) {
            return  str;
        } else if (type == ObjectField.OBJECTMETA_DATE_TYPE) {
            ParsePosition pos = new ParsePosition(0);
             Object ret = mDateFormat.parse(str, pos);   
            if ( ret == null) {
               throw new ObjectException("Invalid Date format of" + fieldName + ",value:" + str);
            }           
            return ret;             
        } else if (type == ObjectField.OBJECTMETA_INT_TYPE) {                
            return Integer.valueOf(str);              
        } else if (type == ObjectField.OBJECTMETA_FLOAT_TYPE) {                
            return Float.valueOf(str);              
        } else if (type == ObjectField.OBJECTMETA_LONG_TYPE) {                
            return Long.valueOf(str);             
        } else if (type == ObjectField.OBJECTMETA_BOOL_TYPE) {                
            return Boolean.valueOf(str);              
        } else if (type == ObjectField.OBJECTMETA_CHAR_TYPE) {                
            return (new Character(str.charAt(0)));                          
        } else {
            throw new ObjectException("Invalid type of" + fieldName + ",value:" + str);
        }
    }
}
