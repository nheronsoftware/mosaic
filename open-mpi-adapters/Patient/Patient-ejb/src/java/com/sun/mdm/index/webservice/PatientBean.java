/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.index.webservice;

import java.util.*;
import com.sun.mdm.index.objects.exception.*;
import com.sun.mdm.index.objects.*;
import com.sun.mdm.index.objects.metadata.MetaDataService;
import java.text.SimpleDateFormat;
import java.text.ParsePosition;
import com.sun.mdm.index.util.LogUtil;
import com.sun.mdm.index.util.Logger;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public final class PatientBean implements ObjectBean
{
    public static final int version = 1;
    
    private PatientObject mPatientObject; 
    private ClearFieldObject mClearFieldObj;
    private final Logger mLogger = LogUtil.getLogger(this.getClass().getName());


    private ArrayList mAlias = new ArrayList();
    private ArrayList mAddress = new ArrayList();
    private ArrayList mPhone = new ArrayList();
    private ArrayList mDriverLicense = new ArrayList();
    private ArrayList mAccount = new ArrayList();
    private ArrayList mMotherName = new ArrayList();
    private ArrayList mMotherId = new ArrayList();
    private ArrayList mIdentifier = new ArrayList();
    private ArrayList mLastUpdate = new ArrayList();

    /**
     * No argument constructor.
     */ 
    public PatientBean() throws ObjectException
    { 
       mPatientObject = new PatientObject();
    }
    
    /**
     * Creates a new PatientBean instance from a PatientObject.
     */ 
    public PatientBean(PatientObject aPatientObject) throws ObjectException
    { 
       mPatientObject = aPatientObject;
       int size = 0;
       Iterator iterator = null;
       Collection children = null;
       iterator = null;
       children = mPatientObject.pGetChildren("Alias");
       if (children != null) {
           size = children.size();
           iterator = children.iterator();
       }  
                               
       for (int i = 0; iterator != null && iterator.hasNext() ; i++) {
              AliasObject oNode = (AliasObject)iterator.next();
              AliasBean aAlias = new AliasBean(oNode, mClearFieldObj);            
              mAlias.add(aAlias);          
         }
   
       iterator = null;
       children = mPatientObject.pGetChildren("Address");
       if (children != null) {
           size = children.size();
           iterator = children.iterator();
       }  
                               
       for (int i = 0; iterator != null && iterator.hasNext() ; i++) {
              AddressObject oNode = (AddressObject)iterator.next();
              AddressBean aAddress = new AddressBean(oNode, mClearFieldObj);            
              mAddress.add(aAddress);          
         }
   
       iterator = null;
       children = mPatientObject.pGetChildren("Phone");
       if (children != null) {
           size = children.size();
           iterator = children.iterator();
       }  
                               
       for (int i = 0; iterator != null && iterator.hasNext() ; i++) {
              PhoneObject oNode = (PhoneObject)iterator.next();
              PhoneBean aPhone = new PhoneBean(oNode, mClearFieldObj);            
              mPhone.add(aPhone);          
         }
   
       iterator = null;
       children = mPatientObject.pGetChildren("DriverLicense");
       if (children != null) {
           size = children.size();
           iterator = children.iterator();
       }  
                               
       for (int i = 0; iterator != null && iterator.hasNext() ; i++) {
              DriverLicenseObject oNode = (DriverLicenseObject)iterator.next();
              DriverLicenseBean aDriverLicense = new DriverLicenseBean(oNode, mClearFieldObj);            
              mDriverLicense.add(aDriverLicense);          
         }
   
       iterator = null;
       children = mPatientObject.pGetChildren("Account");
       if (children != null) {
           size = children.size();
           iterator = children.iterator();
       }  
                               
       for (int i = 0; iterator != null && iterator.hasNext() ; i++) {
              AccountObject oNode = (AccountObject)iterator.next();
              AccountBean aAccount = new AccountBean(oNode, mClearFieldObj);            
              mAccount.add(aAccount);          
         }
   
       iterator = null;
       children = mPatientObject.pGetChildren("MotherName");
       if (children != null) {
           size = children.size();
           iterator = children.iterator();
       }  
                               
       for (int i = 0; iterator != null && iterator.hasNext() ; i++) {
              MotherNameObject oNode = (MotherNameObject)iterator.next();
              MotherNameBean aMotherName = new MotherNameBean(oNode, mClearFieldObj);            
              mMotherName.add(aMotherName);          
         }
   
       iterator = null;
       children = mPatientObject.pGetChildren("MotherId");
       if (children != null) {
           size = children.size();
           iterator = children.iterator();
       }  
                               
       for (int i = 0; iterator != null && iterator.hasNext() ; i++) {
              MotherIdObject oNode = (MotherIdObject)iterator.next();
              MotherIdBean aMotherId = new MotherIdBean(oNode, mClearFieldObj);            
              mMotherId.add(aMotherId);          
         }
   
       iterator = null;
       children = mPatientObject.pGetChildren("Identifier");
       if (children != null) {
           size = children.size();
           iterator = children.iterator();
       }  
                               
       for (int i = 0; iterator != null && iterator.hasNext() ; i++) {
              IdentifierObject oNode = (IdentifierObject)iterator.next();
              IdentifierBean aIdentifier = new IdentifierBean(oNode, mClearFieldObj);            
              mIdentifier.add(aIdentifier);          
         }
   
       iterator = null;
       children = mPatientObject.pGetChildren("LastUpdate");
       if (children != null) {
           size = children.size();
           iterator = children.iterator();
       }  
                               
       for (int i = 0; iterator != null && iterator.hasNext() ; i++) {
              LastUpdateObject oNode = (LastUpdateObject)iterator.next();
              LastUpdateBean aLastUpdate = new LastUpdateBean(oNode, mClearFieldObj);            
              mLastUpdate.add(aLastUpdate);          
         }
   
     }
    /**
     * Creates a new PatientBean instance from a PatientObject.
     * and a ClearFieldObject
     */
    public PatientBean(PatientObject aPatientObject,
       ClearFieldObject clearFieldObj) throws ObjectException
    { 
       mPatientObject = aPatientObject;
       mClearFieldObj = clearFieldObj;
       int size = 0;
       Iterator iterator = null;
       Collection children = null;
       iterator = null;
       children = mPatientObject.pGetChildren("Alias");
       if (children != null) {
           size = children.size();
           iterator = children.iterator();
       }  
                               
       for (int i = 0; iterator != null && iterator.hasNext() ; i++) {
              AliasObject oNode = (AliasObject)iterator.next();
              AliasBean aAlias = new AliasBean(oNode, mClearFieldObj);            
              mAlias.add(aAlias);          
         }
   
       iterator = null;
       children = mPatientObject.pGetChildren("Address");
       if (children != null) {
           size = children.size();
           iterator = children.iterator();
       }  
                               
       for (int i = 0; iterator != null && iterator.hasNext() ; i++) {
              AddressObject oNode = (AddressObject)iterator.next();
              AddressBean aAddress = new AddressBean(oNode, mClearFieldObj);            
              mAddress.add(aAddress);          
         }
   
       iterator = null;
       children = mPatientObject.pGetChildren("Phone");
       if (children != null) {
           size = children.size();
           iterator = children.iterator();
       }  
                               
       for (int i = 0; iterator != null && iterator.hasNext() ; i++) {
              PhoneObject oNode = (PhoneObject)iterator.next();
              PhoneBean aPhone = new PhoneBean(oNode, mClearFieldObj);            
              mPhone.add(aPhone);          
         }
   
       iterator = null;
       children = mPatientObject.pGetChildren("DriverLicense");
       if (children != null) {
           size = children.size();
           iterator = children.iterator();
       }  
                               
       for (int i = 0; iterator != null && iterator.hasNext() ; i++) {
              DriverLicenseObject oNode = (DriverLicenseObject)iterator.next();
              DriverLicenseBean aDriverLicense = new DriverLicenseBean(oNode, mClearFieldObj);            
              mDriverLicense.add(aDriverLicense);          
         }
   
       iterator = null;
       children = mPatientObject.pGetChildren("Account");
       if (children != null) {
           size = children.size();
           iterator = children.iterator();
       }  
                               
       for (int i = 0; iterator != null && iterator.hasNext() ; i++) {
              AccountObject oNode = (AccountObject)iterator.next();
              AccountBean aAccount = new AccountBean(oNode, mClearFieldObj);            
              mAccount.add(aAccount);          
         }
   
       iterator = null;
       children = mPatientObject.pGetChildren("MotherName");
       if (children != null) {
           size = children.size();
           iterator = children.iterator();
       }  
                               
       for (int i = 0; iterator != null && iterator.hasNext() ; i++) {
              MotherNameObject oNode = (MotherNameObject)iterator.next();
              MotherNameBean aMotherName = new MotherNameBean(oNode, mClearFieldObj);            
              mMotherName.add(aMotherName);          
         }
   
       iterator = null;
       children = mPatientObject.pGetChildren("MotherId");
       if (children != null) {
           size = children.size();
           iterator = children.iterator();
       }  
                               
       for (int i = 0; iterator != null && iterator.hasNext() ; i++) {
              MotherIdObject oNode = (MotherIdObject)iterator.next();
              MotherIdBean aMotherId = new MotherIdBean(oNode, mClearFieldObj);            
              mMotherId.add(aMotherId);          
         }
   
       iterator = null;
       children = mPatientObject.pGetChildren("Identifier");
       if (children != null) {
           size = children.size();
           iterator = children.iterator();
       }  
                               
       for (int i = 0; iterator != null && iterator.hasNext() ; i++) {
              IdentifierObject oNode = (IdentifierObject)iterator.next();
              IdentifierBean aIdentifier = new IdentifierBean(oNode, mClearFieldObj);            
              mIdentifier.add(aIdentifier);          
         }
   
       iterator = null;
       children = mPatientObject.pGetChildren("LastUpdate");
       if (children != null) {
           size = children.size();
           iterator = children.iterator();
       }  
                               
       for (int i = 0; iterator != null && iterator.hasNext() ; i++) {
              LastUpdateObject oNode = (LastUpdateObject)iterator.next();
              LastUpdateBean aLastUpdate = new LastUpdateBean(oNode, mClearFieldObj);            
              mLastUpdate.add(aLastUpdate);          
         }
   
     }

    /**
     * Getter for PatientId
     * @return a string value of PatientId
     */     
    public String getPatientId() throws ObjectException
    {
        try
        {
            int type = mPatientObject.pGetType("PatientId");
            Object value = mPatientObject.getValue("PatientId");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for FirstName
     * @return a string value of FirstName
     */     
    public String getFirstName() throws ObjectException
    {
        try
        {
            int type = mPatientObject.pGetType("FirstName");
            Object value = mPatientObject.getValue("FirstName");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for FirstName_Std
     * @return a string value of FirstName_Std
     */     
    public String getFirstName_Std() throws ObjectException
    {
        try
        {
            int type = mPatientObject.pGetType("FirstName_Std");
            Object value = mPatientObject.getValue("FirstName_Std");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for FirstName_Phon
     * @return a string value of FirstName_Phon
     */     
    public String getFirstName_Phon() throws ObjectException
    {
        try
        {
            int type = mPatientObject.pGetType("FirstName_Phon");
            Object value = mPatientObject.getValue("FirstName_Phon");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for MiddleName
     * @return a string value of MiddleName
     */     
    public String getMiddleName() throws ObjectException
    {
        try
        {
            int type = mPatientObject.pGetType("MiddleName");
            Object value = mPatientObject.getValue("MiddleName");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for LastName
     * @return a string value of LastName
     */     
    public String getLastName() throws ObjectException
    {
        try
        {
            int type = mPatientObject.pGetType("LastName");
            Object value = mPatientObject.getValue("LastName");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for LastName_Std
     * @return a string value of LastName_Std
     */     
    public String getLastName_Std() throws ObjectException
    {
        try
        {
            int type = mPatientObject.pGetType("LastName_Std");
            Object value = mPatientObject.getValue("LastName_Std");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for LastName_Phon
     * @return a string value of LastName_Phon
     */     
    public String getLastName_Phon() throws ObjectException
    {
        try
        {
            int type = mPatientObject.pGetType("LastName_Phon");
            Object value = mPatientObject.getValue("LastName_Phon");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for Suffix
     * @return a string value of Suffix
     */     
    public String getSuffix() throws ObjectException
    {
        try
        {
            int type = mPatientObject.pGetType("Suffix");
            Object value = mPatientObject.getValue("Suffix");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for Prefix
     * @return a string value of Prefix
     */     
    public String getPrefix() throws ObjectException
    {
        try
        {
            int type = mPatientObject.pGetType("Prefix");
            Object value = mPatientObject.getValue("Prefix");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for Degree
     * @return a string value of Degree
     */     
    public String getDegree() throws ObjectException
    {
        try
        {
            int type = mPatientObject.pGetType("Degree");
            Object value = mPatientObject.getValue("Degree");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for SSN
     * @return a string value of SSN
     */     
    public String getSSN() throws ObjectException
    {
        try
        {
            int type = mPatientObject.pGetType("SSN");
            Object value = mPatientObject.getValue("SSN");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for DOB
     * @return a string value of DOB
     */     
    public String getDOB() throws ObjectException
    {
        try
        {
            int type = mPatientObject.pGetType("DOB");
            Object value = mPatientObject.getValue("DOB");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for BirthPlace
     * @return a string value of BirthPlace
     */     
    public String getBirthPlace() throws ObjectException
    {
        try
        {
            int type = mPatientObject.pGetType("BirthPlace");
            Object value = mPatientObject.getValue("BirthPlace");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for BirthOrder
     * @return a string value of BirthOrder
     */     
    public String getBirthOrder() throws ObjectException
    {
        try
        {
            int type = mPatientObject.pGetType("BirthOrder");
            Object value = mPatientObject.getValue("BirthOrder");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for Gender
     * @return a string value of Gender
     */     
    public String getGender() throws ObjectException
    {
        try
        {
            int type = mPatientObject.pGetType("Gender");
            Object value = mPatientObject.getValue("Gender");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for MaritalStatus
     * @return a string value of MaritalStatus
     */     
    public String getMaritalStatus() throws ObjectException
    {
        try
        {
            int type = mPatientObject.pGetType("MaritalStatus");
            Object value = mPatientObject.getValue("MaritalStatus");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for Race
     * @return a string value of Race
     */     
    public String getRace() throws ObjectException
    {
        try
        {
            int type = mPatientObject.pGetType("Race");
            Object value = mPatientObject.getValue("Race");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for Ethnic
     * @return a string value of Ethnic
     */     
    public String getEthnic() throws ObjectException
    {
        try
        {
            int type = mPatientObject.pGetType("Ethnic");
            Object value = mPatientObject.getValue("Ethnic");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for Religion
     * @return a string value of Religion
     */     
    public String getReligion() throws ObjectException
    {
        try
        {
            int type = mPatientObject.pGetType("Religion");
            Object value = mPatientObject.getValue("Religion");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for Language
     * @return a string value of Language
     */     
    public String getLanguage() throws ObjectException
    {
        try
        {
            int type = mPatientObject.pGetType("Language");
            Object value = mPatientObject.getValue("Language");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for Nationality
     * @return a string value of Nationality
     */     
    public String getNationality() throws ObjectException
    {
        try
        {
            int type = mPatientObject.pGetType("Nationality");
            Object value = mPatientObject.getValue("Nationality");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for Citizenship
     * @return a string value of Citizenship
     */     
    public String getCitizenship() throws ObjectException
    {
        try
        {
            int type = mPatientObject.pGetType("Citizenship");
            Object value = mPatientObject.getValue("Citizenship");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for SpouseName
     * @return a string value of SpouseName
     */     
    public String getSpouseName() throws ObjectException
    {
        try
        {
            int type = mPatientObject.pGetType("SpouseName");
            Object value = mPatientObject.getValue("SpouseName");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for Maiden
     * @return a string value of Maiden
     */     
    public String getMaiden() throws ObjectException
    {
        try
        {
            int type = mPatientObject.pGetType("Maiden");
            Object value = mPatientObject.getValue("Maiden");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for VeteransStatus
     * @return a string value of VeteransStatus
     */     
    public String getVeteransStatus() throws ObjectException
    {
        try
        {
            int type = mPatientObject.pGetType("VeteransStatus");
            Object value = mPatientObject.getValue("VeteransStatus");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for Status
     * @return a string value of Status
     */     
    public String getStatus() throws ObjectException
    {
        try
        {
            int type = mPatientObject.pGetType("Status");
            Object value = mPatientObject.getValue("Status");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for DOD
     * @return a string value of DOD
     */     
    public String getDOD() throws ObjectException
    {
        try
        {
            int type = mPatientObject.pGetType("DOD");
            Object value = mPatientObject.getValue("DOD");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for DeathIndicator
     * @return a string value of DeathIndicator
     */     
    public String getDeathIndicator() throws ObjectException
    {
        try
        {
            int type = mPatientObject.pGetType("DeathIndicator");
            Object value = mPatientObject.getValue("DeathIndicator");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Setter for PatientId
     * @param a string value of PatientId
     */ 
    public void setPatientId(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mPatientObject.isNullable("PatientId")) {
               mPatientObject.clearField("PatientId");
            } else {
               int type = mPatientObject.pGetType("PatientId");
               Object val = strToObj(value, type, "PatientId");
          
               mPatientObject.setValue("PatientId", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for FirstName
     * @param a string value of FirstName
     */ 
    public void setFirstName(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mPatientObject.isNullable("FirstName")) {
               mPatientObject.clearField("FirstName");
            } else {
               int type = mPatientObject.pGetType("FirstName");
               Object val = strToObj(value, type, "FirstName");
          
               mPatientObject.setValue("FirstName", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for FirstName_Std
     * @param a string value of FirstName_Std
     */ 
    public void setFirstName_Std(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mPatientObject.isNullable("FirstName_Std")) {
               mPatientObject.clearField("FirstName_Std");
            } else {
               int type = mPatientObject.pGetType("FirstName_Std");
               Object val = strToObj(value, type, "FirstName_Std");
          
               mPatientObject.setValue("FirstName_Std", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for FirstName_Phon
     * @param a string value of FirstName_Phon
     */ 
    public void setFirstName_Phon(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mPatientObject.isNullable("FirstName_Phon")) {
               mPatientObject.clearField("FirstName_Phon");
            } else {
               int type = mPatientObject.pGetType("FirstName_Phon");
               Object val = strToObj(value, type, "FirstName_Phon");
          
               mPatientObject.setValue("FirstName_Phon", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for MiddleName
     * @param a string value of MiddleName
     */ 
    public void setMiddleName(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mPatientObject.isNullable("MiddleName")) {
               mPatientObject.clearField("MiddleName");
            } else {
               int type = mPatientObject.pGetType("MiddleName");
               Object val = strToObj(value, type, "MiddleName");
          
               mPatientObject.setValue("MiddleName", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for LastName
     * @param a string value of LastName
     */ 
    public void setLastName(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mPatientObject.isNullable("LastName")) {
               mPatientObject.clearField("LastName");
            } else {
               int type = mPatientObject.pGetType("LastName");
               Object val = strToObj(value, type, "LastName");
          
               mPatientObject.setValue("LastName", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for LastName_Std
     * @param a string value of LastName_Std
     */ 
    public void setLastName_Std(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mPatientObject.isNullable("LastName_Std")) {
               mPatientObject.clearField("LastName_Std");
            } else {
               int type = mPatientObject.pGetType("LastName_Std");
               Object val = strToObj(value, type, "LastName_Std");
          
               mPatientObject.setValue("LastName_Std", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for LastName_Phon
     * @param a string value of LastName_Phon
     */ 
    public void setLastName_Phon(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mPatientObject.isNullable("LastName_Phon")) {
               mPatientObject.clearField("LastName_Phon");
            } else {
               int type = mPatientObject.pGetType("LastName_Phon");
               Object val = strToObj(value, type, "LastName_Phon");
          
               mPatientObject.setValue("LastName_Phon", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for Suffix
     * @param a string value of Suffix
     */ 
    public void setSuffix(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mPatientObject.isNullable("Suffix")) {
               mPatientObject.clearField("Suffix");
            } else {
               int type = mPatientObject.pGetType("Suffix");
               Object val = strToObj(value, type, "Suffix");
          
               mPatientObject.setValue("Suffix", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for Prefix
     * @param a string value of Prefix
     */ 
    public void setPrefix(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mPatientObject.isNullable("Prefix")) {
               mPatientObject.clearField("Prefix");
            } else {
               int type = mPatientObject.pGetType("Prefix");
               Object val = strToObj(value, type, "Prefix");
          
               mPatientObject.setValue("Prefix", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for Degree
     * @param a string value of Degree
     */ 
    public void setDegree(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mPatientObject.isNullable("Degree")) {
               mPatientObject.clearField("Degree");
            } else {
               int type = mPatientObject.pGetType("Degree");
               Object val = strToObj(value, type, "Degree");
          
               mPatientObject.setValue("Degree", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for SSN
     * @param a string value of SSN
     */ 
    public void setSSN(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mPatientObject.isNullable("SSN")) {
               mPatientObject.clearField("SSN");
            } else {
               int type = mPatientObject.pGetType("SSN");
               Object val = strToObj(value, type, "SSN");
          
               mPatientObject.setValue("SSN", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for DOB
     * @param a string value of DOB
     */ 
    public void setDOB(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mPatientObject.isNullable("DOB")) {
               mPatientObject.clearField("DOB");
            } else {
               int type = mPatientObject.pGetType("DOB");
               Object val = strToObj(value, type, "DOB");
          
               mPatientObject.setValue("DOB", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for BirthPlace
     * @param a string value of BirthPlace
     */ 
    public void setBirthPlace(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mPatientObject.isNullable("BirthPlace")) {
               mPatientObject.clearField("BirthPlace");
            } else {
               int type = mPatientObject.pGetType("BirthPlace");
               Object val = strToObj(value, type, "BirthPlace");
          
               mPatientObject.setValue("BirthPlace", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for BirthOrder
     * @param a string value of BirthOrder
     */ 
    public void setBirthOrder(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mPatientObject.isNullable("BirthOrder")) {
               mPatientObject.clearField("BirthOrder");
            } else {
               int type = mPatientObject.pGetType("BirthOrder");
               Object val = strToObj(value, type, "BirthOrder");
          
               mPatientObject.setValue("BirthOrder", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for Gender
     * @param a string value of Gender
     */ 
    public void setGender(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mPatientObject.isNullable("Gender")) {
               mPatientObject.clearField("Gender");
            } else {
               int type = mPatientObject.pGetType("Gender");
               Object val = strToObj(value, type, "Gender");
          
               mPatientObject.setValue("Gender", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for MaritalStatus
     * @param a string value of MaritalStatus
     */ 
    public void setMaritalStatus(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mPatientObject.isNullable("MaritalStatus")) {
               mPatientObject.clearField("MaritalStatus");
            } else {
               int type = mPatientObject.pGetType("MaritalStatus");
               Object val = strToObj(value, type, "MaritalStatus");
          
               mPatientObject.setValue("MaritalStatus", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for Race
     * @param a string value of Race
     */ 
    public void setRace(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mPatientObject.isNullable("Race")) {
               mPatientObject.clearField("Race");
            } else {
               int type = mPatientObject.pGetType("Race");
               Object val = strToObj(value, type, "Race");
          
               mPatientObject.setValue("Race", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for Ethnic
     * @param a string value of Ethnic
     */ 
    public void setEthnic(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mPatientObject.isNullable("Ethnic")) {
               mPatientObject.clearField("Ethnic");
            } else {
               int type = mPatientObject.pGetType("Ethnic");
               Object val = strToObj(value, type, "Ethnic");
          
               mPatientObject.setValue("Ethnic", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for Religion
     * @param a string value of Religion
     */ 
    public void setReligion(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mPatientObject.isNullable("Religion")) {
               mPatientObject.clearField("Religion");
            } else {
               int type = mPatientObject.pGetType("Religion");
               Object val = strToObj(value, type, "Religion");
          
               mPatientObject.setValue("Religion", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for Language
     * @param a string value of Language
     */ 
    public void setLanguage(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mPatientObject.isNullable("Language")) {
               mPatientObject.clearField("Language");
            } else {
               int type = mPatientObject.pGetType("Language");
               Object val = strToObj(value, type, "Language");
          
               mPatientObject.setValue("Language", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for Nationality
     * @param a string value of Nationality
     */ 
    public void setNationality(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mPatientObject.isNullable("Nationality")) {
               mPatientObject.clearField("Nationality");
            } else {
               int type = mPatientObject.pGetType("Nationality");
               Object val = strToObj(value, type, "Nationality");
          
               mPatientObject.setValue("Nationality", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for Citizenship
     * @param a string value of Citizenship
     */ 
    public void setCitizenship(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mPatientObject.isNullable("Citizenship")) {
               mPatientObject.clearField("Citizenship");
            } else {
               int type = mPatientObject.pGetType("Citizenship");
               Object val = strToObj(value, type, "Citizenship");
          
               mPatientObject.setValue("Citizenship", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for SpouseName
     * @param a string value of SpouseName
     */ 
    public void setSpouseName(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mPatientObject.isNullable("SpouseName")) {
               mPatientObject.clearField("SpouseName");
            } else {
               int type = mPatientObject.pGetType("SpouseName");
               Object val = strToObj(value, type, "SpouseName");
          
               mPatientObject.setValue("SpouseName", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for Maiden
     * @param a string value of Maiden
     */ 
    public void setMaiden(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mPatientObject.isNullable("Maiden")) {
               mPatientObject.clearField("Maiden");
            } else {
               int type = mPatientObject.pGetType("Maiden");
               Object val = strToObj(value, type, "Maiden");
          
               mPatientObject.setValue("Maiden", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for VeteransStatus
     * @param a string value of VeteransStatus
     */ 
    public void setVeteransStatus(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mPatientObject.isNullable("VeteransStatus")) {
               mPatientObject.clearField("VeteransStatus");
            } else {
               int type = mPatientObject.pGetType("VeteransStatus");
               Object val = strToObj(value, type, "VeteransStatus");
          
               mPatientObject.setValue("VeteransStatus", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for Status
     * @param a string value of Status
     */ 
    public void setStatus(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mPatientObject.isNullable("Status")) {
               mPatientObject.clearField("Status");
            } else {
               int type = mPatientObject.pGetType("Status");
               Object val = strToObj(value, type, "Status");
          
               mPatientObject.setValue("Status", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for DOD
     * @param a string value of DOD
     */ 
    public void setDOD(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mPatientObject.isNullable("DOD")) {
               mPatientObject.clearField("DOD");
            } else {
               int type = mPatientObject.pGetType("DOD");
               Object val = strToObj(value, type, "DOD");
          
               mPatientObject.setValue("DOD", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for DeathIndicator
     * @param a string value of DeathIndicator
     */ 
    public void setDeathIndicator(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mPatientObject.isNullable("DeathIndicator")) {
               mPatientObject.clearField("DeathIndicator");
            } else {
               int type = mPatientObject.pGetType("DeathIndicator");
               Object val = strToObj(value, type, "DeathIndicator");
          
               mPatientObject.setValue("DeathIndicator", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
   
            
    /**
     * Getter for Alias
     * @return a collection of AliasBean
     */ 
    public  AliasBean[] getAlias()
    {       
        int length = mAlias.size();
        AliasBean[] arrChildren = new AliasBean[length];
        return (AliasBean[] )mAlias.toArray(arrChildren);           
    }

    /**
     * Return the ith AliasBean
     * @param index of AliasBean
     * @return AliasBean
     */
    public  AliasBean getAlias(int i) throws ObjectException
    {    
      try {
         Collection children = mPatientObject.pGetChildren("Alias");
         int size = 0;
         if (children != null) {
           size = children.size();
         }  
     
         if (size <= i) {                     
            for (int j=size; j < i+1; j++) {
              AliasBean aAlias = new AliasBean(mClearFieldObj);
              ObjectNode oNode = aAlias.getObjectNode();
              mPatientObject.addChild(oNode);
              mAlias.add(aAlias);
                              
            }
         }
         return (AliasBean)mAlias.get(i); 
       } catch (ObjectException ex) {
            throw ex;
       }
    }

    /**
     * Return the size of Alias
     * @return size of Alias
     */
    public int countAlias() {
      return mAlias.size();
    }
    
    /**
     * Delete Alias
     */
    public void deleteAlias(String aliasId) 
       throws ObjectException{
       mPatientObject.deleteChild("Alias", aliasId);
    }

     
    /**
     * Getter for Address
     * @return a collection of AddressBean
     */ 
    public  AddressBean[] getAddress()
    {       
        int length = mAddress.size();
        AddressBean[] arrChildren = new AddressBean[length];
        return (AddressBean[] )mAddress.toArray(arrChildren);           
    }

    /**
     * Return the ith AddressBean
     * @param index of AddressBean
     * @return AddressBean
     */
    public  AddressBean getAddress(int i) throws ObjectException
    {    
      try {
         Collection children = mPatientObject.pGetChildren("Address");
         int size = 0;
         if (children != null) {
           size = children.size();
         }  
     
         if (size <= i) {                     
            for (int j=size; j < i+1; j++) {
              AddressBean aAddress = new AddressBean(mClearFieldObj);
              ObjectNode oNode = aAddress.getObjectNode();
              mPatientObject.addChild(oNode);
              mAddress.add(aAddress);
                              
            }
         }
         return (AddressBean)mAddress.get(i); 
       } catch (ObjectException ex) {
            throw ex;
       }
    }

    /**
     * Return the size of Address
     * @return size of Address
     */
    public int countAddress() {
      return mAddress.size();
    }
    
    /**
     * Delete Address
     */
    public void deleteAddress(String addressId) 
       throws ObjectException{
       mPatientObject.deleteChild("Address", addressId);
    }

     
    /**
     * Getter for Phone
     * @return a collection of PhoneBean
     */ 
    public  PhoneBean[] getPhone()
    {       
        int length = mPhone.size();
        PhoneBean[] arrChildren = new PhoneBean[length];
        return (PhoneBean[] )mPhone.toArray(arrChildren);           
    }

    /**
     * Return the ith PhoneBean
     * @param index of PhoneBean
     * @return PhoneBean
     */
    public  PhoneBean getPhone(int i) throws ObjectException
    {    
      try {
         Collection children = mPatientObject.pGetChildren("Phone");
         int size = 0;
         if (children != null) {
           size = children.size();
         }  
     
         if (size <= i) {                     
            for (int j=size; j < i+1; j++) {
              PhoneBean aPhone = new PhoneBean(mClearFieldObj);
              ObjectNode oNode = aPhone.getObjectNode();
              mPatientObject.addChild(oNode);
              mPhone.add(aPhone);
                              
            }
         }
         return (PhoneBean)mPhone.get(i); 
       } catch (ObjectException ex) {
            throw ex;
       }
    }

    /**
     * Return the size of Phone
     * @return size of Phone
     */
    public int countPhone() {
      return mPhone.size();
    }
    
    /**
     * Delete Phone
     */
    public void deletePhone(String phoneId) 
       throws ObjectException{
       mPatientObject.deleteChild("Phone", phoneId);
    }

     
    /**
     * Getter for DriverLicense
     * @return a collection of DriverLicenseBean
     */ 
    public  DriverLicenseBean[] getDriverLicense()
    {       
        int length = mDriverLicense.size();
        DriverLicenseBean[] arrChildren = new DriverLicenseBean[length];
        return (DriverLicenseBean[] )mDriverLicense.toArray(arrChildren);           
    }

    /**
     * Return the ith DriverLicenseBean
     * @param index of DriverLicenseBean
     * @return DriverLicenseBean
     */
    public  DriverLicenseBean getDriverLicense(int i) throws ObjectException
    {    
      try {
         Collection children = mPatientObject.pGetChildren("DriverLicense");
         int size = 0;
         if (children != null) {
           size = children.size();
         }  
     
         if (size <= i) {                     
            for (int j=size; j < i+1; j++) {
              DriverLicenseBean aDriverLicense = new DriverLicenseBean(mClearFieldObj);
              ObjectNode oNode = aDriverLicense.getObjectNode();
              mPatientObject.addChild(oNode);
              mDriverLicense.add(aDriverLicense);
                              
            }
         }
         return (DriverLicenseBean)mDriverLicense.get(i); 
       } catch (ObjectException ex) {
            throw ex;
       }
    }

    /**
     * Return the size of DriverLicense
     * @return size of DriverLicense
     */
    public int countDriverLicense() {
      return mDriverLicense.size();
    }
    
    /**
     * Delete DriverLicense
     */
    public void deleteDriverLicense(String driverlicenseId) 
       throws ObjectException{
       mPatientObject.deleteChild("DriverLicense", driverlicenseId);
    }

     
    /**
     * Getter for Account
     * @return a collection of AccountBean
     */ 
    public  AccountBean[] getAccount()
    {       
        int length = mAccount.size();
        AccountBean[] arrChildren = new AccountBean[length];
        return (AccountBean[] )mAccount.toArray(arrChildren);           
    }

    /**
     * Return the ith AccountBean
     * @param index of AccountBean
     * @return AccountBean
     */
    public  AccountBean getAccount(int i) throws ObjectException
    {    
      try {
         Collection children = mPatientObject.pGetChildren("Account");
         int size = 0;
         if (children != null) {
           size = children.size();
         }  
     
         if (size <= i) {                     
            for (int j=size; j < i+1; j++) {
              AccountBean aAccount = new AccountBean(mClearFieldObj);
              ObjectNode oNode = aAccount.getObjectNode();
              mPatientObject.addChild(oNode);
              mAccount.add(aAccount);
                              
            }
         }
         return (AccountBean)mAccount.get(i); 
       } catch (ObjectException ex) {
            throw ex;
       }
    }

    /**
     * Return the size of Account
     * @return size of Account
     */
    public int countAccount() {
      return mAccount.size();
    }
    
    /**
     * Delete Account
     */
    public void deleteAccount(String accountId) 
       throws ObjectException{
       mPatientObject.deleteChild("Account", accountId);
    }

     
    /**
     * Getter for MotherName
     * @return a collection of MotherNameBean
     */ 
    public  MotherNameBean[] getMotherName()
    {       
        int length = mMotherName.size();
        MotherNameBean[] arrChildren = new MotherNameBean[length];
        return (MotherNameBean[] )mMotherName.toArray(arrChildren);           
    }

    /**
     * Return the ith MotherNameBean
     * @param index of MotherNameBean
     * @return MotherNameBean
     */
    public  MotherNameBean getMotherName(int i) throws ObjectException
    {    
      try {
         Collection children = mPatientObject.pGetChildren("MotherName");
         int size = 0;
         if (children != null) {
           size = children.size();
         }  
     
         if (size <= i) {                     
            for (int j=size; j < i+1; j++) {
              MotherNameBean aMotherName = new MotherNameBean(mClearFieldObj);
              ObjectNode oNode = aMotherName.getObjectNode();
              mPatientObject.addChild(oNode);
              mMotherName.add(aMotherName);
                              
            }
         }
         return (MotherNameBean)mMotherName.get(i); 
       } catch (ObjectException ex) {
            throw ex;
       }
    }

    /**
     * Return the size of MotherName
     * @return size of MotherName
     */
    public int countMotherName() {
      return mMotherName.size();
    }
    
    /**
     * Delete MotherName
     */
    public void deleteMotherName(String mothernameId) 
       throws ObjectException{
       mPatientObject.deleteChild("MotherName", mothernameId);
    }

     
    /**
     * Getter for MotherId
     * @return a collection of MotherIdBean
     */ 
    public  MotherIdBean[] getMotherId()
    {       
        int length = mMotherId.size();
        MotherIdBean[] arrChildren = new MotherIdBean[length];
        return (MotherIdBean[] )mMotherId.toArray(arrChildren);           
    }

    /**
     * Return the ith MotherIdBean
     * @param index of MotherIdBean
     * @return MotherIdBean
     */
    public  MotherIdBean getMotherId(int i) throws ObjectException
    {    
      try {
         Collection children = mPatientObject.pGetChildren("MotherId");
         int size = 0;
         if (children != null) {
           size = children.size();
         }  
     
         if (size <= i) {                     
            for (int j=size; j < i+1; j++) {
              MotherIdBean aMotherId = new MotherIdBean(mClearFieldObj);
              ObjectNode oNode = aMotherId.getObjectNode();
              mPatientObject.addChild(oNode);
              mMotherId.add(aMotherId);
                              
            }
         }
         return (MotherIdBean)mMotherId.get(i); 
       } catch (ObjectException ex) {
            throw ex;
       }
    }

    /**
     * Return the size of MotherId
     * @return size of MotherId
     */
    public int countMotherId() {
      return mMotherId.size();
    }
    
    /**
     * Delete MotherId
     */
    public void deleteMotherId(String motheridId) 
       throws ObjectException{
       mPatientObject.deleteChild("MotherId", motheridId);
    }

     
    /**
     * Getter for Identifier
     * @return a collection of IdentifierBean
     */ 
    public  IdentifierBean[] getIdentifier()
    {       
        int length = mIdentifier.size();
        IdentifierBean[] arrChildren = new IdentifierBean[length];
        return (IdentifierBean[] )mIdentifier.toArray(arrChildren);           
    }

    /**
     * Return the ith IdentifierBean
     * @param index of IdentifierBean
     * @return IdentifierBean
     */
    public  IdentifierBean getIdentifier(int i) throws ObjectException
    {    
      try {
         Collection children = mPatientObject.pGetChildren("Identifier");
         int size = 0;
         if (children != null) {
           size = children.size();
         }  
     
         if (size <= i) {                     
            for (int j=size; j < i+1; j++) {
              IdentifierBean aIdentifier = new IdentifierBean(mClearFieldObj);
              ObjectNode oNode = aIdentifier.getObjectNode();
              mPatientObject.addChild(oNode);
              mIdentifier.add(aIdentifier);
                              
            }
         }
         return (IdentifierBean)mIdentifier.get(i); 
       } catch (ObjectException ex) {
            throw ex;
       }
    }

    /**
     * Return the size of Identifier
     * @return size of Identifier
     */
    public int countIdentifier() {
      return mIdentifier.size();
    }
    
    /**
     * Delete Identifier
     */
    public void deleteIdentifier(String identifierId) 
       throws ObjectException{
       mPatientObject.deleteChild("Identifier", identifierId);
    }

     
    /**
     * Getter for LastUpdate
     * @return a collection of LastUpdateBean
     */ 
    public  LastUpdateBean[] getLastUpdate()
    {       
        int length = mLastUpdate.size();
        LastUpdateBean[] arrChildren = new LastUpdateBean[length];
        return (LastUpdateBean[] )mLastUpdate.toArray(arrChildren);           
    }

    /**
     * Return the ith LastUpdateBean
     * @param index of LastUpdateBean
     * @return LastUpdateBean
     */
    public  LastUpdateBean getLastUpdate(int i) throws ObjectException
    {    
      try {
         Collection children = mPatientObject.pGetChildren("LastUpdate");
         int size = 0;
         if (children != null) {
           size = children.size();
         }  
     
         if (size <= i) {                     
            for (int j=size; j < i+1; j++) {
              LastUpdateBean aLastUpdate = new LastUpdateBean(mClearFieldObj);
              ObjectNode oNode = aLastUpdate.getObjectNode();
              mPatientObject.addChild(oNode);
              mLastUpdate.add(aLastUpdate);
                              
            }
         }
         return (LastUpdateBean)mLastUpdate.get(i); 
       } catch (ObjectException ex) {
            throw ex;
       }
    }

    /**
     * Return the size of LastUpdate
     * @return size of LastUpdate
     */
    public int countLastUpdate() {
      return mLastUpdate.size();
    }
    
    /**
     * Delete LastUpdate
     */
    public void deleteLastUpdate(String lastupdateId) 
       throws ObjectException{
       mPatientObject.deleteChild("LastUpdate", lastupdateId);
    }

     

    /**
     * Setter for Alias
     * @param index of Alias
     * @param AliasBean
     */ 
    public void setAlias(int index, AliasBean alias)
    {
        try
        {
            mPatientObject.addChild(alias.getObjectNode());
            mAlias.add(index, alias);
        }
        catch (ObjectException ex)
        {
            mLogger.error(ex.getMessage(), ex);
        }
    }

    /**
     * Setter for Alias
     * @param a collection of AliasBean
     */ 
    public void setAlias(AliasBean[] aliass)
    {
        try
        {
           for (int i = 0; i < aliass.length; i++) {
              mPatientObject.addChild(aliass[i].getObjectNode());
              mAlias.add(aliass);
           }
        }
        catch (ObjectException ex)
        {
            mLogger.error(ex.getMessage(), ex);
        }
    }
    /**
     * Setter for Address
     * @param index of Address
     * @param AddressBean
     */ 
    public void setAddress(int index, AddressBean address)
    {
        try
        {
            mPatientObject.addChild(address.getObjectNode());
            mAddress.add(index, address);
        }
        catch (ObjectException ex)
        {
            mLogger.error(ex.getMessage(), ex);
        }
    }

    /**
     * Setter for Address
     * @param a collection of AddressBean
     */ 
    public void setAddress(AddressBean[] addresss)
    {
        try
        {
           for (int i = 0; i < addresss.length; i++) {
              mPatientObject.addChild(addresss[i].getObjectNode());
              mAddress.add(addresss);
           }
        }
        catch (ObjectException ex)
        {
            mLogger.error(ex.getMessage(), ex);
        }
    }
    /**
     * Setter for Phone
     * @param index of Phone
     * @param PhoneBean
     */ 
    public void setPhone(int index, PhoneBean phone)
    {
        try
        {
            mPatientObject.addChild(phone.getObjectNode());
            mPhone.add(index, phone);
        }
        catch (ObjectException ex)
        {
            mLogger.error(ex.getMessage(), ex);
        }
    }

    /**
     * Setter for Phone
     * @param a collection of PhoneBean
     */ 
    public void setPhone(PhoneBean[] phones)
    {
        try
        {
           for (int i = 0; i < phones.length; i++) {
              mPatientObject.addChild(phones[i].getObjectNode());
              mPhone.add(phones);
           }
        }
        catch (ObjectException ex)
        {
            mLogger.error(ex.getMessage(), ex);
        }
    }
    /**
     * Setter for DriverLicense
     * @param index of DriverLicense
     * @param DriverLicenseBean
     */ 
    public void setDriverLicense(int index, DriverLicenseBean driverlicense)
    {
        try
        {
            mPatientObject.addChild(driverlicense.getObjectNode());
            mDriverLicense.add(index, driverlicense);
        }
        catch (ObjectException ex)
        {
            mLogger.error(ex.getMessage(), ex);
        }
    }

    /**
     * Setter for DriverLicense
     * @param a collection of DriverLicenseBean
     */ 
    public void setDriverLicense(DriverLicenseBean[] driverlicenses)
    {
        try
        {
           for (int i = 0; i < driverlicenses.length; i++) {
              mPatientObject.addChild(driverlicenses[i].getObjectNode());
              mDriverLicense.add(driverlicenses);
           }
        }
        catch (ObjectException ex)
        {
            mLogger.error(ex.getMessage(), ex);
        }
    }
    /**
     * Setter for Account
     * @param index of Account
     * @param AccountBean
     */ 
    public void setAccount(int index, AccountBean account)
    {
        try
        {
            mPatientObject.addChild(account.getObjectNode());
            mAccount.add(index, account);
        }
        catch (ObjectException ex)
        {
            mLogger.error(ex.getMessage(), ex);
        }
    }

    /**
     * Setter for Account
     * @param a collection of AccountBean
     */ 
    public void setAccount(AccountBean[] accounts)
    {
        try
        {
           for (int i = 0; i < accounts.length; i++) {
              mPatientObject.addChild(accounts[i].getObjectNode());
              mAccount.add(accounts);
           }
        }
        catch (ObjectException ex)
        {
            mLogger.error(ex.getMessage(), ex);
        }
    }
    /**
     * Setter for MotherName
     * @param index of MotherName
     * @param MotherNameBean
     */ 
    public void setMotherName(int index, MotherNameBean mothername)
    {
        try
        {
            mPatientObject.addChild(mothername.getObjectNode());
            mMotherName.add(index, mothername);
        }
        catch (ObjectException ex)
        {
            mLogger.error(ex.getMessage(), ex);
        }
    }

    /**
     * Setter for MotherName
     * @param a collection of MotherNameBean
     */ 
    public void setMotherName(MotherNameBean[] mothernames)
    {
        try
        {
           for (int i = 0; i < mothernames.length; i++) {
              mPatientObject.addChild(mothernames[i].getObjectNode());
              mMotherName.add(mothernames);
           }
        }
        catch (ObjectException ex)
        {
            mLogger.error(ex.getMessage(), ex);
        }
    }
    /**
     * Setter for MotherId
     * @param index of MotherId
     * @param MotherIdBean
     */ 
    public void setMotherId(int index, MotherIdBean motherid)
    {
        try
        {
            mPatientObject.addChild(motherid.getObjectNode());
            mMotherId.add(index, motherid);
        }
        catch (ObjectException ex)
        {
            mLogger.error(ex.getMessage(), ex);
        }
    }

    /**
     * Setter for MotherId
     * @param a collection of MotherIdBean
     */ 
    public void setMotherId(MotherIdBean[] motherids)
    {
        try
        {
           for (int i = 0; i < motherids.length; i++) {
              mPatientObject.addChild(motherids[i].getObjectNode());
              mMotherId.add(motherids);
           }
        }
        catch (ObjectException ex)
        {
            mLogger.error(ex.getMessage(), ex);
        }
    }
    /**
     * Setter for Identifier
     * @param index of Identifier
     * @param IdentifierBean
     */ 
    public void setIdentifier(int index, IdentifierBean identifier)
    {
        try
        {
            mPatientObject.addChild(identifier.getObjectNode());
            mIdentifier.add(index, identifier);
        }
        catch (ObjectException ex)
        {
            mLogger.error(ex.getMessage(), ex);
        }
    }

    /**
     * Setter for Identifier
     * @param a collection of IdentifierBean
     */ 
    public void setIdentifier(IdentifierBean[] identifiers)
    {
        try
        {
           for (int i = 0; i < identifiers.length; i++) {
              mPatientObject.addChild(identifiers[i].getObjectNode());
              mIdentifier.add(identifiers);
           }
        }
        catch (ObjectException ex)
        {
            mLogger.error(ex.getMessage(), ex);
        }
    }
    /**
     * Setter for LastUpdate
     * @param index of LastUpdate
     * @param LastUpdateBean
     */ 
    public void setLastUpdate(int index, LastUpdateBean lastupdate)
    {
        try
        {
            mPatientObject.addChild(lastupdate.getObjectNode());
            mLastUpdate.add(index, lastupdate);
        }
        catch (ObjectException ex)
        {
            mLogger.error(ex.getMessage(), ex);
        }
    }

    /**
     * Setter for LastUpdate
     * @param a collection of LastUpdateBean
     */ 
    public void setLastUpdate(LastUpdateBean[] lastupdates)
    {
        try
        {
           for (int i = 0; i < lastupdates.length; i++) {
              mPatientObject.addChild(lastupdates[i].getObjectNode());
              mLastUpdate.add(lastupdates);
           }
        }
        catch (ObjectException ex)
        {
            mLogger.error(ex.getMessage(), ex);
        }
    }

    /** 
     * Return PatientObject
     * @return PatientObject ObjectNode
     */  
    public ObjectNode pGetObject() {
        return mPatientObject;
    }

    /** 
     * Return for all children nodes
     * @return collection of children nodes
     */            
    public Collection pGetChildren() {
         ArrayList allChildren = new ArrayList();

         if (mAlias.size() > 0) {
             allChildren.add(mAlias);
         }
         if (mAddress.size() > 0) {
             allChildren.add(mAddress);
         }
         if (mPhone.size() > 0) {
             allChildren.add(mPhone);
         }
         if (mDriverLicense.size() > 0) {
             allChildren.add(mDriverLicense);
         }
         if (mAccount.size() > 0) {
             allChildren.add(mAccount);
         }
         if (mMotherName.size() > 0) {
             allChildren.add(mMotherName);
         }
         if (mMotherId.size() > 0) {
             allChildren.add(mMotherId);
         }
         if (mIdentifier.size() > 0) {
             allChildren.add(mIdentifier);
         }
         if (mLastUpdate.size() > 0) {
             allChildren.add(mLastUpdate);
         }
         return allChildren;
    }
    
    /** 
     * Getter for children of a specified type
     * @param type Type of children to retrieve
     * @return Arraylist of children of specified type
     */
    public Collection pGetChildren(String type) {
        return mPatientObject.pGetChildren(type);
    }

    /** 
     * Getter for child types
     * @return Arraylist of child types
     */
    public ArrayList pGetChildTypes() {
        return mPatientObject.getChildTags();
    }    

    /**
     * Count of all children
     * @return number of children
     */
    public int countChildren() {
        return ((ArrayList) pGetChildren()).size();
    }

    /**
     * Count of children of specified type
     * @param type of children to count
     * @return number of children of specified type
     */
    public int countChildren(String type) {
        return ((ArrayList) pGetChildren(type)).size();
    }

    /**
     * Return parent which is SystemObject  
     */  
    private SystemObject getParentSO() {
        ObjectNode obj = mPatientObject.getParent();
        
        while (obj != null) {
           if (obj instanceof SystemObject) {
              return (SystemObject) obj;
           } else {
              obj = obj.getParent();
           }
        }
        return (SystemObject) obj;
    }   
                
    static String objToString(Object value, int type) throws ObjectException {
        if (value == null) {
            return null;
        } else {
            if ( type == ObjectField.OBJECTMETA_STRING_TYPE) {
                return (String) value;
            }
            else if (type == ObjectField.OBJECTMETA_DATE_TYPE) {
               SimpleDateFormat dateFormat = new SimpleDateFormat(MetaDataService.getDateFormat());
               return dateFormat.format(value);              
            } else {
                return value.toString();
            }
        }
    }
    
    static Object strToObj(String str, int type, String fieldName) throws ObjectException {
        if (str == null || str.trim().length() == 0) {
            return null;
        } else if ( type == ObjectField.OBJECTMETA_STRING_TYPE) {
            return  str;         
        } else if (type == ObjectField.OBJECTMETA_DATE_TYPE) {
            ParsePosition pos = new ParsePosition(0);
            SimpleDateFormat dateFormat = new SimpleDateFormat(MetaDataService.getDateFormat());
            Object ret = dateFormat.parse(str, pos);   
            if ( ret == null) {
               throw new ObjectException("Invalid Date format of" + fieldName + ",value:" + str);
            }           
            return ret;
        } else if (type == ObjectField.OBJECTMETA_INT_TYPE) {                
            return Integer.valueOf(str);              
        } else if (type == ObjectField.OBJECTMETA_FLOAT_TYPE) {                
            return Float.valueOf(str);              
        } else if (type == ObjectField.OBJECTMETA_LONG_TYPE) {                
            return Long.valueOf(str);             
        } else if (type == ObjectField.OBJECTMETA_BOOL_TYPE) {                
            return Boolean.valueOf(str);              
        } else if (type == ObjectField.OBJECTMETA_CHAR_TYPE) {                
            return (new Character(str.charAt(0)));                 
        } else {
            throw new ObjectException("Invalid type of" + fieldName + ",value:" + str);
        }
    }    
}
