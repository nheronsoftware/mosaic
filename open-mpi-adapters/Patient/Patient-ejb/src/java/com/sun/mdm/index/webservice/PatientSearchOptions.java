/* *************************************************************************
 *
 *  Copyright (c) 2009, NetGen Software Inc., All Rights Reserved
 *
 *  This program, and all the routines referenced herein, are the proprietary
 *  properties and trade secrets of NetGen Software Inc.
 *
 *  Except as provided for by license agreement, this program shall not be
 *  duplicated, used, or disclosed without written consent signed by an officer
 *  of NetGen Software Inc.
 *
 ***************************************************************************/
package com.sun.mdm.index.webservice;

import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;
import com.sun.mdm.index.util.Constants;
import com.netgen.mdm.index.webservice.SearchOptionsBean;

/**
 * PatientSearchOptions
 * @author cye
 */
@XmlRootElement
public class PatientSearchOptions implements SearchOptionsBean {

    private String searchId = "BLOCKER-SEARCH";
    private boolean weighted = true;
    private double minimumWeight = Double.NEGATIVE_INFINITY;
    private int maxElements = Constants.DEFAULT_MAX_ELEMENTS;
    private int pageSize = Constants.DEFAULT_PAGE_SIZE;
    private int maxQueryElements = Constants.DEFAULT_MAX_ELEMENTS;
    private int candidateThreshold = 0;
    private List<String> fields;
    private String sortField;
    private boolean reverse;

		public String getSortField() {
			return sortField;
		}
		
		public boolean getReverse() {
			return reverse;
		}
		
    public String getSearchId() {
        return searchId;
    }

    public boolean getWeighted() {
        return weighted;
    }

    public double getMinimumWeight() {
        return minimumWeight;
    }
      
    public int getMaxElements() {
        return maxElements;
    }

    public int getPageSize() {
        return pageSize;
    }

    public int getMaxQueryElements() {
        return maxQueryElements;
    }

    public int getCandidateThreshold() {
        return candidateThreshold;
    }

    public List<String> getFields() {
        return fields;
    }

    public void setSearchId(String searchId) {
        this.searchId = searchId;
    }

    public void setWeighted(boolean weighted) {
        this.weighted = weighted;
    }

    public void setMinimumWeight(double minimumWeight){
        this.minimumWeight = minimumWeight;
    }
  
    public void setMaxElements(int maxElements) {
        this.maxElements = maxElements;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public void setMaxQueryElements(int maxQueryElements) {
        this.maxQueryElements = maxQueryElements;
    }

    public void setCandidateThreshold(int candidateThreshold) {
        this.candidateThreshold = candidateThreshold;
    }

    public void setFields(List<String> fields) {
        this.fields = fields;
    }
    
		public void setSortField(String sortField) {
				this.sortField = sortField;
		}
		
		public void setReverse(boolean reverse) {
	  		this.reverse = reverse;
		}    
}
