/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */

package com.sun.mdm.index.webservice;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.annotation.Resource;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.annotation.security.RunAs;
import com.sun.mdm.index.ejb.master.MasterControllerLocal;
import com.sun.mdm.index.master.UserException;
import com.sun.mdm.index.master.ProcessingException;
import com.sun.mdm.index.master.MatchResult;
import com.sun.mdm.index.master.MergeResult;
import com.sun.mdm.index.master.search.enterprise.EOSearchCriteria;
import com.sun.mdm.index.master.search.enterprise.EOSearchOptions;
import com.sun.mdm.index.master.search.enterprise.EOSearchResultIterator;
import com.sun.mdm.index.master.search.enterprise.EOSearchResultRecord;
import com.sun.mdm.index.master.search.potdup.PotentialDuplicateIterator;
import com.sun.mdm.index.master.search.potdup.PotentialDuplicateSummary;
import com.sun.mdm.index.master.SystemDefinition;
import com.sun.mdm.index.objects.metadata.MetaDataService;
import com.sun.mdm.index.objects.epath.EPathArrayList;
import com.sun.mdm.index.objects.ObjectNode;
import com.sun.mdm.index.objects.SBR;
import com.sun.mdm.index.objects.SystemObject;
import com.sun.mdm.index.objects.EnterpriseObject;
import com.sun.mdm.index.objects.SystemObjectPK;
import com.sun.mdm.index.objects.PatientObject;
import com.sun.mdm.index.page.PageException;
import com.sun.mdm.index.util.Constants;
import com.sun.mdm.index.util.LogUtil;
import com.sun.mdm.index.util.Logger;
import com.netgen.mdm.match.SystemMatchOption;
import com.netgen.mdm.match.SystemMatchResult;

/** 
    Wrapper class to interface MasterController. It invokes methods of MasterControllerEJB
    which is a Stateless session bean. The objects exposed in this class are Java beans that can
    be exposed in BPEL editor or any other editor. The methods are type specific
    to Patient where methods in MasterController are not any object type specific.
 */
@Stateless
@WebService
@TransactionManagement(TransactionManagementType.CONTAINER)
@RunAs("MasterIndex.Admin")
public class PatientEJB {
    private final static String ALPHA_SEARCH = "ALPHA-SEARCH";
    private final static String BLOCKER_SEARCH = "BLOCKER-SEARCH";
    private final static String PHONETIC_SEARCH = "PHONETIC-SEARCH";
    private final static String EJBLOCAL_MASTER = "ejb/PatientMasterController";

    @Resource
    private SessionContext mSessionContext;
    
    @EJB(beanInterface=MasterControllerLocal.class)
    private MasterControllerLocal mMC;

    private final Logger mLogger = LogUtil.getLogger(this);
    
    /**
     * No argument constructor required by container.
     */
    public PatientEJB() {

        if (mLogger.isDebugEnabled()) {
            mLogger.info("Initializing:" + "PatientEJB");
        }
    }

    /** 
     * Processes the system object based on configuration options defined for
     * the master controller and associated components in the runtime xml file.
     * The options affect executeMatch in the following ways: a) Query Builder
     * class and options sets for blocking b) Matching rules, Pass Controller
     * and Block Picker classes c) Decision Maker class and options
     *
     * @param sysObjBean System object to process.
     * @exception ProcessingException An error has occured.
     * @exception UserException A user error occured
     * @return MatchPatientResult.
     */
    @WebMethod
    @WebResult(name="MatchPatientResult") 
    public MatchPatientResult executeMatch(
        @WebParam(name = "sysObjBean") SystemPatient sysObjBean)
    throws ProcessingException, UserException {

        if (mLogger.isDebugEnabled()) {
            mLogger.info("PatientEJB" + "executeMatch" + sysObjBean);
        }

        SystemObject sourceSystem = sysObjBean.pGetSystemObject();
        MatchResult matchResult = mMC.executeMatch(sourceSystem);

        if (mLogger.isDebugEnabled()) {
            mLogger.info("matchResult EUID:" + matchResult.getEUID());
        }

        return new MatchPatientResult(matchResult);
    }
    
    /** 
     * Processes the system object in update mode.  Null fields are left unchanged.
     *
     * @param sysObjBean System object to process.
     * @exception ProcessingException An error has occured.
     * @exception UserException A user error occured
     * @return MatchPatientResult.
     */
    @WebMethod
    @WebResult(name="MatchPatientResult") 
    public MatchPatientResult executeMatchUpdate(
        @WebParam(name = "sysObjBean") SystemPatient sysObjBean)
    throws ProcessingException, UserException {

        if (mLogger.isDebugEnabled()) {
            mLogger.info("PatientEJB" + "executeMatchUpdate" + sysObjBean);
        }

        SystemObject sourceSystem = sysObjBean.pGetSystemObject();
        MatchResult matchResult = mMC.executeMatchUpdate(sourceSystem);

        if (mLogger.isDebugEnabled()) {
            mLogger.info("matchResult EUID:" + matchResult.getEUID());
        }

        return new MatchPatientResult(matchResult);
    }    
   

    /** 
     * Does the exact search on passed ObjectBean attribute values
     *
     * @param objBean The object bean contains the search criteria.
     * @exception ProcessingException An error has occured.
     * @throws UserException Invalid euid (null or empty string)
     * @return SearchObjectResult[]
     */ 
    private SearchPatientResult[] pSearchExact(
        PatientBean objBean)
    throws ProcessingException, UserException {

        if (mLogger.isDebugEnabled()) {
            mLogger.info("psearchExact:" + objBean);
        }

        return pSearch(objBean, ALPHA_SEARCH, false, Double.NEGATIVE_INFINITY);                        
    }
    
    /** 
     * Does the exact search on passed ObjectBean attribute values
     *
     * @param objBean The object bean contains the search criteria.
     * @exception ProcessingException An error has occured.
     * @throws UserException Invalid euid (null or empty string)
     * @return SearchPatientResult[]
     */ 
    @WebMethod
    @WebResult(name="searchPatientResult")
    public SearchPatientResult[] searchExact(
        @WebParam(name = "objBean") PatientBean objBean)
    throws ProcessingException, UserException {

        if (mLogger.isDebugEnabled()) {
            mLogger.info("searchExact:" + objBean);        
        }  

        return (SearchPatientResult[]) pSearchExact(objBean);             
    }

    /** 
     * Does the standardized search on passed PatientBean attribute values
     *
     * @param objBean The Patient contains the search criteria.
     * @exception ProcessingException An error has occured.
     * @throws UserException Invalid euid (null or empty string)
     * @return SearchObjectResult[]
     */ 
    private SearchPatientResult[] pSearchPhonetic(
        PatientBean objBean)
    throws ProcessingException, UserException {

        if (mLogger.isDebugEnabled()) {
            mLogger.info("pSearchPhonetic:" + objBean);
        }

        return pSearch(objBean, PHONETIC_SEARCH, true, Double.NEGATIVE_INFINITY);            
    }

    /** 
     * Does the standardized search on passed PatientBean attribute values
     *
     * @param objBean The Patient contains the search criteria.
     * @exception ProcessingException An error has occured.
     * @throws UserException Invalid euid (null or empty string)
     * @return SearchPatientResult[]
     */ 
    @WebMethod
    @WebResult(name="searchPatientResult")
    public SearchPatientResult[] searchPhonetic(
        @WebParam(name = "objBean") PatientBean objBean)
    throws ProcessingException, UserException {

        if (mLogger.isDebugEnabled()) {
            mLogger.info("SearchPhonetic:" + objBean); 
        }

        return (SearchPatientResult[]) pSearchPhonetic(objBean);             
    }
    
    /** 
     * Does the search enterprise object on passed PatientSearchCriteria and 
     * PatientSearchOptions
     * @param orgSearchCriteria The PatientSearchCriteria contains the search criteria.
     * @param orgSearchOptions The PatientSearchSearchOptions contains the search options.
     * @exception ProcessingException An processing error has occured.
     * @throws UserException An invalid parameters error has occured.
     * @return SearchPatientResultPages
     */    
    @WebMethod
    @WebResult(name="searchPatientResultPage")
    public SearchPatientResultPage searchEnterpriseObject(
        @WebParam(name = "objSearchCriteria") PatientSearchCriteria objSearchCriteria,
        @WebParam(name = "objSearchOptions") PatientSearchOptions objSearchOptions)
        throws ProcessingException, UserException {
        try {
            EOSearchCriteria eoSearchCriteria = new EOSearchCriteria();
            SystemPatient systemPatient = objSearchCriteria.getSystemPatient();
            eoSearchCriteria.setSystemObject(systemPatient.pGetSystemObject());

            EOSearchOptions eoSearchOptions = new EOSearchOptions();

            eoSearchOptions.setMaxElements(objSearchOptions.getMaxElements());
            eoSearchOptions.setPageSize(objSearchOptions.getPageSize());
            eoSearchOptions.setMaxQueryElements(objSearchOptions.getMaxQueryElements());
            eoSearchOptions.setCandidateThreshold(objSearchOptions.getCandidateThreshold());
            EPathArrayList eFields = new EPathArrayList();
            List<String> fields = objSearchOptions.getFields();
            for (String field : fields) {
                eFields.add(field);
            }
            eoSearchOptions.setFieldsToRetrieve(eFields);
            eoSearchOptions.setSearchId(objSearchOptions.getSearchId());
            eoSearchOptions.setWeighted(objSearchOptions.getWeighted());
				    eoSearchOptions.setMinimumWeight(objSearchOptions.getMinimumWeight());

            EOSearchResultIterator searchResult = mMC.searchEnterpriseObject(eoSearchCriteria, eoSearchOptions);

            if (objSearchOptions.getSortField() != null) {
                searchResult.sortBy(objSearchOptions.getSortField(), objSearchOptions.getReverse());
            }
            
            SearchPatientResultPage searchResultPage = new SearchPatientResultPage();
            List<SearchPatientResult> pageData = new ArrayList<SearchPatientResult>();
            while (searchResult.hasNext()) {
                EOSearchResultRecord resultRecord = searchResult.next();
                if (resultRecord != null && 
                    resultRecord.getObject() != null) {
                	PatientObject objectPatient = (PatientObject)resultRecord.getObject();
                	String euid = resultRecord.getEUID();
                	float weighted = resultRecord.getComparisonScore();
                	PatientBean bean = new PatientBean(objectPatient);
                	SearchPatientResult searchPatientResult = new SearchPatientResult(bean, euid, weighted);
                	pageData.add(searchPatientResult);
                }
            }

						searchResultPage.setPageSize(objSearchOptions.getPageSize());
            searchResultPage.setPageData(pageData);
            return searchResultPage;
        } catch (java.rmi.RemoteException rex) {
            throw new ProcessingException(rex);
        }
    }

    /** 
     * Does the block search on passed PatientBean attribute values
     *
     * @param objBean The Patient contains the search criteria.
     * @exception ProcessingException An error has occured.
     * @throws UserException Invalid euid (null or empty string)
     * @return SearchObjectResult[]
     */
    private SearchPatientResult[] pSearchBlock(
        PatientBean objBean)
    throws ProcessingException, UserException {

        if (mLogger.isDebugEnabled()) {
            mLogger.info("pSearchBlock:" + objBean); 
        }

        return pSearch(objBean, BLOCKER_SEARCH, true, Double.NEGATIVE_INFINITY);
    }

    /**
     * Does the enterprise object search on passed PatientBean attribute values
     *
     * @param objBean The PatientBean contains the search criteria.
     * @param searchId Valid search Id includes "BLOCKER-SEARCH", "ALPHA-SEARCH" and "PHONETIC-SEARCH".
     * @param weightOption Set true for "BLOCKER-SEARCH" and "PHONETIC-SEARCH" searchId, false for "ALPHA-SEARCH".
     * @param minimumWeight Minimum match score threshold, any records with match scores above the minimum weight return.   
     * @exception ProcessingException An error has occured.
     * @throws UserException Invalid euid (null or empty string)
     * @return SearchPatientResult[]
     */
    @WebMethod
    @WebResult(name="searchPatientResult")
    public SearchPatientResult[] searchByWeight(
        @WebParam(name = "objBean") PatientBean objBean,
        @WebParam(name = "searchId") String searchId,
        @WebParam(name = "weightOption") boolean weightOption,
        @WebParam(name = "minimumWeight") double minimumWeight)
    throws ProcessingException, UserException {

        if (mLogger.isDebugEnabled()) {
            mLogger.info("Search:" + objBean);
        }
        
        return (SearchPatientResult[]) pSearch(objBean, searchId, weightOption, minimumWeight);
    }
    
    /** 
     * Does the block search on passed PatientBean attribute values
     *
     * @param objBean The Patient contains the search criteria.
     * @exception ProcessingException An error has occured.
     * @throws UserException Invalid euid (null or empty string)
     * @return SearchPatientResult[]
     */
    @WebMethod
    @WebResult(name="searchPatientResult")
    public SearchPatientResult[] searchBlock(
        @WebParam(name = "objBean") PatientBean objBean)
    throws ProcessingException, UserException {

        if (mLogger.isDebugEnabled()) {
            mLogger.info("SearchBlock:" + objBean);   
        }

        return (SearchPatientResult[]) pSearchBlock(objBean);             
    }
    
    /** 
     * Return EnterpiseObject associated with EUID or null if not found.
     *
     * @param euid The EUID on which to perform the action.
     * @exception ProcessingException An error has occured.
     * @throws UserException Invalid euid (null or empty string)
     * @return EnterpriseObject for given EUID or null if not found.
     */
    private EnterprisePatient pGetEnterpriseRecordByEUID(
        String euid)
    throws ProcessingException, UserException {

        if (mLogger.isDebugEnabled()) {
            mLogger.info("pGetEnterpriseRecordByEUID:" + euid); 
        }

        EnterpriseObject eo = mMC.getEnterpriseObject(euid);

        if (null == eo) {
           return null;
        }

        return new EnterprisePatient(eo);
    }

    /** 
     * Return EnterpiseObject associated with EUID or null if not found.
     *
     * @param euid The EUID on which to perform the action.
     * @exception ProcessingException An error has occured.
     * @throws UserException Invalid euid (null or empty string)
     * @return EnterprisePatient for given EUID or null if not found.
     */
    @WebMethod
    @WebResult(name="enterprisePatient")
    public EnterprisePatient getEnterpriseRecordByEUID(
        @WebParam(name = "euid") String euid)
    throws ProcessingException, UserException {

        if (mLogger.isDebugEnabled()) {
            mLogger.info("getEnterpriseRecordByEUID:" + euid);
        }

        return (EnterprisePatient) pGetEnterpriseRecordByEUID(euid);
    }

   /** 
     * Return EnterpriseObject associated with a system object key or null if
     * not found.  Only active system objects can be used for the lookup.
     *
     * @param key The system object key on which to perform the action.
     * @exception ProcessingException An error has occured.
     * @throws UserException Invalid key (null or empty string)
     * @return EnterpriseObject for given key.
     */
    private EnterprisePatient pGetEnterpriseRecordByLID(
        String systemCode, 
        String localid)
    throws ProcessingException, UserException {

        if (mLogger.isDebugEnabled()) {
            mLogger.info("pGetEnterpriseRecordByLID:" + localid + ",systemCode:" + systemCode);
        }

        SystemObjectPK skey = new SystemObjectPK(systemCode, localid);            
        EnterpriseObject eo = mMC.getEnterpriseObject(skey);

        if (null == eo) {
            return null;
        }

        return new EnterprisePatient(eo);
    }

   /** 
     * Return EnterpriseObject associated with a system object key or null if
     * not found.  Only active system objects can be used for the lookup.
     *
     * @param key The system object key on which to perform the action.
     * @exception ProcessingException An error has occured.
     * @throws UserException Invalid key (null or empty string)
     * @return EnterprisePatient for given key.
     */
    @WebMethod
    @WebResult(name="enterprisePatient")
    public EnterprisePatient getEnterpriseRecordByLID(    	
        @WebParam(name = "systemCode") String systemCode, 
        @WebParam(name = "localid") String localid)
        throws ProcessingException, UserException {

        if (mLogger.isDebugEnabled()) {
            mLogger.info("getEnterpriseRecordByLID:" + localid + ",systemCode:" + systemCode);
        }

        return (EnterprisePatient) pGetEnterpriseRecordByLID(systemCode, localid);
    }

    /** 
     * Return SBR associated with an EUID or null if not found.
     *
     * @param euid The EUID on which to perform the action.
     * @exception ProcessingException An error has occured.
     * @exception UserException Invalid euid (null or empty string)
     * @return SBR for given EUID.
     */
    private SBRPatient pGetSBR(
        String euid)
    throws ProcessingException, UserException  {

        if (mLogger.isDebugEnabled()) {
            mLogger.info("pGetSBR EUID:" + euid);
        }
 
        SBR sbr = mMC.getSBR(euid);

        if (null == sbr) {
           return null;
        }

        return new SBRPatient(sbr);

    }

    /** 
     * Return SBR associated with an EUID or null if not found.
     *
     * @param euid The EUID on which to perform the action.
     * @exception ProcessingException An error has occured.
     * @exception UserException Invalid euid (null or empty string)
     * @return SBRPatient for given EUID.
     */    
    @WebMethod
    @WebResult(name="sbrPatient")
    public SBRPatient getSBR(
        @WebParam(name = "euid")  String euid) 
    throws ProcessingException, UserException  {

        if (mLogger.isDebugEnabled()) {
            mLogger.info("getSBR EUID:" + euid);
        }

        return (SBRPatient) pGetSBR(euid);

    }
    
    /** 
     * Return EUID associated with a system object key or null if not found.
     *
     * @param key The system object key on which to perform the action
     * @exception ProcessingException An error has occured.
     * @throws UserException Invalid key (nulls or empty strings)
     * @return EUID for given key.
     */
    @WebMethod
    @WebResult(name="euid") 
    public String getEUID(
         @WebParam(name = "systemCode") String systemCode, 
         @WebParam(name = "localid") String localid)    
    throws ProcessingException, UserException {

        if (mLogger.isDebugEnabled()) {
            mLogger.info("getEUID systemCode:" + systemCode + ", localid:" + localid);
        }

        SystemObjectPK skey = new SystemObjectPK(systemCode, localid);

        return mMC.getEUID(skey);
    }




    /** 
     * Update the database to reflect the new values of the given modified
     * system object.
     *
     * @param sysobjBean The SO to be updated.
     * @exception ProcessingException An error has occured.
     * @exception UserException A user error occured
     */
    @WebMethod     
    public void updateSystemRecord(
        @WebParam(name="sysObjBean") SystemPatient sysObjBean)
    throws ProcessingException, UserException  {

        if (mLogger.isDebugEnabled()) {
            mLogger.info("updateSystemRecord:" + sysObjBean);
        }

        SystemObject so = sysObjBean.pGetSystemObject();                      
        mMC.updateSystemObject(so);
    }


    
   /** 
     * Update the database to reflect the new values of the given modified
     * enterprise object.
     *
     * @param eoBean The EO to be updated.
     * @exception ProcessingException An error has occured.
     * @exception UserException A user error occured
     */
    @WebMethod 
    public void updateEnterpriseRecord(
        @WebParam(name = "eoBean") EnterprisePatient eoBean)
    throws ProcessingException, UserException {

        if (mLogger.isDebugEnabled()) {
            mLogger.info("updateEnterpriseRecord:" + eoBean);
        }
 
        EnterpriseObject originalEO = mMC.getEnterpriseObject(eoBean.getEUID());
        EnterpriseObject eo = eoBean.pUpdateEnterpriseObject(originalEO);
        mMC.updateEnterpriseObject(eo);
    }
    
    


    /** 
     * Adds the SystemObject to the EnterpriseObject specified by EUID.
     *
     * @param euid The EUID on which to perform the action.
     * @param sysObj The system object to be added.
     * @exception ProcessingException An error has occured.
     * @exception UserException A user error occured
     */
    @WebMethod 
    public void addSystemRecord(
        @WebParam(name = "euid") String euid, 
        @WebParam(name = "sysObjBean") SystemPatient sysObjBean)
    throws ProcessingException, UserException {

        if (mLogger.isDebugEnabled()) {
            mLogger.info("addSystemRecord:" + sysObjBean);
        }

        SystemObject so = sysObjBean.pGetSystemObject();    
        mMC.addSystemObject(euid, so);
    }

    /** 
     * Merge the enterprise records based on the given EUID's.
     *
     * @param sourceEUID The EUID to be merged.
     * @param destinationEUID The EUID to be kept.
     * @param calculateOnly Indicate whether to commit changes to DB or just
     * compute the MergeResult.  See Constants.
     * @exception ProcessingException An error has occured.
     * @exception UserException Invalid euids
     * @return Result of merge operation.
     */
    private MergePatientResult pMergeEnterpriseRecord(
        String fromEUID, 
        String toEUID,
        boolean calculateOnly)
    throws ProcessingException, UserException  {

        if (mLogger.isDebugEnabled()) {
            mLogger.info("pMergeEnterpriseRecord fromEUID:" + fromEUID + ", toEUID" + toEUID );
        }
            
        MergeResult mresult = mMC.mergeEnterpriseObject(fromEUID, toEUID, calculateOnly);

        return new MergePatientResult(mresult);
        
    }

    /** 
     * Merge the enterprise records based on the given EUID's.
     *
     * @param sourceEUID The EUID to be merged.
     * @param destinationEUID The EUID to be kept.
     * @param calculateOnly Indicate whether to commit changes to DB or just
     * compute the MergeResult.  See Constants.
     * @exception ProcessingException An error has occured.
     * @exception UserException Invalid euids
     * @return Result of merge operation.
     */
    @WebMethod
    @WebResult(name="mergePatientResult")
    public MergePatientResult mergeEnterpriseRecord(
        @WebParam(name = "fromEUID") String fromEUID, 
        @WebParam(name = "toEUID") String toEUID,
        @WebParam(name = "calculateOnly") boolean calculateOnly)
    throws ProcessingException, UserException  {

        if (mLogger.isDebugEnabled()) {
            mLogger.info("mergeEnterpriseRecord fromEUID:" + fromEUID + ", toEUID" + toEUID );
        }

        return (MergePatientResult) pMergeEnterpriseRecord(fromEUID, toEUID, calculateOnly);        
    }
 
 
    /**
     * Unmerges the two enterprise objects that were involved in the most
     * recent merge transaction for the specified EUID. 
     *
     * @param sourceEUID The EUID to be merged.
     * @param calculateOnly Indicate whether to commit changes to the database or just
     * compute the unmerge result.
     * @return Result of unmerge operation.
     * @exception ProcessingException An unmerge processing error has occured.
     * @exception UserException Invalid parameters.
     */
    @WebMethod
    @WebResult(name = "mergePatientResult")
    public MergePatientResult unmergeEterpriseRecord(@WebParam(name = "uEUID") String uEUID,
                                       @WebParam(name = "calculateOnly") boolean calculateOnly)
    throws ProcessingException, UserException {
           MergeResult umResult = mMC.unmergeEnterpriseObject(uEUID, calculateOnly);
           return new MergePatientResult(umResult);
    }


   /** 
     * Return SystemObject associated with a key or null if not found.
     *
     * @param key The system object key on which to perform the action.
     * @exception ProcessingException An error has occured.
     * @exception UserException Invalid key (null or empty string)
     * @return SystemObject for given key or null if not found.
     */
    private SystemPatient pGetSystemRecord(
        String systemCode, 
        String localid)
    throws ProcessingException, UserException {

        if (mLogger.isDebugEnabled()) {
            mLogger.info("pGetSystemRecord systemCode:" + systemCode + ", localid" + localid );
        }

        SystemPatient s = null;
        SystemObjectPK skey = new SystemObjectPK(systemCode, localid);
        SystemObject so = mMC.getSystemObject(skey);

        if (null != so) {
            s = new SystemPatient(so);
        }
            
        return s;
    }

   /** 
     * Return SystemObject associated with a key or null if not found.
     *
     * @param key The system object key on which to perform the action.
     * @exception ProcessingException An error has occured.
     * @exception UserException Invalid key (null or empty string)
     * @return SystemPatient for given key or null if not found.
     */    
    @WebMethod
    @WebResult(name="systemPatient")
    public SystemPatient getSystemRecord(
        @WebParam(name = "systemCode") String systemCode, 
        @WebParam(name = "localid") String localid)
    throws ProcessingException, UserException {

        if (mLogger.isDebugEnabled()) {
            mLogger.info("getSystemRecord systemCode:" + systemCode + ", localid" + localid );
        }

        return (SystemPatient) pGetSystemRecord(systemCode, localid);
    }
    
    /** Lookup active system objects only for the given EUID.
     *
     * @param euid The EUID on which to perform the action.
     * @exception ProcessingException An error has occured.
     * @exception UserException Invalid euid 
     * @return Array of system objects.
     */
    private SystemPatient[] pGetSystemRecords(
        String euid)
    throws ProcessingException, UserException {

        if (mLogger.isDebugEnabled()) {
            mLogger.info("pGetSystemRecord EUID:" + euid );
        }

        SystemPatient[] systemPatients = null;
        SystemObject[] systemObjects = mMC.lookupSystemObjects(euid);

        if (null != systemObjects) {
            int l = systemObjects.length;
            systemPatients = new SystemPatient[l];
            for (int i = 0; i < l; i++) {
                systemPatients[i] = new SystemPatient(systemObjects[i]); 
            }
        }
        
        return systemPatients;
    }

    /** Lookup active system objects only for the given EUID.
     *
     * @param euid The EUID on which to perform the action.
     * @exception ProcessingException An error has occured.
     * @exception UserException Invalid euid 
     * @return Array of SystemPatient.
     */
    @WebMethod
    @WebResult(name="systemPatients")
    public SystemPatient[] getSystemRecordsByEUID(
        @WebParam(name = "euid") String euid)
    throws ProcessingException, UserException {

        if (mLogger.isDebugEnabled()) {
            mLogger.info("getSystemRecordsByEUID EUID:" + euid );
        }

        return (SystemPatient[]) pGetSystemRecords(euid);
    }

    /** 
     * Lookup system objects with the given EUID and status
     *
     * @param euid The EUID on which to perform the action.
     * @param status Status filter.
     * @exception ProcessingException An error has occured.
     * @exception UserException Invalid parameters
     * @return Array of system objects.
     */
    private SystemPatient[] pGetSystemRecords(
        String euid, 
        String status)
    throws ProcessingException, UserException {

        if (mLogger.isDebugEnabled()) {
            mLogger.info("pGetSystemRecords EUID:" + euid +", status:" + status );
        }

        SystemPatient[] systemPatients = null;
        SystemObject[] systemObjects = mMC.lookupSystemObjects(euid, status);

        if (null != systemObjects) {
            int l = systemObjects.length;
            systemPatients = new SystemPatient[l];
            for (int i = 0; i < l; i++) {
                systemPatients[i] = new SystemPatient(systemObjects[i]); 
            }
        }
        
        return systemPatients;
    }

    /** 
     * Lookup system objects with the given EUID and status
     *
     * @param euid The EUID on which to perform the action.
     * @param status Status filter.
     * @exception ProcessingException An error has occured.
     * @exception UserException Invalid parameters
     * @return Array of SystemPatient.
     */
    @WebMethod
     @WebResult(name="systemPatients")
    public SystemPatient[] getSystemRecordsByEUIDStatus(
        @WebParam(name = "euid") String euid, 
        @WebParam(name = "status") String status)
    throws ProcessingException, UserException {

        if (mLogger.isDebugEnabled()) {
            mLogger.info("getSystemRecordsByEUIDStatus EUID:" + euid +", status:" + status );
        }
        
        return (SystemPatient[]) pGetSystemRecords(euid, status);
    }


   /** 
     * Returns an array of all system object keys belonging to the given EUID.
     *
     * @param euid The EUID on which to perform the action.
     * @exception ProcessingException An error has occured.
     * @exception UserException Invalid euid 
     * @return Array of system object keys.
     */
    private SystemPatientPK[] pGetLIDs(String euid)
    throws ProcessingException, UserException {

        if (mLogger.isDebugEnabled()) {
            mLogger.info("pGetLIDs EUID:" + euid);
        }

        SystemPatientPK[] objNamePKArr = null;
        SystemObjectPK[] sysObjPKArr = mMC.lookupSystemObjectPKs(euid);
        if (null != sysObjPKArr) {
            int count = sysObjPKArr.length;
            objNamePKArr = new SystemPatientPK[count];
            for (int i = 0; i < count; i++) {
                objNamePKArr[i] = new SystemPatientPK(sysObjPKArr[i]);
            }
        }
        
        return objNamePKArr;
    }

   /** 
     * Returns an array of all system object keys belonging to the given EUID.
     *
     * @param euid The EUID on which to perform the action.
     * @exception ProcessingException An error has occured.
     * @exception UserException Invalid euid 
     * @return Array of SystemPatientPK.
     */
    @WebMethod
    @WebResult(name="systemPatientPKs")
    public SystemPatientPK[] getLIDs(
        @WebParam(name = "euid") String euid)
    throws ProcessingException, UserException {

        if (mLogger.isDebugEnabled()) {
            mLogger.info("getLIDs EUID:" + euid);
        }

        return (SystemPatientPK[]) pGetLIDs(euid);
    }

    /** 
     * Returns an array of system object keys with the given status belonging 
     * to the given EUID.
     *
     * @param euid The EUID on which to perform the action.
     * @param status Status filter.
     * @exception ProcessingException An error has occured.
     * @exception UserException Invalid euid or status
     * @return Array of system object keys or null.
     */
    private SystemPatientPK[] pGetLIDsByStatus(
        String euid, 
        String status)
    throws ProcessingException, UserException {

        if (mLogger.isDebugEnabled()) {
            mLogger.info("pGetLIDs EUID:" + euid +", status:" + status);
        }

        SystemPatientPK[] objNamePKArr = null;            
        SystemObjectPK[] sysObjPKArr = mMC.lookupSystemObjectPKs(euid, status);
        if (null != sysObjPKArr) {
            int count = sysObjPKArr.length;
            objNamePKArr = new SystemPatientPK[count];
            for (int i = 0; i < count; i++) {
                objNamePKArr[i] = new SystemPatientPK(sysObjPKArr[i]);
            }
        }
       
        return objNamePKArr;
   }

    /** 
     * Returns an array of system object keys with the given status belonging 
     * to the given EUID.
     *
     * @param euid The EUID on which to perform the action.
     * @param status Status filter.
     * @exception ProcessingException An error has occured.
     * @exception UserException Invalid euid or status
     * @return Array ofSystemPatientPK or null.
     */
    @WebMethod
    @WebResult(name="systemPatientPKs")
    public SystemPatientPK[] getLIDsByStatus(
        @WebParam(name = "euid") String euid, 
        @WebParam(name = "status") String status)
    throws ProcessingException, UserException {

        if (mLogger.isDebugEnabled()) {
            mLogger.info("pGetLIDs EUID:" + euid +", status:" + status);
        }

        return (SystemPatientPK[]) pGetLIDsByStatus(euid, status);
    }
    
    
    /** 
     * Returns an array of system object keys with the given status belonging 
     * to the given destination system whose EUID matches the source system 
     * code / lid.
     *
     * @param sourceSystemCode the source system code
     * @param sourceLID the source local id
     * @param destSystemCode the destination system code
     * @param status status of records in destination system to search for
     * @exception ProcessingException See MasterControllerEJB
     * @exception UserException See MasterControllerEJB
     * @return Array of system object keys or null if not found
     */
    private SystemPatientPK[] pLookupLIDs(
        String sourceSystemCode, 
        String sourceLID, 
        String destSystemCode, 
        String status)
    throws ProcessingException, UserException {

        if (mLogger.isDebugEnabled()) {
            mLogger.info("pLookupLIDs sourceSystemCode:" + sourceSystemCode + 
                ", sourceLID:" + sourceLID + ", destSystemCode:" + destSystemCode + ", status:" +
                status );
        }

        SystemPatientPK[] objNamePKArr = null;
        SystemObjectPK[] sysObjPKArr = mMC.lookupSystemObjectPKs(sourceSystemCode, sourceLID,  destSystemCode,  status);
        if (null != sysObjPKArr) {
            int count = sysObjPKArr.length;
            objNamePKArr = new SystemPatientPK[count];
            for (int i = 0; i < count; i++) {
                objNamePKArr[i] = new SystemPatientPK(sysObjPKArr[i]);
            }
        }
        
        return objNamePKArr;    
   }

    /** 
     * Returns an array of system object keys with the given status belonging 
     * to the given destination system whose EUID matches the source system 
     * code / lid.
     *
     * @param sourceSystemCode the source system code
     * @param sourceLID the source local id
     * @param destSystemCode the destination system code
     * @param status status of records in destination system to search for
     * @exception ProcessingException See MasterControllerEJB
     * @exception UserException See MasterControllerEJB
     * @return Array of SystemPatientPK or null if not found
     */
    @WebMethod
    @WebResult(name="systemPatientPKs")
    public SystemPatientPK[] lookupLIDs(
        @WebParam(name = "sourceSystemCode") String sourceSystemCode, 
        @WebParam(name = "sourceLID") String sourceLID, 
        @WebParam(name = "destSystemCode") String destSystemCode, 
        @WebParam(name = "status") String status)
    throws ProcessingException, UserException {

        if (mLogger.isDebugEnabled()) {
            mLogger.info("lookupLIDs sourceSystemCode:" + sourceSystemCode + 
                ", sourceLID:" + sourceLID + ", destSystemCode:" + destSystemCode + ", status:" +
                status );
        }

        return (SystemPatientPK[]) pLookupLIDs(sourceSystemCode, sourceLID, destSystemCode, status);
    }
            
    
  
    /** 
     * Merge the two lids for the given system.  Note that the keys may both 
     * belong to a single EO, or may belong to two different EO's.
     *
     * @param systemCode The system to which these local id's belong.
     * @param sourceLID The lid to be merged.
     * @param destLID The lid to be kept.
     * @param calculateOnly Indicate whether to commit changes to DB or just
     * compute the MergeResult.  See Constants.
     * @exception ProcessingException See MasterControllerEJB
     * @exception UserException See MasterControllerEJB
     * @return See MasterControllerEJB
     */
    private MergePatientResult pMergeSystemRecord(
        String systemCode, 
        String sourceLID,
        String destLID, 
        boolean calculateOnly)
    throws ProcessingException, UserException {

        if (mLogger.isDebugEnabled()) {
            mLogger.info("pMergeSystemRecord systemCode:" + systemCode + 
                ", sourceLID:" + sourceLID + ", destLID:" + destLID + ", calculateOnly:" +
                calculateOnly );
        }

        MergePatientResult mr = null;
        MergeResult mresult = mMC.mergeSystemObject(systemCode, sourceLID,
                destLID, calculateOnly);

        return new MergePatientResult(mresult);
   }
  
    /** 
     * Merge the two lids for the given system.  Note that the keys may both 
     * belong to a single EO, or may belong to two different EO's.
     *
     * @param systemCode The system to which these local id's belong.
     * @param sourceLID The lid to be merged.
     * @param destLID The lid to be kept.
     * @param calculateOnly Indicate whether to commit changes to DB or just
     * compute the MergeResult.  See Constants.
     * @exception ProcessingException See MasterControllerEJB
     * @exception UserException See MasterControllerEJB
     * @return MergePatientResult
     */   
   @WebMethod
   @WebResult(name="mergePatientResult")
   public MergePatientResult mergeSystemRecord(
       @WebParam(name = "systemCode") String systemCode, 
       @WebParam(name = "sourceLID") String sourceLID,
       @WebParam(name = "destLID") String destLID, 
       @WebParam(name = "calculateOnly") boolean calculateOnly)
   throws ProcessingException, UserException {

        if (mLogger.isDebugEnabled()) {
            mLogger.info("mergeSystemRecord systemCode:" + systemCode + 
                ", sourceLID:" + sourceLID + ", destLID:" + destLID + ", calculateOnly:" +
                calculateOnly );
        }

        return (MergePatientResult) pMergeSystemRecord(systemCode, sourceLID, 
                  destLID, calculateOnly);
   }

   /**
     * Unmerges the two system objects that were involved in the most recent merge
     * transaction for the specified local identifier. The system object that had a
     * status of "merged" is changed to an active object after the unmerge. If the
     * source enterprise object has more than one active system object after the
     * unmerge and the update mode is set to pessimistic, the application checks
     * whether any key fields were updated in that object. If key fields were updated,
     * potential duplicates are recalculated for the source enterprise object.
     *
     * @param systemCode The system to which the system objects to be unmerged belong.
     * @param sourceLID The local Id of the non-surviving system object.
     * @param destLID The local Id of the surviving system object.
     * @param calculateOnly An indicator of whether commit changes to the database or
     * to simply compute the unmerge results.
     * @return The result of the unmerge transaction.
     * @exception ProcessingException An unmerg processing error occurs.
     * @exception UserException Invalid parameters
     */
    @WebMethod
    @WebResult(name = "mergePatientResult")
    public MergePatientResult unmergeSystemRecord(
            @WebParam(name = "systemCode") String systemCode,
            @WebParam(name = "sourceLID") String sourceLID,
            @WebParam(name = "destLID") String destLID,
            @WebParam(name = "calculateOnly") boolean calculateOnly)
    throws ProcessingException, UserException {
            MergeResult umResult = mMC.unmergeSystemObject(systemCode, sourceLID, destLID, calculateOnly);
            return new MergePatientResult(umResult);
    }

    /** Return a deactivated system object back to active status.
     *
     * @param systemKey The system object key on which to perform the action.
     * @exception ProcessingException An error has occured.
     * @exception UserException Invalid key (null or empty string)
     */
    @WebMethod 
    public void activateSystemRecord(
       @WebParam(name = "systemCode") String systemCode, 
       @WebParam(name = "localid") String localid)
    throws ProcessingException, UserException {

        if (mLogger.isDebugEnabled()) {
            mLogger.info("activateSystemRecord systemCode:" + systemCode + 
                ", localid:" + localid);
        }

        SystemObjectPK skey = new SystemObjectPK(systemCode, localid);
        mMC.activateSystemObject(skey);
   }

    
    /** 
     * Return a deactivated enterprise object back to active status.
     *
     * @param euid The euid on which to perform the action.
     * @exception ProcessingException An error has occured.
     * @exception UserException Invalid euid (null or empty string)
     */
    @WebMethod
    public void activateEnterpriseRecord(
        @WebParam(name = "euid") String euid)
    throws ProcessingException, UserException {

        if (mLogger.isDebugEnabled()) {
            mLogger.info("activateSystemRecord euid:" + euid);
        }
            
        mMC.activateEnterpriseObject(euid);            
   }
    
    
    /** 
     * Deactivate a system object based on the given key. Note that this 
     * is different than deleteSystemObject in that the record is not removed
     * from the database, only its status is changed.
     *
     * @param systemKey The system object key on which to perform the action.
     * @exception ProcessingException An error has occured.
     * @exception UserException Invalid key (null or empty string)
     */
    @WebMethod 
    public void deactivateSystemRecord(
        @WebParam(name = "systemCode") String systemCode, 
        @WebParam(name = "localid") String localid)
    throws ProcessingException, UserException {

        if (mLogger.isDebugEnabled()) {
            mLogger.info("deactivateSystemRecord systemCode:" + systemCode + 
                ", localid:" + localid);
        }

        SystemObjectPK skey = new SystemObjectPK(systemCode, localid);
        mMC.deactivateSystemObject(skey);
    }

    
   /** 
     * Deactivate enterprise object based on key.
     *
     * @param euid The euid on which to perform the action.
     * @exception ProcessingException An error has occured.
     * @exception UserException Invalid euid (null or empty string)
     */
    @WebMethod 
    public void deactivateEnterpriseRecord(
        @WebParam(name = "euid") String euid)
    throws ProcessingException, UserException {

        if (mLogger.isDebugEnabled()) {
            mLogger.info("deactivateSystemRecord euid:" + euid);
        }

        mMC.deactivateEnterpriseObject(euid);
   }
    

    /** 
     * Transfer system object from one enterprise object to another
     *
     * @param destinationEUID The EUID to transfer the SO to.
     * @param systemKey The key of the SO to transfer.
     * @exception ProcessingException An error has occured.
     * @exception UserException Invalid parameters
     */
    @WebMethod 
    public void transferSystemRecord(
        @WebParam(name = "toEUID") String toEUID, 
        @WebParam(name = "systemCode") String systemCode, 
        @WebParam(name = "localid") String localid)
    throws ProcessingException, UserException {

        if (mLogger.isDebugEnabled()) {
            mLogger.info("transferSystemRecord toEUID:" + toEUID + ", systemCode:" + systemCode +
                ", localid:" + localid);
        }
 
        SystemObjectPK skey = new SystemObjectPK(systemCode, localid);
        mMC.transferSystemObject(toEUID, skey);
    }
    
    /**
     * Returns an array of potential duplicate record based on search
     * criteria.
     * @param pdsoBean PotentialDuplicateSearchObjectBean
     * @exception ProcessingException An error has occured.
     * @exception UserException Invalid search object.
     * @exception PageException An error has occured.
     * @exception RemoteException An error has occured.
     * @return an array of PotentialDuplicateResult.
     */
    @WebMethod
    @WebResult(name="potentialDuplicateResult")
    public PotentialDuplicateResult[] lookupPotentialDuplicates(
            @WebParam(name = "potentialDuplicateSearchObjectBean")PotentialDuplicateSearchObjectBean pdsoBean) 
            throws ProcessingException, UserException, PageException, RemoteException{
        //if no value of pageSize or pageNumber are set(they default to 0), or 
        //they are set to 0 or less, then retrieve maximum records.
        int startIndex=(pdsoBean.getPageNumber()-1) * pdsoBean.getPageSize()+1;
        int endIndex=pdsoBean.getPageNumber() * pdsoBean.getPageSize();
        if (pdsoBean.getPageNumber()<1 || pdsoBean.getPageSize()<1){
            startIndex = 1;            
            if (pdsoBean.getMaxElements()>0){
                endIndex = pdsoBean.getMaxElements();
            }else{
                endIndex = Constants.DEFAULT_MAX_ELEMENTS;
            }                   
        }

        PotentialDuplicateIterator iterator = 
                mMC.lookupPotentialDuplicates(pdsoBean.getPotentialDuplicateSearchObject());
        
        
        int count =0;
        ArrayList<PotentialDuplicateResult> results = new ArrayList<PotentialDuplicateResult>();
        while (iterator.hasNext()) {
            count++;
            if (count>endIndex){
                break;
            }
            PotentialDuplicateSummary pds = iterator.next();
            if (count>=startIndex){
                PotentialDuplicateResult pdbresult = new PotentialDuplicateResult(pds);
                results.add(pdbresult);
            }
        }
        int size = results.size();
        PotentialDuplicateResult[] pdbarray= new PotentialDuplicateResult[size];
        return results.toArray(pdbarray);        
    }
 
   /**
    * Get the configured match threshold.
    * @returns the value of the match threshold.
    */
    @WebMethod
    @WebResult(name="matchthreshold")
    public float getMatchThreshold() {
       	return mMC.getAssumedMatchThreshold();
    }

   /**
    * Get the configured unmatch threshold.
    * @returns the value of the unmatch threshold.
    */
    @WebMethod
    @WebResult(name="unmatchthreshold")
    public float getUnmatchThreshold() {
       	return mMC.getDuplicateThreshold();
    }
     
    /** 
     * Refresh cached code list and system codes from database tables.
     * @exception ProcessingException An error has occured.
     */
     @WebMethod 
     public void refresh()
    		throws ProcessingException {
       	mMC.refresh();
     }

    /**
     * Add a new system definition if the given parameters are valid and the
     * system code does not exist in the system definition table; update
     * the existing system definition with attributes changes if the given
     * parameters are valid and the system code exists in the system definition
     * table; otherwise UserException is thrown.
     * The required system definition attributes are systemcode, id_length and format.
     * The rest are optional.
     * @param  SystemDefinition A set of source system attributes.
     * @exception ProcessingException A processing error has occured.
     * @exception UserException Invalid sorce system attribtes.
     */
    @WebMethod
    public void addSystemDefinition(
        @WebParam(name = "systemDefinition") SystemDefinition systemDefinition)
        throws ProcessingException, UserException {
        mMC.addSystemDefinition(systemDefinition);
    }

   /**
     * Compare two Patient records.
     * 
     * @param SystemPatient Candidate record
     * @param SystemPatient Reference record
     * @param PatientMatchOption Match option. matchThreshold: use-defined match
     *        threshold adjustable ratio ranged from -1.0  to + 1.0.  By default  is 0, no adjustment.
     *        matchStrategy: ALPHA-MATCH  exact search; ADVANCED-MATCH  use probabilistic match
     * @return PatientMatchResult Match result. matchScore: returns match score.
     *         It is valid only for probabilistic match. matchCode: returns match code of match processing.
     *         NOT_MATCH  -  two records not matched.
     *         NAME_MATCH  -  two records matched on first name/last name.
     *         NICK_NAME_MATCH  -  two records matched on first name/last name/nick names.
     *         FORMER_NAME_MATCH  - two records matched on first name/former last names.
     * @exception ProcessingException A processing error has occured.
     * @exception UserException Invalid parameters
     */
    @WebMethod
    @WebResult(name="systemMatchResult")
    public PatientMatchResult matchPatientRecord(
        @WebParam(name = "candidateSystemPerson") SystemPatient candidateSystemPatient,
        @WebParam(name = "referenceSystemPerson") SystemPatient referenceSystemPatient,
        @WebParam(name = "systemPatientMatchOption") PatientMatchOption systemPatientMatchOption)
        throws ProcessingException, UserException{

        SystemObject candidateSystemObject = candidateSystemPatient.pGetSystemObject();
        SystemObject referenceSystemObject = referenceSystemPatient.pGetSystemObject();
        SystemMatchOption systemMatchOption = systemPatientMatchOption.getSystemMatchOption();

        SystemMatchResult systemMatchResult = mMC.matchSystemObject(candidateSystemObject,
                                                                    referenceSystemObject,
                                                                    systemMatchOption);
        return new PatientMatchResult(systemMatchResult);
    }


    /**
     * Get all source system definitions from the current system table.
     *
     * @return  SystemDefinition A set of source system definitions.
     * @exception ProcessingException A processing error has occured.
     */
    @WebMethod
    @WebResult(name="systemDefinitions")
    public SystemDefinition[] getSystemDefinitions ()
        throws ProcessingException{
        return mMC.lookupSystemDefinitions();
    }

    /** 
     * Search enterprise objects for given criteria
     *
     * @param objBean Search criteria
     * @param searchId Search Id BLOCKER-SEARCH, ALPHA-SEARCH and PHONETIC-SEARCH
     * @param weightOption true for BLOCKER-SEARCH, false for ALPHA-SEARCH and PHONETIC-SEARCH
     * @param minimumWeight minimum weight to return
     * @exception ProcessingException An processing error has occured.
     * @throws UserException Invalid search parameters
     * @return a list of enterprise objects
     */
    private SearchPatientResult[] pSearch(PatientBean objBean, String searchId, boolean weightOption, double minimumWeight)
    	throws ProcessingException, UserException {
        try {

            PatientBean aPatientBean = (PatientBean) objBean;
            EPathArrayList fields = new EPathArrayList();
            fields.add("Enterprise.SystemSBR." + "Patient" + ".EUID");
            SystemPatient sysPatient = new SystemPatient();
            sysPatient.setPatient(aPatientBean);
            SystemObject so = sysPatient.pGetSystemObject();

            String path = "Enterprise.SystemSBR." + "Patient";        
            populateEPathList(fields, path); 
        
            EOSearchOptions searchOptions = new EOSearchOptions(searchId, fields);
            searchOptions.setWeighted(weightOption);
            searchOptions.setMinimumWeight(minimumWeight);
            EOSearchCriteria criteria = new EOSearchCriteria(so);

            EOSearchResultIterator iterator = mMC.searchEnterpriseObject(criteria, searchOptions);
            ArrayList results = new ArrayList();
            while (iterator.hasNext()) {
                EOSearchResultRecord resultRecord = iterator.next();
                ObjectNode objectNode = resultRecord.getObject();
                String euid = resultRecord.getEUID();
                float weighted = resultRecord.getComparisonScore();
                PatientBean bean = new PatientBean ((PatientObject)objectNode);
                SearchPatientResult searchResult = new SearchPatientResult(bean, euid, weighted);
                results.add(searchResult);
            }

            int size = results.size();
            SearchPatientResult[] searchBeanResults = new SearchPatientResult[size];
            return (SearchPatientResult[])results.toArray(searchBeanResults);
        } catch (UserException ux) {
            throw ux;
        } catch (ProcessingException px) {
            throw px;
        } catch (Exception ex) {
            throw new ProcessingException(ex);
        }        
    }

    /** 
     * Populate EPath list.
     *
     * @param fields EPath array list.
     * @param path EPath.
     * @exception ProcessingException An error has occured.
     */
    private void populateEPathList(EPathArrayList fields, String path)
    	throws ProcessingException {
        try {
            
            String[] keyPaths = MetaDataService.getObjectFK(path);
            
            String[] fieldPaths = MetaDataService.getFieldPaths(path);
            for (int i = 0; i < fieldPaths.length; i++) {
                String fd = fieldPaths[i];
                boolean found = false;
                for (int j = 0; keyPaths != null && j < keyPaths.length; j++) {
                  String keyField = keyPaths[j];
                  int beginIndex = keyField.indexOf('.');
                  String keyStr = keyField.substring(beginIndex);
                  if (fd.endsWith(keyStr)) {
                      found = true;
                      break;
                  }
                }
                if ( found == false) {             
                   fields.add(fd);
                }
            }
            
            String childPaths[] = MetaDataService.getChildTypePaths(path);
            for (int i = 0; childPaths != null && i < childPaths.length; i++) {
                String childPath = childPaths[i];
                populateEPathList(fields, childPath);
             }
               
        } catch (Exception ex) {
            throw new ProcessingException(ex);
        }
    }
}
