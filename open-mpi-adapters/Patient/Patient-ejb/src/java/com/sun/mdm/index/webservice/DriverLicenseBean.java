/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */

package com.sun.mdm.index.webservice;

import java.util.*;
import com.sun.mdm.index.objects.exception.*;
import com.sun.mdm.index.objects.*;
import com.sun.mdm.index.objects.metadata.MetaDataService;
import java.text.SimpleDateFormat;
import java.text.ParsePosition;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public final class DriverLicenseBean implements ObjectBean
{
    public static final int version = 1;

    private DriverLicenseObject mDriverLicenseObject;
    private ClearFieldObject mClearFieldObj;
    private static java.text.SimpleDateFormat mDateFormat = new SimpleDateFormat(MetaDataService.getDateFormat());

    /**
     * Creates a new DriverLicenseBean instance.
     * @throws  ObjectException If creation fails. 
     */ 
    public DriverLicenseBean() throws ObjectException
    { 
       mDriverLicenseObject = new DriverLicenseObject();
    }
    
    /**
     * Creates a new DriverLicenseBean instance from a ClearFieldObject.
     */ 
    public DriverLicenseBean(ClearFieldObject clearFieldObj) throws ObjectException
    { 
       mDriverLicenseObject = new DriverLicenseObject();
       mClearFieldObj = clearFieldObj;
    }

    /**
     * Creates a new DriverLicenseBean instance from a DriverLicenseObject.
     */
    public DriverLicenseBean(DriverLicenseObject aDriverLicenseObject) throws ObjectException
    { 
       mDriverLicenseObject = aDriverLicenseObject;
    }
    
    /**
     * Creates a new DriverLicenseBean instance from 
     * a DriverLicenseObject and a ClearFieldObject.
     */
    public DriverLicenseBean(DriverLicenseObject aDriverLicenseObject,
      ClearFieldObject clearFieldObj) throws ObjectException
    { 
       mDriverLicenseObject = aDriverLicenseObject;
       mClearFieldObj = clearFieldObj;
    }
    
    /**
     * Getter for DriverLicenseId
     * @return a string value of DriverLicenseId
     */    
    public String getDriverLicenseId() throws ObjectException
    {
        try
        {
            int type = mDriverLicenseObject.pGetType("DriverLicenseId");
            Object value = mDriverLicenseObject.getValue("DriverLicenseId");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for Id
     * @return a string value of Id
     */    
    public String getId() throws ObjectException
    {
        try
        {
            int type = mDriverLicenseObject.pGetType("Id");
            Object value = mDriverLicenseObject.getValue("Id");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for IssuingState
     * @return a string value of IssuingState
     */    
    public String getIssuingState() throws ObjectException
    {
        try
        {
            int type = mDriverLicenseObject.pGetType("IssuingState");
            Object value = mDriverLicenseObject.getValue("IssuingState");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for ExpirationDate
     * @return a string value of ExpirationDate
     */    
    public String getExpirationDate() throws ObjectException
    {
        try
        {
            int type = mDriverLicenseObject.pGetType("ExpirationDate");
            Object value = mDriverLicenseObject.getValue("ExpirationDate");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Setter for DriverLicenseId
     * @param string value of DriverLicenseId
     */ 
    public void setDriverLicenseId(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mDriverLicenseObject.isNullable("DriverLicenseId")) {
               mDriverLicenseObject.clearField("DriverLicenseId");
            } else {
               int type = mDriverLicenseObject.pGetType("DriverLicenseId");
               Object val = strToObj(value, type, "DriverLicenseId");
          
               mDriverLicenseObject.setValue("DriverLicenseId", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for Id
     * @param string value of Id
     */ 
    public void setId(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mDriverLicenseObject.isNullable("Id")) {
               mDriverLicenseObject.clearField("Id");
            } else {
               int type = mDriverLicenseObject.pGetType("Id");
               Object val = strToObj(value, type, "Id");
          
               mDriverLicenseObject.setValue("Id", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for IssuingState
     * @param string value of IssuingState
     */ 
    public void setIssuingState(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mDriverLicenseObject.isNullable("IssuingState")) {
               mDriverLicenseObject.clearField("IssuingState");
            } else {
               int type = mDriverLicenseObject.pGetType("IssuingState");
               Object val = strToObj(value, type, "IssuingState");
          
               mDriverLicenseObject.setValue("IssuingState", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for ExpirationDate
     * @param string value of ExpirationDate
     */ 
    public void setExpirationDate(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mDriverLicenseObject.isNullable("ExpirationDate")) {
               mDriverLicenseObject.clearField("ExpirationDate");
            } else {
               int type = mDriverLicenseObject.pGetType("ExpirationDate");
               Object val = strToObj(value, type, "ExpirationDate");
          
               mDriverLicenseObject.setValue("ExpirationDate", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    
    public ObjectNode getObjectNode() {
        return mDriverLicenseObject;
    }

    /** 
     * Return DriverLicenseObject
     * @return DriverLicenseObject
     */ 
    public ObjectNode pGetObject() {
        return mDriverLicenseObject;
    }

    /** 
     * Getter for all children nodes
     * @return null because there is no child at the leaf
     */            
    public Collection pGetChildren() {            
         return null;
    }
    
    /** 
     * Getter for children of a specified type
     * @param type Type of children to retrieve
     * @return null because there is no child at the leaf
     */
    public Collection pGetChildren(String type) {
        return null;
    }

    /** 
     * Getter for child types
     * @return null because there is no child at the leaf
     */
    public ArrayList pGetChildTypes() {
        return null;
    }    

    /**
     * Count of all children
     * @return number of children
     */
    public int countChildren() {
        int count = 0;
        return count;
    }

    /**
     * Count of children of specified type
     * @param type of children to count
     * @return number of children of specified type
     */
    public int countChildren(String type) {
        int count = 0;
        return count;
    }
    
    /**
     * Delete itself from the parent and persist
     */
    public void delete() throws ObjectException {
        ObjectNode parent = mDriverLicenseObject.getParent();
        parent.deleteChild("DriverLicense", mDriverLicenseObject.pGetSuperKey()); 
    }
        
    // Find parent which is SystemObject    
    private SystemObject getParentSO() {
        ObjectNode obj = mDriverLicenseObject.getParent();
        
        while (obj != null) {
           if (obj instanceof SystemObject) {
              return (SystemObject) obj;
           } else {
              obj = obj.getParent();
           }
        }
        return (SystemObject) obj;
    }    
            
    static String objToString(Object value, int type) throws ObjectException {
        if (value == null) {
            return null;
        } else {
            if ( type == ObjectField.OBJECTMETA_STRING_TYPE) {
                return (String) value;
            }
            else if (type == ObjectField.OBJECTMETA_DATE_TYPE) {               
               return mDateFormat.format(value);              
            } else {
                return value.toString();
            }
        }
    }
    
    static Object strToObj(String str, int type, String fieldName) throws ObjectException {
        if (str == null || str.trim().length() == 0) {
            return null;
        } else if ( type == ObjectField.OBJECTMETA_STRING_TYPE) {
            return  str;
        } else if (type == ObjectField.OBJECTMETA_DATE_TYPE) {
            ParsePosition pos = new ParsePosition(0);
             Object ret = mDateFormat.parse(str, pos);   
            if ( ret == null) {
               throw new ObjectException("Invalid Date format of" + fieldName + ",value:" + str);
            }           
            return ret;             
        } else if (type == ObjectField.OBJECTMETA_INT_TYPE) {                
            return Integer.valueOf(str);              
        } else if (type == ObjectField.OBJECTMETA_FLOAT_TYPE) {                
            return Float.valueOf(str);              
        } else if (type == ObjectField.OBJECTMETA_LONG_TYPE) {                
            return Long.valueOf(str);             
        } else if (type == ObjectField.OBJECTMETA_BOOL_TYPE) {                
            return Boolean.valueOf(str);              
        } else if (type == ObjectField.OBJECTMETA_CHAR_TYPE) {                
            return (new Character(str.charAt(0)));                          
        } else {
            throw new ObjectException("Invalid type of" + fieldName + ",value:" + str);
        }
    }
}
