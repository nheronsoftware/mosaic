/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */

package com.sun.mdm.index.webservice;

import java.util.*;
import com.sun.mdm.index.objects.exception.*;
import com.sun.mdm.index.objects.*;
import com.sun.mdm.index.objects.metadata.MetaDataService;
import java.text.SimpleDateFormat;
import java.text.ParsePosition;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public final class IdentifierBean implements ObjectBean
{
    public static final int version = 1;

    private IdentifierObject mIdentifierObject;
    private ClearFieldObject mClearFieldObj;
    private static java.text.SimpleDateFormat mDateFormat = new SimpleDateFormat(MetaDataService.getDateFormat());

    /**
     * Creates a new IdentifierBean instance.
     * @throws  ObjectException If creation fails. 
     */ 
    public IdentifierBean() throws ObjectException
    { 
       mIdentifierObject = new IdentifierObject();
    }
    
    /**
     * Creates a new IdentifierBean instance from a ClearFieldObject.
     */ 
    public IdentifierBean(ClearFieldObject clearFieldObj) throws ObjectException
    { 
       mIdentifierObject = new IdentifierObject();
       mClearFieldObj = clearFieldObj;
    }

    /**
     * Creates a new IdentifierBean instance from a IdentifierObject.
     */
    public IdentifierBean(IdentifierObject aIdentifierObject) throws ObjectException
    { 
       mIdentifierObject = aIdentifierObject;
    }
    
    /**
     * Creates a new IdentifierBean instance from 
     * a IdentifierObject and a ClearFieldObject.
     */
    public IdentifierBean(IdentifierObject aIdentifierObject,
      ClearFieldObject clearFieldObj) throws ObjectException
    { 
       mIdentifierObject = aIdentifierObject;
       mClearFieldObj = clearFieldObj;
    }
    
    /**
     * Getter for IdentifierId
     * @return a string value of IdentifierId
     */    
    public String getIdentifierId() throws ObjectException
    {
        try
        {
            int type = mIdentifierObject.pGetType("IdentifierId");
            Object value = mIdentifierObject.getValue("IdentifierId");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for Id
     * @return a string value of Id
     */    
    public String getId() throws ObjectException
    {
        try
        {
            int type = mIdentifierObject.pGetType("Id");
            Object value = mIdentifierObject.getValue("Id");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for Type
     * @return a string value of Type
     */    
    public String getType() throws ObjectException
    {
        try
        {
            int type = mIdentifierObject.pGetType("Type");
            Object value = mIdentifierObject.getValue("Type");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for NamespaceId
     * @return a string value of NamespaceId
     */    
    public String getNamespaceId() throws ObjectException
    {
        try
        {
            int type = mIdentifierObject.pGetType("NamespaceId");
            Object value = mIdentifierObject.getValue("NamespaceId");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for UniversalId
     * @return a string value of UniversalId
     */    
    public String getUniversalId() throws ObjectException
    {
        try
        {
            int type = mIdentifierObject.pGetType("UniversalId");
            Object value = mIdentifierObject.getValue("UniversalId");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for UniversalIdType
     * @return a string value of UniversalIdType
     */    
    public String getUniversalIdType() throws ObjectException
    {
        try
        {
            int type = mIdentifierObject.pGetType("UniversalIdType");
            Object value = mIdentifierObject.getValue("UniversalIdType");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for EffectiveDate
     * @return a string value of EffectiveDate
     */    
    public String getEffectiveDate() throws ObjectException
    {
        try
        {
            int type = mIdentifierObject.pGetType("EffectiveDate");
            Object value = mIdentifierObject.getValue("EffectiveDate");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for ExpirationDate
     * @return a string value of ExpirationDate
     */    
    public String getExpirationDate() throws ObjectException
    {
        try
        {
            int type = mIdentifierObject.pGetType("ExpirationDate");
            Object value = mIdentifierObject.getValue("ExpirationDate");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Setter for IdentifierId
     * @param string value of IdentifierId
     */ 
    public void setIdentifierId(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mIdentifierObject.isNullable("IdentifierId")) {
               mIdentifierObject.clearField("IdentifierId");
            } else {
               int type = mIdentifierObject.pGetType("IdentifierId");
               Object val = strToObj(value, type, "IdentifierId");
          
               mIdentifierObject.setValue("IdentifierId", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for Id
     * @param string value of Id
     */ 
    public void setId(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mIdentifierObject.isNullable("Id")) {
               mIdentifierObject.clearField("Id");
            } else {
               int type = mIdentifierObject.pGetType("Id");
               Object val = strToObj(value, type, "Id");
          
               mIdentifierObject.setValue("Id", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for Type
     * @param string value of Type
     */ 
    public void setType(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mIdentifierObject.isNullable("Type")) {
               mIdentifierObject.clearField("Type");
            } else {
               int type = mIdentifierObject.pGetType("Type");
               Object val = strToObj(value, type, "Type");
          
               mIdentifierObject.setValue("Type", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for NamespaceId
     * @param string value of NamespaceId
     */ 
    public void setNamespaceId(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mIdentifierObject.isNullable("NamespaceId")) {
               mIdentifierObject.clearField("NamespaceId");
            } else {
               int type = mIdentifierObject.pGetType("NamespaceId");
               Object val = strToObj(value, type, "NamespaceId");
          
               mIdentifierObject.setValue("NamespaceId", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for UniversalId
     * @param string value of UniversalId
     */ 
    public void setUniversalId(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mIdentifierObject.isNullable("UniversalId")) {
               mIdentifierObject.clearField("UniversalId");
            } else {
               int type = mIdentifierObject.pGetType("UniversalId");
               Object val = strToObj(value, type, "UniversalId");
          
               mIdentifierObject.setValue("UniversalId", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for UniversalIdType
     * @param string value of UniversalIdType
     */ 
    public void setUniversalIdType(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mIdentifierObject.isNullable("UniversalIdType")) {
               mIdentifierObject.clearField("UniversalIdType");
            } else {
               int type = mIdentifierObject.pGetType("UniversalIdType");
               Object val = strToObj(value, type, "UniversalIdType");
          
               mIdentifierObject.setValue("UniversalIdType", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for EffectiveDate
     * @param string value of EffectiveDate
     */ 
    public void setEffectiveDate(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mIdentifierObject.isNullable("EffectiveDate")) {
               mIdentifierObject.clearField("EffectiveDate");
            } else {
               int type = mIdentifierObject.pGetType("EffectiveDate");
               Object val = strToObj(value, type, "EffectiveDate");
          
               mIdentifierObject.setValue("EffectiveDate", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for ExpirationDate
     * @param string value of ExpirationDate
     */ 
    public void setExpirationDate(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mIdentifierObject.isNullable("ExpirationDate")) {
               mIdentifierObject.clearField("ExpirationDate");
            } else {
               int type = mIdentifierObject.pGetType("ExpirationDate");
               Object val = strToObj(value, type, "ExpirationDate");
          
               mIdentifierObject.setValue("ExpirationDate", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    
    public ObjectNode getObjectNode() {
        return mIdentifierObject;
    }

    /** 
     * Return IdentifierObject
     * @return IdentifierObject
     */ 
    public ObjectNode pGetObject() {
        return mIdentifierObject;
    }

    /** 
     * Getter for all children nodes
     * @return null because there is no child at the leaf
     */            
    public Collection pGetChildren() {            
         return null;
    }
    
    /** 
     * Getter for children of a specified type
     * @param type Type of children to retrieve
     * @return null because there is no child at the leaf
     */
    public Collection pGetChildren(String type) {
        return null;
    }

    /** 
     * Getter for child types
     * @return null because there is no child at the leaf
     */
    public ArrayList pGetChildTypes() {
        return null;
    }    

    /**
     * Count of all children
     * @return number of children
     */
    public int countChildren() {
        int count = 0;
        return count;
    }

    /**
     * Count of children of specified type
     * @param type of children to count
     * @return number of children of specified type
     */
    public int countChildren(String type) {
        int count = 0;
        return count;
    }
    
    /**
     * Delete itself from the parent and persist
     */
    public void delete() throws ObjectException {
        ObjectNode parent = mIdentifierObject.getParent();
        parent.deleteChild("Identifier", mIdentifierObject.pGetSuperKey()); 
    }
        
    // Find parent which is SystemObject    
    private SystemObject getParentSO() {
        ObjectNode obj = mIdentifierObject.getParent();
        
        while (obj != null) {
           if (obj instanceof SystemObject) {
              return (SystemObject) obj;
           } else {
              obj = obj.getParent();
           }
        }
        return (SystemObject) obj;
    }    
            
    static String objToString(Object value, int type) throws ObjectException {
        if (value == null) {
            return null;
        } else {
            if ( type == ObjectField.OBJECTMETA_STRING_TYPE) {
                return (String) value;
            }
            else if (type == ObjectField.OBJECTMETA_DATE_TYPE) {               
               return mDateFormat.format(value);              
            } else {
                return value.toString();
            }
        }
    }
    
    static Object strToObj(String str, int type, String fieldName) throws ObjectException {
        if (str == null || str.trim().length() == 0) {
            return null;
        } else if ( type == ObjectField.OBJECTMETA_STRING_TYPE) {
            return  str;
        } else if (type == ObjectField.OBJECTMETA_DATE_TYPE) {
            ParsePosition pos = new ParsePosition(0);
             Object ret = mDateFormat.parse(str, pos);   
            if ( ret == null) {
               throw new ObjectException("Invalid Date format of" + fieldName + ",value:" + str);
            }           
            return ret;             
        } else if (type == ObjectField.OBJECTMETA_INT_TYPE) {                
            return Integer.valueOf(str);              
        } else if (type == ObjectField.OBJECTMETA_FLOAT_TYPE) {                
            return Float.valueOf(str);              
        } else if (type == ObjectField.OBJECTMETA_LONG_TYPE) {                
            return Long.valueOf(str);             
        } else if (type == ObjectField.OBJECTMETA_BOOL_TYPE) {                
            return Boolean.valueOf(str);              
        } else if (type == ObjectField.OBJECTMETA_CHAR_TYPE) {                
            return (new Character(str.charAt(0)));                          
        } else {
            throw new ObjectException("Invalid type of" + fieldName + ",value:" + str);
        }
    }
}
