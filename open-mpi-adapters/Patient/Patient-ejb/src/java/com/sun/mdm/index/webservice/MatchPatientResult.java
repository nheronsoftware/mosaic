/* *************************************************************************
 *
 *  Copyright (c) 2009, NetGen Software Inc., All Rights Reserved
 *
 *  This program, and all the routines referenced herein, are the proprietary
 *  properties and trade secrets of NetGen Software Inc.
 *
 *  Except as provided for by license agreement, this program shall not be
 *  duplicated, used, or disclosed without written consent signed by an officer
 *  of NetGen Software Inc.
 *
 ***************************************************************************/
 package com.sun.mdm.index.webservice;

import java.util.List;
import java.util.ArrayList;
import com.sun.mdm.index.master.MatchResult;
import com.sun.mdm.index.objects.PotentialDuplicate;
import com.sun.mdm.index.objects.exception.ObjectException;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Return value from Patient merge related functions.
 */
@XmlRootElement
public class MatchPatientResult {
    public static final int version = 1;
    
    private String mEUID;
    private int mResultCode;
    private List<PotentialBean> mPotentials;
    	    
    /**
     * No argument constructor.
     */ 
    public MatchPatientResult() {
    }
    
    /**
     * Creates a new MatchPatientResult instance from a MatchResult
     */ 
    public MatchPatientResult(MatchResult matchResult) {
        mEUID = matchResult.getEUID();
        mResultCode = matchResult.getResultCode();
        try {
            PotentialDuplicate[] duplicates = matchResult.getPotentialDuplicates();
            if (duplicates != null && duplicates.length > 0) {
                mPotentials = new ArrayList<PotentialBean>();
                for (PotentialDuplicate duplicate : duplicates) {
                     mPotentials.add(new PotentialBean(duplicate.getEUID1(), duplicate.getEUID2(), duplicate.getProbability()));
                }
            }
        } catch (ObjectException oex) {
        }    
    }
            
    /** Getter for EUID attribute of the MatchResult object
     * @return EUID
     */
    public String getEUID(){
        return mEUID;
    }
    
    /** 
     * Setter EUID
     * @param euid
     */
    public void setEUID(String euid){
        mEUID = euid;
    } 
    
    /** Getter for ResultCode
     * @return resultCode
     */
    public int getResultCode(){
        return mResultCode;
    }
    
    /** 
     * Setter for ResultCode
     * @param resultCode
     */
    public void setResultCode(int resultCode){
        mResultCode =resultCode;
    }
    
    public List<PotentialBean> getPotentials() {
        return this.mPotentials;
    }

    public void setPotentials(List<PotentialBean> potentials) {
        this.mPotentials = potentials;
    } 
}
