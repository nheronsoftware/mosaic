/* *************************************************************************
 *
 *  Copyright (c) 2009, NetGen Software Inc., All Rights Reserved
 *
 *  This program, and all the routines referenced herein, are the proprietary
 *  properties and trade secrets of NetGen Software Inc.
 *
 *  Except as provided for by license agreement, this program shall not be
 *  duplicated, used, or disclosed without written consent signed by an officer
 *  of NetGen Software Inc.
 *
 ***************************************************************************/
package com.sun.mdm.index.webservice;

import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;
import com.sun.mdm.index.page.PageException;

/**
 * SearchPatientResultPage
 * @author cye
 */
@XmlRootElement
public class SearchPatientResultPage {

    List<SearchPatientResult> pageData;

    private int pageSize;

    private int maxElements;

    private int currentPosition;

    public void setPageData(List<SearchPatientResult> pageData) {
        this.pageData =  pageData;
        currentPosition = 0;
        maxElements = pageData.size();
    }

    public List<SearchPatientResult> getPageData() {
        return this.pageData;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageSize() {
        return this.pageSize;
    }

     public SearchPatientResult first()
        throws PageException {
        currentPosition = 0;
        return next();
    }

    public SearchPatientResult next()
        throws PageException {
        if (hasNext()) {
            SearchPatientResult result = pageData.get(currentPosition);
            if (currentPosition < maxElements) {
                currentPosition++;
            }
            return result;
        } else {
            throw new PageException("no record");
        }
    }

    public boolean hasNext() {
        if (pageData != null &&
            maxElements > 0 &&
            currentPosition >= 0 &&
            currentPosition < maxElements) {
            return true;
        } else {
            return false;
        }
    }

    public SearchPatientResult prev()
        throws PageException {
        if (maxElements > 0) {
            if (currentPosition > 0){
               currentPosition--;
            }
            SearchPatientResult result = pageData.get(currentPosition);
            return result;
        } else {
            throw new PageException("no record");
        }
    }
 
    public int getMaxElements() {
        return maxElements;
    }

    public void setMaxElements(int maxElements) {
        this.maxElements = maxElements;
    }

}
