/* *************************************************************************
 *
 *  Copyright (c) 2009, NetGen Software Inc., All Rights Reserved
 *
 *  This program, and all the routines referenced herein, are the proprietary
 *  properties and trade secrets of NetGen Software Inc.
 *
 *  Except as provided for by license agreement, this program shall not be
 *  duplicated, used, or disclosed without written consent signed by an officer
 *  of NetGen Software Inc.
 *
 ***************************************************************************/
package com.sun.mdm.index.webservice;

import javax.xml.bind.annotation.XmlRootElement;
import com.netgen.mdm.match.SystemMatchResult;

/**
 * PatientMatchResult class.
 * @author cye
 */
@XmlRootElement
public class PatientMatchResult {

    private SystemMatchResult systemMatchResult;

    public PatientMatchResult() {
    }

    public PatientMatchResult(SystemMatchResult systemMatchResult) {
        this.systemMatchResult = systemMatchResult;
    }

    public void setSystemMatchResult(SystemMatchResult systemMatchResult) {
        this.systemMatchResult = systemMatchResult;
    }

    public SystemMatchResult getSystemMatchResult(){
        return  this.systemMatchResult;
    }
}
