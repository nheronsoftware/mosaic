/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */

package com.sun.mdm.index.webservice;

import java.util.*;
import com.sun.mdm.index.objects.exception.*;
import com.sun.mdm.index.objects.*;
import com.sun.mdm.index.objects.metadata.MetaDataService;
import java.text.SimpleDateFormat;
import java.text.ParsePosition;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public final class AliasBean implements ObjectBean
{
    public static final int version = 1;

    private AliasObject mAliasObject;
    private ClearFieldObject mClearFieldObj;
    private static java.text.SimpleDateFormat mDateFormat = new SimpleDateFormat(MetaDataService.getDateFormat());

    /**
     * Creates a new AliasBean instance.
     * @throws  ObjectException If creation fails. 
     */ 
    public AliasBean() throws ObjectException
    { 
       mAliasObject = new AliasObject();
    }
    
    /**
     * Creates a new AliasBean instance from a ClearFieldObject.
     */ 
    public AliasBean(ClearFieldObject clearFieldObj) throws ObjectException
    { 
       mAliasObject = new AliasObject();
       mClearFieldObj = clearFieldObj;
    }

    /**
     * Creates a new AliasBean instance from a AliasObject.
     */
    public AliasBean(AliasObject aAliasObject) throws ObjectException
    { 
       mAliasObject = aAliasObject;
    }
    
    /**
     * Creates a new AliasBean instance from 
     * a AliasObject and a ClearFieldObject.
     */
    public AliasBean(AliasObject aAliasObject,
      ClearFieldObject clearFieldObj) throws ObjectException
    { 
       mAliasObject = aAliasObject;
       mClearFieldObj = clearFieldObj;
    }
    
    /**
     * Getter for AliasId
     * @return a string value of AliasId
     */    
    public String getAliasId() throws ObjectException
    {
        try
        {
            int type = mAliasObject.pGetType("AliasId");
            Object value = mAliasObject.getValue("AliasId");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for FirstName
     * @return a string value of FirstName
     */    
    public String getFirstName() throws ObjectException
    {
        try
        {
            int type = mAliasObject.pGetType("FirstName");
            Object value = mAliasObject.getValue("FirstName");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for FirstName_Std
     * @return a string value of FirstName_Std
     */    
    public String getFirstName_Std() throws ObjectException
    {
        try
        {
            int type = mAliasObject.pGetType("FirstName_Std");
            Object value = mAliasObject.getValue("FirstName_Std");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for FirstName_Phon
     * @return a string value of FirstName_Phon
     */    
    public String getFirstName_Phon() throws ObjectException
    {
        try
        {
            int type = mAliasObject.pGetType("FirstName_Phon");
            Object value = mAliasObject.getValue("FirstName_Phon");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for MiddleName
     * @return a string value of MiddleName
     */    
    public String getMiddleName() throws ObjectException
    {
        try
        {
            int type = mAliasObject.pGetType("MiddleName");
            Object value = mAliasObject.getValue("MiddleName");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for LastName
     * @return a string value of LastName
     */    
    public String getLastName() throws ObjectException
    {
        try
        {
            int type = mAliasObject.pGetType("LastName");
            Object value = mAliasObject.getValue("LastName");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for LastName_Std
     * @return a string value of LastName_Std
     */    
    public String getLastName_Std() throws ObjectException
    {
        try
        {
            int type = mAliasObject.pGetType("LastName_Std");
            Object value = mAliasObject.getValue("LastName_Std");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for LastName_Phon
     * @return a string value of LastName_Phon
     */    
    public String getLastName_Phon() throws ObjectException
    {
        try
        {
            int type = mAliasObject.pGetType("LastName_Phon");
            Object value = mAliasObject.getValue("LastName_Phon");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for Suffix
     * @return a string value of Suffix
     */    
    public String getSuffix() throws ObjectException
    {
        try
        {
            int type = mAliasObject.pGetType("Suffix");
            Object value = mAliasObject.getValue("Suffix");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for Prefix
     * @return a string value of Prefix
     */    
    public String getPrefix() throws ObjectException
    {
        try
        {
            int type = mAliasObject.pGetType("Prefix");
            Object value = mAliasObject.getValue("Prefix");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for Degree
     * @return a string value of Degree
     */    
    public String getDegree() throws ObjectException
    {
        try
        {
            int type = mAliasObject.pGetType("Degree");
            Object value = mAliasObject.getValue("Degree");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for Type
     * @return a string value of Type
     */    
    public String getType() throws ObjectException
    {
        try
        {
            int type = mAliasObject.pGetType("Type");
            Object value = mAliasObject.getValue("Type");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for Representation
     * @return a string value of Representation
     */    
    public String getRepresentation() throws ObjectException
    {
        try
        {
            int type = mAliasObject.pGetType("Representation");
            Object value = mAliasObject.getValue("Representation");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Setter for AliasId
     * @param string value of AliasId
     */ 
    public void setAliasId(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mAliasObject.isNullable("AliasId")) {
               mAliasObject.clearField("AliasId");
            } else {
               int type = mAliasObject.pGetType("AliasId");
               Object val = strToObj(value, type, "AliasId");
          
               mAliasObject.setValue("AliasId", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for FirstName
     * @param string value of FirstName
     */ 
    public void setFirstName(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mAliasObject.isNullable("FirstName")) {
               mAliasObject.clearField("FirstName");
            } else {
               int type = mAliasObject.pGetType("FirstName");
               Object val = strToObj(value, type, "FirstName");
          
               mAliasObject.setValue("FirstName", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for FirstName_Std
     * @param string value of FirstName_Std
     */ 
    public void setFirstName_Std(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mAliasObject.isNullable("FirstName_Std")) {
               mAliasObject.clearField("FirstName_Std");
            } else {
               int type = mAliasObject.pGetType("FirstName_Std");
               Object val = strToObj(value, type, "FirstName_Std");
          
               mAliasObject.setValue("FirstName_Std", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for FirstName_Phon
     * @param string value of FirstName_Phon
     */ 
    public void setFirstName_Phon(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mAliasObject.isNullable("FirstName_Phon")) {
               mAliasObject.clearField("FirstName_Phon");
            } else {
               int type = mAliasObject.pGetType("FirstName_Phon");
               Object val = strToObj(value, type, "FirstName_Phon");
          
               mAliasObject.setValue("FirstName_Phon", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for MiddleName
     * @param string value of MiddleName
     */ 
    public void setMiddleName(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mAliasObject.isNullable("MiddleName")) {
               mAliasObject.clearField("MiddleName");
            } else {
               int type = mAliasObject.pGetType("MiddleName");
               Object val = strToObj(value, type, "MiddleName");
          
               mAliasObject.setValue("MiddleName", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for LastName
     * @param string value of LastName
     */ 
    public void setLastName(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mAliasObject.isNullable("LastName")) {
               mAliasObject.clearField("LastName");
            } else {
               int type = mAliasObject.pGetType("LastName");
               Object val = strToObj(value, type, "LastName");
          
               mAliasObject.setValue("LastName", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for LastName_Std
     * @param string value of LastName_Std
     */ 
    public void setLastName_Std(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mAliasObject.isNullable("LastName_Std")) {
               mAliasObject.clearField("LastName_Std");
            } else {
               int type = mAliasObject.pGetType("LastName_Std");
               Object val = strToObj(value, type, "LastName_Std");
          
               mAliasObject.setValue("LastName_Std", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for LastName_Phon
     * @param string value of LastName_Phon
     */ 
    public void setLastName_Phon(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mAliasObject.isNullable("LastName_Phon")) {
               mAliasObject.clearField("LastName_Phon");
            } else {
               int type = mAliasObject.pGetType("LastName_Phon");
               Object val = strToObj(value, type, "LastName_Phon");
          
               mAliasObject.setValue("LastName_Phon", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for Suffix
     * @param string value of Suffix
     */ 
    public void setSuffix(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mAliasObject.isNullable("Suffix")) {
               mAliasObject.clearField("Suffix");
            } else {
               int type = mAliasObject.pGetType("Suffix");
               Object val = strToObj(value, type, "Suffix");
          
               mAliasObject.setValue("Suffix", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for Prefix
     * @param string value of Prefix
     */ 
    public void setPrefix(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mAliasObject.isNullable("Prefix")) {
               mAliasObject.clearField("Prefix");
            } else {
               int type = mAliasObject.pGetType("Prefix");
               Object val = strToObj(value, type, "Prefix");
          
               mAliasObject.setValue("Prefix", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for Degree
     * @param string value of Degree
     */ 
    public void setDegree(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mAliasObject.isNullable("Degree")) {
               mAliasObject.clearField("Degree");
            } else {
               int type = mAliasObject.pGetType("Degree");
               Object val = strToObj(value, type, "Degree");
          
               mAliasObject.setValue("Degree", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for Type
     * @param string value of Type
     */ 
    public void setType(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mAliasObject.isNullable("Type")) {
               mAliasObject.clearField("Type");
            } else {
               int type = mAliasObject.pGetType("Type");
               Object val = strToObj(value, type, "Type");
          
               mAliasObject.setValue("Type", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for Representation
     * @param string value of Representation
     */ 
    public void setRepresentation(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mAliasObject.isNullable("Representation")) {
               mAliasObject.clearField("Representation");
            } else {
               int type = mAliasObject.pGetType("Representation");
               Object val = strToObj(value, type, "Representation");
          
               mAliasObject.setValue("Representation", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    
    public ObjectNode getObjectNode() {
        return mAliasObject;
    }

    /** 
     * Return AliasObject
     * @return AliasObject
     */ 
    public ObjectNode pGetObject() {
        return mAliasObject;
    }

    /** 
     * Getter for all children nodes
     * @return null because there is no child at the leaf
     */            
    public Collection pGetChildren() {            
         return null;
    }
    
    /** 
     * Getter for children of a specified type
     * @param type Type of children to retrieve
     * @return null because there is no child at the leaf
     */
    public Collection pGetChildren(String type) {
        return null;
    }

    /** 
     * Getter for child types
     * @return null because there is no child at the leaf
     */
    public ArrayList pGetChildTypes() {
        return null;
    }    

    /**
     * Count of all children
     * @return number of children
     */
    public int countChildren() {
        int count = 0;
        return count;
    }

    /**
     * Count of children of specified type
     * @param type of children to count
     * @return number of children of specified type
     */
    public int countChildren(String type) {
        int count = 0;
        return count;
    }
    
    /**
     * Delete itself from the parent and persist
     */
    public void delete() throws ObjectException {
        ObjectNode parent = mAliasObject.getParent();
        parent.deleteChild("Alias", mAliasObject.pGetSuperKey()); 
    }
        
    // Find parent which is SystemObject    
    private SystemObject getParentSO() {
        ObjectNode obj = mAliasObject.getParent();
        
        while (obj != null) {
           if (obj instanceof SystemObject) {
              return (SystemObject) obj;
           } else {
              obj = obj.getParent();
           }
        }
        return (SystemObject) obj;
    }    
            
    static String objToString(Object value, int type) throws ObjectException {
        if (value == null) {
            return null;
        } else {
            if ( type == ObjectField.OBJECTMETA_STRING_TYPE) {
                return (String) value;
            }
            else if (type == ObjectField.OBJECTMETA_DATE_TYPE) {               
               return mDateFormat.format(value);              
            } else {
                return value.toString();
            }
        }
    }
    
    static Object strToObj(String str, int type, String fieldName) throws ObjectException {
        if (str == null || str.trim().length() == 0) {
            return null;
        } else if ( type == ObjectField.OBJECTMETA_STRING_TYPE) {
            return  str;
        } else if (type == ObjectField.OBJECTMETA_DATE_TYPE) {
            ParsePosition pos = new ParsePosition(0);
             Object ret = mDateFormat.parse(str, pos);   
            if ( ret == null) {
               throw new ObjectException("Invalid Date format of" + fieldName + ",value:" + str);
            }           
            return ret;             
        } else if (type == ObjectField.OBJECTMETA_INT_TYPE) {                
            return Integer.valueOf(str);              
        } else if (type == ObjectField.OBJECTMETA_FLOAT_TYPE) {                
            return Float.valueOf(str);              
        } else if (type == ObjectField.OBJECTMETA_LONG_TYPE) {                
            return Long.valueOf(str);             
        } else if (type == ObjectField.OBJECTMETA_BOOL_TYPE) {                
            return Boolean.valueOf(str);              
        } else if (type == ObjectField.OBJECTMETA_CHAR_TYPE) {                
            return (new Character(str.charAt(0)));                          
        } else {
            throw new ObjectException("Invalid type of" + fieldName + ",value:" + str);
        }
    }
}
