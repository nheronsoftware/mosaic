/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */

package com.sun.mdm.index.webservice;

import java.util.*;
import com.sun.mdm.index.objects.exception.*;
import com.sun.mdm.index.objects.*;
import com.sun.mdm.index.objects.metadata.MetaDataService;
import java.text.SimpleDateFormat;
import java.text.ParsePosition;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public final class LastUpdateBean implements ObjectBean
{
    public static final int version = 1;

    private LastUpdateObject mLastUpdateObject;
    private ClearFieldObject mClearFieldObj;
    private static java.text.SimpleDateFormat mDateFormat = new SimpleDateFormat(MetaDataService.getDateFormat());

    /**
     * Creates a new LastUpdateBean instance.
     * @throws  ObjectException If creation fails. 
     */ 
    public LastUpdateBean() throws ObjectException
    { 
       mLastUpdateObject = new LastUpdateObject();
    }
    
    /**
     * Creates a new LastUpdateBean instance from a ClearFieldObject.
     */ 
    public LastUpdateBean(ClearFieldObject clearFieldObj) throws ObjectException
    { 
       mLastUpdateObject = new LastUpdateObject();
       mClearFieldObj = clearFieldObj;
    }

    /**
     * Creates a new LastUpdateBean instance from a LastUpdateObject.
     */
    public LastUpdateBean(LastUpdateObject aLastUpdateObject) throws ObjectException
    { 
       mLastUpdateObject = aLastUpdateObject;
    }
    
    /**
     * Creates a new LastUpdateBean instance from 
     * a LastUpdateObject and a ClearFieldObject.
     */
    public LastUpdateBean(LastUpdateObject aLastUpdateObject,
      ClearFieldObject clearFieldObj) throws ObjectException
    { 
       mLastUpdateObject = aLastUpdateObject;
       mClearFieldObj = clearFieldObj;
    }
    
    /**
     * Getter for LastUpdateId
     * @return a string value of LastUpdateId
     */    
    public String getLastUpdateId() throws ObjectException
    {
        try
        {
            int type = mLastUpdateObject.pGetType("LastUpdateId");
            Object value = mLastUpdateObject.getValue("LastUpdateId");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for DateTime
     * @return a string value of DateTime
     */    
    public String getDateTime() throws ObjectException
    {
        try
        {
            int type = mLastUpdateObject.pGetType("DateTime");
            Object value = mLastUpdateObject.getValue("DateTime");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for DateTimePrecision
     * @return a string value of DateTimePrecision
     */    
    public String getDateTimePrecision() throws ObjectException
    {
        try
        {
            int type = mLastUpdateObject.pGetType("DateTimePrecision");
            Object value = mLastUpdateObject.getValue("DateTimePrecision");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for NamespaceId
     * @return a string value of NamespaceId
     */    
    public String getNamespaceId() throws ObjectException
    {
        try
        {
            int type = mLastUpdateObject.pGetType("NamespaceId");
            Object value = mLastUpdateObject.getValue("NamespaceId");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for UniversalId
     * @return a string value of UniversalId
     */    
    public String getUniversalId() throws ObjectException
    {
        try
        {
            int type = mLastUpdateObject.pGetType("UniversalId");
            Object value = mLastUpdateObject.getValue("UniversalId");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Getter for UniversalIdType
     * @return a string value of UniversalIdType
     */    
    public String getUniversalIdType() throws ObjectException
    {
        try
        {
            int type = mLastUpdateObject.pGetType("UniversalIdType");
            Object value = mLastUpdateObject.getValue("UniversalIdType");
            return objToString(value, type);        
        }
        catch (ObjectException e)
        {
            throw e;
        }
    }

    /**
     * Setter for LastUpdateId
     * @param string value of LastUpdateId
     */ 
    public void setLastUpdateId(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mLastUpdateObject.isNullable("LastUpdateId")) {
               mLastUpdateObject.clearField("LastUpdateId");
            } else {
               int type = mLastUpdateObject.pGetType("LastUpdateId");
               Object val = strToObj(value, type, "LastUpdateId");
          
               mLastUpdateObject.setValue("LastUpdateId", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for DateTime
     * @param string value of DateTime
     */ 
    public void setDateTime(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mLastUpdateObject.isNullable("DateTime")) {
               mLastUpdateObject.clearField("DateTime");
            } else {
               int type = mLastUpdateObject.pGetType("DateTime");
               Object val = strToObj(value, type, "DateTime");
          
               mLastUpdateObject.setValue("DateTime", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for DateTimePrecision
     * @param string value of DateTimePrecision
     */ 
    public void setDateTimePrecision(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mLastUpdateObject.isNullable("DateTimePrecision")) {
               mLastUpdateObject.clearField("DateTimePrecision");
            } else {
               int type = mLastUpdateObject.pGetType("DateTimePrecision");
               Object val = strToObj(value, type, "DateTimePrecision");
          
               mLastUpdateObject.setValue("DateTimePrecision", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for NamespaceId
     * @param string value of NamespaceId
     */ 
    public void setNamespaceId(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mLastUpdateObject.isNullable("NamespaceId")) {
               mLastUpdateObject.clearField("NamespaceId");
            } else {
               int type = mLastUpdateObject.pGetType("NamespaceId");
               Object val = strToObj(value, type, "NamespaceId");
          
               mLastUpdateObject.setValue("NamespaceId", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for UniversalId
     * @param string value of UniversalId
     */ 
    public void setUniversalId(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mLastUpdateObject.isNullable("UniversalId")) {
               mLastUpdateObject.clearField("UniversalId");
            } else {
               int type = mLastUpdateObject.pGetType("UniversalId");
               Object val = strToObj(value, type, "UniversalId");
          
               mLastUpdateObject.setValue("UniversalId", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    /**
     * Setter for UniversalIdType
     * @param string value of UniversalIdType
     */ 
    public void setUniversalIdType(String value) throws ObjectException
    {
        try
        {
            Object clearFieldIndicator = (mClearFieldObj != null) ? mClearFieldObj.getClearFieldIndicator() : null;
            if (value != null && clearFieldIndicator != null &&
                value.equals(clearFieldIndicator) &&
                mLastUpdateObject.isNullable("UniversalIdType")) {
               mLastUpdateObject.clearField("UniversalIdType");
            } else {
               int type = mLastUpdateObject.pGetType("UniversalIdType");
               Object val = strToObj(value, type, "UniversalIdType");
          
               mLastUpdateObject.setValue("UniversalIdType", val);
            }
        
        }
        catch(ObjectException e)
        {
            throw e;
        }
    }
    
    
    public ObjectNode getObjectNode() {
        return mLastUpdateObject;
    }

    /** 
     * Return LastUpdateObject
     * @return LastUpdateObject
     */ 
    public ObjectNode pGetObject() {
        return mLastUpdateObject;
    }

    /** 
     * Getter for all children nodes
     * @return null because there is no child at the leaf
     */            
    public Collection pGetChildren() {            
         return null;
    }
    
    /** 
     * Getter for children of a specified type
     * @param type Type of children to retrieve
     * @return null because there is no child at the leaf
     */
    public Collection pGetChildren(String type) {
        return null;
    }

    /** 
     * Getter for child types
     * @return null because there is no child at the leaf
     */
    public ArrayList pGetChildTypes() {
        return null;
    }    

    /**
     * Count of all children
     * @return number of children
     */
    public int countChildren() {
        int count = 0;
        return count;
    }

    /**
     * Count of children of specified type
     * @param type of children to count
     * @return number of children of specified type
     */
    public int countChildren(String type) {
        int count = 0;
        return count;
    }
    
    /**
     * Delete itself from the parent and persist
     */
    public void delete() throws ObjectException {
        ObjectNode parent = mLastUpdateObject.getParent();
        parent.deleteChild("LastUpdate", mLastUpdateObject.pGetSuperKey()); 
    }
        
    // Find parent which is SystemObject    
    private SystemObject getParentSO() {
        ObjectNode obj = mLastUpdateObject.getParent();
        
        while (obj != null) {
           if (obj instanceof SystemObject) {
              return (SystemObject) obj;
           } else {
              obj = obj.getParent();
           }
        }
        return (SystemObject) obj;
    }    
            
    static String objToString(Object value, int type) throws ObjectException {
        if (value == null) {
            return null;
        } else {
            if ( type == ObjectField.OBJECTMETA_STRING_TYPE) {
                return (String) value;
            }
            else if (type == ObjectField.OBJECTMETA_DATE_TYPE) {               
               return mDateFormat.format(value);              
            } else {
                return value.toString();
            }
        }
    }
    
    static Object strToObj(String str, int type, String fieldName) throws ObjectException {
        if (str == null || str.trim().length() == 0) {
            return null;
        } else if ( type == ObjectField.OBJECTMETA_STRING_TYPE) {
            return  str;
        } else if (type == ObjectField.OBJECTMETA_DATE_TYPE) {
            ParsePosition pos = new ParsePosition(0);
             Object ret = mDateFormat.parse(str, pos);   
            if ( ret == null) {
               throw new ObjectException("Invalid Date format of" + fieldName + ",value:" + str);
            }           
            return ret;             
        } else if (type == ObjectField.OBJECTMETA_INT_TYPE) {                
            return Integer.valueOf(str);              
        } else if (type == ObjectField.OBJECTMETA_FLOAT_TYPE) {                
            return Float.valueOf(str);              
        } else if (type == ObjectField.OBJECTMETA_LONG_TYPE) {                
            return Long.valueOf(str);             
        } else if (type == ObjectField.OBJECTMETA_BOOL_TYPE) {                
            return Boolean.valueOf(str);              
        } else if (type == ObjectField.OBJECTMETA_CHAR_TYPE) {                
            return (new Character(str.charAt(0)));                          
        } else {
            throw new ObjectException("Invalid type of" + fieldName + ",value:" + str);
        }
    }
}
