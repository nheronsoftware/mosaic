/* *************************************************************************
 *
 *  Copyright (c) 2011, NetGen Software Inc., All Rights Reserved
 *
 *  This program, and all the routines referenced herein, are the proprietary
 *  properties and trade secrets of NetGen Software Inc.
 *
 *  Except as provided for by license agreement, this program shall not be
 *  duplicated, used, or disclosed without written consent signed by an officer
 *  of NetGen Software Inc.
 *
 ***************************************************************************/
package org.openhealthexchange.pixpdqadapter.iopenmpi;

import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Properties;

import javax.xml.ws.BindingProvider;
import org.openhealthexchange.pixpdqadapter.iopenmpi.data.MatchResultBean;
import org.openhealthexchange.pixpdqadapter.iopenmpi.data.MergeResultBean;
import com.netgen.ihe.patient.PatientBean;
import com.netgen.ihe.patient.PotentialBean;
import com.netgen.ihe.patient.IdentifierBean;
import com.netgen.ihe.patient.SystemPatient;
import com.netgen.ihe.patient.SystemPatientPK;
import com.netgen.ihe.patient.PatientEJBService;
import com.netgen.ihe.patient.PatientEJB;
import com.netgen.ihe.patient.MatchPatientResult;
import com.netgen.ihe.patient.MergePatientResult;
import com.netgen.ihe.patient.SearchPatientResult;
import com.netgen.ihe.patient.ProcessingException_Exception;
import com.netgen.ihe.patient.UserException_Exception;

/**
 * This class is to interact with the iOpenMpi patient mpi application.
 * 
 * @author charles.ye@netgensoftware.com
 * @version 1.0  
 */
public class iOpenMpiService {
	private static final String PATIEN_ENDPOINT_URL = "iopenmpi.patient.endpoint.url";
	private PatientEJB mpiService;
	private float matchThreshold;
	
	public iOpenMpiService() {
		Properties properties = new Properties();
		try {
			properties.load(getClass().getResourceAsStream("/../iopenmpi.properties"));
			String url = (String)properties.get(PATIEN_ENDPOINT_URL);
			initialize(url);
		} catch (IOException ioex) {
		}
	}
	
	public void initialize(String url) {
		PatientEJBService service = new PatientEJBService();
		mpiService = service.getPatientEJBPort();	
		Map<String, Object> context = ((BindingProvider)mpiService).getRequestContext();
		context.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, url);  
		matchThreshold = mpiService.getMatchThreshold();
	}
	
	public List<IdentifierBean> findPatientIdentifiers(IdentifierBean patient)
			throws MpiServiceException {
		try {
			List<IdentifierBean> patientIdentifiers = new ArrayList<IdentifierBean>();
			String euid = mpiService.getEUID(patient.getNamespaceId(), patient.getId());
			List<SystemPatientPK> systemPKs = mpiService.getLIDs(euid);
			if (systemPKs != null) {
				for (SystemPatientPK systemPK : systemPKs) {
					IdentifierBean idBean = new IdentifierBean();
					idBean.setId(systemPK.getLocalId());
					idBean.setNamespaceId(systemPK.getSystemCode());
					patientIdentifiers.add(idBean);
				}
			}
			return patientIdentifiers;
		} catch (UserException_Exception uex) {
			throw new MpiServiceException(uex);
		} catch (ProcessingException_Exception pex) {
			throw new MpiServiceException(pex);
		}
	}
	
	public String isValidPatient(IdentifierBean patient)
			throws MpiServiceException {
		try {
			String euid = mpiService.getEUID(patient.getNamespaceId(), patient.getId());
			return euid; 
		} catch (UserException_Exception uex) {
			throw new MpiServiceException(uex);
		} catch (ProcessingException_Exception pex) {
			throw new MpiServiceException(pex);
		}
	}
	
	public MatchResultBean createPatient(PatientBean patient) 
			throws MpiServiceException {
		try {
			SystemPatient patientSystem = new SystemPatient();
			
			patientSystem.setPatient(patient);
			List<IdentifierBean> idBeans = patient.getIdentifier();
			if (idBeans.size() == 0) {
				throw new MpiServiceException("invalid patient identifier.");
			}	
			String localId = idBeans.get(0).getId();
			String systemId = idBeans.get(0).getNamespaceId();
			patientSystem.setSystemCode(systemId);
			patientSystem.setLocalId(localId);
			patientSystem.setStatus("active");

			MatchPatientResult matchResult = mpiService.executeMatchUpdate(patientSystem);
			List<PotentialBean> potentialBeans = matchResult.getPotentials();
			
			if (potentialBeans != null && !potentialBeans.isEmpty()) {
				for (PotentialBean potentialBean : potentialBeans) {
					if (potentialBean.getScore() >= matchThreshold) { 
						mpiService.mergeEnterpriseRecord(potentialBean.getEuid1(), potentialBean.getEuid2(), false);
					}
				}
			}
			
			MatchResultBean matchResultBean = new MatchResultBean(matchResult.getEUID(), 
											  Integer.toString(matchResult.getResultCode()));
			matchResultBean.setEuid(matchResult.getEUID());
			
			List<SystemPatientPK> systemPKs = mpiService.getLIDs(matchResult.getEUID());
			if (systemPKs != null) {
				for (SystemPatientPK systemPK : systemPKs) {
					IdentifierBean idBean = new IdentifierBean();
					idBean.setId(systemPK.getLocalId());
					idBean.setNamespaceId(systemPK.getSystemCode());
					matchResultBean.setIdentifierBean(idBean);
				}
			}
			return matchResultBean;
		} catch (UserException_Exception uex) {
			throw new MpiServiceException(uex);
		} catch (ProcessingException_Exception pex) {
			throw new MpiServiceException(pex);
		}
	}
 	
	public MatchResultBean updatePatient(PatientBean patient) 
		throws MpiServiceException {
		return createPatient(patient);
	}	

	public MergeResultBean mergePatient(IdentifierBean survivingPatient,
			IdentifierBean mergedPatient) throws MpiServiceException {
		try {
			MergeResultBean mergeBean = new MergeResultBean();
			String survivingEuid = isValidPatient(survivingPatient);
			if (survivingEuid == null) {
				throw new MpiServiceException(
						"invalid surviving patient identifier:"
								+ survivingPatient.getNamespaceId() + ","
								+ survivingPatient.getId());
			}
			String mergedEuid = isValidPatient(mergedPatient);
			if (mergedEuid == null) {
				throw new MpiServiceException(
						"invalid merged patient identifier:"
								+ mergedPatient.getNamespaceId() + ","
								+ mergedPatient.getId());
			}
			MergePatientResult mergedResult = null;
			if (!survivingEuid.equals(mergedEuid)) {
				mergedResult = mpiService.mergeEnterpriseRecord(mergedEuid,
						survivingEuid, false);
			} else {
				mergedResult = mpiService.mergeSystemRecord(
						survivingPatient.getNamespaceId(),
						mergedPatient.getId(), survivingPatient.getId(), false);
			}
			mergeBean.setEuid(mergedResult.getDestinationEO().getEnterprisePatient().getEUID());
			List<SystemPatient> patients = mergedResult.getDestinationEO().getEnterprisePatient().getSystemPatient();
			for (SystemPatient patient : patients) {
				IdentifierBean idBean = new IdentifierBean();
				idBean.setId(patient.getLocalId());
				idBean.setNamespaceId(patient.getSystemCode());
				mergeBean.setIdentifierBean(idBean);
			}
			return mergeBean;
		} catch (UserException_Exception uex) {
			throw new MpiServiceException(uex);
		} catch (ProcessingException_Exception pex) {
			throw new MpiServiceException(pex);
		}
	}

	public List<PatientBean> findCandidates(PatientBean patient)
			throws MpiServiceException {
		try {
			List<PatientBean> patients = new ArrayList<PatientBean>();
			boolean alpha = false;
			String firstName = patient.getFirstName(); 
			if (firstName != null &&
				firstName.contains("*")) {
				alpha = true;
				patient.setFirstName(firstName.replace('*', '%'));
			}
			String lastName = patient.getLastName(); 
			if (lastName != null &&
				lastName.contains("*")) {
				alpha = true;
				patient.setLastName(lastName.replace('*', '%'));
			}
			List<IdentifierBean> idBeans = patient.getIdentifier();
			if (idBeans != null & !idBeans.isEmpty()) {
				IdentifierBean idBean = idBeans.get(0);
				String id = idBean.getId();
				if (id != null && id.contains("*")) {
					id = id.replace('*', '%');
					idBean.setId(id);
					alpha = true;
					idBeans.set(0, idBean);
				}
			}
			
			List<SearchPatientResult> searchResults = null;
			if (alpha) {
				searchResults = mpiService.searchExact(patient);
			} else {
				searchResults = mpiService.searchBlock(patient);
			}
			if (searchResults != null) {
				for (SearchPatientResult searchResult : searchResults) {
					patients.add(searchResult.getPatient());
				}
			}
			return patients;
		} catch (UserException_Exception uex) {
			throw new MpiServiceException(uex);
		} catch (ProcessingException_Exception pex) {
			throw new MpiServiceException(pex);
		}
	}
}
