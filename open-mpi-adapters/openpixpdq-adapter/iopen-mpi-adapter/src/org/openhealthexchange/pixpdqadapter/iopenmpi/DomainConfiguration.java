/* *************************************************************************
 *
 *  Copyright (c) 2011, NetGen Software Inc., All Rights Reserved
 *
 *  This program, and all the routines referenced herein, are the proprietary
 *  properties and trade secrets of NetGen Software Inc.
 *
 *  Except as provided for by license agreement, this program shall not be
 *  duplicated, used, or disclosed without written consent signed by an officer
 *  of NetGen Software Inc.
 *
 ***************************************************************************/
package org.openhealthexchange.pixpdqadapter.iopenmpi;

import java.util.List;
import java.util.ArrayList;
import java.io.File;
import java.io.InputStream;
import java.io.IOException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import org.openhealthexchange.pixpdqadapter.iopenmpi.data.Domain;

/**
 * This class implements to load the Domain configuration.
 * 
 * @author charles.ye@netgensoftware.com
 * @version 1.0
 */
public class DomainConfiguration {

	private List<Domain> domains = new ArrayList<Domain>();
	
	/* Singleton instance */
	private static DomainConfiguration instance = new DomainConfiguration();
	
	public static synchronized DomainConfiguration getInstance() {
		return instance;
	}
	
	public List<Domain> load(File xmlFile) 
		throws MpiServiceException {
		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xmlFile);
			doc.getDocumentElement().normalize();
			parse(doc);
			return domains;
		} catch (IOException ioex) {
			throw new MpiServiceException(ioex);
		} catch (SAXException sex) {
			throw new MpiServiceException(sex);			
		} catch (ParserConfigurationException pex) {
			throw new MpiServiceException(pex);
		}
	}
	
	public List<Domain> load(InputStream xmlSource) throws MpiServiceException {
		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xmlSource);
			doc.getDocumentElement().normalize();
			parse(doc);
			return domains;
		} catch (IOException ioex) {
			throw new MpiServiceException(ioex);
		} catch (SAXException sex) {
			throw new MpiServiceException(sex);
		} catch (ParserConfigurationException pex) {
			throw new MpiServiceException(pex);
		}
	}

	public void parse(Document doc) 
			throws MpiServiceException {
		NodeList domainNodes = doc.getElementsByTagName("domain");
		for (int i = 0; i < domainNodes.getLength(); i++) {
			Node domainNode = domainNodes.item(i);
			if (domainNode.getNodeType() == Node.ELEMENT_NODE) {
				Domain domain = new Domain();
				Element domainElement = (Element) domainNode;
				NamedNodeMap attributes = domainNode.getAttributes();
				Node name = attributes.getNamedItem("name");
				domain.setName(name.getNodeValue());
				domain.setNamespaceId(getTagValue("NamespaceId", domainElement));
				domain.setUniversalId(getTagValue("UniversalId", domainElement));
				domain.setUniversalIdType(getTagValue("UniversalIdType",
						domainElement));
				domains.add(domain);
			}
		}
	}
	
	private String getTagValue(String tag, Element el) {
		NodeList nlList = el.getElementsByTagName(tag).item(0).getChildNodes();
		Node nValue = (Node) nlList.item(0);
		return nValue.getNodeValue();
	}

	public static void main(String args[]) {
		try {
			DomainConfiguration instance = DomainConfiguration.getInstance();
			List<Domain> domains = instance.load(new File("C:/OpenHealthTools/openpixpdq-src/iOpenMpiAdapter/resources/iopenmpi_domains.xml"));
			for (Domain domain : domains) {
				System.out.println(domain.toString());
			}
		} catch (MpiServiceException mex) {
			mex.printStackTrace();
		}
	}	
}
