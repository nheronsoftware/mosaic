/* *************************************************************************
 *
 *  Copyright (c) 2011, NetGen Software Inc., All Rights Reserved
 *
 *  This program, and all the routines referenced herein, are the proprietary
 *  properties and trade secrets of NetGen Software Inc.
 *
 *  Except as provided for by license agreement, this program shall not be
 *  duplicated, used, or disclosed without written consent signed by an officer
 *  of NetGen Software Inc.
 *
 ***************************************************************************/
package org.openhealthexchange.pixpdqadapter.iopenmpi.data;

import java.util.ArrayList;
import java.util.List;
import com.netgen.ihe.patient.IdentifierBean;

/**
 * This class wraps the merge result of the iOpenMpi patient mpi application.
 * 
 * @author charles.ye@netgensoftware.com
 * @version 1.0
 */
public class MergeResultBean {
	
	private List<IdentifierBean> idBeans;
	
	private String euid;
	
	public MergeResultBean(){	
	}
	
	public MergeResultBean(String euid) {
		this.euid = euid;
	}
		
	
	public List<IdentifierBean> getIdentifierBeans() {
		return this.idBeans;
	}

	public String getEuid() {
		return this.euid;
	}
	
	public void setIdentifierBean(IdentifierBean idBean) {
		if (idBeans == null) {
			idBeans = new ArrayList<IdentifierBean>();
		}
		this.idBeans.add(idBean);
	}
	
	public void setEuid(String euid){
		this.euid = euid;
	}	
}
