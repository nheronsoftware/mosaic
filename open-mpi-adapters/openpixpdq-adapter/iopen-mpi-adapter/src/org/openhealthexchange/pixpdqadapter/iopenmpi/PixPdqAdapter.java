/* *************************************************************************
 *
 *  Copyright (c) 2011, NetGen Software Inc., All Rights Reserved
 *
 *  This program, and all the routines referenced herein, are the proprietary
 *  properties and trade secrets of NetGen Software Inc.
 *
 *  Except as provided for by license agreement, this program shall not be
 *  duplicated, used, or disclosed without written consent signed by an officer
 *  of NetGen Software Inc.
 *
 ***************************************************************************/
package org.openhealthexchange.pixpdqadapter.iopenmpi;

import java.util.List;
import java.util.ArrayList;
import java.io.InputStream;
import org.openhealthexchange.openpixpdq.ihe.IPixPdqAdapter;
import com.misyshealthcare.connect.net.IConnectionDescription;
import com.misyshealthcare.connect.net.Identifier;
import org.openhealthexchange.pixpdqadapter.iopenmpi.data.Domain;

/**
 * This class implements the IPixPdqAdapter interface. 
 * 
 * @author charles.ye@netgensoftware.com
 * @version 1.0
 */
public abstract class PixPdqAdapter implements IPixPdqAdapter {

	protected iOpenMpiService mpiService;
	protected List<Identifier> ids;
	
    /**
     * Gets the domains (or assigning authorities) supported by this PIX Manager. The domains
     * can be configured in either OpenPIXPDQ or eMPI. If configured by OpenPIXPDQ, the 
     * domains can be fetched from the ConnectionDescription object; otherwise, the 
     * domains need to be provided by the underlying eMPI.
     * 
     * @param connection the {@link IConnectionDescription} object from which
     * 		  to get the supported domains if domains are to be fetched from OpenPIXPDQ;
     * 	 	  it is not used if domains are to be fetched from eMPI. 
     * @return A list of domain identifiers.
     */
	public List<Identifier> getDomainIdentifiers(
			IConnectionDescription connection) {
		try {
			ids = new ArrayList<Identifier>();
			DomainConfiguration instance = DomainConfiguration.getInstance();
			InputStream xmlSource = this.getClass().getClassLoader()
					.getResourceAsStream("/../iopenmpi_domains.xml");
			List<Domain> domains = instance.load(xmlSource);
			for (Domain domain : domains) {
				Identifier id = new Identifier(domain.getNamespaceId(),
						domain.getUniversalId(), domain.getUniversalIdType());
				ids.add(id);
			}

		} catch (MpiServiceException mex) {
		}
		return ids;
	}

    public void initialize() {
    	mpiService = new iOpenMpiService();
    }
    
}
