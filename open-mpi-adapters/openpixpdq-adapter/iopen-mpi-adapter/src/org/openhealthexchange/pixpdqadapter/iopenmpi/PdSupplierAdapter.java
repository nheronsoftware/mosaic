/* *************************************************************************
 *
 *  Copyright (c) 2011, NetGen Software Inc., All Rights Reserved
 *
 *  This program, and all the routines referenced herein, are the proprietary
 *  properties and trade secrets of NetGen Software Inc.
 *
 *  Except as provided for by license agreement, this program shall not be
 *  duplicated, used, or disclosed without written consent signed by an officer
 *  of NetGen Software Inc.
 *
 ***************************************************************************/
package org.openhealthexchange.pixpdqadapter.iopenmpi;

import java.util.ArrayList;
import java.util.List;
import com.netgen.ihe.patient.PatientBean;

import org.apache.log4j.Logger;
import org.openhealthexchange.openpixpdq.data.MessageHeader;
import org.openhealthexchange.openpixpdq.data.Patient;
import org.openhealthexchange.openpixpdq.ihe.IPdSupplierAdapter;
import org.openhealthexchange.openpixpdq.ihe.PdSupplierException;
import org.openhealthexchange.openpixpdq.ihe.pdq.PdqQuery;
import org.openhealthexchange.openpixpdq.ihe.pdq.PdqResult;
import org.openhealthexchange.pixpdqadapter.iopenmpi.data.Transformer;

/**
 * This class implements the IPdSupplierAdapter interface. It is to interact 
 * between the PdSupplier of OpenPixPDq and the iOpenMpi patient mpi application. 
 * 
 * @author charles.ye@netgensoftware.com
 * @version 1.0
 */
public class PdSupplierAdapter extends PixPdqAdapter implements IPdSupplierAdapter {
	private static Logger log = Logger.getLogger(PdSupplierAdapter.class);
	
	public PdSupplierAdapter(){
		initialize();
	}

    /**
     * Finds a list of matched patients based on PDQ query parameters.
     *
     * @param query the PdqQuery
     * @param header the MessageHeader
     * @throws PdSupplierException when there is trouble finding the patients
     * @return a PdqResult which contains a list of list 
     *         of Patient</code  The first list is a list 
     *         of different logic patients, while the second list is a list of 
     *         the same patient in different domain systems. PdqResult also 
     *         contains a continuation reference number.
     * @see PdqResult        
     */
    public PdqResult findPatients(PdqQuery query, MessageHeader header) 
    		throws PdSupplierException {
    	try {
    		PatientBean patientBean = Transformer.toBean(query);
			List<PatientBean> results = mpiService.findCandidates(patientBean);
			if (results == null) {
				throw new PdSupplierException("Failed to find patients");
			}
			//Converts to Patients
			List<List<Patient>> allPatients = new ArrayList<List<Patient>>();
			for (PatientBean result : results) {
				List<Patient> patients = new ArrayList<Patient>();
				Patient patient = Transformer.toObject(result);
				patients.add(patient);
				allPatients.add(patients);
			}
			return new PdqResult(allPatients);
		} catch (MpiServiceException mex) {
			throw new PdSupplierException(mex);
		}   	
    }
 
    /**
     * Cancels the existing PDQ Query whose reference id is given by pointer.
     * 
     * @param String the tag of query to be canceled.
     * @param messageQueryName the messageQueryName 
     * @throws PdSupplierException when there is trouble canceling the query.
     */
    public void cancelQuery(String queryTag, String messageQueryName) 
    		throws PdSupplierException {
    	
    }
}
