/* *************************************************************************
 *
 *  Copyright (c) 2011, NetGen Software Inc., All Rights Reserved
 *
 *  This program, and all the routines referenced herein, are the proprietary
 *  properties and trade secrets of NetGen Software Inc.
 *
 *  Except as provided for by license agreement, this program shall not be
 *  duplicated, used, or disclosed without written consent signed by an officer
 *  of NetGen Software Inc.
 *
 ***************************************************************************/
package org.openhealthexchange.pixpdqadapter.iopenmpi;

import java.util.List;
import java.util.ArrayList;
import org.apache.log4j.Logger;
import org.openhealthexchange.openpixpdq.data.MessageHeader;
import org.openhealthexchange.openpixpdq.data.Patient;
import org.openhealthexchange.openpixpdq.data.PatientIdentifier;
import org.openhealthexchange.openpixpdq.ihe.IPixManagerAdapter;
import org.openhealthexchange.openpixpdq.ihe.PixManagerException;
import org.openhealthexchange.pixpdqadapter.iopenmpi.data.Transformer;
import org.openhealthexchange.pixpdqadapter.iopenmpi.data.MatchResultBean;
import org.openhealthexchange.pixpdqadapter.iopenmpi.data.MergeResultBean;
import com.netgen.ihe.patient.PatientBean;
import com.netgen.ihe.patient.IdentifierBean;

/**
 * This class implements the IPixManagerAdapter interface. It is the bridge 
 * between the PixManager of the OpenPixPDq and the iOpenmpi patient mpi application. 
 * 
 * @author charles.ye@netgensoftware.com
 * @version 1.0
 */
public class PixManagerAdapter extends PixPdqAdapter implements IPixManagerAdapter {
	private static Logger log = Logger.getLogger(PixManagerAdapter.class);
	
	public PixManagerAdapter() {
		initialize();
	}
	
    /**
     * Whether the given patient is a valid patient in the eMPI database.
     *
     * @param pid the {@link PatientIdentifier} to be checked
	 * @param header the MessageHeader from the incoming PIX client message
     * @return true if the patient id is valid; false otherwise.
     */
	public boolean isValidPatient(PatientIdentifier pid, MessageHeader header)
			throws PixManagerException {
		boolean isValid = false;
		try {
			IdentifierBean identifierBean = Transformer.toBean(pid);			
			isValid = mpiService.isValidPatient(identifierBean) == null ? false : true; 
		} catch (MpiServiceException mex) {
			throw new PixManagerException(mex);
		}
		return isValid;
	}

    /**
     * Finds from the underneath eMPI all patient ids cross all patient 
     * domains (assigning authorities) given a patient id in a particular domain.
     * All retrieved patient ids must represent the same logic patient, 
     * though they may exist in different patient id domains.  
     *
     * @param header the MessageHeader of the incoming PIX Query message
     * @return A list of PatientIdentifier which does not include the request patient id.
     *         Return an empty list instead of null if no matching is found.
     * @throws PixManagerException when there is trouble cross finding all patients
     */
    public List<PatientIdentifier> findPatientIds(PatientIdentifier pid, MessageHeader header) 
    		throws PixManagerException {
    	List<PatientIdentifier> pids = new ArrayList<PatientIdentifier>();
		try {
			IdentifierBean identifierBean = Transformer.toBean(pid);			
			List<IdentifierBean> identifierBeans = mpiService.findPatientIdentifiers(identifierBean);
			if (identifierBeans != null && 
				identifierBeans.size() != 0) {
				for (IdentifierBean idBean : identifierBeans) {
					if (!idBean.getId().equals(pid.getId()) &&
						!idBean.getNamespaceId().equals(pid.getAssigningAuthority().getNamespaceId()) ) {
						PatientIdentifier id = Transformer.toObject(idBean);
						pids.add(id);
					}
				}				
			}
		} catch (MpiServiceException mex) {
			throw new PixManagerException(mex);
		}
		return pids;
    }

	/**
	 * Creates a new patient in the eMPI database.This method sends the patient 
	 * demographics contained in the Patient to the underneath eMPI.
	 * <p>
	 * @param patient the demographics of the patient to be created
     * @param header the MessageHeader of the incoming PIX Feed message
     * @return a list of new matching PatientIdentifiers of this patient as a result of 
     *         creating this patient. OpenPIXPDQ will send a PIX Update Notification message 
     *         for this list to those PIX Consumers that have subscribed to PIX Update Notification.
     * <p>
     *         If PIX Update Notification is not supported, or if there is no matching 
     *         (i.e, the patient is registered for the first time, no need to send PIX Update 
     *         Notification Message), an empty list or null can be returned.
	 * @throws PixManagerException When there is trouble creating the patient
	 */
	public List<PatientIdentifier> createPatient(Patient patient, MessageHeader header) 
			throws PixManagerException {
		try {
			PatientBean patientBean = Transformer.toBean(patient);
			log.info("creating patient:" + patientBean.getFirstName() + "," + patientBean.getLastName());
			MatchResultBean matchBean = mpiService.createPatient(patientBean);
			List<PatientIdentifier> matching = new ArrayList<PatientIdentifier>();
			//If there is any matching, send PIX Update Notification. 
			//If no matching, it must be the first time registration, then no need to notify.
			if (matchBean.getMatchCode() == MatchResultBean.MATCHED ||
				matchBean.getMatchCode() == MatchResultBean.MATCHED_SYSTEM_ID) {							
				List<IdentifierBean> idBeans = matchBean.getIdentifierBeans();
				for (IdentifierBean idBean : idBeans) {
					PatientIdentifier patientId = Transformer.toObject(idBean);  
					matching.add(patientId);
				}
				return matching;
			} else {
				return matching;
			}
		}  catch (MpiServiceException mex) {
			throw new PixManagerException(mex);
		}
	}

	/**
	 * Updates the patient's demographics in the eMPI's database. This 
	 * method sends the updated patient demographics contained
	 * in the Patient to the underneath eMPI.
	 * <p>
	 * @param patient the new demographics of the patient to be updated
     * @param header the MessageHeader of the incoming PIX Update message
     * @return a list of list of updated matching {@link PatientIdentifier}s
     *         as a result of updating this patient. The outer list is used to 
     *         store different logic patients, while each inner list represents 
     *         the same logic patient with matching patient ids across patient id domains.
     *         For each inner list (matching list), OpenPIXPDQ will send a PIX Update 
     *         Notification message to those PIX Consumers that have subscribed 
     *         to PIX Update Notification. 
     *<p>
     *         For example, if patient(A)'s address is 
     *         updated, and this results in an un-matching of originally matched 
     *         patients (A, B, C & D), two lists are created, one list representing 
     *         updated matching patients (A, E & F); the other one representing updated 
     *         un-matching patients (B, C & D). 
     *<p>
     *         If PIX Update Notification is not supported, or if there is no update 
     *         on the patient matching list, just return an empty list or null.  
     *            
	 * @throws PixManagerException when there is trouble updating the patient
	 */
	public List<List<PatientIdentifier>> updatePatient(Patient patient,
			MessageHeader header) throws PixManagerException {
		try {
			List<List<PatientIdentifier>> updates = new ArrayList<List<PatientIdentifier>>();

			PatientBean patientBean = Transformer.toBean(patient);
			if (patientBean.getIdentifier() == null ||
				patientBean.getIdentifier().isEmpty()) {
				throw new PixManagerException("invalid patient Identifier."); 
			}
			IdentifierBean idBeanUpdated = patientBean.getIdentifier().get(0);
			log.info("updating patient:" + idBeanUpdated.getNamespaceId() + "," + idBeanUpdated.getId());
			MatchResultBean matchBean = mpiService.updatePatient(patientBean);
			List<PatientIdentifier> matchings = new ArrayList<PatientIdentifier>();
			List<PatientIdentifier> unmatchings = new ArrayList<PatientIdentifier>();
			List<IdentifierBean> idBeans = matchBean.getIdentifierBeans();
			if (idBeans != null) {
				for (IdentifierBean idBean : idBeans) {
					PatientIdentifier patientId = Transformer.toObject(idBean);  
					if (idBean.getId().equals(idBeanUpdated.getId()) &&
						idBean.getNamespaceId().equals(idBeanUpdated.getNamespaceId())) {
						matchings.add(patientId);
					} else {
						unmatchings.add(patientId);
					}
				}
			}
			if (idBeans != null && 
			    matchBean.getMatchCode() == MatchResultBean.MATCHED_SYSTEM_ID) {
				matchings.add(Transformer.toObject(idBeanUpdated));
			}
			if (!matchings.isEmpty()) {
				updates.add(matchings);
			}
			if (!unmatchings.isEmpty()) {
				updates.add(unmatchings);
			}
			return updates;
		} catch (MpiServiceException mex) {
			throw new PixManagerException(mex);
		}
	}

	/**
	 * Merges two patients together because they have been found to be
	 * the same patient.  The first argument describes the surviving patient 
	 * demographics; the second argument represents the patient to be merged
	 * with the surviving patient. This method sends the surviving and merged
	 * patients to the underneath eMPI.
	 * 
	 * @param patientMain the surviving patient
	 * @param patientOld the patient to be replaced, and merged with the surviving patient
     * @param header the MessageHeader of the incoming PIX Merge message
     * @return a list of list of updated matching {@link PatientIdentifier}s
     *         as a result of merging patients. The outer list is used to 
     *         store different logic patients, while each inner list represents 
     *         the same logic patient with matching patient ids across patient id domains.
     *         For each inner list (matching list), OpenPIXPDQ will send a PIX Update 
     *         Notification message to those PIX Consumers that have subscribed 
     *         to PIX Update Notification. 
     * <p>
     *         If PIX Update Notification is not supported, or if there is no update 
     *         on the patient matching list, just return an empty list or null.  
     * 
	 * @throws PixManagerException when there is trouble merging the patients
	 */
	public List<List<PatientIdentifier>> mergePatients(Patient patientMain,
			Patient patientOld, MessageHeader header)
			throws PixManagerException {
		try {
			List<List<PatientIdentifier>> merges = new ArrayList<List<PatientIdentifier>>();

			PatientBean survivingBean = Transformer.toBean(patientMain);
			IdentifierBean survivingIdBean = survivingBean.getIdentifier().get(0);
			PatientBean mergedBean = Transformer.toBean(patientOld);
			IdentifierBean mergedIdBean = mergedBean.getIdentifier().get(0);

			List<IdentifierBean> originalmatchings = mpiService.findPatientIdentifiers(survivingIdBean);
			
			MergeResultBean mergeBean = mpiService.mergePatient(mergedIdBean, survivingIdBean);
			List<IdentifierBean> newmatchings = mergeBean.getIdentifierBeans();
			List<PatientIdentifier> unmatchings = new ArrayList<PatientIdentifier>();
			List<PatientIdentifier> matchings = new ArrayList<PatientIdentifier>();
				
			for (IdentifierBean  matching :  newmatchings) {
				PatientIdentifier patientId = Transformer.toObject(matching);  
				if (!originalmatchings.contains(matching)) {
					unmatchings.add(patientId);
				} else {
					matchings.add(patientId);
				}
			}
			merges.add(matchings);
			if (!unmatchings.isEmpty()) {
				merges.add(unmatchings);				
			}
			return merges;
		} catch (MpiServiceException mex) {
			throw new PixManagerException(mex);
		}
	}
	
}
