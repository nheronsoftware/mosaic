/* *************************************************************************
 *
 *  Copyright (c) 2011, NetGen Software Inc., All Rights Reserved
 *
 *  This program, and all the routines referenced herein, are the proprietary
 *  properties and trade secrets of NetGen Software Inc.
 *
 *  Except as provided for by license agreement, this program shall not be
 *  duplicated, used, or disclosed without written consent signed by an officer
 *  of NetGen Software Inc.
 *
 ***************************************************************************/
package org.openhealthexchange.pixpdqadapter.iopenmpi.data;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.ArrayList;
import org.openhealthexchange.openpixpdq.data.Patient;
import org.openhealthexchange.openpixpdq.data.DriversLicense;
import org.openhealthexchange.openpixpdq.data.PersonName;
import org.openhealthexchange.openpixpdq.data.PatientIdentifier;
import org.openhealthexchange.openpixpdq.ihe.pdq.PdqQuery;
import com.misyshealthcare.connect.base.SharedEnums;
import com.misyshealthcare.connect.base.SharedEnums.AddressType;
import com.misyshealthcare.connect.base.SharedEnums.SexType;
import com.misyshealthcare.connect.base.demographicdata.Address;
import com.misyshealthcare.connect.base.demographicdata.PhoneNumber;
import com.misyshealthcare.connect.net.Identifier;
import com.netgen.ihe.patient.PatientBean;
import com.netgen.ihe.patient.AddressBean;
import com.netgen.ihe.patient.PhoneBean;
import com.netgen.ihe.patient.DriverLicenseBean;
import com.netgen.ihe.patient.IdentifierBean;
import com.netgen.ihe.patient.MotherNameBean;

/**
 * This class is to transform the openpixpdq patient data model to the iOpenMpi
 * patient bean model.
 * 
 * @author charles.ye@netgensoftware.com
 * @version 1.0
 */
public class Transformer {

	public static Patient toObject(PatientBean patientBean) {
		Patient patient = new Patient();
		// Address
		List<AddressBean> addressBeans = patientBean.getAddress();
		if (addressBeans != null) {
			for (AddressBean addressBean : addressBeans) {
				Address address = new Address();
				address.setAddLine1(addressBean.getAddressLine1());
				address.setAddLine2(addressBean.getAddressLine2());
				address.setAddCity(addressBean.getCity());
				address.setAddState(addressBean.getState());
				address.setAddCountry(addressBean.getCountry());
				address.setAddZip(addressBean.getZip());
				if (addressBean.getAddressType() != null) {
					address.setAddType(AddressType.hl7ValueOf(addressBean
							.getAddressType()));
				}
				patient.addAddress(address);
			}
		}
		//Gender
		if (patientBean.getGender() != null) {
			patient.setAdministrativeSex(SexType.getByString(patientBean.getGender()));
		}
		//BirthDate
		if (patientBean.getDOB() != null) {
			patient.setBirthDateTime(Helper.format(patientBean.getDOB()));
		}
		//Birth place
		patient.setBirthPlace(patientBean.getBirthPlace());
		//Drivers' License
		List<DriverLicenseBean> dlBeans = patientBean.getDriverLicense();
		if (dlBeans != null && !dlBeans.isEmpty()) {
			DriversLicense dl = new DriversLicense();
			dl.setLicenseNumber(dlBeans.get(0).getId());
			dl.setIssuingState(dlBeans.get(0).getIssuingState());
			if (dlBeans.get(0).getExpirationDate() != null) {
				dl.setExpirationDate(Helper.format(dlBeans.get(0).getExpirationDate()));
			}
			patient.setDriversLicense(dl);
		}
		//Enthnic group
		patient.setEthnicGroup(patientBean.getEthnic());
		//Marital Status
		patient.setMaritalStatus(patientBean.getMaritalStatus());
		//Mothers Maiden Name
		if (patientBean.getMotherName() != null && !patientBean.getMotherName().isEmpty()) {
			MotherNameBean motherNameBean = patientBean.getMotherName().get(0);
			PersonName maidenName = new PersonName();
			maidenName.setFirstName(motherNameBean.getFirstName());
			maidenName.setLastName(motherNameBean.getLastName());
			maidenName.setSecondName(motherNameBean.getMiddleName());
			maidenName.setPrefix(motherNameBean.getPrefix());
			maidenName.setSuffix(motherNameBean.getSuffix());
			maidenName.setDegree(motherNameBean.getDegree());
			patient.setMonthersMaidenName(maidenName);
		}		
	    //Patient ID
		if (patientBean.getIdentifier() != null) {
			List<PatientIdentifier> pids = new ArrayList<PatientIdentifier>();
			for (IdentifierBean idBean : patientBean.getIdentifier()) {
				PatientIdentifier id = new PatientIdentifier();
				id.setId(idBean.getId());
				if (idBean.getEffectiveDate() != null) {
					id.setEffectiveDate(Helper.format(idBean.getEffectiveDate()));
				}
				if (idBean.getExpirationDate() != null) {
					id.setExpirationDate(Helper.format(idBean.getExpirationDate()));
				}
				Identifier ca = new Identifier(idBean.getNamespaceId(),
											   idBean.getUniversalId(),
											   idBean.getUniversalIdType());
				id.setAssigningAuthority(ca);
				id.setAssigningFacility(ca);
				id.setIdentifierTypeCode(idBean.getType());
				pids.add(id);
			}
			patient.setPatientIds(pids);
		}
		//Patient Name
		PersonName patientName = new PersonName();
		patientName.setFirstName(patientBean.getFirstName());
		patientName.setSecondName(patientBean.getMiddleName());
		patientName.setLastName(patientBean.getLastName());
		patientName.setDegree(patientBean.getDegree());
		patientName.setPrefix(patientBean.getPrefix());
		patientName.setSuffix(patientBean.getSuffix());
		patient.setPatientName(patientName);
		//PhoneNumbers
		if (patientBean.getPhone() != null && !patientBean.getPhone().isEmpty()) {
			List<PhoneNumber> phones = new ArrayList<PhoneNumber>();
			for (PhoneBean phoneBean : patientBean.getPhone()) {
				PhoneNumber phone = new PhoneNumber();
				phone.setNumber(phoneBean.getPhone());
				phone.setExtension(phoneBean.getExtension());
				phone.setAreaCode(phoneBean.getAeraCode());
				phone.setCountryCode(phoneBean.getCountryCode());
				phone.setNote(phoneBean.getNote());
				if (phoneBean.getUseCode() != null) { 
					phone.setType(Helper.phoneType(phoneBean.getUseCode()));
				}
			}
			patient.setPhoneNumbers(phones);
		}
		//Language
		patient.setPrimaryLanguage(patientBean.getLanguage());
		//Race
		patient.setRace(patientBean.getRace());
		//Religion
		patient.setReligion(patientBean.getReligion());
		//SSN
		patient.setSsn(patientBean.getSSN());
		//Death indicator
		if (patientBean.getDeathIndicator() != null) {
			patient.setDeathIndicator(Boolean.parseBoolean(patientBean.getDeathIndicator()));
		}
		return patient;
	}
	
	public static PatientBean toBean(PdqQuery query) {
		PatientBean patientBean = new PatientBean();
		if (query.getPersonName() != null) {
			patientBean.setFirstName(query.getPersonName().getFirstName());
			patientBean.setMiddleName(query.getPersonName().getSecondName());
			patientBean.setLastName(query.getPersonName().getLastName());
			patientBean.setDegree(query.getPersonName().getDegree());
			patientBean.setPrefix(query.getPersonName().getPrefix());
			patientBean.setSuffix(query.getPersonName().getSuffix());
		}
		if (query.getSsn() != null ) {
			patientBean.setSSN(query.getSsn());
		}
		if (query.getSex() != null) {
			patientBean.setGender(query.getSex().getCDAValue());
		}
		if (query.getBirthDate() != null) {
			patientBean.setDOB(Helper.format(query.getBirthDate()));
		}
		if (query.getDriversLicense() != null) {
			DriverLicenseBean dlBean = toBean(query.getDriversLicense());
			patientBean.getDriverLicense().add(dlBean);
		}
		if (query.getAddress() != null ) {
			AddressBean addressBean = toBean(query.getAddress());
			patientBean.getAddress().add(addressBean);
		}
		if	(query.getPhone() != null) { 
			PhoneBean phone = toBean(query.getPhone());
			patientBean.getPhone().add(phone);
		}
		if (query.getPatientIdentifier() != null) {
			IdentifierBean idBean = toBean(query.getPatientIdentifier());
			patientBean.getIdentifier().add(idBean);
        }				
		return patientBean;
	}
	
	public static IdentifierBean toBean(PatientIdentifier pid) {
		IdentifierBean pidBean = new IdentifierBean();
		pidBean.setId(pid.getId());
		pidBean.setType(pid.getIdentifierTypeCode());
		if (pid.getAssigningAuthority() != null) {
			pidBean.setNamespaceId(pid.getAssigningAuthority().getNamespaceId());
			pidBean.setUniversalId(pid.getAssigningAuthority().getUniversalId());
			pidBean.setUniversalIdType(pid.getAssigningAuthority()
					.getUniversalIdType());
		}
		if (pid.getEffectiveDate() != null) {
			pidBean.setEffectiveDate(Helper.format(pid.getEffectiveDate()));
		}
		if (pid.getExpirationDate() != null) {
			pidBean.setExpirationDate(Helper.format(pid.getExpirationDate()));
		}
		return pidBean;
	}

	public static PatientIdentifier toObject(IdentifierBean pidBean) {
		PatientIdentifier pid = new PatientIdentifier();
		pid.setId(pidBean.getId());
		pid.setIdentifierTypeCode(pidBean.getType());
		if (pidBean.getEffectiveDate() != null) {
			pid.setEffectiveDate(Helper.format(pidBean
					.getEffectiveDate()));
		}
		if (pidBean.getExpirationDate() != null) {
			pid.setExpirationDate(Helper.format(pidBean
					.getExpirationDate()));
		}
		Identifier ca = new Identifier(pidBean.getNamespaceId(),
				pidBean.getUniversalId(), pidBean.getUniversalIdType());
		pid.setAssigningAuthority(ca);
		pid.setAssigningFacility(ca);
		return pid;
	}

	public static PatientBean toBean(Patient patient) {
		PatientBean patientBean = new PatientBean();
		
		patientBean.setBirthPlace(patient.getBirthPlace());
		patientBean.setDeathIndicator(Boolean.toString(patient.isDeathIndicator()));
		patientBean.setCitizenship(patient.getCitizenship());
		patientBean.setLanguage(patient.getPrimaryLanguage());
		patientBean.setFirstName(patient.getPatientName().getFirstName());
		patientBean.setMiddleName(patient.getPatientName().getSecondName());
		patientBean.setLastName(patient.getPatientName().getLastName());
		patientBean.setPrefix(patient.getPatientName().getPrefix());
		patientBean.setSuffix(patient.getPatientName().getSuffix());
		patientBean.setDegree(patient.getPatientName().getDegree());
		patientBean.setSSN(patient.getSsn());
		if (patient.getAdministrativeSex() != null) {
			patientBean.setGender((patient.getAdministrativeSex().getCDAValue()));
		}
		if (patient.getBirthDateTime() != null) {
			patientBean.setDOB(Helper.format(patient.getBirthDateTime()));
		}
		patientBean.setRace(patient.getRace());
		patientBean.setReligion(patient.getReligion());
		patientBean.setMaritalStatus(patient.getMaritalStatus());
		patientBean.setEthnic(patient.getEthnicGroup());
		List<DriverLicenseBean> dlBeans = patientBean.getDriverLicense();
		if (patient.getDriversLicense() != null) {
			DriverLicenseBean dlBean = toBean(patient.getDriversLicense());
			dlBeans.add(dlBean);
		}
		List<AddressBean> addressBeans = patientBean.getAddress();		
		for (Address address : patient.getAddresses()) {
			AddressBean addressBean = toBean(address);
			addressBeans.add(addressBean);
		}
		List<PhoneBean> phoneBeans = patientBean.getPhone();
		for (PhoneNumber phone : patient.getPhoneNumbers()) {
			PhoneBean phoneBean = toBean(phone);
			phoneBeans.add(phoneBean);
		}	
		List<IdentifierBean> pidBeans = patientBean.getIdentifier();		
		for (PatientIdentifier pid : patient.getPatientIds()) {
			IdentifierBean pidBean = toBean(pid);
			pidBeans.add(pidBean);
		}		
		return patientBean;
	}

	public static DriverLicenseBean toBean(DriversLicense driverLicense) {
		DriverLicenseBean dlBean = new DriverLicenseBean();
		dlBean.setDriverLicenseId(driverLicense.getLicenseNumber());
		dlBean.setIssuingState(driverLicense.getIssuingState());
		if (driverLicense.getExpirationDate() != null) {
			dlBean.setExpirationDate(Helper.format(driverLicense.getExpirationDate()));
		}
		return dlBean;
	}

	public static AddressBean toBean(Address address) {
		AddressBean addressBean = new AddressBean();
		addressBean.setAddressLine1(address.getAddLine1());
		addressBean.setAddressLine2(address.getAddLine2());
		addressBean.setCity(address.getAddCity());
		addressBean.setState(address.getAddState());
		addressBean.setCountry(address.getAddCountry());
		addressBean.setZip(address.getAddZip());
		if (address.getAddType() != null) {
			addressBean.setAddressType(address.getAddType().getValue());
		}
		return addressBean;
	}

	public static PhoneBean toBean(PhoneNumber phone) {
		PhoneBean phoneBean = new PhoneBean();
		phoneBean.setPhone(phone.getNumber());
		phoneBean.setExtension(phone.getExtension());
		phoneBean.setAeraCode(phone.getAreaCode());
		phoneBean.setCountryCode(phone.getCountryCode());
		if (phone.getType() != null) {
			phoneBean.setUseCode(phone.getType().getValue());
		}
		phoneBean.setNote(phone.getNote());
		return phoneBean;
	}
}
