/* *************************************************************************
 *
 *  Copyright (c) 2011, NetGen Software Inc., All Rights Reserved
 *
 *  This program, and all the routines referenced herein, are the proprietary
 *  properties and trade secrets of NetGen Software Inc.
 *
 *  Except as provided for by license agreement, this program shall not be
 *  duplicated, used, or disclosed without written consent signed by an officer
 *  of NetGen Software Inc.
 *
 ***************************************************************************/
package org.openhealthexchange.pixpdqadapter.iopenmpi.data;

import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.text.ParseException;

import com.misyshealthcare.connect.base.SharedEnums.PhoneType;

/**
 * This class implements the utilities.
 * 
 * @author charles.ye@netgensoftware.com
 * @version 1.0
 */
public class Helper {
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd" + " HH:mm:ss");
		
	public static Calendar format(String value) {
		Calendar calendar = Calendar.getInstance();
		try {
			calendar.setTime(dateFormat.parse(value + " 00:00:00"));
		} catch (ParseException pex) {
		}
		return calendar;
	}
	
	public static String format(Calendar calendar) {
		return dateFormat.format(calendar.getTime());
	}
	
	public static PhoneType phoneType(String phoneNumber) {

		if (phoneNumber.equalsIgnoreCase("home")) {
			return PhoneType.HOME;
		}
		if (phoneNumber.equalsIgnoreCase("work")) {
			return PhoneType.WORK;
		}
		if (phoneNumber.equalsIgnoreCase("cell")) {
			return PhoneType.CELL;
		}
		if (phoneNumber.equalsIgnoreCase("Emergency")) {
			return PhoneType.EMERGENCY;
		}
		if (phoneNumber.equalsIgnoreCase("Fax")) {
			return PhoneType.FAX;
		}
		if (phoneNumber.equalsIgnoreCase("Service")) {
			return PhoneType.SERVICE;
		} else {
			return PhoneType.UNKNOWN;
		}
	}
}
