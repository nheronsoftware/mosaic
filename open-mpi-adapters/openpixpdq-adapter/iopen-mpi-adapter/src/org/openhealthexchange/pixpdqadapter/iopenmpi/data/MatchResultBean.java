/* *************************************************************************
 *
 *  Copyright (c) 2011, NetGen Software Inc., All Rights Reserved
 *
 *  This program, and all the routines referenced herein, are the proprietary
 *  properties and trade secrets of NetGen Software Inc.
 *
 *  Except as provided for by license agreement, this program shall not be
 *  duplicated, used, or disclosed without written consent signed by an officer
 *  of NetGen Software Inc.
 *
 ***************************************************************************/
package org.openhealthexchange.pixpdqadapter.iopenmpi.data;

import java.util.List;
import java.util.ArrayList;
import com.netgen.ihe.patient.IdentifierBean;

/**
 * This class wraps the match result of the iOpenMpi patient mpi application.
 * 
 * @author charles.ye@netgensoftware.com
 * @version 1.0
 */
public class MatchResultBean {

	public static final String MATCHED ="3";
	
	public static final String MATCHED_SYSTEM_ID ="2";
	
	public static final String ADDED_EO ="1";
	
	public static final String NOT_MATCHED_NOT_ALLOWED_ADD ="11";
	
	public static final String MATCHED_NOT_ALLOWED_UPDATE ="12";
	
	public static final String MATCHED_SYSTEM_ID_NOT_ALLOWED_UPDATE ="13";
		
	private List<IdentifierBean> idBeans;
	
	private String euid;
	
	private String matchCode;
	
	public MatchResultBean(){	
	}
	
	public MatchResultBean(String euid, String matchCode) {
		this.euid = euid;
		this.matchCode = matchCode;
	}
	
	public String getMatchCode(){
		return this.matchCode;
	}
	
	public List<IdentifierBean> getIdentifierBeans() {
		return this.idBeans;
	}

	public String getEuid() {
		return this.euid;
	}
	
	public void setMatchCode(String matchCode){
		this.matchCode = matchCode;
	}
	
	public void setIdentifierBean(IdentifierBean idBean) {
		if (idBeans == null) {
			idBeans = new ArrayList<IdentifierBean>();
		}
		this.idBeans.add(idBean);
	}
	
	public void setEuid(String euid){
		this.euid = euid;
	}
}
