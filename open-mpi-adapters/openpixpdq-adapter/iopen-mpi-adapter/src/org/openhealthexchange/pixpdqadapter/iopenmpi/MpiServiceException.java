/* *************************************************************************
 *
 *  Copyright (c) 2011, NetGen Software Inc., All Rights Reserved
 *
 *  This program, and all the routines referenced herein, are the proprietary
 *  properties and trade secrets of NetGen Software Inc.
 *
 *  Except as provided for by license agreement, this program shall not be
 *  duplicated, used, or disclosed without written consent signed by an officer
 *  of NetGen Software Inc.
 *
 ***************************************************************************/
package org.openhealthexchange.pixpdqadapter.iopenmpi;

/**
 * This class implements the mpi service exception
 * 
 * @author charles.ye@netgensoftware.com
 * @version 1.0  
 */
public class MpiServiceException extends Exception {

    public MpiServiceException() {
        super();
    }

    public MpiServiceException(String msg) {
        super(msg);
    }

    public MpiServiceException(String msg, Throwable t) {
        super(msg, t);
    }

    public MpiServiceException(Throwable t) {
        super(t);
    }

}
