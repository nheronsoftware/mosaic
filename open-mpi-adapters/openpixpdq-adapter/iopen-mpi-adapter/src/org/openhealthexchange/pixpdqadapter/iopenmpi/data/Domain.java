/* *************************************************************************
 *
 *  Copyright (c) 2011, NetGen Software Inc., All Rights Reserved
 *
 *  This program, and all the routines referenced herein, are the proprietary
 *  properties and trade secrets of NetGen Software Inc.
 *
 *  Except as provided for by license agreement, this program shall not be
 *  duplicated, used, or disclosed without written consent signed by an officer
 *  of NetGen Software Inc.
 *
 ***************************************************************************/
package org.openhealthexchange.pixpdqadapter.iopenmpi.data;

import java.io.Serializable;

/**
 * This class implements the IHE Domain.
 * 
 * @author charles.ye@netgensoftware.com
 * @version 1.0
 */
public class Domain implements Serializable {
    private static final long serialVersionUId = 7485065400263134364L;

    private String name;
    
    private String namespaceId;
    
    private String universalId;
    
    private String universalIdType;
    
    private String description;

    public Domain() {
    }

    public Domain(String namespaceId) {
        this(namespaceId, null, null);
    }

    public Domain(String namespaceId, String universalId) {
        this(namespaceId, universalId, "ISO");
    }

    public Domain(String namespaceId, String universalId, String universalIdType) {
        this.namespaceId = namespaceId;
        this.universalId = universalId;
        this.universalIdType = universalIdType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNamespaceId() {
        return namespaceId;
    }

    public void setName(String name) {
    	this.name = name;
    }
    
    public void setNamespaceId(String namespaceId) {
        this.namespaceId = namespaceId;
    }

    public String getName() {
    	return this.name;
    }
    
    public String getUniversalId() {
        return universalId;
    }

    public void setUniversalId(String universalId) {
        this.universalId = universalId;
    }

    public String getUniversalIdType() {
        return universalIdType;
    }

    public void setUniversalIdType(String universalIdType) {
        this.universalIdType = universalIdType;
    }

	@Override
	public boolean equals(Object obj) {
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}

		final Domain other = (Domain) obj;
		if ((this.namespaceId == null) ? (other.namespaceId != null)
				: !this.namespaceId.equals(other.namespaceId)) {
			return false;
		}
		if ((this.universalId == null) ? (other.universalId != null)
				: !this.universalId.equals(other.universalId)) {
			return false;
		}
		if ((this.universalIdType == null) ? (other.universalIdType != null)
				: !this.universalIdType.equals(other.universalIdType)) {
			return false;
		}

		return true;
	}

 
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Domain: ");   
        if (name != null) {
            sb.append("name='");      
            sb.append(name);
            sb.append("', ");               
        }

        if (namespaceId != null) {
            sb.append("namespaceId='");      
            sb.append(namespaceId);
            sb.append("', ");               
        }

        if (universalId != null) {
            sb.append("universalId='");      
            sb.append(universalId);
            sb.append("', ");                
        }

        if (universalIdType != null) {
            sb.append("universalIdType='");  
            sb.append(universalIdType);
            sb.append("'");                 
        }

        return sb.toString();
    }	
}
