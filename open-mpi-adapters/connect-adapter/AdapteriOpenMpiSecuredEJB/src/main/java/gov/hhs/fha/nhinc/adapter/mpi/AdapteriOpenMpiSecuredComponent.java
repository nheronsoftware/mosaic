/* *************************************************************************
 *
 *  Copyright (c) 2011, NetGen Software Inc., All Rights Reserved
 *
 *  This program, and all the routines referenced herein, are the proprietary
 *  properties and trade secrets of NetGen Software Inc.
 *
 *  Except as provided for by license agreement, this program shall not be
 *  duplicated, used, or disclosed without written consent signed by an officer
 *  of NetGen Software Inc.
 *
 ***************************************************************************/
package gov.hhs.fha.nhinc.adapter.mpi;

import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.xml.ws.BindingType;
import org.hl7.v3.PRPAIN201306UV02;
import org.hl7.v3.PRPAIN201305UV02;

/**
 * AdapteriOpenMpiSecuredComponent class
 * @author charles.ye@netgensoftware.com
 */
@WebService(serviceName = "CONNECTAdapter", portName = "AdapterComponentMpiSecuredPort", endpointInterface = "gov.hhs.fha.nhinc.adaptercomponentmpi.AdapterComponentMpiSecuredPortType", targetNamespace = "urn:gov:hhs:fha:nhinc:adaptercomponentmpi", wsdlLocation = "META-INF/wsdl/AdapteriOpenMpiSecured/AdapterComponentSecuredMpi.wsdl")
@Stateless(name="AdapteriOpenMpiSecuredComponent")
@BindingType(value = javax.xml.ws.soap.SOAPBinding.SOAP12HTTP_BINDING)
public class AdapteriOpenMpiSecuredComponent {
    
    private iOpenMpiService mpiService;
    
    public AdapteriOpenMpiSecuredComponent() {
        mpiService = new iOpenMpiService(); 
    }
    
    public PRPAIN201306UV02 findCandidates(PRPAIN201305UV02 request) {
        return mpiService.findCandidates(request);
    }
}
