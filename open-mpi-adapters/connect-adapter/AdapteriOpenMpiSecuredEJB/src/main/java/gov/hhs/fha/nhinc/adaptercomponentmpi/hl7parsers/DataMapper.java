/* *************************************************************************
 *
 *  Copyright (c) 2011, NetGen Software Inc., All Rights Reserved
 *
 *  This program, and all the routines referenced herein, are the proprietary
 *  properties and trade secrets of NetGen Software Inc.
 *
 *  Except as provided for by license agreement, this program shall not be
 *  duplicated, used, or disclosed without written consent signed by an officer
 *  of NetGen Software Inc.
 *
 ***************************************************************************/
package gov.hhs.fha.nhinc.adaptercomponentmpi.hl7parsers;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import gov.hhs.fha.nhinc.mpilib.Address;
import gov.hhs.fha.nhinc.mpilib.Addresses;
import gov.hhs.fha.nhinc.mpilib.Identifier;
import gov.hhs.fha.nhinc.mpilib.Identifiers;
import gov.hhs.fha.nhinc.mpilib.Patient;
import gov.hhs.fha.nhinc.mpilib.Patients;
import gov.hhs.fha.nhinc.mpilib.PersonName;
import gov.hhs.fha.nhinc.mpilib.PersonNames;
import gov.hhs.fha.nhinc.mpilib.PhoneNumber;
import gov.hhs.fha.nhinc.mpilib.PhoneNumbers;
import com.netgen.ihe.patient.PatientBean;
import com.netgen.ihe.patient.AddressBean;
import com.netgen.ihe.patient.IdentifierBean;
import com.netgen.ihe.patient.PhoneBean;

/**
 * This class is to interact with hl7 patient and iopen mpi patient
 *
 * @author charles.ye@netgensoftware.com
 * @version 1.0
 */
public class DataMapper {

     public static AddressBean map(Address hl7address) {
        AddressBean address = new AddressBean();
        address.setAddressLine1(hl7address.getStreet1());
        address.setAddressLine2(hl7address.getStreet2());
        address.setCity(hl7address.getCity());
        address.setState(hl7address.getState());
        address.setZip(hl7address.getZip());
        return address;
     }

     public static List<IdentifierBean> map(Identifiers hl7identifiers) {
        List<IdentifierBean> identifiers = new ArrayList<IdentifierBean>();
        Iterator<Identifier> elements = hl7identifiers.iterator();
        while (elements.hasNext()) {
            Identifier hl7identifier = elements.next();
            IdentifierBean identifier = map(hl7identifier);
            identifiers.add(identifier);
        }
        return identifiers;
     }

     public static IdentifierBean map(Identifier hl7identifier) {
        IdentifierBean identifier = new IdentifierBean();
        identifier.setId(hl7identifier.getId());
        identifier.setNamespaceId(hl7identifier.getOrganizationId());
        return identifier;
     }

      public static Patients map(List<PatientBean> patients) {
        Patients hl7patients = new Patients();
        for (PatientBean patient : patients) {
            Patient hl7patient = map(patient);
            hl7patients.add(hl7patient);
        }
        return hl7patients;
     }

     public static Patient map(PatientBean patient) {
        Patient hl7patient = new Patient();
        PersonNames names = new PersonNames();
        PersonName name = new PersonName();
        name.setFirstName(patient.getFirstName());
        name.setLastName(patient.getLastName());
        names.add(name);
        hl7patient.setNames(names);
        hl7patient.setFirstName(patient.getFirstName());
        hl7patient.setLastName(patient.getLastName());
        hl7patient.setGender(patient.getGender());
        hl7patient.setDateOfBirth(patient.getDOB());
        hl7patient.setSSN(patient.getSSN());

        Addresses hl7addresses = new Addresses();
        List<AddressBean> addresses = patient.getAddress();
        if (addresses != null && 
            !addresses.isEmpty()) {
            for (AddressBean address : addresses) {
                Address hl7address = map(address);
                hl7addresses.add(hl7address);
            }
            hl7patient.setAddresses(hl7addresses);
        }

        PhoneNumbers hl7phones = new PhoneNumbers();
        List<PhoneBean> phones = patient.getPhone();
        if (phones != null &&
            !phones.isEmpty()) {
            for (PhoneBean phone : phones) {
                PhoneNumber hl7phone = map(phone);
                hl7phones.add(hl7phone);
            }
            hl7patient.setPhoneNumbers(hl7phones);
        }

        Identifiers hl7identifiers = new Identifiers();
        List<IdentifierBean> identifiers = patient.getIdentifier();
        if (identifiers != null &&
            !identifiers.isEmpty()) {
            for (IdentifierBean identifier : identifiers) {
                Identifier hl7identifier = map(identifier);
                hl7identifiers.add(hl7identifier);
            }
            hl7patient.setIdentifiers(hl7identifiers);
        }

        return hl7patient;
     }

     public static Identifier map(IdentifierBean identifier) {
        Identifier hl7identifier = new Identifier();
        hl7identifier.setId(identifier.getId());
        hl7identifier.setOrganizationId(identifier.getNamespaceId());
        return hl7identifier;
     }

     public static PhoneNumber map(PhoneBean phone) {
        PhoneNumber hl7phone = new PhoneNumber();
        hl7phone.setPhoneNumber(phone.getPhone());
        return hl7phone;
     }

     public static Address map(AddressBean address) {
        Address hl7address = new Address();
        hl7address.setStreet1(address.getAddressLine1());
        hl7address.setStreet2(address.getAddressLine2());
        hl7address.setCity(address.getCity());
        hl7address.setState(address.getState());
        hl7address.setZip(address.getZip());
        return hl7address;
     }
}
