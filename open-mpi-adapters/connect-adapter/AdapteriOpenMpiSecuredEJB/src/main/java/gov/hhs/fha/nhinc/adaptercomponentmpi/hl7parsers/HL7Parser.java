/* *************************************************************************
 *
 *  Copyright (c) 2011, NetGen Software Inc., All Rights Reserved
 *
 *  This program, and all the routines referenced herein, are the proprietary
 *  properties and trade secrets of NetGen Software Inc.
 *
 *  Except as provided for by license agreement, this program shall not be
 *  duplicated, used, or disclosed without written consent signed by an officer
 *  of NetGen Software Inc.
 *
 ***************************************************************************/
package gov.hhs.fha.nhinc.adaptercomponentmpi.hl7parsers;

import java.util.List;
import org.hl7.v3.PRPAIN201305UV02;
import org.hl7.v3.PRPAMT201306UV02ParameterList;
import org.hl7.v3.PRPAIN201306UV02;
import gov.hhs.fha.nhinc.mpilib.Address;
import gov.hhs.fha.nhinc.mpilib.Identifiers;
import gov.hhs.fha.nhinc.mpilib.PersonName;
import gov.hhs.fha.nhinc.mpilib.Patients;
import gov.hhs.fha.nhinc.mpi.adapter.component.hl7parsers.HL7Parser201305;
import gov.hhs.fha.nhinc.mpi.adapter.component.hl7parsers.HL7DbParser201305;
import gov.hhs.fha.nhinc.mpi.adapter.component.hl7parsers.HL7Parser201306;
import com.netgen.ihe.patient.PatientBean;
import com.netgen.ihe.patient.AddressBean;
import com.netgen.ihe.patient.IdentifierBean;
import com.netgen.ihe.patient.PhoneBean;

/**
 * HL7Parser parser
 *
 * @author charles.ye@netgensoftware.com
 * @version 1.0
 */
public class HL7Parser {

    public static PatientBean parse(PRPAIN201305UV02 message) {
        PatientBean patient = new PatientBean();
        PRPAMT201306UV02ParameterList queryParameters = HL7Parser201305.ExtractHL7QueryParamsFromMessage(message);
        if (queryParameters != null) {
            // patient Identifier for Demographic Query and Feed mode
            PersonName name = HL7Parser201305.ExtractPersonName(queryParameters);
            if (name != null) {
                patient.setFirstName(name.getFirstName());
                patient.setLastName(name.getFirstName());
                patient.setMiddleName(name.getMiddleName());
            }

            String gender = HL7Parser201305.ExtractGender(queryParameters);
            if (gender != null) {
                patient.setGender(gender);
            }

            String dob = HL7Parser201305.ExtractBirthdate(queryParameters);
            if (dob != null) {
                patient.setDOB(dob);
            }
        }
        return patient;
    }

    public static PRPAIN201306UV02 parse(List<PatientBean> patients, PRPAIN201305UV02 request) {
        Patients hl7patients = DataMapper.map(patients);
        PRPAIN201306UV02 response = HL7Parser201306.BuildMessageFromMpiPatient(hl7patients, request);
        return response;
    }
}
