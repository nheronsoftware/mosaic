/* *************************************************************************
 *
 *  Copyright (c) 2011, NetGen Software Inc., All Rights Reserved
 *
 *  This program, and all the routines referenced herein, are the proprietary
 *  properties and trade secrets of NetGen Software Inc.
 *
 *  Except as provided for by license agreement, this program shall not be
 *  duplicated, used, or disclosed without written consent signed by an officer
 *  of NetGen Software Inc.
 *
 ***************************************************************************/
package gov.hhs.fha.nhinc.adapter.mpi;

import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import javax.xml.ws.BindingProvider;
import org.hl7.v3.PRPAIN201306UV02;
import org.hl7.v3.PRPAIN201305UV02;
import gov.hhs.fha.nhinc.properties.PropertyAccessor;
import gov.hhs.fha.nhinc.properties.PropertyAccessException;
import gov.hhs.fha.nhinc.adaptercomponentmpi.hl7parsers.HL7Parser;
import com.netgen.ihe.patient.PatientBean;
import com.netgen.ihe.patient.PatientEJBService;
import com.netgen.ihe.patient.PatientEJB;
import com.netgen.ihe.patient.SearchPatientResult;
import com.netgen.ihe.patient.ProcessingException_Exception;
import com.netgen.ihe.patient.UserException_Exception;

/**
 * This class is to interact with the iOpenMpi patient mpi application.
 * 
 * @author charles.ye@netgensoftware.com
 * @version 1.0  
 */
public class iOpenMpiService {

    public static final String IOPENMPI_PROPERTY_FILE = "iopenmpi";

    public static final String IOPENMPI_PATIEN_ENDPOINT_URL = "iopenmpi.patient.endpoint.url";

    public static final String IOPENMPI_PATIEN_MATCH_THRESHOLD = "iopenmpi.patient.match.threshold";
    
    private PatientEJB mpiService;
    
    private float matchThreshold;
       
    public iOpenMpiService() {
        initialize();
    }

    public void initialize() {
        try {
            String url = PropertyAccessor.getProperty(IOPENMPI_PROPERTY_FILE, IOPENMPI_PATIEN_ENDPOINT_URL);
            String value = PropertyAccessor.getProperty(IOPENMPI_PROPERTY_FILE, IOPENMPI_PATIEN_MATCH_THRESHOLD);
            matchThreshold = Float.parseFloat(value); 
            PatientEJBService service = new PatientEJBService();
            mpiService = service.getPatientEJBPort();
            Map<String, Object> context = ((BindingProvider) mpiService).getRequestContext();
            context.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, url);
        } catch (PropertyAccessException pex) {
        }
    }

    public PRPAIN201306UV02 findCandidates(PRPAIN201305UV02 request) {
        PRPAIN201306UV02 response = null;
        try {
            PatientBean patient = HL7Parser.parse(request);
            boolean alpha = false;
            String firstName = patient.getFirstName();
            if (firstName != null && 
                firstName.contains("*")) {
                alpha = true;
                patient.setFirstName(firstName.replace('*', '%'));
            }
            String lastName = patient.getLastName();
            if (lastName != null && 
                lastName.contains("*")) {
                alpha = true;
                patient.setLastName(lastName.replace('*', '%'));
            }
            List<SearchPatientResult> searchResults = null;
            if (alpha) {
                searchResults = mpiService.searchExact(patient);
            } else {
                searchResults = mpiService.searchBlock(patient);
            }
            List<PatientBean> patients = new ArrayList<PatientBean>();
            for (SearchPatientResult searchResult : searchResults) {
                if(searchResult.getComparisonScore() >= matchThreshold) {
                   patients.add(searchResult.getPatient());
                }  
            }
            response = HL7Parser.parse(patients, request);
        } catch (UserException_Exception uex) {
        } catch (ProcessingException_Exception pex) {
        }
        return response;
    }
}
