/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html
 * or mural/license.txt. See the License for the specific language 
 * governing permissions and limitations under the License.  
 *
 * When distributing Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at mural/license.txt.
 * If applicable, add the following below the CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.dm.dq.profiler;

import com.sun.dm.dimi.datareader.DataSourceReaderFactory;
import com.sun.dm.dimi.datareader.GlobalDataObjectReader;
import com.sun.dm.dq.process.DataAnalysisProcess;
import com.sun.dm.dq.process.ProfilingProcess;
import com.sun.dm.dq.rules.data.DataReader.CustomDataReader;
import com.sun.dm.dq.rules.data.DataReader.DataObjectDataReader;
import com.sun.dm.dq.rules.exception.DBHandlerException;
import com.sun.dm.dq.rules.exception.ProfilerRuleException;
import com.sun.dm.dq.util.Localizer;
import com.sun.mdm.index.dataobject.DataObject;
import com.sun.mdm.index.dataobject.InvalidRecordFormat;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.logging.LogManager;

import net.java.hulp.i18n.Logger;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class Profiler {

    /**
     * Logger instance
     */
    private static Logger mLogger = Logger.getLogger(Profiler.class.getName());
    /**
     * i18n utility 
     */
    private static Localizer mLoc = Localizer.get();
    private static CustomDataReader customDSReader;
    private static DataObjectDataReader dataObjectDBReader;
    private static GlobalDataObjectReader doReader;
    private static ProfilingProcess profiler;

    public static void main(String[] args) {

        /**
         * @param args the command line arguments
         */
        confiugreLogger();
        if (args.length > 0) {

            String confLoc = args[0];
            profileData(confLoc);
            mLogger.info(mLoc.x("PRF009: Finished Profiling !!! "));
            System.exit(0);
        }

    }

    private static DataObject readNext() {
        DataObject dataObject = null;
        if (customDSReader != null) {
            dataObject = customDSReader.readNext();
        } else if (dataObjectDBReader != null) {
            dataObject = dataObjectDBReader.readNext();
        } else {
            try {
                dataObject = (DataObject) doReader.readDataObject();
                if (mLogger.isFine()) {
                    mLogger.fine("got the data object from plug-in successfully");
                }
            } catch (InvalidRecordFormat e) {
                mLogger.severe(mLoc.x("PRF010: Invalid Record Format: {0}", e), e);

            }
        }
        return dataObject;
    }

    private static void confiugreLogger() {

        FileInputStream ins = null;
        try {
            LogManager logManager;
            String config = "config/logger.properties";
            File f = new File("./logs");

            if (f.exists() && f.isDirectory()) {
                mLogger.info(mLoc.x("logger dir exist"));
            } else {
                mLogger.info(mLoc.x("creating new logger dir"));
                f.mkdir();
            }

            logManager = LogManager.getLogManager();
            ins = new FileInputStream(config);
            logManager.readConfiguration(ins);

        } catch (IOException ex) {
            mLogger.infoNoloc(ex.getLocalizedMessage());
        } catch (SecurityException ex) {
            mLogger.infoNoloc(ex.getLocalizedMessage());
        } catch (Exception ex) {
            mLogger.infoNoloc(ex.getLocalizedMessage());
        } finally {
            try {
                if (ins != null) {
                    ins.close();
                }
            } catch (IOException ex) {
                mLogger.infoNoloc(ex.getLocalizedMessage());
            } catch (Exception ex) {
                mLogger.infoNoloc(ex.getLocalizedMessage());
            }
        }

    }

    protected static void profileData(String configLocation) {

        boolean configset = false;
        DataObject dobj;

        try {
            DataAnalysisProcess da = new DataAnalysisProcess(configLocation);

            profiler = da.getProfilingProcess();
            mLogger.info(mLoc.x("PRF001: Received Profiler Process !!!"));
            customDSReader = profiler.getDbReader();
            dataObjectDBReader = profiler.getDataObjectdbReader();
            if (customDSReader == null && dataObjectDBReader == null) {
                configset = DataSourceReaderFactory.setEViewConfigFilePath(profiler.getObjectDefFilePath());
                if (configset) {
                    doReader = DataSourceReaderFactory.getDataSourceReader(profiler.getDBPath(), true);
                    if (doReader == null) {
                        mLogger.severe(mLoc.x("PRF002: Failed to load data reader !!!"));
                        return;
                    }
                }
            }

            int count = 0;
            boolean continueLoop = true;

            int recordNumber = 1;
            int startFrom = profiler.getStartFrom();

            while (recordNumber < startFrom) {
                dobj = readNext();
                dobj = null;
                recordNumber++;
            }

            while (continueLoop) {
                count++;

                dobj = readNext();
                if (dobj != null) {

                    profiler.executeRules(dobj);
                    if (doReader != null) {
                        doReader.submitObjectForFinalization(dobj);
                    }
                    if (profiler.getProfileSize() == count) {
                        mLogger.info(mLoc.x("PRF003: Profiler has finished profiling : {0}  records. ", String.valueOf(count - 1).toString()));
                        mLogger.info(mLoc.x("PRF004: Generating Reports ..."));
                        profiler.generateReport();
                        mLogger.info(mLoc.x("PRF005: Reports generation is done !!! "));
                        if (doReader != null) {
                            doReader.close();
                            dobj = null;
                        }
                        break;
                    }
                } else if (count > 1) {
                    mLogger.info(mLoc.x("PRF003: Profiler has finished profiling : {0}  records. ", String.valueOf(count - 1).toString()));
                    mLogger.info(mLoc.x("PRF004: Generating Reports ..."));
                    profiler.generateReport();
                    mLogger.info(mLoc.x("PRF005: Reports generation is done !!! "));
                    dobj = null;
                    //System.exit(0);
                    break;
                } else {
                    if (doReader != null) {
                        doReader.close();
                        dobj = null;

                    //System.exit(0);
                    }
                    break;
                }
            }


        } catch (DBHandlerException ex) {
            mLogger.severe(mLoc.x("PRF006: Data Handler failed: {0}", ex), ex);
        } catch (ProfilerRuleException ex) {
            mLogger.severe(mLoc.x("PRF007: Profiler Rule exception: {0}", ex), ex);
        } catch (InvalidRecordFormat ex) {
            mLogger.severe(mLoc.x("PRF008: Invalid record format: {0}", ex), ex);
        } catch (Exception ex) {
            mLogger.severe(mLoc.x("PRF099: System Exception: {0}", ex), ex);
        }

    }
}

