/*
 * ProfilingProcessTest.java
 * JUnit based test
 *
 * Created on October 29, 2007, 5:56 PM
 */

package com.sun.dm.dq.process;

import junit.framework.*;
import com.sun.dm.dq.rules.data.DBHandler;
import com.sun.dm.dq.rules.data.DataObjectHandler;
import com.sun.dm.dq.rules.exception.DBHandlerException;
import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import com.sun.dm.dq.rules.exception.ProfilerRuleException;
import com.sun.dm.dq.rules.process.rulestep.ProfilingRulesImpl;
import com.sun.dm.dq.rules.variablelist.VarListProcess;
import com.sun.mdm.index.dataobject.DataObject;
import com.sun.dm.dq.schema.ProfilingRuleType;

/**
 *
 * @author abhijeet
 */
public class ProfilingProcessTest extends TestCase {
    
    ProfilingProcess instance;
    
    public ProfilingProcessTest(String testName) {
        super(testName);
    }
    
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    protected void tearDown() throws Exception {
        super.tearDown();
    }
    
    /**
     * Test of getProfilingProcess method, of class com.sun.di.process.ProfilingProcess.
     */
    public void testGetProfilingProcess() throws Exception {
        System.out.println("getProfilingProcess");
        
       /* DataAnalysisProcess da = new DataAnalysisProcess(System.getProperty("user.dir") + "\\src\\test\\java\\resources\\TestDataAnalysisConfig.xml");
        instance = da.getProfilingProcess();
        assertNotNull(instance);*/
        // TODO review the generated test code and remove the default call to fail.
        // fail("The test case is a prototype.");
    }
    
    /**
     * Test of executeRules method, of class com.sun.di.process.ProfilingProcess.
     */
    public void testExecuteRules() throws Exception {
        System.out.println("executeRules");
        
       /* DataAnalysisProcess da = new DataAnalysisProcess(System.getProperty("user.dir") + "\\src\\test\\java\\resources\\TestDataAnalysisConfig.xml");
        instance = da.getProfilingProcess();
        assertNotNull(instance);
        
        DataObjectCreator dc = new DataObjectCreator();
        
        
        DataObject parent = dc.createDataObject("My First Name","My Last Name","30","Male","10/11/2005");
        
        for (int i = 0; i < instance.getProfileSize() ; i++) {
            
            if ( i%100 == 0) {
                parent = dc.createDataObject("My First Name" ,"My Last Name","30","Male","10/11/2005");
            } else {
                parent = dc.createDataObject("My First Name" + String.valueOf(i),"My Last Name"+ String.valueOf(i),"30","Male","10/11/2005");
            }
            instance.executeRules(parent);
        }
        */
        
        // TODO review the generated test code and remove the default call to fail.
        // fail("The test case is a prototype.");
    }
    
    /**
     * Test of GenerateReport method, of class com.sun.di.process.ProfilingProcess.
     */
    
    public void testGenerateReport() throws Exception {
        System.out.println("testGenerateReport");
        
       /* DataAnalysisProcess da = new DataAnalysisProcess("C:/ETL/Cleansing_Design/test_cleansing_datalength.xml");
        instance = da.getProfilingProcess();
        assertNotNull(instance);
        
        DataObjectCreator dc = new DataObjectCreator();
        
        
        DataObject parent = dc.createDataObject("My First Name","My Last Name","30","Male","10/11/2005");
        
        for (int i = 0; i < instance.getProfileSize() ; i++) {
            
            if ( i%100 == 0) {
                parent = dc.createDataObject("My First Name" ,"My Last Name","30","Male","10/11/2005");
            } else {
                parent = dc.createDataObject("My First"+ String.valueOf(i)  + "Name" + String.valueOf(i),"My Last" + String.valueOf(i) + "Name","30","Male","10/11/2005");
            }
            instance.executeRules(parent);
        }
        instance.generateReport();
        */
        // TODO review the generated test code and remove the default call to fail.
        // fail("The test case is a prototype.");
    }
    
}
