<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
   <title>Understanding the Mural Match Engine </title>

   <meta http-equiv="Content-type" content="text/html; charset=iso-8859-1">
   <meta http-equiv="content-language" content="en-US">
   <meta name="keywords" content="">
   <meta name="description" content="Understanding the Mural Match Engine provides information to help you understand and work with the Mural Match Engine. It describes the configuration files and how you can customize the way data is matched. It also lists and describes each matching comparator function and explains how they can be customized. This guide also provides general information on determining match and duplicate weight thresholds.">
   <meta name="date" content="2008-06-01">
   <meta name="author" content="Carol Thom">

   <link rel="stylesheet" type="text/css" href="https://mural.dev.java.net/css/muraldoc.css">
   </head>

<body>

   <!-- Copyright (c) 2008 Sun Microsystems, Inc. All rights reserved. -->
   <!-- Use is subject to license terms. -->

   <a name="top"></a>
   <h1>Understanding the Mural Match Engine </h1>
   <div class="articledate" style="margin-left: 0px;">Last Updated: June 2008</div>

   <div class="embeddedtocpane">
     <h5><span class="tocpageleft"><a href="ref_sme-person_c.html">Previous</a></span>
         <span class="tocpageright"></span></h5>

     <h5><span class="toctitle">Contents</span></h5>
     <div class="embeddedtoc">
       <p class="toc level1"><a href="landingpage.html">Understanding the Mural Match Engine</a></p>
<p class="toc level1 tocsp"><a href="ref_sme-overview_c.html">Mural Match Engine Overview</a></p>
<p class="toc level1 tocsp"><a href="ref_sme-match_c.html">Mural Match Engine Matching Configuration</a></p>
<p class="toc level1 tocsp"><a href="ref_sme-comparators_c.html">Mural Match Engine Comparison Functions</a></p>
<p class="toc level1 tocsp"><a href="ref_sme-plugins_c.html">Creating Custom Comparators for the Mural Match Engine</a></p>
<p class="toc level1 tocsp"><a href="ref_sme-person_c.html">Mural Match Engine Configuration for Common Data Types</a></p>
<div class="onpage">
<p class="toc level1 tocsp"><a href="">Fine-Tuning Weights and Thresholds for Master Index Studio</a></p>
<p class="toc level2"><a href="#ref_sme-data-anal_c">Data Analysis Overview</a></p>
<p class="toc level2"><a href="#ref_sme-thresholds_c">Customizing the Match Configuration and Thresholds</a></p>
<p class="toc level3"><a href="#ref_sme-matchfields_c">Determining the Match Fields</a></p>
<p class="toc level3"><a href="#ref_sme-matchconfig_c">Customizing the Match Configuration</a></p>
<p class="toc level4"><a href="#gfjsr">Probabilities or Agreement Weights</a></p>
<p class="toc level4"><a href="#gfjsq">Defining Relative Value</a></p>
<p class="toc level4"><a href="#gfjuj">Determining the Weight Range</a></p>
<p class="toc level5"><a href="#gfjtl">Weight Ranges Using Agreement Weights</a></p>
<p class="toc level5"><a href="#gfjty">Weight Ranges Using Probabilities</a></p>
<p class="toc level4 tocsp"><a href="#gfjsy">Comparison Functions</a></p>
<p class="toc level3 tocsp"><a href="#ref_sme-weight_c">Determining the Weight Thresholds</a></p>
<p class="toc level4"><a href="#gfjsw">Specifying the Weight Thresholds</a></p>
<p class="toc level5"><a href="#gfjud">Weight Distribution Method</a></p>
<p class="toc level5"><a href="#gfjth">Percentage Method</a></p>
<p class="toc level4 tocsp"><a href="#gfjta">Fine-tuning the Thresholds</a></p>
</div>

     </div>
   </div>


   <div class="maincontent">
      <a name="ref_sme-analysis_c"></a><h3>Fine-Tuning Weights and Thresholds for Master Index Studio</h3><p>Each Master Index Studio implementation is unique, typically requiring extensive data analysis to
determine how to best configure the structure and matching logic of the master
index application. The following topics provide an overview of the process of fine-tuning
the matching logic in the match configuration file and fine-tuning the match and
duplicate thresholds.</p>
<ul><li><p><a href="#ref_sme-data-anal_c">Data Analysis Overview</a></p></li>
<li><p><a href="#ref_sme-thresholds_c">Customizing the Match Configuration and Thresholds</a></p></li></ul>


<a name="ref_sme-data-anal_c"></a>

<h2>Data Analysis Overview</h2>
<p>A thorough analysis of the data to be shared with the master
index application is a must before beginning any implementation. This analysis not only
defines the types of data to include in the object structure, but indicates
the relative reliability of each system&rsquo;s data, helps determine which fields to use for
matching, and indicates the relative reliability of each match field.</p><p><a name="indexterm-404"></a><a name="indexterm-405"></a><a name="indexterm-406"></a><a name="indexterm-407"></a>To begin the analysis, the legacy data that will be converted into the
master index database is extracted and analyzed. Once the initial analysis is complete, you
can perform an iterative process to fine-tune the matching and duplicate thresholds and
to determine the level of potential duplication in the existing data. If you
plan to use the Data Profiler and Bulk Matcher tools generated by Master
Index Studio to analyze data, review the information in  <a href="https://open-dm-dq.dev.java.net/docs/analysis/landingpage.html">Analyzing and Cleansing Data for a Sun Master Index Analyzing and Cleansing Data for a Master Index</a> and
<a href="https://open-dm-mi.dev.java.net/docs/load/lidseveispv.html">Loading the Initial Data Set for a Master Index Loading the Initial Data Set for a Master Index</a> before you extract the legacy data.</p>

<a name="ref_sme-thresholds_c"></a>

<h2>Customizing the Match Configuration and Thresholds</h2>
<p>There are three primary steps to customizing how records are matched in a
master index application.</p>
<ul><li><p><a href="#ref_sme-matchfields_c">Determining the Match Fields</a></p></li>
<li><p><a href="#ref_sme-matchconfig_c">Customizing the Match Configuration</a></p></li>
<li><p><a href="#ref_sme-weight_c">Determining the Weight Thresholds</a></p></li></ul>


<div class="indent"><a name="ref_sme-matchfields_c"></a><h3>Determining the Match Fields</h3>
<p>Before extracting data for analysis, review the types of data stored in the
messages generated by each system. Use these messages to determine which fields and
objects to include in the object structure of the master index application. From
this object structure, select the fields to use for matching. When selecting these
fields, keep in mind how representative each field is of a specific object.
For example, in a master person index, the social security number field, first
and last name fields, and birth date are good representations whereas marital status,
suffix, and title are not. Certain address information or a home telephone number
might also be considered. In a master company index, the match fields might
include any of the fields parsed from the complete company name field, as
well as a tax ID number or address and telephone information.</p></div>


<div class="indent"><a name="ref_sme-matchconfig_c"></a><h3>Customizing the Match Configuration</h3>
<p>Once you determine the fields to use for matching, determine how the weights
will be generated for each field. The primary tasks include determining whether to
use probabilities or agreement weight ranges and then choosing the best comparison functions
to use for each match field.</p>

<div class="indent"><a name="gfjsr"></a><h3>Probabilities or Agreement Weights</h3>
<p>The first step in configuring the match configuration is to decide whether to
use m-probabilities and u-probabilities or agreement and disagreement weight ranges. Both methods will
give you similar results, but agreement and disagreement weight ranges allow you to
specify the precise maximum and minimum weights that can be applied to each
match field, giving you control over the value of the highest and lowest
matching weights that can be assigned to each record.</p></div>


<div class="indent"><a name="gfjsq"></a><h3>Defining Relative Value</h3>
<p>For each field used for matching, define either the m-probabilities and u-probabilities or
the agreement and disagreement weight ranges in the match configuration file. Review the
information provided under <a href="ref_sme-overview_c.html#ref_sme-weight-formula_c">Mural Match Engine Matching Weight Formulation</a> to help determine how to configure these values.
Remember that a higher m-probability or agreement weight gives the field a higher
weight when field values agree.</p></div>


<div class="indent"><a name="gfjuj"></a><h3>Determining the Weight Range</h3>
<p>In order to find the initial values to set for the match
and duplicate thresholds, you must determine the total range of matching weights that
can be assigned to a record. This weight is the sum of all
weights assigned to each match field. Using the data analysis tool provided can
help you determine the match and duplicate thresholds.</p>

<div class="indent"><a name="gfjtl"></a><h3>Weight Ranges Using Agreement Weights</h3>
<p>For agreement and disagreement weight ranges, determining the match weight ranges is very
straightforward. Simply total the maximum agreement weights for each field to determine the
maximum match weight. Then total the minimum disagreement weights for each match field
to determine the minimum match weight. The following table provides a sample agreement/disagreement configuration
for matching on person data. As you can see, the range of
match weights generated for a master index application with this configuration is from -36
to +38.</p><a name="gfjss"></a><h6>Table&nbsp;1 Sample Agreement and Disagreement Weight Ranges</h6><table><col width="33%"><col width="33%"><col width="33%"><tr><th align="left" valign="top" scope="column"><p>Field Name</p></th>
<th align="left" valign="top" scope="column"><p>Maximum Agreement Weight</p></th>
<th align="left" valign="top" scope="column"><p>Minimum Disagreement Weight</p></th>
</tr>
<tr><td align="left" valign="top" scope="row"><p>First Name</p></td>
<td align="left" valign="top" scope="row"><p>8</p></td>
<td align="left" valign="top" scope="row"><p>-8</p></td>
</tr>
<tr><td align="left" valign="top" scope="row"><p>Last Name</p></td>
<td align="left" valign="top" scope="row"><p>8</p></td>
<td align="left" valign="top" scope="row"><p>-8</p></td>
</tr>
<tr><td align="left" valign="top" scope="row"><p>Date of Birth</p></td>
<td align="left" valign="top" scope="row"><p>7</p></td>
<td align="left" valign="top" scope="row"><p>-5</p></td>
</tr>
<tr><td align="left" valign="top" scope="row"><p>Gender</p></td>
<td align="left" valign="top" scope="row"><p>5</p></td>
<td align="left" valign="top" scope="row"><p>-5</p></td>
</tr>
<tr><td align="left" valign="top" scope="row"><p>SSN</p></td>
<td align="left" valign="top" scope="row"><p>10</p></td>
<td align="left" valign="top" scope="row"><p>-10</p></td>
</tr>
<tr><td align="left" valign="top" scope="row"><p><b>Maximum Match Weight</b></p></td>
<td align="left" valign="top" scope="row"><p>38</p></td>
<td align="left" valign="top" scope="row"></td>
</tr>
<tr><td align="left" valign="top" scope="row"><p><b>Minimum Match Weight</b></p></td>
<td align="left" valign="top" scope="row"></td>
<td align="left" valign="top" scope="row"><p>-36</p></td>
</tr>
</table></div>


<div class="indent"><a name="gfjty"></a><h3>Weight Ranges Using Probabilities</h3>
<p>Determining the match weight ranges when using m-probabilities and u-probabilities is a little
more complicated than using agreement and disagreement weights. To determine the maximum weight
that will be generated for each field, use the following formula:</p><pre>LOG2(m_prob/u_prob)</pre><p>To determine the minimum match weight that will be generated for each field,
use the following formula:</p><pre>LOG2((1-m_prob)/(1-u_prob))</pre><p>The following table illustrates m-probabilities and u-probabilities, including the corresponding agreement and disagreement
weights that are generated with each combination of probabilities. As you can see,
the range of match weights generated for a master index application with this
configuration is from -35.93 to +38</p><a name="gfjtd"></a><h6>Table&nbsp;2 Sample m-probabilities and u-probabilities</h6><table><col width="26%"><col width="16%"><col width="16%"><col width="18%"><col width="20%"><tr><th align="left" valign="top" scope="column"><p>Field Name</p></th>
<th align="left" valign="top" scope="column"><p>m-probability</p></th>
<th align="left" valign="top" scope="column"><p>u-probability</p></th>
<th align="left" valign="top" scope="column"><p>Max Agreement Weight</p></th>
<th align="left" valign="top" scope="column"><p>Min Disagreement Weight</p></th>
</tr>
<tr><td align="left" valign="top" scope="row"><p>First Name</p></td>
<td align="left" valign="top" scope="row"><p>.996</p></td>
<td align="left" valign="top" scope="row"><p>.004</p></td>
<td align="left" valign="top" scope="row"><p>7.96</p></td>
<td align="left" valign="top" scope="row"><p>-7.96</p></td>
</tr>
<tr><td align="left" valign="top" scope="row"><p>Last Name</p></td>
<td align="left" valign="top" scope="row"><p>.996</p></td>
<td align="left" valign="top" scope="row"><p>.004</p></td>
<td align="left" valign="top" scope="row"><p>7.96</p></td>
<td align="left" valign="top" scope="row"><p>-7.96</p></td>
</tr>
<tr><td align="left" valign="top" scope="row"><p>Date
of Birth</p></td>
<td align="left" valign="top" scope="row"><p>.97</p></td>
<td align="left" valign="top" scope="row"><p>.007</p></td>
<td align="left" valign="top" scope="row"><p>7.11</p></td>
<td align="left" valign="top" scope="row"><p>-5.04</p></td>
</tr>
<tr><td align="left" valign="top" scope="row"><p>Gender</p></td>
<td align="left" valign="top" scope="row"><p>.97</p></td>
<td align="left" valign="top" scope="row"><p>.03</p></td>
<td align="left" valign="top" scope="row"><p>5.01</p></td>
<td align="left" valign="top" scope="row"><p>-5.01</p></td>
</tr>
<tr><td align="left" valign="top" scope="row"><p>SSN</p></td>
<td align="left" valign="top" scope="row"><p>.999</p></td>
<td align="left" valign="top" scope="row"><p>.001</p></td>
<td align="left" valign="top" scope="row"><p>9.96</p></td>
<td align="left" valign="top" scope="row"><p>-9.96</p></td>
</tr>
<tr><td align="left" valign="top" scope="row"><p><b>Maximum Match Weight</b></p></td>
<td align="left" valign="top" scope="row"></td>
<td align="left" valign="top" scope="row"></td>
<td align="left" valign="top" scope="row"><p>38</p></td>
<td align="left" valign="top" scope="row"></td>
</tr>
<tr><td align="left" valign="top" scope="row"><p><b>Minimum Match Weight</b></p></td>
<td align="left" valign="top" scope="row"></td>
<td align="left" valign="top" scope="row"></td>
<td align="left" valign="top" scope="row"></td>
<td align="left" valign="top" scope="row"><p>-35.93</p></td>
</tr>
</table></div>
</div>


<div class="indent"><a name="gfjsy"></a><h3>Comparison Functions</h3>
<p>The match configuration file defines several match types for different types of fields.
You can either modify existing rows in this file or create new rows
that define custom matching logic. To determine which comparison functions to use, review
the information provided in <a href="ref_sme-comparators_c.html">Mural Match Engine Comparison Functions</a>. Choose the comparison functions that best suit how
you want the match fields to be processed.</p></div>
</div>


<div class="indent"><a name="ref_sme-weight_c"></a><h3>Determining the Weight Thresholds</h3>
<p>Weight thresholds tell the master index application how to process incoming records based
on the matching probability weights generated by the Mural Match Engine. Two parameters
in master.xml provide the master index application with the information needed to determine
if records should be flagged as potential duplicates, if records should be automatically
matched, or if a record is not a potential match to any existing
records.</p>
<ul><li><p><b>Match Threshold</b> - Specifies the weight at which two profiles are assumed to represent the same person and are automatically matched (this depends on the setting for the OneExactMatch parameter).</p></li>
<li><p><b>Duplicate Threshold</b> - Specifies the minimum weight at which two profiles are considered potential duplicates of one another. The matching threshold indicates the maximum weight for potential duplicates.</p></li></ul>
<p><a href="#gfjsp">Figure&nbsp;1</a> illustrates the match and duplicate thresholds in comparison to total composite match
weights.</p><a name="gfjsp"></a><h6>Figure&nbsp;1 Weight Thresholds</h6><img src="figures/thresholds.gif" alt="Figure illustrates the matching and duplicate thresholds in relation to the minimum and maximum weights." width="534" height="454"></img>

<div class="indent"><a name="gfjsw"></a><h3>Specifying the Weight Thresholds</h3>
<p>There are many techniques for determining the initial settings for the match and
duplicate thresholds. This section discusses two methods. You can also use the Data
Profiler and Bulk Matcher to determine these thresholds. For more information, see
<a href="https://open-dm-dq.dev.java.net/docs/analysis/landingpage.html">Analyzing and Cleansing Data for a Sun Master Index Analyzing and Cleansing Data for a Master Index</a> and <a href="https://open-dm-mi.dev.java.net/docs/load/lidseveispv.html">Loading the Initial Data Set for a Master Index Loading the Initial Data Set for a Master Index</a>.</p><p>The first method, the weight distribution method, is based on the calculation of
the error rates of false matches and false non-matches from analyzing the distribution
spectrum of all the weighted pairs. This is the standard method, and is
illustrated in <a href="#gfjsz">Figure&nbsp;2</a>. The second method, the percentage method relies on measuring the
total maximum and minimum weights of all the matched fields and then specifying
a certain percentage of these values as the initial thresholds.</p><p>The weight distribution method is more thorough and powerful but requires analyzing a
large amount of data (match weights) to be statistically reliable. It does not
apply well in cases where one candidate record is matched against very few
reference records. The percentage method, though simple, is very reliable and precise when
dealing with such situations. For both methods, defining the match threshold and the
duplicate threshold is an iterative process.</p>

<div class="indent"><a name="gfjud"></a><h3>Weight Distribution Method</h3>
<p>Each record pair in the master index application can be classified into three
categories: matches, non-matches, and potential matches. In general, the distribution of records is
similar to the graph shown in <a href="#gfjsz">Figure&nbsp;2</a>. Your goal is to make
sure that very few records fall into the False Matches region (if any),
and that as few as possible fall into the False Non-matches region. You
can see how modifying the thresholds changes this distribution. Balance this against the
number of records falling within the Manual Review section, as these will each
need to be reviewed, researched, and resolved individually.</p><a name="gfjsz"></a><h6>Figure&nbsp;2 Weight Distribution Chart</h6><a href="figures/weightdist2.gif" target="_blank"><img src="figures/thumb-weightdist2.gif" alt="Figure shows a standard matching weight distribution curve."><br>Click to Expand</a></div>


<div class="indent"><a name="gfjth"></a><h3>Percentage Method</h3>
<p>Using this method, you set the initial thresholds as a percentage of the
maximum and minimum weights. Using the information provided under <a href="#gfjtl">Weight Ranges Using Agreement Weights</a>or <a href="#gfjty">Weight Ranges Using Probabilities</a>, determine
the maximum and minimum values that can be generated for composite match weights. For
the initial run, the match threshold is set intentionally high to catch only
the most probable matches. The duplicate threshold is set intentionally low to catch
a large set of possible matches.</p><p>Set the match threshold at 70% of the maximum composite weight starting from
zero as the neutral value. Using the weight range samples in <a href="#gfjtd">Table&nbsp;2</a>,
this would be 70% of 38, or 26.6. Set the duplicate threshold near
the neutral value (that is, the value in the center of the maximum
and minimum weight range). The value could be set between 10% of the
maximum weight and 10% of the minimum weight. Using the samples above, this
would be between 3.8 (10% of 38) and -3.6 (10% of -36).</p></div>
</div>


<div class="indent"><a name="gfjta"></a><h3>Fine-tuning the Thresholds</h3>
<p>Achieving the correct thresholds for your implementation is an iterative process. First, using
the initial thresholds described earlier, process the data extracts into the master index
database. Then analyze the resulting assumed match and potential duplicates, paying close attention
to the assumed match records with matching weights close to the match threshold,
to potential duplicate records close to either threshold, and to non-matches near the
duplicate threshold.</p><p>If you find that most or all of the assumed matches at
the low end of the match range are not actually duplicate records, raise
the match threshold accordingly. If, on the other hand, you find several potential
duplicates at the high end of the duplicate range that are actual matches,
decrease the match threshold accordingly. If you find that most or all of
the potential duplicate records in the low end of the duplicate range should
not be considered duplicate matches, consider raising the duplicate threshold. Conversely, if you find
several non-matches with weight near the duplicate threshold that should be considered potential
duplicates, lower the duplicate threshold.</p><p>Repeat the process of loading and analyzing data and adjusting the thresholds until
you are satisfied with the results.</p></div>
</div>

   </div>

   <div class="BottomPageControlPane">
      <table class="pagecontrol">
         <tr>
            <td class="pageleft">
               <a href="ref_sme-person_c.html">Previous</a>
            </td>
            <td class="pageright">

            </td>
         </tr>
      </table>

   </div>

</body>
</html>
