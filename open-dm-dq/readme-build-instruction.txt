How to build MDM:

Prerequisite:
1. Subversion
2. Maven
3. Ant
4. Oracle, MySql and SqlSever jdbc drivers (To run JUnit tests)
5. Test databases for Oracle, MySql and SqlSever

Steps:

C:\MDMInternal\MDMTrunk>svn checkout http://svn.stc.com/svn/open-dm-dq/branches/soarelease/trunk open-dm-dq --username parijatkar

C:\MDMInternal\MDMTrunk>svn checkout http://svn.stc.com/svn/open-dm-di/branches/soarelease/trunk open-dm-di --username parijatkar

C:\MDMInternal\MDMTrunk>svn checkout http://svn.stc.com/svn/open-dm-mi/branches/soarelease/trunk open-dm-mi --username parijatkar

C:\MDMInternal\MDMTrunk>cd open-dm-mi

C:\MDMInternal\MDMTrunk\open-dm-mi>edit env.bat

C:\MDMInternal\MDMTrunk\open-dm-mi>env.bat

C:\MDMInternal\MDMTrunk\open-dm-mi>mvn install -Dmaven.test.skip=true [skips JUnit]

C:\MDMInternal\MDMTrunk\open-dm-mi>cd ..\open-dm-di

C:\MDMInternal\MDMTrunk\open-dm-di>mvn install -Dmaven.test.skip=true [skips JUnit]

C:\MDMInternal\MDMTrunk\open-dm-di>cd ..\open-dm-dq

C:\MDMInternal\MDMTrunk\open-dm-dq>mvn install -Dmaven.test.skip=true [skips JUnit]

C:\MDMInternal\MDMTrunk\open-dm-dq>cd ..\open-dm-mi

C:\MDMInternal\MDMTrunk\open-dm-mi>cp build-mural-nbm.xml ..\

C:\MDMInternal\MDMTrunk\open-dm-mi>cd ..

C:\MDMInternal\MDMTrunk>ant -f build-mural-nbm.xml make-mural-nbm

Deliverable:

C:\MDMInternal\MDMTrunk\mural-nbm\mural.nbm


How to install this nbm file in the GlassfishESB or JavaCAPS R6 netbeans:

1. Start NetBeans.
2. From the NetBeans Tools menu, click Plugins.
3. Click the Downloaded tab and then click Add Plugins.
4. Navigate to the location where you saved mural.nbm, select the file, and then click Open.
5. Click Install.
6. On the Installer, click Next, select the license acceptance check box, and then click Install again.
7. If a validation warning window appears, click Continue.
8. When the installation is complete, click Finish and close the Plugins window.






