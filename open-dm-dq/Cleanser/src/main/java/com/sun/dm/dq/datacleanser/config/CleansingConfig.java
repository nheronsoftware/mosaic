/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html
 * or mural/license.txt. See the License for the specific language 
 * governing permissions and limitations under the License.  
 *
 * When distributing Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at mural/license.txt.
 * If applicable, add the following below the CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */


package com.sun.dm.dq.datacleanser.config;

/**
 *
 * @author 
 */
public class CleansingConfig {
    
    
    /* 
     * path to cleansing rules
     */
    private String rulesConfigURI;
    
   
    
    /*
     * identifies the source system uniquely
     * taken as a command-line parameter from the user
     * while sourcing from the plug-in
     */
    private String systemCode;
    
    /**
     * identifies the user which is a system field
     * written to the output good file consumed by loader
     */
    private String userCode;
    
    /**
     * identifies the user which is a system field
     * written to the output good file consumed by loader
     */
    private String localID;
    
    /**
     * identifies the user which is a system field
     * written to the output good file consumed by loader
     */
    private String updateDate;
    
    public CleansingConfig() {
        rulesConfigURI="";
        systemCode = "";
        localID = "";
        userCode = "";
        updateDate = "";
    }

    public String getLocalID() {
        return localID;
    }

    public void setLocalID(String localID) {
        this.localID = localID;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }
    
    public void setRulesConfigURI(String rulesConfigURI) {
        this.rulesConfigURI = rulesConfigURI;
    };
    
    public String getRulesConfigURI() {
        return this.rulesConfigURI;
    }
   
    
    public void setSystemCode(String systemCode) {
        this.systemCode = systemCode;
    }
    
    public String getSystemCode() {
        return this.systemCode;
    }
    
    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }
    
    public String getUserCode() {
        return this.userCode;
    }
}
