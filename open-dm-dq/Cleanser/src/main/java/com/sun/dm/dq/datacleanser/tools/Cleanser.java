/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * Cleanser.java
 * 
 * Created on Aug 29, 2007, 6:27:57 PM
 * 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sun.dm.dq.datacleanser.tools;

import com.sun.mdm.index.objects.ObjectNode;
import com.sun.mdm.index.objects.SystemObject;
import com.sun.mdm.index.objects.exception.ObjectException;
import com.sun.mdm.index.matching.StandardizationException;

import com.sun.dm.dq.datacleanser.config.CleansingConfig;
import com.sun.dm.dq.datacleanser.util.ErrorSerializer;
import com.sun.mdm.index.dataobject.objectdef.DataObjectAdapter;
import com.sun.dm.dq.datacleanser.util.StandardizationAdapter;
import com.sun.dm.dq.datacleanser.util.SystemFieldGenerator;
import com.sun.dm.dq.process.CleansingProcess;
import com.sun.dm.dq.process.DataAnalysisProcess;
import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import com.sun.dm.dq.rules.output.RuleErrorObject;
import com.sun.dm.dq.rules.output.RulesOutput;

import java.io.File;
import java.util.Iterator;

import com.sun.mdm.index.dataobject.DataObject;
import com.sun.mdm.index.dataobject.InvalidRecordFormat;
import com.sun.mdm.index.dataobject.ChildType;
import com.sun.mdm.index.dataobject.objectdef.ObjectDefinition;

import com.sun.dm.dimi.datawriter.DOWriter;
import com.sun.dm.dimi.datareader.DataSourceReaderFactory;
import com.sun.dm.dimi.datareader.GlobalDataObjectReader;
import com.sun.dm.dimi.datawriter.DataObjectWriterFactory;

import com.sun.dm.dimi.util.PluginConstants;
import com.sun.dm.dq.rules.data.DataReader.CustomDataReader;
import com.sun.dm.dq.rules.data.DataReader.DataObjectDataReader;

import com.sun.dm.dq.util.Localizer;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.logging.LogManager;
import net.java.hulp.i18n.Logger;

/**
 * This class represents the main driver class of the Data Cleanser
 * It does the following: 
 *      a. Sources data objects from staging database, Good file sources and Bad file Sources     
 *      b. Apply Cleansing rules on the data object
 *      c. if( data object passes all the business rules ) {
 *              Standardize, normalize, phoneticize 
 *              write to Good file
 *          } else {
 *              write to Reject file ( User will correct the errors and feed the file back ) 
 *          }
 * 
 */
public class Cleanser {

    /**
     * Cleansing configuration object which 
     * defines the source, target good file & reject file, 
     * path to eView config files, system code and usercode
     * Note that systemcode and usercode will be deprecated soon once
     * Systemfield injection is done to ObjectDefinition
     */
    private CleansingConfig configuration;
    /**
     * Abstract reader interface to source DataObjects from Staging database, 
     * Good file sources and bad file sources. 
     */
    private GlobalDataObjectReader dsReader;
    private CustomDataReader customDSReader;
    private DataObjectDataReader dataObjectDBReader;
    /**
     * DataObject writer for good file which is ready to be consumed by Bulk Loader. 
     */
    private DOWriter goodfileWriter;
    /**
     * DataObject writer which will write a reject file with information about 
     * reason for rejections etc. 
     */
    private String datasourceLoc;
    /**
     * type of datasource viz. jdbc, goodfile or bad file
     */
    private int dataSourceType;
    /**
     * DataObject writer which will write a reject file with information about 
     * reason for rejections etc. 
     */
    private DOWriter rejectfileWriter;
    /**
     * Rules processor instance
     */
    private CleansingProcess cleanser;
    /**
     * instance of standardization adapter
     */
    private StandardizationAdapter standardizer;
    /**
     * Master-Index Object Definition File Name 
     */
    private static final String EVIEW_CONFIG_FILE_NAME = "object.xml";
    /**
     * Logger instance
     */
    private static transient final Logger mLogger = Logger.getLogger(Cleanser.class);
    /**
     * i18n utility 
     */
    private transient static final Localizer mLoc = Localizer.get();
    private static long goodCount = 0;
    private static long badCount = 0;
    private static long recCount = 0;
    private boolean isStandardize;

    /**
     * Constructor which takes a Cleansing configuration object
     * @param configObject
     * @throws java.io.IOException
     */
    public Cleanser(CleansingConfig configObject) throws IOException {
        this.configuration = configObject;
        init(configObject);
        if (standardizer == null && isStandardize == true) {
            mLogger.info(mLoc.x("CLN088: Unable to create Standerdizer instance, Standerdization will not be done ..."));
        } else if (isStandardize == false) {
            mLogger.info(mLoc.x("CLN077: Standardizer is disabled !!!"));
        }
    }

    /** 
     * init method which creates readers, writers and standardizer instance
     * and gets rules processor instance
     * @param configObject
     * @throws java.io.IOException
     */
    private void init(CleansingConfig configObject) {
        mLogger.info(mLoc.x("CLN001: Cleanser process Started"));
        try {

            if (mLogger.isFine()) {
                mLogger.fine("initialized Readers and writers successfully");
            }

            DataAnalysisProcess dp = new DataAnalysisProcess(configObject.getRulesConfigURI());

            cleanser = dp.getCleansingProcess();
            if (cleanser == null) {
                mLogger.infoNoloc("Unable to load Cleanser Process !!!");
                System.exit(1);
            }
            if (mLogger.isFine()) {
                mLogger.fine("got the rules processor instance successfully");
            }

            customDSReader = cleanser.getDbReader();
            dataObjectDBReader = cleanser.getDataObjectdbReader();
            dataSourceType = PluginConstants.JDBC_DATASOURCE;
            isStandardize = cleanser.isStandardizer();
            long count = cleanser.getCleanserStartCounter();
            SystemFieldGenerator.setSequenceCounter(count - 1);
            if (customDSReader == null || dataObjectDBReader == null) {
                DataSourceReaderFactory.setEViewConfigFilePath(cleanser.getObjectDefFilePath());
                DataObjectAdapter.init(DataSourceReaderFactory.getEViewConfigFilePath());
                datasourceLoc = cleanser.getDbConnection();

                mLogger.infoNoloc("dataSourceLocation : " + datasourceLoc);
                dsReader = DataSourceReaderFactory.getDataSourceReader(datasourceLoc, true);
                dataSourceType = dsReader.getDataSourceType();
            }
            goodfileWriter = DataObjectWriterFactory.getNewDataObjectWriter(new File(cleanser.getGoodFilePath()), true);
            rejectfileWriter = DataObjectWriterFactory.getNewDataObjectWriter(new File(cleanser.getBadFilePath()), false);

            if (isStandardize == true) {
                standardizer = new StandardizationAdapter();
                standardizer.initialize();
            }
            if (mLogger.isFine()) {
                mLogger.fine("standardizer initialized successfully");
            }

            mLogger.info(mLoc.x("CLN002: Cleanser init Completed Successfully"));
        } catch (StandardizationException stdex) {
            mLogger.severe(mLoc.x("CLN003: Cleanser failed at initialization of Standardizer"), stdex);
        } catch (IOException e) {
            mLogger.severe(mLoc.x("CLN004: Cleanser init failed"), e);
        } catch (InstantiationException e) {
            mLogger.severe(mLoc.x("CLN005: Standardization Adapter Instantiation failure"), e);
        } catch (ProcessXMLDataException e) {
            mLogger.severe(mLoc.x("CLN006: Error in parsing cleansing configuration file"), e);
        } catch (ClassNotFoundException e) {
            mLogger.severe(mLoc.x("CLN007: Class Loading Error in Standardizer"), e);
        } catch (IllegalAccessException e) {
            mLogger.severe(mLoc.x("CLN008: Access Exception in Standardizer"), e);
        } catch (Exception e) {
            mLogger.severe(mLoc.x("CLN005: Standardization Adapter Instantiation failure"), e);
        }
    }

    /**
     * This method is the interface to the DataIntegrator plug-in for Master-Index for 
     * sourcing data from Staging database. 
     * @return DataObject
     */
    private DataObject readNext() {
        DataObject dataObject = null;
        if (this.customDSReader != null) {
            dataObject = customDSReader.readNext();
        } else if (this.dataObjectDBReader != null) {
            dataObject = dataObjectDBReader.readNext();
        } else {
            try {
                dataObject = (DataObject) this.dsReader.readDataObject();
                if (mLogger.isFine()) {
                    mLogger.fine("got the data object from plug-in successfully");
                }
            } catch (InvalidRecordFormat e) {
                mLogger.severe(mLoc.x("CLN009: Invalid Record Format"), e);
                appendToRejectBuffer(dataObject, null, mLoc.x("CLN009: Invalid Record Format") + e.getMessage());
            }
        }

        return dataObject;
    }

    /**
     * Utility method to Standardize, Normalize and Phoneticize Data Objects
     * @param dataObject before Standardization
     * @return dataObject after Standardization
     * @throws com.sun.mdm.index.objects.exception.ObjectException
     * @throws java.lang.Exception
     */
    private DataObject standardize(DataObject dataObject, ArrayList fields) {
        DataObject tmpDataObject = dataObject;
        DataObject retDataObject = null;

        try {
            removeSystemFields(dataObject);
            ObjectNode node = DataObjectAdapter.toObjectNode(dataObject);

            ObjectDefinition objDef = DataObjectAdapter.getObjectDefinition();
            if (mLogger.isFine()) {
                mLogger.fine("Calling standardize on data Object");
                mLogger.fine("ObjectNode from DataObject::" + ((node != null) ? node.toString() : ""));
                mLogger.fine("ObjectDefinition loaded successfully");
            }
            SystemObject sysObject = new SystemObject();
            sysObject.setChildType(objDef.getName());
            sysObject.setObject(node);

            SystemObject standardizedObj = standardizer.standardize(sysObject);
            ObjectNode standardizedNode = standardizedObj.getObject();
            /*if (mLogger.isDebugEnabled()) {
            ((standardizedNode != null) ? standardizedNode.toString() : ""))  ;
            } mLogger.debug("standardized ObjectNode::" +*/
            retDataObject = DataObjectAdapter.fromObjectNode(standardizedNode);
            appendSystemFields(fields, retDataObject);
        /*if (mLogger.isDebugEnabled()) {
        mLogger.debug("retDataObject::" + retDataObject);
        }*/
        } catch (ObjectException e) {
            mLogger.severe(mLoc.x("CLN010:: Failure trying to construct SystemObject"), e);
            //appendSystemFields(fields, dataObject);
            appendToRejectBuffer(tmpDataObject, null, mLoc.x("CLN066:: Failure in Standarization Call") + e.getMessage());
        } catch (StandardizationException e) {
            mLogger.severe(mLoc.x("CLN011:: Failure in Standardization Engine"), e);
            //appendSystemFields(fields, dataObject);
            appendToRejectBuffer(tmpDataObject, null, mLoc.x("CLN066:: Failure in Standarization Call") + e.getMessage());
        } catch (InstantiationException e) {
            mLogger.severe(mLoc.x("CLN012:: Failure in Standardization Engine"), e);
            //appendSystemFields(fields, dataObject);
            appendToRejectBuffer(tmpDataObject, null, mLoc.x("CLN066:: Failure in Standarization Call") + e.getMessage());
        } catch (ParseException e) {
            mLogger.severe(mLoc.x("CLN017:: Failure in Standardization Engine"), e);
            //appendSystemFields(fields, dataObject);
            appendToRejectBuffer(tmpDataObject, null, mLoc.x("CLN066:: Failure in Standarization Call") + e.getMessage());
        } catch (Exception e) {
            mLogger.severe(mLoc.x("CLN018:: Failure in Standardization Engine"), e);
            appendSystemFields(fields, dataObject);
            appendToRejectBuffer(tmpDataObject, null, mLoc.x("CLN066:: Failure in Standarization Call") + e.getMessage());
        }
        return retDataObject;

    }

    /**
     * This method is used to append DataObject which has successfully 
     * passed business rules and also have got successfully standardized
     * @param dataObject
     */
    private void appendToGoodBuffer(DataObject dataObject) {
        if (mLogger.isFine() && dataObject != null) {
            mLogger.fine("retDataObject::" + dataObject.toString());
        }
        if (dataSourceType == PluginConstants.JDBC_DATASOURCE) {
            SystemFieldGenerator.appendSystemFields(null, null, null, null, dataObject);
        } else /*if (dataSourceType == PluginConstants.RAW_FILE_DATASOURCE)*/ {
            SystemFieldGenerator.appendSystemFields(null, null, null, null, dataObject);
        }

        goodfileWriter.write(dataObject);
        goodCount++;
    }

    /**
     * This method finds the last field of the last child instance 
     * and adds the error record to that. Incases where failures are 
     * not due to rules engine, the exception message is written to the reject file
     * @param dataObject
     * @param errors list of rule errors
     * @param errorReason reason code for the error
     */
    public void appendToRejectBuffer(DataObject dataObject, ArrayList<ArrayList<RuleErrorObject>> errors, String errorReason) {
        mLogger.fine("appending to reject buffer");
        DataObject lastChildTypeInstance = null;
        ArrayList<ChildType> childTypes = dataObject.getChildTypes();
        ChildType lastType = null;
        for (Iterator iter = childTypes.iterator(); iter.hasNext();) {
            lastType = (ChildType) iter.next();
        }

        if (lastType != null) {
            ArrayList<DataObject> children = lastType.getChildren();
            for (Iterator iter = children.iterator(); iter.hasNext();) {
                lastChildTypeInstance = (DataObject) iter.next();
            }
        }

        if (lastChildTypeInstance != null) {
            if (errors != null) {
                lastChildTypeInstance.addFieldValue(ErrorSerializer.serializeRuleErrors(errors));
            } else {
                lastChildTypeInstance.addFieldValue(errorReason);
            }

        } else if (errors != null) {
            dataObject.addFieldValue(ErrorSerializer.serializeRuleErrors(errors));
        } else {
            dataObject.addFieldValue(errorReason);
        }
        /*if (dataSourceType == PluginConstants.JDBC_DATASOURCE) {
        SystemFieldGenerator.appendSystemFields(null, null, null, null, dataObject);
        }*/
        rejectfileWriter.write(dataObject);
        badCount++;
    }

    /**
     * Flush the IO Buffers
     */
    private void flushBuffer() {
        try {
            if (goodfileWriter != null) {
                goodfileWriter.flush();
            }
            if (rejectfileWriter != null) {
                rejectfileWriter.flush();
            }
        } catch (Exception e) {
            mLogger.severe(mLoc.x("CLN:014 Failure in Flushing Cleanser buffers"), e);
        }
    }

    /*
     * Removes system fields from dataobject before standardization
     */
    private static void removeSystemFields(DataObject dataObject) {
        for (int i = 0; i < 5; i++) {
            dataObject.remove(0);
        }
    }

    /**
     * append System Fields to dataobject
     * @param fields ID, systemcode, LID, updateDate, usercode
     * @param dataObject
     */
    private static void appendSystemFields(ArrayList fields, DataObject dataObject) {
        for (int i = 4; i >= 0; i--) {
            dataObject.add(0, (String) fields.get(i));
        }
    }

    /**
     * cleansing operation on dataobject
     * @param dataObject
     * @return RuleErrorObjects representing non-conformance of dataobject to rules
     */
    public ArrayList<ArrayList<RuleErrorObject>> cleanse(DataObject dataObject) {
        RulesOutput output = cleanser.executeRules(dataObject);
        if (output == null || output.GetErrorList() == null) {
            return null;
        }
        return output.GetErrorList();
    }

    public static void usage() {
        mLogger.info(mLoc.x("CLN014:: Usage: java com.sun.dm.dq.datacleanser"));
        mLogger.info(mLoc.x("CLN015:: rulesConfigURI SystemCode UserCode"));
    }

    /**
     * This method executes the cleansing process
     * Extracts data objects from the plugin and 
     * passes it through the rules processor and based on the 
     * results, appends to either good buffer or reject buffer
     */
    public void execute() {
        //cleanser.init();
        DataObject object = this.readNext();
        DataObject tmpobject = null;
        if (object != null && mLogger.isFine()) {
            mLogger.fine("printing dataobject" + object.toString());
        }

        while (object != null) {
            ArrayList fields = (ArrayList) object.getFieldValues().clone();
            recCount++;
            mLogger.fine(fields.toString());
            mLogger.fine("printing dataobject after removing system fields" + object.toString());
            ArrayList<ArrayList<RuleErrorObject>> errors = this.cleanse(object);

            if (errors == null || errors.isEmpty()) {
                if (standardizer != null) {
                    tmpobject = this.standardize(object, fields);
                    if (tmpobject != null) {
                        object = tmpobject;
                        //appendSystemFields(fields, object);
                        this.appendToGoodBuffer(object);
                    }
                } else {

                    this.appendToGoodBuffer(object);
                }

            } else {
                mLogger.fine("this is a rejected record");
                //appendSystemFields(fields, object);
                this.appendToRejectBuffer(object, errors, null);
            }
            if (this.dsReader != null) {
                this.dsReader.submitObjectForFinalization(object);
            }
            object = this.readNext();
        }
        this.flushBuffer();

    }

    private static void confiugreLogger() {


        FileInputStream ins = null;
        try {
            LogManager logManager;
            String config = "config/logger.properties";
            File f = new File("./logs");

            if (f.exists() && f.isDirectory()) {
                mLogger.info(mLoc.x("logger dir exist"));
            } else {
                mLogger.info(mLoc.x("creating new logger dir"));
                f.mkdir();
            }

            logManager = LogManager.getLogManager();
            ins = new FileInputStream(config);
            logManager.readConfiguration(ins);
        } catch (IOException ex) {
            mLogger.infoNoloc(ex.getLocalizedMessage());
        } catch (SecurityException ex) {
            mLogger.infoNoloc(ex.getLocalizedMessage());
        } finally {
            try {
                if (ins != null) {
                    ins.close();
                }
            } catch (IOException ex) {
                mLogger.infoNoloc(ex.getLocalizedMessage());

            }
        }

    }

    public static void main(String[] args) throws Exception {
        confiugreLogger();
        int noOfInputs = args.length;
        CleansingConfig configObject = new CleansingConfig();
        Cleanser cleanser = null;
        if (noOfInputs < 1) {
            usage();
            System.exit(0);


        } else {
            usage();


            if (noOfInputs >= 1) {

                configObject.setRulesConfigURI(args[0]);
            }
            /* if (noOfInputs >= 2) {
            if (args[1].equalsIgnoreCase("-1") == false) {
            configObject.setSystemCode(args[1]);
            }
            }
            if (noOfInputs >= 3) {
            if (args[2].equalsIgnoreCase("-1") == false) {
            configObject.setLocalID(args[2]);
            }
            }
            if (noOfInputs >= 4) {
            if (args[3].equalsIgnoreCase("-1") == false) {
            configObject.setUpdateDate(args[3]);
            }
            }
            if (noOfInputs >= 5) {
            if (args[4].equalsIgnoreCase("-1") == false) {
            configObject.setUserCode(args[4]);
            }
            }*/

            /*if (noOfInputs >= 2) {
            mLogger.info("rulesConfigURI :: " + args[0] + ",SystemCode :: " + args[1] + ",UserCode :: " + args[2]);
            //String systemCode, String localId, Date updateDate, String userCode
            configObject.setSystemCode(args[1]);
            configObject.setUserCode("");*/
            cleanser = new Cleanser(configObject);
            mLogger.info(mLoc.x("CLN016:: Cleanser process created "));

            if (cleanser != null) {
                cleanser.execute();
                mLogger.info(mLoc.x("CLN017:: Cleanser process execution finished "));
                mLogger.info(mLoc.x("CLN018:: Total records processed = {0}  Bad records = {1}  Good Records = {2}", recCount, badCount, goodCount));
                recCount = 0;
                badCount = 0;
                goodCount = 0;
                System.exit(0);
            }

        }

    }
}

