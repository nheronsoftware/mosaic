/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html
 * or mural/license.txt. See the License for the specific language 
 * governing permissions and limitations under the License.  
 *
 * When distributing Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at mural/license.txt.
 * If applicable, add the following below the CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.dm.dq.datacleanser.util;

import com.sun.dm.dq.util.Localizer;
import com.sun.mdm.index.objects.SystemObject;
import com.sun.mdm.index.matching.StandardizationException;
import com.sun.mdm.index.matching.StandardizerAPIHelper;
import com.sun.mdm.index.matching.StandardizerAPI;
import com.sun.mdm.index.matching.StandardizerEngineConfiguration;
import com.sun.mdm.index.configurator.impl.standardization.SystemObjectStandardization;
import com.sun.mdm.index.configurator.impl.standardization.StandardizationConfiguration;
import com.sun.mdm.index.configurator.ConfigurationService;
import com.sun.mdm.index.objects.exception.ObjectException;
import com.sun.mdm.index.objects.exception.NotNullableFieldException;
import com.sun.mdm.index.util.LogUtil;
import com.sun.mdm.index.util.Logger;

/**
 * Wrapper EJB for the standardization engine adapter. Loads and forwards requests to the
 * adapter interface implementation configured.
 * 
 * @author Refactored the code to remove requirement to have an EJB container
 * $Revision: 1.1 $
 */
public class StandardizationAdapter {

    private StandardizerAPI standardizerAPIImpl;
    private StandardizerEngineConfiguration standardizerEngineConfig;
    private StandardizationConfiguration standardizationConfig;
    private final Logger mLogger = LogUtil.getLogger(this);
    private final Localizer sLoc = Localizer.get();

    /**
     * No argument constructor required by container.
     */
    public StandardizationAdapter() throws Exception {
        try {
            initialize();
        } catch (Throwable ex) {
            throw new Exception(sLoc.x("STD001: Creation and initialization failed.").toString(), ex);
        }
    }

    /**
     * Load and initialize the configured standardizer API implementation
     * @throws StandardizationException Initializing the configured API implementation failed
     * @throws InstantiationException instantiating the configured API implementation
     * class failed
     * @throws ClassNotFoundException The class for the configured API implementation
     * class could not be found 
     * @throws IllegalAccessException The current security settings do not allow
     * loading and instantiating the class configured for API implementation
     */
    public void initialize()
            throws StandardizationException, InstantiationException, ClassNotFoundException, IllegalAccessException {
        mLogger.debug("initialize()");
        try {
            // use StandardizerAPIHelper as factory 
            mLogger.debug("Get an instance of StandardizerAPI");
            standardizerAPIImpl = new StandardizerAPIHelper().getStandardizerAPIImpl();
            if (standardizerAPIImpl == null) {
                throw new StandardizationException(sLoc.x("STD003: No StandardizerAPI implementation configured.").toString());
            }
            mLogger.debug("Get an instance of StandardizerEngineConfig");
            standardizerEngineConfig = new StandardizerAPIHelper().getStandardizerEngineConfigImpl();
            if (standardizerEngineConfig == null) {
                mLogger.debug("No StandardizerEngineConfig implementation configured.");
            }

            mLogger.debug("Initialize StandardizerAPI");
            standardizerAPIImpl.initialize(standardizerEngineConfig);
        } catch (StandardizationException ex) {
            mLogger.error(ex);
            throw new StandardizationException(sLoc.x("STD004: Initializing the standardization engine failed.").toString(), ex);
        } catch (InstantiationException ex) {
            throw ex;
        } catch (ClassNotFoundException ex) {
            mLogger.error(ex);
            throw new ClassNotFoundException(sLoc.x("STD005: Loading the user API implementation class failed.").toString(), ex);
        } catch (IllegalAccessException ex) {
            mLogger.error(ex);
            throw new IllegalAccessException(sLoc.x("STD006: Accessing the user API implementation class failed.").toString());
        } catch (RuntimeException ex) {
            mLogger.error(ex);
            throw new RuntimeException(sLoc.x("STD007: Initialize failed.").toString(), ex);
        } catch (LinkageError ex) {
            mLogger.error(ex);
            throw new LinkageError(sLoc.x("STD008: Failed to load a native library.").toString());
        }
    }

    /**
     * cleanup of standardizer API
     * @throws exception
     */
    public void cleanUp() throws Exception {
        mLogger.debug("ejbRemove()");
        try {
            if (standardizerAPIImpl != null) {
                standardizerAPIImpl.shutdown();
            }
        } catch (StandardizationException ex) {
            throw new Exception(sLoc.x("STD009: Shutting down the standardization engine failed").toString(), ex);
        }
    }

    /**
     * Standardize a SystemObject
     * @param objToStandardize the SystemObject to standardize
     * @throws StandardizationException the standardization process failed
     * @throws ObjectException accessing/manipulating the value objects failed
     * @throws InstantiationException instantiating value objects failed
     * @return the standardized SystemObject
     */
    public SystemObject standardize(SystemObject objToStandardize)
            throws StandardizationException, ObjectException, InstantiationException {
        if (objToStandardize == null) {
            mLogger.debug("Standardize was called on a null object, returning it unchanged.");
        } else if (objToStandardize.getObject() == null) {
            mLogger.debug("Standardize was called on a SystemObject containing a null object, returning it unchanged.");
        } else {
            String objType = objToStandardize.getObject().pGetType();
            if (objType != null) {
                SystemObjectStandardization metaData = getStandardizationMetaData(objType);
                if (metaData != null) {
                    try {
                        standardizerAPIImpl.standardize(objToStandardize, metaData);
                    } catch (NotNullableFieldException e) {
                        e.printStackTrace();
                    }
                } else {
                    throw new StandardizationException(sLoc.x("STD010: Could not retrieve the standardization configuration for " + "object type: {0} . The configuration service returned null.", objType).toString());
                }
            } else {
                throw new ObjectException(sLoc.x("STD011: The object to be standardized does not have a type attribute set." + "The standardizer is unable to retrieve standardardization configuration without it.").toString());
            }
        }

        return objToStandardize;
    }

    /**
     * Retrieve the Configuration object that allows access to the standardization
     * metadata for SystemObjects
     * @InstantiationException instantiating the classes needed to access the 
     * configuration failed
     * @return standardization configuration object
     */
    StandardizationConfiguration getStandardizationConfig() throws java.lang.InstantiationException {
        if (standardizationConfig == null) {
            ConfigurationService cfgFactory = ConfigurationService.getInstance();
            standardizationConfig = (StandardizationConfiguration) cfgFactory.getConfiguration(
                    StandardizationConfiguration.STANDARDIZATION);
        }
        return standardizationConfig;
    }

    /**
     * Retrieve the metadata configured for a specific SystemObject
     * @sysObjName the name of the SystemObject to retrieve the configuration for
     * @InstantiationException instantiating the classes needed to access the 
     * configuration failed
     */
    SystemObjectStandardization getStandardizationMetaData(String sysObjName) throws java.lang.InstantiationException {
        SystemObjectStandardization metaData = getStandardizationConfig().getSystemObjectStandardization(sysObjName);
        return metaData;
    }
}



