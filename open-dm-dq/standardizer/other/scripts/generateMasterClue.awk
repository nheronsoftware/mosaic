BEGIN {
    FS = "\t"
    previousId = ""

    inputType[""] = "UNSPECIFIED"
    inputType["1P"] = "UNKNOWN_1P"
    inputType["A1"] = "ALPHA_ONE"
    inputType["A2"] = "ALPHA_TWO"
    inputType["A3"] = "UNKNOWN_A3"
    inputType["AM"] = "AMPERSAND"
    inputType["AN"] = "ALPHA_NUM"
    inputType["AU"] = "GENERIC_WORD"
    inputType["B*"] = "UNKNOWN_B_STAR"
    inputType["B+"] = "UNKNOWN_B_PLUS"
    inputType["BI"] = "UNKNOWN_BI"
    inputType["BN"] = "UNKNOWN_BN"
    inputType["BP"] = "BUILDING_PROPERTY"
    inputType["BS"] = "UNKNOWN_BS"
    inputType["BT"] = "UNKNOWN_BT"
    inputType["BU"] = "BUILDING_UNIT"
    inputType["BX"] = "POST_OFFICE_BOX"
    inputType["CN"] = "UNKNOWN_CN"
    inputType["D1"] = "DIGIT"
    inputType["DA"] = "LEADING_DASH"
    inputType["DB"] = "UNKNOWN_DB"
    inputType["DM"] = "UNKNOWN_DM"
    inputType["DR"] = "STREET_DIRECTION"
    inputType["EI"] = "EXTRA_INFORMATION"
    inputType["EN"] = "UNKNOWN_EN"
    inputType["EX"] = "EXTENSION"
    inputType["FC"] = "NUMERIC_FRACTION"
    inputType["H*"] = "UNKNOWN_H_STAR"
    inputType["H+"] = "UNKNOWN_H_PLUS"
    inputType["HN"] = "UNKNOWN_HN"
    inputType["HR"] = "HIGHWAY_ROUTE"
    inputType["HS"] = "UNKNOWN_HS"
    inputType["MP"] = "MILE_POST"
    inputType["N*"] = "UNKNOWN_N_STAR"
    inputType["N+"] = "UNKNOWN_N_PLUS"
    inputType["NA"] = "UNKNOWN_NA"
    inputType["NB"] = "UNKNOWN_NB"
    inputType["NL"] = "COMMON_WORD"
    inputType["NU"] = "NUMERIC_VALUE"
    inputType["OT"] = "ORDINAL_TYPE"
    inputType["P*"] = "UNKNOWN_P_STAR"
    inputType["P+"] = "UNKNOWN_P_PLUS"
    inputType["PD"] = "UNKNOWN_PD"
    inputType["PT"] = "PREFIX_TYPE"
    inputType["R*"] = "UNKNOWN_R_STAR"
    inputType["R+"] = "UNKNOWN_R_PLUS"
    inputType["RR"] = "RURAL_ROUTE"
    inputType["SA"] = "STATE_ABBREVIATION"
    inputType["SD"] = "UNKNOWN_SD"
    inputType["ST"] = "UNKNOWN_ST"
    inputType["T*"] = "UNKNOWN_T_STAR"
    inputType["T+"] = "UNKNOWN_T_PLUS"
    inputType["TB"] = "UNKNOWN_TB"
    inputType["TY"] = "STREET_TYPE"
    inputType["W*"] = "UNKNOWN_W_STAR"
    inputType["W+"] = "UNKNOWN_W_PLUS"
    inputType["WD"] = "STRUCTURE_DESCRIPTOR"
    inputType["WI"] = "STRUCTURE_IDENTIFIER"
    inputType["XN"] = "UNKNOWN_XN"

    print "<?xml version='1.0' encoding='ISO-8859-1'?>"
    print "<masterClues>"
} 

$1 != previousId {
    if (previousId != "")
        print "  </masterClueGroup>"

    previousId = $1
    print "  <masterClueGroup id=\"" $1 "\">"
}

{
    print "    <masterClue type=\"" inputType[$2] "\">"
    print "      <fullName>" $3 "</fullName>"
    print "      <standardAbbreviation>" $4 "</standardAbbreviation>"
    print "      <shortAbbreviation>" $5 "</shortAbbreviation>"
    print "      <uspsAbbreviation>" $6 "</uspsAbbreviation>"
    print "    </masterClue>"
}

END {
    if (previousId != "")
        print "  </masterClueGroup>"

    print "</masterClues>"
}
