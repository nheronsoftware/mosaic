#!/bin/bash
inputFileName="addressMasterClues"
outputFileName="masterClues"

inputDir="../../data/config/sbme/standardizer"
inputExtension=".dat"

outputDir="../../data/config/mdm/standardizer/address"
outputExtension=".xml"

generate() {
    awk -f extractMasterClue${1}.awk \
        $inputDir/${inputFileName}${2}${inputExtension} |\
    sort -t'	' |\
    awk -f generateMasterClue.awk \
        > $outputDir/${outputFileName}${2}${outputExtension}

    xmllint --format \
        $outputDir/${outputFileName}${2}${outputExtension} \
        -o $outputDir/${outputFileName}${2}${outputExtension}
}

generate EN AU
generate EN UK
generate EN US

generate FR FR
