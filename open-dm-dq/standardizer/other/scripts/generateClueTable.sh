#!/bin/bash
inputDir="../../data/config/sbme/standardizer"
inputFileName="addressClueAbbrev"
inputExtension=".dat"

outputDir="../../data/config/mdm/standardizer/address"
outputFileName="clues"
outputExtension=".xml"

generate() {
    awk -f generateClueTable${1}.awk \
        $inputDir/${inputFileName}${2}${inputExtension} \
        > $outputDir/${outputFileName}${2}${outputExtension}

    xmllint --format \
        $outputDir/${outputFileName}${2}${outputExtension} \
        -o $outputDir/${outputFileName}${2}${outputExtension}
}

generate EN AU
generate EN UK
generate EN US

generate FR FR
