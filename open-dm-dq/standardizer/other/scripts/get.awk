#/this.loadField\(address\.get[A-Z]/ {
#    propertyName = gensub(/.*address\.get([A-Za-z]+)\(\).*/, "\\1", "g")
#
#    propertySymbol = ""
#    for (i = 1; i <= length(propertyName); i++) {
#        letter = substr(propertyName, i, 1) 
#        if (i > 1 && letter >= "A" && letter <= "Z") {
#            propertySymbol = propertySymbol "_"
#        }
#        if (letter >= "a" && letter <= "z") {
#            letter = toupper(letter)
#        }
#        propertySymbol = propertySymbol letter
#    }
#
#    gsub(/address.get[A-Za-z]+\(\)/, "address, Address." propertySymbol)
#
#    print
#    next
#}

/address\.get[A-Z]/ {
    propertyName = gensub(/.*address\.get([A-Za-z]+)\(\).*/, "\\1", "g")

    propertySymbol = ""
    for (i = 1; i <= length(propertyName); i++) {
        letter = substr(propertyName, i, 1) 
        if (i > 1 && letter >= "A" && letter <= "Z") {
            propertySymbol = propertySymbol "_"
        }
        if (letter >= "a" && letter <= "z") {
            letter = toupper(letter)
        }
        propertySymbol = propertySymbol letter
    }

    gsub(/address.get[A-Za-z]+\(\)/, "address.get(Address." propertySymbol ")")

    print
    next
}

{ print }
