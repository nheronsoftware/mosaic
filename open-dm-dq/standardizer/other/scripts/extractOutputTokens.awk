BEGIN {
    idx = 0
}
/^ *$/ {next}
{
    if (getline == 0) {
        print "Unexpected EOF: " + FILENAME > "/dev/stderr"
        exit
    }

    count = split(substr($0, 1, 35), tokens)
    for (i = 1; i <= count; i++)
        outputTokens[tokens[i]]++
}
END {
    for (token in outputTokens)
        print token | "sort"
}
