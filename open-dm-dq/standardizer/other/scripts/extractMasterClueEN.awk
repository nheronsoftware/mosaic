BEGIN {
    OFS = "\t"
}

{
    id = extract(1, 4) + 0
    type = extract(61, 2)
    fullName = extract(5, 23)
    standardAbbreviation = extract(29, 13)
    shortAbbreviation = extract(43, 5)
    uspsAbbreviation = extract(49, 4)

    print id, \
          type, \
          fullName, \
          standardAbbreviation, \
          shortAbbreviation, \
          uspsAbbreviation
}

function extract(offset, len) {
    result = substr($0, offset, len)
    gsub(/^[	 ]*/, "", result)
    gsub(/[	 ]*$/, "", result)
    gsub(/\&/, "&amp;", result)
    return result
}
