BEGIN {
    FS = OFS = "|"
}
{
    if (length($3) > 0) {
        record[$3]++
    }
}
END {
    for (key in record) {
        print key, record[key] | "sort -t\\| -k2nr -o person-names-training.txt"
    }
}
