#!/bin/bash
tr -d '\015' < person-names.txt |\
awk '
    NF == 2 && $1 ~ /^[-A-Z]+$/ && ($2 ~ /^[A-Z]+$/ || $2 ~ /^O['"'"'`][A-Z]+$/ || $2 ~ /^[A-Z]+-[A-Z]+$/) {
        if (length($1) > 2) print $1
    }

    NF == 3 && $1 ~ /^[-A-Z]+$/ && $2 ~ /^[-A-Z]+$/ && ($3 ~ /^[A-Z]+$/ || $3 ~ /^O['"'"'`][A-Z]+$/ || $3 ~ /^[A-Z]+-[A-Z]+$/) {
        if (length($1) > 2) print $1
        if (length($2) > 2) print $2
    }
' |\
awk 'BEGIN{FS="-"} {for(i=1;i<=NF;i++)if(length($i)>2)print $i}' |\
sort -u - census-female-names.txt census-male-names.txt -o givenNames.txt

tr -d '\015' < person-names.txt |\
awk '
    NF == 2 && $1 ~ /^[-A-Z]+$/ && ($2 ~ /^[A-Z]+$/ || $2 ~ /^O['"'"'`][A-Z]+$/ || $2 ~ /^[A-Z]+-[A-Z]+$/) {
        if (length($2) > 2) print $2
    }

    NF == 3 && $1 ~ /^[-A-Z]+$/ && $2 ~ /^[-A-Z]+$/ && ($3 ~ /^[A-Z]+$/ || $3 ~ /^O['"'"'`][A-Z]+$/ || $3 ~ /^[A-Z]+-[A-Z]+$/) {
        if (length($3) > 2) print $3
    }
' |\
tr "\`" "'" |\
awk 'BEGIN{FS="-"} {for(i=1;i<=NF;i++)print $i}' |\
sort -u - census-last-names.txt -o surnames.txt

mv givenNames.txt surnames.txt ../resource
