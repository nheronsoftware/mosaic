#!/bin/bash
count=1000
size=`wc -l < person-names.txt`

awk -v count=$count -v size=$size '
    BEGIN {
        srand()
        for (i = 1; i <= count; i++) {
            do {
                rn = int(rand() * (size + 1))
            } while (rn in records)
            records[rn] = rn
        }
    }
    {
        if (NR in records)
            print
    }' \
person-names.txt | tee person-name-sample.txt
