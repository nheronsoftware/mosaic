#!/bin/bash

#(cd popular; awk -f extract-names.awk *.txt; cd ..)
#
#sort -u census-last-names.txt -o last-names.txt
#sort -u census-female-names.txt popular-female-names.txt -o female-names.txt
#sort -u census-male-names.txt popular-male-names.txt -o male-names.txt
#
#tr a-z A-Z < corporations.txt |\
#awk '
#    BEGIN { FS = OFS = "\t" }
#    { print $7 | "sort -u -o corporation-names.txt" }
#    { for (i = 18; i <= 24; i++) print $i | "sort -u -o person-names.txt" }'

awk '{ for (i = 1; i <= NF; i++) if ($i ~ /^[A-Z][-'"'"'A-Z]+$/) print $i }' person-names.txt |\
sort -u -o person-words.txt

