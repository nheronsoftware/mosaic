1 rue Jean Jaures, BP 27	NU TY AU AU BX NU:HN PT NA NA BX XN
BoxDescript	BP
BoxIdentif	27
HouseNumber	1
MatchStreetName	JEAN JAURES
OrigStreetName	JEAN JAURES
StreetNamePrefType	RUE

10 r du vieux march� aux vins	NU TY NL EX AU AU AU:HN PT NA NA NA NA NA
HouseNumber	10
MatchStreetName	VX MARCH� AUX VINS
OrigStreetName	DU VIEUX MARCH� AUX VINS
StreetNamePrefType	RUE

106 chem Place	NU TY TY:HN PT NA
HouseNumber	106
MatchStreetName	PL
OrigStreetName	PLACE
StreetNamePrefType	CHEM

12 PLACE DE LA GARE,BP 12	NU TY NL NL AU BX NU:HN PT NA NA NA BX XN
BoxDescript	BP
BoxIdentif	12
HouseNumber	12
MatchStreetName	GARE
OrigStreetName	DE LA GARE
StreetNamePrefType	PL

12 boulevard de la corderie	NU TY NL NL AU:HN PT NA NA NA
HouseNumber	12
MatchStreetName	CORDERIE
OrigStreetName	DE LA CORDERIE
StreetNamePrefType	BD

145 r Michel Carr� zi des Algorithmes BP 9006	NU TY AU AU:HN PT NA NA|BP NL AU:BT BN BN|BX NU:BX XN
BoxDescript	BP
BoxIdentif	9006
HouseNumber	145
MatchPropertyName	ALGORITHMES
MatchStreetName	MICHEL CARR�
OrigPropertyName	DES ALGORITHMES
OrigStreetName	MICHEL CARR�
PropDesSufType	ZI
StreetNamePrefType	RUE

14Ter pl Gambetta	NU EN TY AU:HN HS PT NA
HouseNumber	14
HouseNumberSuffix	T
MatchStreetName	GAMBETTA
OrigStreetName	GAMBETTA
StreetNamePrefType	PL

15 pl 11 Novembre 1918	NU TY NU DM NU:HN PT NA NA NA
HouseNumber	15
MatchStreetName	11 NOV 1918
OrigStreetName	11 NOVEMBRE 1918
StreetNamePrefType	PL

194 crs G�n de Gaulle	NU TY AU NL AU:HN PT NA NA NA
HouseNumber	194
MatchStreetName	GEN GAULLE
OrigStreetName	G�N DE GAULLE
StreetNamePrefType	CRS

2 impasse Gabriel Lacueille	NU TY AU AU:HN PT NA NA
HouseNumber	2
MatchStreetName	GABRIEL LACUEILLE
OrigStreetName	GABRIEL LACUEILLE
StreetNamePrefType	IMP

24B grand rue Charles de Gaulle	NU EN EX TY AU NL AU:HN HS ST ST NA NA NA
HouseNumber	24
HouseNumberSuffix	B
MatchStreetName	CHARLES GAULLE
OrigStreetName	CHARLES DE GAULLE
StreetNameSufType	GD RUE

3 pl Novembre	NU TY DM:HN PT NA
HouseNumber	3
MatchStreetName	NOV
OrigStreetName	NOVEMBRE
StreetNamePrefType	PL

32 patio Reflets	NU AU AU:HN NA NA
HouseNumber	32
MatchStreetName	PATIO REFLETS
OrigStreetName	PATIO REFLETS

34 av du G�n de Gaulle	NU TY NL AU NL AU:HN PT NA NA NA NA
HouseNumber	34
MatchStreetName	GEN GAULLE
OrigStreetName	DU G�N DE GAULLE
StreetNamePrefType	AV

44	NU:HN
HouseNumber	44

48 rte nationale de saint louis	NU TY NL AU AU:HN PT NA NA NA
HouseNumber	48
MatchStreetName	ST LOUIS
OrigStreetName	DE SAINT LOUIS
StreetNamePrefType	RTN

5 cours Charlemagne	NU TY AU:HN PT NA
HouseNumber	5
MatchStreetName	CHARLEMAGNE
OrigStreetName	CHARLEMAGNE
StreetNamePrefType	CRS

50 Grande Rue	NU EX TY:HN NA ST
HouseNumber	50
MatchStreetName	GDE
OrigStreetName	GRANDE
StreetNameSufType	RUE

55 Le Bourg	NU NL BP:HN NA NA
HouseNumber	55
MatchStreetName	BRG
OrigStreetName	LE BOURG

6 Bis rue Gustave Brindeau	NU EN TY AU AU:HN HS PT NA NA
HouseNumber	6
HouseNumberSuffix	B
MatchStreetName	GUSTAVE BRINDEAU
OrigStreetName	GUSTAVE BRINDEAU
StreetNamePrefType	RUE

Appartement B, le barcleleau, 1 allee limousine	WD A1 NL:WD WI NL|AU:BN|NU TY AU:HN PT NA
HouseNumber	1
MatchPropertyName	BARCLELEAU
MatchStreetName	LIMOUSINE
OrigPropertyName	BARCLELEAU
OrigStreetName	LIMOUSINE
StreetNamePrefType	ALL
WithinStructDescript	APP
WithinStructIdentif	B

BP 50048	BX NU:BX XN
BoxDescript	BP
BoxIdentif	50048

Boutique 93 grand rue charles de Gaulles	BU NU EX TY AU NL AU:BN HN ST ST NA NA NA
HouseNumber	93
MatchPropertyName	BOUT
MatchStreetName	CHARLES GAULLES
OrigPropertyName	BOUTIQUE
OrigStreetName	CHARLES DE GAULLES
StreetNameSufType	GD RUE

Cellule 47 centre cial F�ches	WD NU:WD WI|BP AU:BT BN
MatchPropertyName	F�CHES
OrigPropertyName	F�CHES
PropDesSufType	CCAL
WithinStructDescript	CELL
WithinStructIdentif	47

INGENIEUR ESCALIER 1,BATIMENT A,4 ALLEE DES JONQUILLES	AU:BN|WI NU:WD WI|BU:EI|A1:EI|NU TY NL AU:HN PT NA NA
ExtraInfo	BATIMENT A
HouseNumber	4
MatchPropertyName	INGENIEUR
MatchStreetName	JONQUILLES
OrigPropertyName	INGENIEUR
OrigStreetName	DES JONQUILLES
StreetNamePrefType	ALL
WithinStructDescript	ESC
WithinStructIdentif	1

Le Pr� des Landes	NL AU NL AU:BN BN BN BN
MatchPropertyName	PR� LANDES
OrigPropertyName	LE PR� DES LANDES

PLACE DES QUATRE VINGTS	TY NL AU AU:PT NA NA NA
MatchStreetName	QUATRE VINGTS
OrigStreetName	DES QUATRE VINGTS
StreetNamePrefType	PL

PLACE DU 77 IEME	TY NL NU AU:PT NA NA NA
MatchStreetName	77 IEME
OrigStreetName	DU 77 IEME
StreetNamePrefType	PL

POST RESTANTE PARIS AUTEUIL	BP AU AU:BT BN BN
MatchPropertyName	PARIS AUTEUIL
OrigPropertyName	PARIS AUTEUIL
PropDesSufType	PSTR

Pont de Cervi�res 20 r Joseph Silvestre	TY NL AU NU TY AU AU:BT BN BN HN PT NA NA
HouseNumber	20
MatchPropertyName	CERVI�RES
MatchStreetName	JOSEPH SILVESTRE
OrigPropertyName	DE CERVI�RES
OrigStreetName	JOSEPH SILVESTRE
PropDesSufType	PONT
StreetNamePrefType	RUE

ROUTE DE VILLENAVE	TY NL AU:PT NA NA
MatchStreetName	VILLENAVE
OrigStreetName	DE VILLENAVE
StreetNamePrefType	RTE

RUE DU PAPE JEAN XXIII	TY NL AU AU AU:PT NA NA NA NA
MatchStreetName	PAPE JEAN XXIII
OrigStreetName	DU PAPE JEAN XXIII
StreetNamePrefType	RUE

b�t D r�sid Arago 2	BU A1 BP AU NU:BY BI BT BN BN
CenterDescript	B�timent
CenterIdentif	D
MatchPropertyName	ARAGO 2
OrigPropertyName	ARAGO 2
PropDesSufType	RES

centre cial Le Ch�ne	BP NL AU:BT BN BN
MatchPropertyName	CH�NE
OrigPropertyName	LE CH�NE
PropDesSufType	CCAL

gal Grands Homme	TY AU AU:PT NA NA
MatchStreetName	GRANDS HOMME
OrigStreetName	GRANDS HOMME
StreetNamePrefType	GAL

imm Central Parc	BU AU AU:BT BN BN
MatchPropertyName	CENTRAL PARC
OrigPropertyName	CENTRAL PARC
PropDesSufType	IMM

lot 112 r Le Corbusier	AU:BN|NU TY NL AU:HN PT NA NA
HouseNumber	112
MatchPropertyName	LOT
MatchStreetName	CORBUSIER
OrigPropertyName	LOT
OrigStreetName	LE CORBUSIER
StreetNamePrefType	RUE

lot Beaulieu 1 r Champollion	BP AU:BT BN|NU TY AU:HN PT NA
HouseNumber	1
MatchPropertyName	BEAULIEU
MatchStreetName	CHAMPOLLION
OrigPropertyName	BEAULIEU
OrigStreetName	CHAMPOLLION
PropDesSufType	LOT
StreetNamePrefType	RUE

quart Loubian av Charles de Gaulle	BP AU:BT BN|TY AU NL AU:PT NA NA NA
MatchPropertyName	LOUBIAN
MatchStreetName	CHARLES GAULLE
OrigPropertyName	LOUBIAN
OrigStreetName	CHARLES DE GAULLE
PropDesSufType	QUA
StreetNamePrefType	AV

rte D�partementale 651	HR NU:PT NA
MatchStreetName	651
OrigStreetName	651
StreetNamePrefType	RTED

rte Goulets	TY AU:PT NA
MatchStreetName	GOULETS
OrigStreetName	GOULETS
StreetNamePrefType	RTE

rue Albert 1er de Belgique	TY AU AU NL AU:PT NA NA NA NA
MatchStreetName	ALBERT 1ER BELGIQUE
OrigStreetName	ALBERT 1ER DE BELGIQUE
StreetNamePrefType	RUE

rue du 11 Novembre 1918	TY NL NU DM NU:PT NA NA NA NA
MatchStreetName	11 NOV 1918
OrigStreetName	DU 11 NOVEMBRE 1918
StreetNamePrefType	RUE

r�sid Clos Arlac 5 r des �illets	BP TY AU:BT BN BN|NU TY NL AU:HN PT NA NA
HouseNumber	5
MatchPropertyName	CLOS ARLAC
MatchStreetName	�ILLETS
OrigPropertyName	CLOS ARLAC
OrigStreetName	DES �ILLETS
PropDesSufType	RES
StreetNamePrefType	RUE

zi Alfred Daney r La Motte Picquet	BP AU AU:BT BN BN|TY NL AU AU:PT NA NA NA
MatchPropertyName	ALFRED DANEY
MatchStreetName	MOTTE PICQUET
OrigPropertyName	ALFRED DANEY
OrigStreetName	LA MOTTE PICQUET
PropDesSufType	ZI
StreetNamePrefType	RUE

zone artisanale Alleux	BP AU:BT BN
MatchPropertyName	ALLEUX
OrigPropertyName	ALLEUX
PropDesSufType	ZAR

