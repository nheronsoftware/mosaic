<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="xml" indent="yes"/>
	
	<xsl:template match="mdmAddressParser">
		<bean id="parser" class="com.sun.mdm.sbme.datatype.address.parser.AddressParser">
			<property name="locale">
				<ref local="locale"/>
			</property>
		</bean>
	</xsl:template>
	
	<xsl:template match="javaParser">
		<bean id="parser">
			<xsl:apply-templates select="@*|node()"/>
		</bean>
	</xsl:template>

</xsl:stylesheet>
