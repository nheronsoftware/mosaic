<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="xml" indent="yes"/>
	
	<xsl:template match="patterns">
		<bean id="patternRegistry" class="com.sun.mdm.sbme.datatype.address.pattern.PatternRegistry">
			<property name="patternMap">
				<map>
					<xsl:apply-templates select="pattern"/>
				</map>
			</property>
		</bean>
	</xsl:template>
	
	<xsl:template match="pattern">
		<entry>
		<!--  FIXME: This creates a redundant copy of the input token list -->
			<key>
				<list>
					<xsl:for-each select="inputTokens/inputToken">
						<value type="com.sun.mdm.sbme.datatype.address.pattern.InputTokenType">
							<xsl:value-of select="normalize-space(string(.))"/>
						</value>
					</xsl:for-each>
				</list>
			</key>
			<bean class="com.sun.mdm.sbme.datatype.address.pattern.AddressPattern">
				<constructor-arg>
					<list>
						<xsl:for-each select="inputTokens/inputToken">
							<value type="com.sun.mdm.sbme.datatype.address.pattern.InputTokenType">
								<xsl:value-of select="normalize-space(string(.))"/>
							</value>
						</xsl:for-each>
					</list>
				</constructor-arg>
				<constructor-arg>
					<list>
						<xsl:for-each select="outputTokens/outputToken">
							<value type="com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType">
								<xsl:value-of select="normalize-space(string(.))"/>
							</value>
						</xsl:for-each>
					</list>
				</constructor-arg>
				<constructor-arg>
					<value>
						<xsl:value-of select="normalize-space(string(priority))"/>
					</value>
				</constructor-arg>
				<constructor-arg>
					<value>
						<xsl:value-of select="normalize-space(string(patternClass))"/>
					</value>
				</constructor-arg>
				<constructor-arg>
					<value>
						<xsl:value-of select="normalize-space(string(scoringOption))"/>
					</value>
				</constructor-arg>
			</bean>
		</entry>
	</xsl:template>

</xsl:stylesheet>
