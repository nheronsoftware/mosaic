<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="xml" indent="yes"/>
	
	<xsl:template match="masterClues">
		<bean id="masterClueRegistry" class="com.sun.mdm.sbme.datatype.address.builder.MasterClueRegistry">
			<property name="masterClues">
				<map>
					<xsl:apply-templates select="masterClueGroup"/>
				</map>
			</property>
			<property name="inputTokenTypeMapper">
				<bean class="{@mapper}"/>
			</property>
		</bean>
	</xsl:template>

	<xsl:template match="masterClues/masterClueGroup">
		<entry key="{normalize-space(@id)}">
			<map>
				<xsl:apply-templates select="masterClue"/>
			</map>
		</entry>
	</xsl:template>

	<xsl:template match="masterClueGroup/masterClue">
		<entry>
			<key>
				<value type="com.sun.mdm.sbme.datatype.address.pattern.InputTokenType">
					<xsl:value-of select="normalize-space(@type)"/>
				</value>
			</key>
			<bean class="com.sun.mdm.sbme.datatype.address.builder.MasterClue">
				<xsl:for-each select="*">
					<xsl:if test="string-length(.) != 0">
						<property name="{name(.)}">
							<value><xsl:value-of select="string(.)"/></value>
						</property>
					</xsl:if>
				</xsl:for-each>
			</bean>
		</entry>
	</xsl:template>
	
</xsl:stylesheet>
