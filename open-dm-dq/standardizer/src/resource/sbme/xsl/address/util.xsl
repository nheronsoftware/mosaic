<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
				xmlns:exsl="http://exslt.org/common"
                extension-element-prefixes="exsl">
                
	<xsl:output method="xml" indent="yes"/>

	<xsl:template match="include">
		<xsl:apply-templates select="document(@href)"/>
	</xsl:template>
			
	<xsl:template name="toUppercase">
		<xsl:param name="string"/>
		<xsl:value-of select="translate($string, 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ')"/>
	</xsl:template>
	
	<xsl:template name="scriptingSupport">
		<xsl:param name="language"/>
		<xsl:param name="contextName"/>
		<xsl:param name="interfaces"/>
		<xsl:call-template name="dynamicProxy">
			<xsl:with-param name="interfaces">
				<xsl:for-each select="exsl:node-set($interfaces)/interface">
					<interface class="{@class}">
						<xsl:for-each select="method">
							<method name="{@name}">
								<xsl:for-each select="argument">
									<argument name="{@name}" type="{@type}"/>
								</xsl:for-each>
								<executor class="com.sun.inti.components.proxy.ScriptExecutor">
									<constructor-arg>
										<value>
										<xsl:choose>
											<xsl:when test="string-length(normalize-space(string($language))) = 0">
												<xsl:text>javascript</xsl:text>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="$language"/>
											</xsl:otherwise>
										</xsl:choose>
										</value>
									</constructor-arg>
									<constructor-arg>
										<value>
											<xsl:value-of select="string(script)"/>
										</value>
									</constructor-arg>
									<constructor-arg>
										<list>
											<xsl:for-each select="argument">
												<value>
													<xsl:value-of select="@name"/>
												</value>
											</xsl:for-each>
										</list>
									</constructor-arg>
								<constructor-arg>
									<value>
										<xsl:choose>
											<xsl:when test="string-length(normalize-space(string($contextName))) = 0">
												<xsl:text>scriptContext</xsl:text>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="$contextName"/>
											</xsl:otherwise>
										</xsl:choose>
										</value>
									</constructor-arg>
								</executor>
							</method>
						</xsl:for-each>
					</interface>
				</xsl:for-each>
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
	
	<xsl:template name="dynamicProxy">
		<xsl:param name="interfaces"/>
		<bean class="com.sun.inti.components.beans.InterfaceImplementorFactoryBean">
			<constructor-arg>
				<list>
					<xsl:for-each select="exsl:node-set($interfaces)/interface">
						<bean class="com.sun.inti.components.proxy.InterfaceDescriptor">
							<property name="interfaceClass">
								<value>
									<xsl:value-of select="@class"/>
								</value>
							</property>
							<property name="methodDescriptors">
								<list>
									<xsl:for-each select="method">
										<bean class="com.sun.inti.components.proxy.MethodDescriptor">
											<property name="name">
												<value>
													<xsl:value-of select="@name"/>
												</value>
											</property>
											<property name="executor">
												<bean class="{executor/@class}">
													<xsl:apply-templates select="executor/*"/>
												</bean>
											</property>
											<property name="argumentDescriptors">
												<list>
													<xsl:for-each select="argument">
														<bean class="com.sun.inti.components.proxy.ArgumentDescriptor">
															<property name="name">
																<value>
																	<xsl:value-of select="@name"/>
																</value>
															</property>
															<property name="type">
																<value>
																	<xsl:value-of select="@type"/>
																</value>
															</property>
														</bean>
													</xsl:for-each>
												</list>
											</property>
										</bean>
									</xsl:for-each>
								</list>
							</property>
						</bean>
					</xsl:for-each>
				</list>
			</constructor-arg>
			<constructor-arg>
				<value>
					<xsl:choose>
						<xsl:when test="@singleton">
							<xsl:value-of select="@singleton"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>false</xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</value>
			</constructor-arg>
		</bean>
	</xsl:template>
	
	<xsl:template match="scriptExecutor">
		<bean class="com.sun.inti.components.proxy.ScriptExecutor">
			<constructor-arg>
				<value>
					<xsl:value-of select="@language"/>
				</value>
			</constructor-arg>
			<constructor-arg>
				<value>
					<xsl:value-of select="string(script)"/>
				</value>
			</constructor-arg>
			<constructor-arg>
				<list>
					<xsl:for-each select="argument">
						<value>
							<xsl:value-of select="@name"/>
						</value>
					</xsl:for-each>
				</list>
			</constructor-arg>
			<constructor-arg>
				<value>
					<xsl:value-of select="@contextName"/>
				</value>
			</constructor-arg>
		</bean>
	</xsl:template>
</xsl:stylesheet>
