<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="xml" indent="yes"/>
	
	<xsl:template match="mdmAddressNormalizer">
		<bean id="normalizer" class="com.sun.mdm.sbme.normalizer.DefaultNormalizer">
			<property name="clueRegistry">
				<ref local="clueRegistry"/>
			</property>
			<property name="clueValidators">
				<list>
					<xsl:apply-templates select="clueValidators/*"/>
				</list>
			</property>
			<property name="clueWordTokenPopulators">
				<list>
					<xsl:apply-templates select="tokenPopulators/*"/>
				</list>
			</property>
			<property name="normalizationPostprocessor">
				<xsl:apply-templates select="*[contains(name(.), 'Postprocessor')]"/>
			</property>
		</bean>
	</xsl:template>
	
	<xsl:template match="mdmNormalizationPostprocessor">
		<bean class="com.sun.mdm.sbme.datatype.address.normalizer.postprocessor.DefaultNormalizationPostprocessor"/>
	</xsl:template>
	
	<xsl:template match="javaClueValidator">
		<bean>
			<xsl:apply-templates select="@*|node()"/>
		</bean>
	</xsl:template>
	
	<xsl:template match="javaTokenPopulator">
		<bean>
			<xsl:apply-templates select="@*|node()"/>
		</bean>
	</xsl:template>

</xsl:stylesheet>
