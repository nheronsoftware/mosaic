<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
				xmlns:exsl="http://exslt.org/common"
                extension-element-prefixes="exsl">
                
	<xsl:output method="xml"
				indent="yes"
				doctype-public="-//SPRING//DTD BEAN 2.0//EN"
				doctype-system="http://www.springframework.org/dtd/spring-beans-2.0.dtd"/>

	<xsl:include href="util.xsl"/>	
	<xsl:include href="locale.xsl"/>	
	<xsl:include href="cleanser.xsl"/>	
	<xsl:include href="parser.xsl"/>	
	<xsl:include href="normalizer.xsl"/>	
	<xsl:include href="patternFinder.xsl"/>	
	<xsl:include href="builder.xsl"/>	
	<xsl:include href="patternRegistry.xsl"/>	
	<xsl:include href="clueRegistry.xsl"/>
	<xsl:include href="masterClueRegistry.xsl"/>
	
	<xsl:template match="standardizer">
		<beans>
			<bean id="standardizer" class="com.sun.mdm.sbme.DefaultStandardizer">
				<property name="cleanser">
					<ref local="cleanser"/>
				</property>
				<property name="parser">
					<ref local="parser"/>
				</property>
				<property name="normalizer">
					<ref local="normalizer"/>
				</property>
				<property name="patternFinder">
					<ref local="patternFinder"/>
				</property>
				<property name="builder">
					<ref local="builder"/>
				</property>
			</bean>
			
			<xsl:apply-templates/>
			
			<bean id="customEditorConfigurer" 
			      class="org.springframework.beans.factory.config.CustomEditorConfigurer">
			  <property name="customEditors">
			    <map>
			      <entry key="java.lang.StringBuilder">
			        <bean class="com.sun.inti.components.beans.StringBuilderPropertyEditor"/>
			      </entry>
			    </map>
			  </property>
			</bean>
		</beans>
	</xsl:template>

	<xsl:template match="@*|node()"  priority="-1">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
	
</xsl:stylesheet>
