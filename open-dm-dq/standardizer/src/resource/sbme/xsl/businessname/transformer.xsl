<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="xml" indent="yes"/>
	
    <xsl:template match="javaTransform">
    	<bean>
    			<xsl:apply-templates select="@*|node()"/>
    	</bean>
	</xsl:template>

    <xsl:template match="mdmBusinessNameParser/recordTransforms">
    	<property name="recordTransformer">
    		<bean class="com.sun.inti.components.string.transform.CompoundStringTransformer">
    			<constructor-arg>
		    		<list>
		    			<xsl:apply-templates/>
	    			</list>
	    		</constructor-arg>
	    		<constructor-arg>
	    			<value>
		    			<xsl:choose>
		    				<xsl:when test="not(@stopOnExit)">
		    					<xsl:text>false</xsl:text>
		    				</xsl:when>
		    				<xsl:otherwise>
		    					<xsl:value-of select="@stopOnExit"/>
		    				</xsl:otherwise>
		    			</xsl:choose>
		    		</value>
	    		</constructor-arg>
	    	</bean>
    	</property>
    </xsl:template>
    
    <xsl:template match="mdmBusinessNameParser/fieldTransforms">
    	<property name="fieldTransformer">
    		<bean class="com.sun.inti.components.string.transform.CompoundStringTransformer">
    			<constructor-arg>
		    		<list>
		    			<xsl:apply-templates/>
	    			</list>
	    		</constructor-arg>
	    		<constructor-arg>
	    			<value>
		    			<xsl:choose>
		    				<xsl:when test="not(@stopOnExit)">
		    					<xsl:text>false</xsl:text>
		    				</xsl:when>
		    				<xsl:otherwise>
		    					<xsl:value-of select="@stopOnExit"/>
		    				</xsl:otherwise>
		    			</xsl:choose>
		    		</value>
	    		</constructor-arg>
	    	</bean>
    	</property>
    </xsl:template>
		
    <xsl:template match="transforms">
   		<bean class="com.sun.inti.components.string.transform.CompoundStringTransformer">
   			<constructor-arg>
	    		<list>
	    			<xsl:apply-templates/>
    			</list>
    		</constructor-arg>
    		<constructor-arg>
    			<value>
	    			<xsl:choose>
	    				<xsl:when test="not(@stopOnExit)">
	    					<xsl:text>false</xsl:text>
	    				</xsl:when>
	    				<xsl:otherwise>
	    					<xsl:value-of select="@stopOnExit"/>
	    				</xsl:otherwise>
	    			</xsl:choose>
	    		</value>
    		</constructor-arg>
    	</bean>
    </xsl:template>
    
    <xsl:template match="scriptTransform">
    	<xsl:call-template name="scriptingSupport">
    		<xsl:with-param name="language" select="@language"/>
    		<xsl:with-param name="contextName" select="@scriptContext"/>
    		<xsl:with-param name="interfaces">
	    		<interface class="com.sun.inti.components.string.transform.StringTransformer">
	    			<method name="transform">
	    				<argument name="addressString" type="java.lang.String"/>
	    				<script><xsl:value-of select="string(.)"/></script>
	    			</method>
	    		</interface>
    		</xsl:with-param>
    	</xsl:call-template>
    </xsl:template>
    
    <xsl:template match="replace">
    	<bean class="com.sun.inti.components.string.transform.SimpleRegexStringTransformer">
    		<constructor-arg>
    			<value>
    				<xsl:value-of select="@regex"/>
    			</value>
    		</constructor-arg>
    		<constructor-arg>
    			<value>
    				<xsl:value-of select="@replacement"/>
    			</value>
    		</constructor-arg>
    		<constructor-arg>
    			<value>false</value>
    		</constructor-arg>
    	</bean>
    </xsl:template>
    
    <xsl:template match="replaceAll">
    	<bean class="com.sun.inti.components.string.transform.SimpleRegexStringTransformer">
    		<constructor-arg>
    			<value>
    				<xsl:value-of select="@regex"/>
    			</value>
    		</constructor-arg>
    		<constructor-arg>
    			<value>
    				<xsl:value-of select="@replacement"/>
    			</value>
    		</constructor-arg>
    		<constructor-arg>
    			<value>true</value>
    		</constructor-arg>
    	</bean>
    </xsl:template>
    
	<xsl:template match="if">
		<bean class="com.sun.inti.components.string.transform.IfStringTransformer">
			<constructor-arg>
				<xsl:choose>
					<xsl:when test="@matches">
						<bean class="com.sun.inti.components.string.match.RegexCharSequenceMatcher">
							<constructor-arg>
								<value><xsl:value-of select="@matches"/></value>
							</constructor-arg>
						</bean>
					</xsl:when>
					<xsl:when test="@contains">
						<bean class="com.sun.inti.components.string.match.ContainsCharSequenceMatcher">
							<constructor-arg>
								<value><xsl:value-of select="@contains"/></value>
							</constructor-arg>
						</bean>
					</xsl:when>
					<xsl:when test="@startsWith">
						<bean class="com.sun.inti.components.string.match.StartsWithCharSequenceMatcher">
							<constructor-arg>
								<value><xsl:value-of select="@startWith"/></value>
							</constructor-arg>
						</bean>
					</xsl:when>
					<xsl:when test="@endsWith">
						<bean class="com.sun.inti.components.string.match.EndsWithCharSequenceMatcher">
							<constructor-arg>
								<value><xsl:value-of select="@startWith"/></value>
							</constructor-arg>
						</bean>
					</xsl:when>
				</xsl:choose>
			</constructor-arg>
			<constructor-arg>
				<xsl:apply-templates/>
			</constructor-arg>
		</bean>
    </xsl:template>
    
	<xsl:template match="choose">
		<bean class="com.sun.inti.components.string.transform.ChooseStringTransformer">
			<constructor-arg>
				<list>
					<xsl:apply-templates select="when"/>
				</list>
			</constructor-arg>
			<xsl:if test="otherwise">
				<constructor-arg>
					<xsl:apply-templates select="otherwise"/>
				</constructor-arg>
			</xsl:if>
		</bean>
    </xsl:template>
    
	<xsl:template match="choose/when">
		<bean class="com.sun.inti.components.string.transform.ChooseStringTransformer$WhenHandler">
			<constructor-arg>
				<xsl:choose>
					<xsl:when test="@matches">
						<bean class="com.sun.inti.components.string.match.RegexCharSequenceMatcher">
							<constructor-arg>
								<value><xsl:value-of select="@matches"/></value>
							</constructor-arg>
						</bean>
					</xsl:when>
					<xsl:when test="@contains">
						<bean class="com.sun.inti.components.string.match.ContainsCharSequenceMatcher">
							<constructor-arg>
								<value><xsl:value-of select="@contains"/></value>
							</constructor-arg>
						</bean>
					</xsl:when>
					<xsl:when test="@startsWith">
						<bean class="com.sun.inti.components.string.match.StartsWithCharSequenceMatcher">
							<constructor-arg>
								<value><xsl:value-of select="@startWith"/></value>
							</constructor-arg>
						</bean>
					</xsl:when>
					<xsl:when test="@endsWith">
						<bean class="com.sun.inti.components.string.match.EndsWithCharSequenceMatcher">
							<constructor-arg>
								<value><xsl:value-of select="@startWith"/></value>
							</constructor-arg>
						</bean>
					</xsl:when>
				</xsl:choose>
			</constructor-arg>
			<constructor-arg>
				<xsl:apply-templates/>
			</constructor-arg>
		</bean>
    </xsl:template>
    
	<xsl:template match="choose/otherwise">
		<xsl:apply-templates/>
    </xsl:template>
    
	<xsl:template match="global">
		<bean class="com.sun.inti.components.string.transform.GlobalStringTransformer">
			<constructor-arg>
				<bean class="com.sun.inti.components.string.transform.CompoundStringTransformer">
					<constructor-arg>
						<list>
							<xsl:apply-templates/>
						</list>
					</constructor-arg>
				</bean>
			</constructor-arg>
		</bean>
    </xsl:template>
    
	<xsl:template match="deleteChars">
		<bean class="com.sun.inti.components.string.transform.CharDeletionStringTransformer">
			<xsl:for-each select="*">
				<constructor-arg>
					<xsl:apply-templates select="."/>
				</constructor-arg>
			</xsl:for-each>
		</bean>
	</xsl:template>    
    
	<xsl:template match="deleteChars/whereAnyOf">
		<bean class="com.sun.inti.components.string.match.OrCompositeCharMatcher">
			<constructor-arg>
				<list>
					<xsl:apply-templates/>
				</list>
			</constructor-arg>
		</bean>
    </xsl:template>
    
	<xsl:template match="deleteChars/whereAllOf">
		<bean class="com.sun.inti.components.string.match.AndCompositeCharMatcher">
			<constructor-arg>
				<list>
					<xsl:apply-templates/>
				</list>
			</constructor-arg>
		</bean>
    </xsl:template>
    
	<xsl:template match="deleteChars/whereNot">
		<bean class="com.sun.inti.components.string.match.CompoundCharMatcher">
			<constructor-arg>
				<list>
					<xsl:apply-templates/>
				</list>
			</constructor-arg>
		</bean>
    </xsl:template>
    
	<xsl:template match="deleteChars/exceptIf">
		<bean>
			<xsl:attribute name="class">
				<xsl:value-of select="@allowedBy"/>
			</xsl:attribute>
			<xsl:apply-templates select="@*[name() != 'allowedBy']|node()"/>
		</bean>
	</xsl:template>    
    
	<xsl:template match="not">
		<bean class="com.sun.inti.components.string.match.NegationCharMatcher">
			<constructor-arg>
				<xsl:apply-templates/>
			</constructor-arg>
		</bean>
    </xsl:template>
    
	<xsl:template match="letterOrDigit">
		<bean class="com.sun.inti.components.string.match.LetterOrDigitCharMatcher"/>
    </xsl:template>
    
	<xsl:template match="charIn">
		<bean class="com.sun.inti.components.string.match.ArrayCharMatcher">
			<constructor-arg>
				<value><xsl:value-of select="string(.)"/></value>
			</constructor-arg>
		</bean>
    </xsl:template>
    
	<xsl:template match="isChar">
		<bean class="com.sun.inti.components.string.match.FixedCharMatcher">
			<constructor-arg>
				<value><xsl:value-of select="substring(string(.), 1, 1)"/></value>
			</constructor-arg>
		</bean>
    </xsl:template>
    
	<xsl:template match="trim">
		<bean class="com.sun.inti.components.string.transform.TrimStringTransformer" scope="singleton"/>
	</xsl:template>    
	
	<xsl:template match="uppercase">
		<bean class="com.sun.inti.components.string.transform.UpperCaseStringTransformer" scope="singleton"/>
	</xsl:template>    

</xsl:stylesheet>
