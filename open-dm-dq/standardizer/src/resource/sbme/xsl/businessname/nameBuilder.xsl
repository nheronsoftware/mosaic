<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="xml" indent="yes"/>
	
	<xsl:template match="mdmBusinessNameBuilder">
		<bean id="nameBuilder" class="com.sun.mdm.sbme.datatype.businessname.builder.DefaultBusinessNameBuilder">
			<property name="processor">
				<bean class="com.sun.mdm.sbme.datatype.businessname.builder.CompoundInputTokenTypeProcessor">
					<property name="processors">
						<list>
							<xsl:apply-templates select="imageTypeProcessors/*"/>
						</list>
					</property>
				</bean>
			</property>
		</bean>
	</xsl:template>
</xsl:stylesheet>
