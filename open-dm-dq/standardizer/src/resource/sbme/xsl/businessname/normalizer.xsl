<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="xml" indent="yes"/>
	
	<xsl:template match="mdmBusinessNameNormalizer">
		<bean id="normalizer" class="com.sun.mdm.sbme.datatype.businessname.normalizer.DefaultBusinessNameNormalizer">
			<property name="tokenHandler">
				<bean class="com.sun.mdm.sbme.datatype.businessname.normalizer.handler.CompoundTokenHandler">
					<property name="handlers">
						<list>
							<bean class="com.sun.mdm.sbme.datatype.businessname.normalizer.handler.ConditionalTokenHandler">
								<property name="pairs">
									<list>
										<xsl:apply-templates select="tokenHandlerPairs/*"/>
									</list>
								</property>
								<property name="maxImageSize">
									<value>2</value>
								</property>
								<property name="transformer">
									<ref bean="DiacriticalMarkStringTransformer"/>
								</property>
							</bean>
							<bean class="com.sun.mdm.sbme.datatype.businessname.normalizer.handler.IndustrySectorTokenHandler">
								<property name="industryCatagoryCodes">
									<ref bean="industryCatagoryCodes"/>
								</property>
							</bean>
						</list>
					</property>
				</bean>
			</property>
		</bean>
	</xsl:template>
</xsl:stylesheet>
