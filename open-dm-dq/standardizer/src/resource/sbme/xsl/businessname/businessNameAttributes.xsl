<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
				xmlns:util="http://www.springframework.org/schema/util">
	<xsl:output method="xml" indent="yes"/>
	
	<xsl:template match="companies">
		<xsl:if test="companies/primaryNameCompany">
			<util:map id="primaryNames">
				<xsl:apply-templates/>
			</util:map>
		</xsl:if>	
			
		<xsl:if test="companies/mergerNames">
			<util:map id="mergerNames">
				<xsl:apply-templates/>
			</util:map>
		</xsl:if>
		
		<xsl:if test="companies/formerNameCompany">
			<util:map id="formerNames">
				<xsl:apply-templates/>
			</util:map>
		</xsl:if>
		
		<xsl:if test="companies/newNameCompany">
			<util:map id="newNames">
				<xsl:apply-templates/>
			</util:map>
		</xsl:if>
	</xsl:template>
	
	<xsl:template match="companies/primaryNameCompany">
		<entry>
		    <key>
		    	<value><xsl:value-of select="normalize-space(string(name))"/></value>
		    </key>
			<value><xsl:value-of select="normalize-space(string(industrySector))"/></value>
		</entry>
	</xsl:template>
	
	<xsl:template match="companies/mergerNameCompany">
		<entry>
		    <key>
		    	<value><xsl:value-of select="normalize-space(string(name))"/></value>
		    </key>
			<value><xsl:value-of select="normalize-space(string(industrySector))"/></value>
		</entry>
	</xsl:template>
	
	<xsl:template match="companies/formerNameCompany">
		<entry>
		    <key>
		    	<value><xsl:value-of select="normalize-space(string(former))"/></value>
		    </key>
			<value><xsl:value-of select="normalize-space(string(new))"/></value>
		</entry>
	</xsl:template>
	
	<xsl:template match="companies/newNameCompany">
		<entry>
		    <key>
		    	<value><xsl:value-of select="normalize-space(string(new))"/></value>
		    </key>
			<value><xsl:value-of select="normalize-space(string(former))"/></value>
		</entry>
	</xsl:template>

</xsl:stylesheet>
