<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="xml" indent="yes"/>
	
	<xsl:template match="mdmBusinessNameParser">
		<bean id="parser" class="com.sun.mdm.sbme.datatype.businessname.parser.DefaultBusinessNameParser">
			<property name="postprocessor">
				<bean class="com.sun.mdm.sbme.datatype.businessname.parser.postprocessor.CompoundTokenPostprocessor">
					<property name="postprocessors">
						<list>
							<xsl:apply-templates select="tokenPostprocessors/*"/>
						</list>
					</property>
				</bean>
			</property>
			<xsl:apply-templates select="recordTransforms"/>
			<xsl:apply-templates select="fieldTransforms"/>
		</bean>
	</xsl:template>
	
</xsl:stylesheet>
