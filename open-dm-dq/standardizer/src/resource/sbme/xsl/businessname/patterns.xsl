<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:util="http://www.springframework.org/schema/util"
				version="1.0">
   <xsl:output method="xml" indent="yes"/>
   
   <xsl:template match="patterns">
       <property name="patterns">
           <util:map map-class="java.util.TreeMap">
               <xsl:apply-templates select="pattern"/>
           </util:map>
       </property>
       
   </xsl:template>
   
   <xsl:template match="patterns/pattern">
   	<entry>
   		<key>
   			<value>
   				<xsl:value-of select="normalize-space(string(patternSize))"/>
   				<xsl:text> </xsl:text>
   				<xsl:value-of select="normalize-space(string(inputPattern))"/>
   			</value>
   		</key>
       <bean class="com.sun.mdm.sbme.datatype.businessname.Pattern">
    		<constructor-arg>
				<value><xsl:value-of select="normalize-space(string(patternSize))"/></value>
			</constructor-arg>
			<constructor-arg>
				<value><xsl:value-of select="normalize-space(string(inputPattern))"/></value>
			</constructor-arg>
			<constructor-arg>
				<value><xsl:value-of select="normalize-space(string(outputPattern))"/></value>
			</constructor-arg>
       </bean>
     </entry>
   </xsl:template>

</xsl:stylesheet>
