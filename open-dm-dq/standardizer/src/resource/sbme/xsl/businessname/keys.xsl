<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
				xmlns:util="http://www.springframework.org/schema/util">
   <xsl:output method="xml" indent="yes"/>
   
   <xsl:include href="industryTypeKeys.xsl"/>
   <xsl:include href="countryKeys.xsl"/>
   <xsl:include href="cityStateKeys.xsl"/>
   <xsl:include href="associationKeys.xsl"/>
   <xsl:include href="organizationKeys.xsl"/>
   <xsl:include href="industryCatagoryCodes.xsl"/>
   <xsl:include href="businessNameAttributes.xsl"/>
   	<xsl:include href="terms.xsl"/>
   
   	<xsl:template match="murlameBusinessNameData">
		<xsl:apply-templates/>
	</xsl:template>
	
     <xsl:template match="keys">
       <xsl:if test="country">
	       <util:list id="countryNames">
	           <xsl:apply-templates select="country/name"/>
	       </util:list>
	       
	       <util:list id="countryCodes">
	           <xsl:apply-templates select="country/code"/>
	       </util:list>
	       
	       <util:list id="countryNationalities">
	           <xsl:apply-templates select="country/nationality"/>
	       </util:list>
	  </xsl:if>
       
       <xsl:if test="cityState">
	       <util:list id="cityStateNames">
	           <xsl:apply-templates select="cityState/name"/>
	       </util:list>

	       <util:list id="cityStateTypes">
	           <xsl:apply-templates select="cityState/type"/>
	       </util:list>

	       <util:list id="cityStateCountryCodes">
	           <xsl:apply-templates select="cityState/countryCode"/>
	       </util:list>
	   </xsl:if>
	   
	   <xsl:if test="adjectiveTypes/adjective">
	       <util:list id="adjectiveTypes">
	           <xsl:apply-templates select="adjectiveTypes/adjective"/>
	       </util:list>
       </xsl:if>
       
       <xsl:if test="industryTypeRegistry/industryType">
	  		<util:map id="industryRegistry">
				<xsl:apply-templates select="industryTypeRegistry/industryType"/>
			</util:map>
	   </xsl:if>
		
       <xsl:if test="association">
	       <util:map id="associations">
	  			<xsl:apply-templates select="association"/>
	       </util:map>
	   </xsl:if>
       
       <xsl:if test="organization">
	       <util:map id="organizations">
				<xsl:apply-templates select="organization"/>
	       </util:map>
	   </xsl:if>
       
       <xsl:if test="industryCatagory">
	       <util:map id="industryCatagoryCodes">
				<xsl:apply-templates select="industryCatagory"/>
	       </util:map>
	   </xsl:if>
       
   </xsl:template>
   
   <xsl:template match="keys/adjectiveTypes/adjective">
       <value><xsl:value-of select="normalize-space(string(.))"/></value>
   </xsl:template>

</xsl:stylesheet>
