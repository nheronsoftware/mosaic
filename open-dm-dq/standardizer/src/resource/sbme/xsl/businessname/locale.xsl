<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="xml" indent="yes"/>
	
	<xsl:template match="locale">
		<bean id="locale" class="{@class}">
			<constructor-arg>
				<value><xsl:value-of select="@id"/></value>
			</constructor-arg>
			<constructor-arg>
				<value><xsl:value-of select="@connectorTokens"/></value>
			</constructor-arg>
		</bean>
	</xsl:template>
</xsl:stylesheet>
