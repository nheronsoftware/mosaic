<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
   <xsl:output method="xml" indent="yes"/>
   
   <xsl:template match="keys/cityState/name">
   		<value><xsl:value-of select="normalize-space(string(.))"/></value>
   </xsl:template>
   <xsl:template match="keys/cityState/type">
       <value><xsl:value-of select="normalize-space(string(.))"/></value>
   </xsl:template>
   <xsl:template match="keys/cityState/countryCode">
       <value><xsl:value-of select="normalize-space(string(.))"/></value>
   </xsl:template>
      
</xsl:stylesheet>
