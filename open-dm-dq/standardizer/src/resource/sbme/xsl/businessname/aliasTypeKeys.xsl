<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
   <xsl:output method="xml" indent="yes"/>
   
   <xsl:template match="aliasKeys">       
        <bean id="aliasTypes" class="com.sun.mdm.sbme.datatype.businessname.normalizer.AliasTypeRegistry">
       		<constructor-arg>
	       		<map>
					<xsl:apply-templates select="alias"/>
				</map>
			</constructor-arg>
		</bean>
   </xsl:template>
       
   <xsl:template match="aliasKeys/alias">
		<entry>
			<key>
				<value><xsl:value-of select="normalize-space(string(aliasName))"/></value>
			</key>
			<list>
				<xsl:apply-templates select="assumedNames/name"/>
			</list>
		</entry>
	</xsl:template>
	
	<xsl:template match="aliasKeys/alias/assumedNames/name">
		<value><xsl:value-of select="normalize-space(string(.))"/></value>
	</xsl:template>

</xsl:stylesheet>
   
