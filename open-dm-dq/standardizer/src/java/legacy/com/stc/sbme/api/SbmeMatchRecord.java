/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * Base class that defines the properties of any records that
 * need to be compared
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
public class SbmeMatchRecord {

    /*
     * A HashMap variable that holds the pairs of keys/values of
     * the matching fields
     */
    private HashMap matchFields; 

    /**
     * The default constructor
     */
    public SbmeMatchRecord() {
       matchFields = new HashMap();
    }
     
    /**
     * Returns a String representation of the value stored in the field
     *
     * @param  key the value associated with the matching field 
     * @return the value of the matching key
     */
    public String getMatchKeyValue(String key) {
        return (String) matchFields.get(key);
    }

    /**
     * Sets the value of the matching field specified by the 'key' argument
     *
     * @param key the matching field's name
     * @param value the corresponding matching value
     */
    public void setMatchKeyValue(String key, String value) {
        matchFields.put(key, value);
    }

    /**
     * Returns a list of values associated with one specifc matching field
     *
     * @param  key the value associated with the matching field 
     * @return an ArrayList of all the field's value
     */
    public List getMatchKeyValues(String key) {
        return (ArrayList) matchFields.get(key);
    }

    /**
     * Sets the multiple values associated with one matching field 
     *
     * @param key the match value
     * @param values an ArrayList instance representing all the values
     */
    public void setMatchKeyValues(String key, List values) {
        matchFields.put(key, values);
    }
    
    /**
     * Returns all the matching fields' names associated with one match object
     *
     * @param matchObject the SbmeMatchRecord instance
     * @return the set of all the SbmeMatchRecord object's keys
     */
    public Set getAllMatchFields(SbmeMatchRecord matchObject) {
        return matchFields.keySet();
    }
}
