/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.stand.address;

/**
 * 
 * 
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
class ClueTable {

    // input name
    private StringBuffer name;
    // output name (translation)
    private StringBuffer translation;

    private StringBuffer clueWordId1;
    private StringBuffer clueType1;

    private StringBuffer clueWordId2;
    private StringBuffer clueType2;

    private StringBuffer clueWordId3;
    private StringBuffer clueType3;

    private StringBuffer clueWordId4;
    private StringBuffer clueType4;

    private StringBuffer clueWordId5;
    private StringBuffer clueType5;
    // state code
    private StringBuffer state;
    // county code
    private StringBuffer county;
    // place code
    private StringBuffer place;
    // zip code
    private StringBuffer zip;
    // minimum comparator score
    private StringBuffer minComparScore;
    // storage flag ('*' or ' ')
    private char storeFlag;
    private char language;
    // usage flag ('B' = com/res, ' ' = both
    private char usageFlag;
    // pointer to the next element in the linked list
    private ClueTable next;

    /**
     * Default constructor 
     */
    ClueTable() {
    
       name = new StringBuffer();
       translation = new StringBuffer();

       clueWordId1 = new StringBuffer();
       clueType1 = new StringBuffer();
       clueWordId2 = new StringBuffer();
       clueType2 = new StringBuffer();
       clueWordId3 = new StringBuffer();
       clueType3 = new StringBuffer();
       clueWordId4 = new StringBuffer();
       clueType4 = new StringBuffer();
       clueWordId5 = new StringBuffer();
       clueType5 = new StringBuffer();

       state = new StringBuffer();
       county = new StringBuffer();
       place = new StringBuffer();
       zip = new StringBuffer();
       minComparScore = new StringBuffer();
    }
    
    
    /**
     * get the name
     * @return the name
     */
    StringBuffer getName() {
        return name;
    }
    
    /**
     * get the name
     * @return the name
     */
    StringBuffer getTranslation() {
        return translation;
    }
    
    /**
     * get the name
     * @return the name
     */
    StringBuffer getClueWordId1() {
        return clueWordId1;
    }
    
    /**
     * get the name
     * @return the name
     */
    StringBuffer getClueType1() {
        return clueType1;
    }
    
    /**
     * get the name
     * @return the name
     */
    StringBuffer getClueWordId2() {
        return clueWordId2;
    }
    
    /**
     * get the name
     * @return the name
     */
    StringBuffer getClueType2() {
        return clueType2;
    }
    
    /**
     * get the name
     * @return the name
     */
    StringBuffer getClueWordId3() {
        return clueWordId3;
    }
    
    /**
     * get the name
     * @return the name
     */
    StringBuffer getClueType3() {
        return clueType3;
    }
    
    /**
     * get the name
     * @return the name
     */
    StringBuffer getClueWordId4() {
        return clueWordId4;
    }
    
    /**
     * get the name
     * @return the name
     */
    StringBuffer getClueType4() {
        return clueType4;
    }
    
    /**
     * get the name
     * @return the name
     */
    StringBuffer getClueWordId5() {
        return clueWordId5;
    }
    
    /**
     * get the name
     * @return the name
     */
    StringBuffer getClueType5() {
        return clueType5;
    }
    
    /**
     * get the name
     * @return the name
     */
    StringBuffer getState() {
        return state;
    }
    
    /**
     * get the name
     * @return the name
     */
    StringBuffer getCounty() {
        return county;
    }
    
    /**
     * get the name
     * @return the name
     */
    StringBuffer getPlace() {
        return place;
    }
    
    /**
     * get the name
     * @return the name
     */
    StringBuffer getZip() {
        return zip;
    }
    
    /**
     * get the name
     * @return the name
     */
    StringBuffer getMinComparScore() {
        return minComparScore;
    }
    
    /**
     * get the name
     * @return the name
     */
    char getStoreFlag() {
        return storeFlag;
    }
    
    /**
     * get the name
     * @return the name
     */
    char getLanguage() {
        return language;
    }
    
    /**
     * get the name
     * @return the name
     */
    char getUsageFlag() {
        return usageFlag;
    }
    
    /**
     * get the name
     * @return the name
     */
    ClueTable getNext() {
        return next;
    }



    /**
     * set the name 
     * @param name the name
     * @return the name
     */
    StringBuffer setName(StringBuffer  name) {
        return this.name = name;
    }
    
    /**
     * set the name 
     * @param translation the translation
     * @return the name
     */
    StringBuffer setTranslation(StringBuffer  translation) {
        return this.translation = translation;
    }

    /**
     * set the name 
     * @param clueWordId1 the clueWordId1
     * @return the name
     */
    StringBuffer setClueWordId1(StringBuffer  clueWordId1) {
        return this.clueWordId1 = clueWordId1;
    }

    /**
     * set the name 
     * @param clueType1 the name
     * @return the name
     */
    StringBuffer setClueType1(StringBuffer  clueType1) {
        return this.clueType1 = clueType1;
    }

    /**
     * set the name 
     * @param clueWordId2 the name
     * @return the name
     */
    StringBuffer setClueWordId2(StringBuffer  clueWordId2) {
        return this.clueWordId2 = clueWordId2;
    }

    /**
     * set the name 
     * @param clueType2 the name
     * @return the name
     */
    StringBuffer setClueType2(StringBuffer  clueType2) {
        return this.clueType2 = clueType2;
    }

    /**
     * set the name 
     * @param clueWordId3 the name
     * @return the name
     */
    StringBuffer setClueWordId3(StringBuffer  clueWordId3) {
        return this.clueWordId3 = clueWordId3;
    }

    /**
     * set the name 
     * @param clueType3 the name
     * @return the name
     */
    StringBuffer setClueType3(StringBuffer  clueType3) {
        return this.clueType3 = clueType3;
    }

    /**
     * set the name 
     * @param clueWordId4 the name
     * @return the name
     */
    StringBuffer setClueWordId4(StringBuffer  clueWordId4) {
        return this.clueWordId4 = clueWordId4;
    }

    /**
     * set the name 
     * @param clueType4 the name
     * @return the name
     */
    StringBuffer setClueType4(StringBuffer  clueType4) {
        return this.clueType4 = clueType4;
    }

    /**
     * set the name 
     * @param clueWordId5 the name
     * @return the name
     */
    StringBuffer setClueWordId5(StringBuffer  clueWordId5) {
        return this.clueWordId5 = clueWordId5;
    }

    /**
     * set the name 
     * @param clueType5 the name
     * @return the name
     */
    StringBuffer setClueType5(StringBuffer  clueType5) {
        return this.clueType5 = clueType5;
    }

    /**
     * set the name 
     * @param state the name
     * @return the name
     */
    StringBuffer setState(StringBuffer  state) {
        return this.state = state;
    }

    /**
     * set the name 
     * @param county the name
     * @return the name
     */
    StringBuffer setCounty(StringBuffer  county) {
        return this.county = county;
    }

    /**
     * set the 
     * @param place the place
     * @return the 
     */
    StringBuffer setPlace(StringBuffer  place) {
        return this.place = place;
    }

    /**
     * set the zip code
     * @param zip the zip code
     * @return the the zip code
     */
    StringBuffer setZip(StringBuffer  zip) {
        return this.zip = zip;
    }

    /**
     * set the 
     * @param minComparScore the
     * @return the 
     */
    StringBuffer setMinComparScore(StringBuffer minComparScore) {
        return this.minComparScore = minComparScore;
    }

    /**
     * set the 
     * @param ch the store flag
     * @return the 
     */
    char setStoreFlag(char ch) {
        return this.storeFlag = ch;
    }

    /**
     * set the usage flag
     * @param ch the 
     * @return the usage flag
     */
    char setUsageFlag(char ch) {
        return this.usageFlag = ch;
    }

    /**
     * set the language
     * @param language the 
     * @return the language
     */
    char setLanguage(char language) {
        return this.language = language;
    }

    /**
     * set the 
     * @param next the 
     * @return the 
     */
    ClueTable setNext(ClueTable next) {
        return this.next = next;
    }
}
