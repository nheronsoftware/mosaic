/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.stand.businessname;

import java.util.ArrayList;
import java.util.StringTokenizer;

import com.stc.sbme.api.SbmeBusinessName;
import com.stc.sbme.api.SbmeStandardizationException;
import com.stc.sbme.stand.util.TableSearch;
import com.stc.sbme.util.EmeUtil;

/**
 * Main class to process any request to match candidate to reference
 * records
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
public class BusinessNameNormalizer {
    
    /*
     * These fields should be replaced with the chosed fields that define
     * a business
     */
    private static final String PRIMARY_NAME = SbmeBusinessName.PRIMARY_NAME;
    private static final String ORG_TYPE_KEY = SbmeBusinessName.ORG_TYPE_KEY;
    private static final String ASSOC_TYPE_KEY = SbmeBusinessName.ASSOC_TYPE_KEY;
    private static final String INDUSTRY_TYPE_KEY = SbmeBusinessName.INDUSTRY_TYPE_KEY;
    private static final String ALIAS_LIST = SbmeBusinessName.ALIAS_LIST;
    private static final String URL_NAME = SbmeBusinessName.URL_NAME;
    private static final String INDUSTRY_SECTOR_LIST = SbmeBusinessName.INDUSTRY_SECTOR_LIST;
    
    /**
     * A factory method that returns an instance of the class
     * @return a BusinessNameNormalizer instance
     */
    public static BusinessNameNormalizer getInstance() {
        return new BusinessNameNormalizer();
    }
    
    
    /**
     * This method is used in situations where we receive an already parsed
     * objects to normalize.
     *
     * @param fieldKey the different fields contained inside the object. We
     *                 can get them by using the method getAllFields in
     *                 SbmeBusinessName class.
     * @param fieldValue the value of the fields
     * @param sVar an instance of the BusinessStandVariables class that holds
     *             the shared variables in all the classes
     * @return an array of normalized fields
     * @throws SbmeStandardizationException a stand exception
     */
    public String[] performFieldsNormalization(String[] fieldKey, String[] fieldValue,
                                               BusinessStandVariables sVar)
        throws SbmeStandardizationException {
        
        /* The number of tokens */
        int numberKeys = fieldKey.length;
        
        int i;
        int index;
        
        /* The array of strings that hold the normalized fields */
        String[] normFields = new String[numberKeys];
        
        
       /*
        * For every field, search for the corresponding standard field
        * Then, update the field
        */
        for (i = 0; i < numberKeys; i++) {
            
            /* First, search if the field's key is a company's name */
            if (fieldKey[i].compareTo(PRIMARY_NAME) == 0) {
                
                /* Call the corresponding table */
                
            } else if (fieldKey[i].compareTo(ORG_TYPE_KEY) == 0) {
                
                /* Call the corresponding table */
            }
        }
        return normFields;
    }
    
    
    /**
     * This method is used in situations where we receive a raw string
     *
     * @param standFields holds the standardized fields
     * @param sVar an instance of the BusinessStandVariables class that holds
     *             the shared variables in all the classes
     * @return an ArrayList of normalized fields
     * @throws SbmeStandardizationException a stand exception
     */
    public ArrayList performStringNormalization(ArrayList standFields, 
                                                BusinessStandVariables sVar, int ind)
        throws SbmeStandardizationException {
 
        int i;
        int len;
        ArrayList fieldsType;

        fieldsType = searchForTypes(standFields, sVar, ind);

        /* implementation */
        return fieldsType;
    }
    
    
    /**
     * 
     * 
     *
     * @param ind the index inside the list of tokens
     * @param token the string representation of the token
     */
    private ArrayList searchForTypes(ArrayList fieldsList, BusinessStandVariables sVar, int ind)
        throws SbmeStandardizationException {
            
        // The index inside the tables
        int index;
        int index1;
        int indexT;

        int i;
        int j = 0;
        int k = 0;
        int len;
        int len2;
        boolean ind2Tok;
        boolean notStand;
        boolean prev2Tok = false;
        String clStr;
        String clStr2 = "";                
        StringBuffer it = new StringBuffer();
        it.setLength(1);
        
        String token;
        StringBuffer token2 = new StringBuffer();
        
        sVar.incompAliasFlag = false;
        
        // Holds the types of the tokens inside a merger token.
        StringBuffer mergeTypes;
        
        /* Total number of fields inside arrayFields */
        int numberOfFields = fieldsList.size();
        
        // Holds the types of the fields
        ArrayList fieldsType = new ArrayList(numberOfFields);
        
        sVar.aliasList.clear();
        sVar.aliasListType[ind].clear();
        sVar.industrySectorList.clear();

        // Mapp the Alias ArrayList to the HashMap that it holds
        sVar.aliasList.add(0, sVar.aliasListType[ind]);

       
        /* Loop over each token and search for its type(s) if not already found */
        for (i = 0; i < numberOfFields; i++) {

            index = -1;
            index1 = -1; 
            
            // The ith token 
            token = (String) fieldsList.get(i);
            
            // Reset the variable to zero
            it.setCharAt(0, '2');
            ind2Tok = false;
            notStand = false;

            // Check if the field contains diacretical marks. 
            // Remove them when searching the tables
            clStr = EmeUtil.removeDiacriticalMark(token); 
            token2.setLength(0);
            
            if (i < numberOfFields - 1) {
                token2.append(token);                  
                token2.append(" ").append((String) fieldsList.get(i + 1));
                ind2Tok = true;
                // Check if the field contains diacretical marks. 
                // Remove them when searching the tables
                clStr2 = EmeUtil.removeDiacriticalMark(token2.toString());                 
            } 

            // The length of the ith token 
            len = token.length();

            /*
             * Search for the different types of the keywords and eventually
             * replace them with the standard equivalents
             * Search only for tokens that are more than one-character long.
             */
            if (len > 1 || Character.isLetter(token.toCharArray()[0])) { 


                /* Search for tokens in the list of connector tokens */
                if ((index = TableSearch.searchTable(clStr, 1, sVar.nbConnectorTokens,
                    sVar.connectorToken)) >= 0) {


                    // Update the connector tokens type flag
                    sVar.connectorTokensFlag[i] = true;
        
                    // Update the list that holds the different types of the tokens
                    fieldsType.add(i, "CTT");
                    continue;
                }
                
                // Special consideration for token AND.
                if (token.compareTo("AND") == 0) { 
                    fieldsType.add(i, "GLU");
                    continue;
                }
                
                /* Search for tokens in the list of two-token company's primary names */
                if (ind2Tok && (index = TableSearch.searchTable(clStr2, 1, sVar.nbPrimaryNames, sVar.primaryName)) >= 0) {
          
                    // Read the list of industry categories associated with the field
                    sVar.industrySectorList.add(0, sVar.industrySector1[index]);

                    // Search for any corresponding acronym  
                    if ((index = sVar.aliasTypeStandField.indexOf(clStr2)) >= 0) {
                    
                        // Read the company acronym
                        sVar.aliasListType[ind].put("CompanyAcronym",
                        sVar.aliasTypeField.get(index));
                        
                        sVar.incompAliasFlag = true;
                    }              
                    
                    // Search for the company former name
                    if ((index = sVar.companyNewName.indexOf(clStr2)) >= 0) {
                    
                        // Read the company acronym
                        sVar.aliasListType[ind].put("CompanyFormerName",
                        sVar.companyFormerName.get(index));
                        
                        sVar.incompAliasFlag = true;
                    }
                    
                    // Update the industry type flag 
                    sVar.primaryNameFlag = true;
        
                    // Update the list that holds the different types of the tokens
                    fieldsType.add(i, "PNT");  

                    // Update all the tokens' flags 
                    updateFlags(i, 2, numberOfFields, sVar);                     
                    
                    fieldsList.remove(i);
//FIXME: added to avoid exception
if (i < fieldsList.size())                    
                    fieldsList.remove(i);        
                    fieldsList.add(i, token2.toString());
                    --numberOfFields; 
                               
                    continue;

                /* Search for tokens in the list of one-token company's primary names */
                } else if ((index = TableSearch.searchTable(clStr, 1, sVar.nbPrimaryNames, sVar.primaryName)) >= 0) {
          
                    // Read the list of industry categories associated with the field
                    sVar.industrySectorList.add(0, sVar.industrySector1[index]);

                    // Search for any corresponding acronym  
                    if ((index = sVar.aliasTypeStandField.indexOf(clStr)) >= 0) {
                    
                        // Read the company acronym
                        sVar.aliasListType[ind].put("CompanyAcronym",
                        sVar.aliasTypeField.get(index));
                        
                        sVar.incompAliasFlag = true;
                    }                   
                    
                    // Search for the company former name
                    if ((index = sVar.companyNewName.indexOf(clStr)) >= 0) {
                    
                        // Read the company acronym
                        sVar.aliasListType[ind].put("CompanyFormerName",
                        sVar.companyFormerName.get(index));
                        
                        sVar.incompAliasFlag = true;
                    }
                    
                    // Update the industry type flag 
                    sVar.primaryNameFlag = true;
        
                    // Update the list that holds the different types of the tokens
                    fieldsType.add(i, "PNT");
                    
                    continue;
                    
                } else if (!sVar.primaryNameFlag && (index = TableSearch.searchTable(clStr2.toString(), 1, 
                        sVar.nbPrimarySlashNames, sVar.primarySlashName)) >= 0) {
                    // Read the list of industry categories associated with the field
                    sVar.industrySectorList.add(0, sVar.industrySector2[index]);
            
                    // Search for any corresponding acronym
                    if ((index = sVar.aliasTypeStandField.indexOf(clStr2)) >= 0) {

                        // Read the company acronym
                        sVar.aliasListType[ind].put("CompanyAcronym",
                        sVar.aliasTypeField.get(index));
                
                        sVar.incompAliasFlag = true;
                    }

                    // Search for the company former name
                    if ((index = sVar.companyNewName.indexOf(clStr2)) >= 0) {
            
                        // Read the company acronym
                        sVar.aliasListType[ind].put("CompanyFormerName",
                        sVar.companyFormerName.get(index));
                
                        sVar.incompAliasFlag = true;
                    }

                    // Update the industry type flag
                    sVar.mergerIndexFlag[i] = true;

                    // Update the list that holds the different types of the tokens
                    fieldsType.add(i, "PN-PN");
                    
                    // Update all the tokens' flags 
                    updateFlags(i, 2, numberOfFields, sVar);                     
                    
                    fieldsList.remove(i);
//FIXME: added to avoid exception
if (i < fieldsList.size())                   
                    fieldsList.remove(i);        
                    fieldsList.add(i, token2.toString());
                    --numberOfFields;                     
                    
                    continue;
                // Scan the list of merged companies
                } else if (!sVar.primaryNameFlag && (index = TableSearch.searchTable(clStr, 1, 
                        sVar.nbPrimarySlashNames, sVar.primarySlashName)) >= 0) {

                    // Read the list of industry categories associated with the field
                    sVar.industrySectorList.add(0, sVar.industrySector2[index]);

                    // Search for any corresponding acronym
                    if ((index = sVar.aliasTypeStandField.indexOf(clStr)) >= 0) {

                        // Read the company acronym
                        sVar.aliasListType[ind].put("CompanyAcronym",
                        sVar.aliasTypeField.get(index));
                        
                        sVar.incompAliasFlag = true;
                    }


                    // Search for the company former name
                    if ((index = sVar.companyNewName.indexOf(clStr)) >= 0) {
                    
                        // Read the company acronym
                        sVar.aliasListType[ind].put("CompanyFormerName",
                        sVar.companyFormerName.get(index));
                        
                        sVar.incompAliasFlag = true;
                    }


		            // Update the industry type flag
                    sVar.mergerIndexFlag[i] = true;
        
                    // Update the list that holds the different types of the tokens
                    fieldsType.add(i, "PN-PN");
                    continue;
                }

                // Search for business common terms
                index = sVar.bizCommonTerm.indexOf(clStr);

                if (index >= 0) {

                    // Update the list that holds the different types of the tokens
                    fieldsType.add(i, "BCT");
                    continue;
                }


                // If it is a url
                if (sVar.urlTypeFlag[i]) { 
                
                    // Update the list that holds the different types of the tokens
                    fieldsType.add(i, "URL");
                    continue;
                }

                
                /* Search for tokens in the list of acronyms */
                if (!sVar.primaryNameFlag && (index = sVar.aliasTypeField.indexOf(clStr)) >= 0) {
                    
                    // Create a local arraylist
                    ArrayList al = new ArrayList();
                    
                    /* Account for all the names that share this specific acronym */
                    for (j = index; j <= sVar.aliasTypeField.lastIndexOf(clStr); j++){
                        al.add(0, sVar.aliasTypeStandField.get(j));
                    }

                    // Append the list of companies' names to the HashMap
                    sVar.aliasListType[ind].put("CompanyListOfAcronym", al);

                    fieldsList.set(i, token);
        
                    // Update the organization type flag 
                    sVar.aliasTypeKeyFlag[i] = true;
                    sVar.incompAliasFlag = true;
        
                    // Update the list that holds the different types of the tokens
                    fieldsType.add(i, "ALT");

                    continue;
                }


                /* Search for tokens in the list of location-related words */
                index = sVar.countryStandName.indexOf(clStr);

                if (index >= 0) {

                    // Update the organization type flag
                    sVar.countryTypeKeyFlag[i] = true;

                    // Update the list that holds the different types of the tokens
                    fieldsType.add(i, "CNT");
                    continue;
                }

                index = sVar.nationality.indexOf(clStr);

                if (index >= 0) {

                    // Update the organization type flag
                    sVar.nationalityTypeKeyFlag[i] = true;

                    // Update the list that holds the different types of the tokens
                    fieldsType.add(i, "NAT");
                    continue;
                }

                // Search for cities or states
                index = sVar.cityOrStateName.indexOf(clStr);

                if (index >= 0) {

                    // Update the list that holds the different types of the tokens
                    fieldsType.add(i, "CST");
                    continue;
                }

                // Search for cities or states
                index = sVar.cityOrStateName.indexOf(clStr2);

                if (index >= 0) {

                    // Update the list that holds the different types of the tokens
                    fieldsType.add(i, "CST");
                    fieldsType.add(++i, "CST");

                    continue;
                }


                /* Search for the token in the list of adjective words */
                if ((index = TableSearch.searchTable(clStr, 1, sVar.nbAdjTypeFields,
                    sVar.adjTypeField)) >= 0) {
        
                    // Update the organization type flag
                    sVar.adjTypeKeyFlag[i] = true;
                }
               
              
                /* Search for the token in the list of industry-related words */
                if (ind2Tok && ((index1 = sVar.industryTypeField.indexOf(clStr2.toString())) >= 0)) {
                        
					it.setCharAt(0, '1');
                } else if  ((index = sVar.industryTypeField.indexOf(clStr)) >= 0) {
                    it.setCharAt(0, '0');
                }
				
				indexT = Math.max(index, index1);                

                if (indexT >= 0) {         
        
                    /* Check if there is a standard form for this token */                 	            
                    if (sVar.industryTypeStandField[indexT].compareTo("0") != 0) {                 
                        fieldsList.set(i, sVar.industryTypeStandField[indexT]);
                        
                    } else if (it.charAt(0) == '1') {
                        notStand = true;
                    }     	
                    
                    // Read the list of industry categories associated with the field
                    if (!sVar.primaryNameFlag && !sVar.mergerIndexFlag[i]){
                    
                        StringTokenizer st = new StringTokenizer(sVar.industrySector[indexT].toString());
                        len2 = st.countTokens();
                   
                        if (len2 == 1){
                            sVar.industrySectorList.add(0, st.nextToken());

                        } else if (len2 > 1){
                            for (j = 0; j < len2; j++) {
                                sVar.industrySectorList.add(0, st.nextToken());
                            }
                        }
                    }            

        
                    // Update the organization type flag
                    sVar.industryTypeKeyFlag[i] = true;
                    sVar.incompAliasFlag = true;
        
                    // If there is an overlaping between the two different types
                    // (industry and adjective), then use a type that account for both of them
                    if (sVar.adjTypeKeyFlag[i]) { 
                        fieldsType.add(i, "IDT-AJT");
                        
                    } else {
                            fieldsType.add(i, "IDT");      
                    }
                    
                    // Re-ajust the number of tokens when a industry type 
                    // is more than one token
                    if (it.charAt(0) == '1') {                                              

                        // Update all the tokens' flags 
                        updateFlags(i, 2, numberOfFields, sVar);                         
                        
                        if (notStand) {                                               
                            fieldsList.remove(i); 
                            fieldsList.remove(i); 
                            fieldsList.add(i, token2.toString());
                            prev2Tok = true;
              
                        } else {                                                                     
                            fieldsList.remove(i + 1); 
                            prev2Tok = false;                     
                        }
                        --numberOfFields;
                    }   
                                  
                    continue;
                }
                
                // After making sure that the type is not a "IDT-AJT", 
                // we update the adjective type
                if (sVar.adjTypeKeyFlag[i]) { 
                    fieldsType.add(i, "AJT");
                    continue;
                }


                /* Search for the token in the list of association-type words */
                if ((index = TableSearch.searchTable(clStr, 1, sVar.nbAssocTypeFields,
                    sVar.assocTypeField)) >= 0) {
        
                    /* Check if there is a standard form for this token */
                    if (sVar.assocTypeStandField[index].compareTo("0") != 0) {
        
                        fieldsList.set(i, sVar.assocTypeStandField[index]);
                    }
        
                    // Update the organization type flag
                    sVar.assocTypeKeyFlag[i] = true;
                    sVar.incompAliasFlag = true;

                    // Update the list that holds the different types of the tokens
                    fieldsType.add(i, "AST");

                    continue;
                }
                

                /* Search for tokens in the list of two-token organization-type keyword */
                if (ind2Tok && (index = TableSearch.searchTable(clStr2, 1, sVar.nbOrgTypeFields, sVar.orgTypeField)) >= 0) {
          
                    /* Check if there is a standard form of this token */
                    if (sVar.orgTypeStandField[index].compareTo("0") != 0) {
        
                        fieldsList.set(i, sVar.orgTypeStandField[index]);
                    }


                    // Update the organization type flag
                    sVar.orgTypeKeyFlag[i] = true;
                    sVar.incompAliasFlag = true;

                    // Update the list that holds the different types of tokens
                    fieldsType.add(i, "ORT");
                    
                    // Update all the tokens' flags 
                    updateFlags(i, 2, numberOfFields, sVar); 
                    
                    fieldsList.remove(i); 
                    fieldsList.remove(i);        
                    fieldsList.add(i, token2.toString());
                    --numberOfFields;                     
                    continue;
                    
                /* Search for the token as an organization-type keyword */
                } else if ((index = TableSearch.searchTable(clStr, 1, sVar.nbOrgTypeFields,
                    sVar.orgTypeField)) >= 0) {
        
                    /* Check if there is a standard form of this token */
                    if (sVar.orgTypeStandField[index].compareTo("0") != 0) {
        
                        fieldsList.set(i, sVar.orgTypeStandField[index]);
                    }
        
                    // Update the organization type flag
                    sVar.orgTypeKeyFlag[i] = true;
                    sVar.incompAliasFlag = true;

                    // Update the list that holds the different types of tokens
                    fieldsType.add(i, "ORT");
                    
                    continue;
                }


                /*
                 * If the primary name is not found, then search for it in the table
                 of former names
                 */
                 if (!sVar.primaryNameFlag) {
             
                    /* Search for the token as an the company former name */
                    if ((index = sVar.companyFormerName.indexOf(clStr)) >= 0) {

                        // Read the list of industry categories associated with the field
                        sVar.formerName = new String((String) sVar.companyFormerName.get(index));
 

                        // Search for any corresponding acronym
                        if ((index = sVar.aliasTypeStandField.indexOf(clStr)) >= 0) {
                    
                            // Read the company acronym
                            sVar.aliasListType[ind].put("CompanyAcronym",
                            sVar.aliasTypeField.get(index));
                            
                            sVar.incompAliasFlag = true;
                        }


                        // Search for the present company name
                        if ((index = sVar.companyFormerName.indexOf(clStr)) >= 0) {
                    
                            // Read the company acronym
                            sVar.aliasListType[ind].put("CompanyPresentName",
                            sVar.companyNewName.get(index));
                            
                            sVar.incompAliasFlag = true;
                        }

                        // Update the industry type flag
                        sVar.formerNameFlag = true;
                    }
                 }

                // If it is a double dash separator
                if (sVar.separatorCharKeyFlag[i]) {
                
                    // Update the list that holds the different types of tokens
                    fieldsType.add(i, "SEP");
                    continue;
                }
                                
                // If the type is not found, but a slash characters exists within the token
                if (sVar.mergerIndexFlag[i]) {

                    // First search for the types of merged tokens
                    mergeTypes = searchForMergedTokenTypes(i, clStr, sVar);
                    
                    fieldsType.add(i, mergeTypes.toString());
                    continue;
                }
 
 
                // If the type is not found, but a dash characters exists within the token
                if (sVar.glueCharKeyFlag[i]) {
                    
                    // Append a not found glue keyword
                    fieldsType.add(i, "NFG");
                    sVar.incompAliasFlag = true;

                    continue;
                }


                // Finally, we assign a simple not found keyword
                if (token.length() == 1) {
                    // Finally, we assign a simple not found keyword
                    fieldsType.add(i, "NFC");
                } else {             
                    fieldsType.add(i, "NF");
                }
                sVar.incompAliasFlag = true;


            // The token field is a special character
            } else if (len == 1) {
            
                // If the token is a comma
                if (sVar.commaFlag[i]) {
                    
                    // Append the different types associated with the comma character
                    fieldsType.add(i, "SEP-GLC");
                    continue;
                }
                
                // If the token is a dash
                if (sVar.dashFlag[i]) {
                    
                    // Append the different types associated with the comma character
                    fieldsType.add(i, "SEP-GLD");
                    continue;
                }
 
                // If the token is a slash
                if (sVar.slashFlag[i]) {
                
                    // Append the different types associated with the comma character
                    fieldsType.add(i, "GLU-MRG");
                    continue;
                }
 
                // If the token is an ampersand 
                if (sVar.ampersandTypeFlag[i]) {

                    /* Search for tokens in the list of two-token organization-type keyword */
                    if (ind2Tok && (index = TableSearch.searchTable(clStr2, 1, sVar.nbAssocTypeFields, 
                        sVar.assocTypeField)) >= 0) {        
                        
                        // Update the organization type flag
                        sVar.assocTypeKeyFlag[i] = true;
                        sVar.incompAliasFlag = true;

                        // Update the list that holds the different types of tokens
                        fieldsType.add(i, "AST");
                    
                        // Update all the tokens' flags 
                        updateFlags(i, 2, numberOfFields, sVar); 
                    
                        fieldsList.remove(i); 
                        fieldsList.remove(i);   
                        
                        /* Check if there is a standard form of this token */
                        if (sVar.assocTypeStandField[index].compareTo("0") != 0) {        
                            fieldsList.add(i, sVar.assocTypeStandField[index]); 
                            
                        } else {
                            fieldsList.add(i, token2.toString());                      
                        }                 
                        --numberOfFields;                     
                        continue;                        
                    }                    
                                      
                    // Append the different types associated with the ampersand character
                    fieldsType.add(i, "GLU");

                    continue;
                }
                
                // Finally, we assign a simple not found keyword
                fieldsType.add(i, "NFC");
            }            
        }  
        
        len = sVar.industrySectorList.size();       
        // make sure that every element in the industrySectorList is unique
        for (i = 0; i < len - 1; i++) {
            for (k = i + 1; k < len; k++) {

                if
                (sVar.industrySectorList.get(i).equals(sVar.industrySectorList.get(k)))
                {
                    sVar.industrySectorList.remove(k);
                    len--;
                }
            }
        }

        for (i = 0; i < len; i++) {

            // Convert the industry sector code to the a meaningfull text representation
            index = sVar.categoryCode.indexOf(sVar.industrySectorList.get(i).toString());

            if (index  >= 0) {
                sVar.industrySectorList.set(i, sVar.categoryName.get(index));
            }
        }

        return fieldsType;
    }
                
                
    /**
     * 
     *
     * @param ind the index inside the list of tokens
     * @param token the string representation of the token
     */
    private StringBuffer searchForMergedTokenTypes(int ind, String token, 
                                                   BusinessStandVariables sVar) {
    
        StringTokenizer stok;
        int numberOfSubtokens;
        int index;
        int i;
        
        StringBuffer names = new StringBuffer();
        
        /* 
         * Before searching inside the token for business names, we first search
         * for the entire token inside the table of business primary names with full.
         */
        if ((index = TableSearch.searchTable(token, 1, sVar.nbPrimarySlashNames,
            sVar.primarySlashName)) >= 0) {

            return new StringBuffer("PNT");
        }


        /* Parse the string and search for special characters that are considered as delimiters */
        stok = new StringTokenizer(token, "/");
        
        /* Count the number of fields inside the string */ 
        numberOfSubtokens = stok.countTokens();
        
        
        for (i = 0; i < numberOfSubtokens; i++) {
            
            // search for the different subtokens
            token = stok.nextToken();
            
            /* Search for tokens in the list of  words */
            if ((index = TableSearch.searchTable(token, 1, sVar.nbPrimaryNames,
                sVar.primaryName)) >= 0) {
    
                // search for the second subtokens
                if (stok.hasMoreTokens()) {
                    token = stok.nextToken(); 
                    i++; 
                }
                /* Search for tokens in the list of words */ 
                if ((index = TableSearch.searchTable(token, 1, sVar.nbPrimaryNames,
                    sVar.primaryName)) >= 0) {
    
                    // 
                    if (names.length() == 0) {
                        names.append("PN-PN");
                    }                    
                    continue;
                } else {
                    // 
                    if (names.length() == 0) {                    
                        names.append("PN-NF"); 
                    }                   
                }                                
                
            } else {
            
                // search for the second subtokens
                if (stok.hasMoreTokens()) {
                    token = stok.nextToken(); 
                    i++; 
                }
            
                /* Search for tokens in the list of location-related words */ 
                if ((index = TableSearch.searchTable(token, 1, sVar.nbPrimaryNames,
                    sVar.primaryName)) >= 0) {
    
                    // 
                    if (names.length() == 0) {                    
                        names.append("NF-PN");
                    }                 
                    continue;
                } else {
                    if (names.length() == 0) {                    
                        names.append("NF-NF");
                    }
                }
            }
        }
        return names;
    }
    
    static void updateFlags(int i, int tokensNum, int length, BusinessStandVariables sVar) 
        throws SbmeStandardizationException {
        int k;
        int j;

        if (tokensNum == 2) {
            j = i + 1;
        } else if (tokensNum == 3) {
            j = i + 2;
        } else {
            throw new SbmeStandardizationException("The first parameter of updateFlags must be"
                    + " eight 2 or 3");
        }
                
        for (; j < length - 1; j++) {
            if (tokensNum == 2) {        
                k = j + 1;
            } else {
                k = j + 2;
            }
            
            sVar.orgTypeKeyFlag[j] = sVar.orgTypeKeyFlag[k];        
            sVar.assocTypeKeyFlag[j] = sVar.assocTypeKeyFlag[k]; 
            sVar.locationTypeKeyFlag[j] = sVar.locationTypeKeyFlag[k]; 
            sVar.industryTypeKeyFlag[j] = sVar.industryTypeKeyFlag[k];
            sVar.unfoundTypeKeyFlag[j] = sVar.unfoundTypeKeyFlag[k];        
            sVar.adjTypeKeyFlag[j] = sVar.adjTypeKeyFlag[k]; 
            sVar.nationalityTypeKeyFlag[j] = sVar.nationalityTypeKeyFlag[k]; 
            sVar.aliasTypeKeyFlag[j] = sVar.aliasTypeKeyFlag[k];                  
            sVar.urlTypeFlag[j] = sVar.urlTypeFlag[k];        
            sVar.ampersandTypeFlag[j] = sVar.ampersandTypeFlag[k]; 
            sVar.commaFlag[j] = sVar.commaFlag[k]; 
            sVar.separatorCharKeyFlag[j] = sVar.separatorCharKeyFlag[k]; 
            sVar.glueCharKeyFlag[j] = sVar.glueCharKeyFlag[k]; 
            sVar.mergerIndexFlag[j] = sVar.mergerIndexFlag[k];                         
        }
    }         
}
