/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.stand.address;
import com.stc.sbme.util.EmeUtil;

/**
 * Searches the master clue table for the type id and returns the translation in the
 * requested form. Valid abbreviations: * 'F' - Full; 'S' - Standard; 'H' - Short; 
 * 'U' - USPS
 *
 * @author Sofiane Ouaguenouni 
 * @version $Revision: 1.1.2.1 $
 */
class StandIden {
    /**
     *
     * @param str the input string 
     * @param outstr the output string
     */
    protected static void searchStandIden(StringBuffer str, StringBuffer outstr, String domain) {
    
        int numlen, digit, digit1, digit2;
        int prevDigit, end, floor, strIdx;
        int level, len, lenFirst, lenFl, lenStr, lenStar, len2star, lenBefstar;
        StringBuffer star, star3, star2;
        StringBuffer temp = new StringBuffer(11);
        int typeIden = 0;
        int count = 0;
        int i = 0;
        int chr1Cnt = 0;
        int chr2Cnt = 0;
        int dig1Cnt = 0;
        int dig2Cnt = 0;
        int befStar, aftStar, midlen;
        boolean and = false;
        boolean iSkip = false;
        boolean iCheck = false;
        boolean iMore = true;
        // Make the different keywords defined in this method language-dependent
        String Fl; 
        String Lvl;
        String Fld; 
        String Lvld; 
        String rd;
        String RD;
        String nd;
        String ND;      
        String th;
        String TH;       
        String st;
        String ST;        
        String Side;     
        String Bsmt;        
        String Inte;       
        String Rear;                     
        String Botm;
        
        String Midl;
        String Lowr;
        String Uppr;
        String Frnt;
        String Down; 
                       
        String Left;
        String Rght;
        String Lbby;
        String Ofc;
        String Top;
        String PH;               
        String Up;    
        if (domain.compareTo("FR") == 0) {
            Fl = "Etg";
            Fld = "Etg-";
            Lvl = "Niv";
            Lvld = "Niv-";
            nd = "eme";
            ND = "EME";
            rd = "eme";
            RD = "EME";  
            th = "ere";
            TH = "ERE";
            st = "er";
            ST = "ERE";
             
            Side = "C�t�";
            Bsmt = "SSOL";
            Inte = "Inte";
            Rear = "ANNX";
            Botm = "BASSE";           
            Midl = "Milieu";
            Lowr = "BAS";
            Uppr = "HTE";
            Frnt = "Face";
            Down = "BASSE";            
            Left = "Gauche";
            Rght = "DRTE";
            Lbby = "Lbby";
            Ofc = "Bure";
            Top = "Ht";         
            PH = "TERR";
            Up = "HTS"; 
        } else {
            Fl = "Fl";
            Fld = "Fl-";
            Lvl = "Lvl";
            Lvld = "Lvl-";
            nd = "nd";
            ND = "ND"; 
            rd = "rd";
            RD = "RD";   
            th = "th";
            TH = "TH";
            st = "st";
            ST = "ST";    
            Side = "Side"; 
            Bsmt = "Bsmt";
            Inte = "Inte";
            Rear = "Rear";
            Botm = "Botm";        
            Midl = "Midl";
            Lowr = "Lowr";
            Uppr = "Uppr";
            Frnt = "Frnt";
            Down = "Down";            
            Left = "Left";
            Rght = "Rght";
            Lbby = "Lbby";
            Ofc = "Ofc";
            Top = "Top";         
            PH = "PH";
            Up = "Up";                                                                        
        }

        /* Make sure that the '*' is at least at index 2. SUBJECT TO REVIEW */
        if ((strIdx = EmeUtil.strchrInt(str, '*')) < 2) {
            outstr.setLength(0);
            outstr.append(str);
            return;
        }

        EmeUtil.strncpy(outstr, "         ", 9);
        // Determine length of input string and the first position of an asterisk
        //..  23nd*Fl*5 --> *Fl*5  ..
        star = EmeUtil.strchr(str, '*');
        
        if (star == null) {
            // Single character sent
            EmeUtil.strcpy(outstr, str);
            return;
        }
        
        floor = EmeUtil.strncmp(Fl, str, 2);
        level = EmeUtil.strncmp(Lvl, str, 3);

        if (floor == 0 || level == 0) {
            star2 = EmeUtil.strchr(star, 1, '*');
            if (star2 == null) {
                star2 = EmeUtil.strchr(star, '*');
                len2star = star2.length();
                EmeUtil.strncpy(temp, star2, 1, len2star - 1);
                temp.setCharAt(len2star - 1, '*');

                if (floor == 0) {
                    EmeUtil.strcpy(temp, len2star, Fl);
                }
                if (level == 0) {
                    EmeUtil.strcpy(temp, len2star, Lvl);
                }
                EmeUtil.strcpy(str, temp);
            } else {
                star2 = EmeUtil.strchr(star, 1, '*');
                if (star2 != null) {
                    len2star = star2.length();
                    lenStar = star.length();
                    lenStr = str.length();
                    midlen = lenStar - len2star;
                    lenFirst = lenStr - lenStar + 1;

                    EmeUtil.strncpy(temp, star, 1, midlen);
                    EmeUtil.strncpy(temp, midlen, str, lenFirst);
                    EmeUtil.strncpy(temp, midlen + lenFirst, star2, 1, 0);
                    EmeUtil.strcpy(str, temp);
                }
            }
        }       

        // Flips the input: 3*Fl4th ----->> to 4th*Fl*3
        floor = EmeUtil.strncmp(Fl, star, 1, 2);
        level = EmeUtil.strncmp(Lvl, star, 1, 3);
        star2 = EmeUtil.strchr(star, 1, '*');
        
        if (EmeUtil.strncmp(nd, str, strIdx - 2, 2) != 0
            && EmeUtil.strncmp(ND, str, strIdx - 2, 2) != 0 && EmeUtil.strncmp(rd, str, strIdx - 2, 2) != 0
            && EmeUtil.strncmp(RD, str, strIdx - 2, 2) != 0 && EmeUtil.strncmp(th, str, strIdx - 2, 2) != 0
            && EmeUtil.strncmp(TH, str, strIdx - 2, 2) != 0 && EmeUtil.strncmp(st, str, strIdx - 2, 2) != 0
            && EmeUtil.strncmp(ST, str, strIdx - 2, 2) != 0) {
            
            if (floor == 0) {
                if ((digit = Character.digit(star.charAt(4), 10)) != 0) {
                    lenStar = star.length();
                    lenStr = str.length();
                    lenFirst = lenStr - lenStar;
                    lenFl = lenStr - lenFirst - 4;

                    EmeUtil.strncpy(temp, star, 4, lenFl);
                    temp.setCharAt(lenFl, '*');
                    EmeUtil.strncpy(temp, lenFl + 1, star, 1, 2);
                    end = lenFl + 3;
                    temp.setCharAt(end, '*');
                    EmeUtil.strncpy(temp, end + 1, str, lenFirst);
                    EmeUtil.strcpy(str, temp);
                }
            } else if (level == 0) {
                if ((digit = Character.digit(star.charAt(5), 10)) != 0) {
                    lenStar = star.length();
                    lenStr = str.length();
                    lenFirst = lenStr - lenStar;
                    lenFl = lenStr - lenFirst - 5;

                    EmeUtil.strncpy(temp, star, 5, lenFl);
                    temp.setCharAt(lenFl, '*');
                    EmeUtil.strncpy(temp, lenFl + 1, star, 1, 2);
                    end = lenFl + 3;
                    temp.setCharAt(end, '*');
                    EmeUtil.strncpy(temp, end + 1, str, lenFirst);
                    EmeUtil.strcpy(str, temp);
                }
            }
        }

        // 3nd*Fl*5 --> *Fl*5
        star = EmeUtil.strchr(str, '*');
        lenStr = str.length();
        // Eg: Length of *Fl*5  .
        lenStar = star.length();           
        lenFirst = lenStr - lenStar;

        // If "Side" is contained in input string, handle separately
        if (EmeUtil.strncmp(nd, str, strIdx - 2, 2) != 0
            && EmeUtil.strncmp(ND, str, strIdx - 2, 2) != 0 && EmeUtil.strncmp(rd, str, strIdx - 2, 2) != 0
            && EmeUtil.strncmp(RD, str, strIdx - 2, 2) != 0 && EmeUtil.strncmp(th, str, strIdx - 2, 2) != 0
            && EmeUtil.strncmp(TH, str, strIdx - 2, 2) != 0 && EmeUtil.strncmp(st, str, strIdx - 2, 2) != 0
            && EmeUtil.strncmp(ST, str, strIdx - 2, 2) != 0) {
        
            if (((digit = Character.digit(star.charAt(1), 10)) != 0)
                && (EmeUtil.strncmp(Side, str.toString(), 4) == 0 
                || EmeUtil.strncmp(Bsmt, str.toString(), 4) == 0 || EmeUtil.strncmp(Inte, str.toString(), 4) == 0 
                || EmeUtil.strncmp(Rear, str.toString(), 4) == 0 || EmeUtil.strncmp(Botm, str.toString(), 4) == 0
                || EmeUtil.strncmp(Midl, str.toString(), 4) == 0 || EmeUtil.strncmp(Lowr, str.toString(), 4) == 0
                || EmeUtil.strncmp(Uppr, str.toString(), 4) == 0 || EmeUtil.strncmp(Frnt, str.toString(), 4) == 0
                || EmeUtil.strncmp(Down, str.toString(), 4) == 0 || EmeUtil.strncmp(Left, str.toString(), 4) == 0
                || EmeUtil.strncmp(Rght, str.toString(), 4) == 0 || EmeUtil.strncmp(Lbby, str.toString(), 4) == 0
                || EmeUtil.strncmp(Ofc, str.toString(), 3) == 0 || EmeUtil.strncmp(Top, str.toString(), 3) == 0
                || EmeUtil.strncmp(PH, str.toString(), 2) == 0 || EmeUtil.strncmp(Up, str.toString(), 2) == 0)) {
                
                EmeUtil.strcpy(outstr, star.substring(1));
                EmeUtil.strncpy(outstr, lenStar - 1, str, lenFirst);
                outstr.setLength(lenStr - 1);
                return;
            } else if (((digit = Character.digit(str.charAt(0), 10)) != 0)
                && (EmeUtil.strncmp(Side, star, 1, 4) == 0 
                || EmeUtil.strncmp(Bsmt, star, 1, 4) == 0 || EmeUtil.strncmp(Inte, star, 1, 4) == 0
                || EmeUtil.strncmp(Rear, star, 1, 4) == 0 || EmeUtil.strncmp(Botm, star, 1, 4) == 0
                || EmeUtil.strncmp(Midl, star, 1, 4) == 0 || EmeUtil.strncmp(Lowr, star, 1, 4) == 0
                || EmeUtil.strncmp(Uppr, star, 1, 4) == 0 || EmeUtil.strncmp(Frnt, star, 1, 4) == 0
                || EmeUtil.strncmp(Down, star, 1, 4) == 0 || EmeUtil.strncmp(Left, star, 1, 4) == 0
                || EmeUtil.strncmp(Rght, star, 1, 4) == 0 || EmeUtil.strncmp(Lbby, star, 1, 4) == 0
                || EmeUtil.strncmp(Ofc, star, 1, 3) == 0 || EmeUtil.strncmp(Top, star, 1, 3) == 0
                || EmeUtil.strncmp(PH, star, 1, 2) == 0 || EmeUtil.strncmp(Up, star, 1, 2) == 0)) {
                
                EmeUtil.strcpy(outstr, str);
                //    *star = '-';
                EmeUtil.strcpy(outstr, lenFirst, star, 1);
                outstr.setLength(lenStr - 1);
                return;
            }
        } 

        // Search for second asterisk
        star2 = EmeUtil.strchr(star, 1, '*');
        if (star2 != null) {
            floor = EmeUtil.strncmp(Fl, star2, 1, 2);
            level = EmeUtil.strncmp(Lvl, star2, 1, 2);

            if (floor == 0 || level == 0) {
                star2.setCharAt(3, '*');
                lenBefstar = lenStr - lenStar;
                EmeUtil.strncpy(star2, 4, str, lenBefstar);
                EmeUtil.strncpy(str, star, 1, lenStr);
                str.setLength(lenStr);

                // REinitialize
                star = EmeUtil.strchr(str, '*');
                lenStr = str.length();
                lenStar = star.length();
                star2 = EmeUtil.strchr(star, 1, '*');
            }   
        }

        //    Look for the following types :  LOWR*BOTM  ---->  BOTM-LOWR
        //    UP*BSMT    ---->  BSMT-UP;  UP*TOP     ---->  TOP-UP
        //    Skip any which contain "RD" etc. or digits.
        if ((lenStr >= 5) && (star2 == null) && (lenStar >= 3)
            && (EmeUtil.strncmp(nd, str, strIdx - 2, 2) != 0 && EmeUtil.strncmp(ND, str, strIdx - 2, 2) != 0
                && EmeUtil.strncmp(rd, str, strIdx - 2, 2) != 0 && EmeUtil.strncmp(RD, str, strIdx - 2, 2) != 0
                && EmeUtil.strncmp(th, str, strIdx - 2, 2) != 0 && EmeUtil.strncmp(TH, str, strIdx - 2, 2) != 0
                && EmeUtil.strncmp(st, str, strIdx - 2, 2) != 0 && EmeUtil.strncmp(ST, str, strIdx - 2, 2) != 0)
            && ((digit = Character.digit(star.charAt(1), 10)) == 0) 
            && ((digit = Character.digit(str.charAt(0), 10)) == 0)) {
            
            lenBefstar = lenStr - lenStar;
            // Put in alphabetic order by flipping the before portion of the
            // asterisk and the portion after the asterisk.
             if (str.charAt(0) > star.charAt(1)) {
                EmeUtil.strncpy(outstr, star, 1, lenStr);
                EmeUtil.strncpy(outstr, lenStar - 1, "-", 1);
                EmeUtil.strncpy(outstr, lenStar, str, lenBefstar);
                outstr.setLength(lenStr);
                return;
            } else  {
            // No need to flip, just put a '-' in
                star.setCharAt(0, '-');
                EmeUtil.strcpy(outstr, str);
                outstr.setLength(lenStr);
                return;
            }
        }

        // Check for Input: 2*G or 2*4   Output: 2G or 2-4
        befStar = lenStr - lenStar;
        aftStar = lenStr - befStar - 1;
        star2 = EmeUtil.strchr(star, 1, '*');
        if (star2 != null) {
            aftStar = aftStar - star2.length();
        }

        for (i = 0; i < befStar; i++) {
            if ((digit = Character.digit(str.charAt(i), 10)) == 0) {
                // Count number of characters
                chr1Cnt++;   
            } else {
                // Count number of digits
                dig1Cnt++;   
            }                   
        }

        for (i = 1; i <= aftStar; i++) {
            if ((digit = Character.digit(star.charAt(i), 10)) == 0) {
                // Count of characters after '*'
                chr2Cnt++;
            } else {
                dig2Cnt++;
            }
        }

        if (chr1Cnt == befStar && (lenStr < 3 || star.charAt(1) == '&')) {
             if (chr2Cnt == aftStar && (star.charAt(1) == '&')) {
                // Both characters, put a dash
                star.setCharAt(0, '-');
                EmeUtil.strcpy(outstr, str);
            } else if (dig2Cnt == aftStar || star.charAt(1) == '&') {
                // Both different,put togther
                EmeUtil.strncpy(outstr, str, befStar);
                EmeUtil.strcpy(outstr, befStar, star, 1);
            }
        } else if (dig1Cnt == befStar && (lenStr < 3 || star.charAt(1) == '&')) {
            if (dig2Cnt == aftStar && star.charAt(1) == '&') {
                star.setCharAt(0, '-');
                EmeUtil.strcpy(outstr, str);
            } else if (chr2Cnt == aftStar || star.charAt(1) == '&') {
                EmeUtil.strncpy(outstr, str, befStar);
                EmeUtil.strcpy(outstr, befStar, star, 1);
            }
        } else {
        // Check for Input: 3G*1 --->   Output: 3G1    or
        // Input: 3G*b --->   Output: 3G-b
            if (EmeUtil.strncmp(nd, str, strIdx - 2, 2) == 0
                || EmeUtil.strncmp(ND, str, strIdx - 2, 2) == 0 || EmeUtil.strncmp(rd, str, strIdx - 2, 2) == 0
                || EmeUtil.strncmp(RD, str, strIdx - 2, 2) == 0 || EmeUtil.strncmp(th, str, strIdx - 2, 2) == 0
                || EmeUtil.strncmp(TH, str, strIdx - 2, 2) == 0 || EmeUtil.strncmp(st, str, strIdx - 2, 2) == 0
                || EmeUtil.strncmp(ST, str, strIdx - 2, 2) == 0) {
                iSkip = true;
            } else {
                digit1 = Character.digit(star.charAt(1), 10);
                digit2 = Character.digit(star.charAt(1), 10);

                if ((digit1 != 0 && digit2 == 0) || (digit1 == 0 && digit2 != 0)) {
                    EmeUtil.strncpy(outstr, str, befStar);
                    EmeUtil.strcpy(outstr, befStar, star, 1);
                } else {
                    star.setCharAt(0, '-');
                    EmeUtil.strcpy(outstr, str);
                }
            }
        }

        if (!iSkip) {
            while (iMore) {
                iMore = false;

                if (star.charAt(1) == '&') {
                    and = true;
                }
                star = EmeUtil.strchr(star, 1, '*');

                if (star != null) {
                    len = outstr.length() - star.length();

                    if ((digit = Character.digit(star.charAt(1), 10)) == 0) {
                        if ((dig2Cnt == aftStar) || and) {
                            EmeUtil.strcpy(star, star, 1);
                            EmeUtil.strcpy(outstr, len, star);
                        } else if (chr2Cnt == aftStar) {
                            star.setCharAt(0, '-');
                            EmeUtil.strcpy(outstr, len, star);
                        }
                    } else {
                        if (dig2Cnt == aftStar) {
                            star.setCharAt(0, '-');
                            EmeUtil.strcpy(outstr, len, star);
                        } else if (chr2Cnt == aftStar) {
                            EmeUtil.strcpy(star, star, 1);
                            EmeUtil.strcpy(outstr, len, star);
                        }
                    }
                    iMore = true;
                    continue;
                } else {
                    return;
                }
            }
        }

        // Check for a digit in the first character
        if ((digit = Character.digit(str.charAt(0), 10)) != 0) {
        
            // Check for Fl or Lvl contained in the string after the first star.
            floor = EmeUtil.strncmp(Fl, star, 1, 2);
            level = EmeUtil.strncmp(Lvl, star, 1, 2);

            if (floor == 0 || level == 0) {
                if (EmeUtil.strncmp(nd, str, strIdx - 2, 2) == 0
                    || EmeUtil.strncmp(ND, str, strIdx - 2, 2) == 0 || EmeUtil.strncmp(rd, str, strIdx - 2, 2) == 0
                    || EmeUtil.strncmp(RD, str, strIdx - 2, 2) == 0 || EmeUtil.strncmp(th, str, strIdx - 2, 2) == 0
                    || EmeUtil.strncmp(TH, str, strIdx - 2, 2) == 0 || EmeUtil.strncmp(st, str, strIdx - 2, 2) == 0
                    || EmeUtil.strncmp(ST, str, strIdx - 2, 2) == 0) {
                    
                    // Copy the digits before the st, nd, th, rd to the output string.
                    // Numlen is the number of digits before the ND, ST, TH, RD.
                    numlen = lenStr - lenStar - 2;

                    for (i = numlen - 1; i >= 0; i--) {
                        outstr.setCharAt(i, str.charAt(i));
                    }

                    // Check for a second asterisk.  Eg: 22nd*Fl*G
                    star = EmeUtil.strchr(star, 1, '*');
                    // Only one asterisk. Input: 2nd*Fl ---> Output: 2Fl
                    if (star == null) {
                        if (floor == 0) {
                            EmeUtil.strncpy(outstr, numlen, Fl, 2);
                            outstr.setLength(numlen + 2);
                        }
                        
                        if (level == 0) {
                            EmeUtil.strncpy(outstr, numlen, Lvl, 3);
                            outstr.setLength(numlen + 3);
                        }
                        iCheck = true;
                    }

                    if (!iCheck) {
                        len2star = star.substring(1).length();
                        // Determine if first two characters after the asterisk are digits or
                        // letters.
                        digit = Character.digit(star.charAt(1), 10);

                        if (star.charAt(2) != '*') {
                            digit2 = Character.digit(star.charAt(2), 10);
                        } else {
                        // Shift over on character if a asterisk is in the star+2
                        // position. Example:   star = *2*3
                            digit2 = Character.digit(star.charAt(3), 10);

                            if ((digit2 != 0 && digit == 0) || (digit2 == 0 && digit != 0)) {
                                for (i = 2; i <= len2star; i++) {
                                    star.setCharAt(i, star.charAt(i + 1));
                                }
                                star.setLength(len2star);
                                len2star--;
                            }
                        }

                        // First or first and second characters are letters.
                        // Input: 2nd*Fl*EG ---> Output: 2EG
                        if ((digit == 0 && digit2 == 0 && len2star < 3)
                            || (digit == 0 && len2star == 1) || (digit == 0 && star2 != null && len2star < 3)) {
                            
                            EmeUtil.strcpy(outstr, numlen, star, 1);
                            
                        } else if ((digit != 0 && digit2 != 0 && len2star < 3)
                            || (digit != 0 && len2star == 1) || (digit != 0 && star2 != null && len2star < 3)) {
                        // First or first and second characters are digits.
                        // Input: 2nd*Fl*34 ---> Output: 2-34
                            outstr.setCharAt(numlen, '-');
                            EmeUtil.strcpy(outstr, numlen + 1, star, 1);
                        } else {
                            // First and second characters after asterisk are mixed.
                            // Input: 2nd*Fl*2E   ----> Output: 2Fl-2E   or
                            // Input: 2nd*Fl*Rear ----> Output: 2Fl-Rear
                            if (floor == 0) {
                                EmeUtil.strncpy(outstr, numlen, Fld, 3);
                                EmeUtil.strcpy(outstr, numlen + 3, star, 1);
                            }

                            if (level == 0) {
                                EmeUtil.strncpy(outstr, numlen, Lvld, 4);
                                EmeUtil.strcpy(outstr, numlen + 4, star, 1);
                            }
                        }
                    }
                }
            } else {
                // First character is a digit, but no Fl or Lvl in the field.
                // Input: 2nd*Rear ---> Output: 2Fl-Rear
                if (lenStar > 3) {
                    i = 0;
                    while (digit != 0) {
                        outstr.setCharAt(i, str.charAt(i));
                        i++;
                        digit = Character.digit(str.charAt(i), 10);
                    }
                    
                    EmeUtil.strncpy(outstr, i, Fld, 3);
                    EmeUtil.strcpy(outstr, i + 3, star, 1);
                }
            }
        }                         
        // Search for more asterisks and append onto the string
        
        // Before searching for another asterisk, check that previous star
        // did not contain an ampersand
        star = EmeUtil.strchr(outstr, '*');

        if (star != null) {
            len = outstr.length() - star.length();
            star.setCharAt(0, '-');
            len++;
            //.. Increment len to include '-'
            i = 0;

            while ((star.charAt(1 + i) != '*') && (star.length() > (1 + i))) {
                outstr.setCharAt(len + i, star.charAt(1 + i));
                i++;
            }
            star = EmeUtil.strchr(star, 1, '*');

            while (star != null) {
                len = outstr.length() - star.length();
                prevDigit = Character.digit(outstr.charAt(len - 1), 10);
                digit = Character.digit(star.charAt(1), 10);
                
                // Two consecutive letters or two consecutive digits.
                if (prevDigit == 0 && digit == 0 || prevDigit != 0 && digit != 0) {
                    outstr.setCharAt(len, '-');
                    i = 1;

                    while (star.charAt(i) != '*' && (star.length() > (i))) {
                        outstr.setCharAt(len + i, star.charAt(i));
                        outstr.setLength(len + 1 + i);
                        i++;
                    }
                } else {
                    i = 0;
                    while ((star.charAt(i + 1) != '*') && (star.length() > (1 + i))) {
                        outstr.setCharAt(len + i, star.charAt(1 + i));
                        outstr.setLength(len + 1 + i);
                        i++;
                    }
                }
                star = EmeUtil.strchr(star, 1, '*');
            }
        }
    }
}
