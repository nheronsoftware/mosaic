/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.stand.address;

import com.stc.sbme.stand.util.ReadStandConstantsValues;
import com.stc.sbme.util.EmeUtil;


/**
 * 
 * 
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
// RRC public
public class OutputPattern {

//	 RRC
	private static final String SEPARATOR = ":";
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(inputPatt);
		builder.append(SEPARATOR);
		builder.append(outputPatt);
		return builder.toString();
	}

    private final int PATSIZE;

    /* input pattern string */
    private StringBuffer inputPatt;
    /* output pattern string */
    private StringBuffer outputPatt;
    /* pattern type */
    private StringBuffer patternType;
    /* priority */
    private StringBuffer priority;
    /* beginning word position */
    private int beg;
    /* ending word position */
    private int end;

    /**
     * Default constructor
     */
    OutputPattern(String domain) {
        PATSIZE = ReadStandConstantsValues.getInstance(domain).getPatSize();
        inputPatt = new StringBuffer(PATSIZE);
        outputPatt = new StringBuffer(PATSIZE);
        patternType = new StringBuffer(3);
        priority = new StringBuffer(3);
    }

    OutputPattern(String domain, OutputPattern op) {
        PATSIZE = ReadStandConstantsValues.getInstance(domain).getPatSize();
        inputPatt = new StringBuffer().append(op.getInputPatt());
        outputPatt = new StringBuffer().append(op.getOutputPatt());
        patternType = new StringBuffer().append(op.getPatternType());
        priority = new StringBuffer().append(op.getPriority());
    }
    
    /**
     * 
     * @return a string
     */
    StringBuffer getInputPatt() {
        return inputPatt;
    }

    /**
     * 
     * @return a string
     */
    StringBuffer getOutputPatt() {
        return outputPatt;
    }

    /**
     * 
     * @return a string
     */
    StringBuffer getPatternType() {
        return patternType;
    }

    /**
     * 
     * @return a string
     */
    StringBuffer getPriority() {
        return priority;
    }

    /**
     * 
     * @return the starting endex of the string
     */
    public int getBeg() {
        return beg;
    }

    /**
     * 
     * @return the ending endex of the string
     */
    int getEnd() {
        return end;
    }

    /**
     * @param s aaa
     */
    void setInputPatt(String  s) {
        EmeUtil.replace(this.inputPatt, s);
    }

    /**
     * @param s aaa
     */
    void setOutputPatt(String s) {
        EmeUtil.replace(this.outputPatt, s);
    }

    /**
     * @param s aaa
     */
    void setPatternType(String s) {
        EmeUtil.replace(this.patternType, s);
    }

    /**
     * @param s aaa
     */
    void setPriority(String s) {
        EmeUtil.replace(this.priority, s);
    }

    /**
     * @param beg aaa
     */
    void setBeg(int beg) {
        this.beg = beg;
    }

    /**
     * @param end aaa
     */
    void setEnd(int end) {
        this.end = end;
    }
}
