/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.stand.businessname;

import java.util.ArrayList;

import com.stc.sbme.util.EmeUtil;

/**
 * 
 * 
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
public class StandBusinessName {
    // Added for capturing pattern signature
    private String signature;
    public String getSignature() { return signature; }
    public void setSignature(String signature) { this.signature = signature; }
    

    /* Defines the primary name associated with a business entity */
    private StringBuffer primaryName;
    
    /* Defines the organization type of the business entity */
    private StringBuffer orgTypeKeyword;
    
    /* Defines the association type of the business entity */
    private StringBuffer assocTypeKeyword;
    
    /* Defines the association type of the business entity */
    private ArrayList industrySectorList;
    
    /* Defines the association type of the business entity */
    private StringBuffer industryTypeKeyword;
    
    /* Defines the association type of the business entity */
    private ArrayList aliasList;
    
    /* Defines the association type of the business entity */
    private StringBuffer url;

    /* Defines the association type of the business entity */
    private StringBuffer notFoundType;
    

    private StandBusinessName() {
        super();

        primaryName = new StringBuffer();
        orgTypeKeyword = new StringBuffer();
        assocTypeKeyword = new StringBuffer();
        industrySectorList = new ArrayList();
        industryTypeKeyword = new StringBuffer();
        aliasList = new ArrayList();
        url = new StringBuffer();
        notFoundType = new StringBuffer();
    }


    /**
     * Define an access method to obtain an instance of this class
     * @return an instance of this class
     */
    public static StandBusinessName getSAInstance() {
        return new StandBusinessName();
    }


    /**
     * 
     * @return a string
     */
    public StringBuffer getPrimaryName() {
        return primaryName;
    }

    /**
     * 
     * @return a string
     */
    public StringBuffer getOrgTypeKeyword() {
        return orgTypeKeyword;
    }

    /**
     * 
     * @return a string
     */
    public StringBuffer getAssocTypeKeyword() {
        return assocTypeKeyword;
    }
 
    /**
     *
     * @return a string
     */
    public StringBuffer getIndustryTypeKeyword() {
        return industryTypeKeyword;
    }

    /**
     *
     * @return a string
     */
    public StringBuffer getUrl() {
        return url;
    }
    
    /**
     *
     * @return a string
     */
    public StringBuffer getNotFoundType() {
        return notFoundType;
    }

    
    /**
     * 
     * @return a string
     */
    public ArrayList getAliasList() {
        return aliasList;
    }
    
    
    /**
     * 
     * @return a string
     */
    public ArrayList getIndustrySectorList() {
        return industrySectorList;
    }
    
    


    /**
     * 
     * @param s a 
     * @return a house prefix number
     */
    public StringBuffer setPrimaryName(String s) {
     return EmeUtil.replace(this.primaryName, s);
    }

    /**
     * 
     * @param s a 
     * @return a house number
     */
    public StringBuffer setOrgTypeKeyword(String s) {
     return EmeUtil.replace(this.orgTypeKeyword, s);
    }

    /**
     * 
     * @param s a 
     * @return a house number
     */
    public StringBuffer setAssocTypeKeyword(String s) {
     return EmeUtil.replace(this.assocTypeKeyword, s);
    }
 
    
    /**
     *
     * @param s a
     * @return a house number
     */
    public StringBuffer setIndustryTypeKeyword(String s) {
     return EmeUtil.replace(this.industryTypeKeyword, s);
    }

    /**
     *
     * @param s a
     * @return a house number
     */
    public StringBuffer setUrl(String s) {
     return EmeUtil.replace(this.url, s);
    }
    
    /**
     *
     * @param s a
     * @return a house number
     */
    public StringBuffer setNotFoundType(String s) {
     return EmeUtil.replace(notFoundType, s);
    }


   /**
     * 
     * @param s a 
     * @return a house number
     */
    public ArrayList setIndustrySectorList(ArrayList s) {
     return EmeUtil.replace(this.industrySectorList, s);
    }
    
    
    /**
     * ArrayList
     * @param s a 
     * @return a house number
     */
    public ArrayList setAliasList(ArrayList s) {
     return EmeUtil.replace(this.aliasList, s);
    }     
}
