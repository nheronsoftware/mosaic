/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.stand.address;

import java.util.StringTokenizer;

import com.stc.sbme.api.SbmeMatchEngineException;
import com.stc.sbme.api.SbmeStandardizationException;
import com.stc.sbme.stand.util.ReadStandConstantsValues;
import com.stc.sbme.util.EmeUtil;
import com.stc.sbme.util.SbmeLogUtil;
import com.stc.sbme.util.SbmeLogger;

/**
 * 
 * 
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
class AddressStandBuilder {

    private final int NUMBER_OUTPUT_FIELD_SIZE;
    private final int PREFIX_OUTPUT_FIELD_SIZE;
    private final int SUFFIX_OUTPUT_FIELD_SIZE;
    private final int DIRECTION_OUTPUT_FIELD_SIZE;
    private final int MAX_OUTPUT_PATTERNS;
    private final int TYPE_OUTPUT_FIELD_SIZE;
    private final int NAME_OUTPUT_FIELD_SIZE;
    private final int EXTENSION_OUTPUT_FIELD_SIZE;
    private final int EXTRAINFO_OUTPUT_FIELD_SIZE;
    private final int MAX_INPUT_TOKENS;
    private final String domain;
    private final SearchComboPatterns searchComboPatterns;
    private final AddressMasterCluesTable addressMasterCluesTable;
    
    private final SbmeLogger mLogger = SbmeLogUtil.getLogger(this);  
    private final boolean mDebug = mLogger.isDebugEnabled();
    private static final SbmeLogger LOGGER = SbmeLogUtil.getLogger("com.stc.sbme.stand.address.AddressStandBuilder");

    private static int all = 0;
    private static int allButW = 1;
    private static int allButH = 2;

    private static int spaces = 0;
    private static int squash = 1;
    private static int dash = 2;
    private static int asterisk = 3;
    
    AddressClueWordsTable clueW;
    
    AddressStandBuilder(String domain) {
        NUMBER_OUTPUT_FIELD_SIZE = ReadStandConstantsValues.getInstance(domain).getNumberOutputFieldSize();
        PREFIX_OUTPUT_FIELD_SIZE = ReadStandConstantsValues.getInstance(domain).getPrefixOutputFieldSize();
        SUFFIX_OUTPUT_FIELD_SIZE = ReadStandConstantsValues.getInstance(domain).getSuffixOutputFieldSize();
        DIRECTION_OUTPUT_FIELD_SIZE = ReadStandConstantsValues.getInstance(domain).getDirectionOutputFieldSize();
        MAX_OUTPUT_PATTERNS = ReadStandConstantsValues.getInstance(domain).getMaxOutputPatterns();
        TYPE_OUTPUT_FIELD_SIZE = ReadStandConstantsValues.getInstance(domain).getTypeOutputFieldSize();
        NAME_OUTPUT_FIELD_SIZE = ReadStandConstantsValues.getInstance(domain).getNameOutputFieldSize();
        EXTENSION_OUTPUT_FIELD_SIZE = ReadStandConstantsValues.getInstance(domain).getExtensionOutputFieldSize();
        EXTRAINFO_OUTPUT_FIELD_SIZE = ReadStandConstantsValues.getInstance(domain).getExtrainfoOutputFieldSize();
        MAX_INPUT_TOKENS = ReadStandConstantsValues.getInstance(domain).getMaxInputTokens();    
        searchComboPatterns = new SearchComboPatterns(domain);
        addressMasterCluesTable = AddressMasterCluesTable.getInstance(domain);
        this.domain = domain;
    }
    
    /**
     *
     * @param sVar an instance of the AddressVariables class
     * @param returnPatt an array of patterns
     * @param numPatts the number of patterns
     * @param state the state code
     * @param zip the zip code
     * @param aLoad an instance of the loader class
     * @return a Address object
     * @throws SbmeStandardizationException exception
     * @throws SbmeMatchEngineException exception
     */
    StandAddress buildStandAddress(AddressStandVariables sVar, OutputPattern[] returnPatt, int numPatts,
                                          StringBuffer state, StringBuffer zip, AddressTablesLoader aLoad)
        throws SbmeStandardizationException, SbmeMatchEngineException {

        StringBuffer bNum = new StringBuffer(NUMBER_OUTPUT_FIELD_SIZE);
        StringBuffer bNumPre = new StringBuffer(PREFIX_OUTPUT_FIELD_SIZE);
        StringBuffer bNumSuf = new StringBuffer(SUFFIX_OUTPUT_FIELD_SIZE);

        StringBuffer otok = new StringBuffer(3);
        StringBuffer itok = new StringBuffer(3);
        StringBuffer prevOtok = new StringBuffer(3);

        char matchAbb;
        char abb;

        StringBuffer hold = new StringBuffer();
        StringBuffer oSD = new StringBuffer(DIRECTION_OUTPUT_FIELD_SIZE);
        
        StringBuffer fract;
        TokenStruct[] tstBeg = new TokenStruct[MAX_INPUT_TOKENS];
        Posstr[] pa = new Posstr[MAX_OUTPUT_PATTERNS];

        clueW =  aLoad.getClueWord();
        
        StandAddress sa = StandAddress.getSAInstance(domain);
        
        int i;
        int j;
        int lenn;
        int lln;
        int beg;
        int end;
        int pos;
        int diffInd;
        StringBuffer outPatIn;
        StringBuffer outPatOut;
        OutputPattern outPat;
        TokenStruct tokStIs = new TokenStruct(domain);
        
        int otokH1;
        int otokH2;
        int otokNA;
        int otokHN;
        
        int itokTY;
        int itokNL;

        int is = 0;
        Integer cword;
        int oSDcword = 0;
        boolean nAonly;
        if (domain.compareTo("FR") == 0 || domain.compareTo("AU") == 0) {
            abb = 'H';
        } else {
            abb = Character.toUpperCase(sVar.abbrevOpt);
        }

        if (abb == 'E' || abb == 'F' || abb == 'S') {
            matchAbb = 'F';

        } else {
            matchAbb = 'U';
        }

        /* initialize TokenPosition objects */
        for (i = 0; i < MAX_OUTPUT_PATTERNS; i++) {
            pa[i] = new Posstr();
        }

        lln = sVar.tokensSt.length;
        for (i = 0; i < lln; i++) {
            tstBeg[i] = new TokenStruct(domain);
        }        

        /* Initialize sa */
        initAddressStruct(sa, all);

        nAonly = true;


        searchComboPatterns.buildPositionArray(pa, returnPatt, numPatts);

        j = sVar.tokensSt.length;
        
        for (i = 0; i < j; i++) {

            if (sVar.tokensSt[i] == null) {
                break;
            }

            sVar.tokensSt[i].createCopy(tstBeg[i]);
        }

        /* loop through each sub-pattern found */
        for (i = 0; i < numPatts; i++) {

            tstBeg[is].createCopy(tokStIs);

            pos = pa[i].getOrder() - 1;

            outPat = returnPatt[pos];
            outPatIn = outPat.getInputPatt();
            outPatOut = outPat.getOutputPatt();
            
            beg = outPat.getBeg();
            end = outPat.getEnd();
            diffInd = end - beg;
            is = beg - 1;

            
           /* 
            * For each sub-pattern, individually process each token. I think this is the
            * hard code for F Scott Fitzgerald Ave, so the F goes to street name,
            * not house number suffix.
            */
            if ((EmeUtil.strcmp(outPatIn, "NU A1") == 0)
                    && (EmeUtil.strcmp(outPatOut, "HN HS") == 0)
                    && (sVar.usf == 'T')) {

                EmeUtil.strncpy(outPatOut, 3, "NA", 2);
            }


            for (j = 0; j <= diffInd; j++, is++) {
                // Keep track of the previous output type
                if (j > 0 && EmeUtil.strcmp(otok, "NA") == 0) {
                    prevOtok.setLength(0);
                    prevOtok.append("NA");
                }
                EmeUtil.sprintf(otok, "%.2s", outPatOut, 3 * j);
                EmeUtil.sprintf(itok, "%.2s", outPatIn, 3 * j);
 
                tokStIs = sVar.tokensSt[is];

               /*
                * call getClueWordID to calculate and return the proper clue
                * word id for the current token
                */                                
                cword = new Integer(getClueWordID(tokStIs, itok));

                // copy the content of the comparison into the variable otokH1
                otokH1 = EmeUtil.strcmp(otok, "H1");
                otokH2 = EmeUtil.strcmp(otok, "H2");
                otokNA = EmeUtil.strcmp(otok, "NA");
                otokHN = EmeUtil.strcmp(otok, "HN");
                
                itokTY = EmeUtil.strcmp(itok, "TY");
                itokNL = EmeUtil.strcmp(itok, "NL");

                if (otokNA != 0) {
                    nAonly = false;
                }

               /*
                * Strip leading zeros from all numeric fields except the house numbers
                * (they will be dealt with later).
                */
                if ((EmeUtil.strcmp(itok, "NU") == 0) 
                        && (otokH1 != 0)
                        && (otokH2 != 0)
                        && (otokHN != 0)
                        && (EmeUtil.strcmp(otok, "XN") != 0)) {
                    stripLeadingZeros(tokStIs.getInputImage());
                }

               /*
                * NOTE:  when loading certain fields, you must check to see if
                * a fraction was earlier combined with the field.  If so,
                * remove it and load it in its appropriate field.
                */
                if (otokH1 == 0 || otokHN == 0) {
                
                    checkForSpelledOutNumber(tokStIs, state, zip);

                    if ((fract = fractionWithinNumber(tokStIs.getInputImage())) != null) {

                        loadField(sa.getHouseNumSuffix(), SUFFIX_OUTPUT_FIELD_SIZE, fract.toString(), 0,
                                  itok, abb, spaces);
                    }

                    loadField(sa.getHouseNumber(), NUMBER_OUTPUT_FIELD_SIZE, 
                        tokStIs.getInputImage().toString(), cword.intValue(), itok, abb, spaces);

                } else if (otokH2 == 0) {

                    checkForSpelledOutNumber(tokStIs, state, zip);

                    if ((fract = fractionWithinNumber(tokStIs.getInputImage())) != null) {

                        loadField(sa.getHouseNumSuffix(), SUFFIX_OUTPUT_FIELD_SIZE, fract.toString(), 0, itok,
                                  abb, spaces);
                    }

                    loadField(sa.getSecondHouseNumber(), NUMBER_OUTPUT_FIELD_SIZE, 
                        tokStIs.getInputImage().toString(), cword.intValue(), itok, abb, spaces);

                } else if (EmeUtil.strcmp(otok, "P1") == 0) {

                    loadField(sa.getHouseNumPrefix(), PREFIX_OUTPUT_FIELD_SIZE, 
                        tokStIs.getInputImage().toString(), cword.intValue(), itok, 'S', spaces);

                } else if (EmeUtil.strcmp(otok, "P2") == 0) {

                    loadField(sa.getSecondHouseNumberPrefix(), PREFIX_OUTPUT_FIELD_SIZE, 
                        tokStIs.getInputImage().toString(), cword.intValue(), itok, 'S', spaces);

                } else if (EmeUtil.strcmp(otok, "HS") == 0) {

                    loadField(sa.getHouseNumSuffix(), SUFFIX_OUTPUT_FIELD_SIZE, 
                        tokStIs.getInputImage().toString(), cword.intValue(), itok, abb, spaces);

                } else if (EmeUtil.strcmp(otok, "PD") == 0) {
                
                    loadField(sa.getStreetNamePrefDirection(), DIRECTION_OUTPUT_FIELD_SIZE, 
                        tokStIs.getInputImage().toString(), cword.intValue(), itok, abb, spaces);

                    sa.setStreetNamePrefDirectionC(cword.intValue());

                } else if (EmeUtil.strcmp(otok, "PT") == 0) {

                    if (checkPT(tokStIs, cword.intValue(), itok, abb, sa)) {

                        continue;
                    }
                                       
                   /*
                    * checkPTtoNA will change:
                    *       IMAGE:  ST         ST
                    * CLUE WORD ID: 263         79
                    * INPUT TOKEN:  TY  -to-   AU
                    * OUTPUT TOKEN:  PT         NA
                    */            
                    if (checkPTtoNA(tokStIs, cword, outPatIn,
                        3 * j, outPatOut, 3 * j)) {
                        
                        is--;
                        j--;
                        
                        continue;
                    }

                    loadField(sa.getStreetNamePrefType(), TYPE_OUTPUT_FIELD_SIZE, 
                        tokStIs.getInputImage().toString(), cword.intValue(), itok, abb, spaces);

                    sa.setStreetNamePrefTypeC(cword.intValue());

                } else if (otokNA == 0) {

                    loadField(sa.getOrigStreetName(), NAME_OUTPUT_FIELD_SIZE, 
                        tokStIs.getInputImage().toString(), 0, itok, abb, spaces);

                   /*
                    * If the abbreviation option is 'E' (use expanded matching names),
                    * then loadStorageField will copy the storage name to the matching
                    * name:  eg, we want "fifth" ave to have a matching name of "5th"
                    */
                    if (!loadStorageField(sa.getStorStreetName(), NAME_OUTPUT_FIELD_SIZE, cword,
                        tokStIs.getInputImage(), abb, state, zip)) {
                        
                        loadField(sa.getStorStreetName(), NAME_OUTPUT_FIELD_SIZE,
                            tokStIs.getInputImage().toString(), 0, itok, abb, spaces);
                    }
                    
                    // Make sure "st barnabas" has a matching name of "saint barnabas"
                    // for type 'E'
                    if (itokTY == 0) {
                    
                        lenn = AddressClueWordsTable.trimln(tokStIs.getInputImage());
                        
                        /* create a copy of inputImage with all characters in uppercase */
                        AddressNormalizer.upCase(tokStIs.getInputImage(), hold);

                        if (EmeUtil.strncmp(hold, "ST", lenn) == 0) {
                            
                            loadField(sa.getMatchStreetName(), NAME_OUTPUT_FIELD_SIZE, "Saint",
                                (matchAbb == 'F' ? 0 : cword.intValue()), itok, matchAbb, spaces);
                                
                        } else if (EmeUtil.strncmp(hold, "DR", lenn) == 0) {
                                                   
                            loadField(sa.getMatchStreetName(), NAME_OUTPUT_FIELD_SIZE, "Dr",
                                (matchAbb == 'F' ? 0 : cword.intValue()), itok, matchAbb, spaces);

                        } else {
                        
                            loadField(sa.getMatchStreetName(), NAME_OUTPUT_FIELD_SIZE, 
                            tokStIs.getInputImage().toString(), cword.intValue(), itok, matchAbb, spaces);
                        }

                    } else {
                    
                        // If the abbreviation option is 'E', then all matching names now
                        // need the FULL names:
                        //     springhill    -->   springhill
                        //     Spring hill   -->   Spr Hl
                        //     Before, these two names wouldn't match
                        //     springhill    -->   springhill
                        //     Spring hill   -->   Spring Hill
                        //     Now they can.
                    	// RRC
                        if (itokNL != 0/* || EmeUtil.strcmp(prevOtok, "NA") == 0*/) {
                            loadField(sa.getMatchStreetName(), NAME_OUTPUT_FIELD_SIZE, 
                            tokStIs.getInputImage().toString(), cword.intValue(), itok, matchAbb, spaces);
                        }
                    } 
                      
                } else if (EmeUtil.strcmp(otok, "ST") == 0) {
                
                    loadField(sa.getStreetNameSufType(), TYPE_OUTPUT_FIELD_SIZE,
                        tokStIs.getInputImage().toString(), cword.intValue(), itok, abb, spaces);

                    sa.setStreetNameSufTypeC(cword.intValue());
                } else if (EmeUtil.strcmp(otok, "S2") == 0) {
                    loadField(sa.getSecondStreetNameSufType(), TYPE_OUTPUT_FIELD_SIZE,
                        tokStIs.getInputImage().toString(), cword.intValue(), itok, abb, spaces);

                    sa.setSecondStreetNameSufTypeC(cword.intValue());
                } else if (EmeUtil.strcmp(otok, "SD") == 0) {
                
                    EmeUtil.strcpy(oSD, tokStIs.getInputImage());
                    
                    oSDcword = cword.intValue();

                    loadField(sa.getStreetNameSufDirection(), DIRECTION_OUTPUT_FIELD_SIZE,
                        tokStIs.getInputImage().toString(), cword.intValue(), itok, abb, spaces);

                    sa.setStreetNameSufDirectionC(cword.intValue());
                    
                } else if (EmeUtil.strcmp(otok, "EX") == 0) {
                
                    loadField(sa.getStreetNameExtensionIndex(), EXTENSION_OUTPUT_FIELD_SIZE,
                        tokStIs.getInputImage().toString(), cword.intValue(), itok, abb, spaces);

                    sa.setStreetNameExtensionIndexC(cword.intValue());
                    
                } else if (EmeUtil.strcmp(otok, "WD") == 0) {
                
                    loadField(sa.getWithinStructDescript(), TYPE_OUTPUT_FIELD_SIZE,
                        tokStIs.getInputImage().toString(), cword.intValue(), itok, 'U', spaces);

                    sa.setWithinStructDescriptC(cword.intValue());
                    
                } else if (EmeUtil.strcmp(otok, "WI") == 0) {
                
                    checkForSpelledOutNumber(tokStIs, state, zip);

                    loadField(sa.getWithinStructIdentif(), NUMBER_OUTPUT_FIELD_SIZE,
                        tokStIs.getInputImage().toString(), cword.intValue(), itok, abb, spaces);

                } else if (EmeUtil.strcmp(otok, "NB") == 0) {
                
                    checkForSpelledOutNumber(tokStIs, state, zip);

                    loadField(bNum, NUMBER_OUTPUT_FIELD_SIZE,
                        tokStIs.getInputImage().toString(), cword.intValue(), itok, abb, squash);

                } else if (EmeUtil.strcmp(otok, "1P") == 0) {
                
                    loadField(bNumPre, PREFIX_OUTPUT_FIELD_SIZE,
                        tokStIs.getInputImage().toString(), cword.intValue(), itok, 'S', spaces);

                } else if (EmeUtil.strcmp(otok, "BS") == 0) {
                
                    loadField(bNumSuf, SUFFIX_OUTPUT_FIELD_SIZE,
                        tokStIs.getInputImage().toString(), cword.intValue(), itok, abb, spaces);

                } else if (EmeUtil.strcmp(otok, "DB") == 0) {
                
                    loadField(sa.getPropDesPrefDirection(), DIRECTION_OUTPUT_FIELD_SIZE,
                        tokStIs.getInputImage().toString(), cword.intValue(), itok, abb, spaces);

                    sa.setPropDesPrefDirectionC(cword.intValue());
                    
                } else if (EmeUtil.strcmp(otok, "TB") == 0) {
                
                    loadField(sa.getPropDesPrefType(), TYPE_OUTPUT_FIELD_SIZE,
                        tokStIs.getInputImage().toString(), cword.intValue(), itok, abb, spaces);

                    sa.setPropDesPrefTypeC(cword.intValue());
                    
                } else if (EmeUtil.strcmp(otok, "BN") == 0) {
                
                    loadField(sa.getOrigPropertyName(), NAME_OUTPUT_FIELD_SIZE,
                        tokStIs.getInputImage().toString(), 0, itok, abb, spaces);

                    if (itokTY == 0) {
                    
                        loadField(sa.getMatchPropertyName(), NAME_OUTPUT_FIELD_SIZE,
                            tokStIs.getInputImage().toString(), 0, itok, abb, spaces);
                            
                    } else {

                        if (itokNL != 0) {
                        
                            loadField(sa.getMatchPropertyName(), NAME_OUTPUT_FIELD_SIZE,
                                tokStIs.getInputImage().toString(), cword.intValue(), itok, 'S', spaces);
                        }
                    }
                    
                } else if (EmeUtil.strcmp(otok, "BT") == 0) {
                
                    loadField(sa.getPropertySufType(), TYPE_OUTPUT_FIELD_SIZE,
                        tokStIs.getInputImage().toString(), cword.intValue(), itok, abb, spaces);

                    sa.setPropertySufTypeC(cword.intValue());
                    
                } else if (EmeUtil.strcmp(otok, "BD") == 0) {
                
                    loadField(sa.getPropertySufDirection(), DIRECTION_OUTPUT_FIELD_SIZE,
                        tokStIs.getInputImage().toString(), cword.intValue(), itok, abb, spaces);

                    sa.setPropertySufDirectionC(cword.intValue());
                    
                } else if (EmeUtil.strcmp(otok, "BY") == 0) {
                
                    loadField(sa.getStructureDescript(), TYPE_OUTPUT_FIELD_SIZE,
                        tokStIs.getInputImage().toString(), cword.intValue(), itok, 'F', spaces);

                } else if (EmeUtil.strcmp(otok, "BI") == 0) {
                
                    loadField(sa.getStructureIdentif(), NUMBER_OUTPUT_FIELD_SIZE,
                        tokStIs.getInputImage().toString(), cword.intValue(), itok, 'F', spaces);

                } else if (EmeUtil.strcmp(otok, "RR") == 0) {
                
                    loadField(sa.getRuralRouteDescript(), TYPE_OUTPUT_FIELD_SIZE,
                        tokStIs.getInputImage().toString(), cword.intValue(), itok, abb, spaces);

                    sa.setRuralRouteDescriptC(cword.intValue());
                    
                } else if (EmeUtil.strcmp(otok, "RN") == 0) {
                
                    checkForSpelledOutNumber(tokStIs, state, zip);

                    if ((fract = fractionWithinNumber(tokStIs.getInputImage())) != null) {

                        loadField(sa.getExtraInfo(), EXTRAINFO_OUTPUT_FIELD_SIZE, fract.toString(), 
                                  0, itok, abb, spaces);
                    }

                    loadField(sa.getRuralRouteIdentif(), NUMBER_OUTPUT_FIELD_SIZE,
                        tokStIs.getInputImage().toString(), cword.intValue(), itok, abb, dash);

                } else if (EmeUtil.strcmp(otok, "BX") == 0) {
                

                    loadField(sa.getBoxDescript(), TYPE_OUTPUT_FIELD_SIZE,
                        tokStIs.getInputImage().toString(), cword.intValue(), itok, 'S', spaces);

                    sa.setBoxDescriptC(cword.intValue());


                } else if (EmeUtil.strcmp(otok, "XN") == 0) {
                
                    checkForSpelledOutNumber(tokStIs, state, zip);

                    loadField(sa.getBoxIdentif(), NUMBER_OUTPUT_FIELD_SIZE,
                        tokStIs.getInputImage().toString(), cword.intValue(), itok, abb, dash);


                } else if (EmeUtil.strcmp(otok, "EI") == 0) {
                
                    loadField(sa.getExtraInfo(), EXTRAINFO_OUTPUT_FIELD_SIZE,
                        tokStIs.getInputImage().toString(), 0, itok, abb, spaces);


                } else if (EmeUtil.strcmp(otok, "NL") == 0) {
                    continue;
                    
                } else if (EmeUtil.strcmp(otok, "T2") == 0) {
                
                    loadField(sa.getSecondStreetNameSufType(), TYPE_OUTPUT_FIELD_SIZE,
                        tokStIs.getInputImage().toString(), cword.intValue(), itok, abb, spaces);

                    sa.setSecondStreetNameSufTypeC(cword.intValue());
                      
                } else if (EmeUtil.strcmp(otok, "N2") == 0) {
                
                    loadField(sa.getOrigSecondStreetName(), NAME_OUTPUT_FIELD_SIZE, 
                        tokStIs.getInputImage().toString(), 0, itok, abb, spaces);
                    
                } else {
                    
                    mLogger.error("ERROR:  output token type does not exist: "
                    + otok + "\ninput pattern: " + outPatIn
                    + "outputPattern: " + outPatOut);

                    throw new SbmeStandardizationException("ERROR:  output token type does not exist: "
                    + otok + "\ninput pattern: " + outPatIn
                    + "outputPattern: " + outPatOut);
                }
            }
        }

        // Before returning, a variety of checks will be made for special situations on
        // certain fields
        checkBox(sa, abb);
        checkHouseNumber(sa);
        checkBuildingNumber(sa, bNum, bNumPre, bNumSuf);
    
        if (sVar.usf != 'T' && sVar.usf != 'S') {
            checkDirections(sa, oSD, oSDcword, abb);
        }
        
        checkWithinStructure(sa, abb); 
        checkStreetName(sa, nAonly, sVar.usf);
        
        /* Return the standardized address */
        return sa;
    }



    /*
     *  Loads the field, fld, in the structure, sa, with the information sent.
     */
    private void loadField(StringBuffer fld, int maxlen, String image, int cword, Object inToks,
                           char abbrevOpt, int combine)
        throws SbmeStandardizationException, SbmeMatchEngineException {
    
        StringBuffer addfield = new StringBuffer();
        StringBuffer hold = new StringBuffer();
        StringBuffer padch = new StringBuffer(3);
        Master mp;
        StringBuffer inTok;
        String tspace = "  ";
        String inTokName = inToks.getClass().getName();
        
        int len = 0;
        int len2 = 0;
 
        // Make sure that you cast to the right class
        if (inTokName.compareTo("java.lang.StringBuffer") == 0) {
            inTok = (StringBuffer) inToks;
            
        } else if (inTokName.compareTo("java.lang.String") == 0) {
            inTok = new StringBuffer((String) inToks);

        } else {
            inTok = null;
        }
            
        // If the clue word id for the token is 0 (ex: AU's, NU's, etc), then the
        // master clues table cannot be searched
        if (cword == 0) {
            EmeUtil.replace(addfield, image);
            
        } else  {
        // If the clue type is AU and the clue word id non-zero, then this could be
        // possibly consecutive AUs that were combined. In this case, you want to get
        // the abbreviations from the master clue table of all words in the string...
        // eg: 123 mountain beach rd        123 mt bch rd
            if ((mp = addressMasterCluesTable.searchForMasterEntry(cword, inTok, domain)) == null) {
            
                mLogger.error("ERROR:  master entry wasnt found for"
                    + "clue word id " + cword + " clue type" + inTok);
                
                throw new SbmeStandardizationException("ERROR:  master entry wasnt found for "
                + "clue word id " + cword + " clue type " + inTok);
            }

            // If abbrevOpt is 'E', 'F', 'S', 'H', or 'U' then get the abbrev. from
            // the master clue table. EXCEPTION:  if using abbrev opt 'F', then dont
            // expand directions. Example:  "E street" was incorrectly matching to
            // "East street"
            if (abbrevOpt == 'F') {
                addfield = mp.getFullName();
                
            } else if (abbrevOpt == 'S') {
                addfield = mp.getStdAbbrev();
                
            } else if (abbrevOpt == 'H') {
                addfield = mp.getShortAbbrev();
                
            } else if (abbrevOpt == 'U') {
                addfield = mp.getUspsAbbrev();
                
            } else if (abbrevOpt == 'E') {
                addfield = mp.getUspsAbbrev();
                
            }
        }

        // if combine == spaces then add a space between concatenations,
        // else if combine == squash, then add nothing,
        // else if combine == asterisk, then add an asterisk,
        // else if combine == dash then add a dash ONLY WHEN concatenating
        // integers, otherwise add nothing.
        len = AddressClueWordsTable.trimln(fld);
        
        
        EmeUtil.sprintf(hold, "%." + len + "s", fld);

        if (len == 0 || combine == squash) {
        
            len2 = 0; 
            EmeUtil.strcpy(padch, tspace);
            
        } else if (combine == spaces) {
            len2 = 1; 
            EmeUtil.strcpy(padch, tspace);
            
        } else if (combine == asterisk) {
            len2 = 1; 
            EmeUtil.strcpy(padch, "* ");
            
        } else {
        
            if (Character.isDigit(fld.charAt(len - 1)) 
                && Character.isDigit(addfield.charAt(0))) {
                
                len2 = 1;
                EmeUtil.strcpy(padch, "- ");
                
            } else {
                len2 = 0;
                EmeUtil.strcpy(padch, tspace);
            }
        }


        EmeUtil.sprintf(hold, len, "%." + len2 + "s", "%." + AddressClueWordsTable.trimln(addfield)
        + "s", padch, addfield);

        EmeUtil.sprintf(fld, "%." + (maxlen - 1) + "s", hold);
    }


    /*
     *  Used only when loading the Storage name field. It searches the clue table for the
     *  image and checks the storage flag: If it is '*', then the name is loaded from the
     *  standard abbreviation field, and the function returns "true", otherwise "false".
     */
    private boolean loadStorageField(StringBuffer fld, int maxlen, Integer cword, StringBuffer image,
                                     char abb, StringBuffer state, StringBuffer zip)
        throws SbmeStandardizationException, SbmeMatchEngineException {
        
        ClueTable ct;
        StringBuffer hold = new StringBuffer();
        StringBuffer outstring = new StringBuffer(5);
        StringBuffer fhold = new StringBuffer();
        int len = 0;
    
        // If the clue word id for the token is 0, then a separate
        // storage name can't exist, hence, return FALSE.
        if (cword.intValue() == 0) {
             return false;
        }
        
        AddressNormalizer.upCase(image, hold);

        if ((ct = clueW.searchForClue(hold, state, zip)) == null) {
            
            mLogger.error("ERROR:  clue word entry wasnt found for"
            + "input image: " + image);

            throw new SbmeStandardizationException("ERROR:  clue word entry wasnt found for"
            + "input image: " + image);
        }

        if (ct.getStoreFlag() == '*') {
        
            len = AddressClueWordsTable.trimln(fld);
            
            EmeUtil.sprintf (fhold, "%." + len + "s", fld);
            EmeUtil.sprintf (fhold, len, "%." + (AddressClueWordsTable.trimln(fld) == 0 ? 0 : 1) + "s",
                    "%." + AddressClueWordsTable.trimln(ct.getTranslation()) + "s", " ", ct.getTranslation());
                    
            EmeUtil.sprintf(fld, "%." + (maxlen - 1) + "s", fhold);

            if (abb == 'E') {
            
                EmeUtil.strcpy(image, ct.getTranslation());
                cword = Integer.valueOf("0");
            }
            return true;
        }
        return false;
    }


    /*
     *  Receives an input token, inTok, and determines its clue word ID using the
     *  information loaded in the token struct, tst.
     */
    private int getClueWordID(TokenStruct tst, StringBuffer inTok) 
        throws SbmeStandardizationException {


        if (EmeUtil.strcmp(tst.getClueType1(), inTok) == 0) {
        
            return tst.getClueWordId1();
            
        } else if (EmeUtil.strcmp(tst.getClueType2(), inTok) == 0) {

            return tst.getClueWordId2();
            
        } else if (EmeUtil.strcmp(tst.getClueType3(), inTok) == 0) {

            return tst.getClueWordId3();

        } else if (EmeUtil.strcmp(tst.getClueType4(), inTok) == 0) {

            return tst.getClueWordId4();
            
        } else if (EmeUtil.strcmp(tst.getClueType5(), inTok) == 0) {

            return tst.getClueWordId5();
            
        } else  {
            
        LOGGER.error("ERROR:  The input token " + inTok + " for the current"
            + " input image " + tst.getInputImage() + " could not be found in its"
            + "respective token structure.");

            throw new SbmeStandardizationException("ERROR:  The input token " + inTok + " for the current"
            + " input image " + tst.getInputImage() + " could not be found in its"
            + "respective token structure.");
        }
    }

    /*
     *  Initializes the individual fields in the address structure, sa.
     */
    private void initAddressStruct(StandAddress sa, int flds) {
    
        String empty = "";
        int zero = 0;

        if (flds != allButH) {
            sa.setHouseNumber(empty);
        }

        if (flds != allButH) {
            sa.setSecondHouseNumber(empty);
        }

        if (flds != allButH) {
            sa.setHouseNumPrefix(empty);
        }

        if (flds != allButH) {
            sa.setSecondHouseNumberPrefix(empty);
        }

        if (flds != allButH) {
            sa.setHouseNumSuffix(empty);
        }

        sa.setStreetNamePrefDirection(empty);
        sa.setStreetNamePrefType(empty);
        sa.setOrigStreetName(empty);
        sa.setMatchStreetName(empty);
        sa.setStorStreetName(empty);
        sa.setStreetNameSufType(empty);
        sa.setStreetNameSufDirection(empty);
        sa.setStreetNameExtensionIndex(empty);

        if (flds != allButW) {
             sa.setWithinStructDescript(empty);
        }
        
        if (flds != allButW) {
            sa.setWithinStructIdentif(empty);
        }

        sa.setPropDesPrefDirection(empty);
        sa.setPropDesPrefType(empty);
        sa.setOrigPropertyName(empty);
        sa.setMatchPropertyName(empty);
        sa.setPropertySufType(empty);
        sa.setPropertySufDirection(empty);
        sa.setStructureDescript(empty);
        sa.setStructureIdentif(empty);
        sa.setRuralRouteDescript(empty);
        sa.setRuralRouteIdentif(empty);
        sa.setBoxDescript(empty);
        sa.setBoxIdentif(empty);
        sa.setExtraInfo(empty);

        sa.setStreetNamePrefDirectionC(zero);
        sa.setStreetNamePrefTypeC(zero);
        sa.setStreetNameSufTypeC(zero);
        sa.setStreetNameSufDirectionC(zero);
        sa.setPropDesPrefDirectionC(zero);
        sa.setPropDesPrefTypeC(zero);
        sa.setPropertySufTypeC(zero);
        sa.setPropertySufDirectionC(zero);
        sa.setRuralRouteDescriptC(zero);
        sa.setBoxDescriptC(zero);
        sa.setStreetNameExtensionIndexC(zero);

        if (flds == all) {
            sa.setWithinStructDescriptC(zero);
        }
    }


    /*
     *  Strips leading zeros from numbers, except for the ones in the
     *  Secondary House Number field.
     */    
    private void stripLeadingZeros(StringBuffer image) {
    
        if ((image.charAt(0) != '0') || (image.length() == 0)) {
            return;
        }       
        
        int i = 0;
        
        while(image.charAt(i) == '0') {
          
            if (image.length() > 1) {
                image.deleteCharAt(i);
            } else {
                return;
            }          
        }
    }
    


    /*
     *  Receives a character string and return the fraction embedded in that string if
     *  it exists, NULL otherwise.  Remove fraction if it exists.
     */
    private StringBuffer fractionWithinNumber(StringBuffer string) {
    

        StringTokenizer st = new StringTokenizer(string.toString(), " /");
//        Tokenizer st = new Tokenizer(string.toString(), "/");        
        int nTok = st.countTokens();
        
        if (nTok < 3) { 
//        if (nTok == 1) {               
            return null; 
        }

        int i;
        int startW = 0;
        int endW = 0;
        char ch;
        int len = string.length();
        int lastInd = len - 1;
        boolean tokenFound = false;
        
        for (i = 0; i < len; i++) {
            
            ch = string.charAt(i);
            
            if (Character.isWhitespace(ch) && !tokenFound) {
                startW = i;
                
            } else if (ch == '/') {
                tokenFound = true;
                
            } else if (Character.isWhitespace(ch) || (i == lastInd)) {
                endW = i;
                break;
            }
        }
        StringBuffer retBuf = new StringBuffer(string.substring(startW + 1, endW + 1));
        string.delete(startW, endW+1);
        return retBuf;
    }


    /*
     *  Only needed if both a prefix and a suffix direction were loaded, and if the
     *  suffix direction is n, s, w, or e AND the street name isn't all numeric
     *  (this check saves UTAH addresses) and no wsi currently exists.
     *  If so, the suffix direction is moved over to the within structure identifier
     *  field.  This will prevent the following address from having 2 directions:
     *
     *  123 n main ave s:
     *   INPUT TOKENS  -  NU DR AU TY  |  DR
     *  OUTPUT TOKENS  -  HN PD NA ST  |  SD
     *  PATTERN TYPES  -  H*           |  H+
     */
    private boolean checkDirections(StandAddress sa, StringBuffer oSD, 
                                    int oSDcword, char abb) {
    
        String w;
        String W;
        if (domain.compareTo("FR") == 0) {
            w = "o";
            W = "O";                   
        } else {
            w = "w";
            W = "W";  // RRC Fixed            
        }      
        if (sa.getStreetNamePrefDirection().length() != 0 && sa.getStreetNameSufDirection().length() != 0
        && !AddressNormalizer.allDigits(sa.getOrigStreetName())) {
        
            if (EmeUtil.strcmp(oSD, "N") == 0 
            || EmeUtil.strcmp(oSD, "n") == 0 
            || EmeUtil.strcmp(oSD, "S") == 0 
            || EmeUtil.strcmp(oSD, "s") == 0 
            || EmeUtil.strcmp(oSD, "E") == 0 
            || EmeUtil.strcmp(oSD, "e") == 0 
            || EmeUtil.strcmp(oSD, W) == 0
            || EmeUtil.strcmp(oSD, w) == 0) {
            
                if (sa.getWithinStructIdentif().length() != 0) {
                
                      EmeUtil.strcat(sa.getWithinStructIdentif(), "*");
                }
                
                EmeUtil.strcat(sa.getWithinStructIdentif(), oSD);

                sa.setStreetNameSufDirection("");

                sa.setStreetNameSufDirectionC(0);
            }
        }
        return true;
    }


    /*
     *  If the prefix type is Bsrt (or some other deviation), then the type is changed to
     *  rt and "business" is placed in the extension field.
     */
    private boolean checkPT(TokenStruct tst, int cword, StringBuffer itok,
                            char abb, StandAddress sa)
        throws SbmeStandardizationException, SbmeMatchEngineException {
        
        if (cword == 33 && EmeUtil.strcmp(itok, "HR") == 0) {
        
            loadField(sa.getStreetNameExtensionIndex(), EXTENSION_OUTPUT_FIELD_SIZE, " ", 6, "EX", abb, 0);
            
            sa.setStreetNameExtensionIndexC(6);
            
            loadField(sa.getStreetNamePrefType(), TYPE_OUTPUT_FIELD_SIZE, " ", 227, "HR", abb, 0);
            sa.setStreetNamePrefTypeC(227);

            return true;
        }
        return false;
    }
    
    
    /**
     *  Only called when loading Prefix Types. If the input image is ST, LE, or LA, then
     *  it is placed in the name field.  
     *  EX:  101 st barnabas rd
     *       HN  PT NA       ST
     *  Here, "st" obviously belongs to the name field.
     *
     * @param tst aaa
     * @param cword aaa
     * @param itok aaa
     * @param ind aaa
     * @param otok aaa
     * @return ind2 aaa
     */
    boolean checkPTtoNA(TokenStruct tst, Integer cword, StringBuffer itok,
                               int ind, StringBuffer otok, int ind2) {
                               
        StringBuffer hold = new StringBuffer();
        int len;
       
        len = AddressClueWordsTable.trimln(tst.getInputImage());
        
        AddressNormalizer.upCase(tst.getInputImage(), hold);


        if (EmeUtil.strncmp(hold, "ST", len) == 0) {
                            
            EmeUtil.strncpy(itok, ind, "AU", 2);
            EmeUtil.strncpy(otok, ind2, "NA", 2);
            cword = Integer.valueOf("79");      
            
            return true;
            
        } else if (EmeUtil.strncmp(hold, "LE", len) == 0) {
                   
            EmeUtil.strncpy(itok, ind, "AU", 2);
            EmeUtil.strncpy(otok, ind2, "NA", 2);
            cword = Integer.valueOf("0");
            return true;
            
        } else if (EmeUtil.strncmp(hold, "LA", len) == 0) {
                   
            EmeUtil.strncpy(itok, ind, "AU", 2);
            EmeUtil.strncpy(otok, ind2, "NA", 2);
            cword = Integer.valueOf("0");
            return true;
        }
        return false;
    }


    /*
     *  Checks the PrimaryHouseNumber field to see if multiple numbers have been added;
     *  if so, it splits them up into PrimaryHouseNumber and SecondaryHouseNumber.
     *  EX:  101 120 main st --> "101" & "120". Also, if H1 and H2 are present and P2 is
     *  blank, then add '-' to P2. This will handle the QUEENS style addresses.
     *  Also, check for H1 = "0".
     */
    private void checkHouseNumber(StandAddress sa)
        throws SbmeStandardizationException, SbmeMatchEngineException {
        
        int blpos;

        if ((blpos = EmeUtil.strchrInt(sa.getHouseNumber(), ' ')) != -1) {
        
            // If you found a second house number , then return it after
            // first eliminating it from the original number
            EmeUtil.sprintf(sa.getSecondHouseNumber(), "%." + AddressClueWordsTable.trimln(sa.getHouseNumber(),
                            blpos + 1) + "s", sa.getHouseNumber().substring(blpos + 1));
        }
        
        if ((sa.getHouseNumber().length() != 0) && (sa.getSecondHouseNumber().length() != 0)
            && (sa.getSecondHouseNumberPrefix().length() == 0)) {

            sa.setSecondHouseNumberPrefix(0, '-');
        }

        if (sa.getHouseNumber().length() > 0) {
        
            if ((sa.getHouseNumber().charAt(0) == '0')
                && (EmeUtil.strcmp(sa.getHouseNumber(), "0") != 0)) {
                
                stripLeadingZeros(sa.getHouseNumber());
            }
        }
    }
    

    /*
     *  Checks all NU tokens for "spelled out" numbers (ie. ONE, TWO, etc) and ordinals 
     *  (ie. FIRST, SECOND, etc) and change them to numerics.
     */
    private void checkForSpelledOutNumber(TokenStruct tst, StringBuffer state, 
                                          StringBuffer zip) {
                                          
        ClueTable clue;
        StringBuffer hold = new StringBuffer();

        if (EmeUtil.strcmp(tst.getClueType2(), "NU") == 0
            || EmeUtil.strcmp(tst.getClueType2(), "OT") == 0) {

            AddressNormalizer.upCase(tst.getInputImage(), hold);
            
            if ((clue = clueW.searchForClue(hold, state, zip)) == null) {
                return;
            }
            
            EmeUtil.strncpy(tst.getInputImage(), clue.getTranslation(),
                            AddressClueWordsTable.trimln(tst.getInputImage()));
        }
    }

    /*
     *  Places a "#" in the wsd field if wsd is blank and wsi is non-blank.  Also, the
     *  wsi field is squashed if needed. If no wsi exists and wsd != blank, then copy wsd
     *  to wsi.
     */
    private void checkWithinStructure(StandAddress sa, char abb)
        throws SbmeStandardizationException, SbmeMatchEngineException {
        
        StringBuffer outstr = new StringBuffer(NUMBER_OUTPUT_FIELD_SIZE);

        if (sa.getWithinStructIdentif().length() != 0) {

            if(domain.compareTo("AU") == 0) {
                StringBuffer within = sa.getWithinStructIdentif();
                int len = within.length();
                if (len != 0) {
                    if (AddressNormalizer.dashFound(within) && sa.getHouseNumber().length() == 0) {
                        sa.setHouseNumber(within.toString());                
                        sa.setWithinStructIdentif("");
                    }
                }
            }
            if (sa.getWithinStructDescript().length() == 0) {
                if (sa.getWithinStructIdentif().toString().compareTo("G") == 0
                || sa.getWithinStructIdentif().toString().compareTo("UG") == 0
                || sa.getWithinStructIdentif().toString().compareTo("LG") == 0) {
                    loadField(sa.getWithinStructDescript(), TYPE_OUTPUT_FIELD_SIZE, "Fl", 0, "", abb, spaces);
                } else {                
                    if (domain.compareTo("AU") == 0) {                               
                        loadField(sa.getWithinStructDescript(), TYPE_OUTPUT_FIELD_SIZE, "U", 0, "", abb, spaces);
                    } else {
                        loadField(sa.getWithinStructDescript(), TYPE_OUTPUT_FIELD_SIZE, "#", 0, "", abb, spaces);                        
                    }
                }
            }
            
            StandIden.searchStandIden(sa.getWithinStructIdentif(), outstr, domain);
            
            EmeUtil.strcpy(sa.getWithinStructIdentif(), outstr);
            
        } else if (sa.getWithinStructDescript().length() != 0) {


            if (sa.getWithinStructDescript().charAt(0) == '#') {
     
                loadField(sa.getExtraInfo(), EXTRAINFO_OUTPUT_FIELD_SIZE, "#", 0, "WD", 'S', spaces);
                sa.setWithinStructDescript("-");
                
            } else if ((sa.getWithinStructDescript().charAt(0) == 'G')  
                || (sa.getWithinStructDescript().toString().compareTo("UG") == 0)
                || (sa.getWithinStructDescript().toString().compareTo("LG") == 0)) {
                sa.setWithinStructIdentif(sa.getWithinStructDescript().toString());
                sa.setWithinStructDescript("Fl");
            } else {
                sa.setWithinStructDescript(sa.getWithinStructDescript().toString());
                sa.setWithinStructIdentif("-");
            }
        }
    }


    /*
     *  Checks if any of the following fields has values:
     *      1.  Building Number Prefix
     *      2.  Building Number
     *      3.  Building Number Suffix
     *  If yes, all 3 of them must be moved because these fields are no longer valid.  
     *  If the WithinStructureIdentifier field is blank, then put them there, otherwise
     *  put them in the new StructureIdentifier field.
     */
    private void checkBuildingNumber(StandAddress sa, StringBuffer bNum, StringBuffer bNumPre,
                                            StringBuffer bNumSuf)
        throws SbmeStandardizationException, SbmeMatchEngineException {
        
        StringBuffer tp;
        StringBuffer hold = new StringBuffer(30);

        if (bNumPre.length() == 0 && bNum.length() == 0 && bNumSuf.length() == 0) {
            return;
        }

        EmeUtil.sprintf(hold, "%." + AddressClueWordsTable.trimln(bNumPre) + "s", "%."
            + AddressClueWordsTable.trimln(bNum) + "s", "%." + AddressClueWordsTable.trimln(bNumSuf)
            + "s", bNumPre, bNum, bNumSuf);

        EmeUtil.strcpy((sa.getWithinStructIdentif().length() == 0 ? sa.getWithinStructIdentif()
            : sa.getStructureIdentif()), hold);
    }

    /*
     *
     */
    private void checkBox(StandAddress sa, char abb)
        throws SbmeStandardizationException, SbmeMatchEngineException {
        String Box;
        String BX;  
        if (domain.compareTo("FR") == 0) {
            Box = "Boite";
            BX = "BP";                   
        } else {
            Box = "Box";
            BX = "BX";              
        }      
        
        // If bxi has a value and bxd is blank then place "box" in bxd
        if (sa.getBoxIdentif().length() != 0 && sa.getBoxDescript().length() == 0) {
        
            loadField(sa.getBoxDescript(), TYPE_OUTPUT_FIELD_SIZE, Box, 1, BX, 'S', spaces);            
            
            sa.setBoxDescriptC(1);
        }
    }


    /*
     *  If the usage flag is C (commercial only), and only the street name field was
     *  loaded, then move all to the building's name. Also, check for numerics and
     *  ordinal numbers in the street name field and expand if necessary.
     */
    private void checkStreetName(StandAddress sa, boolean nAonly, char usf)
        throws SbmeStandardizationException, SbmeMatchEngineException {
        
        int i;
        int p;
        int ln;
        int ln2;
        String North;
        String South; 
        String East;
        String West;   
        String W;                  
        if (domain.compareTo("FR") == 0) {
            North = "Nord";
            South = "Sud";  
            East = "Est";
            West = "Ouest"; 
            W = "O";                             
        } else {
            North = "North";
            South = "South";  
            East = "East";
            West = "West"; 
            W = "W";                    
        }      
        
        StringBuffer ordtyp = new StringBuffer(3);
        StringBuffer holdNm = new StringBuffer(NAME_OUTPUT_FIELD_SIZE + 1);
        StringBuffer holdNm2 = new StringBuffer(NAME_OUTPUT_FIELD_SIZE + 1);
        StringBuffer apos;
        StringBuffer matchName = sa.getMatchStreetName();
        StringBuffer origName = sa.getOrigStreetName();
        

        AddressNormalizer.upCase(matchName, holdNm);
        AddressNormalizer.upCase(origName, holdNm2);

        if (EmeUtil.strcmp(holdNm, holdNm2) != 0) {
        
            if (EmeUtil.strcmp(holdNm, "N") == 0)  {
                EmeUtil.strcpy(matchName, North);
            }
            
            if (EmeUtil.strcmp(holdNm, "S") == 0) {
                EmeUtil.strcpy(matchName, South);
            }
            
            if (EmeUtil.strcmp(holdNm, "E") == 0) {
                EmeUtil.strcpy(matchName, East);
            }
            
            if (EmeUtil.strcmp(holdNm, W) == 0) {
                EmeUtil.strcpy(matchName, West);
            }
        }

        // Check for spelled out OT's in the storage and matching names
        if (correctOTcontext(origName, holdNm)) {
            sa.setMatchStreetName("");
            sa.setStorStreetName("");

            EmeUtil.strcpy(matchName, holdNm);
            EmeUtil.strcpy(sa.getStorStreetName(), holdNm);
        }
        
        // Check for all numeric in the street name.  If so, add its
        // proper OT.  NOTE:  this is only valid if there is no
        // prefix type (this is to prevent "123 st hwy 4" from being
        // butchered), or if a UTAH address is present (PD and SD both
        // exists), or if no extension is present (EX must be blank: 123 old 5).
        if ((sa.getStreetNamePrefType().length() == 0) && (sa.getStreetNameExtensionIndex().length() == 0)
            && (sa.getStreetNamePrefDirection().length() == 0) || (sa.getStreetNameSufDirection().length() == 0)) {
            
            if (origName.length() != 0 && AddressNormalizer.allDigits(origName)) {
            
                AddressNormalizer.matchNumberWithOT(origName, ordtyp, domain);
                
                ln = AddressClueWordsTable.trimln(origName);
                
                sa.setOrigStreetName(EmeUtil.strcpy(origName, ln, ordtyp).toString());
            }

            if (matchName.length() != 0 && AddressNormalizer.allDigits(matchName)) {
            
                AddressNormalizer.matchNumberWithOT(matchName, ordtyp, domain);
                
                ln = AddressClueWordsTable.trimln(matchName);
                sa.setMatchStreetName(EmeUtil.strcpy(matchName, ln, ordtyp).toString());
            }

            if (sa.getStorStreetName().length() != 0 && AddressNormalizer.allDigits(sa.getStorStreetName())) {
            
                AddressNormalizer.matchNumberWithOT(sa.getStorStreetName(), ordtyp, domain);
                
                ln = AddressClueWordsTable.trimln(sa.getStorStreetName());
                sa.setStorStreetName(EmeUtil.strcpy(sa.getStorStreetName(), ln, ordtyp).toString());
            }
        }

        // Check for building names in the street name field
        if (usf == 'C' && nAonly) {
        
            sa.setOrigPropertyName(origName.toString());
            sa.setMatchPropertyName(matchName.toString());
             
            sa.setOrigStreetName("");
            sa.setMatchStreetName("");
            sa.setStorStreetName("");
        }

        // If usage flag is street name only (T) then append extra information
        // field, if any, to the street name fields
        if (usf == 'T' && sa.getExtraInfo().length() != 0) {
        
            ln = AddressClueWordsTable.trimln(sa.getExtraInfo());
            ln2 = AddressClueWordsTable.trimln(origName);
            
            EmeUtil.sprintf(origName, ln2, " %." + ln + "s", sa.getExtraInfo());
            ln2 = AddressClueWordsTable.trimln(matchName);
            
            EmeUtil.sprintf(matchName, ln2, " %." + ln + "s", sa.getExtraInfo());
            ln2 = AddressClueWordsTable.trimln(sa.getStorStreetName());
            
            EmeUtil.sprintf(sa.getStorStreetName(), ln2, " %." + ln + "s", sa.getExtraInfo());

            sa.setExtraInfo("");
        }
    }


    /*
     *  Checks strings for "spelled out" ordinal types, eg. "fifty second" is changed
     *  to its numeric ordinal type: 52nd.
     */
    private boolean correctOTcontext(StringBuffer str, StringBuffer out) {

        StringBuffer hold = new StringBuffer();
        int i;
        int j;
        int lOT1;
        int kOT1;
        String sOT1;
        String sOT2;
        int lenOT1[] = {6, 6, 5, 5, 5, 7, 6, 6};

        String TWENTY;
        String THIRTY; 
        String FORTY;
        String FIFTY;   
        String SIXTY; 
        String SEVENTY;
        String EIGHTY;   
        String NINETY;
        
        String FIRST;
        String SECOND; 
        String THIRD;
        String FOURTH;   
        String FIFTH; 
        String SIXTH;
        String SEVENTH;   
        String EIGHTH;
        String NINTH;
        if (domain.compareTo("FR") == 0) {
            TWENTY = "2VINGT";
            THIRTY = "3TRENTE";  
            FORTY = "4QUARANTE";
            FIFTY = "5CINQUANTE"; 
            SIXTY = "6SOIXANTE";
            SEVENTY = "7SOIXANTE-DIX";
            EIGHTY = "8QUATRE-VINGT"; 
            NINETY = "9QUATRE-VINGT-DIX";

            FIRST = "1erPREMIER";
            SECOND = "2emeDEUXIEME"; 
            THIRD = "3emeTROISIEME";
            FOURTH = "4emeQUATRIEME";   
            FIFTH = "5emeCINQUIEME"; 
            SIXTH = "6emeSIXIEME";
            SEVENTH = "7emeSEPTIEME";   
            EIGHTH = "8emeHUITIEME";
            NINTH = "9emeNEUVIEME";                                                     
        } else {
            TWENTY = "2TWENTY";
            THIRTY = "3THIRTY";  
            FORTY = "4FORTY";
            FIFTY = "5FIFTY"; 
            SIXTY = "6SIXTY";
            SEVENTY = "7SEVENTY";
            EIGHTY = "8EIGHTY"; 
            NINETY = "9NINETY";                      

            FIRST = "1stFIRST";
            SECOND = "2ndSECOND"; 
            THIRD = "3rdTHIRD";
            FOURTH = "4thFOURTH";   
            FIFTH = "5thFIFTH"; 
            SIXTH = "6thSIXTH";
            SEVENTH = "7thSEVENTH";   
            EIGHTH = "8thEIGHTH";
            NINTH = "9thNINTH";   
        }      
        String[] spelledOT1 = {TWENTY, THIRTY, FORTY, FIFTY, 
                               SIXTY, SEVENTY, EIGHTY, NINETY};

        String[] spelledOT2 = {FIRST, SECOND, THIRD, FOURTH, 
                               FIFTH, SIXTH, SEVENTH, EIGHTH, NINTH};
                               
        AddressNormalizer.upCase(str, hold);

        // Change FIFTY SECOND to 52nd, THIRTY FIRST to 31st, etc.
        for (i = 0; i < 8; i++) {
            
            sOT1 = spelledOT1[i];
            
            if (EmeUtil.strncmp(sOT1, 1, hold, lenOT1[i]) == 0) {
            
                if (hold.length() <= lenOT1[i]) {
                    continue;
                }
                
                lOT1 = hold.substring(lenOT1[i] + 1).length();
                kOT1 = lenOT1[i] + 1;

                for (j = 0; j < 9; j++) {
                    sOT2 = spelledOT2[j];
                
                    if (EmeUtil.strncmp(sOT2, 3, hold, kOT1, lOT1) == 0) {
                        
                        EmeUtil.strncpy(out, sOT1, 1);
                        EmeUtil.strncpy(out, 1, sOT2, 3);

                        // *(out+4) = '\0';
                        out.setLength(4);
                        return true;
                    }
                }
            }
        }
        return false;
    }   
}
