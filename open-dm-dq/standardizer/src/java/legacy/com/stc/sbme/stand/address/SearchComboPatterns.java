/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.stand.address;

import java.util.ArrayList;

import com.stc.sbme.api.SbmeMatchEngineException;
import com.stc.sbme.stand.util.ReadStandConstantsValues;
import com.stc.sbme.util.EmeUtil;

/**
 * Receives the array of pattern structs (pattsfound), the number of sub-patterns found 
 * in pattsfound (numpatts) and a pass value. If pass == 1 (ie. the first time called), 
 * then any search pattern is allowed, else any pattern string containing a "+" 
 * is dis-allowed.
 * EXAMPLE: given  123 87 MAIN AVE SOUTH:  the pattern types found from searching are:  
 *          NU H*DR. On the first pass we hope to find NU H*, hence, the pattern types 
 *          will then be: H+ H* DR. Subsequently, on the 2nd pass we hope to find: H* DR. 
 *
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
class SearchComboPatterns {

    private final int MAX_OUTPUT_PATTERNS;
    private final int MAX_PATT_SIZE;
    private final SearchPatternTable searchPatternTable;
    
    private final Posstr[] posarr;

    SearchComboPatterns(String domain) {
        MAX_OUTPUT_PATTERNS = ReadStandConstantsValues.getInstance(domain).getMaxOutputPatterns();
        MAX_PATT_SIZE = ReadStandConstantsValues.getInstance(domain).getMaxPattSize();
        posarr = new Posstr[MAX_OUTPUT_PATTERNS];
        searchPatternTable = new SearchPatternTable(domain);
    }
    
    /** 
     * 
     * 
     * @param pattsfound aaa
     * @param numpatts aaa
     * @param pass aaa
     * @param usf aaa
     * @param state aaa
     * @param zip aaa
     * @throws SbmeMatchEngineException exception
     */ 
    protected void searchComboPatterns(OutputPattern[] pattsfound, int numpatts, int pass, char usf,
                                              StringBuffer state, StringBuffer zip, int[] lineNums, ArrayList fieldList)
        throws SbmeMatchEngineException {
        
        int j;
        int i;
        int pos;
        int fr;
        int to;
        boolean starfound;
        
        StringBuffer string = new StringBuffer(MAX_PATT_SIZE);
        StringBuffer ip = new StringBuffer(MAX_PATT_SIZE);
        StringBuffer op = new StringBuffer(MAX_PATT_SIZE);
        PatternTable temp;

        // Instantiate TokenPosition array of objects
        for (i = 0; i < MAX_OUTPUT_PATTERNS; i++) {
            posarr[i] = new Posstr();
        }

        for (fr = 1, to = numpatts; (to - fr) > 0; to--) {

            // A problem could potentially exist with order of elements in the array
            // of structures:   the first element could contain tokens 2 - 4 and the
            // second element could contain token 1.  We want these switched, hence,
            // building a "position array" will resolve this.
            buildPositionArray(posarr, pattsfound, numpatts);

            // First, make sure that at least 1 '*' token pattern exists
            for (starfound = false, j = fr; j <= to; j++) {
            
                pos = posarr[j - 1].getOrder() - 1;

                if (pattsfound[pos].getPatternType().charAt(1) == '*') {
                    starfound = true;
                }
            }
            
            if (!starfound) {
                return;
            }

            // A problem could potentially exist with order of elements in the array
            // of structures:   the first element could contain tokens 2 - 4 and the
            // second element could contain token 1.  We want these switched, hence,
            // building a "position array" will resolve this.
            ArrayList newLineNums = new ArrayList();
            ArrayList newFieldList = new ArrayList();
            buildStringFromPatterns(string, pattsfound, fr, to, lineNums, newLineNums, fieldList, newFieldList);

            // FOR ALL PASSES:   Only search for the string if at least one "*"
            //                   pattern exists in the current string.
            // FOR THE 1st PASS: patterns containing "+" pattern types ARE allowed
            //FOR THE 2nd PASS: patterns containing "+" pattern types ARE NOT allowed
            if ((EmeUtil.strchrInt(string, '*') != -1)
                && (pass == 1 ? true : (EmeUtil.strchrInt(string, '+') == -1 ? true : false)) 
                && (temp = searchPatternTable.searchPatternTable(string, ip, op, usf,
                                                                 state, zip)) != null
//                && (AddressPatternFinder.validatePattern(op.toString(), newLineNums, newFieldList))
                && !outputPatternAlreadyUsed(temp, pattsfound, fr)
                && !outputTokAlreadyUsed(temp, pattsfound, fr)) {
                        modifyStructure(temp, pattsfound, ip, op, fr, to);
            } else if ((to - fr) <= 1) {
                fr++;
                to = numpatts + 1;
            }
        }
    }

    private boolean outputTokAlreadyUsed(PatternTable pattern, OutputPattern[] patterns, int index) {
        String tokens[] = EmeUtil.stringArrayTokens(pattern.getOutputTokenString().toString(), " ");
        for (int i=0; i <= index; i++) {
            for (int j=0; j < tokens.length; j++) {
                if (patterns[i].getOutputPatt().indexOf(tokens[j]) != -1) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean outputPatternAlreadyUsed(PatternTable pattern, OutputPattern[] patterns, int index) {
        for (int i=0; i <= index; i++) {
            if (pattern.getPatternType().toString().equals(patterns[i].getPatternType().toString())) {
              return true;
           }
        }
        return false;
    }
    
    /**
     *  buildStringFromPatterns will construct a character string
     *  from elements "fr" to "to" in the array of structs, pattsfound.
     * 
     * @param string aaa
     * @param pattsfound aaa
     * @param fr aaa
     * @param to aaa
     * @throws SbmeMatchEngineException an exception
     */
    protected void buildStringFromPatterns(StringBuffer string, OutputPattern[] pattsfound, 
                                                  int fr, int to, int lineNumsArray[], ArrayList newLineNums, 
                                                  ArrayList oldFieldList, ArrayList newFieldList) throws SbmeMatchEngineException {
                                                  
        int pos;
        int i;
        int len;
        for (i = 0; fr <= to; fr++) {
            len = 0;
            // "pos", calculated from the array posarr, is used to properly
            // increment in the array of structs, pattsfound.
            pos = posarr[fr - 1].getOrder() - 1;

            // Determine length of sub-string
            if ((pattsfound[pos].getPatternType().charAt(1) != '*')
                && (pattsfound[pos].getPatternType().charAt(1) != '+')) {
                
                for (int w = pattsfound[pos].getBeg() - 1; w < pattsfound[pos].getEnd(); w++) {
                    len += 3;
                    newLineNums.add(new Integer(lineNumsArray[w]));
                    newFieldList.add(oldFieldList.get(w));
                }
            } else {
                len = 3;
                newLineNums.add(new Integer(-1));
                newFieldList.add("");
            }
            
            string = string.append(EmeUtil.sprintf("%." + len + "s ",
                (pattsfound[pos].getPatternType().charAt(1) == '*'
                || (pattsfound[pos].getPatternType().charAt(1) == '+'))
                ? (pattsfound[pos].getPatternType()).toString()
                : (pattsfound[pos].getInputPatt()).toString()));

            i += len;
            
            
        }
        string.setLength(i - 1);
    }

    /**
     *  modifyStructure will modify the proper element in the pattsfound structure if a
     *  valid combination pattern was found.
     *
     * @param patt aaa
     * @param pattsfound aaa
     * @param ip aaa
     * @param op aaa
     * @param fr aaa
     * @param to aaa
     * @throws SbmeMatchEngineException an exception
     */
    protected void modifyStructure(PatternTable patt, OutputPattern[] pattsfound, 
                                          StringBuffer ip, StringBuffer op, int fr, int to)
        throws SbmeMatchEngineException {
        
            
        int pos = 0, j;

        while ((to - fr) >= 0) {
        
            j = posarr[fr - 1].getOrder() - 1;

            if (pattsfound[j].getPatternType().charAt(1) != '*') {
            
                EmeUtil.sprintf(pattsfound[j].getPatternType(), "%.2s", patt.getPatternType());
                
                EmeUtil.sprintf(pattsfound[j].getOutputPatt(), "%.2s", op.substring(pos));
                
                EmeUtil.sprintf(pattsfound[j].getPriority(), "%.2s", patt.getPriority());
            }
            fr++; 
            pos += 3;
        }
        
        /*
         * Go through all patterns in pattsfound and arrange by order
         * Traverse each pattern in order and match to the pattern in patt
         * If patt output is a non * output, then overwrite the output in pattsfound
         */
            
            /*
        AddressOutputPattern[] alignedPatterns = new AddressOutputPattern[pattsfound.length];
        int count = 0;
        for (int i = fr - 1; i < to; i++) {
            alignedPatterns[posarr[i].getOrder() - 1] = pattsfound[i];
            count++;
        }
        int pos = 0;
        count = 0;
        while (pos < op.length() - 1) {
            if (op.charAt(pos+1) == '*') {
                //Compare to output pattern type
                StringBuffer patType = alignedPatterns[count].getPatternType();
                if (patType.charAt(pos) == op.charAt(pos) && patType.charAt(pos+1) == '*') {
                    pos+=3;
                } else {
                    throw new SbmeMatchEngineException("InputPattern mismatch");
                }
            } else {
                for (int j = 0; j < alignedPatterns[count].getOutputPatt().length(); j++) {
                    alignedPatterns[count].getOutputPatt().setCharAt(j, op.charAt(pos));
                    pos++;
                }
                pos++;
            }
            count++;
        }
             **/
    }

    /**
     *  buildPositionArray loads the posarr structure, which is
     *  needed to retrieve the elements in the pattsfound array
     *  of structure AS THEY ARE ORDERED IN THE ADDRESS STRING,
     *  not as the were loaded.
     *
     * @param posarr aaa
     * @param pattsfound aaa
     * @param numpatts aaa
     */
    protected void buildPositionArray(Posstr[] posarr, OutputPattern[] pattsfound, 
                                             int numpatts) {
                                             
        int hold;
        int i;
        int j;
        int k = 0;
        
        boolean switchmade = true;

        // Initialize all the beginning token and order values to 0 for all elements.
        for (i = 0; i < MAX_OUTPUT_PATTERNS; i++) {
        
            posarr[i].setBegTok((i < numpatts) ? pattsfound[i].getBeg() : 0);
            
            posarr[i].setOrder((i < numpatts) ? i + 1 : 0);
        }
        
        // load the structure
        while (k < (numpatts - 1) && switchmade) {
        
            switchmade = false; 
            k++;
            
            for (j = 0; j < numpatts - k; j++) {
            
                if (posarr[j].getBegTok() > posarr[j + 1].getBegTok()) {
                
                    switchmade = true;

                    hold = posarr[j].getBegTok();
                    posarr[j].setBegTok(posarr[j + 1].getBegTok());
                    posarr[j + 1].setBegTok(hold);

                    hold = posarr[j].getOrder();
                    posarr[j].setOrder(posarr[j + 1].getOrder());
                    posarr[j + 1].setOrder(hold);
                }
            }
        }
    }
}
