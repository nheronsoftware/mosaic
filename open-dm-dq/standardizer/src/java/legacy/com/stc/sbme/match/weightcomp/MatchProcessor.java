/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.match.weightcomp;

import java.text.ParseException;

import com.stc.sbme.api.SbmeMatchRecord;
import com.stc.sbme.api.SbmeMatchingEngine;
import com.stc.sbme.api.SbmeMatchingException;
import com.stc.sbme.match.util.ReadMatchConstantsValues;
import com.stc.sbme.util.EmeUtil;
import com.stc.sbme.util.SbmeLogUtil;
import com.stc.sbme.util.SbmeLogger;

/**
 * Main class to process any request to match candidate to reference
 * records
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
public class MatchProcessor
implements Cloneable {
    
    // Indices of the first character of a comparison type. Value: {1, 2, 3}
    private static final int PRIMARY = ReadMatchConstantsValues.getPrimary();
    private static final int SECONDARY = ReadMatchConstantsValues.getSecondary();
    private static final int TERTIARY = ReadMatchConstantsValues.getTertiary();
    private static final int N_FIELDS = ReadMatchConstantsValues.getNFields();  
      
    /* Associated with types of matching comparisons */
    private static final char INVERSION = ReadMatchConstantsValues.getInversion();
    private static final char MAJOR_INVERSION = ReadMatchConstantsValues.getMajorInversion();
    private static final char CROSS = ReadMatchConstantsValues.getCross();
    private static final char CRITICAL = ReadMatchConstantsValues.getCritical();
    private int pat; 
 
    private final SbmeLogger mLogger = SbmeLogUtil.getLogger(this);  
    private final boolean mDebug = mLogger.isDebugEnabled();
       
    //private static LinearSumAssignmentAlgorithms lsap =
    //LinearSumAssignmentAlgorithms.getInstance();
    
    /**
     * Default constructor
     */
    public MatchProcessor() {
    }
    
    /**
     * Reads two strings and compute the matching weight between them
     *
     * @param fieldsIDs The types of fields
     * @param candRecArrayVals an array of reference records
     * @param refRecArrayVals an array of reference records
     * @param mEng an instance of the SbmeMatchingEngine class
     * @return a real number that measures the degree of match
     * @throws SbmeMatchingException an exception
     * @throws ParseException if a string parsing failed
     */
    public double[][] performMatch(String[] fieldsIDs, String[][] candRecArrayVals,
    String[][] refRecArrayVals, SbmeMatchingEngine mEng)
    throws SbmeMatchingException, ParseException {
        
        int i;
        int j;
        int k;
        boolean iss;
        int criticalAchieved;
        int bonus;
        int questionablePattHold;
        int smallerLength;
        
        int[] jIndex;
        int theIndx;
        
        int fieldsNumber;
        int blockArecNumber;
        int blockBrecNumber;
        String ssr;
        
        double bonusWeight;
        double tempCompWeight;
        double wgtIncrement = 0.0;
        double secondWgtIncrement;
        double agrW;
        double disW;
        MatchVariables matchVar = mEng.getMatchVar(mEng);
        MatchConfigFile mConfig = mEng.getMatchConfig(mEng);
        StringBuffer sCompj;
 
        /*
         * Get the number of the matching records and associated fields
         */
        fieldsNumber = fieldsIDs.length;
        blockArecNumber = candRecArrayVals.length;
        blockBrecNumber = refRecArrayVals.length;
 
        if (mDebug) {
            mLogger.debug("Test if all the provided match types are valid");
        }
 
        // Make sure that all the matching fields are 
        // valid types in the matchConfigFile
        for (i = 0; i < fieldsNumber; i++) {
            
            iss = false;
            ssr = fieldsIDs[i];
            
            for (j = 0; j < N_FIELDS; j++) {

                if (EmeUtil.compareStrings(ssr, matchVar.fieldName[j])) {
                    iss = true;
                    break;
                }
            }
            
            if (!iss) {
                throw new SbmeMatchingException("The match type: " + fieldsIDs[i]
                    + " is invalid! \n You need to define it within the matchConfigFile.cfg file");                
            }
        }

        if (mDebug) {
            mLogger.debug("Start the matching process!");
        }
        
        // Define the number of indexes associated with matching variables
        jIndex = new int[fieldsNumber];
        // Initialize them
        for (i = 0; i < fieldsNumber; i++) {
            jIndex[i] = -1;
        }       
        
        /* Define a variable that holds the fields of candidate records */
        String[] candRecVals = new String[fieldsNumber];
        String[] fnVal = new String[2];
        double[][] computedWeight = new double[blockArecNumber][blockBrecNumber];
        
        /* Define a variable that holds the fields of reference records */
        String[] refRecVals = new String[fieldsNumber];
        String[] mdVal = new String[2];
        
        /* Define a variable that holds the field's names */
        String fieldName;

        // Returns the corresponding indexes of the fields in the config file
        jIndex = mConfig.getFieldIndices(fieldsIDs, mEng);
        
        for (i = 0; i < blockArecNumber; i++) {
            
            // Array of fields of record i in the candidate block
            candRecVals = candRecArrayVals[i];
            
            // Loop over a corresponding block in the corresponding file 'sortb.dat'.
            for (k = 0; k < blockBrecNumber; k++) {
                
                computedWeight[i][k] = 0.0;
                tempCompWeight = 0.0;
                bonusWeight = 0.0;
                criticalAchieved = 0;
                pat = 0;
                bonus = 0;
                
                
                // Array of fields of record k in the candidate block
                refRecVals = refRecArrayVals[k];
                

                // Loop over all the matching fields in the actual record from
                // 'sorta.dat'
                for (j = 0; j < fieldsNumber; j++) {
                    
                    /* field's name */
                    fieldName = fieldsIDs[j];
                    theIndx = jIndex[j];
                    agrW = matchVar.agrWeight[theIndx];
                    disW = matchVar.disWeight[theIndx];
                    
                    
                    sCompj = matchVar.comparatorType[j];
                    
                    /* Hold the values of first name and middle name for later use */
                    if (fieldName.compareTo("FirstName") == 0) {
                        
                        fnVal[0] = candRecVals[j];
                        fnVal[1] = refRecVals[j];
                        
                    } else if (fieldName.compareTo("MiddleName") == 0) {
                        
                        mdVal[0] = candRecVals[j];
                        mdVal[1] = refRecVals[j];
                    }                    
                    
                    // Counts the number of matching patterns
                    pat *= 2;
                    questionablePattHold = pat;

                    // Call method weight() to calculate the weight associated
                    // with (j,k) pair.
                    wgtIncrement = WeightComputation.weight(0, candRecVals, fieldName, j, theIndx,
                                                            refRecVals, mEng);

                    // Test if the weight defines an agreement match --> increment
                    // the pat variable.
                    if (wgtIncrement > 0) {
                        pat++;
                    }
                    
                    // Add the calculated weight to the sum of weights for the pair
                    tempCompWeight += wgtIncrement;
                    
                    // Test the case where we have a disagreement weight and where
                    // we use the INVERSION option in the comparator
                    if ((wgtIncrement == disW)
                        && ((sCompj.charAt(SECONDARY)
                             == Character.toLowerCase(INVERSION))
                           || (sCompj.charAt(TERTIARY)
                             == Character.toLowerCase(INVERSION))
                           || (sCompj.charAt(3)
                             == Character.toLowerCase(INVERSION)))) {
                        
                        // First calculate the weight associated with the first field
                        wgtIncrement = WeightComputation.weight(1, candRecVals, fieldName, j, theIndx,
                                                                refRecVals, mEng);
                      

                        // Then, calculate the weight associated with the second
                        // field
                        secondWgtIncrement = WeightComputation.weight(2, candRecVals, fieldName, j,
                                                                      theIndx, refRecVals, mEng);
                        
                        // Test if both fields have an agreement weights
                        if ((wgtIncrement > 0.0) && (secondWgtIncrement > 0.0)) {
                            
                            // Remove the contribution of disagreement weights
                            // to the overall weight from both adjacent fields
                            tempCompWeight -= disW + matchVar.disWeight[jIndex[j + 1]];
                            
//                            tempCompWeight -= matchVar.disWeight[j + 1];
                            
                            // Then, add the contribution of both agreement weights
                            // from the adjacent fields and divide them by 1 if we
                            // use MAJOR_INVERSION, or by 2 if we use INVERSION
                            tempCompWeight += (wgtIncrement + secondWgtIncrement)
                            / (((sCompj.charAt(SECONDARY) == MAJOR_INVERSION)
                            || (sCompj.charAt(TERTIARY) == MAJOR_INVERSION)
                            || (sCompj.charAt(3) == MAJOR_INVERSION)) ? 1.0 : 2.0);
                        }
                    }
                    
                    // Test the use of the CROSS option, which corresponds to:
                    // if 2 or more important fields match, their weight is doubled,
                    // and if any of the fields with a 'x' does not match, the rule
                    // does not apply.
                    if ((matchVar.nCrosses != 0)
                        && ((sCompj.charAt(SECONDARY) == CROSS)
                            || (sCompj.charAt(TERTIARY) == CROSS)
                            || (sCompj.charAt(3) == CROSS))) {
                        
                        
                        // In case of agreement weight, increment some params
                        if (wgtIncrement == agrW) {
                            
                            bonus++;
                            bonusWeight += agrW;
                        }
                    }
                    
                    // Test the use of the CRITICAL option, which corresponds to:
                    // The use of code 'k' for a field authorize a sub-group of
                    // fields with 'x' to be doubled as far as there is a field with
                    // 'k' that match.
                    if ((matchVar.criticalTest != 0)
                        && ((sCompj.charAt(SECONDARY) == CRITICAL)
                            || (sCompj.charAt(TERTIARY) == CRITICAL)
                            || (sCompj.charAt(3) == CRITICAL))) {
                        
                        // In case of agreement weight, increment some params
                        if (wgtIncrement == agrW) {
                            
                            bonus++;
                            bonusWeight += agrW;
                            criticalAchieved = 1;
                        }
                    }

                    
                    
                } // end of loop over match fields

                // In case we used the CRITICAL option
                if (matchVar.criticalTest != 0) {
                    
                    // Plus, there were some agreement weights, and
                    // nCrosses < bonus+1 (?)
                    if ((criticalAchieved != 0) && (matchVar.nCrosses < (bonus + 1))) {
                        
                        // Add to the total weight
                        tempCompWeight += bonusWeight;
                    }
                    
                } else {
                    // In all the other situation else than CRITICAL. NEED REVIEW.
                    
                    if (matchVar.nCrosses == bonus) {
                        tempCompWeight += bonusWeight;
                    }
                }
                
                // In case we use the provided downweight parameters (dw >0), plus
                // the total weight is in the range ]12, -10[
                if ((matchVar.dw > 0) && (tempCompWeight < 12.0)
                    && (tempCompWeight > -10.0)) {
                    
                    // Use the downweighting function to update the total weight
                    tempCompWeight += downWeight(fnVal, mdVal, matchVar);
                }
                
                // NEED REVIEW!
                matchVar.dwi[matchVar.dw1] = 0;
                matchVar.dwi[matchVar.dw2] = 0;
                matchVar.dwi[matchVar.dw3] = 0;
                
                computedWeight[i][k] = tempCompWeight;
                
            }
        }
        return computedWeight;
    }
    
    
    /**
     * We can use downweight adjustment parameters associated with
     * data-specific matching variables. We have dw=1 (upweight adjustment
     * for transposed middle & first names, dw=3 (downweight for simultaneous
     * disagreement of first name, sex and age. dw=2, corresponds to both
     * upweight and downweight.
     *
     * @param fnVal
     * @param mdVal
     * @return matchVar
     */
    private static double downWeight(String[] fnVal, String[] mdVal, MatchVariables matchVar) {
        
        double dummy = 0.0;
        
        // Refers to first name
        int m1 = matchVar.m1;
        // Refers to middle name
        int m2 = matchVar.m2;
        
        // Upweight adjustment for transposed middle & first names
        if (matchVar.dw < 3) {
            
            // Compare first name's first character with middle name one character
            if (EmeUtil.strncmp(fnVal[1], mdVal[0], 1) == 0) {
                
                // Reverse the fields' indices to double check
                if (EmeUtil.strncmp(mdVal[1], fnVal[0], 1) == 0) {
                    
                    dummy += matchVar.fm1;
                    
                } else if (EmeUtil.strncmp(mdVal[1], fnVal[0], 1) != 0) {
                    // If the result is different use a different parameter
                    
                    dummy += matchVar.fm2;
                }
                
            } else if (EmeUtil.strncmp(fnVal[1], mdVal[0], 1) != 0) {
                // If it does not match, reverse the indices.
                
                if (EmeUtil.strncmp(mdVal[1], fnVal[0], 1) == 0) {
                    
                    // Add the real value to this variable
                    dummy += matchVar.fm2;
                }
            }
        }
        
        // This part handles downweight for simultaneous disagreement of
        // first name, sex and age
        if (matchVar.dw > 1) {
            
            // NEED MORE INVESTIGATION
            if ((matchVar.dwi[matchVar.dw1] == 1)
            && (matchVar.dwi[matchVar.dw2] == 1)) {
                
                dummy -= ((matchVar.dwi[matchVar.dw3] == 1) ? matchVar.fw1 : matchVar.fw2);
                
            } else if ((matchVar.dwi[matchVar.dw1] == 1)
            && (matchVar.dwi[matchVar.dw3] == 1)) {
                
                dummy -= matchVar.fw3;
            }
        }
        return dummy;
    }


    /**
     *
     *
     * @param matchFieldsIDs the type of match field
     * @param candRecVals the candidate list of records
     * @param refRecVals the reference list of records
     * @param mEng an instance of the SbmeMatchingEngine class
     * @throws SbmeMatchingException any generic exeption in the matching engine
     * @return a double
     */
    public double performMatch(String[] matchFieldsIDs, String[] candRecVals,
        String[] refRecVals, SbmeMatchingEngine mEng)
        throws SbmeMatchingException {

        throw new SbmeMatchingException("This method is not functional \n"
            + " Use the generic method performMatch(String[] fieldID, String[][] candRecs,"
            + " String[][] refRecs, SbmeMatchingEngine mEng)");
    }

 
    /**
     *
     *
     * @param matchFieldsIDs the type of match field
     * @param candRecVals the values of the fields of candidate record
     * @param refRecArrayVals the values of the fields of an array of ref records
     * @param mEng an instance of the SbmeMatchingEngine class
     * @throws SbmeMatchingException any generic exeption in the matching engine
     * @return an array of doubles
     */
    public double[] performMatch(String[] matchFieldsIDs, String[] candRecVals,
        String[][] refRecArrayVals, SbmeMatchingEngine mEng)
        throws SbmeMatchingException {

            throw new SbmeMatchingException("This method is not functional \n"
                + " Use the generic method performMatch(String[] fieldID, String[][] candRecs,"
                + " String[][] refRecs, SbmeMatchingEngine mEng)");
    }

 
 
    
    /**
     *
     *
     * @param aStr a candidate object
     * @param bStr a reference object
     * @param mEng an instance of the SbmeMatchingEngine class
     * @throws SbmeMatchingException any generic exeption in the matching engine
     * @return a double
     */
    public double performMatch(SbmeMatchRecord aStr, SbmeMatchRecord bStr,
    SbmeMatchingEngine mEng)
        throws SbmeMatchingException {

        throw new SbmeMatchingException("This method is not functional \n"
            + " Use the generic method performMatch(String[] fieldID, String[][] candRecs,"
            + " String[][] refRecs, SbmeMatchingEngine mEng)");
    }


    /**
     *
     *
     * @param aStr a candidate object
     * @param bStr an array of reference objects
     * @param mEng an instance of the SbmeMatchingEngine class
     * @throws SbmeMatchingException any generic exeption in the matching engine
     * @return an array of doubles
     */
    public double[] performMatch(SbmeMatchRecord aStr, SbmeMatchRecord[] bStr,
    SbmeMatchingEngine mEng)
        throws SbmeMatchingException {

        throw new SbmeMatchingException("This method is not functional \n"
            + " Use the generic method performMatch(String[] fieldID, String[][] candRecs,"
            + " String[][] refRecs, SbmeMatchingEngine mEng)");
    }
    
    /**
     *
     *
     * @param aStr an array of candidate objects
     * @param bStr an array of reference objects
     * @param mEng an instance of the SbmeMatchingEngine class
     * @throws SbmeMatchingException any generic exeption in the matching engine
     * @return a 2D array of doubles
     */
    public double[][] performMatch(SbmeMatchRecord[] aStr, SbmeMatchRecord[] bStr,
    SbmeMatchingEngine mEng)
        throws SbmeMatchingException {

        throw new SbmeMatchingException("This method is not functional \n"
            + " Use the generic method performMatch(String[] fieldID, String[][] candRecs,"
            + " String[][] refRecs, SbmeMatchingEngine mEng)");
    }
    
    /**
     *
     *
     * @param str a record object
     * @param mEng an instance of the SbmeMatchingEngine class
     * @throws SbmeMatchingException any generic exeption in the matching engine
     * @return a double
     */
    public double[][] performMatch(SbmeMatchRecord[] str, SbmeMatchingEngine mEng)
        throws SbmeMatchingException {

        throw new SbmeMatchingException("This method is not functional \n"
            + " Use the generic method performMatch(String[] fieldID, String[][] candRecs,"
            + " String[][] refRecs, SbmeMatchingEngine mEng)");
    }
    
    /**
     *
     *
     * @param matchFieldsIDs an array of fields' types
     * @param str the string values of the fields
     * @param mEng an instance of the SbmeMatchingEngine class
     * @throws SbmeMatchingException any generic exeption in the matching engine
     * @return a double
     */
    public double[][] performMatch(String[] matchFieldsIDs, String[][] str,
    SbmeMatchingEngine mEng)
        throws SbmeMatchingException {

        throw new SbmeMatchingException("This method is not functional \n"
            + " Use the generic method performMatch(String[] fieldID, String[][] candRecs,"
            + " String[][] refRecs, SbmeMatchingEngine mEng)");
    }
}
