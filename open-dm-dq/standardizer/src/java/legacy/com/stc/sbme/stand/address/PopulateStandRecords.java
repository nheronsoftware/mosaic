/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.stand.address;


/**
 * Loads the address clue words table and assign the data to the
 * corresponding variables
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
public class PopulateStandRecords {
    

    /**
     * Define a factory method that return an instance of the class
     * @return an PopulateStandRecords instance
     */
    public static PopulateStandRecords getInstance() {
        return new PopulateStandRecords();
    }

    /** 
     * 
     * 
     * @param standAddr aaa 
     * @param orderedKeys aaa 
     * @param orderedValues aaa
     * @return an integer
     */ 
    public static int copyFields(StandAddress standAddr, StringBuffer[][] orderedKeys,
                                 StringBuffer[][] orderedValues) {
        
        /* Generic string */
        StringBuffer strB;
        
        int k = 0;
        
        /* Read from field house number prefix */
        strB = standAddr.getHouseNumPrefix();
        
        if (!(strB.toString().compareTo("") == 0)) {
        
            orderedKeys[0][k] = new StringBuffer("HouseNumberPrefix");
            orderedValues[0][k] = new StringBuffer(strB.toString());
            
            /* free the string and increment the index*/
            strB.setLength(0);
            k++;
        }
        

        /* Read from field house number prefix */
        strB = standAddr.getHouseNumber();
        
        if (!(strB.toString().compareTo("") == 0)) {

            orderedKeys[0][k] = new StringBuffer("HouseNumber");
            orderedValues[0][k] = new StringBuffer(strB.toString());


            /* free the string and increment the index*/
            strB.setLength(0);
            k++;
        }

        /* Read from field house number prefix */
        strB = standAddr.getSecondHouseNumberPrefix();

        if (!(strB.toString().compareTo("") == 0)) {
        
            orderedKeys[0][k] = new StringBuffer("SecondHouseNumberPrefix");
            orderedValues[0][k] = new StringBuffer(strB.toString());
            
            /* free the string and increment the index*/
            strB.setLength(0);
            k++;
        }

        /* Read from field house number prefix */
        strB = standAddr.getSecondHouseNumber();
        
        if (!(strB.toString().compareTo("") == 0)) {
            orderedKeys[0][k] = new StringBuffer("SecondHouseNumber");
            orderedValues[0][k] = new StringBuffer(strB.toString());
            
            /* free the string and increment the index*/
            strB.setLength(0);
            k++;
        }
        
        /* Read from field house number prefix */
        strB = standAddr.getHouseNumSuffix();
        
        if (!(strB.toString().compareTo("") == 0)) {
            orderedKeys[0][k] = new StringBuffer("HouseNumberSuffix");
            orderedValues[0][k] = new StringBuffer(strB.toString());
            
            /* free the string and increment the index*/
            strB.setLength(0);
            k++;
        }

        /* Read from field house number prefix */
        strB = standAddr.getStreetNamePrefDirection();
        
        if (!(strB.toString().compareTo("") == 0)) {
            orderedKeys[0][k] = new StringBuffer("StreetNamePrefDirection");
            orderedValues[0][k] = new StringBuffer(strB.toString());
            
            /* free the string and increment the index*/
            strB.setLength(0);
            k++;
        }

        /* Read from field house number prefix */
        strB = standAddr.getStreetNamePrefType();
  
        if (!(strB.toString().compareTo("") == 0)) {
            orderedKeys[0][k] = new StringBuffer("StreetNamePrefType");
            orderedValues[0][k] = new StringBuffer(strB.toString());
            
            /* free the string and increment the index*/
            strB.setLength(0);
            k++;
        }

        /* Read from field house number prefix */
        strB = standAddr.getOrigSecondStreetName();

        if (!(strB.toString().compareTo("") == 0)) {
            orderedKeys[0][k] = new StringBuffer("OrigSecondStreetName");
            orderedValues[0][k] = new StringBuffer(strB.toString());
            
            /* free the string and increment the index*/
            strB.setLength(0);
            k++;
        }
        
        /* Read from field house number prefix */
        strB = standAddr.getOrigStreetName();

        if (!(strB.toString().compareTo("") == 0)) {
            orderedKeys[0][k] = new StringBuffer("OrigStreetName");
            orderedValues[0][k] = new StringBuffer(strB.toString());
            
            /* free the string and increment the index*/
            strB.setLength(0);
            k++;
        }

        /* Read from field house number prefix */
        strB = standAddr.getMatchStreetName();

        if (!(strB.toString().compareTo("") == 0)) {
            orderedKeys[0][k] = new StringBuffer("MatchStreetName");
            orderedValues[0][k] = new StringBuffer(strB.toString());
            
            /* free the string and increment the index*/
            strB.setLength(0);
            k++;
        }


        /* Read from field house number prefix */
        strB = standAddr.getStreetNameSufType();

        if (!(strB.toString().compareTo("") == 0)) {
            orderedKeys[0][k] = new StringBuffer("StreetNameSufType");
            orderedValues[0][k] = new StringBuffer(strB.toString());
            
            /* free the string and increment the index*/
            strB.setLength(0);
            k++;
        }
        
        /* Read from field house number prefix */
        strB = standAddr.getSecondStreetNameSufType();

        if (!(strB.toString().compareTo("") == 0)) {
            orderedKeys[0][k] = new StringBuffer("SecondStreetNameSufType");
            orderedValues[0][k] = new StringBuffer(strB.toString());
            
            /* free the string and increment the index*/
            strB.setLength(0);
            k++;
        }
 
        /* Read from field house number prefix */
        strB = standAddr.getStreetNameSufDirection();

        if (!(strB.toString().compareTo("") == 0)) {
            orderedKeys[0][k] = new StringBuffer("StreetNameSufDirection");
            orderedValues[0][k] = new StringBuffer(strB.toString());
            
            /* free the string and increment the index*/
            strB.setLength(0);
            k++;
        }
 
        /* Read from field house number prefix */
        strB = standAddr.getStreetNameExtensionIndex();

        if (!(strB.toString().compareTo("") == 0)) {
            orderedKeys[0][k] = new StringBuffer("StreetNameExtensionIndex");
            orderedValues[0][k] = new StringBuffer(strB.toString());
            
            /* free the string and increment the index*/
            strB.setLength(0);
            k++;
        }

        /* Read from field house number prefix */
        strB = standAddr.getWithinStructDescript();

        if (!(strB.toString().compareTo("") == 0)) {
            orderedKeys[0][k] = new StringBuffer("WithinStructDescript");
            orderedValues[0][k] = new StringBuffer(strB.toString());
            
            /* free the string and increment the index*/
            strB.setLength(0);
            k++;
        }
 
        /* Read from field house number prefix */
        strB = standAddr.getWithinStructIdentif();

        if (!(strB.toString().compareTo("") == 0)) {
            orderedKeys[0][k] = new StringBuffer("WithinStructIdentif");
            orderedValues[0][k] = new StringBuffer(strB.toString());
            
            /* free the string and increment the index*/
            strB.setLength(0);
            k++;
        }
 
        /* Read from field house number prefix */
        strB = standAddr.getPropDesPrefDirection();

        if (!(strB.toString().compareTo("") == 0)) {
            orderedKeys[0][k] = new StringBuffer("PropDesPrefDirection");
            orderedValues[0][k] = new StringBuffer(strB.toString());
            
            /* free the string and increment the index*/
            strB.setLength(0);
            k++;
        }
 
        /* Read from field house number prefix */
        strB = standAddr.getPropDesPrefType();

        if (!(strB.toString().compareTo("") == 0)) {
            orderedKeys[0][k] = new StringBuffer("PropDesPrefType");
            orderedValues[0][k] = new StringBuffer(strB.toString());
            
            /* free the string and increment the index*/
            strB.setLength(0);
            k++;
        }
 
        /* Read from field house number prefix */
        strB = standAddr.getOrigPropertyName();

        if (!(strB.toString().compareTo("") == 0)) {
            orderedKeys[0][k] = new StringBuffer("OrigPropertyName");
            orderedValues[0][k] = new StringBuffer(strB.toString());
            
            /* free the string and increment the index*/
            strB.setLength(0);
            k++;
        }

        /* Read from field house number prefix */
        strB = standAddr.getMatchPropertyName();

        if (!(strB.toString().compareTo("") == 0)) {
            orderedKeys[0][k] = new StringBuffer("MatchPropertyName");
            orderedValues[0][k] = new StringBuffer(strB.toString());
            
            /* free the string and increment the index*/
            strB.setLength(0);
            k++;
        }

        /* Read from field house number prefix */
        strB = standAddr.getPropertySufType();
        
        if (!(strB.toString().compareTo("") == 0)) {
            orderedKeys[0][k] = new StringBuffer("PropertySufType");
            orderedValues[0][k] = new StringBuffer(strB.toString());
            
            /* free the string and increment the index*/
            strB.setLength(0);
            k++;
        }

        /* Read from field house number prefix */
        strB = standAddr.getPropertySufDirection();
        
        if (!(strB.toString().compareTo("") == 0)) {
            orderedKeys[0][k] = new StringBuffer("PropertySufDirection");
            orderedValues[0][k] = new StringBuffer(strB.toString());
            
            /* free the string and increment the index*/
            strB.setLength(0);
            k++;
        }

        /* Read from field house number prefix */
        strB = standAddr.getStructureDescript();
        
        
        if (!(strB.toString().compareTo("") == 0)) {
            orderedKeys[0][k] = new StringBuffer("CenterDescript");
            orderedValues[0][k] = new StringBuffer(strB.toString());
            
            /* free the string and increment the index*/
            strB.setLength(0);
            k++;
        }

        /* Read from field house number prefix */
        strB = standAddr.getStructureIdentif();
        
        if (!(strB.toString().compareTo("") == 0)) {
            orderedKeys[0][k] = new StringBuffer("CenterIdentif");
            orderedValues[0][k] = new StringBuffer(strB.toString());
            
            /* free the string and increment the index*/
            strB.setLength(0);
            k++;
        }

        /* Read from field house number prefix */
        strB = standAddr.getRuralRouteDescript();
        
        if (!(strB.toString().compareTo("") == 0)) {
            orderedKeys[0][k] = new StringBuffer("RuralRouteDescript");
            orderedValues[0][k] = new StringBuffer(strB.toString());
            
            /* free the string and increment the index*/
            strB.setLength(0);
            k++;
        }

        /* Read from field house number prefix */
        strB = standAddr.getRuralRouteIdentif();
        
        if (!(strB.toString().compareTo("") == 0)) {
            orderedKeys[0][k] = new StringBuffer("RuralRouteIdentif");
            orderedValues[0][k] = new StringBuffer(strB.toString());
            
            /* free the string and increment the index*/
            strB.setLength(0);
            k++;
        }

        /* Read from field house number prefix */
        strB = standAddr.getBoxDescript();
        
        
        if (!(strB.toString().compareTo("") == 0)) {
            orderedKeys[0][k] = new StringBuffer("BoxDescript");
            orderedValues[0][k] = new StringBuffer(strB.toString());
            
            /* free the string and increment the index*/
            strB.setLength(0);
            k++;
        }

        /* Read from field house number prefix */
        strB = standAddr.getBoxIdentif();
        
        if (!(strB.toString().compareTo("") == 0)) {
            orderedKeys[0][k] = new StringBuffer("BoxIdentif");
            orderedValues[0][k] = new StringBuffer(strB.toString());
            
            /* free the string and increment the index*/
            strB.setLength(0);
            k++;
        }

        /* Read from field house number prefix */
        strB = standAddr.getExtraInfo();

        if (!(strB.toString().compareTo("") == 0)) {
            if (k == 0) {
                orderedKeys[0][k] = new StringBuffer("OrigPropertyName");
                orderedValues[0][k] = new StringBuffer(strB.toString());
                k++;
                orderedKeys[0][k] = new StringBuffer("MatchPropertyName");
                orderedValues[0][k] = new StringBuffer(strB.toString());                
            } else {
                orderedKeys[0][k] = new StringBuffer("ExtraInfo");
                orderedValues[0][k] = new StringBuffer(strB.toString());                
            }
            
            /* free the string and increment the index*/
            strB.setLength(0);
            k++;
        }

        return k;
    }
}
