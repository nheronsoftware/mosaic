/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StreamTokenizer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.StringTokenizer;

import com.stc.sbme.api.SbmeMatchEngineException;

/**
 * A bunch of Java methods that mimic the functionality of standard
 * C functions
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
public class EmeUtil {

/*
 * Java methods equivalent to "strcat" C functions
 */

    /**
     * Concatenates the string s to the string sb and returns the new string
     *
     * @param sb the initial string
     * @param s the string that we add to the original one
     * @return the association of the two strings
     */
    public static StringBuffer strcat(StringBuffer sb, String s) {
       return sb.append(s);
    }


    /**
     * 
     * 
     * @param al  the list where we concatenate the string
     * @param s the string
     * @return a StringBuffer
     * @throws Exception see above
     */
/*    public static StringBuffer strcat(ArrayList al, String s)
        throws Exception {

        // Get the substring from the ArrayList
        StringBuffer sb = EmeUtil.getSbPointer(al, 0);

        sb.append(s);

        // Update the associated ArrayList
        EmeUtil.upAList(sb, al);

        return sb;
    }
*/

    /**
     * Concatenates the string sb2 to the string sb and returns the new string
     * 
     * @param sb the original stringbuffer
     * @param sb2 the stringbuffer to append to the original one
     * @return the resulted string from the concatenation
     */
    public static StringBuffer strcat(StringBuffer sb, StringBuffer sb2) {
        return sb.append(sb2);
    }

    /**
     * Concatenates the substring of sb2, starting at index sb2Index, to the string sb 
     * and returns the new string
     * 
     * @param sb the original stringbuffer
     * @param sb2 the stringbuffer to add
     * @param sb2Index the starting index of the appended string
     * @return the total string
     */
    public static StringBuffer strcat(StringBuffer sb, StringBuffer sb2, int sb2Index) {

        int len2 = sb2.length();

        // First check if the index sb2Index is within the string range itself othewise
        // keep all the string
        if (sb2Index < len2) {
            return sb.append(sb2.substring(sb2Index));
        } else {
            return sb.append(sb2);

        }
    }

/*
 * Java methods equivalent to "strncat" C functions
 */

    /**
     * Concatenates the substring of finSb, starting at index sb2Index and over a length of
     * 'length', to the substring of intSb starting from 0 to the length sbIndex. 
     * It returns a new string that is the concatenation of the above substrings
     * 
     * @param intSb the initial string
     * @param sbIndex the index of the initial string at which we concatenate the othe string
     * @param finSb the string to which we extract the substring to append to the initial one.
     * @param sb2Index aaa
     * @param length aaa
     * @throws NullPointerException an exception
     * @return a StringBuffer
     */
    public static StringBuffer strncat(StringBuffer intSb, int sbIndex, StringBuffer finSb, 
                                       int sb2Index, int length) 
        throws NullPointerException {

        int len1 = intSb.length();
        int len2 = finSb.length();

        String s1;
        String s2;
        
        // First check if the index i1 is within the string range itself and
        // keep only the needed substring
        if (sbIndex < len1) {
            s1 = intSb.substring(sbIndex);
        } else {
            s1 = intSb.toString();
        }
      
        // Then, check if the index i2 is within the string range itself and
        // keep only the needed substring
        if (sb2Index < len2) {
            s2 = finSb.substring(sb2Index);
        } else {
            s2 = finSb.toString();
        }
        
        // Then compare the length of the strings with the length over which we compare them
        if (len2 > length) {
            s2 = s2.substring(0, length);
        }
        
        // Concatenate the two strings
        return (new StringBuffer(s1).append(s2));
    }


    /**
     * Version that update the ArrayList associated with the StringBuffer
     * 
     * @param al aaa
     * @param sbIndex aaa
     * @param finSb aaa
     * @param sb2Index aaaa
     * @param length aaaa
     * @return a StringBuffer
     * @throws SbmeMatchEngineException an exception
     */
    public static StringBuffer strncat(ArrayList al, int sbIndex, StringBuffer finSb,
                                       int sb2Index, int length)
        throws SbmeMatchEngineException {
        
        // Get the substring from the ArrayList
        StringBuffer intSb = EmeUtil.getSbPointer(al, sbIndex);


        StringBuffer sbf = new StringBuffer(intSb.substring(sbIndex));

        int finLength = finSb.length();
        
        
        if (finLength > (length - sb2Index)) {
            sbf.append(finSb.toString().toCharArray(), sb2Index, length);
            
        } else {
            sbf.append(finSb);
        }

        // Update the associated ArrayList
        EmeUtil.upAList(sbf, al);
        return sbf;
    }


    /**
     * 
     * 
     * @param intSb aaaa
     * @param sbIndex aaaa
     * @param finSb aaaa
     * @return a StringBuffer
     */
    public static StringBuffer strncat(StringBuffer intSb, int sbIndex, StringBuffer finSb) {

        return (new StringBuffer(intSb.substring(sbIndex)).append(finSb));
    }

    /**
     * 
     * 
     * @param intSb aaaa
     * @param sbIndex aaaa
     * @param finS aaaa
     * @return a StringBuffer
     */
    public static StringBuffer strncat(StringBuffer intSb, int sbIndex, String finS) {

        return (new StringBuffer(intSb.substring(sbIndex)).append(finS));
        
    }


    /**
     * 
     * 
     * @param intSb aaaa
     * @param sbIndex aaa
     * @param finSb aaa
     * @param length aaaa
     * @return a StringBuffer
     */
    public static StringBuffer strncat(StringBuffer intSb, int sbIndex, StringBuffer finSb,
                                       int length) {

        int finLength = finSb.length();
        
        
        if (finLength > length) {
            return (new StringBuffer(intSb.substring(sbIndex)).append(finSb.substring(0, length)));
            
        } else {
            return (new StringBuffer(intSb.substring(sbIndex)).append(finSb));
        }
    }


    /**
     * 
     * 
     * @param sb aaaa
     * @param sb2 aaaa
     * @param sb2Index aaaa
     * @param length aaaa
     * @return a StringBuffer
     */
    public static StringBuffer strncat(StringBuffer sb, StringBuffer sb2, int sb2Index, 
                                       int length) {

        int sb2Length = sb2.length();
        
        if (sb2Length > (length - sb2Index)) {
            return sb.append(sb2.substring(sb2Index, length));
            
        } else {
            return sb.append(sb2);
        }
    }


    /**
     * Version that update the ArrayList associated with the StringBuffer
     * 
     * @param al aaaa
     * @param sb2 aaaa
     * @param sb2Index aaaa
     * @param length aaaa
     * @return a StringBuffer
     * @throws SbmeMatchEngineException aaaa
     */
    public static StringBuffer strncat(ArrayList al, StringBuffer sb2, int sb2Index, int length)
        throws SbmeMatchEngineException {

        // Get the substring from the ArrayList
        StringBuffer sb = EmeUtil.getSbPointer(al, 0);

        char[] cr = sb2.toString().toCharArray();

        int charLength = cr.length;
        
        if (charLength > (length - sb2Index)) {
            sb.append(cr, sb2Index, length);
            
        } else { 
            sb.append(cr);
        }

        // Update the associated ArrayList
        EmeUtil.upAList(sb, al);

        return sb;
    }


    /**
     * 
     * 
     * @param sb aaaa
     * @param sbIndex aaaa
     * @param cb aaaa
     * @param cbIndex aaaa
     * @param length aaaa
     * @return a StringBuffer
     */
    public static StringBuffer strncat(StringBuffer sb, int sbIndex, char[] cb, int cbIndex, 
                                       int length) {

        String s = sb.substring(sbIndex); 

        StringBuffer sbf = new StringBuffer(s);
        
        int charLength = cb.length;
        
        
        if (charLength > (length - cbIndex)) {
            sbf.append(cb, cbIndex, length);
            
        } else { 
            sbf.append(cb);
        }
        return sbf;
    }

    /**
     * 
     * 
     * @param al aaaa
     * @param sbIndex aaaa
     * @param cb aaaa
     * @param cbIndex aaaa
     * @param length aaaa
     * @return a StringBuffer
     * @throws SbmeMatchEngineException aaaa
     */
    public static StringBuffer strncat(ArrayList al, int sbIndex, char[] cb, int cbIndex, int length)
        throws SbmeMatchEngineException {

        // Get the substring from the ArrayList
        StringBuffer sbf = EmeUtil.getSbPointer(al, sbIndex);

        int charLength = cb.length;
        
        if (charLength > (length - cbIndex)) {
            sbf.append(cb, cbIndex, length);
            
        } else {  
            sbf.append(cb);
        }

        EmeUtil.upAList(sbf, al);

        return sbf;
    }

    /**
     * 
     * 
     * @param sb aaaa
     * @param cb aaaa
     * @param cbIndex  aaaa
     * @param length aaaa
     * @return a StringBuffer
     */
    public static StringBuffer strncat(StringBuffer sb, char[] cb, int cbIndex, int length) {

        int charLength = cb.length;
        
        if (charLength > (length - cbIndex)) {
            sb.append(cb, cbIndex, length);
            
        } else { 
            sb.append(cb);
        }
        return sb;
    }


    /**
     * 
     * 
     * @param al aaaa
     * @param cb aaaa
     * @param cbIndex aaaa
     * @param length aaaa
     * @return a StringBuffer
     * @throws SbmeMatchEngineException aaaa
     */
    public static StringBuffer strncat(ArrayList al, char[] cb, int cbIndex, int length)
        throws SbmeMatchEngineException {

        // Get the substring from the ArrayList
        StringBuffer sbf = EmeUtil.getSbPointer(al, 0);

        int cbLength = cb.length;
        
        if (cbLength > (length - cbIndex)) {
            sbf.append(cb, cbIndex, length);
            
        } else {
            sbf.append(cb);
        }
        
        // Update the associated ArrayList
        EmeUtil.upAList(sbf, al);

        return sbf;
    }

    /**
     * 
     * 
     * @param sb aaaa
     * @param sChar aaaa
     * @param length aaaa
     * @return a StringBuffer
     */
    public static StringBuffer strncat(StringBuffer sb, char[] sChar, int length) {

        int charLength = sChar.length;
        
        if (charLength > length) {
            sb.append(sChar, 0, length);
            
        } else {
            sb.append(sChar);
        }
        return sb;
    }


    /**
     * 
     * 
     * @param sb aaaa
     * @param fb aaaa
     * @param length aaaa
     * @return a StringBuffer
     */
    public static StringBuffer strncat(StringBuffer sb, StringBuffer fb, int length) {

        int fbLength = fb.length();
        
        if (fbLength > length) {
            sb.append(fb.toString().toCharArray(), 0, length);
            
        } else {
            sb.append(fb);
        }
        return sb;
    }


    /**
     * 
     * 
     * @param al aaaa
     * @param fb aaaa
     * @param length aaaa
     * @return a StringBuffer
     * @throws SbmeMatchEngineException aaaa
     */
    public static StringBuffer strncat(ArrayList al, StringBuffer fb, int length)
        throws SbmeMatchEngineException {

        StringBuffer sbf = EmeUtil.getSbPointer(al, 0);
        
        int fsLength = fb.length();
        
        if (fsLength > length) {
            sbf.append(fb.toString().toCharArray(), 0, length);
            
        } else {
            sbf.append(fb);
        }
        
        EmeUtil.upAList(sbf, al);

        return sbf;
    }


    /**
     * 
     * 
     * @param sb aaaa
     * @param fs aaaa
     * @param length aaaa
     * @return a StringBuffer
     */
    public static StringBuffer strncat(StringBuffer sb, String fs, int length) {

        int fsLength = fs.length();
        
        if (fsLength > length) {
            sb.append(fs.toCharArray(), 0, length);
            
        } else {
            sb.append(fs);
        }
        return sb;
    }


    /**
     * 
     * 
     * @param al aaaa
     * @param fs aaaa
     * @param length aaaa
     * @return a StringBuffer
     * @throws SbmeMatchEngineException aaaa
     */
    public static StringBuffer strncat(ArrayList al, String fs, int length)
        throws SbmeMatchEngineException {

        // Get the substring from the ArrayList
        StringBuffer sb = EmeUtil.getSbPointer(al, 0);
        
        int sbLength = sb.length();
        int fsLength = fs.length();
        
        if (fsLength > length) {
            sb.append(fs.toCharArray(), 0, length);
            
        } else {
            sb.append(fs);
        }

        // Update the associated ArrayList
        EmeUtil.upAList(sb, al);

        return sb;
    }

/*
 * Java methods equivalent to "strncmp" C functions
 */
 
    /**
     * 
     * 
     * @param intSb aaaa
     * @param finSb aaaa
     * @return an integer value from the comparison
     */
    public static int strcmp1(StringBuffer intSb, StringBuffer finSb) {

        if ((intSb == null) || (finSb == null)) {
            return 2;
        }
        
        if ((intSb.toString()).compareTo(finSb.toString()) > 0) {
//        if ((intSb.toString()).compareTo(String.valueOf(finSb)) > 0) {
            return 1;

        } else if ((intSb.toString()).compareTo(finSb.toString()) < 0) {
//        } else if ((intSb.toString()).compareTo(String.valueOf(finSb)) < 0) {
            return -1;

        } else {
            return 0;
        }
    }

    /**
     * 
     * 
     * @param intSb aaaa
     * @param finSb aaaa
     * @return an integer value from the comparison
     */
    public static final int strcmp(StringBuffer intSb, StringBuffer finSb) {

        if ((intSb == null) || (finSb == null)) {
            return 2;
        }
        
        int j;
        int len1 = intSb.length();
        int len2 = finSb.length();
        
        if (len1 != len2) {
            return 1;
        }

        for (j = 0; j < len1; j++) {
        
            if (intSb.charAt(j) == finSb.charAt(j)) {
                continue;
            } else {
                return 1;
            }
        }
        
        return 0;
    }
    

    /**
     * 
     * 
     * @param intSb aaaa
     * @param finS aaaa
     * @return an integer value from the comparison
     */
    public static int strcmp1(StringBuffer intSb, String finS) {

        if ((intSb == null) || (finS == null)) {
            return 2;
        }
        if ((intSb.toString()).compareTo(finS) > 0) {
            return 1;

        } else if ((intSb.toString()).compareTo(finS) < 0) {
            return -1;

        } else {
            return 0;
        }
    }

        
    /**
     * 
     * 
     * @param intSb aaaa
     * @param finS aaaa
     * @return an integer value from the comparison
     */
    public static int strcmp(StringBuffer intSb, String finS) {

        if ((intSb == null) || (finS == null)) {
            return 2;
        }

        int j;
        int len1 = intSb.length();
        int len2 = finS.length();
        
        if (len1 != len2) {
            return 1;
        }

        for (j = 0; j < len1; j++) {
        
            if (intSb.charAt(j) == finS.charAt(j)) {
                continue;
            } else {
                return 1;
            }
        }
        
        return 0;
    }


    /**
     * 
     * 
     * @param intS aaaa
     * @param finS aaaa
     * @return an integer value from the comparison
     */
    public static int strcmp(String intS, String finS) {

        if ((intS == null) || (finS == null)) {
            return 2;
        }

        if (intS.compareTo(finS) > 0) {
            return 1;

        } else if (intS.compareTo(finS) < 0) {
            return -1;

        } else {
            return 0;
        }
    }


    /**
     * 
     * 
     * @param intS aaaa
     * @param finSb aaaa
     * @return an integer value from the comparison
     */
    public static int strcmp1(String intS, StringBuffer finSb) {

        if ((intS == null) || (finSb == null)) {
            return 2;
        }

        if (intS.compareTo(finSb.toString()) > 0) {
//        if (intS.compareTo(String.valueOf(finSb)) > 0) {
            return 1;

        } else if (intS.compareTo(finSb.toString()) < 0) {
//        } else if (intS.compareTo(String.valueOf(finSb)) < 0) {
            return -1;

        } else {
            return 0;
        }
    }


    /**
     * 
     * 
     * @param intS aaaa
     * @param finSb aaaa
     * @return an integer value from the comparison
     */
    public static int strcmp(String intS, StringBuffer finSb) {

        if ((intS == null) || (finSb == null)) {
            return 2;
        }
        
        int j;
        int len1 = intS.length();
        int len2 = finSb.length();
        
        if (len1 != len2) {
            return 1;
        }

        for (j = 0; j < len1; j++) {
        
            if (intS.charAt(j) == finSb.charAt(j)) {
                continue;
            } else {
                return 1;
            }
        }
        
        return 0;

    }
    


    /**
     * 
     * 
     * @param intSb aaaa
     * @param finSb aaaa
     * @param len aaaa
     * @return an integer value from the string comparison
     */
    public static int strncmp(StringBuffer intSb, StringBuffer finSb, int len) {

        if ((intSb == null) || (finSb == null)) {
            return 2;
        }

        int len1 = intSb.length();
        int len2 = finSb.length();
        
        String s1;
        String s2;
        
        if (len1 < len) { 
            s1 = intSb.toString();
        } else {
            s1 = intSb.substring(0, len);
        }

        if (len2 < len) {
            s2 = finSb.toString();
        } else {
            s2 = finSb.substring(0, len);
        }
        

        if (s1.compareTo(s2) > 0) {
            return 1;

        } else if (s1.compareTo(s2) < 0) {
            return -1;

        } else {
            return 0;
        }
    }


    /**
     * 
     * 
     * @param intSb aaaa
     * @param finS aaaa
     * @param len  aaaa
     * @return an integer value from the comparison
     */
    public static int strncmp(StringBuffer intSb, String finS, int len) {

        if ((intSb == null) || (finS == null)) {
            return 2;
        }

        int len1 = intSb.length();
        int len2 = finS.length();
        
        String s1;
        String s2;
        
        
        if (len1 < len) { 
            s1 = intSb.toString();
        } else {
            s1 = intSb.substring(0, len);
        }

        if (len2 < len) {
            s2 = finS;
        } else {
            s2 = finS.substring(0, len);
        }
            
        if (s1.compareTo(s2) > 0) {
            return 1;

        } else if (s1.compareTo(s2) < 0) {
            return -1;

        } else {
            return 0;
        }
    }


    /**
     * 
     * 
     * @param intS aaaa
     * @param finSb aaaa
     * @param len aaaa
     * @return an integer value from the comparison
     */
    public static int strncmp(String intS, StringBuffer finSb, int len) {

        if ((intS == null) || (finSb == null)) {
            return 2;
        }

        int len1 = intS.length();
        int len2 = finSb.length();

        String s1;
        String s2;

        if (len1 < len) { 
            s1 = intS;
        } else {
            s1 = intS.substring(0, len);
        }

        if (len2 < len) {
            s2 = finSb.toString();
            
        } else {
            s2 = finSb.substring(0, len);
        }


        if (s1.compareTo(s2) > 0) {
            return 1;

        } else if (s1.compareTo(s2) < 0) {
            return -1;

        } else {
            return 0;
        }
    }


    /**
     * 
     * 
     * @param intS aaaa
     * @param finS aaaa
     * @param len aaaa
     * @return an integer value from the comparison
     */
    public static int strncmp(String intS, String finS, int len) {

        if ((intS == null) || (finS == null)) {
            return 2;
        }

        int len1 = intS.length();
        int len2 = finS.length();

        String s1;
        String s2;

        if (len1 < len) { 
            s1 = intS;
        } else {
            s1 = intS.substring(0, len);
        }

        if (len2 < len) {
            s2 = finS;
            
        } else { 
            s2 = finS.substring(0, len);
        }

        if (s1.compareTo(s2) > 0) {
            return 1;

        } else if (s1.compareTo(s2) < 0) {
            return -1;

        } else {
            return 0;
        }
    }


    /**
     * 
     * 
     * @param intSb aaaa
     * @param finSb aaaa
     * @param index aaaa
     * @param len aaaa
     * @return an integer value from the comparison
     */
    public static int strncmp(StringBuffer intSb, StringBuffer finSb, int index, int len) {
        
        if ((intSb == null) || (finSb == null)) {
            return 2;
        }

        int len1 = intSb.length(); 
        int len2 = finSb.length();

        String s1;
        String s2;

        // First check if the index is within the string itself and 
        // keep only the jneeded substring
        if (index < len2) { 
            s2 = finSb.substring(index);
        } else {
            s2 = finSb.toString();
        }
        
        // Then compare the length of the strings with the length over which we compare them
        if (len1 < len) { 
            s1 = intSb.toString();
        } else {
            s1 = intSb.substring(0, len);
        }

        if (s2.length() > len) {
            s2 = s2.substring(0, len);
        }


        if (s1.compareTo(s2) > 0) {
            return 1;

        } else if (s1.compareTo(s2) < 0) {
            return -1;

        } else {
            return 0;
        }
    }


    /**
     * 
     * 
     * @param intS aaaa
     * @param finSb aaaa
     * @param index aaaa
     * @param len aaaa
     * @return an integer value from the comparison
     */
    public static int strncmp(String intS, StringBuffer finSb, int index, int len) {

        if ((intS == null) || (finSb == null)) {
            return 2;
        }

        int len1 = intS.length();
        int len2 = finSb.length();

        String s1;
        String s2;
        
        // First check if the index is within the string itself and 
        // keep only the jneeded substring
        if (index < len2) { 
            s2 = finSb.substring(index);
        } else {
            s2 = finSb.toString();
        }
        
        // Then compare the length of the strings with the length over which we compare them
        if (len1 < len) { 
            s1 = intS;
        } else {
            s1 = intS.substring(0, len);
        }

        if (s2.length() > len) {
            s2 = s2.substring(0, len);
        }
        
        
        if (s1.compareTo(s2) > 0) {
            return 1;

        } else if (s1.compareTo(s2) < 0) {
            return -1;

        } else {
            return 0;
        }
    }


    /**
     * 
     * 
     * @param intSb aaaa
     * @param i1 aaaa
     * @param finSb aaaa
     * @param i2 aaaa
     * @param len aaaa
     * @return an integer value from the comparison
     */
    public static int strncmp(StringBuffer intSb, int i1, StringBuffer finSb, int i2,
                              int len) {

        if ((intSb == null) || (finSb == null)) {
            return 2;
        }

        int len1 = intSb.length();
        int len2 = finSb.length();

        String s1;
        String s2;
        
        // First check if the index i1 is within the string range itself and
        // keep only the needed substring
        if (i1 < len1) {
            s1 = intSb.substring(i1);
        } else {
            s1 = intSb.toString();
        }

        // First check if the index i2 is within the string range itself and
        // keep only the needed substring
        if (i2 < len2) {
            s2 = finSb.substring(i2);
        } else {
            s2 = finSb.toString();
        }
        

        // Then compare the length of the strings with the length over which we compare them
        if (s1.length() > len) {
            s1 = s1.substring(0, len);
        }

        if (s2.length() > len) {
            s2 = s2.substring(0, len);
        }


        if (s1.compareTo(s2) > 0) {
            return 1;

        } else if (s1.compareTo(s2) < 0) {
            return -1;

        } else {
            return 0;
        }
    }


    /**
     * 
     * 
     * @param intS aaaa
     * @param i1 aaaa
     * @param finSb aaaa
     * @param len aaaa
     * @return an integer value from the comparison
     */
    public static int strncmp(String intS, int i1, StringBuffer finSb, int len) {

        if ((intS == null) || (finSb == null)) {
            return 2;
        }

        int len1 = intS.length();
        int len2 = finSb.length();

        String s1;
        String s2;
        
        // First check if the index i1 is within the string range itself and
        // keep only the needed substring
        if (i1 < len1) {
            s1 = intS.substring(i1);
        } else {
            s1 = intS;
        }
        
        // Then compare the length of the strings with the length over which we compare them
        if (s1.length() > len) {
            s1 = s1.substring(0, len);
        }
        
        // Then compare the length of the strings with the length over which we compare them
        if (len2 < len) {
            s2 = finSb.toString();
        } else {
            s2 = finSb.substring(0, len);
        }
        
        
        if (s1.compareTo(s2) > 0) {
            return 1;

        } else if (s1.compareTo(s2) < 0) {
            return -1;

        } else {
            return 0;
        }
    }


    /**
     * 
     * 
     * @param intS aaaa
     * @param i1 aaaa
     * @param finSb aaaa
     * @param i2 aaaa aaaa
     * @param len aaaa
     * @return an integer value from the comparison
     */
    public static int strncmp(String intS, int i1, StringBuffer finSb, int i2, int len) {

        if ((intS == null) || (finSb == null)) {
            return 2;
        }

        int len1 = intS.length();
        int len2 = finSb.length();

        String s1;
        String s2;
        
        // First check if the index i1 is within the string range itself and
        // keep only the needed substring
        if (i1 < len1) {
            s1 = intS.substring(i1);
        } else {
            s1 = intS;
        }

        // First check if the index i2 is within the string range itself and
        // keep only the needed substring
        if (i2 < len2) {
            s2 = finSb.substring(i2);
        } else {
            s2 = finSb.toString();
        }
        

        // Then compare the length of the strings with the length over which we compare them
        if (s1.length() > len) {
            s1 = s1.substring(0, len);
        }

        if (s2.length() > len) {
            s2 = s2.substring(0, len);
        }
        

        if (s1.compareTo(s2) > 0) {
            return 1;

        } else if (s1.compareTo(s2) < 0) {
            return -1;

        } else {
            return 0;
        }
    }


    /**
     * 
     * 
     * @param intSb aaaa
     * @param i1 aaaa
     * @param finS  aaaa
     * @param len aaaa
     * @return an integer value from the comparison
     */
    public static int strncmp(StringBuffer intSb, int i1, String finS, int len) {

        if ((intSb == null) || (finS == null)) {
            return 2;
        }

        int len1 = intSb.length();
        int len2 = finS.length();

        String s1;
        String s2;
        
        // First check if the index i1 is within the string range itself and
        // keep only the needed substring
        if (i1 < len1) {
            s1 = intSb.substring(i1);
        } else {
            s1 = intSb.toString();
        }
        
        // Then compare the length of the strings with the length over which we compare them
        if (s1.length() > len) {
            s1 = s1.substring(0, len);
        }
        
        // Then compare the length of the strings with the length over which we compare them
        if (len2 < len) {
            s2 = finS;
        } else {
            s2 = finS.substring(0, len);
        }
        

        if (s1.compareTo(s2) > 0) {
            return 1;

        } else if (s1.compareTo(s2) < 0) {
            return -1;

        } else {
            return 0;
        }
    }


    /**
     * 
     * 
     * @param intSb aaaa
     * @param i1 aaaa
     * @param finC aaaa
     * @param i2 aaaa
     * @param len aaaa
     * @return an integer value from the comparison
     */
    public static int strncmp(StringBuffer intSb, int i1, char[] finC, int i2, int len) {

        if ((intSb == null) || (finC.length == 0)) {
            return 2;
        }

        int len1 = intSb.length();
        int len2 = finC.length;

        String s1;
        String s2;
        
        // First check if the index i1 is within the string range itself and
        // keep only the needed substring
        if (i1 < len1) {
            s1 = intSb.substring(i1);
        } else {
            s1 = intSb.toString();
        }

        // First check if the index i2 is within the string range itself and
        // keep only the needed substring
        if (i2 < len2) {
            s2 = (String.copyValueOf(finC)).substring(i2);
        } else {
            s2 = String.copyValueOf(finC);
        }
        

        // Then compare the length of the strings with the length over which we compare them
        if (s1.length() > len) {
            s1 = s1.substring(0, len);
        }

        if (s2.length() > len) {
            s2 = s2.substring(0, len);
        }
        

        if (s1.compareTo(s2) > 0) {
            return 1;

        } else if (s1.compareTo(s2) < 0) {
            return -1;

        } else {
            return 0;
        }
    }


/*
 * Java methods that output a sub-buffer
 */
 
    /**
     * 
     * 
     * @param intChar aaaa
     * @param start aaaa
     * @return a StringBuffer
     */
    public static StringBuffer subBuffer(char[] intChar, int start) {

        String s1;
        
        // First check if the index 'start' is within the array range.
        if (start < intChar.length) {
            s1 = (new String(intChar)).substring(start);
        } else {
            s1 = new String(intChar);
        }
        
        return new StringBuffer(s1);
    }


    /**
     * 
     * 
     * @param sBuff aaaa
     * @param start aaaa
     * @return a StringBuffer
     */
    public static StringBuffer subBuffer(StringBuffer sBuff, int start) {

        String s1;
        
        // First check if the index start is within the string range
        if (start < sBuff.length()) {
            s1 = sBuff.substring(start);
        } else {
            s1 = sBuff.toString();
        }
        return new StringBuffer(s1);
    }

/*
 * Java methods for strcpy c-function
 */

    /**
     * Copy the string finSb into the original string intSb
     * 
     * @param intSb the original string
     * @param finSb the replacement string
     * @throws NullPointerException an exception
     * @return the new string 
     */
    public static StringBuffer strcpy(StringBuffer intSb, StringBuffer finSb)
        throws NullPointerException {

        intSb.setLength(0);
        return intSb.append(finSb);
        
    }


    /**
     * Copy the string finSb into the original string intSb starting at index ind
     * 
     * @param intSb the original string
     * @param ind the replacement string
     * @param finSb aaaa
     * @throws NullPointerException an exception
     * @return a StringBuffer
     */
    public static StringBuffer strcpy(StringBuffer intSb, int ind, StringBuffer finSb) 
        throws NullPointerException {

        int len1 = intSb.length();

        // First, check if the index ind is within the string range itself and
        // keep only the needed substring
        if (ind < len1) {

            intSb.setLength(ind);
            return intSb.append(finSb);
            
        } else {
            return intSb.append(finSb);
        }
    }


    /**
     * Copy the substring of finSb, starting at index ind, into the original string intSa
     *
     * @param intSb the original string
     * @param finSb the replacement string
     * @param ind aaaa
     * @throws NullPointerException an exception
     * @return a StringBuffer
     */
    public static StringBuffer strcpy(StringBuffer intSb, StringBuffer finSb, int ind)
        throws NullPointerException {
        
        int len = finSb.length();
        intSb.setLength(0);
        
        if (ind < len) {
            return intSb.append(finSb.substring(ind, len));
     
        } else {
            return intSb.append(finSb);
        }
    }


    /**
     * Copy a substring of finSb, starting at index ind2 to the end of the string, 
     * into the original string intSa, starting at index ind1. The returned string is the
     * concatenation of a substring of the original string, from index inde1, and a substring
     * of the second string, starting at index ind2.
     * 
     * @param intSb the original string
     * @param ind1 the start index of the original string
     * @param finSb the replacement string
     * @param ind2 the start index of the second string
     * @return the final string
     */
    public static StringBuffer strcpy(StringBuffer intSb, int ind1, StringBuffer finSb, int ind2) {

//        String s;
        
        int len1 = intSb.length();
        int len2 = finSb.length();
        
        if (ind1 < len1) {
            intSb.setLength(ind1);
        }
        
        if (ind2 < len2) {
//            s = finSb.substring(ind2, len2);
            return intSb.append(finSb.substring(ind2, len2));
        } else {
            return intSb.append(finSb);
        }
    }


    /**
     * Copy the string finSb into the original string intS starting at index ind
     * 
     * @param intSb the original string
     * @param ind the start index of the original string
     * @param finS the replacement string
     * @return the resulted string
     */
    public static StringBuffer strcpy(StringBuffer intSb, int ind, String finS) {

        int len1 = intSb.length();
        int len2 = finS.length();

        // First check if the index ind is within the string range itself and
        // keep only the needed substring
        if (ind < len1) {
            intSb.setLength(ind);
            return intSb.append(finS);
            
        } else {
            return intSb.append(finS);
        }
    }


    /**
     * Copy the string finS into the original string intSb
     * 
     * @param intSb the original string
     * @param finS the replacement string
     * @return the resulted string
     */
    public static StringBuffer strcpy(StringBuffer intSb, String finS) {

        intSb.setLength(0);
        return intSb.append(finS);
    }

/*
 * Java methods for strncpy c-function
 */

    /**
     * Copy the substring of finSb over a length 'length' into the original string intSb
     * 
     * @param intSb the original string
     * @param finSb the replacement string
     * @param length the length of the substring of the replacement string
     * @return the resulted string
     */
    public static StringBuffer strncpy(StringBuffer intSb, StringBuffer finSb, int length) {
         
        int len = finSb.length();
        int i;
        
        // Free the initial string
        intSb.setLength(0);
        
        /* Handle case when finSb length is less than the defined length */
        if (len < length) {
            
            // Append the finSb string 
            intSb.append(finSb);
            
            // Fill the string with a  length-long whitespace characters
            for (i = 0; i < length - len; i++) {
                intSb.append(" ");
            }
            return intSb;
        }

        return intSb.append(finSb.substring(0, length));
    }


    /**
     * Copy the string finS over a length 'length' into the original string intSb
     * 
     * @param intSb the original string
     * @param finS the replacement string
     * @param length the length of the substring of the replacement string
     * @return the resulted string
     */
    public static StringBuffer strncpy(StringBuffer intSb, String finS, int length) {

        int len = finS.length();
        int i;
        
        // Free the initial string
        intSb.setLength(0);
        
        /* Handle case when finSb length is less than the defined length */
        if (len < length) {
            
            // Append the finSb string
            intSb.append(finS);
            
            // Fill the string with a  length-long whitespace characters
            for (i = 0; i < length - len; i++) {
                intSb.append(" ");
            }
            return intSb;
        }
        
//        intSb.replace(0, length, finS);
//        return intSb;
        return intSb.append(finS.substring(0, length));
        
    }


    /**
     * Copy the string finS into the original string intSb starting at index ind and
     * over length 'length'.
     * 
     * @param intSb the original string
     * @param ind the start index of the original string
     * @param finS the replacement string
     * @param length the length of the substring of the replacement string
     * @throws IndexOutOfBoundsException an exception
     * @return the resulted string
     */
    public static StringBuffer strncpy(StringBuffer intSb, int ind, String finS, int length)
        throws IndexOutOfBoundsException {


        int len1 = intSb.length();
        int len2 = finS.length();
        int i;

        // Handle case when finSb length is less than the defined length 
        if (len2 < length) {
            
            // Append the finSb string  
            intSb.append(finS);
            
            // Fill the string with a  length-long whitespace characters 
            for (i = 0; i < length - len2; i++) {
                intSb.append(" ");
            }
            return intSb;
        }

        intSb.replace(ind, ind + length, finS.substring(0, length));

        return intSb;
        

    }


    /**
     * Copy the string finSb into the original string intSb starting at index ind and
     * over length 'length'.
     * 
     * @param intSb the original string
     * @param ind the start index of the original string
     * @param finSb the replacement string
     * @param length the length of the substring of the replacement string
     * @return the resulted string
     */
    public static StringBuffer strncpy(StringBuffer intSb, int ind, StringBuffer finSb, int length) {

        int len2 = finSb.length();
        int i;
        
        intSb.setLength(ind);
        
        // Handle case when finSb length is less than the defined length 
        if (len2 < length) {
            
            // Append the finSb string  
            intSb.append(finSb);
            
            // Fill the string with a  length-long whitespace characters 
            for (i = 0; i < length - len2; i++) {
                intSb.append(" ");
            }
            return intSb;
        }
        
        return intSb.replace(ind, ind + length, finSb.substring(0, length));
    }


    /**
     * Copy the substring finSb, starting at index index, into the original string intSb
     * over length 'length'.
     * 
     * @param intSb the original string
     * @param finSb the replacement string
     * @param index the start index of the substring of the replacement string
     * @param length the length of the substring of the replacement string
     * @throws NullPointerException an exception
     */
    public static void strncpy(StringBuffer intSb, StringBuffer finSb, int index, int length)
        throws NullPointerException {

        int len2 = finSb.length();
        int len1;
        int i;
        
        intSb.setLength(0);

        // First, extract the substring from intSb
        if (index < len2) {
            intSb.append(finSb.substring(index));
        }
        
        len1 = intSb.length();
        
        /* Handle case when finSb is null or smaller than the expected length */
        if (len1 < length) {
        
            // Append the finSb string 
            if (index >= len2) {
                intSb.append(finSb);
            }
            
            // Fill the string with a  length-long whitespace characters
            for (i = 0; i < length - len1; i++) {
                intSb.append(" ");
            }
            return;
        }
        
        intSb.setLength(length);
    }


    /**
     * Copy the substring finSb, starting at index index, into the original string intSb,
     * starting at index ind2, over length 'length'.
     * 
     * @param intSb the original string
     * @param ind1 the start index of the substring of the original string
     * @param finSb the replacement string
     * @param ind2 the start index of the substring of the replacement string
     * @param length the length of the substring of the replacement string
     * @return the resulted string
     */
    public static StringBuffer strncpy(StringBuffer intSb, int ind1, StringBuffer finSb,
                                       int ind2, int length) {

        int len1;
        int len2 = finSb.length();
        int i;
        
        intSb.setLength(ind1);

        // And, extract the substring from finSb
        if (ind2 < len2) {
            intSb.append(finSb.substring(ind2));
        }
        
        len1 = intSb.length();
        
        // Handle case when finSb is null or smaller than the expected length 
        if (len1 < length) {
        
            // Fill the string with a  length-long whitespace characters
            for (i = 0; i < length - len1; i++) {
                intSb.append(" ");
            }
            return intSb;
        }

        return intSb.delete(length, len1);
    }


    /**
     * Copy the sub-Array of characters chA, starting at index ind, into the original string intSb,
     * over length 'length'.
     * 
     * @param intSb the original string
     * @param chA the replacement string
     * @param ind the start index of the substring of the replacement string
     * @param length the length of the substring of the replacement string
     * @throws NullPointerException an exception
     * @return the resulted string
     */
    public static StringBuffer strncpy(StringBuffer intSb, char[] chA, int ind, int length)
        throws NullPointerException {

        int len2 = chA.length;
        int len1;
        int i;
        
        intSb.setLength(0);
 
        // And, extract the substring from finSb
        if (ind < len2) {
            intSb.append(String.valueOf(chA).substring(ind));
        }
        
        len1 = intSb.length();
        
        // Handle case when finSb is smaller than the expected length 
        if (len1 < length) {
        
            // Fill the string with a  length-long whitespace characters
            for (i = 0; i < length - len1; i++) {
                intSb.append(" ");
            }
            return intSb;
        }
        
        return intSb.delete(length, len1);
    }


    /**
     * Copy the sub-Array of characters chA, starting at index ind, into the original 
     * string intSb, over length 'length'.
     * 
     * @param intSb  the original string
     * @param ind1 the start index of the substring of the original string
     * @param chA the replacement string
     * @param ind2 the start index of the substring of the replacement string
     * @param length aaaa
     * @return the resulted string
     */
    public static StringBuffer strncpy(StringBuffer intSb, int ind1, char[] chA, int ind2,
                                       int length) {

        int len1;
        int len2 = chA.length;
        int i;
        
        intSb.setLength(ind1);

        // And, extract the substring from finSb
        if (ind2 < len2) {
            intSb.append(String.valueOf(chA).substring(ind1));
        }
        
        len1 = intSb.length();
        
        // Handle case when finSb is null or smaller than the expected length 
        if (len1 < length) {
        
            // Fill the string with a  length-long whitespace characters
            for (i = 0; i < length - len1; i++) {
                intSb.append(" ");
            }
            return intSb;
        }
        
        return intSb.delete(length, len1);
    }


/*
 * Java methods for geting a string equivalent to char pointer
 */

    /**
     * Initializes the ArrayList elements linked to a given StringBuffer sb.
     * Fills elements with sb at index 0, then the indexes of any null characters
     inside the string
     * 
     * @param sb aaaa
     * @param size aaaa
     * @return an ArrayList
     * @throws SbmeMatchEngineException aaaa
     */
    public static ArrayList initAList(StringBuffer sb, int size)
        throws SbmeMatchEngineException {

        // The first element of the Object[] is the string itself.
        ArrayList al = new ArrayList(size + 1);
        al.add(0, sb);

        // The following elements hold indexes corresponding to the string elements
        for (int i = 0; i < size; i++) {

            al.add(i + 1, new Integer(-1));
        }
        return al;
    }


    /**
     * 
     * 
     * @param sb aaaa
     * @param al aaaa
     * @throws SbmeMatchEngineException aaaa
     */
    public static void upAList(StringBuffer sb, ArrayList al)
        throws SbmeMatchEngineException {

        int i;
        int aLen;
        int sbLen;

        aLen = al.size();
        sbLen = sb.length();

        al.clear();

        al.add(0, sb);

        for (i = 0; i < sbLen; i++) {
            al.add(i + 1, new Integer(-1));
        }
    }
 
 
    /**
     * Sets a StringBuffer's character at index ind to null, to mimic the equivalent 
     * functionality of a C-code pointer to char.
     * For example, if ind=0, then the stringbuffer should be empty
     * 
     * @param sbStat aaaa
     * @param ind aaaa
     * @return anArrayList
     * @throws SbmeMatchEngineException aaaa
     */
    public static ArrayList setNullSbPointer(ArrayList sbStat, int ind) 
        throws SbmeMatchEngineException {

        // The first element of the Object[] is the string itself.
        int oLen = sbStat.size();
        int ix;

        if (oLen == 0) {
            return sbStat;
        }

        // Check if a null character at index ind already exists
        for (int j = 1; j < oLen; j++) {

            ix = ((Integer) sbStat.get(j)).intValue();

            if (ix == ind) {
                return sbStat;
            }
        }

        sbStat.add(oLen, new Integer(ind));
            // The elements of sbStat of index >=1 are integers that
            // indicate the position of the null character in the string
        return sbStat;
    }


    /**
     * Reduce the StringBuffer to a length ind
     * 
     * @param sa aaaa
     * @param ind aaaa
     * @return a StringBuffer
     */
    public static StringBuffer setEndSb(StringBuffer sa, int ind) {

        sa.setLength(ind);
        return sa;
    }


    /**
     * Returns a sub-StringBuffer of sa that starts at index ind and finish
     * at the first encountered null character. Note that the integer values
     * in Object[] must first be initialized to zero.
     * 
     * @param obj aaaa
     * @param ind aaaa
     * @return a StringBuffer
     * @throws SbmeMatchEngineException an exception
     */
    public static StringBuffer getSbPointer(ArrayList obj, int ind) 
        throws SbmeMatchEngineException {

        StringBuffer sb = (StringBuffer) obj.get(0);
        int sbLen = sb.length();
        int i;
        int j;
        int iLen = sbLen;
        int ix = -1;

        // If the string is empty, there is nothing to add!
        if (sbLen == 0) {
            return sb;
        }

        // Do not process negative indexes. Should we?
        if (ind < 0) {
 
            throw new IndexOutOfBoundsException("The index must be larger or equal to zero "
            + "and less than " + sbLen);
        }

        // Search for null characters in sb from index ind up to sbLen.
        if (ind < sbLen) {

test:
            for (i = ind; i < sbLen; i++) {

                // for every character in sb, check if it is null
                for (j = 1; j < obj.size(); j++) {

                    ix = ((Integer) obj.get(j)).intValue();

                    if (ix ==  i) {
                        iLen = i - ind;
                        break test;
                    }
                }
            }

        } else {

            for (j = 1; j < obj.size(); j++) {

                ix = ((Integer) obj.get(j)).intValue();

                if (ix ==  ind) {
                    return new StringBuffer("");
                }
            }
        }

        if (iLen > 0) {

            for (int k = 0; k < iLen; k++) {
                sb.setCharAt(k, sb.charAt(ind + k));
            }
            sb.setLength(iLen);

        } else {
            sb = new StringBuffer("");
        }
        return sb;
    }


/*
 * Java methods for fgets c-function
 */

    /**
     * 
     * 
     * @param br aaaa
     * @param cr aaaa
     * @param size aaaa
     * @return an Object instance
     * @throws SbmeMatchEngineException aaaa
     */
    public static Object fgets(BufferedReader br, char[] cr, int size)
        throws SbmeMatchEngineException {

        String s;
        
        try {
            s = br.readLine();
        } catch (IOException ie) {
            throw new SbmeMatchEngineException(" ", ie);
        }

        if (s == null) {
            return Boolean.FALSE;
        }

        int k = s.length();
        if (k < size - 1) {
            cr = s.toCharArray();

        } else {
            cr = (s.substring(0, size - 1)).toCharArray();
        }
        s = String.valueOf(cr);
        return s;

    }


    /**
     * 
     * 
     * @param br  aaaa
     * @param sb aaaa
     * @param size aaaa
     * @return the size of the string
     * @throws SbmeMatchEngineException aaaa
     */
    public static Object fgets(BufferedReader br, StringBuffer sb, int size)
        throws SbmeMatchEngineException  {

        String s;

        try {
            s = br.readLine();
        } catch (IOException ie) {
            throw new SbmeMatchEngineException(" ", ie);
        }


        if (s == null) {
            return Boolean.FALSE;
        }

        sb.delete(0, sb.length());
        int k = s.length();

        if ((k < size) || (size == 0)) {
            sb.insert(0, s);

        } else {
            sb.insert(0, s.substring(0, size));
        }
        return sb;
    }

/*
 * Java methods for feof c-function
 */

    /**
     * 
     * 
     * @param br aaaa
     * @return a boolean
     * @throws SbmeMatchEngineException aaaa
     */
    public static boolean feof(BufferedReader br)
        throws SbmeMatchEngineException {

        String s;

        try {
            br.mark(500);
            s = br.readLine();
            br.reset();
        } catch (IOException ie) {
            throw new SbmeMatchEngineException(" ", ie);
        }

        return (s == null);
    }

/*
 * Java methods for I/O c-function
 */


    /**
     * 
     * 
     * @param file aaaa
     * @return a BufferedReader
     * @throws SbmeMatchEngineException aaaa
     */
    public static BufferedReader openBR(File file)
        throws SbmeMatchEngineException {
        
        try {
            FileInputStream fS = new FileInputStream(file);
            InputStreamReader sR = new InputStreamReader(fS);
            BufferedReader br = new BufferedReader(sR);
            return br;
        } catch (FileNotFoundException fe) { 
            throw new SbmeMatchEngineException("File not found ", fe);
        }
    }


    /**
     * 
     * 
     * @param string aaaa
     * @return a BufferedReader
     * @throws SbmeMatchEngineException aaaa
     */
    public static BufferedReader openBR(String string)
        throws SbmeMatchEngineException {

        try {
            FileInputStream fS = new FileInputStream(string);
            InputStreamReader sR = new InputStreamReader(fS);
            BufferedReader br = new BufferedReader(sR);
            return br;
        } catch (FileNotFoundException fe) { 
            throw new SbmeMatchEngineException("File not found ", fe);
        }
    }


    /**
     * 
     * 
     * @param sb aaaa
     * @return a BufferedReader
     * @throws SbmeMatchEngineException aaaa
     */
    public static BufferedReader openBR(StringBuffer sb)
        throws SbmeMatchEngineException {

        try {
            String s = sb.toString();
            FileInputStream fS = new FileInputStream(s);
            InputStreamReader sR = new InputStreamReader(fS);
            BufferedReader br = new BufferedReader(sR);
            return br;
        } catch (FileNotFoundException fe) { 
            throw new SbmeMatchEngineException("File not found ", fe);
        }
    }


    /**
     * 
     * 
     * @param file aaaa
     * @return a StreamTokenizer
     * @throws SbmeMatchEngineException aaaa
     */
    public static StreamTokenizer openST(File file)
        throws SbmeMatchEngineException {

        try {
            FileInputStream fS = new FileInputStream(file);
            InputStreamReader sR = new InputStreamReader(fS);
            BufferedReader bR = new BufferedReader(sR);
            StreamTokenizer tokens = new StreamTokenizer(bR);
            return tokens;
        } catch (FileNotFoundException fe) { 
            throw new SbmeMatchEngineException("File not found ", fe);
        }
    }


    /**
     * 
     * 
     * @param string  aaaa
     * @return a StreamTokenizer
     * @throws SbmeMatchEngineException aaaa
     */
    public static StreamTokenizer openST(String string)
        throws SbmeMatchEngineException {

        try {
            FileInputStream fS = new FileInputStream(string);
            InputStreamReader sR = new InputStreamReader(fS);
            BufferedReader bR = new BufferedReader(sR);
            StreamTokenizer tokens = new StreamTokenizer(bR);
            return tokens;
        } catch (FileNotFoundException fe) { 
            throw new SbmeMatchEngineException("File not found ", fe);
        }
    }


    /**
     * 
     * 
     * @param file aaaa
     * @return a PrintWriter
     * @throws SbmeMatchEngineException aaaa
     */
    public static PrintWriter openPW(File file)
        throws SbmeMatchEngineException {

        try {
            FileOutputStream oS = new FileOutputStream(file);
            PrintWriter pw = new PrintWriter(oS);
            return pw;
        } catch (FileNotFoundException fe) { 
            throw new SbmeMatchEngineException("File not found ", fe);
        }
    }


    /**
     * 
     * 
     * @param string aaaa
     * @return a PrintWriter
     * @throws SbmeMatchEngineException aaaa
     */
    public static PrintWriter openPW(String string)
        throws SbmeMatchEngineException {

        try {
            FileOutputStream oS = new FileOutputStream(string);
            PrintWriter pw = new PrintWriter(oS);
            return pw;
        } catch (FileNotFoundException fe) { 
            throw new SbmeMatchEngineException("File not found ", fe);
        }
    }


    /**
     * 
     * 
     * @param sb aaaa
     * @return a PrintWriter
     * @throws SbmeMatchEngineException aaaa
     */
    public static PrintWriter openPW(StringBuffer sb)
        throws SbmeMatchEngineException {

        try {
            FileOutputStream oS = new FileOutputStream(sb.toString());
            PrintWriter pw = new PrintWriter(oS);
            return pw;
        } catch (FileNotFoundException fe) { 
            throw new SbmeMatchEngineException("File not found ", fe);
        }
    }


    /**
     * 
     * 
     * @param sb aaaa
     * @return a BufferedWriter
     * @throws SbmeMatchEngineException aaaa
     */
    public static BufferedWriter openBrPW(StringBuffer sb)
        throws SbmeMatchEngineException {

        try {
            FileOutputStream oS = new FileOutputStream(sb.toString());
            PrintWriter pw = new PrintWriter(oS);
            BufferedWriter bw = new BufferedWriter(pw);
            return bw;
        } catch (FileNotFoundException fe) { 
            throw new SbmeMatchEngineException("File not found ", fe);
        }
    }


    /**
     * 
     * 
     * @param sb  aaaa
     * @param size aaaa
     * @return a BufferedWriter
     * @throws SbmeMatchEngineException aaaa
     */
    public static BufferedWriter openBrPW(StringBuffer sb, int size)
        throws SbmeMatchEngineException {

        try {
            FileOutputStream oS = new FileOutputStream(sb.toString());
            PrintWriter pw = new PrintWriter(oS);
            BufferedWriter bw = new BufferedWriter(pw, size);
            return bw;
        } catch (FileNotFoundException fe) { 
            throw new SbmeMatchEngineException("File not found ", fe);
        }
    }

/*
 * Java methods for fprintf c-function
 */
 
 
    /**
     * 
     * 
     * @param sb aaaa
     * @param s aaaa
     * @param sb1 aaaa
     * @return a aaaa
     * @throws SbmeMatchEngineException aaaa
     */
    public static StringBuffer sprintf(StringBuffer sb, String s, StringBuffer sb1)
        throws SbmeMatchEngineException {

        sb.setLength(0);
        
        return sb.append(getSubstring(sb1, s));
    }


    /**
     * 
     * 
     * @param aList aaaa
     * @param aL aaaa
     * @param aL1 aaaa
     * @param len aaaa
     * @throws SbmeMatchEngineException aaaa
     */
    public static void sprintf(ArrayList aList, ArrayList aL, ArrayList aL1, int len)
        throws SbmeMatchEngineException {

        int i;

        for (i = 0; i < len; i++) {       
            aList.add(i, getSubstring(aL1.get(i).toString(), aL.get(i).toString()));
        }
    }


    /**
     * 
     * 
     * @param sb aaaa
     * @param aL aaaa
     * @param aL1 aaaa
     * @param len aaaa
     * @throws SbmeMatchEngineException aaaa
     */
    public static void sprintf(StringBuffer sb, ArrayList aL, ArrayList aL1, int len)
        throws SbmeMatchEngineException {

        int i;
        sb.setLength(0);

        for (i = 0; i < len; i++) {
//            sb.append(new PrintfFormat(aL.get(i).toString()).sprintf(aL1.get(i).toString()));
            sb.append(getSubstring(aL1.get(i).toString(), aL.get(i).toString()));
        }
    }


    /**
     * 
     * 
     * @param sb aaaa
     * @param i aaaa
     * @param s1 aaaa
     * @param s2 aaaa
     * @param sb1 aaaa
     * @param sb2 aaaa
     * @throws SbmeMatchEngineException aaaa
     */
    public static void sprintf(StringBuffer sb, int i, String s1, String s2, StringBuffer sb1,
                               StringBuffer sb2)
        throws SbmeMatchEngineException {

        sb.setLength(i);

//        sb.append(new PrintfFormat(s1).sprintf(sb1));
//        sb.append(new PrintfFormat(s2).sprintf(sb2));
        sb.append(getSubstring(sb1, s1));
        sb.append(getSubstring(sb2, s2));
    }


    /**
     * 
     * 
     * @param sb aaaa
     * @param i aaaa
     * @param s1 aaaa
     * @param s2 aaaa
     * @param sb1 aaaa
     * @param str2 aaaa
     * @throws SbmeMatchEngineException aaaa
     */
    public static void sprintf(StringBuffer sb, int i, String s1, String s2, StringBuffer sb1, 
                               String str2)
        throws SbmeMatchEngineException {

//        String s = sb.substring(i);
        sb.append(sb.substring(i));

//        sb.append(new PrintfFormat(s1).sprintf(sb1));
//        sb.append(new PrintfFormat(s2).sprintf(sb2));
        sb.append(getSubstring(sb1, s1));
        sb.append(getSubstring(str2, s2));
    }


    /**
     * 
     * 
     * @param sb aaaa
     * @param s1 aaaa
     * @param s2 aaaa
     * @param s3 aaaa
     * @param sb1 aaaa
     * @param sb2 aaaa
     * @param sb3 aaaa
     * @throws SbmeMatchEngineException aaaa
     */
    public static void sprintf(StringBuffer sb, String s1, String s2, String s3, StringBuffer sb1,
                               StringBuffer sb2, StringBuffer sb3)
        throws SbmeMatchEngineException {

        sb.setLength(0);
//        sb.append(new PrintfFormat(s1).sprintf(sb1));
//        sb.append(new PrintfFormat(s2).sprintf(sb2));
//        sb.append(new PrintfFormat(s3).sprintf(sb3));
        sb.append(getSubstring(sb1, s1));
        sb.append(getSubstring(sb2, s2));
        sb.append(getSubstring(sb3, s3));
    }


    /**
     * 
     * 
     * @param sb aaaa
     * @param i aaaa
     * @param s1 aaaa
     * @param s2 aaaa
     * @param str3 aaaa
     * @param sb2 aaaa
     * @throws SbmeMatchEngineException aaaa
     */
    public static void sprintf(StringBuffer sb, int i, String s1, String s2,
                               String str3, StringBuffer sb2)
        throws SbmeMatchEngineException {

        sb.setLength(i);
//        sb.append(new PrintfFormat(s1).sprintf(str3));
//        sb.append(new PrintfFormat(s2).sprintf(sb2));
        sb.append(getSubstring(str3, s1));
        sb.append(getSubstring(sb2, s2));
    }


    /**
     * 
     * 
     * @param sb aaaa
     * @param i aaaa
     * @param s aaaa
     * @param sb1 aaaa
     * @return a stringbuffer
     * @throws SbmeMatchEngineException aaaa
     */
    public static StringBuffer sprintf(StringBuffer sb, int i, String s, StringBuffer sb1) 
        throws SbmeMatchEngineException {

        sb.setLength(i);
//        return sb.append(new PrintfFormat(s).sprintf(sb1));
        return sb.append(getSubstring(sb1, s));
    }


    /**
     * 
     * 
     * @param sb aaaa
     * @param s  aaaa
     * @param sb1 aaaa
     * @param ind aaaa
     * @return a stringbuffer
     * @throws SbmeMatchEngineException aaaa
     */
    public static StringBuffer sprintf(StringBuffer sb, String s, StringBuffer sb1, int ind)
        throws SbmeMatchEngineException {

        sb.setLength(0);

        // Take into consideration the case where the index is out of range
        if (ind < sb1.length()) {

             sb.append(getSubstring(sb1.substring(ind), s));

             return sb;
            
        } else {
            throw new IndexOutOfBoundsException("Index out of range");
        }
    }


    /**
     * 
     * 
     * @param sb aaaa
     * @param i1 aaaa
     * @param s aaaa
     * @param sb1 aaaa
     * @param i2 aaaa
     * @return aaaaa
     * @throws SbmeMatchEngineException an exception
     */
    public static StringBuffer sprintf(StringBuffer sb, int i1, String s, StringBuffer sb1, int i2)
         throws SbmeMatchEngineException {

        sb.setLength(i1);
        
        // Take into consideration the case where the index is out of range
        if (i2 < sb1.length()) {
//            return sb.append(new PrintfFormat(s).sprintf(sb1.substring(i2)));
            return sb.append(getSubstring(sb1.substring(i2), s));
        } else {
            throw new IndexOutOfBoundsException("Index out of range");
        }
    }

    /**
     * 
     * 
     * @param sb aaaa
     * @param i1 aaaa
     * @param s aaaa
     * @param s1 aaaa
     * @return aaaa
     * @throws SbmeMatchEngineException aaaa
     */
    public static String sprintf(StringBuffer sb, int i1, String s, String s1)
        throws SbmeMatchEngineException {

        sb.setLength(i1);
//        sb.append(new PrintfFormat(s).sprintf(s1));
        return sb.append(getSubstring(s1, s)).toString();
//        return sb.toString();
    }


    /**
     * 
     * 
     * @param sb aaaa aaaa
     * @param s aaaa
     * @param s1 aaaa
     * @return aaaaa
     * @throws SbmeMatchEngineException aaaa
     */
    public static StringBuffer sprintf(StringBuffer sb, String s, String s1)
        throws SbmeMatchEngineException {

        sb.setLength(0);
//        sb.append(new PrintfFormat(s).sprintf(s1));
        return sb.append(getSubstring(s1, s));
//        return sb;
    }


    /**
     * 
     * 
     * @param s aaaa
     * @param s2 aaaa
     * @return aaaa
     * @throws SbmeMatchEngineException aaaa
     */
    public static StringBuffer sprintf(String s, String s2)
        throws SbmeMatchEngineException {

//         StringBuffer sb = new StringBuffer();
//        sb.append(new PrintfFormat(s).sprintf(s2));
        return new StringBuffer(getSubstring(s2, s));
//        return sb;
    }

/*
 * Create ArraList of Ojects
 */

    /**
     * 
     * 
     * @param aS aaaa
     * @param sb aaaa
     */
    public static void addSb(ArrayList aS, StringBuffer sb) {

        // Mesure the length of the ArrayList, then add a new StringBuffer
        aS.add(sb);
    }


    /**
     * 
     * 
     * @param aS aaaa
     * @param al aaaa
     */
    public static void addAl(ArrayList aS, ArrayList al) {

        // Mesure the length of the ArrayList, then add a new StringBuffer
        aS.add(al);
    }


    /**
     * Method used in setters
     * 
     * @param sb aaaa
     * @param s aaaa
     * @return a StringBuffer
     */
    public static StringBuffer replace(StringBuffer sb, String s) {

        if (sb == null) {
            sb = new StringBuffer();
        }

        sb.setLength(0);
        return sb.append(s);
    }
    
    /**
     * Method used in setters
     *
     * @param al aaaa
     * @param s aaaa
     * @return an ArrayList
     */
    public static ArrayList replace(ArrayList al, ArrayList s) {

        if (al == null) {
            al = new ArrayList();
        }
        
        int i;
        
        int len = s.size();
        
        for (i = 0; i < len; i++) {
            al.add(0, s.get(i));
        }
        
        return al;
    }

    /**
     * Method used in setters
     *
     * @param al aaaa
     * @param s aaaa
     * @return an ArrayList
     */
    public static HashMap replaceHash(HashMap hs, HashMap s) {

        if (hs == null) {
            hs = new HashMap();
        }
        
        int i;
        Set st;
        Object[] objs;
        
        int len = s.size();
        st = s.keySet();

        objs = st.toArray();
        String[] keys = new String[len];
        
        for (i = 0; i < len; i++) {
            hs.put(keys[i], s.get(keys[i]));
        }
        
        return hs;
    }

    
/*
 * Read a subset of the elements of an array
 */
 
    /**
     * 
     * 
     * @param sb aaaa
     * @param in aaaa
     * @param size aaaa
     * @return an array of StringBuffer objects
     */
    public static StringBuffer[] readSubArray(StringBuffer[] sb, int in, int size) {

        int i;

        StringBuffer[] ssb = new StringBuffer[size - in];

        for (i = 0; i < size - in; i++) {
            ssb[i] = sb[i + in];
        }

        return ssb;
    }

/*
 * memset-equivalent function
 */

    /**
     * 
     * 
     * @param sb aaaa
     * @param cha aaaa
     * @param size aaaa
     * @return a String
     */
    public static String memset(StringBuffer sb, char cha, int size) {

        int i;

        if (sb == null) {
            sb = new StringBuffer();
        }

        int len = sb.length();

        if (size >= len) {
            sb.setLength(0);

            for (i = 0; i < len; i++) {
                sb.append(cha);
            }
          
        } else {

            for (i = 0; i < size; i++) {
                sb.setCharAt(i, cha);
            }
        }
        return sb.toString();
    }


    /**
     * 
     * 
     * @param sb aaaa
     * @param cha aaaa
     * @param size aaaa
     * @return a StringBuffer
     */
    public static StringBuffer memset1(StringBuffer sb, char cha, int size) {

        int i;

        if (sb == null) {
            sb = new StringBuffer();
        }

        int len = sb.length();

        if (size >= len) {

            for (i = 0; i < len; i++) {
                sb.setCharAt(i, cha);
            }

        } else {

            for (i = 0; i < size; i++) {
                sb.setCharAt(i, cha);
            }
        }
        return sb;
    }


    /**
     * 
     * 
     * @param sb aaaa
     * @param cha aaaa
     * @param size aaaa
     * @return a StringBuffer
     */
    public static StringBuffer memset2(StringBuffer sb, char cha, int size) {

        int i;
        int len = sb.length();

        if (size > len) {
            size = len;
        }

        for (i = 0; i < size; i++) {
            sb.setCharAt(i, cha);
        }
        return sb;
    }


    /**
     * 
     * 
     * @param s aaaa
     * @param cha aaaa
     * @param size aaaa
     * @return a StringBuffer
     */
    public static StringBuffer memset(String s, char cha, int size) {

        int i;
        StringBuffer sb;

        if (s == null) {
            s = new String();
        }

        sb = new StringBuffer(s);

        int len = s.length();

        if (size >= len) {

            for (i = 0; i < len; i++) {
                sb.setCharAt(i, cha);
            }

        } else {

            for (i = 0; i < size; i++) {
                sb.setCharAt(i, cha);
            }
        }
        return sb;
    }

/*
 * strchr-equivalent function 
 */ 

    /**
     * 
     * 
     * @param sb aaaa
     * @param cha aaaa
     * @return an integer
     */
    public static int strchrInt(StringBuffer sb, char cha) {
        
        return sb.toString().indexOf(cha);
    }


    /**
     * 
     * 
     * @param sb aaaa
     * @param cha aaaa
     * @return a StringBuffer
     */
    public static StringBuffer strchr(StringBuffer sb, char cha) {
    
        // If the string sb is null return a null object
        if (sb == null) {
            return null;
        }
        
        // The length of the string
        int iLen = sb.length();
        int i;
        
        // Search for the character cha
        for (i = 0; i < iLen; i++) {
            if (sb.charAt(i) == cha) {
                return sb.delete(0, i);
            }
        }
        
        return null;
    }


    /**
     * 
     * 
     * @param sb aaaa
     * @param ind aaaa
     * @param cha aaaa
     * @return a StringBuffer
     */
    public static StringBuffer strchr(StringBuffer sb, int ind, char cha) {

        // If the string sb is null return a null object
        if (sb == null) { 
            return null;
        }

        // The length of the string
        int iLen = sb.length();
        int i;
        
        // Search for the character cha
        for (i = ind; i < iLen; i++) {
            if (sb.charAt(i) == cha) {
                return sb.delete(0, i);
            }
        }
        
        return null;
    }


    /**
     * 
     * 
     * @param s aaaa
     * @param delim aaaa
     * @return an array of String objects
     */
    public static String[] stringArrayTokens(String s, String delim) {

        int i = 0;
        StringTokenizer st = new StringTokenizer(s, delim);

        String[] sa = new String[st.countTokens()];

        // count the number of tokens in the string, and initialize the array of strings
        // Assign tokens of tokens_blank to arrays of strings 
        while (st.hasMoreElements()) {
            sa[i] = new String(st.nextToken());
            i++;
        }
        return sa;
    }


    /**
     * 
     * 
     * @param s aaaa
     * @param delim aaaa
     * @return an array of characters
     */
    public static char[] charArrayTokens(String s, String delim) {

        int i = 0;
        StringTokenizer st = new StringTokenizer(s, delim);

        char[] ca = new char[st.countTokens()];

        // count the number of tokens in the string, and initialize the array of strings
        // Assign tokens of tokens_blank to arrays of strings 
        while (st.hasMoreElements()) {

            ca[i] = st.nextToken().charAt(0);
            i++;
        }
        return ca;
    }

/*
 * strstr-equivalent function
 */

    /**
     * 
     * 
     * @param sb aaaa
     * @param s aaaa
     * @return an integer
     */
    public static int strstr(StringBuffer sb, String s) {
        return sb.toString().indexOf(s);
    }
/*
 * Unsigned integer function
 */

    /**
     * 
     * 
     * @param up aaaa
     * @param down aaaa
     * @return an integer
     */
    public static int unsignedIntRemainder(int up, int down) {

        long lVar = (4294967296L + up) % down;
        return (int) lVar;
    }

/*
 * Parse integers 
 */

    /**
     * 
     * 
     * @param s aaaa
     * @return an integer
     */
    public static int parseIntMethod(String s) {

        int i;
        int j = 0;

        // Check if the string is empty
        for (i = 0; i < s.length(); i++) {

            if (s.charAt(i) != ' ') {
                j = 1;
                break;
            }
        }

        // parse normally as in java
        if (j == 1) {
            return Integer.parseInt(s.trim());

        } else {
            return 0;
        }
    }
    
    
    /**
     * 
     * 
     * @param sb the input string
     * @param s the format 
     * @return a formatted substring
     * @throws SbmeMatchEngineException aaaa
     */
    public static String getSubstring(StringBuffer sb, String s)
        throws SbmeMatchEngineException {
        
        int nLen = 0;
        int number = 0;
        int i = 2;
        int sLen = s.length();
        int sbLen = sb.length();
        char cr;
        
        // Make sure that the first two chars are '%.'
        if (s.substring(0, 2).compareTo("%.") == 0) { 
            
            while (Character.isDigit(s.charAt(i))) {
                i++;
            }
            
            nLen = i - 2;
            number = Integer.parseInt(s.substring(2, i));
            cr = s.charAt(i);

            if (cr == 's' && (sLen == i + 1)) {
                
                if (sbLen > number) {
                    return sb.substring(0, number);
                } else {
                    return sb.toString();
                }
                
            } else if (cr == 's' && (sLen > (i + 1))) {
                
                if (sbLen > number) {
                    return sb.substring(0, number).concat(" ");
                } else {
                    return sb.append(" ").toString();
                }
            }
        }
        return "";
        
    }


    /**
     * 
     * 
     * @param str the input string
     * @param s the format 
     * @return a formatted substring
     * @throws SbmeMatchEngineException aaaa
     */
    public static String getSubstring(String str, String s)
        throws SbmeMatchEngineException {
        
        int nLen = 0;
        int number = 0;
        int i = 2;
        int sLen = s.length();
        int strLen = str.length();
        char cr;
        
        // Make sure that the first two chars are '%.'
        if (s.substring(0, 2).compareTo("%.") == 0) { 
            
            while (Character.isDigit(s.charAt(i))) {
                i++;
            }
            
            nLen = i - 2;
            number = Integer.parseInt(s.substring(2, i));
            cr = s.charAt(i);

            if (cr == 's' && (sLen == i + 1)) {
                if (strLen > number) {
                    return str.substring(0, number);
                } else {
                    return str;
                }
                
            } else if (cr == 's' && (sLen > (i + 1))) {
                
                if (strLen > number) {
                    return str.substring(0, number).concat(" ");
                } else {
                    return str.concat(" ");
                }
            }
        }
        return "";      
    }
    
    /**
     *
     *
     * @param str the input string
     * @return a boolean that specify if the string is formed only from numbers
     */
    public static boolean isNumber(String str) {
            int i;
            int len = str.length();

            for (i = 0; i < len; i++) {
                if (!Character.isDigit(str.charAt(i)) && !(str.charAt(i) == '.')
                    && (i > 0 && !(str.charAt(i) == ','))) {

                    return false;
                }
            }
            return true;
    }  
    
    /**
     *
     *
     * @param str the input string
     * @return a boolean that specify if the string is formed only from numbers
     */
    public static boolean isAlphaNumeric(String str) {
            int i;
            int len = str.length();

            for (i = 0; i < len; i++) {
                if (!Character.isLetterOrDigit(str.charAt(i)) && !(str.charAt(i) ==
                '.')
                    && (i > 0 && !(str.charAt(i) == ','))) {
                    return false;
                }
            }
            return true;
    }
    
    /**
     * Trim the string on the left side only
     *
     * @param str the input string
     * @return a left-trimed StringBuffer
     */
    public static void trimOnLeft(StringBuffer str) {

        int len = str.length();

        if (len == 0) {
            return;
        }

        while((str.length() > 0) 
            && Character.isWhitespace(str.charAt(str.length() - 1)) ) {
            str.deleteCharAt(str.length() - 1);
        }
    }


    
    /**
     * Compares a string to a stringbuffer
     *
     * @param s1 the first string
     * @param s2 the second string
     * @return the comparison result 
     */
    public static boolean compareStrings(String s1, StringBuffer s2) {
        
        int len1 = s1.length();
        int len2 = s2.length();
        int i;
        
        if (len1 != len2) {
            return false;
        }        
                
        for (i = 0; i < len1; i++) {
            
            if (s1.charAt(i) !=  s2.charAt(i)) {
                return false;
            }
        }
        
        return true;       
    }
  
    /**
     * Compares two strings over a length len
     *
     * @param s1 the first string
     * @param s2 the second string
     * @param ind the length over which we compare the strings
     * @return true if they match over length len, false otherwise 
     */
    public static boolean compareNStrings(String s1, String s2, int len) {
        
        int len1 = s1.trim().length();
        int len2 = s2.trim().length();
        int minL = Math.min(len1, len2);
        int i;
        
        // If the two strings have different lengths and the scale over which we 
        // compare is longer than the min
        if ((len1 != len2) & minL < len) {
            return false;
        }        
        
        for (i = 0; i < minL; i++) {    
            if (s1.charAt(i) !=  s2.charAt(i)) {
                return false;
            }
        }
        
        return true;       
    }
    
    /**
     * Removes diacritical mark from a character
     *
     * @param ch a character
     * @return the same input character without the diacritical mark
     * if any. 
     */ 
    public static char removeDiacriticalMark(char c) {
        
        if (c < 192)
            return c;
        if (c >= 192 && c <= 197)
            return 'A';
        if (c == 199)
            return 'C';
        if (c >= 200 && c <= 203)
            return 'E';
        if (c >= 204 && c <= 207)
            return 'I';
        if (c == 208)
            return 'D';
        if (c == 209)
            return 'N';
        if ((c >= 210 && c <= 214) || c == 216)
            return 'O';
        if (c >= 217 && c <= 220)
            return 'U';
        if (c == 221)
            return 'Y';
        if (c >= 224 && c <= 229)
            return 'a';
        if (c == 231)
            return 'c';
        if (c >= 232 && c <= 235)
            return 'e';
        if (c >= 236 && c <= 239)
            return 'i';
        if (c == 240)
            return 'd';
        if (c == 241)
            return 'n';
        if ((c >= 242 && c <= 246) || c == 248)
            return 'o';
        if (c >= 249 && c <= 252)
            return 'u';
        if (c == 253 || c == 255)
            return 'y';
    
        return c;   
    }            
    /**
     * Removes diacritical mark from a string
     *
     * @param st a string
     * @return the same input string without the diacritical mark if any. 
     */ 
    public static String removeDiacriticalMark(String st) {
        
        int len = st.length();
        String tempS = new String(st);
        
        for (int i = 0; i < len; i++) {
             char ch = tempS.charAt(i);
             tempS = tempS.replace(ch, removeDiacriticalMark(ch));   
        }  
        return tempS;   
    }                            
}
