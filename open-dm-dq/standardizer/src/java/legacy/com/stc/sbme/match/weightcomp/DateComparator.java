/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.match.weightcomp;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.StringTokenizer;

import com.stc.sbme.api.SbmeMatchingException;

/**
 * This class handles comparison between dates and times in a standard format  
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
public class DateComparator {

    /**
     * 
     *
     * @param dateA  Candidate's string record.
     * @param dateB  Reference's string record.
     * @param index  the time unit used
     * @param matchVar an instance of the matchVariables class
     * @return a real number that measures the degree of match [0,1]
     * @throws SbmeMatchingException an exception
     * @throws ParseException if a string parsing failed
     */
    static double compareDates(String dateA, String dateB, int index, MatchVariables matchVar)
        throws SbmeMatchingException, ParseException {
        
        boolean secondChar = true;
        char theUnit = '0';
        int i = 0;
        int[] unitsListA = {-1, -1, -1, -1, -1, -1};
        int[] unitsListB = {-1, -1, -1, -1, -1, -1};

        if (matchVar.comparatorType[index].charAt(0) != 'd') {
            throw new SbmeMatchingException("Use a date comparator starting with: 'd'");
        }
        
        if (matchVar.comparatorType[index].charAt(1) == ' ') {
            theUnit = 'D';
            secondChar = false;
            
        } else {
            theUnit = matchVar.comparatorType[index].charAt(1);
        } 

        // Handle the date format for both records
        DateFormat df = new SimpleDateFormat("yyyyMMdd", Locale.US);                 
        Calendar cal1 = new GregorianCalendar();
        Calendar cal2 = new GregorianCalendar();

        // Parse the dates into their basic components
        Date date1 = df.parse(dateA);
        Date date2 = df.parse(dateB);

        cal1.setTime(date1);
        cal2.setTime(date2);

        // For use in the transposer
        if (matchVar.trans[index]) {
        
            StringTokenizer sTA = new StringTokenizer(date1.toString(), " :");
            StringTokenizer sTB = new StringTokenizer(date2.toString(), " :");
            
            String[] timeSA = new String[8];
            String[] timeSB = new String[8];
            
            while (sTA.hasMoreTokens()) { 
            
                if (i == 0) {
                    sTA.nextToken();

                } else if (i == 1) {                    
                    unitsListA[1] = cal1.get(Calendar.MONTH);
                    
                    if (unitsListA[1] < 10) {
                        timeSA[1] = "0".concat(Integer.toString(unitsListA[1] + 1));
                    } else {
                        timeSA[1] = Integer.toString(unitsListA[1] + 1);
                    }
                    
                    sTA.nextToken();
                    
                } else if (i == 2) {
                    timeSA[2] = sTA.nextToken();
                } else if (i == 3) {
                    timeSA[3] = sTA.nextToken();
                } else if (i == 4) {
                    timeSA[4] = sTA.nextToken();
                } else if (i == 5) {
                    timeSA[5] = sTA.nextToken();
                } else if (i == 7) {
                    timeSA[0] = sTA.nextToken();
                } else {
                    sTA.nextToken();
                }
                
                i++;
            }
            
            i = 0;
            while (sTB.hasMoreTokens()) {
            
                if (i == 0) {
                    timeSB[0] = sTB.nextToken();

                } else if (i == 1) {
                    unitsListB[1] = cal2.get(Calendar.MONTH);
                    
                    if (unitsListB[1] < 10) { 
                        timeSB[1] = "0".concat(Integer.toString(unitsListB[1] + 1));
                    } else {
                        timeSB[1] = Integer.toString(unitsListB[1] + 1);
                    }
                    
                    sTB.nextToken();
                    
                } else if (i == 2) {
                    timeSB[2] = sTB.nextToken();
                } else if (i == 3) {
                    timeSB[3] = sTB.nextToken();
                } else if (i == 4) {
                    timeSB[4] = sTB.nextToken();
                } else if (i == 5) {
                    timeSB[5] = sTB.nextToken();
                } else if (i == 7) { 
                    timeSB[0] = sTB.nextToken();
                } else {
                    sTB.nextToken();
                }
                
                i++;
            }
            
            // Call the transposer and return a standard date objects
            return transposeAndCompareDates(timeSA, timeSB, index, theUnit, matchVar);
            
        } else {
        
            if (theUnit == 'Y') {
                unitsListA[0] = cal1.get(Calendar.YEAR);
                unitsListB[0] = cal2.get(Calendar.YEAR);
            
            } else if (theUnit == 'M') {
                unitsListA[0] = cal1.get(Calendar.YEAR);
                unitsListB[0] = cal2.get(Calendar.YEAR);
            
                unitsListA[1] = cal1.get(Calendar.MONTH);
                unitsListB[1] = cal2.get(Calendar.MONTH);
            
            } else if (theUnit == 'D' || !secondChar) {
                unitsListA[0] = cal1.get(Calendar.YEAR);
                unitsListB[0] = cal2.get(Calendar.YEAR);
            
                unitsListA[2] = cal1.get(Calendar.DAY_OF_YEAR);
                unitsListB[2] = cal2.get(Calendar.DAY_OF_YEAR);
            
            } else if (theUnit == 'H') {
                unitsListA[0] = cal1.get(Calendar.YEAR);
                unitsListB[0] = cal2.get(Calendar.YEAR);

                unitsListA[2] = cal1.get(Calendar.DAY_OF_YEAR);
                unitsListB[2] = cal2.get(Calendar.DAY_OF_YEAR);
            
                unitsListA[3] = cal1.get(Calendar.HOUR_OF_DAY);
                unitsListB[3] = cal2.get(Calendar.HOUR_OF_DAY);
                
            } else if (theUnit == 'm') {
            
                unitsListA[0] = cal1.get(Calendar.YEAR);
                unitsListB[0] = cal2.get(Calendar.YEAR);

                unitsListA[2] = cal1.get(Calendar.DAY_OF_YEAR);
                unitsListB[2] = cal2.get(Calendar.DAY_OF_YEAR);
            
                unitsListA[3] = cal1.get(Calendar.HOUR_OF_DAY);
                unitsListB[3] = cal2.get(Calendar.HOUR_OF_DAY);
            
                unitsListA[4] = cal1.get(Calendar.MINUTE);
                unitsListB[4] = cal2.get(Calendar.MINUTE);
            
            } else if (theUnit == 's') {
            
                unitsListA[0] = cal1.get(Calendar.YEAR);
                unitsListB[0] = cal2.get(Calendar.YEAR);

                unitsListA[2] = cal1.get(Calendar.DAY_OF_YEAR);
                unitsListB[2] = cal2.get(Calendar.DAY_OF_YEAR);

                unitsListA[3] = cal1.get(Calendar.HOUR_OF_DAY);
                unitsListB[3] = cal2.get(Calendar.HOUR_OF_DAY);
            
                unitsListA[4] = cal1.get(Calendar.MINUTE);
                unitsListB[4] = cal2.get(Calendar.MINUTE);
            
                unitsListA[5] = cal1.get(Calendar.SECOND);
                unitsListB[5] = cal2.get(Calendar.SECOND);
            
            } else {
                throw new SbmeMatchingException("The comparator you are using does not exist."
                    + "you must use one of the following: 'dY', 'dM', 'dD', 'dH', 'dm' and 'ds'");
            }

        
            // Don't check for transpositions
            // Evaluate the dates measured in the selected unit
            return compareDatesDist(unitsListA, unitsListB, theUnit, cal1, cal2, matchVar, index);
        }
    }


    /*
     * Consider the records as strings and measure how close they are from each other 
     */
    private static double transposeAndCompareDates(String[] listA, String[] listB, int index, char unit,
                                                   MatchVariables mVar)
            throws SbmeMatchingException {
            
        double yearW;
        double monthW;
        double dayW;
        double hourW;
        double minuteW;
        double secW;      
        double weight = 0;
        
        // Compare only the year components
        if (unit == 'Y') { 
            
            // Compare only year
            weight = GenStringComparator.genStrComparator(listA[0], listB[0], index, true, mVar);
            
        // Compare the year and months elements
        } else if (unit == 'M') {
            
            // Combine months and years into the weight
            yearW = GenStringComparator.genStrComparator(listA[0], listB[0], index, true, mVar);
            monthW = GenStringComparator.genStrComparator(listA[1], listB[1], index, true, mVar);

            weight = (yearW + monthW) / 2.;
        
        // Compare the year, the months and the day elements
        } else if (unit == 'D') {

            // Combine months and years and days into the weight
            yearW = GenStringComparator.genStrComparator(listA[0], listB[0], index, true, mVar);
            monthW = GenStringComparator.genStrComparator(listA[1], listB[1], index, true, mVar);
            dayW = GenStringComparator.genStrComparator(listA[2], listB[2], index, true, mVar);

            
            weight = (yearW + monthW + dayW) / 3.;
        
        } else if (unit == 'H') {

            // Combine months and years and days into the weight
            yearW = GenStringComparator.genStrComparator(listA[0], listB[0], index, true, mVar);
            monthW = GenStringComparator.genStrComparator(listA[1], listB[1], index, true, mVar);
            dayW = GenStringComparator.genStrComparator(listA[2], listB[2], index, true, mVar);
            hourW = GenStringComparator.genStrComparator(listA[3], listB[3], index, true, mVar);
            
            weight = (yearW + monthW + dayW + hourW) / 4.;
            
        } else if (unit == 'm') {

            // Combine months and years and days into the weight
            yearW = GenStringComparator.genStrComparator(listA[0], listB[0], index, true, mVar);
            monthW = GenStringComparator.genStrComparator(listA[1], listB[1], index, true, mVar);
            dayW = GenStringComparator.genStrComparator(listA[2], listB[2], index, true, mVar);
            hourW = GenStringComparator.genStrComparator(listA[3], listB[3], index, true, mVar);
            minuteW = GenStringComparator.genStrComparator(listA[4], listB[4], index, true, mVar);
            
            weight = (yearW + monthW + dayW + hourW + minuteW) / 5.;
            
        } else if (unit == 's') {
            
            // Combine months and years and days into the weight
            yearW = GenStringComparator.genStrComparator(listA[0], listB[0], index, true, mVar);
            monthW = GenStringComparator.genStrComparator(listA[1], listB[1], index, true, mVar);
            dayW = GenStringComparator.genStrComparator(listA[2], listB[2], index, true, mVar);
            hourW = GenStringComparator.genStrComparator(listA[3], listB[3], index, true, mVar);
            minuteW = GenStringComparator.genStrComparator(listA[4], listB[4], index, true, mVar);
            secW = GenStringComparator.genStrComparator(listA[5], listB[5], index, true, mVar);
            
            weight =  (yearW + monthW + dayW + hourW + minuteW + secW) / 6.;
        }
        
        return weight;
    }


    /*
     * Measure the relative distance between the compared dates
     */
    private static double compareDatesDist(int[] listA, int[] listB, char unit, Calendar cal1,
                                       Calendar cal2, MatchVariables mVar, int index)
            throws SbmeMatchingException {
        
        int i;
        int maxDayInYearA;
        int maxDayInYearB;
        double weight = 0;
        int delY = listB[0] - listA[0];

        // Compare only the year components
        if (unit == 'Y') { 
            
            // Compute the time-distance comparator
            weight = computeRelativeDiff(delY, index, mVar);
            
        // Compare the year and months elements
        } else if (unit == 'M') {
        
            int delM = listB[1] - listA[1];          
            delM += (12 * delY);

            // Compute the time-distance comparator
            weight = computeRelativeDiff(delM, index, mVar);
        
        // Compare the year, the months and the day elements
        } else if (unit == 'D') {

            int delD = 0;
            
            maxDayInYearA = cal1.getActualMaximum(Calendar.DAY_OF_YEAR);
            maxDayInYearB = cal2.getActualMaximum(Calendar.DAY_OF_YEAR);
            
            // If in the same year
            if (delY == 0) { 
                delD = listB[2] - listA[2];
                
            } else if (delY == 1) {
                delD = maxDayInYearA - listA[2] + listB[2];
                
            } else if (delY == -1) {
                delD = listB[2] - maxDayInYearB - listA[2];
                
            } else if (delY > 1) {
                
                delD = maxDayInYearA - listA[2] + listB[2];
       
                int interYear;
                GregorianCalendar cal = new GregorianCalendar();

                // If the years are different, then we need to compute the maximum number
                // of days relative to the respective years
                // Loop over the difference of years
                for (i = 0; i < Math.abs(delY) - 1; i++) {
                   
                   interYear = listA[0] + i + 1;
                   
                   if (cal.isLeapYear(interYear)) { 
                       delD += 366;
                   } else {
                       delD += 365;
                   }
                }
 
            } else if (delY < -1) {
                
                delD = listB[2] - maxDayInYearB - listA[2];

                int interYear;
                GregorianCalendar cal = new GregorianCalendar();

                // If the years are different, then we need to compute the maximum number
                // of days relative to the respective years
                // Loop over the difference of years
                for (i = 0; i < Math.abs(delY) - 1; i++) {
                   
                   interYear = listB[0] + i + 1;
                   
                   if (cal.isLeapYear(interYear)) { 
                       delD -= 366;
                   } else {
                       delD -= 365;
                   }
                }
            }
 
            // Compute the time-distance comparator
            weight = computeRelativeDiff(delD, index, mVar);
        
        } else if (unit == 'H') {
            
            int delD = 0;
            int delH = 0;

            maxDayInYearA = cal1.getActualMaximum(Calendar.DAY_OF_YEAR);
            maxDayInYearB = cal2.getActualMaximum(Calendar.DAY_OF_YEAR);

            // If in the same year
            if (delY == 0) {
                delD = listB[2] - listA[2];
                
            } else if (delY == 1) {
                delD = maxDayInYearA - listA[2] + listB[2];
                
            } else if (delY == -1) {
                delD = listB[2] - maxDayInYearB - listA[2];
            }
       
            // Check first if the compared times are within the same day
            if (delD == 0) {
            
                delH = listB[3] - listA[3];
                
            } else {
                delH = listB[3] - listA[3] + 24 * delD;              
            }
            
            // Compute the time-distance comparator
            weight = computeRelativeDiff(delH, index, mVar);
            
        } else if (unit == 'm') {
        
            int delD = listB[2] - listA[2];
            int delH = listB[3] - listA[3];
            int delm = listB[4] - listA[4];
            
            maxDayInYearA = cal1.getActualMaximum(Calendar.DAY_OF_YEAR);
            maxDayInYearB = cal2.getActualMaximum(Calendar.DAY_OF_YEAR);

            // If in the same year
            if (delY == 0) {
                delD = listB[2] - listA[2];

            } else if (delY == 1) {
                delD = maxDayInYearA - listA[2] + listB[2];

            } else if (delY == -1) {
                delD = listB[2] - maxDayInYearB - listA[2];
            }
  
            // Check first if the compared times are within the same day
            if (delD == 0) {
                delH = listB[3] - listA[3];
                
            } else {
                delH = listB[3] - listA[3] + 24 * delD;
            }

            if (delH == 0) {
                delm = listB[4] - listA[4];
                
            } else {
                delm = listB[4] - listA[4] + 60 * delH;
            }
                
            // Compute the time-distance comparator
            weight = computeRelativeDiff(delm, index, mVar);
            
        } else if (unit == 's') {

            int delD = listB[2] - listA[2];
            int delH = listB[3] - listA[3];
            int delm = listB[4] - listA[4];
            int dels = listB[5] - listA[5];
 
            maxDayInYearA = cal1.getActualMaximum(Calendar.DAY_OF_YEAR);
            maxDayInYearB = cal2.getActualMaximum(Calendar.DAY_OF_YEAR);

            // If in the same year
            if (delY == 0) {
                delD = listB[2] - listA[2];

            } else if (delY == 1) {
                delD = maxDayInYearA - listA[2] + listB[2];

            } else if (delY == -1) {
                delD = listB[2] - maxDayInYearB - listA[2];
            }

            // Check first if the compared times are within the same day
            if (delD == 0) {
                delH = listB[3] - listA[3];
                
            } else {
                delH = listB[3] - listA[3] + 24 * delD;
            }

            if (delH == 0) {
                delm = listB[4] - listA[4];
                
            } else {
                delm = listB[4] - listA[4] + 60 * delH;
            }

            if (delm == 0) {
                dels = listB[5] - listA[5];
                
            } else {
                dels = listB[5] - listA[5] + 60 * delm;
            }

            // Compute the time-distance comparator
            weight =  computeRelativeDiff(dels, index, mVar);
        }

        return weight;
    }
    

    /*
     *
     */
    private static double computeRelativeDiff(int delX, int index, MatchVariables mVar) {
        
        double compV;
        double abDelX;
        
        // Case when the reference date is ahead of the candidate date
        if (delX >= 0) {
            
            if (delX <= mVar.param2[index]) {
                compV = 1.0 - ((double) delX) / (mVar.param2[index] + 1);
            } else {
                compV = 0.0;
            }
        // Case when the candidate date is ahead of the reference date
        } else {
            
            abDelX = Math.abs(delX);
            
            if (abDelX <= mVar.param1[index]) {
                compV = 1.0 - (abDelX) / (mVar.param1[index] + 1);
            } else {
                compV = 0.0;
            }
        }
        
        return compV;
    }
}
