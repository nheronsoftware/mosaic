/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.stand.address;

import com.stc.sbme.api.SbmeStandardizationException;
import com.stc.sbme.stand.util.TableSearch;

/**
 * Loads the address clue words table and assign the data to the
 * corresponding variables
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
public class AddressStateCode
    implements AddressProperties {
    

    /**
     * Define a factory method that return an instance of the class 
     * @return a AddressStateCode instance
     */
    public static AddressStateCode getInstance() {
        return new AddressStateCode();
    }

    /** 
     * Performs the parsing of the input string 
     * 
     * @param str the string 
     * @param domain the geo-location
     * @param sVar the basic instance that provide access to any other used instances 
     * @throws SbmeStandardizationException an exception 
     * @return a StringBuffer
     */ 
    public StringBuffer findStateCode(String str, String domain, AddressStandVariables sVar) 
        throws SbmeStandardizationException {
        
        /* Search for integers that are zipcodes */
        return extractStateCode(str, domain, sVar);
    }
        
    /** 
     * 
     * 
     * @param strg the string where we search for the zip code
     * @param domain the geo-location
     * @param sVar the 
     * @throws SbmeStandardizationException an exception 
     * @return a StringBuffer
     */ 
    private StringBuffer extractStateCode(String strg, String domain, AddressStandVariables sVar)
        throws SbmeStandardizationException {
        
        int i;
        int j;
        int k;
        int numTok;
        int index;
        
        StringBuffer[] state = new StringBuffer[5];
        
        int strLength = strg.length();
        
        StringBuffer str = new StringBuffer(strg);
        
        /* Number of numeric tokens */
        numTok = 0;
        
        /* search for tokens that have some digits inside then. */
        for (i = 0; i < strLength; i++) {
            
            if (Character.isWhitespace(str.charAt(i))
                && Character.isLetter(str.charAt(i + 1))) {
                
                for (j = i + 2; j < strLength; j++) {
                
                    if (Character.isWhitespace(str.charAt(j))) {
                    
                        state[numTok] = new StringBuffer();
                        state[numTok].append(str.substring(i, j));
                        
                        numTok++;
                        i = j + 1;
                        break;
                        
                    } else if (Character.isLetter(str.charAt(j))) {
                        continue;
                    } else {
                        i = j + 1;
                        break;
                    }
                }
            }
        }
        
        /* Test if the token is a zipcode. Analyze if it is a 5-digit or more */
        for (k = numTok - 1; k >= 0; k--) {

            state[numTok].toString().toUpperCase();
        
            if (state[numTok].length() == 2) {
           
                if ((index = TableSearch.searchTable(state[numTok].toString(), 1, sVar.nbState,
                                                     sVar.stateAb)) >= 0) {
                    sVar.stateFlag = true;
                }
                
            } else {
            
                if ((index = TableSearch.searchTable(state[numTok].toString(), 1, sVar.nbState, 
                                                     sVar.stateFull)) >= 0) {
                    sVar.stateFlag = true;
                    state[numTok] = sVar.stateAb[index];
                }
            }

            /* return the short state code */
            return state[numTok];
        }
        
        return new StringBuffer("     ");
    }
}
