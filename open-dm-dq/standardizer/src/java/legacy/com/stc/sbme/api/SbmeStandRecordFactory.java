/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.api;

/**
 * A factory class that creates an instance of the the standardization
 * class passed as an argument
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
public class SbmeStandRecordFactory {
    
    /**
     * Create an instance of the class in the argument. Handle any exception
     *
     * @param className name of the standardization class
     * @return an instance of the standardization class passed as an argument
     * @throws IllegalArgumentException the generic stand exception class
     */
    public static SbmeStandRecord getInstance(String className)
    throws IllegalArgumentException {
        
        /* Check for case when the class is SbmePersonName */
        if (className.equals("PersonName")) {
            
            /* Create and return an instance of the class SbmePersonName */
            return new SbmePersonName();
            
        } else if (className.equals("BusinessName")) {
            
            /* Create and return an instance of the class */
            return new SbmeBusinessName();
            
        } else if (className.equals("Address")) {
            
            /* Create and return an instance of the class BusinessName */
            return new SbmeAddress();
            
        } else {
            
            // Handle exceptions
            String message = "The class' name is not recognized";
            throw new IllegalArgumentException(message);
        }
    }
}
