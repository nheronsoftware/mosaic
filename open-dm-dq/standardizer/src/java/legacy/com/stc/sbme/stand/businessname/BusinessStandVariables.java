/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.stand.businessname;

import java.util.ArrayList;
import java.util.HashMap;

import com.stc.sbme.stand.util.ReadStandConstantsValues;

/**
 * Defines all the 'global' variables used in the standardization code.
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
public class BusinessStandVariables {
    
    /* define all the shared variables inside the different classes */
    /*      */
    private final int WORDS;    
    /*      */
    private final int CITY_MAX;
    /*      */
    private final int PRIMARY_MAX;
    /*      */
    private final int COUNTRY_MAX;
    /*      */
    private final int INDUSTRY_MAX;
    /*      */
    private final int PATTERN_MAX;
    /*      */
    private final int MERGER_MAX;
    /*      */
    private final int ADJECTIVE_MAX;
    /*      */
    private final int ORG_MAX;
    /*      */
    private final int ASSOC_MAX;
    /*      */
    private final int GEN_MAX;
    /*      */
    private final int CHARS_MAX;

    
    /**
     * Default constructor
     */
    public BusinessStandVariables(ReadStandConstantsValues vals) {
        WORDS = vals.getBizMaxWords();    
        CITY_MAX = vals.getCityMax();
        PRIMARY_MAX = vals.getPrimaryMax();
        COUNTRY_MAX = vals.getCountryMax();
        INDUSTRY_MAX = vals.getIndustryMax();
        PATTERN_MAX = vals.getPatternMax();
        MERGER_MAX = vals.getMergerMax();
        ADJECTIVE_MAX = vals.getAdjectiveMax();
        ORG_MAX = vals.getOrgMax();
        ASSOC_MAX = vals.getAssocMax();
        GEN_MAX = vals.getGenTermMax();
        CHARS_MAX = vals.getCharsMax();
        orgTypeKeyFlag = new boolean[WORDS];
        assocTypeKeyFlag = new boolean[WORDS];
        locationTypeKeyFlag = new boolean[WORDS];
        connectorTokensFlag = new boolean[WORDS];
        industryTypeKeyFlag = new boolean[WORDS];
        unfoundTypeKeyFlag = new boolean[WORDS];
        nationalityTypeKeyFlag = new boolean[WORDS];
        countryTypeKeyFlag = new boolean[WORDS];
        cityTypeKeyFlag = new boolean[WORDS];
        aliasTypeKeyFlag = new boolean[WORDS];
        urlTypeFlag = new boolean[WORDS];
        commaFlag = new boolean[WORDS];
        separatorCharKeyFlag = new boolean[WORDS];
        mergerIndexFlag = new boolean[WORDS];
        glueCharKeyFlag = new boolean[WORDS];
        ampersandTypeFlag = new boolean[WORDS];
        numberOfTypes = new int[WORDS];
        specChars1 = new String[CHARS_MAX];
        specChars2 = new String[CHARS_MAX];
        specChars = new String[100];
        delimiters = " \t\n\r\f";
        orgTypeField = new String[ORG_MAX];
        orgTypeStandField = new String[ORG_MAX];
        assocTypeField = new String[ASSOC_MAX];
        assocTypeStandField = new String[ASSOC_MAX];
        industryTypeField = new ArrayList();
        industryTypeStandField = new String[INDUSTRY_MAX];
        industrySector = new StringBuffer[INDUSTRY_MAX];    
        adjTypeField = new String[ADJECTIVE_MAX];
        adjTypeStandField = new String[ADJECTIVE_MAX];
        aliasTypeField = new ArrayList();
        aliasTypeStandField = new ArrayList();
        primaryName = new String[PRIMARY_MAX];
        industrySector1 = new StringBuffer[PRIMARY_MAX];    
        primarySlashName = new StringBuffer[MERGER_MAX];
        industrySector2 = new StringBuffer[MERGER_MAX];    
        categoryCode = new ArrayList();
        categoryName = new ArrayList();
        industrySectorList = new ArrayList();
        aliasList = new ArrayList();
        bizCommonTerm = new ArrayList();
        countryStandName = new ArrayList();
        countryCode = new String[COUNTRY_MAX];
        nationality = new ArrayList();
        companyFormerName = new ArrayList();
        companyNewName = new ArrayList();
        cityOrStateName = new ArrayList();
        cityOrStateIdentifier = new String[CITY_MAX];
        cityOrStateCode = new String[CITY_MAX];
        connectorToken = new String[GEN_MAX];
        inputPattern = new String[PATTERN_MAX];
        outputPattern = new String[PATTERN_MAX];
        adjTypeKeyFlag = new boolean[WORDS];
        dashFlag = new boolean[WORDS];
        slashFlag = new boolean[WORDS];        
    }

    /**
     * Default constructor
     */
    /*
    public BusinessStandVariables(boolean bl) {
        super();
        
        groupList = bl;
    }
      */  
    
    /**      */
    public boolean primaryNameFlag;
    /**  Control the size of AliasList and SectorList. False by default to false */
    public boolean groupList;    
    /**      */
    public boolean formerNameFlag;
    /**      */
    public boolean incompAliasFlag;
    /**      */
    public boolean[] orgTypeKeyFlag;
    /**      */
    public boolean[] assocTypeKeyFlag;
    /**      */
    public boolean[] locationTypeKeyFlag;
    /**      */
    public boolean[] connectorTokensFlag;
    /**      */
    public boolean[] industryTypeKeyFlag;
    /**      */
    public boolean[] unfoundTypeKeyFlag;
    /**      */
    public boolean[] nationalityTypeKeyFlag;
    /**      */
    public boolean[] countryTypeKeyFlag;
    /**      */
    public boolean[] cityTypeKeyFlag;

    /**      */
    public boolean[] aliasTypeKeyFlag;
    /**      */
    public boolean[] urlTypeFlag;
    /**      */
    public boolean[] commaFlag;
    /**      */
    public boolean[] separatorCharKeyFlag;
    /**      */
    public boolean[] mergerIndexFlag;
    /**      */
    public boolean[] glueCharKeyFlag;
    /**      */
    public boolean[] ampersandTypeFlag;

    /**      */
    public int[] numberOfTypes;
 
    /**      */
    public String[] specChars1;
    /**      */
    public String[] specChars2;
    /**      */
    public String[] specChars;
    
    /**      */
    public String delimiters;
    
    /**      */
    public boolean indSectorF;
    /**      */
    public boolean aliasListF;
    /**      */
    public int intV;
    /**      */
    public int numberOfAliasesInList;
    /**      */
    public int numberOfSectorInList;
    /**      */
    public int nbOrgTypeFields;
    /**      */
    public int nbAssocTypeFields;
    /**      */
    public int nbIndustryTypeFields;
    /**      */
    public int nbAdjTypeFields;
    /**      */
    public int nbCountryCodes;
    /**      */
    public int nbCityStateCodes;
    /**      */
    public int nbAliasTypes;
    /**      */
    public int nbPrimarySlashNames;
    /**      */
    public int nbPrimaryNames;
    /**      */
    public int nbPatterns;
    /**      */
    public int nbCategoryCodes;
    /**      */
    public int nbBizCommonTerms;
    /**      */
    public int nbCompanyFormerNames;
    /**      */
    public int nbConnectorTokens;
    /**      */
    public int nbSpecChars;
    

    /**      */
    public String[] orgTypeField;
    /**      */
    public String[] orgTypeStandField;
    /**      */
    public String[] assocTypeField;
    /**      */
    public String[] assocTypeStandField;
    /**      */
    public ArrayList industryTypeField;
    /**      */
    public String[] industryTypeStandField;
    /**      */
    public StringBuffer[] industrySector;    
    /**      */
//    public StringBuffer[] industryCategoriesCode = new StringBuffer[BIZDEX];
    /**      */
    public String[] adjTypeField;
    /**      */
    public String[] adjTypeStandField;
    /**      */
    public ArrayList aliasTypeField;
    /**      */
    public ArrayList aliasTypeStandField;
    /**      */
    public String[] primaryName;
    /**      */
    public StringBuffer[] industrySector1;    
    /**      */
    public StringBuffer[] primarySlashName;
    /**      */
    public StringBuffer[] industrySector2;    
    /**      */
    public ArrayList categoryCode;
    /**      */
    public ArrayList categoryName;
    /**      */
    public ArrayList industrySectorList;
    /**      */
    public ArrayList aliasList;
    /**      */
    public HashMap[] aliasListType;
    /**      */
    public ArrayList bizCommonTerm;
    /**      */
    public ArrayList countryStandName;
    /**      */
    public String[] countryCode;
    /**      */
    public ArrayList nationality;
    /**      */
    public ArrayList companyFormerName;
    /**      */
    public ArrayList companyNewName;
    /**      */
    public ArrayList cityOrStateName;
    /**      */
    public String[] cityOrStateIdentifier;
    /**      */
    public String[] cityOrStateCode;
    /**      */
    public String[] connectorToken;
    /**      */
    public String formerName;

    /**      */
    public String[] inputPattern;
    /**      */
    public String[] outputPattern;
    /**      */
    public boolean[] adjTypeKeyFlag;
    /**      */
    public boolean[] dashFlag;
    /**      */
    public boolean[] slashFlag;
    
}
