/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.stand.address;

import com.stc.sbme.api.SbmeStandardizationException;
import com.stc.sbme.util.EmeUtil;
import com.stc.sbme.util.SbmeLogUtil;
import com.stc.sbme.util.SbmeLogger;

/**
 * Loads all the person name tables and assign the data to the 
 * corresponding variables
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
class LegitTokenExists {

    private String[] tokenStringArray;
    private final StandTokensRead standTokensRead;
    
    private static final SbmeLogger LOGGER = SbmeLogUtil.getLogger("com.stc.sbme.stand.address.LegitTokenExists");

    LegitTokenExists(String domain) {
        standTokensRead = new StandTokensRead(domain);
    }
    
    /**
     *
     *
     * @param usage aaa
     * @param clue aaa
     * @param clueArray aaa
     * @return a boolean
     * @throws SbmeStandardizationException an exception
     */
    protected boolean legitTokenExists(char usage, ClueTable clue, int[] clueArray) 
        throws SbmeStandardizationException {
    
        int i;
        int count = 0;
        boolean match = false;

        for (i = 0; i < 5; i++) {
            clueArray[i] = 0;
        }

        if (EmeUtil.strncmp(clue.getClueType1(), "  ", 2) == 0) {
            clueArray[0] = 1;
        }

        if (EmeUtil.strncmp(clue.getClueType2(), "  ", 2) == 0) {
            clueArray[1] = 1;
        }
        
        if (EmeUtil.strncmp(clue.getClueType3(), "  ", 2) == 0) {
             clueArray[2] = 1;
        }
        
        if (EmeUtil.strncmp(clue.getClueType4(), "  ", 2) == 0) {
            clueArray[3] = 1;
        }
        
        if (EmeUtil.strncmp(clue.getClueType5(), "  ", 2) == 0) {
            clueArray[4] = 1;
        }


        // Check to see if all clue_type are blanks, if true, abort
        for (i = 0; i < 5; i++) {
            count += clueArray[i];
        }

        if (count == 5) {
            LOGGER.error("Error! All clue_type_# are blanks.Aborted.");
            throw new SbmeStandardizationException("Error! All clue_type_# are blanks.Aborted.");
        }

        if (usage == ' ') {
            tokenStringArray = standTokensRead.getLegalTokensBlank();
        }
        
        if (usage == 'B') {
            tokenStringArray = standTokensRead.getLegalTokensB();
        }
        
        if (usage == 'T') {
            tokenStringArray = standTokensRead.getLegalTokensT();
        }
        
        if (usage == 'S') {
            tokenStringArray = standTokensRead.getLegalTokensS();
        }
        
        if (usage == 'W') {
            tokenStringArray = standTokensRead.getLegalTokensW();
        }
        
        if (usage == 'N') {
            tokenStringArray = standTokensRead.getLegalTokensN();
        }
        
        if (usage == 'R') {
             tokenStringArray = standTokensRead.getLegalTokensR();
        }

        if ((EmeUtil.strncmp(clue.getClueType1(), "  ", 2) != 0)
            && TokenFound.tokenFound(clue.getClueType1(), tokenStringArray)) {
             match = true;
             clueArray[0] = 1;
        }

        if ((EmeUtil.strncmp(clue.getClueType2(), "  ", 2) != 0)
            && TokenFound.tokenFound(clue.getClueType2(), tokenStringArray)) {
             match = true;
             clueArray[1] = 1;
        }

        if ((EmeUtil.strncmp(clue.getClueType3(), "  ", 2) != 0)
            && TokenFound.tokenFound(clue.getClueType3(), tokenStringArray)) {
             match = true;
             clueArray[2] = 1;
        }

        if ((EmeUtil.strncmp(clue.getClueType4(), "  ", 2) != 0)
            && TokenFound.tokenFound(clue.getClueType4(), tokenStringArray)) {
             match = true;
             clueArray[3] = 1;
        }

        if ((EmeUtil.strncmp(clue.getClueType5(), "  ", 2) != 0)
            && TokenFound.tokenFound(clue.getClueType5(), tokenStringArray)) {
             match = true;
             clueArray[4] = 1;
        }
        return match;
    }
}
