/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.stand.address;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.StringTokenizer;

import com.stc.sbme.api.SbmeMatchEngineException;
import com.stc.sbme.api.SbmeStandardizationException;
import com.stc.sbme.stand.util.ReadStandConstantsValues;
import com.stc.sbme.util.EmeUtil;
import com.stc.sbme.util.SbmeLogUtil;
import com.stc.sbme.util.SbmeLogger;

/**
 * Loads the address clue words table and assign the data to the
 * corresponding variables
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
class AddressPatternsTable {

    private final int PATTERN_ARRAYSIZE;
    private final int PATTERN_RECLEN;
    private final int PATTERN_HASHSIZE;
    private final int PATSIZE;

    private final SbmeLogger mLogger = SbmeLogUtil.getLogger(this);  
    private final boolean mDebug = mLogger.isDebugEnabled();

    private final PatternTable[] patterns;
    private OutputTokenMap outtok1;
    private final static HashMap INSTANCES = new HashMap();
    private boolean loadedOnce = false;
    
    /**
     * Inner class for outputToken hash map
     */
//    private static class OutputTokenMap {
    private class OutputTokenMap {        
        HashMap hm = new HashMap();
        
        String get(String inputPattern) {
            String strippedPat = stripMultipleTokens(inputPattern);
            String outputPat = (String) hm.get(strippedPat);
            if (outputPat != null && strippedPat.length() != inputPattern.length()) {
                outputPat = fillMultipleTokens(strippedPat, inputPattern, outputPat);
            }
            return outputPat;
        }
        
        boolean containsPattern(String pattern) {
            return hm.containsKey(stripMultipleTokens(pattern));
        }
        
        void put(String origPattern, String modPattern) {
            hm.put(origPattern, modPattern);
        }
        
        private String fillMultipleTokens(String strippedInputPattern, 
        String fullInputPattern, String strippedOutputPattern) {
            //Traverse the stripped and full input pattern.  If there is a 
            //token mismatch, then do not increment index of the stripped input 
            //pattern and reuse the previous output token.
            ArrayList strippedInputTokens = tokenizePattern(strippedInputPattern);
            ArrayList fullInputTokens = tokenizePattern(fullInputPattern);
            ArrayList strippedOutputTokens = tokenizePattern(strippedOutputPattern);
            ArrayList fullOutputTokens = new ArrayList();
            int strippedIndex = 0;
            for (int fullIndex=0; fullIndex < fullInputTokens.size(); fullIndex++) {
                if (strippedInputTokens.get(strippedIndex).equals(fullInputTokens.get(fullIndex))) {
                    if (strippedIndex < strippedOutputTokens.size()) {
                        fullOutputTokens.add(strippedOutputTokens.get(strippedIndex));
                        strippedIndex++;
                    } else {
                        break;
                    }
                } else {
                    fullOutputTokens.add(strippedOutputTokens.get(strippedIndex - 1));
                }                
            }
            return constructPattern(fullOutputTokens);
        }
        
        private String constructPattern(ArrayList tokens) {
            StringBuffer retVal = new StringBuffer();
            Iterator i = tokens.iterator();
            boolean first = true;
            while (i.hasNext()) {
                String st = (String) i.next();
                if (st.equals("|")) {
                    first = true;
                } else if (!first) {
                    retVal.append(' ');
                } else {
                    first = false;
                }              
                retVal.append(st);
            }
            return retVal.toString();
        }
        
        private ArrayList tokenizePattern(String patterns) {
            ArrayList tokens = new ArrayList();
            StringTokenizer st = new StringTokenizer(patterns, "|");
            boolean firstPattern = true;
            while (st.hasMoreElements()) {
                if (!firstPattern) {
                    tokens.add("|");
                } else {
                    firstPattern = false;
                }
                String pattern = st.nextToken();
                StringTokenizer st2 = new StringTokenizer(pattern, " ");
                while (st2.hasMoreElements()) {
                    String token = st2.nextToken();
                    tokens.add(token);
                }
            }            
            return tokens;
        }
        
        private String stripMultipleTokens(String origPattern) {
            StringTokenizer st = new StringTokenizer(origPattern, "|");
            StringBuffer retPat = new StringBuffer();
            boolean firstPattern = true;
            while (st.hasMoreElements()) {
                String lastToken = null;
                if (!firstPattern) {
                    retPat.append('|');
                } else {
                    firstPattern = false;
                }
                String pattern = st.nextToken();
                StringTokenizer st2 = new StringTokenizer(pattern, " ");
                while (st2.hasMoreElements()) {
                    String token = st2.nextToken();
                    if (lastToken == null) {
                        retPat.append(token);
                    } else if (!token.equals(lastToken)) {
                        retPat.append(' ').append(token);
                    } 
                    lastToken = token;
                }
            }
            return retPat.toString();
        }
    }
    
    static synchronized AddressPatternsTable getInstance(String domain) {
        AddressPatternsTable table = (AddressPatternsTable) INSTANCES.get(domain);
        if (table == null) {
            table = new AddressPatternsTable(domain);
            INSTANCES.put(domain, table);
        }
        return table;
    }
    
    
    /**
     * the default constructor
     */
    private AddressPatternsTable(String domain) {
        PATTERN_ARRAYSIZE = ReadStandConstantsValues.getInstance(domain).getPatternArraySize();
        PATTERN_RECLEN = ReadStandConstantsValues.getInstance(domain).getPatternReclen();
        PATTERN_HASHSIZE = ReadStandConstantsValues.getInstance(domain).getPatternHashSize();
        PATSIZE = ReadStandConstantsValues.getInstance(domain).getPatSize();
        patterns = new PatternTable[PATTERN_ARRAYSIZE];
    }

    /**
     * Loads the data from the clue woeds table into the corresponding variables
     *
     * @param stream the pattern table stream
     * @param sVar an instance of the AddressVariables class
     * @throws SbmeStandardizationException an exception
     * @throws SbmeMatchEngineException a generic match exception
     * @throws IOException an exception
     * @return a boolean 
     */
    synchronized boolean loadPatterns(InputStream stream, InputStream stream2, AddressStandVariables sVar, String domain)
        throws SbmeStandardizationException, SbmeMatchEngineException, IOException {

        if (loadedOnce == true) {
            return true;
        }        

        StringBuffer intok = new StringBuffer(36);
        StringBuffer outtok = new StringBuffer(36);
        StringBuffer pattyp = new StringBuffer(3);
        StringBuffer pri = new StringBuffer(3);
        StringBuffer st = new StringBuffer(3);
        StringBuffer cou = new StringBuffer(4);
        StringBuffer plc = new StringBuffer(5);
        StringBuffer zip = new StringBuffer(6);
        
        String temp1 = "";       
        String temp2 = "";      
                
        char lang;
        char usf;
        char excl;

        StringBuffer srec = new StringBuffer(PATTERN_RECLEN);
        StringBuffer srec1 = new StringBuffer(PATTERN_RECLEN);
        
        int cnt = 0;
        int i;
        int len;

        PatternTable pat;

        for (i = 0; i < PATTERN_ARRAYSIZE; i++) { 
            patterns[i] = null;
        }
  
        BufferedReader brFp = null;
        
        if (mDebug) {
            mLogger.debug("Reading the addressPatternsTable file");
        }

        /* Test if the clue words table exists */
        if (stream != null) {
            brFp = new BufferedReader(new InputStreamReader(stream));
            
        } else {
            mLogger.fatal("The addressPatternsTable file is not found");
            return false;
        }
 

       /*
        * Load the table, processing 3 records at a time:
        *   record 1:  input pattern
        *     "    2:  output pattern, pattern type, priority, etc.
        *     "    3:  blank line
        */
        while (EmeUtil.fgets(brFp, srec, PATTERN_RECLEN) != Boolean.FALSE) {

            // Trim on the left side
            EmeUtil.trimOnLeft(srec);
            len = srec.length();


            // Handle cases where the line is empty
            if (len == 0) {
                continue;
            }

            if (len > 35) {
                EmeUtil.strcpy(intok, srec.substring(1, 36));
            }

            // Retrive the translation record
            if (EmeUtil.fgets(brFp, srec, PATTERN_RECLEN) == Boolean.FALSE) {  
                mLogger.error("No translation record for " + intok);
                throw new SbmeStandardizationException("No translation record for " + intok);
            }
            
            // Trim on the left side
            len = srec.length();


            /* load the variables */
            if (len > 35) {
                EmeUtil.strcpy(outtok, srec.substring(1, 36));
            }
            
            if (len > 38) {
                EmeUtil.strcpy(pattyp, srec.substring(37, 39));    
            }
            
            if (len > 41) {
                EmeUtil.strcpy(pri, srec.substring(40, 42).trim());
            }

            if (len > 43) {
                EmeUtil.strcpy(st, srec.substring(42, 44));
            } else {
                EmeUtil.strcpy(st, "  ");
            }
            
            if (len > 46) {
                EmeUtil.strcpy(cou, srec.substring(44, 47));
            } else {
                EmeUtil.strcpy(cou, "   ");
            }
            
            if (len > 50) {
                EmeUtil.strcpy(plc, srec.substring(47, 51));
            } else {
                EmeUtil.strcpy(plc, "    ");
            }
            
            if (len > 55) {
                EmeUtil.strcpy(zip, srec.substring(51, 56));
            } else {
                EmeUtil.strcpy(zip, "     ");
            }

            if (len > 56) {
                lang = srec.charAt(56);
            } else {
                lang = ' ';
            }

            if (len > 57) {
                usf = srec.charAt(57);
            } else {
                usf = ' ';
            }

            if (len > 58) {
                excl = srec.charAt(58);
            } else {
                excl = ' ';
            }

            pat = addPattern(intok, outtok, pattyp, pri, st, cou, plc, zip, lang,
                              usf, excl, domain);

            if (EmeUtil.fgets(brFp, srec, PATTERN_RECLEN) == Boolean.FALSE) {
                break;
            }
            cnt++;


            if (cnt > PATTERN_ARRAYSIZE) {

        mLogger.error("ERROR:  trying to load too many patterns.");
        mLogger.error("        Max number that can be loaded: " + PATTERN_ARRAYSIZE);
                    
                return false;
            }
        }
        brFp.close();
        

        srec.setLength(0);
        outtok1 = new OutputTokenMap();
        brFp = null;
        
        if (mDebug) {
            mLogger.debug("Reading the addressOutpatterns file");
        }

        /* Test if the clue words table exists */
        if (stream2 != null) {
            brFp = new BufferedReader(new InputStreamReader(stream2));
            
        } else {
            mLogger.fatal("The addressPatternsTable file is not found");
            return false;
        }
                
        while (EmeUtil.fgets(brFp, srec, PATTERN_RECLEN) != Boolean.FALSE) {

            // Trim on the left side
            len = srec.toString().trim().length();
            
            if (len > 40) {
                temp1 = srec.substring(0, 40).trim();

            } else if (len > 2) {
                temp1 = srec.toString().trim();
            }

            // Handle cases where the line 1 is empty
            if (len == 0) {
                continue;
            }       

            // Retrive the translation record
            if (EmeUtil.fgets(brFp, srec1, PATTERN_RECLEN) == Boolean.FALSE) {  
                mLogger.error("No translation record for " + intok);
                throw new SbmeStandardizationException("No translation record for " + intok);
            } 
            
            temp2 = srec1.toString().trim();

            len = temp2.length();  
         
            // Handle cases where the line 2 is empty
            if (len == 0) {
                continue;
            }                                      

            //if (!outtok1.containsPattern(temp1)) {
                outtok1.put(temp1, temp2);
            //}            
        }
        brFp.close();
        // Set the load once to true to prevent multiple threads from reloading
        loadedOnce = true;        
        return true;
    }


    /*
     * 
     * @param str the input string
     * @return an integer
     */
    private int patternHash(StringBuffer str) {

        int val;
        int len;
        int i;
        int sLen;
        char cStr;

        len = AddressClueWordsTable.trimln(str);
        sLen = str.length();
        cStr = str.charAt(0);

        for (val = 0, i = 0; (i < sLen) && (i < len); i++) {
            val = str.charAt(i) + (31 * val);
        }

        if (val < 0) {
            val = EmeUtil.unsignedIntRemainder(val, PATTERN_HASHSIZE);
        }
        return (val % PATTERN_HASHSIZE);
    }


    /**
     * 
     *
     * @param str the input string
     * @param state the state code
     * @param zip the zip code
     * @param usf aaa
     * @return a PatternRegistry object
     */
    PatternTable searchForPattern(StringBuffer str, StringBuffer state,
                                         StringBuffer zip, char usf) {

        PatternTable temp;
        PatternTable hold;
        
        int ln1;
        int ln2;
        int i;

        // Walk along the linked list
        for (hold = null, temp = patterns[patternHash(str)]; temp != null;
             temp = temp.getNext()) {

            ln1 = AddressClueWordsTable.trimln(str);
            ln2 = AddressClueWordsTable.trimln(temp.getInputTokenString());

            if ((EmeUtil.strncmp(str, temp.getInputTokenString(), ln1) == 0)
                && (ln1 == ln2)) {

                if ((EmeUtil.strncmp(temp.getState(), "  ", 2) != 0)
                    && (EmeUtil.strncmp(temp.getState(), state, 2) != 0)) {

                    continue;
                }

                if ((EmeUtil.strncmp(temp.getZipCode(), "  ", 2) != 0)
                    && (EmeUtil.strncmp(temp.getZipCode(), zip, 2) != 0)) {

                    continue;
                }

               /* 
                * Save the pattern where usage flag is blank, unless the exclude
                * flag is set. If the incoming usage flag is blank, then you can
                * match to anything. UPDATE:  if the input usage flag is 'S',
                * then you can match to 'T', 'TX', or 'B', but NOT 'BX'.
                */
                if (usf == ' ') {

                    if (temp.getExclude() != 'X') {

                        if (temp.getUsf() == ' ') {
                            return temp;
                        }
                        hold = temp;
                    }
                    
                } else if (usf == 'S') {

                    if ((temp.getUsf() == 'T')
                        || ((temp.getUsf() == 'B') && (temp.getExclude() != 'X'))) {

                        return temp;
                    }
                    
                } else if (usf == temp.getUsf()) {
                    return temp;
                }
            }
        }

        // If you couldnt find matching usage flags, then return NULL
        return  hold;
    }

    /**
     * 
     *
     * @param str the input string
     * @return a PatternRegistry object
     */
    String searchForAlterPattern(String str) {
        
        // Search if the 'str' pattern exist in the addressOutPattern
        String retVal = outtok1.get(str);
        if (retVal != null) {
            return retVal;
        }
       
        //If nothing found then replace any duplicate tokens with EI
        //Begin by splitting the output pattern into lines.  Populate the
        //token-used array for all token types used by line 1.  Move on 
        //line 2, and substitute EI for any token already used by line 1.
        //Update the token-used array, and then move on to line 3, etc...
        StringTokenizer st = new StringTokenizer(str, "|");
        HashSet usedTokens = new HashSet();
        StringBuffer retPat = new StringBuffer();
        boolean firstPattern = true;
        while (st.hasMoreElements()) {
            if (!firstPattern) {
                retPat.append('|');
            } else {
                firstPattern = false;
            }
            String pattern = st.nextToken();
            StringTokenizer st2 = new StringTokenizer(pattern, " ");
            LinkedList tokensSeen = new LinkedList();
            boolean firstToken = true;
            while (st2.hasMoreElements()) {
                String token = st2.nextToken();
                tokensSeen.add(token);
                if (!firstToken) {
                    retPat.append(' ');
                } else {
                    firstToken = false;
                }
                if (usedTokens.contains(token)) {
                    retPat.append("EI");
                } else {
                    retPat.append(token);
                }
            }
            usedTokens.addAll(tokensSeen);
        }
        
        return retPat.toString();
    }

    /* */
    private PatternTable addPattern(StringBuffer intok, StringBuffer outtok,
        StringBuffer pattyp, StringBuffer pri, StringBuffer st, StringBuffer cou,
            StringBuffer plc, StringBuffer zip, char lang, char usf, char excl, String domain) {

        PatternTable find;
        int val;

        PatternTable temp = new PatternTable(domain);

        if (temp == null) {
            return null;
        }

        temp.setInputTokenString(intok.toString());
        temp.setOutputTokenString(outtok.toString());

        EmeUtil.strncpy(temp.getPatternType(), pattyp, 2);
        EmeUtil.strncpy(temp.getPriority(), pri, 2);
        EmeUtil.strncpy(temp.getState(), st, 2);
        EmeUtil.strncpy(temp.getCounty(), cou, 3);
        EmeUtil.strncpy(temp.getPlace(), plc, 4);
        EmeUtil.strncpy(temp.getZipCode(), zip, 5);

        temp.setLanguage(lang);
        temp.setUsf(usf);
        temp.setExclude(excl);

        // SET THE NEXT ENTRY
        temp.setNext(null);   

        val = patternHash(intok);

        if (patterns[val] == null) {
            patterns[val] = temp;
            
        } else {

            for (find = patterns[val]; find.getNext() != null; find = find.getNext()) {
                continue;
            }
            find.setNext(temp);
        }
        return temp;
    }
}
