/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.stand.address;

/**
 * This class reads all the configured maximum values used in the matching code
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
class Master {

    // clue word id
    private StringBuffer clueWordId;
    // full name
    private StringBuffer fullName;
    // standard abbreviation
    private StringBuffer stdAbbrev;
    // short abbreviation
    private StringBuffer shortAbbrev;
    // USPS abbreviation
    private StringBuffer uspsAbbrev;
    // CFCCs allowed (A,B,C,D,E,F,G,H)
    private StringBuffer validCfccs;
    // clue type
    private StringBuffer clueType;
    private char usageFlag;
    private char uspsFlag;
    // foreign language flag
    private char forLangFlag;
    private StringBuffer translation;

    /**
     * the default constructor
     */
    Master() {
    
       clueWordId = new StringBuffer(5);
       fullName = new StringBuffer(25);
       stdAbbrev = new StringBuffer(15);
       shortAbbrev = new StringBuffer(6);
       uspsAbbrev = new StringBuffer(5);
       validCfccs = new StringBuffer(9);
       clueType = new StringBuffer(3);
       translation = new StringBuffer(25);
    }


    /**
     * 
     * @return a clueWordId
     */
    StringBuffer getClueWordId() {
        return clueWordId;
    }

    /**
     * 
     * @return a full name
     */
    StringBuffer getFullName() {
        return fullName;
    }

    /**
     * 
     * @return a standard abbreviation
     */
    StringBuffer getStdAbbrev() {
        return stdAbbrev;
    }

    /**
     * 
     * @return a short abbrev
     */
    StringBuffer getShortAbbrev() {
        return shortAbbrev;
    }

    /**
     * 
     * @return a usps abbrev
     */
    StringBuffer getUspsAbbrev() {
        return uspsAbbrev;
    }

    /**
     * 
     * @return a valid Cfccs
     */
    StringBuffer getValidCfccs() {
        return validCfccs;
    }

    /**
     * 
     * @return a clue type
     */
    StringBuffer getClueType() {
        return clueType;
    }

    /**
     * 
     * @return a usage flag
     */
    char getUsageFlag() {
        return usageFlag;
    }

    /**
     * 
     * @return a usps flag
     */
    char getUspsFlag() {
        return uspsFlag;
    }

    /**
     * 
     * @return a 
     */
    char getForLangFlag() {
        return forLangFlag;
    }

    /**
     * 
     * @return a translation
     */
    StringBuffer getTranslation() {
        return translation;
    }



    /**
     *
     * @param sb a clueWord Id
     */ 
    void setClueWordId(StringBuffer sb) {
        this.clueWordId = sb;
    }

    /**
     *
     * @param sb a full name
     */ 
    void setFullName(StringBuffer sb) {
        this.fullName = sb;
    }

    /**
     *
     * @param sb a standard abbreviation
     */ 
    void setStdAbbrev(StringBuffer sb) {
        this.stdAbbrev = sb;
    }

    /**
     *
     * @param sb a short abbrev
     */ 
    void setShortAbbrev(StringBuffer sb) {
        this.shortAbbrev = sb;
    }

    /**
     *
     * @param sb a usps abbrev
     */ 
    void setUspsAbbrev(StringBuffer sb) {
        this.uspsAbbrev = sb;
    }

    /**
     *
     * @param sb a valid Cfccs
     */ 
    void setValidCfccs(StringBuffer sb) {
        this.validCfccs = sb;
    }

    /**
     *
     * @param sb a cle type
     */ 
    void setClueType(StringBuffer sb) {
        this.clueType = sb;
    }

    /**
     *
     * @param sb a 
     */ 
    void setUsageFlag(char sb) {
        this.usageFlag = sb;
    }

    /**
     *
     * @param sb a 
     */ 
    void setUspsFlag(char sb) {
        this.uspsFlag = sb;
    }

    /**
     *
     * @param charac aaa
     */ 
    void setForLangFlag(char charac) {
        this.forLangFlag = charac;
    }

    /**
     *
     * @param sb a 
     */ 
    void setTranslation(StringBuffer sb) {
        this.translation = sb;
    }
}
