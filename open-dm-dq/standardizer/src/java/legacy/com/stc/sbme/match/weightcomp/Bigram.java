/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.match.weightcomp;

/**
 * This code computes the ratio of the number of bigrams in common
 * in two different strings to the average length of the strings.
 * It is then adjusted to be conform to a similar value in a standard
 * string comparators.
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
public class Bigram {
    
    private static final char BASIC = '1';
    private static final char SWAP = '2';
    
    /**
     * Reads two strings and compute how similar they are relying on an algorithm
     * that compare the bigrams of the two strings.
     *
     * @param      recordA   Candidate's string record.
     * @param      recordB   Reference's string record.
     * @param      method    If equal to one, the basic Bigram algorithm is used.
     *             If equal to 2, a modified version is used that adds transpositions
     * @return     a real number that measures the similarity
     */
    static double bigram(String recordA, String recordB, char method) {
        
        // Initialization needed because the Math.pow() requires it.
        double weight = 1;
        double returnWeight;
        
        int recALength;
        int recBLength;
        int i;
        int j;
        int k;
        int halfCheck = 0;
        int similar = 0;
        int halves = 0;
        
        //Index of the last character in recordA
        k = recordA.length() - 1;
        
        // Identify the strings to be compared by stripping off all leading and
        // trailing spaces.
        for (j = 0; ((recordA.charAt(j) == ' ') && (j < k)); j++) {
            continue;
        }
        
        for (i = k; ((recordA.charAt(i) == ' ') && (i > 0)); i--) {
            continue;
        }
        
        // Length of field from candidate file
        recALength = i + 1 - j;
        
        //Index of the last character in recordA
        k = recordB.length() - 1;
        
        for (j = 0; ((recordB.charAt(j) == ' ') && (j < k)); j++) {
            continue;
        }
        
        for (i = k; ((recordB.charAt(i) == ' ') && (i > 0)); i--) {
            continue;
        }
        
        // Length of field from reference file
        recBLength = i + 1 - j;
        
        // Offers two different methods for Bigram. The basic one and a new
        // version with transposition
        switch(method) {
            
            // Basic method
            case BASIC:
                
                // Loop over all the characters of fieldA up to one character
                // before the end (we compare 2 characters at a time)
                for (i = 0; i < (recALength - 1); i++) {
                    
                    for (j = 0; j < (recBLength - 1); j++) {
                        
                        // Test if every two successive characters are equivalent
                        if ((recordA.charAt(i) == recordB.charAt(j)) && (recordA.charAt(i + 1)
                        == recordB.charAt(j + 1))) {
                            
                            // Increment the count of similar pairs
                            similar++;
                            break;
                        }
                    }
                }
                
                // NEED REVIEW!
                for (i = 0; i < (recBLength - 1); i++) {
                    
                    for (j = 0; j < (recALength - 1); j++) {
                        
                        if ((recordB.charAt(i) == recordA.charAt(j)) && (recordB.charAt(i + 1)
                        == recordA.charAt(j + 1))) {
                            
                            similar++;
                            break;
                        }
                    }
                }
                
                // The weight is the ratio of the total number of similar pairs by the
                // sum of the lengths of both strings
                weight = (similar) / ((recALength + recBLength - 2.0));
                
                break;
                
                // Experimental version which allows for transpositions
            case SWAP:
                
                // Loop over all the characters of the fieldA up to one character
                // before the end (we compare 2 characters at a time).
                for (i = 0; i < (recALength - 1); i++) {
                    
                    halfCheck = 0;
                    
                    for (j = 0; j < (recBLength - 1); j++) {
                        
                        // Test if every two successive characters are equivalent.
                        // Jump out of the inner loop ones a similarity is found
                        if ((recordA.charAt(i) == recordB.charAt(j)) && (recordA.charAt(i + 1)
                        == recordB.charAt(j + 1))) {
                            
                            // Increment the count of similar pairs
                            similar++;
                            halfCheck = 0;
                            break;
                            
                        } else if ((recordA.charAt(i) == recordB.charAt(j + 1))
                        && (recordA.charAt(i + 1) == recordB.charAt(j))) {
                            // Switch the pairs of characters and retest if there is
                            // similarities
                            
                            halfCheck = 1;
                        }
                    }
                    
                    // If the pairs compared are reversely similar, increment the
                    // corresponding index
                    if (halfCheck != 0) {
                        halves++;
                    }
                }
                
                // Loop over all the characters of the fieldB up to one character
                // before the end (we compare 2 characters at a time)
                for (i = 0; i < (recBLength - 1); i++) {
                    
                    halfCheck = 0;
                    
                    // WE COORECTED THE LENGTH IN THE LOOP. THE ORIGINAL
                    // CODE WAS WRONG.
                    for (j = 0; j < (recALength - 1); j++) {
                        
                        // Test if every two successive characters are equivalent.
                        // Jump out of the inner loop ones a similarity is found
                        if ((recordB.charAt(i) == recordA.charAt(j)) && (recordB.charAt(i + 1)
                        == recordA.charAt(j + 1))) {
                            
                            // Increment the count of similar pairs
                            similar++;
                            halfCheck = 0;
                            break;
                            
                        } else if ((recordB.charAt(i) == recordA.charAt(j + 1))
                        && (recordB.charAt(i + 1) == recordA.charAt(j))) {
                            // Switch the pairs of characters and retest if there is
                            // similarities
                            
                            halfCheck = 1;
                        }
                    }
                    
                    // If the pairs compared are reversely similar, increment the
                    // corresponding index
                    if (halfCheck != 0) {
                        halves++;
                    }
                }
                
                // The weight is computed by taking the average value of both
                // similar and Reverse-similar pairs of characters
                weight = ((similar + halves))
                          / ((recALength + recBLength) - 2.0);
                
        }
        
        // In order to get the bigram weight on the same scale as the other
        // string comparators, we raise the weight to the power of 0.2435
        returnWeight = Math.pow(weight, 0.2435);
        
        return returnWeight;     
    }
}
