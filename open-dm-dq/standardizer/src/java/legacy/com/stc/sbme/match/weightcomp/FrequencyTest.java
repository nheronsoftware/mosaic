/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.match.weightcomp;

import com.stc.sbme.api.SbmeMatchingEngine;
import com.stc.sbme.match.util.ReadMatchConstantsValues;
import com.stc.sbme.util.SbmeLogUtil;
import com.stc.sbme.util.SbmeLogger;

/**
 * Check that the frequency tables exists and choose the right index of the table
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
public class FrequencyTest {
    // Index of the first character of a comparison type
    private static final int PRIMARY = ReadMatchConstantsValues.getPrimary();
    // Index of the second character of a comparison type
    private static final int SECONDARY = ReadMatchConstantsValues.getSecondary();
    private static final char FREQUENCY = ReadMatchConstantsValues.getFrequency();
    // Number of frequency tables
    private static final int MAX_NO_TAB = ReadMatchConstantsValues.getMaxNumberTables();
    
    private static final SbmeLogger LOGGER = SbmeLogUtil.getLogger("com.stc.sbme.match.weightcomp.FrequencyTest");  
    
    /**
     *
     *
     * @param mEng a SbmeMatchingEngine instance
     * @return a boolean
     */
    protected static boolean checkFrequency(SbmeMatchingEngine mEng) {
        
        StringBuffer[] methods = mEng.getMatchVar(mEng).comparatorType;
        int[] tableWidth = mEng.getMatchVar(mEng).tableW;
        int gfield = mEng.getMatchC(mEng).getNumFields();
        int[] lengths = mEng.getMatchVar(mEng).lengthF;
        int i;
        int table;
        
        // Loop aver all the matching fields
        for (i = 0; i < gfield; i++) {
            
            // Double check that the first char of comparison type is frequency
            if (methods[i].charAt(PRIMARY) == FREQUENCY) {
                
                table =
                Integer.parseInt(String.valueOf(methods[i].charAt(SECONDARY)));
                
                // Check if the table index is higher than the max.
                if ((table + 1) > MAX_NO_TAB) {
                    LOGGER.error("Too many tables requested");
                    return false;
                }
                // Set the width of a table to the length of matching field + 1
                tableWidth[table] = lengths[i] + 1;
            }
        }
        
        // Reset variable
        table = -1;
        
        //
        for (i = 0; (i < (gfield - 1)) && (i < MAX_NO_TAB); i++) {
            
            if (tableWidth[i] != 0) {
                table = i;
                
            } else if (tableWidth[i + 1] != 0) {
                
                LOGGER.error("Frequency table" + (i + 1) + "is requested, but "
                + "frequency table " + i + " is not");
                return false;
            }
        }
        LOGGER.debug((table + 1) + " frequency tables requested");
        return true;
    }
}
