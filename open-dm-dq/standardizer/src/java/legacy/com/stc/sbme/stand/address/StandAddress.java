/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.stand.address;

import com.stc.sbme.stand.util.ReadStandConstantsValues;
import com.stc.sbme.util.EmeUtil;

/**
 * 
 * 
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
class StandAddress {

// RRC
OutputPattern[] outputPatterns;
public OutputPattern[] getOutputPatterns() {return outputPatterns;}
public void setOutputPatterns(OutputPattern[] outputPatterns) {this.outputPatterns = outputPatterns;}

//    private final int MAX_LENGTH_OF_ANY_OUTPUT_FIELD = ReadStandConstantsValues.getMaxLengthOfAnyOutputField();
    private final int NAME_OUTPUT_FIELD_SIZE;
    private final int NUMBER_OUTPUT_FIELD_SIZE;
    private final int DIRECTION_OUTPUT_FIELD_SIZE;
    private final int TYPE_OUTPUT_FIELD_SIZE;
    private final int PREFIX_OUTPUT_FIELD_SIZE;
    private final int SUFFIX_OUTPUT_FIELD_SIZE;
    private final int EXTENSION_OUTPUT_FIELD_SIZE;
    private final int EXTRAINFO_OUTPUT_FIELD_SIZE;


    /* House number prefix */
    private StringBuffer houseNumPrefix;
    
    /* House number */
    private StringBuffer houseNumber;
    
    // secondary house number prefix/separator
    private StringBuffer secondHouseNumberPrefix;
    
    // Secondary house number
    private StringBuffer secondHouseNumber;
    
    // House number suffix
    private StringBuffer houseNumSuffix;
    
    // street name prefix - directional
    private StringBuffer streetNamePrefDirection;
    
    // street name prefix - type
    private StringBuffer streetNamePrefType;
    
    // original street name
    private StringBuffer origStreetName;

    // original street name
    private StringBuffer origSecondStreetName;
    
    // matching street name
    private StringBuffer matchStreetName;
    
    // storage street name
    private StringBuffer storStreetName;
    
    // street name suffix - type
    private StringBuffer streetNameSufType;

    // Second street name suffix - type
    private StringBuffer secondStreetNameSufType;

    // street name suffix - directional
    private StringBuffer streetNameSufDirection;
    
    // street name extension indicator
    private StringBuffer streetNameExtensionIndex;
    
    // within structure descriptor
    private StringBuffer withinStructDescript;
    
    // within structure identifier
    private StringBuffer withinStructIdentif;
    
    // property description prefix - directional
    private StringBuffer propDesPrefDirection;
    
    // property description prefix - type
    private StringBuffer propDesPrefType;
    
    // original property description name
    private StringBuffer origPropertyName;
    
    // matching property description name
    private StringBuffer matchPropertyName;
    
    // property description suffix - type
    private StringBuffer propertySufType;
    
    // property description suffix - directional
    private StringBuffer propertySufDirection;

    // structure descriptor
    private StringBuffer structureDescript;
    
    // structure identifier
    private StringBuffer structureIdentif;
    
    // rural route descriptor
    private StringBuffer ruralRouteDescript;
    
    // rural route identifier
    private StringBuffer ruralRouteIdentif;
    
    // box descriptor
    private StringBuffer boxDescript;
    
    // box identifier
    private StringBuffer boxIdentif;
    
    // extra information
    private StringBuffer extraInfo;

    // street name prefix - directional code
    private int streetNamePrefDirectionC;
    
    // street name prefix - type code
    private int streetNamePrefTypeC;
    
    // street name suffix - type code
    private int streetNameSufTypeC;
    private int secondStreetNameSufTypeC;
    
    // street name suffix - directional code
    private int streetNameSufDirectionC;
    
    // within structure descriptor code
    private int withinStructDescriptC;
    
    // property description prefix - directional code
    private int propDesPrefDirectionC;
    
    // property description prefix - type code
    private int propDesPrefTypeC;
    
    // property description suffix - type code
    private int propertySufTypeC;
    
    // property description suffix - direction code
    private int propertySufDirectionC;
    
    // rural route descriptor code
    private int ruralRouteDescriptC;
    
    // box descriptor code
    private int boxDescriptC;
    
    // street name extension indicator code
    private int streetNameExtensionIndexC;


    private StandAddress(String domain) {
        super();

    //    private static final int MAX_LENGTH_OF_ANY_OUTPUT_FIELD = ReadStandConstantsValues.getMaxLengthOfAnyOutputField();
        NAME_OUTPUT_FIELD_SIZE = ReadStandConstantsValues.getInstance(domain).getNameOutputFieldSize();
        NUMBER_OUTPUT_FIELD_SIZE = ReadStandConstantsValues.getInstance(domain).getNumberOutputFieldSize();
        DIRECTION_OUTPUT_FIELD_SIZE = ReadStandConstantsValues.getInstance(domain).getDirectionOutputFieldSize();
        TYPE_OUTPUT_FIELD_SIZE = ReadStandConstantsValues.getInstance(domain).getTypeOutputFieldSize();
        PREFIX_OUTPUT_FIELD_SIZE = ReadStandConstantsValues.getInstance(domain).getPrefixOutputFieldSize();
        SUFFIX_OUTPUT_FIELD_SIZE = ReadStandConstantsValues.getInstance(domain).getSuffixOutputFieldSize();
        EXTENSION_OUTPUT_FIELD_SIZE = ReadStandConstantsValues.getInstance(domain).getExtensionOutputFieldSize();
        EXTRAINFO_OUTPUT_FIELD_SIZE = ReadStandConstantsValues.getInstance(domain).getExtrainfoOutputFieldSize();        
        
        houseNumPrefix = new StringBuffer(PREFIX_OUTPUT_FIELD_SIZE);
        houseNumber = new StringBuffer(NUMBER_OUTPUT_FIELD_SIZE);
        secondHouseNumberPrefix = new StringBuffer(PREFIX_OUTPUT_FIELD_SIZE);

        secondHouseNumber = new StringBuffer(NUMBER_OUTPUT_FIELD_SIZE);
        houseNumSuffix = new StringBuffer(SUFFIX_OUTPUT_FIELD_SIZE);
        streetNamePrefDirection = new StringBuffer(DIRECTION_OUTPUT_FIELD_SIZE);
        streetNamePrefType = new StringBuffer(TYPE_OUTPUT_FIELD_SIZE);
        origStreetName = new StringBuffer(NAME_OUTPUT_FIELD_SIZE);
        origSecondStreetName = new StringBuffer(NAME_OUTPUT_FIELD_SIZE);     
        matchStreetName = new StringBuffer(NAME_OUTPUT_FIELD_SIZE);
        storStreetName = new StringBuffer(NAME_OUTPUT_FIELD_SIZE);
        streetNameSufType = new StringBuffer(TYPE_OUTPUT_FIELD_SIZE);
        secondStreetNameSufType = new StringBuffer(TYPE_OUTPUT_FIELD_SIZE);
        
        streetNameSufDirection = new StringBuffer(DIRECTION_OUTPUT_FIELD_SIZE);
        streetNameExtensionIndex = new StringBuffer(EXTENSION_OUTPUT_FIELD_SIZE);

        withinStructDescript = new StringBuffer(TYPE_OUTPUT_FIELD_SIZE);
        withinStructIdentif = new StringBuffer(NUMBER_OUTPUT_FIELD_SIZE);
        propDesPrefDirection = new StringBuffer(DIRECTION_OUTPUT_FIELD_SIZE);

        propDesPrefType = new StringBuffer(TYPE_OUTPUT_FIELD_SIZE);
        origPropertyName = new StringBuffer(NAME_OUTPUT_FIELD_SIZE);
        matchPropertyName = new StringBuffer(NAME_OUTPUT_FIELD_SIZE);
        propertySufType = new StringBuffer(TYPE_OUTPUT_FIELD_SIZE);
        propertySufDirection = new StringBuffer(DIRECTION_OUTPUT_FIELD_SIZE);

        structureDescript = new StringBuffer(TYPE_OUTPUT_FIELD_SIZE);
        structureIdentif = new StringBuffer(NUMBER_OUTPUT_FIELD_SIZE);
        ruralRouteDescript = new StringBuffer(TYPE_OUTPUT_FIELD_SIZE);
        ruralRouteIdentif = new StringBuffer(NUMBER_OUTPUT_FIELD_SIZE);
        boxDescript = new StringBuffer(TYPE_OUTPUT_FIELD_SIZE);
        boxIdentif = new StringBuffer(NUMBER_OUTPUT_FIELD_SIZE);
        extraInfo = new StringBuffer(EXTRAINFO_OUTPUT_FIELD_SIZE);
        
    }


    /**
     * Define an access method to obtain an instance of this class
     * @return an instance of this class
     */
    static StandAddress getSAInstance(String domain) {
        return new StandAddress(domain);
    }


    /**
     * 
     * @return a string
     */
    StringBuffer getHouseNumPrefix() {
        return houseNumPrefix;
    }

    /**
     * 
     * @return a string
     */
    StringBuffer getHouseNumber() {
        return houseNumber;
    }

    /**
     * 
     * @return a string
     */
    StringBuffer getSecondHouseNumberPrefix() {
        return secondHouseNumberPrefix;
    }

    /**
     * 
     * @return a string
     */
    StringBuffer getSecondHouseNumber() {
        return secondHouseNumber;
    }

    /**
     * 
     * @return a string
     */
    StringBuffer getHouseNumSuffix() {
        return houseNumSuffix;
    }

    /**
     * 
     * @return a string
     */
    StringBuffer getStreetNamePrefDirection() {
        return streetNamePrefDirection;
    }

    /**
     * 
     * @return a string
     */
    StringBuffer getStreetNamePrefType() {
        return streetNamePrefType;
    }

    /**
     * 
     * @return a string
     */
    StringBuffer getOrigStreetName() {
        return origStreetName;
    }

    /**
     * 
     * @return a string
     */
    StringBuffer getOrigSecondStreetName() {
        return origSecondStreetName;
    }

    /**
     * 
     * @return a string
     */
    StringBuffer getMatchStreetName() {
        return matchStreetName;
    }

    /**
     * 
     * @return a string
     */
    StringBuffer getStorStreetName() {
        return storStreetName;
    }

    /**
     * 
     * @return a string
     */
    StringBuffer getStreetNameSufType() {
        return streetNameSufType;
    }

    /**
     * 
     * @return a string
     */
    StringBuffer getSecondStreetNameSufType() {
        return secondStreetNameSufType;
    }

    /**
     * 
     * @return a string
     */
    StringBuffer getStreetNameSufDirection() {
        return streetNameSufDirection;
    }

    /**
     * 
     * @return a string
     */
    StringBuffer getStreetNameExtensionIndex() {
        return streetNameExtensionIndex;
    }

    /**
     * 
     * @return a string
     */
    StringBuffer getWithinStructDescript() {
        return withinStructDescript;
    }

    /**
     * 
     * @return a string
     */
    StringBuffer getWithinStructIdentif() {
        return withinStructIdentif;
    }

    /**
     * 
     * @return a string
     */
    StringBuffer getPropDesPrefDirection() {
        return propDesPrefDirection;
    }

    /**
     * 
     * @return a string
     */
    StringBuffer getPropDesPrefType() {
        return propDesPrefType;
    }

    /**
     * 
     * @return a string
     */
    StringBuffer getOrigPropertyName() {
        return origPropertyName;
    }

    /**
     * 
     * @return a string
     */
    StringBuffer getMatchPropertyName() {
        return matchPropertyName;
    }

    /**
     * 
     * @return a string
     */
    StringBuffer getPropertySufType() {
        return propertySufType;
    }

    /**
     * 
     * @return a string
     */
    StringBuffer getPropertySufDirection() {
        return propertySufDirection;
    }

    /**
     * 
     * @return a string
     */
    StringBuffer getStructureDescript() {
        return structureDescript;
    }

    /**
     * 
     * @return a string
     */
    StringBuffer getStructureIdentif() {
        return structureIdentif;
    }

    /**
     * 
     * @return a string
     */
    StringBuffer getRuralRouteDescript() {
        return ruralRouteDescript;
    }

    /**
     * 
     * @return a string
     */
    StringBuffer getRuralRouteIdentif() {
        return ruralRouteIdentif;
    }

    /**
     * 
     * @return a string
     */
    StringBuffer getBoxDescript() {
        return boxDescript;
    }

    /**
     * 
     * @return a string
     */
    StringBuffer getBoxIdentif() {
        return boxIdentif;
    }

    /**
     * 
     * @return a string
     */
    StringBuffer getExtraInfo() {
        return extraInfo;
    }

    /**
     * 
     * @return a string
     */
    int getStreetNamePrefDirectionC() {
        return streetNamePrefDirectionC;
    }

    /**
     * 
     * @return a string
     */
    int getStreetNamePrefTypeC() {
        return streetNamePrefTypeC;
    }

    /**
     * 
     * @return a string
     */
    int getStreetNameSufTypeC() {
        return streetNameSufTypeC;
    }

    /**
     * 
     * @return a string
     */
    int getSecondStreetNameSufTypeC() {
        return secondStreetNameSufTypeC;
    }
    
    /**
     * 
     * @return a string
     */
    int getStreetNameSufDirectionC() {
        return streetNameSufDirectionC;
    }

    /**
     * 
     * @return a string
     */
    int getWithinStructDescriptC() {
        return withinStructDescriptC;
    }

    /**
     * 
     * @return a string
     */
    int getPropDesPrefDirectionC() {
        return propDesPrefDirectionC;
    }

    /**
     * 
     * @return a string
     */
    int getPropDesPrefTypeC() {
        return propDesPrefTypeC;
    }

    /**
     * 
     * @return a string
     */
    int getPropertySufTypeC() {
        return propertySufTypeC;
    }

    /**
     * 
     * @return a string
     */
    int getPropertySufDirectionC() {
        return propertySufDirectionC;
    }

    /**
     * 
     * @return a string
     */
    int getRuralRouteDescriptC() {
        return ruralRouteDescriptC;
    }

    /**
     * 
     * @return a string
     */
    int getBoxDescriptC() {
        return boxDescriptC;
    }

    /**
     * 
     * @return a string
     */
    int getStreetNameExtensionIndexC() {
        return streetNameExtensionIndexC;
    }


    /**
     * 
     * @param s a 
     * @return a house prefix number
     */
    StringBuffer setHouseNumPrefix(String  s) {
     return EmeUtil.replace(this.houseNumPrefix, s);
    }

    /**
     * 
     * @param s a 
     * @return a house number
     */
    StringBuffer setHouseNumber(String  s) {
     return EmeUtil.replace(this.houseNumber, s);
    }

    /**
     * 
     * @param s a 
     * @return a second house prefix number
     */
    StringBuffer setSecondHouseNumberPrefix(String  s) {
     return EmeUtil.replace(this.secondHouseNumberPrefix, s);
    }

    /**
     * 
     * @param ic aaa
     * @param c aaa
     * @return a second house prefix number
     */
    StringBuffer setSecondHouseNumberPrefix(int ic, char c) {
        
        // Make sure that the string is not empty
        int len = secondHouseNumberPrefix.length();
        int i;

        if (len < (ic + 1)) {
            for (i = 0; i < (ic + 1 - len); i++) {
                secondHouseNumberPrefix.append(" ");
            }
        }
        
        secondHouseNumberPrefix.setCharAt(ic, c);
        return secondHouseNumberPrefix;
    }

    /**
     * 
     * @param s a 
     * @return a second house number
     */
    StringBuffer setSecondHouseNumber(String  s) {
     return EmeUtil.replace(this.secondHouseNumber, s);
    }

    /**
     * 
     * @param s a 
     * @return a house suffix number
     */
    StringBuffer setHouseNumSuffix(String  s) {
     return EmeUtil.replace(this.houseNumSuffix, s);
    }

    /**
     * 
     * @param s a 
     * @return a street name direction prefix
     */
    StringBuffer setStreetNamePrefDirection(String  s) {
     return EmeUtil.replace(this.streetNamePrefDirection, s);
    }

    /**
     * 
     * @param s a 
     * @return a street name type prefix
     */
    StringBuffer setStreetNamePrefType(String  s) {
     return EmeUtil.replace(this.streetNamePrefType, s);
    }

    /**
     * 
     * @param s a 
     * @return a street original name 
     */
    StringBuffer setOrigStreetName(String  s) {
     return EmeUtil.replace(this.origStreetName, s);
    }

    /**
     * 
     * @param s a 
     * @return a street original name 
     */
    StringBuffer setOrigSecondStreetName(String  s) {
     return EmeUtil.replace(this.origSecondStreetName, s);
    }

    /**
     * 
     * @param s a 
     * @return a matching street name 
     */
    StringBuffer setMatchStreetName(String  s) {
     return EmeUtil.replace(this.matchStreetName, s);
    }

    /**
     * 
     * @param s a 
     * @return the stored street name 
     */
    StringBuffer setStorStreetName(String  s) {
     return EmeUtil.replace(this.storStreetName, s);
    }

    /**
     * 
     * @param s a 
     * @return a street name suffix type 
     */
    StringBuffer setStreetNameSufType(String  s) {
     return EmeUtil.replace(this.streetNameSufType, s);
    }

    /**
     * 
     * @param s a 
     * @return a street name suffix type 
     */
    StringBuffer setSecondStreetNameSufType(String  s) {
     return EmeUtil.replace(this.secondStreetNameSufType, s);
    }

    /**
     * 
     * @param s a 
     * @return a street name direction suffix
     */
    StringBuffer setStreetNameSufDirection(String  s) {
     return EmeUtil.replace(this.streetNameSufDirection, s);
    }

    /**
     * 
     * @param s a 
     * @return a street name extension index 
     */
    StringBuffer setStreetNameExtensionIndex(String  s) {
     return EmeUtil.replace(this.streetNameExtensionIndex, s);
    }

    /**
     * 
     * @param s a 
     * @return a within structure descriptor
     */
    StringBuffer setWithinStructDescript(String  s) {
     return EmeUtil.replace(this.withinStructDescript, s);
    }

    /**
     * 
     * @param s a 
     * @return a within structure identifier
     */
    StringBuffer setWithinStructIdentif(String  s) {
     return EmeUtil.replace(this.withinStructIdentif, s);
    }

    /**
     * 
     * @param s a 
     * @return a property descriptor for direction prefix
     */
    StringBuffer setPropDesPrefDirection(String  s) {
     return EmeUtil.replace(this.propDesPrefDirection, s);
    }

    /**
     * 
     * @param s a 
     * @return a property descriptor for type prefix
     */
    StringBuffer setPropDesPrefType(String  s) {
     return EmeUtil.replace(this.propDesPrefType, s);
    }
    
    /**
     * 
     * @param s a 
     * @return the original property name
     */
    StringBuffer setOrigPropertyName(String  s) {
     return EmeUtil.replace(this.origPropertyName, s);
    }

    /**
     * 
     * @param s a 
     * @return the matching property name
     */
    StringBuffer setMatchPropertyName(String  s) {
     return EmeUtil.replace(this.matchPropertyName, s);
    }

    /**
     * 
     * @param s a 
     * @return a property suffix type
     */
    StringBuffer setPropertySufType(String  s) {
     return EmeUtil.replace(this.propertySufType, s);
    }

    /**
     * 
     * @param s a 
     * @return a property suffix direction
     */
    StringBuffer setPropertySufDirection(String  s) {
     return EmeUtil.replace(this.propertySufDirection, s);
    }

    /**
     * 
     * @param s a 
     * @return a structure descriptor
     */
    StringBuffer setStructureDescript(String  s) {
     return EmeUtil.replace(this.structureDescript, s);
    }

    /**
     * 
     * @param s a 
     * @return a structure identifier
     */
    StringBuffer setStructureIdentif(String  s) {
     return EmeUtil.replace(this.structureIdentif, s);
    }

    /**
     * 
     * @param s a 
     * @return a rural route descriptor
     */
    StringBuffer setRuralRouteDescript(String  s) {
     return EmeUtil.replace(this.ruralRouteDescript, s);
    }

    /**
     * 
     * @param s a 
     * @return a rural route identifier
     */
    StringBuffer setRuralRouteIdentif(String  s) {
     return EmeUtil.replace(this.ruralRouteIdentif, s);
    }

    /**
     * 
     * @param s a 
     * @return a po box descriptor
     */
    StringBuffer setBoxDescript(String  s) {
     return EmeUtil.replace(this.boxDescript, s);
    }

    /**
     * 
     * @param s a 
     * @return a po box identifier
     */
    StringBuffer setBoxIdentif(String  s) {
     return EmeUtil.replace(this.boxIdentif, s);
    }

    /**
     * 
     * @param s a 
     * @return any fields not caught by the other types
     */
    StringBuffer setExtraInfo(String  s) {
     return EmeUtil.replace(this.extraInfo, s);
    }


    /**
     * 
     * @param m a
     */
    void setStreetNamePrefDirectionC(int m) {
        streetNamePrefDirectionC = m;
    }

    /**
     * 
     * @param m a
     */
    void setStreetNamePrefTypeC(int m) {
        streetNamePrefTypeC = m;
    }

    /**
     * 
     * @param m a
     */
    void setStreetNameSufTypeC(int m) {
        streetNameSufTypeC = m;
    }

    /**
     * 
     * @param m a
     */
//    void setSecondStreetNameSufTypeC(int m) {
//        secondStreetNameSufTypeC = m;
//    }
    
    /**
     * 
     * @param m a
     */
    void setStreetNameSufDirectionC(int m) {
        streetNameSufDirectionC = m;
    }

    /**
     * 
     * @param m a
     */
    void setWithinStructDescriptC(int m) {
        withinStructDescriptC = m;
    }

    /**
     * 
     * @param m a
     */
    void setPropDesPrefDirectionC(int m) {
        propDesPrefDirectionC = m;
    }

    /**
     * 
     * @param m a
     */
    void setPropDesPrefTypeC(int m) {
        propDesPrefTypeC = m;
    }

    /**
     * 
     * @param m a
     */
    void setPropertySufTypeC(int m) {
        propertySufTypeC = m;
    }

    /**
     * 
     * @param m a
     */
    void setPropertySufDirectionC(int m) {
        propertySufDirectionC = m;
    }

    /**
     * 
     * @param m a
     */
    void setRuralRouteDescriptC(int m) {
        ruralRouteDescriptC = m;
    }

    /**
     * 
     * @param m a
     */
    void setBoxDescriptC(int m) {
        boxDescriptC = m;
    }

    /**
     * 
     * @param m a
     */
    void setStreetNameExtensionIndexC(int m) {
        streetNameExtensionIndexC = m;
    }
    /**
	 * @param i
	 */
	public void setSecondStreetNameSufTypeC(int i) {
        secondStreetNameSufTypeC = i;		
	}
}
