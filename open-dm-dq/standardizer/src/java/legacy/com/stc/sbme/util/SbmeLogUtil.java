/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Log utility functions
 * @author  dcidon
 */
public class SbmeLogUtil {
    
    private static SbmePackageTree sLogMapping;
    private static String logPrefix = "STC.eView.SBME";

    /** Constructor to use if you need to set up the prefix name of the path in the logging
     * @param name prefix name of the path in the logging
     */
    public SbmeLogUtil(String name) {
        logPrefix = name;
    }

    static {

        try {
            //Construct a package tree with default string "Common"
            sLogMapping = new SbmePackageTree("Common");
            
            //Construct hashmap used to load tree
            HashMap hm = new HashMap();
            hm.put("API", new String[] {
                        "com.stc.sbme.api"});

            hm.put("Matching", new String[] {
                        "com.stc.sbme.match.emalgorithm",
                        "com.stc.sbme.match.util",
                        "com.stc.sbme.match.weightcomp" });

            hm.put("Standardization", new String[] {
                        "com.stc.sbme.stand.address",
                        "com.stc.sbme.stand.businessname",
                        "com.stc.sbme.stand.personname",
                        "com.stc.sbme.stand.util"}); 
                        
            sLogMapping.addAssignment(hm);
        } catch (Exception e) {
        }
    }

    
    /** Get logger for given object
     * @param obj object to get logger for
     * @return logger
     */
    public static SbmeLogger getLogger(Object obj) {
        return getLogger(obj.getClass().getName());
    }
    
     /** Get logger for given object
     * @param className class name
     * @return logger
     */
    public static SbmeLogger getLogger(String className) {
        String componentName = (String) sLogMapping.getObjectValue(className);
        return SbmeLogger.getLogger(logPrefix + "." + componentName + "." + className);
    }
    
    public static String mapToString(Map map) {
        StringBuffer sb = new StringBuffer();
        Set keys = map.keySet();
        if (keys != null) {
            Iterator i = keys.iterator();
            while (i.hasNext()) {
                Object key = i.next();
                Object value = map.get(key);
                sb.append(key).append('=').append(value).append('\n');
            }
        }
        return sb.toString();
    }
    
    public static String listToString(List list) {
        StringBuffer sb = new StringBuffer();
        Iterator i = list.iterator();
        while (i.hasNext()) {
            Object value = i.next();
            sb.append(value).append('\n');
        }
        return sb.toString();
    }
    
    public static String wrapString(String str) {
        if (str == null || str.length() == 0) {
            return str;
        }
        char[] charArray = str.toCharArray();
        StringBuffer sb = new StringBuffer();
        int count = 0;
        boolean wrap = false;
        for (int i=0; i < charArray.length; i++) {
            sb.append(charArray[i]);
            count++;
            if (count > 80) {
                wrap = true;
            }
            if (wrap && Character.isWhitespace(charArray[i])) {
                sb.append('\n');
                wrap = false;
                count = 0;
            }
        }
        return sb.toString();
    }    
}
