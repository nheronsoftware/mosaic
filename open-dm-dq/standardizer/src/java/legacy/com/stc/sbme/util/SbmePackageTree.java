/*
 * PackageTree.java
 *
 * Created on July 29, 2003, 5:44 AM
 */

package com.stc.sbme.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.StringTokenizer;

import com.stc.sbme.api.SbmeMatchEngineException;

/**
 *
 * @author  dcidon
 */
public class SbmePackageTree {
    
    private final PackageTreeNode mRootNode;
    
    /** Creates a new instance of PackageTree */
    public SbmePackageTree(Object objValue) throws SbmeMatchEngineException {
        if (objValue == null) {
            throw new SbmeMatchEngineException("Object value can not be null for root node");
        }
        mRootNode = new PackageTreeNode(null);
        mRootNode.setObjectValue(objValue);
    }
    
    private class PackageTreeNode {
        
        private final String mPackageComponent;
        private HashMap /* PackageTreeNode */ mChildren;
        private Object mObjectValue;
        
        PackageTreeNode(String packageComponent) {
            mPackageComponent = packageComponent;
        }
        
        String getPackageComponent() {
            return mPackageComponent;
        }
        
        boolean hasChildren() {
            return mChildren != null;
        }
        
        void addChild(PackageTreeNode node) throws SbmeMatchEngineException {
            String packageComponent = node.getPackageComponent();
            if (mChildren == null) {
                mChildren = new HashMap();
            } else {
                if (mChildren.get(packageComponent) != null) {
                    throw new SbmeMatchEngineException("Package componenet already exists: " +
                    packageComponent);
                }
            }
            mChildren.put(packageComponent, node);
        }
        
        Iterator getChildren() {
            if (mChildren == null) {
                return null;
            }
            return mChildren.values().iterator();
        }
        
        PackageTreeNode getChild(String componentName) {
            if (mChildren == null) {
                return null;
            }
            return (PackageTreeNode) mChildren.get(componentName);
        }
        
        Object getObjectValue() {
            return mObjectValue;
        }
        
        void setObjectValue(Object obj) {
            mObjectValue = obj;
        }
        
    }
    
    private ArrayList splitPackageString(String packageString) {
        ArrayList list = new ArrayList();
        StringTokenizer tokenizer = new StringTokenizer(packageString, ".");
        while (tokenizer.hasMoreElements()) {
            list.add(tokenizer.nextElement());
        }
        return list;
    }
    
    public void addAssignment(HashMap hm) throws SbmeMatchEngineException {
        Iterator keys = hm.keySet().iterator();
        while (keys.hasNext()) {
            Object key = keys.next();
            String[] packageStrings = (String[]) hm.get(key);
            for (int i = 0; i < packageStrings.length; i++) {
                addAssignment(packageStrings[i], key); 
            }
        }
    }
    
    public void addAssignment(String packageString, Object objValue) 
    throws SbmeMatchEngineException {
        ArrayList list = splitPackageString(packageString);
        addAssignment(mRootNode, list.iterator(), objValue);
    }
    
    private void addAssignment(PackageTreeNode node, Iterator i, Object objValue) 
    throws SbmeMatchEngineException { 
        if (i.hasNext()) {
            String nextPackageComponent = (String) i.next();
            PackageTreeNode childNode = node.getChild(nextPackageComponent);
            if (childNode == null) {
                childNode = new PackageTreeNode(nextPackageComponent);
                node.addChild(childNode);
            }
            addAssignment(childNode, i, objValue);
        } else {
            node.setObjectValue(objValue);
        }
        
    }
    
    public Object getObjectValue(String packageString) {
        ArrayList list = splitPackageString(packageString);
        return getObjectValue(mRootNode, list.iterator(), mRootNode.getObjectValue());
    }
    
    private Object getObjectValue(PackageTreeNode node, Iterator i, Object objValue) {
        if (node.getObjectValue() != null) {
            objValue = node.getObjectValue();
        }
        if (i.hasNext()) {
            String nextPackageComponent = (String) i.next();
            PackageTreeNode childNode = node.getChild(nextPackageComponent);
            
            if (childNode == null) {
                return objValue;
            } else {
                return getObjectValue(childNode, i, objValue);
            }
        } else {
            return objValue;
        }
    }   
}
