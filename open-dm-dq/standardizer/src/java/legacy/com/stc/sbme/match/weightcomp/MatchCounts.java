/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.match.weightcomp;

/**
 *
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
public class MatchCounts {
    /*
     *
     */
    private int recASkipped;
    /*
     *
     */
    private int recBSkipped;
    /*
     *
     */
    private int numFields;
    /*
     *
     */
    private int matchedPairs;
    /*
     *
     */
    private int clericalPairs;
    /*
     *
     */
    private int unmatchedRecordsA;
    /*
     *
     */
    private int unmatchedRecordsB;
    /*
     *
     */
    private int duplicateRecordsA;
    /*
     *
     */
    private int duplicateRecordsB;
    /*
     *
     */
    private int blankRecordsA;
    /*
     *
     */
    private int blankRecordsB;
    /*
     *
     */
    private int  pairs;
    
    
    /*
     * Custom constructor
     * @param index
     */
    private MatchCounts(int index) {
        super();
        
        setRecASkipped(index);
        setRecBSkipped(index);
        setNumFields(index);
        setMatchedPairs(index);
        setClericalPairs(index);
        setUnmatchedRecordsA(index);
        setUnmatchedRecordsB(index);
        setDuplicateRecordsA(index);
        setDuplicateRecordsB(index);
        setBlankRecordsA(index);
        setBlankRecordsB(index);
        setPairs(index);
    }
    
    /*
     * Default constructor
     */
    private MatchCounts() {
        super();
    }
    
    
    /**
     * A factory method
     *
     * @return a MatchCounts instance
     */
    public static MatchCounts getInstance() {
        return new MatchCounts(0);
    }
    
/*
 * define getter access methods for all the variables
 */
    
    /**
     * gets the number of fields
     *
     * @return numFields
     */
    public int getNumFields() {
        return numFields;
    }
    
    /**
     * gets the number of matched pairs
     *
     * @return matchedPairs
     */
    public int getMatchedPairs() {
        return matchedPairs;
    }
    
    /**
     * gets the number of skipped fields in record A
     *
     * @return recASkipped
     */
    public int getRecASkipped() {
        return recASkipped;
    }
    
    /**
     * gets the number of of skipped fields in record B
     *
     * @return recBSkipped
     */
    public int getRecBSkipped() {
        return recBSkipped;
    }
    
    /**
     * gets the number of fields
     *
     * @return clericalPairs
     */
    public int getClericalPairs() {
        return clericalPairs;
    }
    
    /**
     * gets the number of fields
     *
     * @return unmatchedRecordsA
     */
    public int getUnmatchedRecordsA() {
        return unmatchedRecordsA;
    }
    
    /**
     * gets the number of fields
     *
     * @return unmatchedRecordsB
     */
    public int getUnmatchedRecordsB() {
        return unmatchedRecordsB;
    }
    
    /**
     * gets the number of fields
     *
     * @return duplicateRecordsA
     */
    public int getDuplicateRecordsA() {
        return duplicateRecordsA;
    }
    
    /**
     * gets the number of fields
     *
     * @return duplicateRecordsB
     */
    public int getDuplicateRecordsB() {
        return duplicateRecordsB;
    }
    
    /**
     * gets the number of fields
     *
     * @return blankRecordsA
     */
    public int getBlankRecordsA() {
        return blankRecordsA;
    }
    
    /**
     * gets the number of fields
     *
     * @return blankRecordsB
     */
    public int getBlankRecordsB() {
        return blankRecordsB;
    }
    
    /**
     * gets the number of fields
     *
     * @return pairs
     */
    public int getPairs() {
        return pairs;
    }
    
/*
 * define setter access methods for all the variables
 */
    
    /**
     * sets the number of fields
     *
     * @param numFields the number of fields
     */
    public void  setNumFields(int numFields) {
        this.numFields = numFields;
    }
    
    /**
     * sets the number of fields
     *
     * @param matchedPairs the number of matched pairs
     */
    public void  setMatchedPairs(int matchedPairs) {
        this.matchedPairs = matchedPairs;
    }
    
    /**
     * sets the number of fields
     *
     * @param recASkipped the skipped record
     */
    public void  setRecASkipped(int recASkipped) {
        this.recASkipped = recASkipped;
    }
    
    /**
     * sets the number of fields
     *
     * @param recBSkipped a skipped record in B
     */
    public void  setRecBSkipped(int recBSkipped) {
        this.recBSkipped = recBSkipped;
    }
    
    /**
     * sets the number of fields
     *
     * @param clericalPairs the number of clerical pairs
     */
    public void  setClericalPairs(int clericalPairs) {
        this.clericalPairs = clericalPairs;
    }
    
    /**
     * sets the number of fields
     *
     * @param unmatchedRecordsA an unmatched A record
     */
    public void  setUnmatchedRecordsA(int unmatchedRecordsA) {
        this.unmatchedRecordsA = unmatchedRecordsA;
    }
    
    /**
     * sets the number of fields
     *
     * @param unmatchedRecordsB an unmatched B record
     */
    public void  setUnmatchedRecordsB(int unmatchedRecordsB) {
        this.unmatchedRecordsB = unmatchedRecordsB;
    }
    
    /**
     * sets the number of fields
     *
     * @param duplicateRecordsA a duplicate A record
     */
    public void  setDuplicateRecordsA(int duplicateRecordsA) {
        this.duplicateRecordsA = duplicateRecordsA;
    }
    
    /**
     * sets the number of fields
     *
     * @param duplicateRecordsB a duplicate B record
     */
    public void  setDuplicateRecordsB(int duplicateRecordsB) {
        this.duplicateRecordsB = duplicateRecordsB;
    }
    
    /**
     * sets the number of fields
     *
     * @param blankRecordsA a blank A reocrd
     */
    public void  setBlankRecordsA(int blankRecordsA) {
        this.blankRecordsA = blankRecordsA;
    }
    
    /**
     * sets the number of fields
     *
     * @param blankRecordsB a blank B reocrd
     */
    public void  setBlankRecordsB(int blankRecordsB) {
        this.blankRecordsB = blankRecordsB;
    }
    
    /**
     * sets the number of fields
     *
     * @param pairs pairs
     */
    public void  setPairs(int pairs) {
        this.pairs = pairs;
    }
}
