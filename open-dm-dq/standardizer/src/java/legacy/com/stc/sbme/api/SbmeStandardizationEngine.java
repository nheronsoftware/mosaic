/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.api;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import com.stc.sbme.stand.address.AddressStandVariables;
import com.stc.sbme.stand.address.AddressStandardizer;
import com.stc.sbme.stand.address.AddressTablesLoader;
import com.stc.sbme.stand.businessname.BusinessNameStandardizer;
import com.stc.sbme.stand.businessname.BusinessNameTablesLoader;
import com.stc.sbme.stand.businessname.BusinessStandVariables;
import com.stc.sbme.stand.personname.PersonNameStandVariables;
import com.stc.sbme.stand.personname.PersonNameStandardizer;
import com.stc.sbme.stand.personname.PersonNameTablesLoader;
import com.stc.sbme.stand.util.ReadStandConstantsValues;
import com.stc.sbmeapi.StandConfigFilesAccess;
import com.stc.sbmeapi.impl.StandConfigFilesAccessImpl;


/**
 * The base class that deal with the standardization process
 *
 * @author Sofiane Ouaguenouni
 */
public class SbmeStandardizationEngine {
    
    private HashMap personStans = new HashMap();
    private HashMap addressStans = new HashMap();
    private HashMap bizStans = new HashMap();
    
    private final StandConfigFilesAccess configFilesAccess;
    
    /**
     * Default constructor
     * @param filesAccess a stream to the data files
     * @throws SbmeConfigurationException a missing file exception 
     * @throws IOException an I/O exception
     * @throws SbmeStandardizationException the generic stand exception class
     */
    public SbmeStandardizationEngine(SbmeConfigFilesAccess filesAccess)
        throws SbmeStandardizationException, SbmeConfigurationException, IOException {

        this(0, filesAccess);
    }

        /**
     * Default constructor
     * @param filesAccess a stream to the data files
     * @throws StandConfigFilesAccess a missing file exception
     * @throws IOException an I/O exception
     * @throws SbmeStandardizationException the generic stand exception class
     */
    public SbmeStandardizationEngine(StandConfigFilesAccess filesAccess)
        throws SbmeStandardizationException, SbmeConfigurationException, IOException {

        configFilesAccess = filesAccess;           
    }
    
    /**
     * Custom constructor that instantiate any combination of the
     * diffrent classes types
     *
     * @param indexType deprecated     *
     * @param filesAccess a stream to the data files
     * @throws SbmeConfigurationException a missing file exception 
     * @throws IOException an I/O exception
     * @throws SbmeStandardizationException the generic stand exception
     * @throws SbmeConfigurationException a config exception
     */
    public SbmeStandardizationEngine(int indexType, SbmeConfigFilesAccess filesAccess)
        throws SbmeStandardizationException, SbmeConfigurationException, IOException {
        
        //Index type is ignored
        configFilesAccess = new StandConfigFilesAccessImpl(filesAccess);        
    }
    private void closeStreams(InputStream[] streams) 
        throws IOException {
        int len = streams.length;
        for (int i = 0; i < len; i++) {
            streams[i].close();
        }
    }
    
    private PersonNameStandardizer getPersonNameStandardizer(String domain) 
        throws SbmeConfigurationException, SbmeStandardizationException, SbmeMatchEngineException, IOException {
        PersonNameStandardizer stand = (PersonNameStandardizer) personStans.get(domain);
        if (stand == null) {
            /* Read the content of the data files related to person name standardization */
            PersonNameStandVariables personVars = new PersonNameStandVariables(getReadStandConstantsValues(domain));
            InputStream[] personStream = new InputStream[13];
            personStream[0] = configFilesAccess.providePersonConjonctionFile(domain);
            personStream[1] = configFilesAccess.providePersonGenSuffixFile(domain);
            personStream[2] = configFilesAccess.providePersonFirstNameFile(domain);

            personStream[3] = configFilesAccess.providePersonNamePatternsFile(domain);
            personStream[4] = configFilesAccess.providePersonLastNamePrefixFile(domain);
            personStream[5] = configFilesAccess.provideBusinessOrRelatedFile(domain);

            personStream[6] = configFilesAccess.providePersonOccupSuffixFile(domain);
            personStream[7] = configFilesAccess.providePersonTitleFile(domain);
            personStream[8] = configFilesAccess.providePersonTwoFile(domain);

            personStream[9] = configFilesAccess.providePersonThreeFile(domain);
            personStream[10] = configFilesAccess.provideFirstNameDashFile(domain);
            personStream[11] = configFilesAccess.providePersonSpecCharsFile(domain);
            personStream[12] = configFilesAccess.providePersonLastNameFile(domain);            
            PersonNameTablesLoader personTables = new PersonNameTablesLoader(domain);
            personTables.loadPersonNameTables(personStream, personVars);
            stand = new PersonNameStandardizer(personVars, domain);
            
            personStans.put(domain, stand);
            // Close streams
            closeStreams(personStream);            
        }
        return stand;
    }
   
    private AddressStandardizer getAddressStandardizer(String domain) 
        throws SbmeConfigurationException, SbmeStandardizationException, SbmeMatchEngineException, IOException {
        AddressStandardizer stand = (AddressStandardizer) addressStans.get(domain);
        if (stand == null) {
            getReadStandConstantsValues(domain);  //forces initialization of constants
            AddressStandVariables addressVars = AddressStandVariables.getInstance(domain);
            AddressTablesLoader addressTables = new AddressTablesLoader(domain);
            addressTables.loadAddressTables(addressVars, domain, configFilesAccess);
            stand = new AddressStandardizer(addressVars, addressTables, domain);
            addressStans.put(domain, stand);
        }
        return stand;
    }
    
    private BusinessNameStandardizer getBusinessNameStandardizer(String domain) 
        throws SbmeConfigurationException, SbmeStandardizationException, SbmeMatchEngineException, IOException {
        BusinessNameStandardizer stand = (BusinessNameStandardizer) bizStans.get(domain);
        if (stand == null) {
            getReadStandConstantsValues(domain);  //forces initialization of constants
            BusinessStandVariables bizVars = new BusinessStandVariables(getReadStandConstantsValues(domain));
            InputStream[] bizStream = new InputStream[15];

            bizStream[0] = configFilesAccess.provideOrganizationTypeKeysFile(domain);
            bizStream[1] = configFilesAccess.provideBussinessAssociatitionFile(domain);
            bizStream[2] = configFilesAccess.provideIndustryTypeKeysFile(domain);

            bizStream[3] = configFilesAccess.provideIndustryCategoriesFile(domain);
            bizStream[4] = configFilesAccess.provideBussinessAdjectivesFile(domain);
            bizStream[5] = configFilesAccess.provideBussinessAliasFile(domain);

            bizStream[6] = configFilesAccess.provideCompanyMergerNameFile(domain);
            bizStream[7] = configFilesAccess.provideCompanyPrimaryNameFile(domain);
            bizStream[8] = configFilesAccess.provideBussinessGenTermsFile(domain);

            bizStream[9] = configFilesAccess.provideCountryTypeKeysFile(domain);
            bizStream[10] = configFilesAccess.provideCityStateTypeFile(domain);
            bizStream[11] = configFilesAccess.provideBussinessFormerNameFile(domain);

            bizStream[12] = configFilesAccess.provideBussinessConnectorTokensFile(domain);
            bizStream[13] = configFilesAccess.provideBusinessPatternsFile(domain);
            bizStream[14] = configFilesAccess.provideSpecCharsFile(domain);
            
            BusinessNameTablesLoader bizTables = new BusinessNameTablesLoader();
            bizTables.loadBusinessNameTables(bizStream, bizVars);
            
            stand = new BusinessNameStandardizer(domain, bizVars);
            bizStans.put(domain, stand);
            // Close streams
            closeStreams(bizStream);            
        }
        return stand;
    }
    
    private synchronized ReadStandConstantsValues getReadStandConstantsValues(String domain) 
        throws SbmeConfigurationException, SbmeStandardizationException, IOException {
        ReadStandConstantsValues constants = ReadStandConstantsValues.getInstance(domain);
        if (constants == null) {
            InputStream[] standDataStream = new InputStream[4];
            standDataStream[0] = configFilesAccess.providePersonConstantsFile(domain);
            standDataStream[1] = configFilesAccess.provideAddressConstantsFile(domain);
            standDataStream[2] = configFilesAccess.provideAddressInternalConstantsFile(domain);
            standDataStream[3] = configFilesAccess.provideBizConstantsFile(domain);
            constants = new ReadStandConstantsValues();
            constants.readStandConstants(standDataStream);
            ReadStandConstantsValues.registerInstance(domain, constants);
        }
        return constants;
    }
    
    /**
     * Deprecated
     *
     * @param filesAccess an instance of the class that reads files from the repository
     * @throws IOException an I/O exception
     * @throws SbmeStandardizationException the generic stand exception class
     * @throws SbmeConfigurationException  a config. exception 
     */
    public void initializeData(SbmeConfigFilesAccess filesAccess) 
        throws SbmeStandardizationException, SbmeConfigurationException, IOException {
        
        //TODO: consider removing.  Or just keep empty implementation for backward compatibility.
    }



    /**
     * Deprecated
     *
     * @param filesAccess an instance of the class that reads files from the repository
     * @throws IOException an I/O exception
     * @throws StandConfigFilesAccess the generic stand exception class
     * @throws SbmeConfigurationException  a config. exception 
     */  
      
    public void initializeData(StandConfigFilesAccess filesAccess)
        throws SbmeStandardizationException, SbmeConfigurationException, IOException {
        
        //TODO: consider removing.  Or just keep empty implementation for backward compatibility.
            
    }
   
    
    
    /**
     * Deprecated
     *
     * @param domain geographic location of the lookup tables' data
     * @param indexType offer the choice of instantiating any combination of the
     *                  three different types of standardization. We use:
     *                 1 for person name
     *                 2 for address only
     *                 3 for both person name and address
     *                 4 for business name
     *                 5 for business name and address
     *                 6 for business name and person name
     * @throws SbmeStandardizationException the generic stand exception class
     * @throws SbmeMatchEngineException a generic match exception
     * @throws IOException the java i/o exception class
     */
    public void uploadTables(String domain, int indexType)
        throws SbmeStandardizationException, SbmeMatchEngineException, IOException {
        
        //TODO: consider removing.  Or just keep empty implementation for backward compatibility.
    }
    
    
    /**
     * Uploads all the lookup tables used in the standardization process.
     *
     * @param domain geographic location of the lookup tables' data
     * @throws SbmeStandardizationException the generic stand exception class
     * @throws SbmeMatchEngineException a generic match exception
     * @throws IOException the java i/o exception class
     */
    public void uploadTables(String domain)
        throws SbmeStandardizationException, SbmeMatchEngineException, IOException {
        
        //TODO: consider removing.  Or just keep empty implementation for backward compatibility.
    }
    
    
    /**
     * Clean up the memory from any data related to a given SbmematchingEngine
     * instance that is not needed anymore
     */
    public void shutdown() {
    }
    
    
    /**
     * Normalize an object
     *
     * @param  standObj instance of a class implementing the SbmeStandRecord
     *                  interface.
     * @param domain geographic location of the compared data
     * @return standRecord the standardized object
     * @throws SbmeStandardizationException the generic stand exception class
     */
    public SbmeStandRecord normalize(SbmeStandRecord standObj, String domain)
        throws SbmeStandardizationException {
        
        // Perform the normalization process
        String className = standObj.getType();
        
        try {
            if (className.compareTo("PersonName") == 0) {
                return getPersonNameStandardizer(domain).normalize(standObj);

            } else if (className.compareTo("Address") == 0) {
                return getAddressStandardizer(domain).normalize(standObj);

            } else if (className.compareTo("BusinessName") == 0) {
                return getBusinessNameStandardizer(domain).normalize(standObj);

            } else {
                String msg = "There is no standardization engine for this type";
                throw new IllegalArgumentException(msg);
            }
        } catch (SbmeConfigurationException e) {
            throw new SbmeStandardizationException(e);
        } catch (SbmeMatchEngineException e) {
            throw new SbmeStandardizationException(e);
        } catch (IOException e) {
            throw new SbmeStandardizationException(e);
        }
    }
    
    /**
     * Normalize a set of objects
     *
     * @param  typeNames the type of record
     * @param value the corresponding value
     * @param domain geographic locations of the compared data
     * @return standRecord the standardized 2D array of objects
     * @throws SbmeStandardizationException the generic stand exception class
     * @throws SbmeMatchEngineException a generic match exception
     * @throws IOException exception
     */
    public SbmeStandRecord[] normalize(SbmeStandRecord[] standObjs, String[] domain)
        throws SbmeStandardizationException {
        /* The number of records in the array */
        int numberOfObjects = standObjs.length;

        int i;        
        
        /* Array of returned normalized objects */
        SbmeStandRecord[] standRecord = new SbmeStandRecord[numberOfObjects];
        try {        
            for (i = 0; i < numberOfObjects; i++) {

                /* Find out the type of the object (person, address, business) */
                String className = standObjs[i].getType();

                if (className.compareTo("PersonName") == 0) {
                    standRecord[i] = getPersonNameStandardizer(domain[i]).normalize(standObjs[i]);
                } else if (className.compareTo("Address") == 0) {
                    standRecord[i] = getAddressStandardizer(domain[i]).normalize(standObjs[i]);
                } else if (className.compareTo("BusinessName") == 0) {
                    standRecord[i] = getBusinessNameStandardizer(domain[i]).normalize(standObjs[i]);
                } else {
                    String msg = "";
                    throw new IllegalArgumentException(msg);
                }
            }
        } catch (SbmeConfigurationException e) {
            throw new SbmeStandardizationException(e);
        } catch (SbmeMatchEngineException e) {
            throw new SbmeStandardizationException(e);
        } catch (IOException e) {
            throw new SbmeStandardizationException(e);
        }
        return standRecord;            
    }    
    
    
    /**
     *
     *
     * @param  standObjs Array of instances of a class implementing the
     *                   SbmeStandRecord interface.
     * @param domain geographic location of the compared data
     * @return standRecord the standardized 1D array of objects
     * @throws SbmeStandardizationException the generic stand exception class
     */
    public SbmeStandRecord[] normalize(SbmeStandRecord[] standObjs, String domain)
        throws SbmeStandardizationException {
        
        /* The number of objects in the array */
        int numberOfObjects = standObjs.length;
        
        int i;
        
        /* the name type */
        String className;
        
        /* Store the objects of person name type */
        ArrayList personList = new ArrayList();
        /* Store the objects of address type */
        ArrayList addressList = new ArrayList();
        /* Store the objects of business name type */
        ArrayList businessList = new ArrayList();
        
        /* Array of returned normalized objects */
        SbmeStandRecord[] standRecords = new SbmeStandRecord[numberOfObjects];
        
        /* Mapping between the standObjs indexes and the */
        int[] pMap = new int[numberOfObjects];
        int[] aMap = new int[numberOfObjects];
        int[] bMap = new int[numberOfObjects];
        
        int personIndex = 0;
        int addressIndex = 0;
        int busiIndex = 0;
        
        for (i = 0; i < numberOfObjects; i++) {
            
            /* Find out the type of the object (person, address, business) */
            className = standObjs[i].getType();
            
            if (className.compareTo("PersonName") == 0) {
                
                /* Add it to the set of person name objects */
                personList.add(personIndex, standObjs[i]);
                
                pMap[personIndex] = i;
                personIndex++;
                
            } else if (className.compareTo("Address") == 0) {
                
                addressList.add(addressIndex, standObjs[i]);
                
                aMap[addressIndex] = i;
                addressIndex++;
                
            } else if (className.compareTo("BusinessName") == 0) {
                
                businessList.add(busiIndex, standObjs[i]);
                
                bMap[busiIndex] = i;
                busiIndex++;
                
            } else {
                String msg = "";
                throw new IllegalArgumentException(msg);
            }
        }
        
        try {
            if (personIndex > 0) {

                /* Normalize all the objects that are of person name type */
                getPersonNameStandardizer(domain).normalize(personList);

                for (i = 0; i < personIndex; i++) {
                    standRecords[pMap[i]] = (SbmeStandRecord) personList.get(i);
                }
            }

            if (addressIndex > 0) {
                getAddressStandardizer(domain).normalize(addressList);

                for (i = 0; i < personIndex; i++) {
                    standRecords[aMap[i]] = (SbmeStandRecord) addressList.get(i);
                }
            }

            if (busiIndex > 0) {
                getBusinessNameStandardizer(domain).normalize(businessList);

                for (i = 0; i < personIndex; i++) {
                    standRecords[bMap[i]] = (SbmeStandRecord) businessList.get(i);
                }
            }
        } catch (SbmeConfigurationException e) {
            throw new SbmeStandardizationException(e);
        } catch (SbmeMatchEngineException e) {
            throw new SbmeStandardizationException(e);
        } catch (IOException e) {
            throw new SbmeStandardizationException(e);
        }
        return standRecords;
    }
    
    
    /**
     *
     *
     * @param  typeName the type of the record (person, address,...)
     * @param  value the actual string record
     * @param domain geographic location of the compared data
     * @return the 2D array of standardized objects of either PersonName,
     *         Address or BusinessName
     * @throws SbmeStandardizationException the generic stand exception class
     * @throws SbmeMatchEngineException a generic match exception
     * @throws IOException exception
     */
    public SbmeStandRecord[] standardize(String typeName, String value, String domain)
        throws SbmeStandardizationException, SbmeMatchEngineException, IOException {
        
        /* Check the type of standardization requested */
        if (typeName.compareTo("PersonName") == 0) {
            return getPersonNameStandardizer(domain).standardize(value);
            
        } else if (typeName.compareTo("Address") == 0) {
            return getAddressStandardizer(domain).standardize(value);
            
        } else if (typeName.compareTo("BusinessName") == 0) {
            return getBusinessNameStandardizer(domain).standardize(value);
            
        } else {
            String msg = "";
            throw new IllegalArgumentException(msg);
        }
    }
    
    /**
     * Uploads one of the predefined configuration files used by
     * one specific user (one instance of this class)
     *
     * @param  typeNames the type of record
     * @param value the corresponding value
     * @param domain geographic locations of the compared data
     * @return standRecord the standardized 2D array of objects
     * @throws SbmeStandardizationException the generic stand exception class
     * @throws SbmeMatchEngineException a generic match exception
     * @throws IOException exception
     */
    public SbmeStandRecord[][] standardize(String[] typeNames, String[] value, String[] domain)
        throws SbmeStandardizationException, SbmeMatchEngineException, IOException {
        /* The number of records in the array */
        int numberOfRecs = typeNames.length;

        int i;        
        
        /* Array of returned normalized objects */
        SbmeStandRecord[][] standRecord = new SbmeStandRecord[numberOfRecs][3];
        
        for (i = 0; i < numberOfRecs; i++) {
 
            if (typeNames[i].compareTo("PersonName") == 0) {
                standRecord[i] = getPersonNameStandardizer(domain[i]).standardize(value[i]);
            } else if (typeNames[i].compareTo("Address") == 0) {
                standRecord[i] = getAddressStandardizer(domain[i]).standardize(value[i]);
            } else if (typeNames[i].compareTo("BusinessName") == 0) {
                standRecord[i] = getBusinessNameStandardizer(domain[i]).standardize(value[i]);
            } else {
                String msg = "";
                throw new IllegalArgumentException(msg);
            }
        }

        return standRecord;            
    }
    
    
    /**
     * Uploads one of the predefined configuration files used by
     * one specific user (one instance of this class)
     *
     * @param  typeNames the type of record
     * @param value the corresponding value
     * @param domain geographic location of the compared data
     * @return standRecord the standardized 2D array of objects
     * @throws SbmeStandardizationException the generic stand exception class
     * @throws SbmeMatchEngineException a generic match exception
     * @throws IOException exception
     */
    public SbmeStandRecord[][] standardize(String[] typeNames, String[] value, String domain)
        throws SbmeStandardizationException, SbmeMatchEngineException, IOException {

        /* The number of records in the array */
        int numberOfRecs = typeNames.length;

        int i;
        int j;
        int numberOfElements;
        
        /* Store the objects of person name type */
        ArrayList personList = new ArrayList();
        /* Store the objects of address type */
        ArrayList addressList = new ArrayList();
        /* Store the objects of business name type */
        ArrayList businessList = new ArrayList();
        
        /* Array of returned normalized objects */
        SbmeStandRecord[][] standRecord = new SbmeStandRecord[numberOfRecs][3];
        
        /* Temporary array of SbmeStandRecord objects */
        SbmeStandRecord[][] tempStandRecord = new SbmeStandRecord[numberOfRecs][3];
        
        /* Temporary array of record objects */
        Object[] tempObj;
        
        int tempObjSize;
        
        /* Mapping between the standObjs indexes and the */
        int[] pMap = new int[numberOfRecs];
        int[] aMap = new int[numberOfRecs];
        int[] bMap = new int[numberOfRecs];
        
        int personIndex = 0;
        int addressIndex = 0;
        int busiIndex = 0;
        
        for (i = 0; i < numberOfRecs; i++) {
 
            if (typeNames[i].compareTo("PersonName") == 0) {
                
                /* Add it to the set of person name objects */
                personList.add(personIndex, value[i]);
                
                pMap[personIndex] = i;
                personIndex++;
                
            } else if (typeNames[i].compareTo("Address") == 0) {
                
                addressList.add(addressIndex, value[i]);
                
                aMap[addressIndex] = i;
                addressIndex++;
                
            } else if (typeNames[i].compareTo("BusinessName") == 0) {
                
                businessList.add(busiIndex, value[i]);
                
                bMap[busiIndex] = i;
                busiIndex++;
                
            } else {
                String msg = "";
                throw new IllegalArgumentException(msg);
            }
        }

        if (personIndex > 0) {
            
            /* Normalize all the objects that are of person name type */
            tempStandRecord = getPersonNameStandardizer(domain).standardize(personList);
            
            for (i = 0; i < personIndex; i++) {
                
                /* Calculate the number of SbmeStandRecord objects per ArrayList */
                numberOfElements = tempStandRecord[i].length;
                
                for (j = 0; j < numberOfElements; j++) {
                    standRecord[pMap[i]][j] = tempStandRecord[i][j];
                }
            }
        }
            
        if (addressIndex > 0) {
            
            tempStandRecord = getAddressStandardizer(domain).standardize(addressList);
            
            for (i = 0; i < addressIndex; i++) {
               
                /* Calculate the number of SbmeStandRecord objects per ArrayList */
                numberOfElements = tempStandRecord[i].length;

                for (j = 0; j < numberOfElements; j++) {
                    standRecord[aMap[i]][j] = tempStandRecord[i][j];
                }
            }
        }
            
        if (busiIndex > 0) {
            tempStandRecord = getBusinessNameStandardizer(domain).standardize(businessList);
            
            for (i = 0; i < busiIndex; i++) {
            
                /* Calculate the number of SbmeStandRecord objects per ArrayList */
                numberOfElements = tempStandRecord[i].length;
                
                for (j = 0; j < numberOfElements; j++) {
                    standRecord[bMap[i]][j] = tempStandRecord[i][j];
                }
            }
        }
        return standRecord;
    }
}
