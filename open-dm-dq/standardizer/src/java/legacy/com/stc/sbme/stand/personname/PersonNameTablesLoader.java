/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.stand.personname;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StreamTokenizer;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.TreeSet;

import com.stc.sbme.api.SbmeMatchEngineException;
import com.stc.sbme.api.SbmeStandardizationException;
import com.stc.sbme.stand.util.DataFilesValidator;
import com.stc.sbme.stand.util.ReadStandConstantsValues;
import com.stc.sbme.util.EmeUtil;
import com.stc.sbme.util.SbmeLogUtil;
import com.stc.sbme.util.SbmeLogger;
/**
 * Loads all the person name tables and assign the data to the
 * corresponding variables
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
public class PersonNameTablesLoader {
    // Max number of patterns
    private final int PTRNMAX1;
    private final int PTRNMAX2;
    // Max number of conjonctions
    private final int CONJMAX;
    // Max number of generational suffixes
    private final int JRSRMAX;
    // Max number of first names
    private final int NICKMAX;
    // Max number of prefixes
    private final int PREMAX;
    // Max number of words to skip
    private final int SKPMAX;
    // Max number of suffixes
    private final int SUFMAX;
    // Max number of titles
    private final int TITLMAX;
    // Max number of two-char words
    private final int TWOMAX;
    // Max number of
    private final int THREMAX;
    // Max number of
    private final int BLNKMAX;
    // Max number of

    private final SbmeLogger mLogger = SbmeLogUtil.getLogger(this);  
    private final boolean mDebug = mLogger.isDebugEnabled();
    
        /* */
    private BufferedReader brFptrt;
        /* */
    private StreamTokenizer stFptrt;
    
    public PersonNameTablesLoader(String domain) {
        PTRNMAX1 = ReadStandConstantsValues.getInstance(domain).getPtrnmax1();
        PTRNMAX2 = ReadStandConstantsValues.getInstance(domain).getPtrnmax2();
        CONJMAX = ReadStandConstantsValues.getInstance(domain).getConjmax();
        JRSRMAX = ReadStandConstantsValues.getInstance(domain).getJrsrmax();
        NICKMAX = ReadStandConstantsValues.getInstance(domain).getNickmax();
        PREMAX = ReadStandConstantsValues.getInstance(domain).getPremax();
        SKPMAX = ReadStandConstantsValues.getInstance(domain).getSkpmax();
        SUFMAX = ReadStandConstantsValues.getInstance(domain).getSufmax();
        TITLMAX = ReadStandConstantsValues.getInstance(domain).getTitlmax();
        TWOMAX = ReadStandConstantsValues.getInstance(domain).getTwomax();
        THREMAX = ReadStandConstantsValues.getInstance(domain).getThremax();
        BLNKMAX = ReadStandConstantsValues.getInstance(domain).getBlnkmax();    
    }
    
    /**
     *
     *
     * @param personStream a stream to the data files
     * @param sVar an instance of the BusinessStandVariables class
     * @param domain a geolocation
     * @throws SbmeStandardizationException the generic stand exception
     * @throws SbmeMatchEngineException an exception
     * @throws IOException a java i/o exception
     */
    public void loadPersonNameTables(InputStream[] personStream, PersonNameStandVariables sVar)
    throws SbmeStandardizationException, SbmeMatchEngineException, IOException {
        
        int i = 0;
        int j = 0;
        TreeSet tempString = new TreeSet();
        LinkedHashMap hm = new LinkedHashMap();
        String tempS;
        
	if (mDebug) {
	    mLogger.debug("Reading PersonName conjunctions file");
	}             
       /*
        * Open a BufferedReader/StreamTokenizer stream to read a list of
        * conjunctions that may appear in the name field indicating that
        * 2 or more standardized records are to be produced (for example,
        * '&' or 'C/O')
        */
        readInTable(personStream[0], "Cannot open file of conjunctions");
        
       /*
        * Loop over every line of "conj.dat". Test that the size is less that
        * the max authorized
        */
        while (!EmeUtil.feof(brFptrt) && (stFptrt.lineno() < CONJMAX)) {
            
            stFptrt.nextToken();

            if ((stFptrt.sval == null) || (stFptrt.sval.trim().length() == 0)) {
                continue;
            }
            // Store the data into a temporary TreeSet object           
            tempString.add(stFptrt.sval);
        }
        
        // Store the total number of conj. into nbConj
        sVar.nbConj = tempString.size();
        Iterator iter = tempString.iterator();        

        for (i = 0; iter.hasNext(); i++) {
        
            tempS =  (String)iter.next();                            
            /* Store the conjunction field into conj[] */
            sVar.conj[i] = new StringBuffer(tempS.trim());           
        }
        
        /* Clear the temporary TreeSet and close the stream */
        tempString.clear();
        brFptrt.close();
        
        
        

	if (mDebug) {
	    mLogger.debug("Reading PersonName generational suffixes file");
	}            
       /*
        * Open a BufferedReader/StreamTokenizer stream to read a list of
        * generational suffixes (i.e. 'JR' or 'III')
        */
        readInTable(personStream[1], "Cannot open file jrsr");
        
        /*
         * Loop over every line of "jrsr.dat". Test that the size is less that
         * the max authorized
         */
        while (!EmeUtil.feof(brFptrt) && (stFptrt.lineno() < JRSRMAX)) {
            
            stFptrt.nextToken();
            
            if ((stFptrt.sval == null) || (stFptrt.sval.trim().length() == 0)) {
                continue;
            }
            // Store the data into a temporary TreeSet object           
            tempString.add(stFptrt.sval);
        }
        // Ensure that the list of first names has unique key names and that there
        // is only one level of mapping
        tempString = DataFilesValidator.enforceOneLevelMapping(tempString, 12);
        
        sVar.nbJrsr = tempString.size();
        iter = tempString.iterator();        

        for (i = 0; iter.hasNext(); i++) {
        
            tempS =  (String)iter.next();                              
            // Store the generational suffix field into jrsrcd[]
            sVar.jrsrcd[i] = new StringBuffer(tempS.substring(0, 12).trim());
            
            //  Read the second param jrsr2[] associated with jrsrFlag
            sVar.jrsr2[i] = new StringBuffer(tempS.substring(12).trim());
        }
        
        /* Clear the temporary TreeSet and close the stream */
        tempString.clear();
        brFptrt.close();
        
               

	if (mDebug) {
	    mLogger.debug("Reading PersonName first names file");
	}              
       /*
        * Open a BufferedReader/StreamTokenizer stream to read a list of names
        * used to replace first or middle names (i.e. 'Al' for 'Albert')
        */
        readInTable(personStream[2], "Cannot open file of first names");
        
       /*
        * Loop over every line of "nick.dat". Test that the size is less that
        * the max authorized
        */
        while (!EmeUtil.feof(brFptrt) && (stFptrt.lineno() < NICKMAX)) {
            
            stFptrt.nextToken();
            
            if ((stFptrt.sval == null) || (stFptrt.sval.trim().length() == 0)) {
                continue;
            }
            // Store the data into a temporary TreeSet object           
            tempString.add(stFptrt.sval);
        }
        
        // Ensure that the list of first names has unique key names and that there
        // is only one level of mapping
        tempString = DataFilesValidator.enforceOneLevelMapping(tempString, 16, 32);
        // Store the total number of nick names into nbNick and close the stream
         sVar.nbFirst = tempString.size();
        iter = tempString.iterator();
        
        for (i = 0; iter.hasNext(); i++) {
            
            tempS =  (String)iter.next();        
            /* Store the nick name field into newName[] */
            sVar.firstName[i] = new StringBuffer(tempS.substring(0, 16).trim());
            
            /* Store the standard name field into orig_name[] */
            sVar.standFirstName[i] = tempS.substring(16, 32).trim();
            
            /* Store the standard name field into orig_name[] */
            sVar.nameGender[i] = new StringBuffer(tempS.substring(32).trim());           
        }
        
        /* Clear the temporary TreeSet and close the stream */
        tempString.clear();
        brFptrt.close();
        
        
        /*
         * Open a BufferedReader/StreamTokenizer stream to read a list of names
         * used to replace first or middle names (i.e. 'Al' for 'Albert')
         */
         readInTable(personStream[12], "Cannot open file of last names");
        
        /*
         * Loop over every line of "nick.dat". Test that the size is less that
         * the max authorized
         */
         while (!EmeUtil.feof(brFptrt) && (stFptrt.lineno() < NICKMAX)) {
            
             stFptrt.nextToken();
            
             if ((stFptrt.sval == null) || (stFptrt.sval.trim().length() == 0)) {
                 continue;
             }
             // Store the data into a temporary TreeSet object           
             tempString.add(stFptrt.sval);
         }
        // Ensure that the list of first names has unique key names and that there
        // is only one level of mapping
        tempString = DataFilesValidator.enforceOneLevelMapping(tempString, 30);         
         
         // Store the total number of nick names into nbNick
         sVar.nbLast = tempString.size();
         iter = tempString.iterator();
        
         for (i = 0; iter.hasNext(); i++) {
            
             tempS =  (String)iter.next();                 
             /* Store the nick name field into newName[] */
             sVar.lastName[i] = new StringBuffer(tempS.substring(0, 30).trim());
            
             /* Store the standard name field into orig_name[] */
             sVar.standLastName[i] = tempS.substring(30).trim();            
         }
        
        /* Clear the temporary TreeSet and close the stream */
        tempString.clear();
        brFptrt.close();              
 
	if (mDebug) {
	    mLogger.debug("Reading PersonName patterns file");
	}             
       /*
        * Open a BufferedReader/StreamTokenizer stream to read a list of name
        * patterns that the name string may exhibit. The standardizer uses
        * these patterns to rearrange the name's elements to a common order
        */
        readInTable(personStream[3], "Cannot open file of patterns");
        
       /*
        * Loop over every line of "patt.dat". Test that the size is less that
        * the max authorized
        */
        while (!EmeUtil.feof(brFptrt) && (stFptrt.lineno() < PTRNMAX1)) {
            
            stFptrt.nextToken();
            
            if ((stFptrt.sval == null) || (stFptrt.sval.trim().length() == 0)) {
                continue;
            }
            // Store the data into a temporary TreeSet object           
            tempString.add(stFptrt.sval);
        }
         
        // Store the total number of patterns into nbPatt
        sVar.nbPatt = tempString.size();
        iter = tempString.iterator();            

        for (i = 0; iter.hasNext(); i++) {
            
            tempS =  (String)iter.next();                    
            /* Store the word type field into ptrn1[] */
            sVar.ptrn1[i] = new StringBuffer(tempS.substring(0, 20).trim());
            
            /* 
             * Loop over the 10 integers  associated respectively with Title
             * First or middle name/Prefix of last/Last with no prefix/jr/sr...
             */
            for (j = 0; j < PTRNMAX2; j++) {
                
                sVar.ptrn2[i][j] = Integer.parseInt(tempS.substring(20 + 2 * j, 
                22 + 2 * j).trim());
            }            
        }
        
        /* Clear the temporary TreeSet and close the stream */
        tempString.clear();
        brFptrt.close();
        

	if (mDebug) {
	    mLogger.debug("Reading PersonName last names prefixes file");
	}             
       /*
        * Open a BufferedReader/StreamTokenizer stream to read a list of
        * prefixes found in last names prefixes (i.e. 'de' or 'Vander')
        */
        readInTable(personStream[4], "Cannot open file of prefixes");
        
        while (!EmeUtil.feof(brFptrt) && (stFptrt.lineno() < PREMAX)) {
            
            stFptrt.nextToken();
 
            if ((stFptrt.sval == null) || (stFptrt.sval.trim().length() == 0)) {
                continue;
            }
            // Store the data into a temporary TreeSet object           
            tempString.add(stFptrt.sval);
        }
        // Ensure that the list of last names prefixes has unique key names and that there
        // is only one level of mapping
        tempString = DataFilesValidator.enforceOneLevelMapping(tempString, 20);
         
        // Store the total number of last names prefixes into nbPref
        sVar.nbPref = tempString.size();
        iter = tempString.iterator();               
            
        for (i = 0; iter.hasNext(); i++) {
            
            tempS =  (String)iter.next();                                     
            // Store the prefix field into prefix[]
            sVar.prefix[i] = new StringBuffer(tempS.substring(0, 20).trim());
            /* Store the standard name field into orig_name[] */
            sVar.standPrefix[i] = tempS.substring(20).trim();  
        }
        
        /* Clear the temporary TreeSet and close the stream */
        tempString.clear();
        brFptrt.close();
        

	if (mDebug) {
	    mLogger.debug("Reading PersonName skipped words file");
	}              
       /*
        * Open a BufferedReader/StreamTokenizer stream to read a list of words
        * that should be skipped by the standardizer for being too obvious to
        * standardize It depends on the application in which the engine is used
        * (ex. 'Theatre')
        */
        readInTable(personStream[5], "Cannot open file of skip words");
        
       /*
        * Loop over every line of "skpl.dat". Test that the size is less that
        * the max authorized
        */
        while (!EmeUtil.feof(brFptrt) && (stFptrt.lineno() < SKPMAX)) {
            
            stFptrt.nextToken();
            
            if ((stFptrt.sval == null) || (stFptrt.sval.trim().length() == 0)) {
                continue;
            }
            // Store the data into a temporary TreeSet object           
            tempString.add(stFptrt.sval);
        }
         
        // Store the total number of last names prefixes into nbPref
        sVar.nbOther = tempString.size();
        iter = tempString.iterator();               
            
        for (i = 0; iter.hasNext(); i++) {            
            tempS =  (String)iter.next(); 
                      
            // Store the words to be skipped field into skplst[]
            sVar.other[i] = new StringBuffer(tempS.substring(0, 17).trim());            
            // Store the associated flag into skplst2[]
            sVar.otherIndex[i] = new StringBuffer(tempS.substring(17).trim());
        }
        
        /* Clear the temporary TreeSet and close the stream */
        tempString.clear();
        brFptrt.close();
        
        

	if (mDebug) {
	    mLogger.debug("Reading PersonName occupational suffixes file");
	}             
       /*
        * Open a BufferedReader/StreamTokenizer stream to read a list of
        * occupational suffixes (i.e. 'Esq' or 'PHD')
        */
        readInTable(personStream[6], "Cannot open file of occupational suffixes");
        
       /*
        * Loop over every line of "suff.dat". Test that the size is less that
        * the max authorized
        */
        while (!EmeUtil.feof(brFptrt) && (stFptrt.lineno() < SUFMAX)) {
            
            stFptrt.nextToken();
 
            if ((stFptrt.sval == null) || (stFptrt.sval.trim().length() == 0)) {
                continue;
            }
            // Store the data into a temporary TreeSet object           
            tempString.add(stFptrt.sval);
        }
         
        // Store the total number of occupational suffixes into n_suff
        sVar.nbOccupSuff = tempString.size();
        iter = tempString.iterator();               
            
        for (i = 0; iter.hasNext(); i++) {            
            tempS =  (String)iter.next(); 
           
            // Store the occupational suffixes into suffix[]
            sVar.occupSuffix[i] = new StringBuffer(tempS.trim());
        }
        
        /* Clear the temporary TreeSet and close the stream */
        tempString.clear();        
        brFptrt.close();

	if (mDebug) {
	    mLogger.debug("Reading PersonName titles file");
	}               
       /*
        * Open a BufferedReader/StreamTokenizer stream to read a list of prefix
        * titles (i.e. 'Mr' or 'Dr')
        */
        readInTable(personStream[7], "Cannot open file of titles");
        
       /*
        * Loop over every line of "title.dat". Test that the size is less that
        * the max authorized
        */
        while (!EmeUtil.feof(brFptrt) && (stFptrt.lineno() < TITLMAX)) {
            
            stFptrt.nextToken();
            
            if ((stFptrt.sval == null) || (stFptrt.sval.trim().length() == 0)) {
                continue;
            }
            // Store the data into a temporary TreeSet object           
            tempString.add(stFptrt.sval);
        }
        // Ensure that the list of titles has unique key names and that there
        // is only one level of mapping
        tempString = DataFilesValidator.enforceOneLevelMapping(tempString, 27, 38);        
         
        // Store the total number of prefix title
        sVar.nbTitl = tempString.size();
        iter = tempString.iterator();               
            
        for (i = 0; iter.hasNext(); i++) {            
            tempS =  (String)iter.next(); 

            // Store the prefix title into title[]
            sVar.title[i] = new StringBuffer(tempS.substring(0, 27).trim());
            /* If the token is not standard, use the following token */
            sVar.standTitle[i] = tempS.substring(27, 38).trim();
            /* If the token is not standard, use the following token */
            sVar.titleGender[i] = tempS.substring(38);
        }
        
        /* Clear the temporary TreeSet and close the stream */
        tempString.clear(); 
        brFptrt.close();
            
 
	if (mDebug) {
	    mLogger.debug("Reading PersonName two-character occupational suffixes file");
	}               
       /*
        * Open a BufferedReader/StreamTokenizer stream to read a list of
        * two-letter occupational suffixes with embedded space (i.e. 'M D')
        */
        readInTable(personStream[8], "Cannot open file of two-letter occupational suffixes");
        
       /*
        * Loop over every line of "two.dat". Test that the size is less that
        * the max authorized
        */
        while (!EmeUtil.feof(brFptrt) && (stFptrt.lineno() < TWOMAX)) {
            
            stFptrt.nextToken();

            if ((stFptrt.sval == null) || (stFptrt.sval.trim().length() == 0)) {
                continue;
            }
            // Store the data into a temporary TreeSet object           
            tempString.add(stFptrt.sval);
        }
         
        // Store the total number of two-prefix
        sVar.nbTwo = tempString.size();
        iter = tempString.iterator();               
            
        for (i = 0; iter.hasNext(); i++) {            
            tempS =  (String)iter.next(); 
            
            // Store the 2-suffixes into two_suff[]
            sVar.twoSuff[i] = new StringBuffer(tempS.trim());
        }
        
        /* Clear the temporary TreeSet and close the stream */
        tempString.clear(); 
        brFptrt.close();     
        
  
	if (mDebug) {
	    mLogger.debug("Reading PersonName three-character occupational suffixes file");
	}                
       /*
        * Open a BufferedReader/StreamTokenizer stream to read a list of
        * three-letter occupational suffixes with embedded space (i.e. 'D M D')
        */
        readInTable(personStream[9], "Cannot open file of three suffixes");
        
       /*
        * Loop over every line of "thre.dat". Test that the size is less that
        * the max authorized
        */
        while (!EmeUtil.feof(brFptrt) && (stFptrt.lineno() < THREMAX)) {
            
            stFptrt.nextToken();
            
            if ((stFptrt.sval == null) || (stFptrt.sval.trim().length() == 0)) {
                continue;
            }
            // Store the data into a temporary TreeSet object           
            tempString.add(stFptrt.sval);
        }
         
        // Store the total number of three-prefix
        sVar.nbThree = tempString.size();
        iter = tempString.iterator();               
            
        for (i = 0; iter.hasNext(); i++) {            
            tempS =  (String)iter.next(); 
       
            // Store the three-letter occupational suffixes into three_suff[]
            sVar.threeSuff[i] = new StringBuffer(tempS.trim());
        }
        
        /* Clear the temporary TreeSet and close the stream */
        tempString.clear(); 
        brFptrt.close();
               
 
	if (mDebug) {
	    mLogger.debug("Reading PersonName dashed first names file");
	}              
       /*
        * Open a BufferedReader/StreamTokenizer stream to read a list of words
        * that the standardizer replaces with blanks the list includes i.e.
        * 'Construction', but this is application dependent
        */
        readInTable(personStream[10], "Cannot open file of first names with dash");
        
       /*
        * Loop over every line of "blnk.dat". Test that the size is less that
        * the max authorized
        */
        while (!EmeUtil.feof(brFptrt) && (stFptrt.lineno() < NICKMAX)) {
            
            stFptrt.nextToken();
            
            if ((stFptrt.sval == null) || (stFptrt.sval.trim().length() == 0)) {
                continue;
            }
            // Store the data into a temporary TreeSet object           
            tempString.add(stFptrt.sval);
        }
         
        // Store the total number of dashed first names
        sVar.nbFirstDash = tempString.size();
        iter = tempString.iterator();               
            
        for (i = 0; iter.hasNext(); i++) {            
            tempS =  (String)iter.next(); 
            
            sVar.dashFirstName[i] = new StringBuffer(tempS.substring(0, 20).trim());            
            /* Store the standard name field into orig_name[] */
            sVar.nameDashGender[i] = new StringBuffer(tempS.substring(20, 21).trim());
        }
        
        /* Clear the temporary TreeSet and close the stream */
        tempString.clear(); 
        brFptrt.close();       

	if (mDebug) {
	    mLogger.debug("Reading PersonName special characters file");
	}                
       /*
        * Open a BufferedReader/StreamTokenizer stream to read a list of
        * special characters of type 1.
        */
        readInTable(personStream[11], "Cannot open file of special chars");
        
       /*
        * Loop over every line of "blnk.dat". Test that the size is less that
        * the max authorized
        */
        while (!EmeUtil.feof(brFptrt) && (stFptrt.lineno() < BLNKMAX)) {
            
            stFptrt.nextToken();
            
            if ((stFptrt.sval == null) || (stFptrt.sval.trim().length() == 0)) {
                continue;
            }
            // Store the data into a temporary TreeSet object           
            tempString.add(stFptrt.sval);
        }
         
        // Store the total number of special characters
        sVar.nbChars = tempString.size();
        iter = tempString.iterator();               
            
        for (i = 0; iter.hasNext(); i++) {            
            tempS =  (String)iter.next(); 

            sVar.specChars[i] = new String(tempS.trim());
        }
        
        /* Clear the temporary TreeSet and close the stream */
        tempString.clear();
        brFptrt.close();

	if (mDebug) {
	    mLogger.debug("Finished reding the PersonName data files!");
	}         

     
        return;
    }
    
    
    /**
     * Reads the table with name: fileName
     *
     * @param fileStream the stream from the repository
     * @param errM the error message
     * @throws SbmeStandardizationException
     * @throws IOException
     *
     */
    private void readInTable(InputStream fileStream, String errM)
    throws SbmeStandardizationException, IOException {
        
        int i = 0;
        brFptrt = openFile(fileStream, errM);
        stFptrt = new StreamTokenizer(brFptrt);
        
        /* Read only the record, not the end char. */
        wordCharsMinusEndLine(stFptrt);
    }
    
    
    /**
     * Open and return a stream from the argument file 'fileName'.
     *
     * @param stream the stream from the repository
     * @param errM
     * @throws SbmeStandardizationException
     * @throws IOException
     */
    private BufferedReader openFile(InputStream stream, String errM)
    throws SbmeStandardizationException, IOException {

        BufferedReader brF = null;
        
        if (stream != null) {
            brF = new BufferedReader(new InputStreamReader(stream));
            
        } else {
	        mLogger.error(errM);
            throw new SbmeStandardizationException(errM);
        }
        return brF;
    }
    
    
    /**
     *
     *
     * @param st
     * @throws SbmeStandardizationException
     */
    private static void wordCharsMinusSpace(StreamTokenizer st)
    throws SbmeStandardizationException {
        
           /* All characters are ordinary chars. */
        st.resetSyntax();
           /*
            * All characters from '\u0000' to '\u00FF' are regarded as word
            * constituents
            */
        st.wordChars('\u0000', '\u00FF');
           /* Only character ' ' is considered as white space */
        st.whitespaceChars(' ', ' ');
    }
    
    
    /**
     *
     *
     * @param st
     * @throws SbmeStandardizationException
     */
    private static void wordCharsMinusEndLine(StreamTokenizer st)
    throws SbmeStandardizationException {
        
        /* All characters are ordinary chars. */
        st.resetSyntax();
           /*
            * All characters from '\u0000' to '\u00FF' are regarded as word
            * constituents
            */
        st.wordChars('\u0000', '\u00FF');
        /* Only character ' ' is considered as white space */
        st.whitespaceChars('\n', '\n');
        st.whitespaceChars('\r', '\r');
    }
}
