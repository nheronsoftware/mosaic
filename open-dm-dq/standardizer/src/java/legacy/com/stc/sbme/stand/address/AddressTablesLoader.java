/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.stand.address;

import java.io.File;
import java.io.IOException;

import com.stc.sbme.api.SbmeMatchEngineException;
import com.stc.sbme.api.SbmeStandardizationException;
import com.stc.sbme.util.SbmeLogUtil;
import com.stc.sbme.util.SbmeLogger;
import com.stc.sbmeapi.StandConfigFilesAccess;

/**
 * Loads all the person name tables and assign the data to the 
 * corresponding variables
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
public class AddressTablesLoader {

    private AddressClueWordsTable clueWord;
    private AddressMasterCluesTable masterClues;
    private AddressPatternsTable patt;

    private final SbmeLogger mLogger = SbmeLogUtil.getLogger(this);  
    private final boolean mDebug = mLogger.isDebugEnabled();

    /**
     * Default constructor
     */
    public AddressTablesLoader(String domain) {

        clueWord = new AddressClueWordsTable(domain);
        masterClues = AddressMasterCluesTable.getInstance(domain);
        patt = AddressPatternsTable.getInstance(domain);
    }

    /**
     * Define a factory method that return an instance of the class 
     * @return an AddressClueWordsTable instance
     */
    public AddressClueWordsTable getClueWord() {
        return clueWord;
    }   


    /**
     *
     *
     * @param fileStream the stream from the repository
     * @param sVar an instance of AddressVariables
     * @throws SbmeStandardizationException exception
     * @throws SbmeMatchEngineException a generic match exception
     * @throws IOException exception
     */
    public void loadAddressTables(AddressStandVariables sVar, String domain, StandConfigFilesAccess fileAccess)
        throws SbmeStandardizationException, SbmeMatchEngineException, IOException {

        int i = 0;
        int j = 0;
        
        String addrClueWordsName = "";
        String addrMasterCluesName = "";
        String addrPatternsName = "";
        

        File fptrt = null;
        
        if (mDebug) {
            mLogger.debug("Reading the addressClueWordsTable file");
        }

        /**
         * Uploads the clueWords table
         *
         * @param sEng * 
         */
        if (!clueWord.loadClueWords(fileAccess.provideAddressClueAbbrevFile(domain), sVar, domain)) {
            
            mLogger.error("The addressClueWordsTable file couldn't be loaded!");
            throw new SbmeStandardizationException("File not found: clue_words couldn't be loaded!!!");
        }

        if (mDebug) {
            mLogger.debug("Reading the addressMasterCluesTable file");
        }

        /**
         * Uploads the MasterClueWords table
         *
         * @param sEng * 
         */
        if (!masterClues.loadMasterClues(fileAccess.provideAddressMasterCluesFile(domain), sVar, domain)) {

            mLogger.error("The addressMasterCluesTable file couldn't be loaded!");
            throw new SbmeStandardizationException("File not found: master_clues couldn't be loaded!!!");
        }

        if (mDebug) {
            mLogger.debug("Reading the addressPatternsTable file");
        }

        /**
         * Uploads the patterns table
         *
         * @param sEng * 
         */
        if (!patt.loadPatterns(fileAccess.provideAddressPatternsFile(domain), 
            fileAccess.provideAddressOutputPatternsFile(domain), sVar, domain)) {

            mLogger.error("The addressPatternsTable or the addressOutPatternsTable file couldn't be loaded!");
            throw new SbmeStandardizationException("File not found: patterns couldn't be loaded!!!");
        }
    }
}
