/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.stand.address;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.stc.sbme.api.SbmeMatchEngineException;
import com.stc.sbme.api.SbmeStandardizationException;
import com.stc.sbme.stand.util.ReadStandConstantsValues;
import com.stc.sbme.util.EmeUtil;
import com.stc.sbme.util.SbmeLogUtil;
import com.stc.sbme.util.SbmeLogger;

/**
 * Loads the address clue words table and assign the data to the
 * corresponding variables
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
class AddressClueWordsTable {

    private final int CLUE_ARRAYSIZE;
    private final int CLUE_RECLEN;
    private final int CLUE_HASHSIZE;

    private final SbmeLogger mLogger = SbmeLogUtil.getLogger(this);  
    private final boolean mDebug = mLogger.isDebugEnabled();

    private ClueTable[] entries;
    private StringBuffer image;
    private StringBuffer abbr;
    private StringBuffer cw1;
    private StringBuffer cw2;
    private StringBuffer cw3;
    private StringBuffer cw4;
    private StringBuffer cw5;
    private StringBuffer ct1;
    private StringBuffer ct2;
    private StringBuffer ct3;
    private StringBuffer ct4;
    private StringBuffer ct5;
    private StringBuffer state;
    private StringBuffer county;
    private StringBuffer place;
    private StringBuffer zip;
    private StringBuffer mincs;

    /**
     * Default constructor
     */
    AddressClueWordsTable(String domain) {

        CLUE_ARRAYSIZE = ReadStandConstantsValues.getInstance(domain).getClueArraySize();
        CLUE_RECLEN = ReadStandConstantsValues.getInstance(domain).getClueReclen();
        CLUE_HASHSIZE = ReadStandConstantsValues.getInstance(domain).getClueHashSize();
        entries = new ClueTable[CLUE_ARRAYSIZE];
        image = new StringBuffer(25);
        abbr = new StringBuffer(15);
        cw1 = new StringBuffer(5);
        cw2 = new StringBuffer(5);
        cw3 = new StringBuffer(5);
        cw4 = new StringBuffer(5);
        cw5 = new StringBuffer(5);
        ct1 = new StringBuffer(3);
        ct2 = new StringBuffer(3);
        ct3 = new StringBuffer(3);
        ct4 = new StringBuffer(3);
        ct5 = new StringBuffer(3);
        state = new StringBuffer(3);
        county = new StringBuffer(4);
        place = new StringBuffer(5);
        zip = new StringBuffer(6);
        mincs = new StringBuffer(4);
    }

    /**
     * Loads the data from the clue words table into the corresponding variables
     *
     * @param stream the clue words table stream
     * @param sVar an instance of the AddressVariables class
     * @return a boolean 
     * @throws SbmeStandardizationException an exception
     * @throws SbmeMatchEngineException a generic match exception
     * @throws IOException an exception
     */
    boolean loadClueWords(InputStream stream, AddressStandVariables sVar, String domain)
        throws SbmeStandardizationException, IOException, SbmeMatchEngineException {

        char lang;
        char usf;
        char storeFlag;
        int cnt = 0;
        int i;
        int len;
        boolean localeFR = domain.compareTo("FR") == 0;

        StringBuffer srec = new StringBuffer();

        ClueTable find;
        ClueTable ent;
        BufferedReader brFp = null;
        
        if (mDebug) {
            mLogger.debug("Reading the addressClueWordsTable file");
        }

        
        /* Test if the clue words table exists */
        if (stream != null) {
            brFp = new BufferedReader(new InputStreamReader(stream));
            
        } else {
            mLogger.fatal("The addressClueWordsTable file is not found");
            return false;
        }
     

        // Initialize entries to null
        for (i = 0; i < CLUE_ARRAYSIZE; i++) {
            entries[i] = null;
        }

        //read each record and load it into the hash table
        while (EmeUtil.fgets(brFp, srec, CLUE_RECLEN) != Boolean.FALSE) {

            cnt++;

            // Check for overflow
            if (cnt > CLUE_ARRAYSIZE) {

                mLogger.error("ERROR:  trying to load too many clues.");
                mLogger.error("        Max number that can be loaded: " + CLUE_ARRAYSIZE);
                return false;
            }

            // Handle cases where the line is empty
            // Trim on the left side
            len = srec.length();

            if (len == 0) {
                continue;
            }
            
            if (localeFR) {
                
                if (len > 33) {        
                    EmeUtil.strcpy(image, srec.substring(0, 34));
                }
                if (len > 47) {
                    EmeUtil.strcpy(abbr, srec.substring(34, 48));
                }
                if (len > 53) {
                    EmeUtil.strcpy(cw1, srec.substring(50, 54));
                }
                if (len > 55) {
                    EmeUtil.strcpy(ct1, srec.substring(54, 56));
                }
                if (len > 59) {
                    EmeUtil.strcpy(cw2, srec.substring(56, 60));
                }
                if (len > 61) {
                    EmeUtil.strcpy(ct2, srec.substring(60, 62));
                }
                if (len > 65) {
                    EmeUtil.strcpy(cw3, srec.substring(62, 66));
                }
                if (len > 67) {
                    EmeUtil.strcpy(ct3, srec.substring(66, 68));
                }
                if (len > 71) {
                    EmeUtil.strcpy(cw4, srec.substring(68, 72));
                }
                if (len > 73) {
                    EmeUtil.strcpy(ct4, srec.substring(72, 74));
                }
                if (len > 77) {
                    EmeUtil.strcpy(cw5, srec.substring(74, 78));
                }
                if (len > 79) {
                    EmeUtil.strcpy(ct5, srec.substring(78, 80));
                }
                if (len > 81) {
                    EmeUtil.strcpy(state, srec.substring(80, 82));
                }
                if (len > 84) {
                    EmeUtil.strcpy(county, srec.substring(82, 85));
                }
                if (len > 88) {
                    EmeUtil.strcpy(place, srec.substring(85, 89));
                }
                if (len > 93) {
                    EmeUtil.strcpy(zip, srec.substring(89, 94));
                } 
                usf = ' ';
                lang = ' ';
                storeFlag = ' ';
                                            
            } else {
            if (len > 23) {        
                EmeUtil.strcpy(image, srec.substring(0, 24));
            }
            if (len > 37) {
                EmeUtil.strcpy(abbr, srec.substring(24, 38));
            }
            if (len > 43) {
                EmeUtil.strcpy(cw1, srec.substring(40, 44));
            }
            if (len > 45) {
                EmeUtil.strcpy(ct1, srec.substring(44, 46));
            }
            if (len > 49) {
                EmeUtil.strcpy(cw2, srec.substring(46, 50));
            }
                if (len > 51) {
                EmeUtil.strcpy(ct2, srec.substring(50, 52));
            }
            if (len > 55) {
                EmeUtil.strcpy(cw3, srec.substring(52, 56));
            }
            if (len > 57) {
                EmeUtil.strcpy(ct3, srec.substring(56, 58));
            }
            if (len > 61) {
                EmeUtil.strcpy(cw4, srec.substring(58, 62));
            }
            if (len > 63) {
                EmeUtil.strcpy(ct4, srec.substring(62, 64));
            }
            if (len > 67) {
                EmeUtil.strcpy(cw5, srec.substring(64, 68));
            }
            if (len > 69) {
                EmeUtil.strcpy(ct5, srec.substring(68, 70));
            }
            if (len > 71) {
                EmeUtil.strcpy(state, srec.substring(70, 72));
            }
            if (len > 74) {
                EmeUtil.strcpy(county, srec.substring(72, 75));
            }
            if (len > 78) {
                EmeUtil.strcpy(place, srec.substring(75, 79));
            }
            if (len > 83) {
                EmeUtil.strcpy(zip, srec.substring(79, 84));
            }

            if (len > 86) {
                EmeUtil.strcpy(mincs, srec.substring(84, 87));
            }
            if (len > 38) {
                usf = srec.charAt(38);
            } else {
                usf = ' ';
            }
            if (len > 39) {
                lang = srec.charAt(39);
            } else {
                lang = ' ';
            }
            if (len > 87) {
                storeFlag = srec.charAt(87);
            } else {
                storeFlag = ' ';
            }
            }
            ent = addClue(usf, lang, storeFlag);
 
            // Re-initialize the Ids and types variables
            ct1.setLength(0);
            ct2.setLength(0);
            ct3.setLength(0);
            ct4.setLength(0);
            ct5.setLength(0);
            
            cw1.setLength(0);
            cw2.setLength(0);
            cw3.setLength(0);
            cw4.setLength(0);
            cw5.setLength(0);

            state.setLength(0);
            county.setLength(0);
            place.setLength(0);
            zip.setLength(0);
            mincs.setLength(0);

            srec.setLength(0);
            
        }


        /* Close the stream */
        brFp.close();
        return true;
    }


    /**
     * Performs the parsing of the input string
     *
     * @param usf 
     * @param lang
     * @param storeFlag
     * @return a ClueTable instance
     */
    private ClueTable addClue(char usf, char lang, char storeFlag) 
        throws SbmeStandardizationException {

        ClueTable temp, find;

        int val;

        // Verify if the clue word exist within the string. If not
        if ((temp = searchForClue(image, state, zip)) == null) {

            temp = new ClueTable();

            if ((temp == null) || ((temp.setName(stringCopy(image))) == null)
                || (temp.setTranslation(stringCopy(abbr)) == null)
                || (temp.setUsageFlag(usf) == '\0')
                || (temp.setLanguage(lang) == '\0')
                || (temp.setClueWordId1(stringCopy(cw1)) == null)
                || (temp.setClueType1(stringCopy(ct1)) == null)
                || (temp.setClueWordId2(stringCopy(cw2)) == null)
                || (temp.setClueType2(stringCopy(ct2)) == null)
                || (temp.setClueWordId3(stringCopy(cw3)) == null)
                || (temp.setClueType3(stringCopy(ct3)) == null)
                || (temp.setClueWordId4(stringCopy(cw4)) == null)
                || (temp.setClueType4(stringCopy(ct4)) == null)
                || (temp.setClueWordId5(stringCopy(cw5)) == null)
                || (temp.setClueType5(stringCopy(ct5)) == null)
                || (temp.setState(stringCopy(state)) == null)
                || (temp.setCounty(stringCopy(county)) == null)
                || (temp.setPlace(stringCopy(place)) == null)
                || (temp.setZip(stringCopy(zip)) == null)
                || (temp.setStoreFlag(storeFlag) == '\0')
                || (temp.setMinComparScore(stringCopy(mincs)) == null)) {

                return null;
            }

            // SET THE NEW ENTRY
            temp.setNext(null);  
                 
            val = clueHash(image);
            
            if (entries[val] == null) {
                entries[val] = temp;
                
            } else {
            
                for (find = entries[val]; find.getNext() != null; find = find.getNext()) {
                    continue;
                }
                
                find.setNext(temp);
            }
            
        } else {
        // name FOUND
            mLogger.error(image + " already exists in the clue table");
            throw new SbmeStandardizationException(image + " already exists in the clue hash table");
        }
        return temp;
    }


    /**
     * Search for the clue word inside the string str
     *
     * @param str the input string
     * @param state the state code
     * @param zip the zip code
     * @return a ClueTable instance
     */
    ClueTable searchForClue(StringBuffer str, StringBuffer state,
                                   StringBuffer zip) {

        ClueTable temp;

        int ln1;
        int ln2;

        // Walk along the linked list
        for (temp = entries[clueHash(str)]; temp != null; temp = temp.getNext()) {

            ln1 = trimln(str);
            ln2 = trimln(temp.getName());
            
            if ((EmeUtil.strncmp(str, temp.getName(), ln1) == 0) && (ln1 == ln2)) {

/*
                if ((EmeUtil.strncmp(temp.getState(), "  ", 2) != 0)
                    && (EmeUtil.strncmp(temp.getState(), state, 2) != 0)) {

                    continue;
                }

                if ((EmeUtil.strncmp(temp.getZip(), "  ", 2) != 0)
                    && (EmeUtil.strncmp(temp.getZip(), zip, 2) != 0)) {

                    continue;
                }
*/
                return temp;
            }
        }

        // If not found, then return NULL
        return null;
    }


    /**
     * Performs triming of blank characters at the ends of the string
     *
     * @param str the string from which we trail blanks
     * @return an integer 
     */
    static int trimln(StringBuffer str) {

        int i;
        int len = str.length();
        
        for (i = len - 1; i >= 0; i--) {
            if (!Character.isWhitespace(str.charAt(i))) {
                return i + 1;
            }
        }

        return 0;
    }


    /**
     * Performs triming of blank characters at the end of a substring of 
     * the original string
     *
     * @param str the string to be parsed
     * @param i the index of the start of the substring
     * @return an integer 
     */
    static int trimln(StringBuffer str, int ind) {

        int i;
        String s = str.substring(ind);
        int len = s.length();

        for (i = len - 1; i >= 0; i--) {
            if (!Character.isWhitespace(s.charAt(i))) {
                return i + 1;
            }
        }

        return 0;
    }


    /**
     * Calculates the integer hash value for the string, str, by summing up the
     * ascii values for each character in the string and returning the remainder
     * of that value divided by the maximum hash size macro, CLUE_HASHSIZE.
     *
     * @param str the string from which we trail blanks
     * @return an integer 
     */
    private int clueHash(StringBuffer str) {

        int val;
        int len;
        int i;
        double ll;

        /* Length of the string */
        int strLen = str.length();

        /* Remove the trailing blanks */
        len = trimln(str);


        for (val = 0, i = 0; (i < strLen) && (i < len); i++) {
            val = str.charAt(i) + (31 * val);
        }

        if (val < 0) {
            return EmeUtil.unsignedIntRemainder(val, CLUE_HASHSIZE);
        }
     
        return (val % CLUE_HASHSIZE);
    }

    /**
     * Simply returns a copy of the string string.
     *
     * @param str the string from which we trail blanks
     * @return a StringButfer instance
     */
    private StringBuffer stringCopy(StringBuffer string) {
    
        StringBuffer temp;
        int len;

        if ((len = trimln(string)) == 0) {
            len = string.length();
        }

        temp = new StringBuffer(len);


        if (temp != null) {
            EmeUtil.strncpy(temp, string, len);
        }

        return temp;
    }
}
