/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.api;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * The SbmePersonName represents and manipulates any person name record that
 * has been broken down into its basic components
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
public class SbmePersonName
implements SbmeStandRecord {
    // Added for capturing pattern signature
    private String signature;
    public String getSignature() { return signature; }
    public void setSignature(String signature) { this.signature = signature; }    
    /**
     * Define field name associated with a person name
     */
    public static final String FIRSTNAME = "FirstName";
    /**
     * Define field name associated with a middle name
     */
    public static final String MIDDLENAME = "MiddleName";
    /**
     * Define field name associated with a last name
     */
    public static final String LASTNAME = "LastName";
    /**
     * Define field name associated with a title
     */
    public static final String TITLE = "Title";
    /**
     * Define field name associated with a gender
     */
    public static final String GENDER = "Gender";
    /**
     * Define field name associated with a generational suffix
     */
    public static final String GENSUFFIX = "GenSuffix";
    /**
     * Define field name associated with a occupational suffix
     */
    public static final String OCCUPSUFFIX = "OccupSuffix";
    /**
     * Define field name associated with a last name prefix
     */
    public static final String LASTPREFIX = "LastNamePrefix";
    /**
     * Define field name associated with a person name
     */
    public static final String BUSINESSORRELATED = "BusinessOrRelated";
    /**
     * Define field name associated with a business or related terms
     */
    public static final String PERSONSTATUS = "PersonStatus";
    /**
     * Define field name associated with unknown terms
     */
    public static final String UNKNOWNTYPE = "Unknown";
    
    
    /**
     * Used in the standardizationEngine class to identify the type of records
     */
    public static final String PERSONNAME = "PersonName";
    
    /*
     * Holds keys-values of the different fields
     */
    private HashMap personFieldName;
    
    /*
     * Holds keys-values of the status of the different fields
     */
    private HashMap personFieldStatus;
    
    /*
     * Holds keys-values of the precision of the different records
     */
    private HashMap personRecordAccuracy;
    
    /**
     * Public constructor
     */
    public SbmePersonName() {
        personFieldName = new HashMap();
        personFieldStatus = new HashMap();
        personRecordAccuracy = new HashMap();
    }
    
    /**
     * Getters used to identify
     *
     * @return the type of data (person name)
     */
    public String getType() {
        return PERSONNAME;
    }
    
    /**
     *
     * @return the first name field
     */
    public String getFirstName() {
        return (String) personFieldName.get(FIRSTNAME);
    }
    
    /**
     *
     * @return the last name field
     */
    public String getLastName() {
        return (String) personFieldName.get(LASTNAME);
    }
    
    /**
     *
     * @return the middle name field
     */
    public String getMiddleName() {
        return (String) personFieldName.get(MIDDLENAME);
    }
    
    /**
     *
     * @return the title field
     */
    public String getTitle() {
        return (String) personFieldName.get(TITLE);
    }
    
    /**
     *
     * @return the generational suffix field
     */
    public String getGenSuffix() {
        return (String) personFieldName.get(GENSUFFIX);
    }
    
    /**
     *
     * @return the person's status field
     */
    public String getPersonStatus() {
        return (String) personFieldName.get(PERSONSTATUS);
    }
    
    /**
     *
     * @return the person's gender field
     */
    public String getGender() {
        return (String) personFieldName.get(GENDER);
    }
    
    /**
     *
     * @return the occupational suffix field
     */
    public String getOccupSuffix() {
        return (String) personFieldName.get(OCCUPSUFFIX);
    }
    
    /**
     *
     * @return the last name prefix field
     */
    public String getLastPrefix() {
        return (String) personFieldName.get(LASTPREFIX);
    }
    
    /**
     *
     * @return the business or any related field(s)
     */
    public String getBizOrRelated() {
        return (String) personFieldName.get(BUSINESSORRELATED);
    }
    
    /**
     *
     * @return any unknown type(s)
     */
    public String getUnknownType() {
        return (String) personFieldName.get(UNKNOWNTYPE);
    }
    
    
    /**
     *
     * @param value the first name field
     */
    public void setFirstName(String value) {
        personFieldName.put(FIRSTNAME, value);
    }
    
    /**
     *
     * @param value the last name field
     */
    public void setLastName(String value) {
        personFieldName.put(LASTNAME, value);
    }
    
    /**
     *
     * @param value the middle name field
     */
    public void setMiddleName(String value) {
        personFieldName.put(MIDDLENAME, value);
    }
    
    /**
     *
     * @param value the title field
     */
    public void setTitle(String value) {
        personFieldName.put(TITLE, value);
    }
    
    /**
     *
     * @param value the generational suffix field
     */
    public void setGenSuffix(String value) {
        personFieldName.put(GENSUFFIX, value);
    }
    
    /**
     *
     * @param value the person's gender field
     */
    public void setGender(String value) {
        personFieldName.put(GENDER, value);
    }
    
    /**
     *
     * @param value the occupational suffix field
     */
    public void setOccupSuffix(String value) {
        personFieldName.put(OCCUPSUFFIX, value);
    }
    
    /**
     *
     * @param value the last name prefix field
     */
    public void setLastPrefix(String value) {
        personFieldName.put(LASTPREFIX, value);
    }
    
    /**
     *
     * @param value the business or related term(s)
     */
    public void setBizOrRelated(String value) {
        personFieldName.put(BUSINESSORRELATED, value);
    }
    
    /**
     *
     * @param value the unknown field(s)
     */
    public void setUnknownType(String value) {
        personFieldName.put(UNKNOWNTYPE, value);
    }
    
    
    /**
     *
     * @param key the first name field
     * @return the key's value
     */
    public String getValue(String key) {
        return (String) personFieldName.get(key);
    }
    
    /**
     *
     * @param key the first name field
     * @param value the corresponding value
     */
    public void setValue(String key, String value) {
        personFieldName.put(key, value);
    }

    /**
     * Generic getter method for fields' names.
     * @param key the field's type
     * @return a string array
     */
    public List getValues(String key) {
        return (ArrayList) personFieldName.get(key);
    }
    
    /**
     * Generic setter method for fields' names.
     * @param key the generic key
     * @param values the corresponding array of values
     */
    public void setValues(String key, List values) {
        personFieldName.put(key, values);
    }

    
    /**
     * Generic getter method for fields' status.
     *
     * @param key the first name field
     * @return the last name prefix field
     */
    public String getStatusValue(String key) {
        return (String) personFieldStatus.get(key);
    }
    
    
    /**
     * Generic setter method for fields' status.
     *
     * @param key the status key
     * @param value the coresponding value
     */
    public void setStatusValue(String key, String value) {
        personFieldStatus.put(key, value);
    }
    
    
    /**
     *
     * @return personFieldName.keySet() the set of all the object's keys
     */
    public Set getAllFields() {
        return personFieldName.keySet();
    }
    
    
    /**
     * This method returns all the statuses values associated with the fields
     * in a given PersonName record as a Collection.
     *
     * @return personFieldStatus.values() the set of all the statuses of the
     *         object's fields
     */
    public Collection getAllStatusValues() {
        return personFieldStatus.values();
    }
    
    
    /**
     * This method returns all the names associated with the fields in a
     * given PersonName record as a Collection.
     *
     * @return personFieldName.values() the values associated with the object's keys
     */
    public Collection getAllNameValues() {
        return personFieldName.values();
    }
    
    
    /**
     *
     * @param object the SbmeStandRecord instance
     * @return (int) personRecordAccuracy the degree of accuracy of a standardization
     *          process
     */
    public int getStandDegreeOfAccuracy(SbmeStandRecord object) {
        return ((Integer) personRecordAccuracy.get(object)).intValue();
    }
    
    
    /**
     *
     * @param object the SbmeStandRecord instance
     * @param value the degree of precision of the standardization
     */
    public void setStandDegreeOfAccuracy(SbmeStandRecord object, Integer value) {
        personRecordAccuracy.put(object, value);
    }
}
