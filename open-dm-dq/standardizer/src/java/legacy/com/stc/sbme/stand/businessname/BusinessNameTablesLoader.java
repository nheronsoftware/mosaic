/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.stand.businessname;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StreamTokenizer;
import java.util.Iterator;
import java.util.TreeMap;
import java.util.TreeSet;

import com.stc.sbme.api.SbmeMatchEngineException;
import com.stc.sbme.api.SbmeStandardizationException;
import com.stc.sbme.stand.util.DataFilesValidator;
import com.stc.sbme.util.EmeUtil;
import com.stc.sbme.util.SbmeLogUtil;
import com.stc.sbme.util.SbmeLogger;

/**
 * Loads all the person name tables and assign the data to the
 * corresponding variables
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
public class BusinessNameTablesLoader {

        /* */
    private BufferedReader brFptrt;
        /* */
    private StreamTokenizer stFptrt;
    
    private final SbmeLogger mLogger = SbmeLogUtil.getLogger(this);  
    private final boolean mDebug = mLogger.isDebugEnabled();

    /**
     * Loads and read the adequate tables' content into variables from the
     * BusinessStandVariables
     *
     * @param businessStream a stream to the data files
     * @param sVar an instance of the BusinessStandVariables class
     * @throws SbmeStandardizationException the generic stand exception
     * @throws SbmeMatchEngineException an exception
     * @throws IOException java i/o exception
     */
    public void loadBusinessNameTables(InputStream[] businessStream, BusinessStandVariables sVar)
        throws SbmeStandardizationException, SbmeMatchEngineException, IOException {
        
        int i = 0;
        int j = 0;
        TreeSet tempString = new TreeSet();
        TreeMap hm = new TreeMap();
        String tempS;
        
        if (mDebug) {
            mLogger.debug("Reading the Business Organization-type keywords file");
        }
       /*
        * Open a stream to read a list of organization-type fields of the types: 
        * (Corporation, Ltd...)
        */        
        readInTable(businessStream[0], "Organization-type keywords' file not found");
        
       /* Loop over every line of file */
        while (!EmeUtil.feof(brFptrt)) {
            
            stFptrt.nextToken(); 
            if ((stFptrt.sval == null) || (stFptrt.sval.trim().length() == 0)) {
                continue;
            }
            // Store the data into a temporary TreeSet object           
            tempString.add(stFptrt.sval);
        } 
        // Ensure that the list of Organization-type keywords has unique key names and that there
        // is only one level of mapping
        tempString = DataFilesValidator.enforceOneLevelMapping(tempString, 20);
        
        /* Total number of rows */
        sVar.nbOrgTypeFields = tempString.size();
        Iterator iter = tempString.iterator();
        
        for (i = 0; iter.hasNext(); i++) {
            
            tempS =  (String)iter.next();      
            // Store the organization type field into variable orgTypeFields           
            sVar.orgTypeField[i] = tempS.substring(0, 20).trim();
            //  store the standardized form of the field into orgTypeStandFields
            sVar.orgTypeStandField[i] = tempS.substring(20).trim();
        }      
        
             
            // Store the organization type field into variable orgTypeFields           
//            sVar.orgTypeField[i] = stFptrt.sval.substring(0, 20).trim();

            //  store the standardized form of the field into orgTypeStandFields
//            sVar.orgTypeStandField[i] = stFptrt.sval.substring(20).trim();
            
            /* Increment the count */
//            i++;
//        }        
                
        /* Store the total number of organization-type fields */
//        sVar.nbOrgTypeFields = i;
        // Empty the TreeSet object for next use
        tempString.clear();
        brFptrt.close();



        /* Reset the index and close the stream.*/
//        i = 0;

        if (mDebug) {
            mLogger.debug("Reading the Business Association-type keywords file");
        }
        
       /*
        * Open a stream to read a list of association-type words of the types:
        * (Group, Associates...)
        */
        readInTable(businessStream[1], "Association-type keywords' file not found");
        
       /* Loop over every line of file */
        while (!EmeUtil.feof(brFptrt)) {
            
            stFptrt.nextToken(); 
            if ((stFptrt.sval == null) || (stFptrt.sval.trim().length() == 0)) {
                continue;
            }
            // Store the data into a temporary TreeSet object           
            tempString.add(stFptrt.sval);
        }
        // Ensure that the list of Organization-type keywords has unique key names and that there
        // is only one level of mapping
        tempString = DataFilesValidator.enforceOneLevelMapping(tempString, 20);
        
        /* Total number of rows */
        sVar.nbAssocTypeFields = tempString.size();
        iter = tempString.iterator();
        
        for (i = 0; iter.hasNext(); i++) {
            tempS =  (String)iter.next();  
                
            // Store the organization type field into variable orgTypeFields           
            sVar.assocTypeField[i] = tempS.substring(0, 20).trim();
            //  store the standardized form of the field into orgTypeStandFields
            sVar.assocTypeStandField[i] = tempS.substring(20).trim();
        }
                                        
            // Store the organization type field into variable orgTypeFields
//            sVar.assocTypeField[i] = stFptrt.sval.substring(0, 20).trim();
            
            //  store the standardized form of the field into orgTypeStandFields
//            sVar.assocTypeStandField[i] = stFptrt.sval.substring(20).trim();
            
            /* Increment the count */
//            i++;
//        }
        
        /* Store the total number of organization-type fields */
//        sVar.nbAssocTypeFields = i;
        // Empty the TreeSet object for next use
        tempString.clear();
        brFptrt.close();                


        /* Reset the index and close the stream.*/
//        i = 0;
        

        if (mDebug) {
            mLogger.debug("Reading the Business Industry-type keywords file");
        }
        
       /*
        * Open a stream to read a list of industry-related words of the types:
        * (Aerospace, Medical...)
        */
        readInTable(businessStream[2], "Industry-type keywords' file not found");
        
       /* Loop over every line of file */
        while (!EmeUtil.feof(brFptrt)) {
            
            stFptrt.nextToken();
            if ((stFptrt.sval == null) || (stFptrt.sval.trim().length() == 0)) {
                continue;
            }
            // Store the data into a temporary TreeSet object           
            tempString.add(stFptrt.sval);
        }
        // Ensure that the list of Organization-type keywords has unique key names and that there
        // is only one level of mapping
        tempString = DataFilesValidator.enforceOneLevelMapping(tempString, 20, 40);
        
        /* Total number of rows */
        sVar.nbIndustryTypeFields = tempString.size();
        iter = tempString.iterator();
        
        for (i = 0; iter.hasNext(); i++) {
            tempS =  (String)iter.next();  
                
            // Store the industry-related field into variable industryTypeFields
            sVar.industryTypeField.add(i, tempS.substring(0, 20).trim());
            
            //  store the standardized form of the field into industryTypeStandFields
            sVar.industryTypeStandField[i] = tempS.substring(20, 40).trim();

            //  store the standardized form of the field into industryTypeStandFields
            sVar.industrySector[i] = new StringBuffer(tempS.substring(40).trim());                       
        }                            
        
        /* Store the total number of industry-related fields */
        brFptrt.close();
        // Empty the TreeSet object for next use
        tempString.clear();  


        if (mDebug) {
            mLogger.debug("Reading the Business Categories code file");
        }
        
       /*
        * Open a stream to read a list of industry categories' code:
        * (001-01  Aerospace/Defense - Maintenance & Service...)
        */
        readInTable(businessStream[3], "Categories code file not found");
        
       /* Loop over every line of file */
        while (!EmeUtil.feof(brFptrt)) {
            
            stFptrt.nextToken();
            if ((stFptrt.sval == null) || (stFptrt.sval.trim().length() == 0)) {
                continue;
            }
            // Store the data into a temporary TreeSet object           
            tempString.add(stFptrt.sval);
        }
        
        /* Total number of rows */
        sVar.nbCategoryCodes = tempString.size();
        iter = tempString.iterator();
        
        for (i = 0; iter.hasNext(); i++) {
            tempS =  (String)iter.next();                  
                                    
            // Store the industry-related field into variable industryTypeFields
            sVar.categoryCode.add(i, tempS.substring(0, 15).trim());
            
            //  store the standardized form of the field into industryTypeStandFields
            sVar.categoryName.add(i, tempS.substring(15).trim());
        }
        
        /* Store the total number of industry-related fields */
        brFptrt.close();
        // Empty the TreeSet object for next use
        tempString.clear();  
        

        if (mDebug) {
            mLogger.debug("Reading the Business Adjective-type keywords file");
        }
        
       /*
        * Open a stream to read a list of adjective-type words of the types:
        * (United, Advanced...)
        */
        readInTable(businessStream[4], "Adjective-type keywords' file not found");
        
       /* Loop over every line of file */
        while (!EmeUtil.feof(brFptrt)) {
            
            stFptrt.nextToken();
            if ((stFptrt.sval == null) || (stFptrt.sval.trim().length() == 0)) {
                continue;
            }
            // Store the data into a temporary TreeSet object           
            tempString.add(stFptrt.sval);
        }

        /* Total number of rows */
        sVar.nbAdjTypeFields = tempString.size();
        iter = tempString.iterator();
        
        for (i = 0; iter.hasNext(); i++) {
            tempS =  (String)iter.next();                  
                                                      
            // Store the adjective-type field into variable adjTypeFields
            sVar.adjTypeField[i] = tempS.trim();           
        }
        
        /* Store the total number of industry-related fields */
        brFptrt.close();
        // Empty the TreeSet object for next use
        tempString.clear();  
        
        
        /* Reset the index and close the stream.*/
        i = 0;

        if (mDebug) {
            mLogger.debug("Reading the company names aliases file");
        }
        
       /*
        * Open a stream to read a list of aliases words of the types:
        * (IBM, CIMA...)
        */
        readInTable(businessStream[5], "company names aliases' file not found");
        
       /* Loop over every line of file */
        while (!EmeUtil.feof(brFptrt)) {
            
            stFptrt.nextToken();
            if ((stFptrt.sval == null) || (stFptrt.sval.trim().length() == 0)) {
                continue;
            }
            // Store the data into a temporary TreeSet object           
            tempString.add(stFptrt.sval);
        }
        
        /* Total number of rows */
        sVar.nbAliasTypes = tempString.size();
        iter = tempString.iterator();
        
        for (i = 0; iter.hasNext(); i++) {
            tempS =  (String)iter.next(); 
                                                      
            // Store aliases fields into variable 
            sVar.aliasTypeField.add(i, tempS.substring(0, 30).trim());

            //  store the standardized form of the field into aliasTypeStandFields
            sVar.aliasTypeStandField.add(i, tempS.substring(30).trim());
        }
        
        /* Store the total number of industry-related fields */
        brFptrt.close();
        // Empty the TreeSet object for next use
        tempString.clear();    
              
              
        if (mDebug) {
            mLogger.debug("Reading the merged company names file");
        }
        
       /*
        * Open a stream to read a list of business names with embedded slash:
        * (Dr Pepper/Seven Up...)
        */
        readInTable(businessStream[6], "Merged companies primary names' file not found");
        
       /* Loop over every line of file */
        while (!EmeUtil.feof(brFptrt)) {
            
            stFptrt.nextToken();

            if ((stFptrt.sval == null) || (stFptrt.sval.trim().length() == 0)) {
                continue;
            }
            
            // Store the data into a temporary TreeSet object           
            tempString.add(stFptrt.sval);
        }   
        
        /* Total number of rows */
        sVar.nbPrimarySlashNames = tempString.size();
        iter = tempString.iterator();
        
        for (i = 0; iter.hasNext(); i++) {
            tempS =  (String)iter.next(); 
                             
            // Store  fields into variable
            sVar.primarySlashName[i] = new StringBuffer(tempS.substring(0, 50).trim());            
            //  store the standardized form of the field into aliasTypeStandFields
            sVar.industrySector2[i] = new StringBuffer(tempS.substring(50).trim());
        }
     
        brFptrt.close();
        // Empty the TreeSet object for next use
        tempString.clear();

  
        if (mDebug) {
            mLogger.debug("Reading the company primary names file");
        }
        
       /*
        * Open a stream to read a list of business names with embeeded slash:
        * (SeeBeyond...)
        */
        readInTable(businessStream[7], "company primary names' file not found");
        
       /* Loop over every line of file */
        while (!EmeUtil.feof(brFptrt)) {
            
            stFptrt.nextToken();

            if ((stFptrt.sval == null) || (stFptrt.sval.trim().length() == 0)) {
                continue;
            }
            // Store the data into a temporary TreeSet object           
            tempString.add(stFptrt.sval);
        }

        /* Total number of rows */
        sVar.nbPrimaryNames = tempString.size();
        iter = tempString.iterator();

        for (i = 0; iter.hasNext(); i++) {
            tempS =  (String)iter.next(); 
                                        
            // Store  fields into variable
            sVar.primaryName[i] = tempS.substring(0, 50).trim();         
            //  store the standardized form of the field into aliasTypeStandFields
            sVar.industrySector1[i] = new StringBuffer(tempS.substring(50).trim());            
        }

        brFptrt.close();
        // Empty the TreeSet object for next use
        tempString.clear();


        if (mDebug) {
            mLogger.debug("Reading the business common terms file");
        }
        
       /*
        * Open a stream to read a list of business names with embeeded slash:
        * (SeeBeyond...)
        */
        readInTable(businessStream[8], "business common terms' file not found");
        
       /* Loop over every line of file */
        while (!EmeUtil.feof(brFptrt)) {
            
            stFptrt.nextToken();

            if ((stFptrt.sval == null) || (stFptrt.sval.trim().length() == 0)) {
                continue;
            }
            
            // Store the data into a temporary TreeSet object           
            tempString.add(stFptrt.sval);
        }     
        
        /* Total number of rows */
        sVar.nbBizCommonTerms = tempString.size();
        iter = tempString.iterator();

        for (i = 0; iter.hasNext(); i++) {
            tempS =  (String)iter.next();         
                      
            // Store  fields into variable
            sVar.bizCommonTerm.add(i, tempS.trim());
        }
        
        brFptrt.close();
        // Empty the TreeSet object for next use
        tempString.clear();
       
        
        if (mDebug) {
            mLogger.debug("Reading the Country Codes file");
        }
        
       /*
        * Open a stream to read a list of business names with embeeded slash:
        * (SeeBeyond...)
        */
        readInTable(businessStream[9], "Country Codes file not found");
        
       /* Loop over every line of file */
        while (!EmeUtil.feof(brFptrt)) {
            
            stFptrt.nextToken();

            if ((stFptrt.sval == null) || (stFptrt.sval.trim().length() == 0)) {
                continue;
            }
            // Store the data into a temporary TreeSet object           
            tempString.add(stFptrt.sval);
        }
        
        /* Total number of rows */
        sVar.nbCountryCodes = tempString.size();
        iter = tempString.iterator();

        for (i = 0; iter.hasNext(); i++) {
            tempS =  (String)iter.next(); 
                    
            // Store  fields into variable
            sVar.countryStandName.add(i, tempS.substring(0, 30).trim());            
            //  store the standardized form of the field into aliasTypeStandFields
            sVar.countryCode[i] = tempS.substring(30, 35).trim();
            //  store the standardized form of the field into aliasTypeStandFields
            sVar.nationality.add(i, tempS.substring(35).trim());
        }
    
        brFptrt.close();
        // Empty the TreeSet object for next use
        tempString.clear();


        if (mDebug) {
            mLogger.debug("Reading the City-State Codes file");
        }
        
       /*
        * Open a stream to read a list of business names with embeeded slash:
        * (SeeBeyond...)
        */
        readInTable(businessStream[10], "City-State Codes file not found");
        
       /* Loop over every line of file */
        while (!EmeUtil.feof(brFptrt)) {
            
            stFptrt.nextToken();

            if ((stFptrt.sval == null) || (stFptrt.sval.trim().length() == 0)) {
                continue;
            }
            // Store the data into a temporary TreeSet object           
            tempString.add(stFptrt.sval);
        }
        
        /* Total number of rows */
        sVar.nbCityStateCodes = tempString.size();
        iter = tempString.iterator();

        for (i = 0; iter.hasNext(); i++) {
            tempS =  (String)iter.next(); 

            // Store  fields into variable
            sVar.cityOrStateName.add(i, tempS.substring(0, 25).trim());
            //  store the standardized form of the field into aliasTypeStandFields
            sVar.cityOrStateIdentifier[i] = tempS.substring(25, 30).trim();
            //  store the standardized form of the field into aliasTypeStandFields
            sVar.cityOrStateCode[i] = tempS.substring(30).trim();
        }
        
        brFptrt.close();
        // Empty the TreeSet object for next use
        tempString.clear();
        
        
        if (mDebug) {
            mLogger.debug("Reading the Companies former names file");
        }
        
       /*
        * Open a stream to read a list of business names with embeeded slash:
        * (...)
        */
        readInTable(businessStream[11], "Companies former names file not found");
        
       /* Loop over every line of file */
        while (!EmeUtil.feof(brFptrt)) {
            
            stFptrt.nextToken();

            if ((stFptrt.sval == null) || (stFptrt.sval.trim().length() == 0)) {
                continue;
            }
            // Store the data into a temporary TreeSet object           
            tempString.add(stFptrt.sval);
        }
        
        /* Total number of rows */
        sVar.nbCompanyFormerNames = tempString.size();
        iter = tempString.iterator();

        for (i = 0; iter.hasNext(); i++) {
            tempS =  (String)iter.next();
                                
            // Store  fields into variable
            sVar.companyFormerName.add(i, tempS.substring(0, 40).trim());
            //  store the standardized form of the field into aliasTypeStandFields
            sVar.companyNewName.add(i, tempS.substring(40).trim());
        }
        
        brFptrt.close();
        // Empty the TreeSet object for next use
        tempString.clear();
       

        if (mDebug) {
            mLogger.debug("Reading the business Connector tokens file");
        }
        
       /*
        * Open a stream to read a list of business names with embeeded slash:
        * (...)
        */
        readInTable(businessStream[12], "Connector tokens file not found");
        
       /* Loop over every line of file */
        while (!EmeUtil.feof(brFptrt)) {
            
            stFptrt.nextToken();

            if ((stFptrt.sval == null) || (stFptrt.sval.trim().length() == 0)) {
                continue;
            }
            // Store the data into a temporary TreeSet object           
            tempString.add(stFptrt.sval);
        }
        
        /* Total number of rows */
        sVar.nbConnectorTokens = tempString.size();
        iter = tempString.iterator();

        for (i = 0; iter.hasNext(); i++) {
            tempS =  (String)iter.next();
                                
            // Store  fields into variable
            sVar.connectorToken[i] = tempS.trim();
        }
        
        brFptrt.close();
        // Empty the TreeSet object for next use
        tempString.clear();
        
      
        if (mDebug) {
            mLogger.debug("Reading the business patterns file");
        }
        
       /*
        * Open a stream to read a list of business names with embeeded slash:
        * (...)
        */
        readInTable(businessStream[13], "Business name patterns' file not found");
        
       /* Loop over every line of file */
        while (!EmeUtil.feof(brFptrt)) {
        
            stFptrt.nextToken();
            tempS = stFptrt.sval;
            if ((tempS == null) || (tempS.trim().length() == 0)) {
                continue;
            }
            stFptrt.nextToken();
            
            hm.put(tempS, stFptrt.sval);                   
        }
        
        /* Total number of rows */
        sVar.nbPatterns = hm.size();

        i = 0;
        while (hm.size() > 0) {
            tempS = (String)hm.firstKey();
            
            // Store  fields into variable
            sVar.inputPattern[i] = tempS.trim();            
            //  store the standardized form of the field into aliasTypeStandFields
            sVar.outputPattern[i] = ((String)hm.get(tempS)).trim();
            
            // Remove the mapping for this key
            hm.remove(tempS);
            i++;         
        }       
        brFptrt.close();
          

        if (mDebug) {
            mLogger.debug("Reading the business Special characters file");
        }
        
       /*
        * Open a stream to read a list of business names with embeeded slash:
        * (...)
        */
        readInTable(businessStream[14], "Special characters' file not found");
        
       /* Loop over every line of file */
        while (!EmeUtil.feof(brFptrt)) {
            
            stFptrt.nextToken();

            if ((stFptrt.sval == null) || (stFptrt.sval.trim().length() == 0)) {
                continue;
            }
            // Store the data into a temporary TreeSet object           
            tempString.add(stFptrt.sval);
        }
        
        /* Total number of rows */
        sVar.nbSpecChars = tempString.size();
        iter = tempString.iterator();

        for (i = 0; iter.hasNext(); i++) {
            tempS =  (String)iter.next();
                                
            // Store  fields into variable
            sVar.specChars[i] = tempS.trim();
        }
        
        brFptrt.close();
        // Empty the TreeSet object for next use
        tempString.clear();
        

        if (mDebug) {
            mLogger.debug("Finished reading the business data files!");
        }

    }
    
    
    /**
     * Reads the table with name: fileName
     *
     * @param fileStream the stream from the repository
     * @param errM the error message
     * @throws SbmeStandardizationException
     * @throws IOException
     *
     */
    private void readInTable(InputStream fileStream, String errM)
    throws SbmeStandardizationException, IOException {
        
        int i = 0;
        brFptrt = openFile(fileStream, errM);
        stFptrt = new StreamTokenizer(brFptrt);
        
        /* Read only the record, not the end char. */
        wordCharsMinusEndLine(stFptrt);
    }
    
    
    /**
     * Open and return a stream from the argument file 'fileName'.
     *
     * @param stream the stream from the repository
     * @param errM
     * @throws SbmeStandardizationException
     * @throws IOException
     */
    private BufferedReader openFile(InputStream stream, String errM)
    throws SbmeStandardizationException, IOException {

        BufferedReader brF = null;

        if (stream != null) {
            brF = new BufferedReader(new InputStreamReader(stream));
            
        } else {
            mLogger.error(errM);
            throw new SbmeStandardizationException(errM);
        }
        return brF;
    }


    /**
     *
     *
     * @param st a stream tokenizer
     * @throws  a SbmeStandardizationException exception
     */
    private static void wordCharsMinusEndLine(StreamTokenizer st)
    throws SbmeStandardizationException {
        
        /* All characters are ordinary chars. */
        st.resetSyntax();
           /*
            * All characters from '\u0000' to '\u00FF' are regarded as word
            * constituents
            */
        st.wordChars('\u0000', '\u00FF');
                /* Only character ' ' is considered as white space */
        st.whitespaceChars('\n', '\n');
        st.whitespaceChars('\r', '\r');
    }
}
