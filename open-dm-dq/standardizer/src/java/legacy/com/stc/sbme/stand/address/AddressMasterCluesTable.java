/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.stand.address;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

import com.stc.sbme.api.SbmeMatchEngineException;
import com.stc.sbme.api.SbmeStandardizationException;
import com.stc.sbme.stand.util.ReadStandConstantsValues;
import com.stc.sbme.util.EmeUtil;
import com.stc.sbme.util.SbmeLogUtil;
import com.stc.sbme.util.SbmeLogger;


/**
 * Loads the address clue words table and assign the data to the
 * corresponding variables
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
class AddressMasterCluesTable {

    private final SbmeLogger mLogger = SbmeLogUtil.getLogger(this);  
    private final boolean mDebug = mLogger.isDebugEnabled();
    private static final SbmeLogger LOGGER = SbmeLogUtil.getLogger("com.stc.sbme.stand.address.AddressMasterCluesTable");
    private final static HashMap INSTANCES = new HashMap();
    private boolean loadedOnce = false;

    private Master[] masterCluesTY;
    private Master[] masterCluesAU;
    private Master[] masterCluesBU;
    private Master[] masterCluesDR;
    private Master[] masterCluesEI;
    private Master[] masterCluesEX;
    private Master[] masterCluesEN;    
    private Master[] masterCluesMP;
    private Master[] masterCluesFC;
    private Master[] masterCluesOT;
    private Master[] masterCluesBX;
    private Master[] masterCluesRR;
    private Master[] masterCluesWD;
    private Master[] masterCluesWI;
    private Master[] masterCluesAM;
    private Master[] masterCluesSA;
    private Master[] masterCluesUR;
    private Master[] masterCluesDM;
    private Master[] masterCluesHR;    
    private Master[] masterCluesBP;    
    private Master[] masterCluesCN;       

    private final int MASTER_CLUE_RECLEN;
    
    private final int MASTER_ARRAYSIZE_TY;
    private final int MASTER_ARRAYSIZE_AU;
    private final int MASTER_ARRAYSIZE_BU;
    private final int MASTER_ARRAYSIZE_DR;
    private final int MASTER_ARRAYSIZE_EI;
    private final int MASTER_ARRAYSIZE_EX;
    private final int MASTER_ARRAYSIZE_EN;    
    private final int MASTER_ARRAYSIZE_MP;
    private final int MASTER_ARRAYSIZE_FC;
    private final int MASTER_ARRAYSIZE_OT;
    private final int MASTER_ARRAYSIZE_BX;
    private final int MASTER_ARRAYSIZE_RR;
    private final int MASTER_ARRAYSIZE_WD;
    private final int MASTER_ARRAYSIZE_WI;
    private final int MASTER_ARRAYSIZE_AM;
    private final int MASTER_ARRAYSIZE_SA;
    private final int MASTER_ARRAYSIZE_UR;  
    private final int MASTER_ARRAYSIZE_DM;       
    private final int MASTER_ARRAYSIZE_HR; 
    private final int MASTER_ARRAYSIZE_BP;                         
    private final int MASTER_ARRAYSIZE_CN;                         
    
    static synchronized AddressMasterCluesTable getInstance(String domain) {
        AddressMasterCluesTable table = (AddressMasterCluesTable) INSTANCES.get(domain);
        if (table == null) {
            table = new AddressMasterCluesTable(domain);
            INSTANCES.put(domain, table);
        }
        return table;
    }

    /**
     * The default constructor 
     */
    private AddressMasterCluesTable(String domain) {

        MASTER_CLUE_RECLEN = ReadStandConstantsValues.getInstance(domain).getMasterClueReclen();
    
        MASTER_ARRAYSIZE_TY = ReadStandConstantsValues.getInstance(domain).getMasterArraySizeTY();
        MASTER_ARRAYSIZE_AU = ReadStandConstantsValues.getInstance(domain).getMasterArraySizeAU();
        MASTER_ARRAYSIZE_BU = ReadStandConstantsValues.getInstance(domain).getMasterArraySizeBU();
        MASTER_ARRAYSIZE_DR = ReadStandConstantsValues.getInstance(domain).getMasterArraySizeDR();
        MASTER_ARRAYSIZE_EI = ReadStandConstantsValues.getInstance(domain).getMasterArraySizeEI();
        MASTER_ARRAYSIZE_EX = ReadStandConstantsValues.getInstance(domain).getMasterArraySizeEX();
        MASTER_ARRAYSIZE_EN = ReadStandConstantsValues.getInstance(domain).getMasterArraySizeEN();
        MASTER_ARRAYSIZE_MP = ReadStandConstantsValues.getInstance(domain).getMasterArraySizeMP();
        MASTER_ARRAYSIZE_FC = ReadStandConstantsValues.getInstance(domain).getMasterArraySizeFC();
        MASTER_ARRAYSIZE_OT = ReadStandConstantsValues.getInstance(domain).getMasterArraySizeOT();
        MASTER_ARRAYSIZE_BX = ReadStandConstantsValues.getInstance(domain).getMasterArraySizeBX();
        MASTER_ARRAYSIZE_RR = ReadStandConstantsValues.getInstance(domain).getMasterArraySizeRR();
        MASTER_ARRAYSIZE_WD = ReadStandConstantsValues.getInstance(domain).getMasterArraySizeWD();
        MASTER_ARRAYSIZE_WI = ReadStandConstantsValues.getInstance(domain).getMasterArraySizeWI();
        MASTER_ARRAYSIZE_AM = ReadStandConstantsValues.getInstance(domain).getMasterArraySizeAM();
        MASTER_ARRAYSIZE_SA = ReadStandConstantsValues.getInstance(domain).getMasterArraySizeSA();
        MASTER_ARRAYSIZE_UR = ReadStandConstantsValues.getInstance(domain).getMasterArraySizeUR();        
        MASTER_ARRAYSIZE_DM = ReadStandConstantsValues.getInstance(domain).getMasterArraySizeDM();       
        MASTER_ARRAYSIZE_HR = ReadStandConstantsValues.getInstance(domain).getMasterArraySizeHR();       
        MASTER_ARRAYSIZE_BP = ReadStandConstantsValues.getInstance(domain).getMasterArraySizeBP();       
        MASTER_ARRAYSIZE_CN = ReadStandConstantsValues.getInstance(domain).getMasterArraySizeCN();       
        
        masterCluesTY = new Master[MASTER_ARRAYSIZE_TY];
        masterCluesAU = new Master[MASTER_ARRAYSIZE_AU];
        masterCluesBU = new Master[MASTER_ARRAYSIZE_BU];
        masterCluesDR = new Master[MASTER_ARRAYSIZE_DR];
        masterCluesEI = new Master[MASTER_ARRAYSIZE_EI];
        masterCluesEX = new Master[MASTER_ARRAYSIZE_EX];
        masterCluesEN = new Master[MASTER_ARRAYSIZE_EN];        
        masterCluesMP = new Master[MASTER_ARRAYSIZE_MP];
        masterCluesFC = new Master[MASTER_ARRAYSIZE_FC];
        masterCluesOT = new Master[MASTER_ARRAYSIZE_OT];
        masterCluesBX = new Master[MASTER_ARRAYSIZE_BX];
        masterCluesRR = new Master[MASTER_ARRAYSIZE_RR];
        masterCluesWD = new Master[MASTER_ARRAYSIZE_WD];
        masterCluesWI = new Master[MASTER_ARRAYSIZE_WI];
        masterCluesAM = new Master[MASTER_ARRAYSIZE_AM];
        masterCluesSA = new Master[MASTER_ARRAYSIZE_SA];
        masterCluesUR = new Master[MASTER_ARRAYSIZE_UR];
        masterCluesDM = new Master[MASTER_ARRAYSIZE_DM];
        masterCluesHR = new Master[MASTER_ARRAYSIZE_HR]; 
        masterCluesBP = new Master[MASTER_ARRAYSIZE_BP];               
        masterCluesCN = new Master[MASTER_ARRAYSIZE_CN];                 
    }

    /**
     * Loads the data from the clue woeds table into the corresponding variables
     *
     * @param stream the clue words table stream
     * @param sVar an instance of the AddressVariables class
     * @return a boolean 
     * @throws SbmeStandardizationException an exception
     * @throws SbmeMatchEngineException a generic match exception
     * @throws IOException an exception
     */
    synchronized boolean loadMasterClues(InputStream stream, AddressStandVariables sVar, String domain)
        throws SbmeStandardizationException, SbmeMatchEngineException, IOException {

        if (loadedOnce == true) {
            return true;
        }   
        int cnt = 0;
        int tid;
        int retval;
        int len;
        boolean localeFR = (domain.compareTo("FR") == 0);

        StringBuffer sRec = new StringBuffer(100);

        Master tst;

        BufferedReader brFp = null;

        if (mDebug) {
            mLogger.debug("Reading the addressMasterCluesTable file");
        }

        /* Test if the clue words table exists */
        if (stream != null) {
            brFp = new BufferedReader(new InputStreamReader(stream));
            
        } else {
            mLogger.fatal("The addressMasterCluesTable file is not found");
            return false;
        }


        //read each record and load it into the hash table
        try {
            while (EmeUtil.fgets(brFp, sRec, MASTER_CLUE_RECLEN) != Boolean.FALSE) {


                // Trim on the left side
                len = sRec.length();

                // Handle cases where the line is empty
                if (len == 0) {
                    continue;
                }

                cnt++;

                tst = new Master();

                if (len > 3) {
                    tst.setClueWordId(new StringBuffer(sRec.substring(0, 4)));
                }
                if (localeFR) {
                    
                    if (len > 37) {
                        tst.setFullName(new StringBuffer(sRec.substring(4, 38)));
                    }                                       
                    if (len > 51) {
                        tst.setStdAbbrev(new StringBuffer(sRec.substring(38, 52)));
                    }
                    if (len > 57) {
                        tst.setShortAbbrev(new StringBuffer(sRec.substring(52, 58)));
                    }
                    if (len > 61) {
                        tst.setUspsAbbrev(new StringBuffer(sRec.substring(58, 62)));
                    }
                    if (len > 69) {
                        tst.setValidCfccs(new StringBuffer(sRec.substring(62, 70)));
                    } 
                    if (len > 71) {
                        tst.setClueType(new StringBuffer(sRec.substring(70, 72)));
                    }

                    tst.setUsageFlag(' ');
                    tst.setUspsFlag(' ');
                    tst.setForLangFlag(' ');
                                        
                } else {                
                if (len > 27) {
                    tst.setFullName(new StringBuffer(sRec.substring(4, 28)));

                }
                if (len > 41) {
                    tst.setStdAbbrev(new StringBuffer(sRec.substring(28, 42)));
                }
                if (len > 47) {
                    tst.setShortAbbrev(new StringBuffer(sRec.substring(42, 48)));
                }
                if (len > 51) {
                    tst.setUspsAbbrev(new StringBuffer(sRec.substring(48, 52)));
                }
                if (len > 59) {
                    tst.setValidCfccs(new StringBuffer(sRec.substring(52, 60)));
                }

                if (len > 61) {
                    tst.setClueType(new StringBuffer(sRec.substring(60, 62)));
                }

                if (len > 62) {
                    tst.setUsageFlag(sRec.charAt(62));
                } else {
                    tst.setUsageFlag(' ');
                }

                if (len > 63) {
                    tst.setUspsFlag(sRec.charAt(63));
                } else {
                    tst.setUspsFlag(' ');
                }

                if (len > 64) {
                    tst.setForLangFlag(sRec.charAt(64));
                } else {
                    tst.setForLangFlag(' ');
                }

                if (len > 88) {
                    tst.setTranslation(new StringBuffer(sRec.substring(65, 89)));
                    }
                }

                tid = Integer.parseInt(tst.getClueWordId().toString().trim());

                if (EmeUtil.strcmp(tst.getClueType(), "TY") == 0) {

                    if (test(tid, "TY", MASTER_ARRAYSIZE_TY)) {
                        masterCluesTY[tid] = tst;

                    } else {
                        return false;
                    }

                } else if (EmeUtil.strcmp(tst.getClueType(), "AU") == 0) {

                    if (test(tid, "AU", MASTER_ARRAYSIZE_AU)) {
                        masterCluesAU[tid] = tst;

                    } else {
                        return false;
                    }

                } else if (EmeUtil.strcmp(tst.getClueType(), "BU") == 0) {

                    if (test(tid, "BU", MASTER_ARRAYSIZE_BU)) {
                        masterCluesBU[tid] = tst;

                    } else {
                        return false;
                    }

                } else if (EmeUtil.strcmp(tst.getClueType(), "DR") == 0) {

                    if (test(tid, "DR", MASTER_ARRAYSIZE_DR)) {
                        masterCluesDR[tid] = tst;

                    } else {
                        return false;
                    }

                } else if (EmeUtil.strcmp(tst.getClueType(), "EI") == 0) {

                    if (test(tid, "EI", MASTER_ARRAYSIZE_EI)) {
                        masterCluesEI[tid] = tst;

                    } else {
                        return false;
                    }

                } else if (EmeUtil.strcmp(tst.getClueType(), "EX") == 0) {

                    if (test(tid, "EX", MASTER_ARRAYSIZE_EX)) {
                        masterCluesEX[tid] = tst;

                    } else {
                        return false;
                    }
                } else if (EmeUtil.strcmp(tst.getClueType(), "EN") == 0) {

                    if (test(tid, "EN", MASTER_ARRAYSIZE_EN)) {
                        masterCluesEN[tid] = tst;

                    } else {
                        return false;
                    }
                } else if (EmeUtil.strcmp(tst.getClueType(), "MP") == 0) {

                    if (test(tid, "MP", MASTER_ARRAYSIZE_MP)) {
                        masterCluesMP[tid] = tst;

                    } else {
                        return false;
                    }

                } else if (EmeUtil.strcmp(tst.getClueType(), "FC") == 0) {

                    if (test(tid, "FC", MASTER_ARRAYSIZE_FC)) {
                        masterCluesFC[tid] = tst;

                    } else {
                        return false;
                    }

                } else if (EmeUtil.strcmp(tst.getClueType(), "OT") == 0) {

                    if (test(tid, "OT", MASTER_ARRAYSIZE_OT)) {
                        masterCluesOT[tid] = tst;

                    } else {
                        return false;
                    }

                } else if (EmeUtil.strcmp(tst.getClueType(), "BX") == 0) {

                    if (test(tid, "BX", MASTER_ARRAYSIZE_BX)) {
                        masterCluesBX[tid] = tst;

                    } else {
                        return false;
                    }

                } else if (EmeUtil.strcmp(tst.getClueType(), "RR") == 0) {

                    if (test(tid, "RR", MASTER_ARRAYSIZE_RR)) {
                        masterCluesRR[tid] = tst;

                    } else {
                        return false;
                    }

                } else if (EmeUtil.strcmp(tst.getClueType(), "WD") == 0) {

                    if (test(tid, "WD", MASTER_ARRAYSIZE_WD)) {
                        masterCluesWD[tid] = tst;

                    } else {
                        return false;
                    }

                } else if (EmeUtil.strcmp(tst.getClueType(), "WI") == 0) {

                    if (test(tid, "WI", MASTER_ARRAYSIZE_WI)) {
                        masterCluesWI[tid] = tst;

                    } else {
                        return false;
                    }

                } else if (EmeUtil.strcmp(tst.getClueType(), "AM") == 0) {

                    if (test(tid, "AM", MASTER_ARRAYSIZE_AM)) {
                        masterCluesAM[tid] = tst;

                    } else {
                        return false;
                    }

                } else if (EmeUtil.strcmp(tst.getClueType(), "SA") == 0) {

                    if (test(tid, "SA", MASTER_ARRAYSIZE_SA)) {
                        masterCluesSA[tid] = tst;

                    } else {
                        return false;
                    }

                } else if (EmeUtil.strcmp(tst.getClueType(), "UR") == 0) {

                    if (test(tid, "UR", MASTER_ARRAYSIZE_UR)) {
                        masterCluesUR[tid] = tst;

                    } else {
                        return false;
                    }

                } else if (EmeUtil.strcmp(tst.getClueType(), "HR") == 0) {
                    if (localeFR) {
                        if (test(tid, "HR", MASTER_ARRAYSIZE_HR)) {
                            masterCluesHR[tid] = tst;  
                        } else {
                            return false;
                        }
                    } else {
                        if (test(tid, "TY", MASTER_ARRAYSIZE_TY)) {
                            masterCluesTY[tid] = tst;  
                        } else {
                            return false;
                        }                        
                    }

                } else if (EmeUtil.strcmp(tst.getClueType(), "BP") == 0) {

                    if (localeFR) {
                        if (test(tid, "BP", MASTER_ARRAYSIZE_BP)) {
                            masterCluesBP[tid] = tst;
                        } else {
                            return false;
                        }
                    } else {
                        if (test(tid, "BU", MASTER_ARRAYSIZE_BU)) {
                            masterCluesBU[tid] = tst;
                        } else {
                            return false;
                        }
                    }
                } else if (EmeUtil.strcmp(tst.getClueType(), "DM") == 0) {

                    if (test(tid, "DM", MASTER_ARRAYSIZE_DM)) {
                        masterCluesDM[tid] = tst;
                    } else {
                        return false;
                    }             
                } else if (EmeUtil.strcmp(tst.getClueType(), "CN") == 0) {

                    if (test(tid, "CN", MASTER_ARRAYSIZE_CN)) {
                        masterCluesCN[tid] = tst;
                    } else {
                        return false;
                    }     
                } else {
                    mLogger.error("ERROR: clue type " + tst.getClueType()
                        + " doesn't exist.");

                    return false;
                }
            }
        } catch (RuntimeException e) {
            mLogger.error("Can not read line: " + (cnt + 1) + " of master address clue file", e);
            throw e;
        }

        /* Close the stream */
        brFp.close();
        // Set the load once to true to prevent multiple threads from reloading
        loadedOnce = true;         
        return true;
    }


    /**
     * 
     *
     * @param tid aaa
     * @param s aaa
     * @param mas aaa
     * @return a boolean
     */
    private boolean test(int tid, String s, int mas) {

        String sa = "MASTER_ARRAYSIZE_" + s;

        if (tid > mas) {
            mLogger.error("ERROR:  trying to load too many " + sa + " master clues.");
            mLogger.error("        Highest ID that can be loaded:" + mas);
            mLogger.error("                    Trying to load ID: " + tid);
            return false;
        }
        return true;
    }


    /**
     *
     * @param id aaa
     * @param type aaa
     * @return an instance of the MasterClue class
     */
    protected Master searchForMasterEntry(int id, StringBuffer type, String domain) {
        boolean localeFR = (domain.compareTo("FR") == 0);

        if (EmeUtil.strcmp(type, "TY") == 0) {
            return (masterCluesTY[id]);
            
        } else if (EmeUtil.strcmp(type, "WD") == 0) {
            return masterCluesWD[id];
            
        } else if (EmeUtil.strcmp(type, "WI") == 0) {
            return masterCluesWI[id];
            
        } else if (EmeUtil.strcmp(type, "OT") == 0) {
            return masterCluesOT[id];
            
        } else if (EmeUtil.strcmp(type, "RR") == 0) {
            return masterCluesRR[id];
            
        } else if (EmeUtil.strcmp(type, "FC") == 0) {
            return masterCluesFC[id];
            
        } else if (EmeUtil.strcmp(type, "BX") == 0) {
            return masterCluesBX[id];
            
        } else if (EmeUtil.strcmp(type, "AU") == 0) {
            return masterCluesAU[id];
            
        } else if (EmeUtil.strcmp(type, "BU") == 0) {
             return masterCluesBU[id];
             
        } else if (EmeUtil.strcmp(type, "DR") == 0) {
            return masterCluesDR[id];
            
        } else if (EmeUtil.strcmp(type, "EI") == 0) {
            return masterCluesEI[id];
            
        } else if (EmeUtil.strcmp(type, "EX") == 0) {
            return masterCluesEX[id];
            
        } else if (EmeUtil.strcmp(type, "EN") == 0) {
            return masterCluesEN[id];
        } else if (EmeUtil.strcmp(type, "MP") == 0) {
            return masterCluesMP[id];
            
        } else if (EmeUtil.strcmp(type, "AM") == 0) {
            return masterCluesAM[id];
            
        } else if (EmeUtil.strcmp(type, "SA") == 0) {
            return masterCluesSA[id];
            
        } else if (EmeUtil.strcmp(type, "UR") == 0) {
            return masterCluesUR[id];
            
        } else if (EmeUtil.strcmp(type, "HR") == 0) {
            if (localeFR) {
                return masterCluesHR[id];
            } else {
                return masterCluesTY[id];
            }
            
        } else if (EmeUtil.strcmp(type, "BP") == 0) {
            if (localeFR) {
                return masterCluesBP[id];
            } else {
                return masterCluesBU[id];
            }
            
        } else if (EmeUtil.strcmp(type, "PT") == 0) {
            return masterCluesTY[id];
            
        } else if (EmeUtil.strcmp(type, "DM") == 0) {
            return masterCluesDM[id];            
        } else if (EmeUtil.strcmp(type, "CN") == 0) {
            return masterCluesCN[id];                       
        } else {
            LOGGER.error("ERROR: clue type " + type + " doesn't exist");
            return null;
        }
    }
}
