/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.stand.businessname;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Loads the address clue words table and assign the data to the
 * corresponding variables
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
public class PopulateBizStandRecords {
    

    /**
     * Define a factory method that return an instance of the class
     * @return an PopulateBizStandRecords instance
     */
    public static PopulateBizStandRecords getInstance() {
        return new PopulateBizStandRecords();
    }

    /** 
     * 
     * 
     * @param standBiz aaa
     * @param orderedKeys aaa 
     * @param orderedValues aaa
     * @return an integer
     */ 
    public static int copyFields(StandBusinessName standBiz, StringBuffer[][] orderedKeys,
                                 StringBuffer[][] orderedValues, BusinessStandVariables sVar) {
        
        // If the StandBusinessName object is null return
        if (standBiz == null) { 
            return 0;
        }

        /* Generic string */
        StringBuffer strB;
        StringBuffer strB1;

        int i;      
        int k = 0;
        int j = 0;
        int jk = 0;
        int js;
        ArrayList al;
        
        sVar.indSectorF = false;
        sVar.aliasListF = false;
        
        /* Read from field primary company name */
        strB1 = standBiz.getPrimaryName();

        if (!(strB1.toString().compareTo("") == 0)) {

            orderedKeys[0][k] = new StringBuffer("PrimaryName");
            orderedValues[0][k] = new StringBuffer(strB1.toString());
            
            /* free the string and increment the index*/
            k++;
        }
 
        
        /* Read from field organization type key */
        strB = standBiz.getOrgTypeKeyword();

        if (!(strB.toString().compareTo("") == 0)) {

            orderedKeys[0][k] = new StringBuffer("OrgTypeKeyword");
            orderedValues[0][k] = new StringBuffer(strB.toString());
            
            /* free the string and increment the index*/
            strB.setLength(0);
            k++;
        }
 
        
        /* Read from field organization type key */
        strB = standBiz.getAssocTypeKeyword();

        if (!(strB.toString().compareTo("") == 0)) {
        
            orderedKeys[0][k] = new StringBuffer("AssocTypeKeyword");
            orderedValues[0][k] = new StringBuffer(strB.toString());
            
            /* free the string and increment the index*/
            strB.setLength(0);
            k++;
        }
        
        
        /* Read from field organization type key */
        strB = standBiz.getIndustryTypeKeyword();

        if (!(strB.toString().compareTo("") == 0)) {
        
            orderedKeys[0][k] = new StringBuffer("IndustryTypeKeyword");
            orderedValues[0][k] = new StringBuffer(strB.toString());
            
            /* free the string and increment the index*/
            strB.setLength(0);
            k++;
        }
        
        
        /* Read from field organization type key */
        strB = standBiz.getUrl();

        if (!(strB.toString().compareTo("") == 0)) {
        
            orderedKeys[0][k] = new StringBuffer("Url");
            orderedValues[0][k] = new StringBuffer(strB.toString());
            
            /* free the string and increment the index*/
            strB.setLength(0);
            k++;
        }

        /* Read from field organization type key */
        strB = standBiz.getNotFoundType();

        if (!(strB.toString().compareTo("") == 0)) {


            if ((strB1.toString().compareTo("") == 0)) {
                orderedKeys[0][k] = new StringBuffer("PrimaryName");
                orderedValues[0][k] = new StringBuffer(strB.toString());                
                
            } else {
                orderedKeys[0][k] = new StringBuffer("NotFoundType");
                orderedValues[0][k] = new StringBuffer(strB.toString());                
            }
            
            /* free the string and increment the index*/
            strB.setLength(0);
            k++;
            
        }


        /* Read from field industry list sector */
        al = standBiz.getIndustrySectorList();
        sVar.numberOfSectorInList = al.size();
 
        if (sVar.numberOfSectorInList > 0) {
        
            orderedKeys[0][k] = new StringBuffer("IndustrySectorList");
            sVar.indSectorF = true;

            k++;
            j++;
        }

        /* Read from field organization type key */
        sVar.numberOfAliasesInList = ((HashMap)standBiz.getAliasList().get(0)).size();

        if (sVar.numberOfAliasesInList > 0) {
        
            orderedKeys[0][k] = new StringBuffer("AliasList");
            sVar.aliasListF = true;

            k++;
            j++;
        }

        //
        sVar.intV = j;

        return k;
    }
}
