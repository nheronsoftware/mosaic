/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.util;

//import com.stc.eindex.objects.ObjectNode;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A logger that exposes the same "interface" as the Log4J logger but in fact logs to a
 * java.util.logging.Logger delegate. It can be used to make migration from the log4j
 * package to the java.util.logging package easier.
 *
 * @author Frank Kieviet
 * @version $Revision: 1.1.2.1 $
 */
public final class SbmeLogger {
    private final Logger mDelegate;
    private static final String LOG_PREFIX = "STC.SBME.";
    
    private SbmeLogger(Logger delegate) {
        mDelegate = delegate;
    }

    /**
     * See {@link org.apache.log4j.Logger#getLogger}
     *
     * @param name name of the logger
     * @return Logger instance
     */
    public static SbmeLogger getLogger(String name) {
        return new SbmeLogger(Logger.getLogger(name));
    }

    /**
     * See {@link org.apache.log4j.Logger#}
     *
     * @param clazz Class whose name is to be used as the logger name
     * @return Logger instance
     */
    public static SbmeLogger getLogger(Class clazz) {
        return getLogger(clazz.getName());
    }


    /**
     * See {@link org.apache.log4j.Category#debug}
     *
     * @param message msg to be logged
     */
    public final void debug(Object message) {
        String s = null;
        if (message != null) {
            s = message.toString();
        }
        mDelegate.log(Level.FINE, s);
    }

    /**
     * See {@link org.apache.log4j.Category#debug}
     *
     * @param message msg to be logged
     * @param t exception
     */
    public final void debug(Object message, Throwable t) {
        String s = null;
        if (message != null) {
            s = message.toString();
        }
        mDelegate.log(Level.FINE, s, t);
    }

    /**
     * See {@link org.apache.log4j.Category#err}
     *
     * @param message msg to be logged
     */
    public final void error(Object message) {
        String s = null;
        if (message != null) {
            s = message.toString();
        }
        mDelegate.log(Level.SEVERE, s);
    }

    /**
     * See {@link org.apache.log4j.Category#err}
     *
     * @param message msg to be logged
     * @param t exception to be logged
     */
    public final void error(Object message, Throwable t) {
        String s = null;
        if (message != null) {
            s = message.toString();
        }
        mDelegate.log(Level.SEVERE, s, t);
    }

    /**
     * See {@link org.apache.log4j.Category#fatal}
     *
     * @param message msg to be logged
     */
    public final void fatal(Object message) {
        String s = null;
        if (message != null) {
            s = message.toString();
        }
        mDelegate.log(Level.SEVERE, s);
    }

    /**
     * See {@link org.apache.log4j.Category#fatal}
     *
     * @param message msg to be logged
     * @param t exception to be logged
     */
    public final void fatal(Object message, Throwable t) {
        String s = null;
        if (message != null) {
            s = message.toString();
        }
        mDelegate.log(Level.SEVERE, s, t);
    }

    /**
     * See {@link org.apache.log4j.Category#info}
     *
     * @param message msg to be logged
     */
    public final void info(Object message) {
        String s = null;
        if (message != null) {
            s = message.toString();
        }
        mDelegate.log(Level.INFO, s);
    }

    /**
     * See {@link org.apache.log4j.Category#info}
     *
     * @param message msg to be logged
     * @param t exception to be logged
     */
    public final void info(Object message, Throwable t) {
        String s = null;
        if (message != null) {
            s = message.toString();
        }
        mDelegate.log(Level.INFO, s, t);
    }

    /**
     * See {@link org.apache.log4j.Category#isDebugEnabled}
     *
     * @return if debug logging is enabled
     */
    public final boolean isDebugEnabled() {
        return mDelegate.isLoggable(Level.FINE);
    }

    /**
     * See {@link org.apache.log4j.Category#warn}
     *
     * @param message msg to be logged
     */
    public final void warn(Object message) {
        String s = null;
        if (message != null) {
            s = message.toString();
        }
        mDelegate.log(Level.WARNING, s);
    }

    /**
     * See {@link org.apache.log4j.Category#warn}
     *
     * @param message msg to be logged
     * @param t exception to be logged
     */
    public final void warn(Object message, Throwable t) {
        String s = null;
        if (message != null) {
            s = message.toString();
        }
        mDelegate.log(Level.WARNING, s, t);
    }

    /**
     * See {@link org.apache.log4j.Category#getName}
     *
     * @return String
     */
    public final String getName() {
        return mDelegate.getName();
    }

    /**
     * See {@link org.apache.log4j.Category#getLevel}
     *
     * @return Level
     */
    public final Level getLevel() {
        return mDelegate.getLevel();
    }

    /**
     * See {@link org.apache.log4j.Category#getResourceBundle}
     *
     * @return ResourceBundle
     */
    public final ResourceBundle getResourceBundle() {
        return mDelegate.getResourceBundle();
    }

    /**
     * See {@link org.apache.log4j.Category#isEnabledFor}
     *
     * @param level msg to be logged
     * @return boolean
     */
    public final boolean isEnabledFor(Level level) {
        return mDelegate.isLoggable(level);
    }

    /**
     * See {@link org.apache.log4j.Category#isInfoEnabled}
     *
     * @return boolean
     */
    public final boolean isInfoEnabled() {
        return mDelegate.isLoggable(Level.INFO);
    }

    /**
     * See {@link org.apache.log4j.Category#setLevel}
     *
     * @param level msg to be logged
     */
    public final void setLevel(Level level) {
        mDelegate.setLevel(level);
    }
}
