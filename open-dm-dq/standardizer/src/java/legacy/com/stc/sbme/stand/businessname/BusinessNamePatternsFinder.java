/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.stand.businessname;

import java.util.ArrayList;
import java.util.StringTokenizer;

import com.stc.sbme.api.SbmeStandardizationException;
import com.stc.sbme.util.SbmeLogUtil;
import com.stc.sbme.util.SbmeLogger;

/**
 *
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
public class BusinessNamePatternsFinder {

    private final SbmeLogger mLogger = SbmeLogUtil.getLogger(this);  
    private final boolean mDebug = mLogger.isDebugEnabled();

    /**
     * A factory method that return an instance of the class
     * @return a BusinessNamePatternsFinder instance
     */
    public static BusinessNamePatternsFinder getInstance() {
        return new BusinessNamePatternsFinder();
    }

    /**
     * Search for the best pattern (or patterns if more than one person is in a
     * string) for a one-string person name. Note that such pattern is domain-
     * independent because the structure of names is quasi-universal.
     * The returned values are a subset of the values in fieldList, dictated by the
     * patterns in the pattern table.
     *
     * @param orderedKeys the set of ordered keys
     * @param fieldList the list of patterns found
     * @param sVar an instance of the BusinessStandVariables class that holds
     *             the shared variables in all the classes
     * @return a 2D array of strings
     */
    public String[][] findPatterns(String[][] orderedKeys, ArrayList fieldList,
    BusinessStandVariables sVar) {

        /* implementation */
        return null;
    }

    /**
     * Search for the best patterns (we can have more than one pattern per string)
     * for an array of strings representing person names. The returned values are
     * a subset of the values in fieldList dictated by the pattern table.
     *
     * @param fieldsType hold the set of keys that define a business, and provides
     *                    the best order of the keys from the pattern table
     *                    (following the census approach)
     * @param fieldsList aaa
     * @param sVar an instance of the BusinessStandVariables class that holds
     *             the shared variables in all the classes
     * @return a 3D array of strings
     * @throws SbmeStandardizationException an exception
     */
    public StandBusinessName getBestPatterns(ArrayList fieldsType, ArrayList
    fieldsList,
                                             BusinessStandVariables sVar)
        throws SbmeStandardizationException {

        /* Number of tokens inside the record */
        int numberOfKeys = fieldsType.size();

        int firstChar;
        int numberOfInputPatterns = 0;
        int i;
        int j = 0;
        int t = 0;
        int pattSize;
        int theIndex;
        int numberOfTypes;

        int k = 0;
        boolean notFound = true;
        boolean noPatternFound = false;
        boolean differentPatt = false;

        ArrayList inputPatt = new ArrayList();
        ArrayList outputPatt = new ArrayList();
        ArrayList pattIndex = new ArrayList();
        String str;

        StringBuffer nFound = new StringBuffer();

        /*
         * First, search for the patterns that have the same number of tokens
         * as the input string fieldsType
         */
        for (i = 0; i < sVar.nbPatterns; i++) {

            firstChar = Integer.parseInt(sVar.inputPattern[i].substring(0, 1));

            // Search for patterns with size numberOfKeys
            if (firstChar == numberOfKeys) {

                inputPatt.add(j, sVar.inputPattern[i]);
                outputPatt.add(j, sVar.outputPattern[i]);

                /* Increment the list */
                j++;

                notFound = false;

            } else if (!notFound) {
                           
                break;
            }
        }

        /* Count the number of patterns that have the same number of token as fieldsType*/
        numberOfInputPatterns = inputPatt.size();

        /* Handle the case where no pattern is found */
        if (numberOfInputPatterns == 0) {
            noPatternFound = true;
        }


        // Loop over all the input patterns and compare them with the types of the string
        for (i = 0; i < numberOfInputPatterns; i++) {

            /* Search for a list of types in inputPattern that matches our list of types */
            str = ((String) inputPatt.get(i)).substring(2);

            /* Separate the string into a list of types */
            StringTokenizer stok = new StringTokenizer(str);

            /*
             * Count the number of types inside the string to insure
             * that there is no mistake
             */
            numberOfTypes = stok.countTokens();

            if (numberOfTypes != numberOfKeys) {
            
                mLogger.error("There is a problem with the number of keys"
                    + " in the output patterns!");

                throw new SbmeStandardizationException("There is a problem with the number of keys" 
                    + " in the output patterns!");
            }

            // Search for the corresponding pattern in the patterns table
            for (j = 0; j < numberOfTypes; j++) {

                String s = stok.nextToken();

                // compare the types
                if (s.compareTo((String) fieldsType.get(j)) == 0) {
                    differentPatt = false;
                } else {

                    differentPatt = true;
                    break;
                }
            }

            /* In case the pattern is not found, don't continue */
            if (differentPatt) {
                continue;
            }

            /* If the compared patterns match, then keep the index of the string */
            pattIndex.add(k, new Integer(i));
            k++;
        }

        // The number of patterns that match the actual pattern
        pattSize = pattIndex.size();


        /*
         * Now, read-in the winning patterns. But first, verify how many winning
         patterns
         * we have per record. First, we handle the simple case of 1 pattern, then
         the
         * case of multiple patterns
         */
        if (pattSize == 1) {

            theIndex = ((Integer) pattIndex.get(0)).intValue();

            // There is only one pattern. Copy the output types into
            // an StandBusinessName object
            StandBusinessName businessName = createStandBusinessName(outputPatt.get(theIndex), (String) inputPatt.get(theIndex), fieldsList, sVar);
            return businessName;

        } else if (pattSize > 1) {
            // Resolve the conflict. Choose the best pattern.

            // Select the best pattern
            theIndex = chooseBestPattern(pattIndex, inputPatt);

            // Create the pattern object
            StandBusinessName businessName = createStandBusinessName(outputPatt.get(theIndex), (String) inputPatt.get(theIndex), fieldsList, sVar);
            return businessName;

        } else {

            // Consider all the fields as not found
            for (i = 0; i < numberOfKeys; i++) {

                nFound.append("NF ");
            }

            // There is no pattern for this business entity string
            outputPatt.add(0, nFound.toString());
            // Create the pattern object
            StandBusinessName businessName = createStandBusinessName(outputPatt.get(0), null, fieldsList, sVar);
            return businessName;
        }
    }

    /**
     * Creates the object that holds the standardized business entity elements
     *
     * @param obj the object that holds the string of types
     * @param fieldsList aaa
     * @return a StandBusinessName object that holds the standardized business entity
     */
    private StandBusinessName createStandBusinessName(Object obj, final String inputPattern, ArrayList fieldsList,
            BusinessStandVariables sVar) {

        int i;
        int j = 0;

        StandBusinessName standB = StandBusinessName.getSAInstance();

        /* Cast the object as a string */
        String typeString = (String) obj;

        String token;
        String field;

        StringBuffer[] tempFields = new StringBuffer[8];

        int numberOfTempFields = tempFields.length;

        for (i = 0; i < numberOfTempFields; i++) {
            tempFields[i] = new StringBuffer("");
        }

        /* Separate the string into a list of types */
        StringTokenizer stok = new StringTokenizer(typeString);

        int numberOfTypes = stok.countTokens();

        /* Loop Over all the types inside the pattern and store them
         * inside the StandBusinessName object
         */
        for (i = 0; i < numberOfTypes; i++) {

            token = stok.nextToken();

            field = (String) fieldsList.get(i);

            // Search for primary name types
            if (token.compareTo("PNT") == 0) {

                //
                tempFields[0].append(field);
                tempFields[0].append(" ");

            // Search for organization types
            } else if (token.compareTo("ORT") == 0) {

                //
                tempFields[1].append(field);
                tempFields[1].append(" ");

            // Search for association types
            } else if (token.compareTo("AST") == 0) {

                //
                tempFields[2].append(field);
                tempFields[2].append(" ");


            // Search for industry types
            } else if (token.compareTo("IDT") == 0) {

                //
                tempFields[3].append(field);
                tempFields[3].append(" ");
            
            // Search for uri type
            } else if (token.compareTo("URL") == 0) {

                tempFields[4].append(field);
                tempFields[4].append(" ");

            // Search for uri type
            } else if (token.compareTo("ALT") == 0) {

                tempFields[0].append(field);
                tempFields[0].append(" ");

            // If no type was found in a token (NF) or a token with a slash (NF-NF)
            } else if ((token.compareTo("NF") == 0) || (token.compareTo("NF-NF") ==
            0)) {

                //
                tempFields[5].append(field);
                tempFields[5].append(" ");
            
            // If the token must be deleted in the output object or if the type connector tokens
            // skip them 
            } else if ((token.compareTo("DEL") == 0) || (token.compareTo("CTT") ==
            0)) {
                continue;
            }
        }
        
        // Set the primary name output component
        if (tempFields[0].length() > 0) {
            standB.setPrimaryName(tempFields[0].toString().trim());
        }

        // Set the organization type key output component
        if (tempFields[1].length() > 0) {
            standB.setOrgTypeKeyword(tempFields[1].toString().trim());
        }

        // Set the association type key output component
        if (tempFields[2].length() > 0) {
            standB.setAssocTypeKeyword(tempFields[2].toString().trim());
        }

        // Set the industry type key output component
        if (tempFields[3].length() > 0) {
            standB.setIndustryTypeKeyword(tempFields[3].toString().trim());
        }

        // Set the industry type key output component
        if (tempFields[4].length() > 0) {
            standB.setUrl(tempFields[4].toString().trim());
        }
        
        // Set in the not found key the fields with no type
        if (tempFields[5].length() > 0) {
            standB.setNotFoundType(tempFields[5].toString().trim());
        }



        if (sVar.aliasList.size() > 0) {
            standB.setAliasList(sVar.aliasList);
        }


        if (sVar.industrySectorList.size() > 0) {
            standB.setIndustrySectorList(sVar.industrySectorList);
        }
        
        StringBuilder inputPatterns = new StringBuilder("");
        String inputTypeString = "";
        
        if (inputPattern != null) {
            /* Separate the string into a list of types */
            final StringTokenizer inputStok = new StringTokenizer(inputPattern);
            final int numberOfInputTypes = inputStok.countTokens();
                   
            String inputToken;
            for (i = 0; i < numberOfInputTypes; i++) {
                inputToken = inputStok.nextToken();
                inputPatterns.append(inputToken);
                inputPatterns.append(" ");
            }
            
            //strip off the digit
            inputTypeString = inputPatterns.substring(2).trim() + "|";
        }
        
        standB.setSignature(inputTypeString.trim() + typeString);
        // Return the standardized object
        return standB;
    }

    /**
     * Creates the object that holds the standardized business entity elements
     *
     * @param obj the object that holds the string of types
     * @return a StandBusinessName object that holds the standardized business entity
     */
    private int chooseBestPattern(ArrayList pattIndex, ArrayList inputPatt) {

        int theIndex = 0;
        int i = 0;
        String str;

        /* Number of tokens inside the record */
        int numberOfPatt = pattIndex.size();

//        for (i = 0; i < numberOfPatt; i++) {
//            str = (String) inputPatt.get(i);
//            theIndex = ((Integer) pattIndex.get(i)).intValue();
//        }
//        return theIndex;
          return ((Integer) pattIndex.get(i)).intValue();
    }

}
