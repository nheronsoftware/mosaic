/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.api;

/**
 * Handles exceptions related to unrecognized arguments in the
 * standardization engine
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
public class SbmeIllegalArgumentException
extends SbmeMatchEngineException {
    
    /**
     * Constructs an <code>SbmeIllegalArgumentException</code> with no detail
     * message.
     * @param t a throwable.
     */
    public SbmeIllegalArgumentException(Throwable t) {
        super(t);
    }
    
    /**
     * Constructs an <code>DapIllegalArgumentException</code> with the specified
     * detail message.
     *
     * @param msg the detail message.
     */
    public SbmeIllegalArgumentException(String msg) {
        super(msg);
    }
    
    /**
     * Constructs an <code>DapIllegalArgumentException</code> with the specified
     * detail message.
     *
     * @param msg the detail message.
     * @param t a throwable.
     */
    public SbmeIllegalArgumentException(String msg, Throwable t) {
        super(msg, t);
    }
}
