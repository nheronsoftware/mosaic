/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import com.stc.sbme.stand.address.OutputPattern;

/**
 * The SbmePersonName represents and manipulates any person name record that
 * has been broken down into its basic components
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
public class SbmeAddress
    implements SbmeStandRecord {
    

// RRC
private static final OutputPattern[] EMPTY_PATTERNS = new OutputPattern[0];
OutputPattern[] outputPatterns = EMPTY_PATTERNS;
public OutputPattern[] getOutputPatterns() {return outputPatterns;}
public void setOutputPatterns(OutputPattern[] outputPatterns) {
    this.outputPatterns = outputPatterns;
    Arrays.sort(this.outputPatterns, new Comparator<OutputPattern>() {
        public int compare(OutputPattern left, OutputPattern right) {
            return left.getBeg() - right.getBeg();
        }
    });
}
private String signature;
public String getSignature() {
	if (signature == null) {
		StringBuilder builder = new StringBuilder();
		OutputPattern[] outputPatterns = this.getOutputPatterns();
		for (int i= 0; i < outputPatterns.length; i++) {
			if (i > 0) {
				builder.append('|');
			}
			builder.append(outputPatterns[i]);
		}
		this.signature = builder.toString();
	}
	return this.signature;
}

    /**
     * House number
     */
    public static final String HOUSE_NUMBER = "HouseNumber";

    /**
     * House number prefix
     */
    public static final String HOUSE_NUMBER_PREFIX = "HouseNumberPrefix";

    /**
     * House number suffix
     */
    public static final String HOUSE_NUMBER_SUFFIX = "HouseNumberSuffix";

    /**
     * Second house number
     */
    public static final String SECOND_HOUSE_NUMBER = "SecondHouseNumber";

    /**
     * Second House number prefix
     */
    public static final String SECOND_HOUSE_NUMBER_PREFIX = "SecondHouseNumberPrefix";

    /**
     * Original Street name
     */
    public static final String ORIG_STREET_NAME = "OrigStreetName";

    /**
     * Original Second Street name
     */
    public static final String ORIG_SECOND_STREET_NAME = "OrigSecondStreetName";
    
    /**
     * Matching Street name
     */
    public static final String MATCH_STREET_NAME = "MatchStreetName";

    /**
     * storage street name
     */
    public static final String STOR_STREET_NAME = "StorStreetName";

    /**
     * Street Name Prefix Direction
     */
    public static final String STREET_NAME_PREF_DIRECTION = "StreetNamePrefDirection";

    /**
     * Street Name Prefix Type
     */
    public static final String STREET_NAME_PREF_TYPE = "StreetNamePrefType";

    /**
     * Street Name suffix Direction
     */
    public static final String STREET_NAME_SUF_DIRECTION = "StreetNameSufDirection";

    /** 
     * Street Name suffix Type 
     */
    public static final String STREET_NAME_SUF_TYPE = "StreetNameSufType";

    /** 
     * Second Street Name suffix Type 
     */
    public static final String SECOND_STREET_NAME_SUF_TYPE = "SecondStreetNameSufType";

    /** 
     * street name extension indicator
     */
    public static final String STREET_NAME_EXTENSION_INDEX = "StreetNameExtensionIndex";
    
    /** 
     * within structure descriptor
     */
    public static final String WITHIN_STRUCT_DESCRIPT = "WithinStructDescript";
    
    /** 
     * within structure identifier
     */
    public static final String WITHIN_STRUCT_IDENTIF = "WithinStructIdentif";
    
    /** 
     * property description prefix - directional
     */
    public static final String PROP_DES_PREF_DIRECTION = "PropDesPrefDirection";
    
    /**  
     * property description prefix - type
     */
    public static final String PROP_DES_PREF_TYPE = "PropDesPrefType";

    /** 
     * Original property description name
     */
    public static final String ORIG_PROPERTY_NAME = "OrigPropertyName";
    
 
    /** 
     * matching property description name
     */
    public static final String MATCH_PROPERTY_NAME = "MatchPropertyName";
    
    /** 
     * property description suffix - type
     */
    public static final String PROPERTY_SUF_TYPE = "PropertySufType";
    
    /**  
     * property description suffix - directional
     */
    public static final String PROPERTY_SUF_DIRECTION = "PropertySufDirection";

    /** 
     * structure descriptor
     */
    public static final String STRUCTURE_DESCRIPT = "CenterDescript";
    
    /** 
     * structure identifier
     */
    public static final String STRUCTURE_IDENTIF = "CenterIdentif";
    
    /**  
     * rural route descriptor
     */
    public static final String RURAL_ROUTE_DESCRIPT = "RuralRouteDescript";

    /**  
     * rural route descriptor
     */
    public static final String RURAL_ROUTE_IDENTIF = "RuralRouteIdentif";
    

    /** 
     * box descriptor
     */
    public static final String BOX_DESCRIPT = "BoxDescript";
    
    /** 
     * box identifier
     */
    public static final String BOX_IDENTIF = "BoxIdentif";
    
    /**  
     * extra information
     */
    public static final String EXTRA_INFO = "ExtraInfo";
    
 
    
    /** 
     * Used in the standardizationEngine class to identify the type of records 
     */
    public static final String ADDRESS = "Address";
    
    
    /* Holds keys-values of the different fields */
    private HashMap addressFieldName;
    
    /* Holds keys-values of the status of the different fields */
    private HashMap addressFieldStatus;
    
    /* Holds keys-values of the precision of the different records */
    private HashMap addressRecordAccuracy;
    
    /**
     * Public constructor
     */
    public SbmeAddress() {
        addressFieldName = new HashMap();
        addressFieldStatus = new HashMap();
        addressRecordAccuracy = new HashMap();
    }
    
    /**
     * Getters used to identify
     * @return a string
     */
    public String getType() {
        return ADDRESS;
    }
    
    /**
     * Getters methods that provide the values associated with each field
     * @return a street name string
     */
    public String getHouseNumber() {
        return (String) addressFieldName.get(HOUSE_NUMBER);
    }
    
    /**
     * Setters methods that update the values associated with each field
     * @param value the street name
     */
    public void setHouseNumber(String value) {
        addressFieldName.put(HOUSE_NUMBER, value);
    }
    
    /**
     * Getters methods that provide the values associated with each field
     * @return a street number string
     */
    public String getHouseNumberPrefix() {
        return (String) addressFieldName.get(HOUSE_NUMBER_PREFIX);
    }
    
    /**
     * Setters methods that update the values associated with each field
     * @param value the street number
     */
    public void setHouseNumberPrefix(String value) {
        addressFieldName.put(HOUSE_NUMBER_PREFIX, value);
    }
    
    
    /**
     * Generic getter method for fields' names.
     * @param key the field's type
     * @return a string
     */
    public String getValue(String key) {
        return (String) addressFieldName.get(key);
    }
    
    /**
     * Generic setter method for fields' names.
     * @param key the generic key
     * @param value the corresponding value
     */
    public void setValue(String key, String value) {
        addressFieldName.put(key, value);
    }
 

    /**
     * Generic getter method for fields' names.
     * @param key the field's type
     * @return a string array
     */
    public List getValues(String key) {
        return (ArrayList) addressFieldName.get(key);
    }
    
    /**
     * Generic setter method for fields' names.
     * @param key the generic key
     * @param values the corresponding array of values
     */
    public void setValues(String key, List values) {
        addressFieldName.put(key, values);
    }

    
    /**
     * Generic getter method for fields' status.
     * @param key the status field
     * @return a status value
     */
    public String getStatusValue(String key) {
        return (String) addressFieldStatus.get(key);
    }
    
    /**
     * Generic setter method for fields' status.
     * @param key the status field
     * @param value the corresponding status value
     */
    public void setStatusValue(String key, String value) {
        addressFieldStatus.put(key, value);
    }
    
    /**
     * This method returns all the fields in a particular PersonName record
     * as a Setinterface (use HashMap to implement it).
     * @return a Set object representing all the keys
     */
    public Set getAllFields() {
        return addressFieldName.keySet();
    }
    
    /**
     * This method returns all the statuses values associated with the fields
     * in a given PersonName record as a Collection.
     * @return a Collection object representing all the statuses values
     */
    public Collection getAllStatusValues() {
        return addressFieldStatus.values();
    }
    
    /**
     * This method returns all the names associated with the fields in a
     * given PersonName record as a Collection.
     * @return a Collection object representing all the names' values
     */
    public Collection getAllNameValues() {
        return addressFieldName.values();
    }
    
    /**
     * This method returns
     * @param object the SbmeStandRecord object
     * @return a integer representing the degree of precision of the standardization
     */
    public int getStandDegreeOfAccuracy(SbmeStandRecord object) {
        return ((Integer) addressRecordAccuracy.get(object)).intValue();
    }
    
    /**
     * This method returns
     * @param object the SbmeStandRecord object
     * @param value the degree of accuracy of the stand
     */
    public void setStandDegreeOfAccuracy(SbmeStandRecord object, Integer value) {
        addressRecordAccuracy.put(object, value);
    }
}
