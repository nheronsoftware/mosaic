/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.match.weightcomp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StreamTokenizer;
import java.util.ArrayList;

import com.stc.sbme.api.SbmeMatchEngineException;
import com.stc.sbme.api.SbmeMatchingEngine;
import com.stc.sbme.api.SbmeMatchingException;
import com.stc.sbme.match.util.ReadMatchConstantsValues;
import com.stc.sbme.util.EmeUtil;
import com.stc.sbme.util.SbmeLogUtil;
import com.stc.sbme.util.SbmeLogger;

/**
 *
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
public class MatchConfigFile {

    private static final int N_FIELDS = ReadMatchConstantsValues.getNFields();
    private static final int PRIMARY = ReadMatchConstantsValues.getPrimary();
    private static final int SECONDARY = ReadMatchConstantsValues.getSecondary();
    private static final int TERTIARY = ReadMatchConstantsValues.getTertiary();

    private static final char INVERSION = ReadMatchConstantsValues.getInversion();
    private static final char MAJOR_INVERSION = ReadMatchConstantsValues.getMajorInversion();
    private static final char CROSS = ReadMatchConstantsValues.getCross();
//    private static final String MATCH_LIST = ReadMatchConstantsValues.getMatchTypeList();
    private static final String MATCH_LIST = "u ua us usu uf ul un b1 b2 dY dM dD dH dm ds n nI nR nS c p";       
    private static final char CRITICAL = ReadMatchConstantsValues.getCritical();
    private static final String NULL50 = ReadMatchConstantsValues.NULL50;
    private static final int MAX_NO_TAB = ReadMatchConstantsValues.getMaxNumberTables();

    private final SbmeLogger mLogger = SbmeLogUtil.getLogger(this);  
    private final boolean mDebug = mLogger.isDebugEnabled();

    /**
     * Define a factory method that return an instance of the class
     * @return a MatchConfigFile object
     */
    public static MatchConfigFile getInstance() {
        return new MatchConfigFile();
    }

    /**
     * Uploads one of the predefined configuration files used by
     * one specific user (one instance of this class)
     *
     * @param matchConfigStream a config file stream
     * @param mEng an instance of the SbmeMatchingEngine class
     * @param domain geographic location of the compared data
     * @return an integer
     * @throws SbmeMatchingException a generic exception
     * @throws SbmeMatchEngineException a parsing exception
     * @throws IOException an exception
     */
    public int getConfigData(
        InputStream matchConfigStream,
        SbmeMatchingEngine mEng,
        String domain)
        throws SbmeMatchingException, SbmeMatchEngineException, IOException {

        int i = 0;
        int j = 0;
        int ki = 0;
        int ig;
        int ij;
        int iid;
        int listL;
        int comTypeSwitch;
        int iLine;
        int numP;
        boolean foundI;
        boolean langFound;
        String tempS = null;
        String tempS2 = null;
        ArrayList tempS1 = new ArrayList();
        int tempInt;
        ArrayList al = new ArrayList(4);

        MatchVariables matchVar = mEng.getMatchVar(mEng);
        MatchCounts matchC = mEng.getMatchC(mEng);

        matchVar.nCrosses = 0;
        matchVar.criticalTest = 0;

        /* */
        Reader r = new BufferedReader(new InputStreamReader(matchConfigStream));
        StreamTokenizer psTokens = new StreamTokenizer(r);

        /* First line in the config file */

        // The switch option for the type of initial probabilities we use.
        // (0 if we use the {0.999 0.001} pair, 1 if we use {100, -100})
        psTokens.nextToken();
        tempS = psTokens.sval;

        if ((tempS != null) && (tempS.compareTo("ProbabilityType") == 0)) {

            // Read the value of the switch (0/1)
            psTokens.nextToken();
            tempInt = (int) psTokens.nval;
            
            if (mDebug) {
                mLogger.debug("Read the probability-type switch" + tempInt);
            }

            if ((tempInt == 0) || (tempInt == 1)) {
                comTypeSwitch = ((int) psTokens.nval);
            } else {
                mLogger.error("The probability-type switch value is incorrect" + tempInt);
                throw new SbmeMatchingException("The ProbabilityType switch must be 0 or 1");
            }
        } else {
            mLogger.error("The probability-type switch is missing");
            throw new SbmeMatchingException("The ProbabilityType switch is missing");
        }

        // Read the list of valid matching types
        String[] std = MATCH_LIST.split("\\s");
        listL = std.length;

        // Temporary removed. Will be added in the next version
        matchVar.nTable = 0;
        matchVar.dw = 0;
        matchVar.eMSwitch = 0;

        // Initialize the number of matching fields
        matchC.setNumFields(200);

        // Start reading the data associated with the matching fields
        for (i = 0; i < matchC.getNumFields(); i++) {

            // Verify that the number of matching fields is < N_FIELDS.
            if (i >= N_FIELDS) {

                mLogger.error("You have specified too many matching variables.");
                throw new SbmeMatchingException(
                    "You have specified too many matching variables."
                        + " Increase the Maximum number matching variables");
            }

            if (i == 0) {
                psTokens.nextToken();
            }

            if (psTokens.ttype == StreamTokenizer.TT_WORD
                && psTokens.sval != null) {
                // Name of matching variable
                matchVar.fieldName[i].insert(0, psTokens.sval);
            } else {
                break;
            }

            // Reset the flag associated with the language code for ther unicode comparator
            langFound = false;
            // Index associated with the matching field
            matchVar.fieldIndex[i] = i;

            // Maximum lenght, in characters, of the matching field
            psTokens.nextToken();
            matchVar.lengthF[i] = ((int) psTokens.nval);

            // Flag related to empty/null fields. It specifies a custom value of the weight
            psTokens.nextToken();

            if (psTokens.ttype == StreamTokenizer.TT_WORD) {
                matchVar.nullFlag[i] = psTokens.sval;

            } else {
                matchVar.nullFlag[i] = String.valueOf((int) psTokens.nval);
            }

            // Type of comparison to be used in string comparators. This string may
            // contain up to 4 characters. The basic comparators are:
            // exact ('c'); prorated('p'); ordinary string comparator(uo); special
            // string comparator for first name(uf); special string comparator for
            // last name(ul); special string comparator for numeric component(un)
            // and modified ordinary string comparator(u).
            if (i < matchC.getNumFields()) {
                psTokens.nextToken();
            }

            matchVar.comparatorType[i].insert(0, psTokens.sval);
            
            foundI = false;
            // Verify that the match type is valid
            for (j = 0; j < listL; j++) {
                
                if (std[j].compareTo(psTokens.sval) == 0){
                    foundI = true;
                    break;
                }
            }

            if (!foundI) {
                throw new SbmeMatchingException("The match comparator: '" + psTokens.sval
                    + "' is invalid! choose a valid comparator among: \n " + MATCH_LIST);              
            }

            // If the length of the string is less than 4, add spaces
            iid = 4 - matchVar.comparatorType[i].length();

            if (iid > 0) {

                for (int k = 0; k < iid; k++) {
                    matchVar.comparatorType[i].append(" ");
                }
            }

            // current line number from StreamTokenizer
            iLine = psTokens.lineno();

            // Method 1: Initial value for m-probability that the corresponding fields agree
            // given that the records match
            psTokens.nextToken();
            if (comTypeSwitch == 0) {
                matchVar.Mproba[i] = psTokens.nval;
            }

            // Method 1: Initial value for u-probability that the corresponding fields agree
            // given that the records nonmatch
            psTokens.nextToken();
            if (comTypeSwitch == 0) {
                matchVar.Uproba[i] = psTokens.nval;
            }

            // Method 2: Initial value for m-probability that the corresponding fields agree
            // given that the records match
            psTokens.nextToken();
            if (comTypeSwitch == 1) {
                matchVar.Mproba[i] = psTokens.nval;
                if (mDebug) {
                    mLogger.debug("Reading the m-weight");
                }

                if (matchVar.Mproba[i] > 100.0 || matchVar.Mproba[i] < -100.0) {

                    mLogger.error("the maximum weight should be between [100, -100]");
                    throw new SbmeMatchingException("the maximum weight should be between [100, -100]");
                }
            }

            // Method 2: Initial value for u-probability that the corresponding fields agree
            // given that the records nonmatch
            psTokens.nextToken();
            if (comTypeSwitch == 1) {
                matchVar.Uproba[i] = psTokens.nval;
                if (mDebug) {
                    mLogger.debug("Reading the u-weight");
                }

                if (matchVar.Uproba[i] < -100.0 || matchVar.Uproba[i] > 100.0) {
                    mLogger.error("the minimum weight should be between [100, -100]");
                    throw new SbmeMatchingException("the minimum weight should be between [100, -100]");

                } else if (matchVar.Uproba[i] > matchVar.Mproba[i]) {
                    mLogger.error("the max weight must be greater than the min weight");
                    throw new SbmeMatchingException("the max weight must be greater than the min weight");
                }
            }

            // value associated with the 3-class emalgorithm
            matchVar.fracVal[i] = 0.3;

            // Integer that defines the number of possible patterns
            // in the frequency count algorithm (Default = 2)
            matchVar.fieldPatt[i] = 2;

            // This portion checks if there are any additional fields that represent
            // custom curve-adjustment parameters or any other group of additional fields.

            // Treat the end of line as a token
            psTokens.eolIsSignificant(true);

            psTokens.nextToken();

            if (psTokens.ttype != StreamTokenizer.TT_EOL
                && psTokens.ttype != StreamTokenizer.TT_EOF) {

                // First, test if the match comparator describes dates
                if (matchVar.comparatorType[i].charAt(0) == 'd') {

                    if (mDebug) {
                        mLogger.debug("Reading the Date comparator");
                    }

                    // Read the switch that control the two different options on dates
                    // (relative distance and string-type comparison that handle keypunch,
                    // phonetic and transposition errors
                    if (psTokens.ttype == StreamTokenizer.TT_WORD
                        && ((psTokens.sval.compareTo("y") == 0)
                            || (psTokens.sval.compareTo("n") == 0))) {
                        tempS = psTokens.sval;

                    } else {

                        mLogger.error("You must choose 'y' or 'n' only to select the type of date comparator");
                        throw new SbmeMatchingException(
                            "You must choose 'y' or 'n' only to select the type"
                                + " of date comparator: 'y' for distance and 'n' for string-like comparisons");
                    }

                    // Read the ranges (right and left) over which a date will have a non null weight
                    if (tempS.compareTo("y") == 0) {

                        matchVar.trans[i] = false;

                        // Make sure that the range is a number
                        if (psTokens.nextToken()
                            == StreamTokenizer.TT_NUMBER) {

                            // Read the range (in the specified unit) before the candidate date
                            matchVar.param1[i] = (int) psTokens.nval;

                            // Check that the number is positive
                            if (matchVar.param1[i] < 0) {
                                mLogger.error("Use only positive numbers with in the date comparator's parameters");
                                throw new SbmeMatchingException("Use only positive numbers with in the"
                                        + " date comparator's parameters");
                            }

                        } else {
                            mLogger.error("You must define two integers associated with 'n'");
                            throw new SbmeMatchingException("You must define two integers associated with 'n'");
                        }

                        // Read the range (in the specified unit) after the candidate date
                        if (psTokens.nextToken()
                            == StreamTokenizer.TT_NUMBER) {
                            matchVar.param2[i] = (int) psTokens.nval;

                            // Check that the number is positive
                            if (matchVar.param2[i] < 0) {
                                mLogger.error("Use only positive numbers with in the Date comparator");
                                throw new SbmeMatchingException(
                                    "Use only positive numbers with in the"
                                        + " date comparator's parameters");
                            }

                        } else {
                            mLogger.error("You need to define the second integer associated with 'n'"
                                + " in the date comparator");
                            throw new SbmeMatchingException("You must define the second integer associated with 'n'");
                        }

                    } else if (tempS.compareTo("n") == 0) {
                        matchVar.trans[i] = true;
                    }

                    while ((psTokens.nextToken() != StreamTokenizer.TT_EOL)
                        && (psTokens.ttype != StreamTokenizer.TT_EOF)) {
                        continue;
                    }

                    // Advance to the next token
                    psTokens.nextToken();

                    // handle the case where we compare numbers
                } else if (matchVar.comparatorType[i].charAt(0) == 'n') {

                    // First, test if it is a SSN
                    if ((matchVar.comparatorType[i].length() > 1)
                        && (matchVar.comparatorType[i].charAt(1) == 'S')) {

                        // Case where we require a fixed length SSN (optional)
                        if (psTokens.ttype == StreamTokenizer.TT_NUMBER) {

                            matchVar.ssnLength[i] = (int) psTokens.nval;

                            // Check that the number is positive
                            if (matchVar.ssnLength[i] < 0) {

                                mLogger.error("Use only positive numbers with in the SSN parameters");
                                throw new SbmeMatchingException(
                                    "Use only positive numbers with in the"
                                        + " SSN parameters");
                                
			    } else if (matchVar.lengthF[i] <= matchVar.ssnLength[i]) {
                                mLogger.error("The maximum length in the config file must be larger" 
                                    + " than the specified length of the SSN");
                                throw new SbmeMatchingException("The maximum length in the config file must be larger" 
                                    + " than the specified length of the SSN");                                   
                            }

                            // Move to the next token
                            psTokens.nextToken();

                            // Specify if the SSN must be a numeric or an alphanumeric
                            if (psTokens.ttype == StreamTokenizer.TT_WORD
                                && (((psTokens.sval).compareTo("nu") == 0)
                                    || (psTokens.sval).compareTo("an") == 0)) {

                                matchVar.recType[i] = psTokens.sval;
                            } else {
                                mLogger.error("Use only 'nu' for Numeric or 'an' for Alphanumeric");
                                throw new SbmeMatchingException("Use only 'nu' for Numeric or 'an' for Alphanumeric");
                            }

                            // Move to the next token
                            psTokens.nextToken();

                            // If we don't fix a length, then read only the type of the SSN
                        } else if (psTokens.ttype == StreamTokenizer.TT_WORD) {

                            if (((psTokens.sval).compareTo("nu") == 0)
                                || ((psTokens.sval).compareTo("an") == 0)) {

                                matchVar.recType[i] = psTokens.sval;
                            } else {
                                mLogger.error("Use only 'nu' for Numeric or 'an' for Alphanumeric");
                                throw new SbmeMatchingException("Use only 'nu' for Numeric or 'an' for Alphanumeric");
                            }

                            // Move to the next token
                            psTokens.nextToken();
                        }

                        ki = 0;
                        // Read the list of characters which represent all-similar-character SSNs that must
                        // be exluded by returning a zero weight
                        while (psTokens.ttype != StreamTokenizer.TT_EOL) {

                            if (psTokens.ttype == StreamTokenizer.TT_NUMBER) {
                                tempS2 = Integer.toString((int) psTokens.nval);

                            } else if (
                                psTokens.ttype == StreamTokenizer.TT_WORD) {
                                tempS2 = psTokens.sval.toUpperCase();

                            } else {
                                mLogger.error("Use only alphanumerics with 'nS'");
                                throw new SbmeMatchingException("Use only alphanumerics with 'nS'");
                            }

                            // Enforce the rule of using one character in the pattern list
                            if (tempS2.length() == 1) {

                                tempS1.add(ki, tempS2);
                                ki++;
                                psTokens.nextToken();

                            } else {
                                mLogger.error("Use only characters (with the comparator 'nS')"
                                        + " to define the patterns that are inacceptable");
                                throw new SbmeMatchingException(
                                    "Use only characters (with the comparator 'nS')"
                                        + " to define the patterns that are inacceptable");
                            }
                        }

                        // Count the number of patterns
                        numP = tempS1.size();

                        if (numP > 0) {

                            // Specify that we are using constraints over the SSN
                            matchVar.ssnConstraints[i] = true;

                            matchVar.ssnList[i].append("[");

                            for (ki = 0; ki < numP; ki++) {
                                matchVar.ssnList[i].append(tempS1.get(ki));
                            }
                            matchVar.ssnList[i].append("]");
                        }

                        psTokens.nextToken();

                        // Case when the records are integers or reals
                    } else if (
                        psTokens.ttype == StreamTokenizer.TT_WORD
                            && ((psTokens.sval.compareTo("y") == 0)
                                || (psTokens.sval.compareTo("n") == 0))) {

                        tempS = psTokens.sval;

                        // Test if we use the relative-distance comparator
                        if (tempS.compareTo("y") == 0) {

                            matchVar.trans[i] = false;

                            // Read the threshold distance over which we can compare numbers
                            if (psTokens.nextToken()
                                == StreamTokenizer.TT_NUMBER) {

                                matchVar.param3[i] = psTokens.nval;

                            } else {
                                mLogger.error("WhenHandler using 'nI' or 'nR', we must define a number"
                                        + " to specify the relative distance between the two numbers");
                                throw new SbmeMatchingException(
                                    "WhenHandler using 'nI' or 'nR', we must define a number"
                                        + " to specify the relative distance between the two numbers");
                            }

                        } else if (tempS.compareTo("n") == 0) {
                            matchVar.trans[i] = true;

                        } else {
                            mLogger.error("You must choose 'y' or 'n' only.");
                            throw new SbmeMatchingException("You must choose 'y' or 'n' only.");
                        }

                        while ((psTokens.nextToken() != StreamTokenizer.TT_EOL)
                            && (psTokens.ttype != StreamTokenizer.TT_EOF)) {
                            continue;
                        }
                        psTokens.nextToken();

                    } else {
                        mLogger.error("Number comparator has the options 'nI', 'nR', and 'nS'");
                        throw new SbmeMatchingException(
                            "Number comparator has the options 'nI', 'nR',"
                                + " and 'nS'");
                    }

                // Case where we use the prorated comparator
                } else if (matchVar.comparatorType[i].charAt(0) == 'p') {

                    // Test if the range (the percentage) is a number
                    if (psTokens.ttype == StreamTokenizer.TT_NUMBER) {

                        matchVar.rangeP[i] = psTokens.nval;

                        // Check that the number is positive and within the range
                        if (matchVar.rangeP[i] < 0
                            || matchVar.rangeP[i] > 100) {

                            mLogger.error("Use only positive numbers within [0, 100]");
                            throw new SbmeMatchingException("Use only positive numbers within [0, 100]");
                        }

                        // Move to the next token
                        psTokens.nextToken();

                        // Test if the first parameter is a number
                        if (psTokens.ttype == StreamTokenizer.TT_NUMBER) {

                            matchVar.tolerance1[i] = psTokens.nval;

                            // Check that the number is within the expected range
                            if (matchVar.tolerance1[i] > matchVar.rangeP[i]) {
                                mLogger.error("The tolerance must be smaller that the range");
                                throw new SbmeMatchingException("The tolerance must be smaller that the range");

                            } else if (matchVar.tolerance1[i] < 0) {
                                mLogger.error("The parameters of 'prorated' must be positive");
                                throw new SbmeMatchingException("The parameters of 'prorated' must be positive");
                            }

                        } else {
                            mLogger.error("Use only positive number");
                            throw new SbmeMatchingException("Use only positive number");
                        }

                        // Move to the next token
                        psTokens.nextToken();

                        // Test if the second parameter is a number
                        if (psTokens.ttype == StreamTokenizer.TT_NUMBER) {

                            matchVar.tolerance2[i] = psTokens.nval;

                            // Check that the number is within the expected range
                            if (matchVar.tolerance2[i] > (matchVar.rangeP[i] - matchVar.tolerance2[i])) {

                                mLogger.error("The tolerance must be smaller that the range"
                                        + " minus the other tolerance value");
                                throw new SbmeMatchingException(
                                    "The tolerance must be smaller that the range"
                                        + " minus the other tolerance value");
                            } else if (matchVar.tolerance2[i] < 0) {
                                mLogger.error("The parameters of 'prorated' must be positive");
                                throw new SbmeMatchingException("The parameters of 'prorated' must be positive");
                            }

                        } else {
                            mLogger.error("Use only positive number");
                            throw new SbmeMatchingException("Use only positive number");
                        }

                        while (psTokens.nextToken() != StreamTokenizer.TT_EOL
                            && (psTokens.ttype != StreamTokenizer.TT_EOF)) {
                            continue;
                        }
                        psTokens.nextToken();

                    } else {
                        mLogger.error("All the parameters of the prorated comparator must be numbers");
                        throw new SbmeMatchingException(
                            "All the parameters of the prorated comparator\n"
                                + " must be numbers");
                    }

                    // Case where we have a list of 4 downweighting adjustment parameters (numbers)
                } else if (matchVar.comparatorType[i].charAt(0) == 'u') {
                    // Test if it is the unicode comparator
                    if (matchVar.comparatorType[i].charAt(2) == 'u') {
                   
                        // Test if it is a string
                        if (psTokens.ttype == StreamTokenizer.TT_WORD) {
                            do {  
                                                
                                tempS = psTokens.sval;
                                // Make sure the language abbrev is valid
                                for (ig = 0; ig < matchVar.lang.length; ig++) {
                                                               
                                    if (tempS.compareTo(matchVar.lang[ig]) == 0) {
        
                                        matchVar.language = tempS; 
                                        psTokens.nextToken();
                                        langFound = true;
                                        break;
                                    }
                                }

                            } while (psTokens.ttype != StreamTokenizer.TT_EOF
                                && psTokens.ttype != StreamTokenizer.TT_EOL); 
                            
                            if (!langFound) {
                              mLogger.error("The language abbreviation is incorrect in the config file!");
                                throw new SbmeMatchingException("The language abbreviation '" + psTokens.sval + "' is incorrect in the config file!");                               
                            }
                        } else {
                          mLogger.error("The language abbreviation must be a two-caracter string!");
                            throw new SbmeMatchingException("The language abbreviation must be a two-caracter string!");                                                        
                        }
                        
                    } else {           
                    
    					// Reads the additional fields and write them to the ArrayList al
    					// until the end of line
    					do {
    						if (psTokens.ttype == StreamTokenizer.TT_NUMBER) {
    
    							al.add(new Double((int) psTokens.nval));
    							psTokens.nextToken();
    						} else {
    							mLogger.error("You must use numbers for downweighting");
    							throw new SbmeMatchingException("You must use numbers for downweighting");
    						}
    
    					} while (
    						psTokens.ttype != StreamTokenizer.TT_EOF
    							&& psTokens.ttype != StreamTokenizer.TT_EOL);                   
                    }
                    
                    // Move to the next token 
                    psTokens.nextToken();                    



                } else if (matchVar.comparatorType[i].charAt(0) != 'c') {
                    mLogger.error("The exact match comparator should not have parameters");
                    throw new SbmeMatchingException("The exact match comparator should not have parameters");
                }

                if (psTokens.ttype == StreamTokenizer.TT_EOF) {
                    matchC.setNumFields(i + 1);
                }

            } else {

                if (matchVar.comparatorType[i].charAt(0) == 'd') {
                    mLogger.error("You need to add the parameters related to the date comparator");
                    throw new SbmeMatchingException("You need to add the parameters related to the date comparator");
                }

                if (matchVar.comparatorType[i].charAt(0) == 'n') {
                    mLogger.error("You need to add the parameters related to the numeric comparator");
                    throw new SbmeMatchingException("You need to add the parameters related to the numeric comparator");
                }

                if (matchVar.comparatorType[i].charAt(0) == 'p') {
                    mLogger.error("You need to add the parameters related to the prorated comparator");
                    throw new SbmeMatchingException("You need to add the parameters related to the prorated comparator");
                }

                psTokens.nextToken();
            }

            // If there is less than 4 curve-adjustment parameters, initilize
            // the remaining elements to zero
            for (ij = al.size(); ij < 4; ij++) {

                al.add(new Double(0));
            }

            // Method that specify the curve-adjustment parameters for different comparators
            CurveAdjustor.adjustParameters(matchVar, al, i);

            // Handle the initial m and u probabilities for both choices
            if (comTypeSwitch == 0) {
                // Calculate the initial agreement and disagreement weight
                matchVar.agrWeight[i] =
                    Math.log(matchVar.Mproba[i] / matchVar.Uproba[i])
                        / Math.log(2.);

                matchVar.disWeight[i] =
                    Math.log(
                        (1.0 - matchVar.Mproba[i])
                            / (1.0 - matchVar.Uproba[i]))
                        / Math.log(2.);
            } else {
                matchVar.agrWeight[i] = matchVar.Mproba[i];
                matchVar.disWeight[i] = matchVar.Uproba[i];
            }

            // In the last iteration (record), replace (ignore) any appearance
            // of INVERSION or MAJOR_INVERSION with a white space. Reason unknown
            if (i == (matchC.getNumFields() - 1)) {

                if (matchVar.comparatorType[i].charAt(SECONDARY)
                    == INVERSION) {

                    matchVar.comparatorType[i].setCharAt(SECONDARY, ' ');

                    // logger
                    mLogger.info("Secondary inversion in last matching field ignored.");

                } else if (
                    matchVar.comparatorType[i].charAt(TERTIARY) == INVERSION) {

                    matchVar.comparatorType[i].setCharAt(TERTIARY, ' ');
                    mLogger.info("Secondary inversion in last matching field ignored.");

                } else if (matchVar.comparatorType[i].charAt(3) == INVERSION) {

                    matchVar.comparatorType[i].setCharAt(3, ' ');
                    mLogger.info("Secondary inversion in last matching field ignored.");

                } else if (
                    matchVar.comparatorType[i].charAt(SECONDARY) == MAJOR_INVERSION) {

                    matchVar.comparatorType[i].setCharAt(SECONDARY, ' ');

                    mLogger.info("Secondary major inversion in last matching field ignored.");

                } else if (
                    matchVar.comparatorType[i].charAt(TERTIARY)    == MAJOR_INVERSION) {

                    matchVar.comparatorType[i].setCharAt(TERTIARY, ' ');

                    mLogger.info("Secondary major inversion in last"
                            + "matching field ignored.\n");

                } else if (
                    matchVar.comparatorType[i].charAt(3) == MAJOR_INVERSION) {

                    matchVar.comparatorType[i].setCharAt(3, ' ');

                    mLogger.info("Secondary major inversion in last"
                            + "matching field ignored.\n");
                }
            }

            // Increment nCrosses if there is any apprearance of CROSS
            if ((matchVar.comparatorType[i].charAt(SECONDARY) == CROSS)
                || (matchVar.comparatorType[i].charAt(TERTIARY) == CROSS)
                || (matchVar.comparatorType[i].charAt(3) == CROSS)) {

                matchVar.nCrosses++;
            }

            // Increment nCrosses & criticalTest if there is any apprearance of
            // CRITICAL
            if ((matchVar.comparatorType[i].charAt(SECONDARY) == CRITICAL)
                || (matchVar.comparatorType[i].charAt(TERTIARY) == CRITICAL)
                || (matchVar.comparatorType[i].charAt(3) == CRITICAL)) {

                matchVar.nCrosses++;
                matchVar.criticalTest++;
            }
        }

        // If frequency tables are used, initialize the related variables, then
        // call check_frequency() to create the probabilities
        for (j = 0; j < MAX_NO_TAB; j++) {
            matchVar.tableW[j] = 0;
        }

        if (matchVar.nTable != 0) {

            // Using the frequency tables, create the probabilities
            if (!FrequencyTest.checkFrequency(mEng)) {
                return -1;
            }
        }

        // Load the frequency table(s) if when nTable is positive
        if (matchVar.nTable != 0) {

            if (!GetTable.initTable(matchVar)) {
                return -1;
            }
        }

        // WhenHandler it applies, we can use downweight adjustment parameters associated
        // with data-specific matching variables. We have dw=0 (no adjustment), dw=1
        // (upweight for transposed middle & first names, dw=3 (downweight for
        // simultaneous disagreement of first name, sex and age. dw=2, corresponds to
        // both upweight and downweight.
        if (matchVar.dw > 0) {

            // Corresponds to first name
            psTokens.nextToken();
            matchVar.dw1 = ((int) psTokens.nval);

            // Corresponds to sex
            psTokens.nextToken();
            matchVar.dw2 = ((int) psTokens.nval);

            // Corresponds to age
            psTokens.nextToken();
            matchVar.dw3 = ((int) psTokens.nval);

            // Refers to first name
            psTokens.nextToken();
            matchVar.m1 = ((int) psTokens.nval);

            // Refers to middle name
            psTokens.nextToken();
            matchVar.m2 = ((int) psTokens.nval);

            // Referes to downweighting for simultaneous disagreement on
            // first name, sex and age
            psTokens.nextToken();
            matchVar.fw1 = ((double) psTokens.nval);

            // Referes to downweighting for simultaneous disagreement on
            // first name, age
            psTokens.nextToken();
            matchVar.fw2 = ((double) psTokens.nval);

            // Referes to downweighting for simultaneous disagreement on
            // first name, sex
            psTokens.nextToken();
            matchVar.fw3 = ((double) psTokens.nval);

            // Corresponds to the simultaneous agreement on  both
            // cross-comparson of first & middle names
            psTokens.nextToken();
            matchVar.fm1 = ((double) psTokens.nval);

            // Corresponds to the simultaneous agreement on  sigle
            // cross-comparson of first & middle names
            psTokens.nextToken();
            matchVar.fm2 = ((double) psTokens.nval);

            matchVar.m1--;
            matchVar.m2--;
            matchVar.dw1--;
            matchVar.dw2--;
            matchVar.dw3--;
        }

        // Initialize the number of matching fields
        matchC.setNumFields(i);

        if (matchVar.eMSwitch > 0) {

            // An upper bond on the proportion of pairs estimated as a match.
            // This number is associated with the first class (matches)
            matchVar.proportion = 0.9;

            // The maximum degree of precision wanted in the EM iterative
            // algorithm when estimating the probabilities.
            matchVar.degPrecision = 0.00001;

            // The maximum number of iterations in the E-M algorithm
            matchVar.nIteration = 200;

            // The minimum authorized value for the weights
            matchVar.minD = 0.00001;

            // The maximum authorized value for the weights
            matchVar.maxD = 0.99999;
        }

        // Close the stream to the match config. file.
        matchConfigStream.close();

        return 1;
    }




    /**
     * Return an a set of integers that define uniquely a given matching field's
     * data
     *
     * @param fieldNames name of a matching field.
     * @param mEng an instance of the SbmeMatchingEngine class
     * @return the index associated with the matching field: fieldName
     */
    public int[] getFieldIndices(String[] fieldNames, SbmeMatchingEngine mEng) {

        int i;
        int j;
        int ind = -1;
        String fn;
        int orgNumFields;
        int numFields = fieldNames.length;
        int[] indices = new int[numFields];

        MatchVariables matchVar = mEng.getMatchVar(mEng);
        MatchCounts matchC = mEng.getMatchC(mEng);
        orgNumFields = matchC.getNumFields();
    
        
        for (i = 0; i < numFields; i++) {

            fn = fieldNames[i];

            for (j = 0; j < orgNumFields; j++) {
                
                if (EmeUtil.compareStrings(fn, matchVar.fieldName[j])) {

                    ind = matchVar.fieldIndex[j];
                    break;
                }
            }
            indices[i] = ind;
        }
        return indices;
    }
    
 }
