/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbme.match.weightcomp;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import com.stc.sbme.api.SbmeMatchingException;
import com.stc.sbme.util.EmeUtil;

/**
 * This class handles comparison between dates and times in a standard format  
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
public class Number2NumberComparator
    implements NumbersComparator {

    /**
     * 
     *
     * @param numberA  Candidate's string record.
     * @param numberB  Reference's string record.
     * @param index the time unit used
     * @param matchVar an instance of the matchVariables class
     * @return a real number that measures the degree of match [0,1]
     * @throws SbmeMatchingException an exception
     * @throws ParseException if a string parsing failed
     */
    static double compareNumbers(String numberA, String numberB, int index, MatchVariables matchVar)
        throws SbmeMatchingException, ParseException {
        
        boolean secondChar = true;
        char type = '0';
        String strA;
        String strB;
        Number numA = null;
        Number numB = null;
        boolean isIntA;
        boolean isIntB;

        if (matchVar.comparatorType[index].charAt(0) != 'n') {
            throw new SbmeMatchingException("Use a number comparator starting with: 'n'");
        }
        
        if (matchVar.comparatorType[index].charAt(1) == ' ') {
            type = 'R';
            secondChar = false;
            
        } else {
            type = matchVar.comparatorType[index].charAt(1);
        } 
    
        // Handle the number format
        NumberFormat nf = NumberFormat.getInstance(Locale.US);

        if (type == 'I') {
            nf.setParseIntegerOnly(true);
        }
        
        // Before parsing, make sure that both records are numbers
        if (EmeUtil.isNumber(numberA) && EmeUtil.isNumber(numberB)) {
            numA = nf.parse(numberA);
            numB = nf.parse(numberB);
        }


        // For use in the transposer
        if ((matchVar.trans[index]) || (type == 'S')) {
            
            if (type == 'I') {       

                strA = ((Long) numA).toString();
                strB = ((Long) numB).toString();

            // handle doubles
            } else if (type == 'R') {
            
                if (numA instanceof Long) {
                     strA = ((Long) nf.parse(numberA)).toString();
 
                } else if (numA instanceof Double) {
                    strA = ((Double) nf.parse(numberA)).toString();

                } else {
                    throw new SbmeMatchingException("Use only numbers with the numeric comparator");
                }

                if (numB instanceof Long) {
                    strB = ((Long) nf.parse(numberB)).toString();
 
                } else if (numB instanceof Double) {
                    strB = ((Double) nf.parse(numberB)).toString();

                } else {
                    throw new SbmeMatchingException("Use only numbers with the numeric comparator");
                }
        
            // Handle SSNs
            } else if (type == 'S') {
                
                return transposeAndCompareNumbers(numberA, numberB, type, index, matchVar);
                
            } else { 
                throw new SbmeMatchingException("The comparator you are using does not exist." +
                    "you must use one of the following: 'nI', 'nR' or 'nS'");
            }
        
            // Call the transposer and return the weight associated with
            // comparing the two integers
            return transposeAndCompareNumbers(strA, strB, type, index, matchVar);
                
                
        } else {
        
            // Handle integers
            if (type == 'I') { 
                int intA = (nf.parse(numberA)).intValue();
                int intB = (nf.parse(numberB)).intValue();

                return compareIntegers(intA, intB, index, matchVar);
            
            // handle doubles
            } else if (type == 'R') {
                double realA = (nf.parse(numberA)).doubleValue();
                double realB = (nf.parse(numberB)).doubleValue();
                
                return compareDoubles(realA, realB, index, matchVar);
            
            } else { 
                throw new SbmeMatchingException("The comparator you are using does not exist." +
                    "you must use one of the following: 'nI', 'nR' or 'nS'");
            }
        }
    }
        
        
    /*
     * 
     */    
    private static double transposeAndCompareNumbers(String listA, String listB, char unit,
                                                   int index, MatchVariables mVar)
            throws SbmeMatchingException {
            
        double weight = 0;
        
        // Compare only the year components
        if (unit == 'I') {
            
            // Compare only year
//            weight = GenStringComparator.genStrComparator(listA, listB, index, false, mVar);
            weight = SofStringComparator.sofStrComparator(listA, listB, index, true, mVar);              
            
        // Compare the year and months elements
        } else if (unit == 'R') {
            
            // Combine months and years into the weight
//            weight = GenStringComparator.genStrComparator(listA, listB, index, false, mVar);
            weight = SofStringComparator.sofStrComparator(listA, listB, index, true, mVar);  
                       
        // Compare the year, the months and the day elements
        } else if (unit == 'S') {

            // Combine months and years and days into the weight
//            weight = GenStringComparator.genStrComparator(listA, listB, index, true, mVar);
            weight = SofStringComparator.sofStrComparator(listA, listB, index, true, mVar);                       
        }
        
        return weight;
    }


    /*
     * 
     */ 
    private static double compareIntegers(int listA, int listB, int index, MatchVariables mVar)
            throws SbmeMatchingException {
        
        int deltaInt = Math.abs(listB - listA);
        double compV;

        if (deltaInt <= mVar.param3[index]) { 
            
            compV = 1.0 - (deltaInt) / (mVar.param3[index] + 1);
        
        } else { 
            compV = 0.0;
        }
            
        // Compute the comparator between the 2 integers
        return compV;
    }
 

    /*
     * 
     */     
    private static double compareDoubles(double listA, double listB, int index, MatchVariables mVar)
            throws SbmeMatchingException {
        
        double deltaDouble = Math.abs(listB - listA);
        double compV;
        if (deltaDouble <= mVar.param3[index]) {
            
            compV = 1.0 - deltaDouble / (mVar.param3[index] + 1);
        
        } else { 
            compV = 0.0;
        }
            
        // Compute the comparator between the 2 integers
        return compV;
    }
}
