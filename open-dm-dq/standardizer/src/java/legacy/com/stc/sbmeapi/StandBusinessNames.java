/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbmeapi;

import java.util.ArrayList;
import java.util.Set;


/**
 * Expose all the methods needed to manipulate business names
 *
 * @author Sofiane O.
 */
public interface StandBusinessNames {

    /**
     * 
     * @return a street name string
     */
    String getPrimaryName();
    
    
    /**
     * 
     * @param value the street name
     */
    void setPrimaryName(String value);
    
    
    /**
     * 
     * @return a street number string
     */
    String getOrgTypeKeyword();
    
    
    /**
     * 
     * @param value the street number
     */
    void setOrgTypeKeyword(String value);
    
  
    /**
     * 
     * @return a street number string
     */
    String getAssocTypeKeyword();
    
    
    /**
     * 
     * @param value the street number
     */
    void setAssocTypeKeyword(String value);
    
  
    /**
     * 
     * @return a street number string
     */
    ArrayList getIndustrySectorList();
    
    
    /**
     * 
     * @param value the street number
     */
    void setIndustrySectorList(ArrayList value);
    

    /**
     * 
     * @return a street number string
     */
    String getIndustryTypeKeyword();
    
    
    /**
     * 
     * @param value the street number
     */
    void setIndustryTypeKeyword(String value);
    

    /**
     * 
     * @return a street number string
     */
    ArrayList getAliasList();
    
    
    /**
     * 
     * @param values the street number
     */
    void setAliasList(ArrayList values);

    /**
     * This getter method provides the value associated with the url name
     * @return a company name as url
     */
    public String getUrl();

    /**
     * This method sets the value associated with the url name
     * @param value the company url name
     */
    public void setUrl(String value);

 
    /**
     * This method returns all the fields in a particular PersonName record
     * as a Setinterface (use HashMap to implement it).
     *
     * @return a Set grouping all the fields
     */
    public Set getAllFields();
        
}
