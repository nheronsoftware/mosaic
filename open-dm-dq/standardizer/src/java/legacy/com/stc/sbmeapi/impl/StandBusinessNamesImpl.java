/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbmeapi.impl;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import com.stc.sbmeapi.StandBusinessNames;

/**
 * Expose all the methods needed to manipulate person names
 *
 * @author Sofiane O
 */
public class StandBusinessNamesImpl
    implements StandBusinessNames {
    
    /** Defines the primary name associated with a business entity */
    public static final String PRIMARY_NAME = "PrimaryName";
    /** Defines the organization type of the business entity */
    public static final String ORG_TYPE_KEY = "OrgTypeKeyword";
    /** Defines the association type of the business entity */
    public static final String ASSOC_TYPE_KEY = "AssocTypeKeyword"; 
    /** Defines the association type of the business entity */
    public static final String INDUSTRY_TYPE_KEY = "IndustryTypeKeyword";
    /** Defines the association type of the business entity */
    public static final String ALIAS_LIST = "AliasList";
    /** Defines the association type of the business entity */
    public static final String URL_NAME = "Url";
    /** Defines the association type of the business entity */
    public static final String INDUSTRY_SECTOR_LIST = "IndustrySectorList";
  
        
    /* Holds keys-values of the different fields */
    private HashMap businessFieldName;      

    /**
     * Public constructor
     */
    public StandBusinessNamesImpl() {
        businessFieldName = new HashMap();
    }


    /**
     * This getter method provides the value associated with primary name
     * @return a company primary name
     */
    public String getPrimaryName() {
        return (String) businessFieldName.get(PRIMARY_NAME);
    }
    
    /**
     * This method sets the value associated with primary name
     * @param value the company primary name
     */
    public void setPrimaryName(String value) {
        businessFieldName.put(PRIMARY_NAME, value);
    }
    
    
    /**
     * This getter method provides the value associated with organization type key
     * @return an organization type key
     */
    public String getOrgTypeKeyword() {
        return (String) businessFieldName.get(ORG_TYPE_KEY);
    }
    
    /**
     * This method sets the value associated with organization type key
     * @param value of the organization type key
     */
    public void setOrgTypeKeyword(String value) {
        businessFieldName.put(ORG_TYPE_KEY, value);
    }
  
    /**
     * This getter method provides the value associated with association type key
     * @return an association type key
     */
    public String getAssocTypeKeyword() {
        return (String) businessFieldName.get(ASSOC_TYPE_KEY);
    }
    
    /**
     * This method sets the value associated with association type key
     * @param value of the company association type key
     */
    public void setAssocTypeKeyword(String value) {
        businessFieldName.put(ASSOC_TYPE_KEY, value);
    }
  
    /**
     * This getter method provides the value associated with the alias list
     * @return a list of alternative names
     */
    public ArrayList getAliasList() {
        return (ArrayList) businessFieldName.get(ALIAS_LIST);
    }
    
    /**
     * This method sets the value associated with the alias list
     * @param values list of the company's alternative names
     */
    public void setAliasList(ArrayList values) {
        businessFieldName.put(ALIAS_LIST, values);
    }  
   
    /**
     * This getter method provides the value associated with the industry type key
     * @return a company industry type key
     */
    public String getIndustryTypeKeyword() {
        return (String) businessFieldName.get(INDUSTRY_TYPE_KEY);
    }
    
    /**
     * This method sets the value associated with the industry type key
     * @param value of the company industry type key
     */
    public void setIndustryTypeKeyword(String value) {
        businessFieldName.put(INDUSTRY_TYPE_KEY, value);
    }
    
    /**
     * This getter method provides the value associated with the url name
     * @return a company name as url
     */
    public String getUrl() {
        return (String) businessFieldName.get(URL_NAME);
    }

    /**
     * This method sets the value associated with the url name
     * @param value the company url name
     */
    public void setUrl(String value) {
        businessFieldName.put(URL_NAME, value);
    }

    /**
     * This getter method provides the value associated with the industry sector(s)
     * @return a list of industry sector(s)
     */
    public ArrayList getIndustrySectorList() {
        return (ArrayList) businessFieldName.get(INDUSTRY_SECTOR_LIST);
    }
 
    
    /**
     * This method sets the value associated with the industry sector(s)
     * @param values list of the company's alternative names
     */
    public void setIndustrySectorList(ArrayList values) {
        businessFieldName.put(INDUSTRY_SECTOR_LIST, values);
    }

    /**
     * This method returns all the fields in a particular PersonName record
     * as a Setinterface (use HashMap to implement it).
     *
     * @return a Set grouping all the fields
     */
    public Set getAllFields() {
        return businessFieldName.keySet();
    }
}
