/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbmeapi;

import java.io.InputStream;

import com.stc.sbme.api.SbmeConfigurationException;

/**
 *
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1.2.1 $
 */
public interface StandConfigFilesAccess {

    /**
     * Provides a stream from the person first name file
     *
     * @return the corresponding stream for first name
     * @throws SbmeConfigurationException a file related exception
     */
    InputStream providePersonFirstNameFile(String domain)
        throws SbmeConfigurationException;
    /**
     * Provides a stream from the last first name file
     *
     * @return the corresponding stream for last name
     * @throws SbmeConfigurationException a file related exception
     */
    InputStream providePersonLastNameFile(String domain)
        throws SbmeConfigurationException; 
        
    /**
     * Provides a stream from the person conjonctions file
     *
     * @return a stream from the person conjonctions file
     * @throws SbmeConfigurationException a file related exception
     */
    InputStream providePersonConjonctionFile(String domain)
        throws SbmeConfigurationException;
        
        
    /**
     * Provides a stream from the person generational suffixes file
     *
     * @return a stream from the person generational suffixes file
     * @throws SbmeConfigurationException a file related exception
     */
    InputStream providePersonGenSuffixFile(String domain)
        throws SbmeConfigurationException;
 
        
    /**
     * Provides a stream from the person name patterns file
     *
     * @return a stream from the person name patterns file
     * @throws SbmeConfigurationException a file related exception
     */
    InputStream providePersonNamePatternsFile(String domain)
        throws SbmeConfigurationException;
        
        
    /**
     * Provides a stream from the person last name prefix file
     *
     * @return a stream from the person last name prefix file
     * @throws SbmeConfigurationException a file related exception
     */
    InputStream providePersonLastNamePrefixFile(String domain)
        throws SbmeConfigurationException;
        
        
    /**
     * Provides a stream of the person occupational suffixes file
     *
     * @return a stream of the person occupational suffixes file
     * @throws SbmeConfigurationException a file related exception
     */
    InputStream providePersonOccupSuffixFile(String domain)
        throws SbmeConfigurationException;
 
        
    /**
     * Provides a stream of the person titles file
     *
     * @return  a stream of the person titles file
     * @throws SbmeConfigurationException a file related exception
     */
    InputStream providePersonTitleFile(String domain)
        throws SbmeConfigurationException;

        
    /**
     *
     * @return a stream of the person constants file
     * @throws SbmeConfigurationException a file related exception
     */
    InputStream providePersonConstantsFile(String domain)
        throws SbmeConfigurationException;

    /**
     *
     * @return a stream of the person dashed first names file
     * @throws SbmeConfigurationException a file related exception
     */
    InputStream provideFirstNameDashFile(String domain)
        throws SbmeConfigurationException;
    
    /**
     *
     * @return a stream of the person constants file
     * @throws SbmeConfigurationException a file related exception
     */
    InputStream provideBusinessOrRelatedFile(String domain)
        throws SbmeConfigurationException;
    
    /**
     *
     * @return a stream of the person constants file
     * @throws SbmeConfigurationException a file related exception
     */
    InputStream providePersonSpecCharsFile(String domain)
        throws SbmeConfigurationException;
    
    /**
     *
     * @return a stream of the person constants file
     * @throws SbmeConfigurationException a file related exception
     */
    InputStream providePersonThreeFile(String domain)
        throws SbmeConfigurationException;
    
    /**
     *
     * @return a stream of the person constants file
     * @throws SbmeConfigurationException a file related exception
     */
    InputStream providePersonTwoFile(String domain)
        throws SbmeConfigurationException;
 
        
    /**
     * Provides a stream of the address clues abbreviations file
     *
     * @return a stream of the address clues abbreviations file
     * @throws SbmeConfigurationException a file related exception
     */
    InputStream provideAddressClueAbbrevFile(String domain)
        throws SbmeConfigurationException;

    /**
     * Provides a stream of the address master clues file
     *
     * @return a stream of the address master clues file
     * @throws SbmeConfigurationException a file related exception
     */
    InputStream provideAddressMasterCluesFile(String domain)
        throws SbmeConfigurationException;

    /**
     * Provides a stream of the address patterns file
     *
     * @return a stream of the address patterns file
     * @throws SbmeConfigurationException a file related exception
     */
    InputStream provideAddressPatternsFile(String domain)
        throws SbmeConfigurationException;

    /**
     * Provides a stream of the second address patterns file
     *
     * @return a stream of the address patterns file
     * @throws SbmeConfigurationException a file related exception
     */
    InputStream provideAddressOutputPatternsFile(String domain)
        throws SbmeConfigurationException;

    /**
     * Provides a stream of the address config file
     *
     * @return a stream of the address config file
     * @throws SbmeConfigurationException a file related exception
     */
    InputStream provideAddressConstantsFile(String domain)
        throws SbmeConfigurationException;

    /**
     * Provides a stream of the address config file
     *
     * @return a stream of the address config file
     * @throws SbmeConfigurationException a file related exception
     */
    InputStream provideAddressInternalConstantsFile(String domain)
        throws SbmeConfigurationException;

    /**
     * Provides a stream of the business config file
     *
     * @return a stream of the business config file
     * @throws SbmeConfigurationException a file related exception
     */
    InputStream provideBizConstantsFile(String domain)
        throws SbmeConfigurationException;

    /**
     * Provides a stream of the business adjectives file
     *
     * @return a stream of the business adjectives file
     * @throws SbmeConfigurationException a file related exception
     */
    InputStream provideBussinessAdjectivesFile(String domain)
        throws SbmeConfigurationException;

    /**
     * Provides a stream of the business aliases file
     *
     * @return a stream of the business aliases file
     * @throws SbmeConfigurationException a file related exception
     */
    InputStream provideBussinessAliasFile(String domain)
        throws SbmeConfigurationException;
    
    /**
     * Provides a stream of the business associattion type keys file
     *
     * @return a stream of the business associattion keys file
     * @throws SbmeConfigurationException a file related exception
     */
    InputStream provideBussinessAssociatitionFile(String domain)
        throws SbmeConfigurationException;
    
    
    /**
     * Provides a stream of the business general terms file
     *
     * @return a stream of the business general terms file
     * @throws SbmeConfigurationException a file related exception
     */
    InputStream provideBussinessGenTermsFile(String domain)
        throws SbmeConfigurationException;
    
    
    /**
     * Provides a stream of the city or state keys file
     *
     * @return a stream of the city or state keys file
     * @throws SbmeConfigurationException a file related exception
     */
    InputStream provideCityStateTypeFile(String domain)
        throws SbmeConfigurationException;
    
    
    /**
     * Provides a stream of the business former names file
     *
     * @return a stream of the business former names file
     * @throws SbmeConfigurationException a file related exception
     */
    InputStream provideBussinessFormerNameFile(String domain)
        throws SbmeConfigurationException;
    
    /**
     * Provides a stream of the business merger names file
     *
     * @return a stream of the business merger names file
     * @throws SbmeConfigurationException a file related exception
     */
    InputStream provideCompanyMergerNameFile(String domain)
        throws SbmeConfigurationException;
    
    
    /**
     * Provides a stream of the company prymary names file
     *
     * @return a stream of the company primary names file
     * @throws SbmeConfigurationException a file related exception
     */
    InputStream provideCompanyPrimaryNameFile(String domain)
        throws SbmeConfigurationException;
    
    
    /**
     * Provides a stream of the business connector tokens file
     *
     * @return a stream of the business connector tokens file
     * @throws SbmeConfigurationException a file related exception
     */
    InputStream provideBussinessConnectorTokensFile(String domain)
        throws SbmeConfigurationException;


    /**
     * Provides a stream of the country type keys file
     *
     * @return a stream of the country type keys file
     * @throws SbmeConfigurationException a file related exception
     */
    InputStream provideCountryTypeKeysFile(String domain)
        throws SbmeConfigurationException;


    /**
     * Provides a stream of the industry categories file
     *
     * @return a stream of the industry categories file
     * @throws SbmeConfigurationException a file related exception
     */
    InputStream provideIndustryCategoriesFile(String domain)
        throws SbmeConfigurationException;


    /**
     * Provides a stream of the business connector tokens file
     *
     * @return a stream of the business connector tokens file
     * @throws SbmeConfigurationException a file related exception
     */
    InputStream provideIndustryTypeKeysFile(String domain)
        throws SbmeConfigurationException;


    /**
     * Provides a stream of the Organization Type Keys file
     *
     * @return a stream of the Organization type keys file
     * @throws SbmeConfigurationException a file related exception
     */
    InputStream provideOrganizationTypeKeysFile(String domain)
        throws SbmeConfigurationException;
    /**
     * Provides a stream of the business patterns file
     *
     * @return a stream of the business patterns file
     * @throws SbmeConfigurationException a file related exception
     */
    InputStream provideBusinessPatternsFile(String domain)
        throws SbmeConfigurationException;
    
    
    /**
     * Provides a stream of the special characters to remove file
     *
     * @return a stream of the special characters to remove file
     * @throws SbmeConfigurationException a file related exception
     */
    InputStream provideSpecCharsFile(String domain)
        throws SbmeConfigurationException;

}
