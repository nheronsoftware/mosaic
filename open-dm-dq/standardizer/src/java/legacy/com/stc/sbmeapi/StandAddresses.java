/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbmeapi;

import java.util.Set;

/**
 * Expose all the methods needed to manipulate addresses
 *
 * @author Sofiane O
 */
public interface StandAddresses {

    /**
     * @return a street house number 
     */
    String getHouseNumber();
    
    
    /**
     * Sets the value of the street house number
     * @param value the street house number
     */
    void setHouseNumber(String value);
    String getSecondHouseNumber();    
    void setSecondHouseNumber(String value);
    
    
    /**
     * @return a street house name
     */
    String getOrigStreetName();
    
    
    /**
     * Sets the value of the street house name
     * @param value the street house name
     */
    void setOrigStreetName(String value);
    
    /**
     * @return a street house name
     */
    String getOrigSecondStreetName();
    
    
    /**
     * Sets the value of the street house name
     * @param value the street house name
     */
    void setOrigSecondStreetName(String value);

    /**
     * @return a street house name
     */
    String getMatchStreetName();
    
    
    /**
     * Sets the value of the match (standard version) house name
     * @param value the street match house name
     */
    void setMatchStreetName(String value);
    

    /**
     * @return a street prefix direction
     */
    String getStreetNamePrefDir();
    
    
    /**
     * Sets the value of the street prefix direction
     * @param value the street prefix direction
     */
    void setStreetNamePrefDir(String value);
 
    
    /**
     * @return a street suffix direction
     */
    String getStreetNameSufDir();
    
    
    /**
     * Sets the value of the street suffix direction
     * @param value the street suffix direction
     */
    void setStreetNameSufDir(String value);

    
    /**
     * @return a street prefix type
     */
    String getStreetNamePrefType();
    
    
    /**
     * Sets the value of the street prefix type
     * @param value the street prefix type
     */
    void setStreetNamePrefType(String value);
 
    
    /**
     * @return a street suffix type
     */
    String getStreetNameSufType();
    
    /**
     * @return a street suffix type
     */
    String getSecondStreetNameSufType();
    
    /**
     * Sets the value of the street suffix type
     * @param value the street suffix type
     */
    void setSecondStreetNameSufType(String value);

    /**
     * Sets the value of the street suffix type
     * @param value the street suffix type
     */
    void setStreetNameSufType(String value);
    
    /**
     * @return a street within-structure descriptor (i.e., appt, Level...)
     */
     String getWithinStructDescript();
    
    
    /**
     * Sets the value of the street within-structure descriptor
     * @param value the street within-structure descriptor
     */
    void setWithinStructDescript(String value);

    
    /**
     * @return a street within-structure identifier
     */
    String getWithinStructIdentif();
    
    
    /**
     * Sets the value of the street within-structure identifier
     * @param value the street within-structure identifier
     */
    void setWithinStructIdentif(String value);
    

    /**
     * @return a street rural route descriptor
     */
    String getRuralRouteDescript();
    
    
    /**
     * Sets the value of the street rural route descriptor
     * @param value the street rural route descriptor
     */
    void setRuralRouteDescript(String value);


    /**
     * @return a street rural route identifier
     */
    String getRuralRouteIdentif();
    
    
    /**
     * Sets the value of the street rural route identifier
     * @param value the street rural route identifier
     */
    void setRuralRouteIdentif(String value);
    
    
    /**
     * @return a P.O. Box descriptor
     */
    String getBoxDescript();
    
    
    /**
     * Sets the value of the P.O. Box descriptor
     * @param value the P.O. Box descriptor
     */
    void setBoxDescript(String value);
    
    
    /**
     * @return a P.O. Box identifier
     */
    String getBoxIdentif();
    
    
    /**
     * Sets the value of the P.O. Box identifier
     * @param value the street P.O. Box identifier
     */
    void setBoxIdentif(String value);
    
 
    /**
     * @return a state name or abbreviation
     */
    String getStateName();
    
    
    /**
     * Sets the value of state name or abbreviation
     * @param value the state name or abbreviation
     */
    void setStateName(String value);
    
 
    /**
     * @return a zip code
     */
    String getZipCode();
    
    
    /**
     * Sets the value of the zip code
     * @param value the zip code
     */
    void setZipCode(String value);
 
    
    /**
     * @return a city name
     */
    String getCityName();
    
    
    /**
     * Sets the value of the city name
     * @param value the city name
     */
    void setCityName(String value);   


    /**
     * @return a city name
     */
    String getPropDesPrefDirection();
    
    
    /**
     * Sets the value of the city name
     * @param value the city name
     */
    void setPropDesPrefDirection(String value);  
 
        
    /**
     * @return a city name
     */
    String getPropDesPrefType();
    
    
    /**
     * Sets the value of the city name
     * @param value the city name
     */
    void setPropDesPrefType(String value);  
          
            
    /**
     * @return a city name
     */
    String getOrigPropertyName();

    
    /**
     * Sets the value of the city name
     * @param value the city name
     */
    void setOrigPropertyName(String value);  
        

    /**
     * @return a city name
     */
    String getMatchPropertyName();
        
    /**
     * Sets the value of the city name
     * @param value the city name
     */
    void setMatchPropertyName(String value);      

    
    /**
     * @return a city name
     */
    public String getPropertySufType();

        
    /**
     * Sets the value of the city name
     * @param value the city name
     */
    void setPropertySufType(String value);  
       
    
    /**
     * @return a city name
     */
    String getPropertySufDirection();

        
    /**
     * Sets the value of the city name
     * @param value the city name
     */
    void setPropertySufDirection(String value);         

    /**
     * @return a city name
     */
    String getStructureDescript();

        
    /**
     * Sets the value of the city name
     * @param value the city name
     */
    void setStructureDescript(String value);  
     
    
    /**
     * @return a city name
     */
    String getStructureIdentif();

        
    /**
     * Sets the value of the city name
     * @param value the city name
     */
    void setStructureIdentif(String value);          
    
    /**
     *
     * @return personFieldName.keySet() the set of all the object's keys
     */
    public Set getAllFields();    
}
