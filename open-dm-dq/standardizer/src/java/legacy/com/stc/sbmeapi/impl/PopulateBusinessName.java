/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbmeapi.impl;

import java.util.ArrayList;
import java.util.Set;

import com.stc.sbme.api.SbmeBusinessName;
import com.stc.sbme.api.SbmeStandRecord;
import com.stc.sbme.api.SbmeStandardizationException;
import com.stc.sbmeapi.StandBusinessNames;


/**
 *
 */
public class PopulateBusinessName {

    
    /**
     * Populate the StandPersonNames from the standardization results 
     * obtained from the Sbme engine
     * @param sRec a 2D array of SbmeStandRecord objects (person name)
     * @throws SbmeStandardizationException if the initialization failed
     */        
    public static StandBusinessNames[] returnStandObject(SbmeStandRecord[][] sRec)
            throws SbmeStandardizationException {
        
        int i;
        int j;
        
        Object[] oFields;
        String[] sFields;
        Set hs;
        int numberOfRecords = sRec.length;        
        StandBusinessNames[] sBizName = new StandBusinessNamesImpl[numberOfRecords];
        
        int numberOfFields;
        
        for (i = 0; i < numberOfRecords; i++) {            
        
            hs = sRec[i][0].getAllFields();
            numberOfFields = hs.size();
            sBizName[i] = new StandBusinessNamesImpl();
            
            oFields = hs.toArray();
            sFields = new String[numberOfFields];
                
            for (j = 0; j < numberOfFields; j++) {
            
                sFields[j] = (String) oFields[j];
                
                if (sFields[j].compareTo(StandBusinessNamesImpl.PRIMARY_NAME) == 0) {
                    sBizName[i].setPrimaryName(sRec[i][0].getValue(sFields[j]));
                    continue;
                    
                } else if (sFields[j].compareTo(StandBusinessNamesImpl.ORG_TYPE_KEY) == 0) {          
                    
                    sBizName[i].setOrgTypeKeyword(sRec[i][0].getValue(sFields[j]));
                    continue;
                                        
                } else if (sFields[j].compareTo(StandBusinessNamesImpl.ASSOC_TYPE_KEY) == 0) {             
                        
                    sBizName[i].setAssocTypeKeyword(sRec[i][0].getValue(sFields[j]));
                    continue;
                                                          
                } else if (sFields[j].compareTo(StandBusinessNamesImpl.INDUSTRY_TYPE_KEY) == 0) {                
                        
                    sBizName[i].setIndustryTypeKeyword(sRec[i][0].getValue(sFields[j]));
                    continue;
                                        
                } else if (sFields[j].compareTo(StandBusinessNamesImpl.ALIAS_LIST) == 0) {    
                 
                    sBizName[i].setAliasList((ArrayList) sRec[i][0].getValues(sFields[j]));
                    continue;
                                        
                } else if (sFields[j].compareTo(StandBusinessNamesImpl.URL_NAME) == 0) {                 
                        
                    sBizName[i].setUrl(sRec[i][0].getValue(sFields[j]));
                    continue;
                                        
                } else if (sFields[j].compareTo(StandBusinessNamesImpl.INDUSTRY_SECTOR_LIST) == 0) {                  
             
                    sBizName[i].setIndustrySectorList((ArrayList) sRec[i][0].getValues(sFields[j]));
                    continue;
                }           
            }
        }
        return sBizName;         
    }

    /**
     * Populate the StandPersonNames from the standardization results 
     * obtained from the Sbme engine
     * @param sRec a 2D array of SbmeStandRecord objects (person name)
     * @throws SbmeStandardizationException if the initialization failed
     */        
    public static StandBusinessNames[] returnStandObject(SbmeStandRecord[] sRec)
            throws SbmeStandardizationException {
        
        int i;
        int j;
        
        Object[] oFields;
        String[] sFields;
        Set hs;
        int numberOfRecords = sRec.length;        
        StandBusinessNames[] sBizName = new StandBusinessNamesImpl[numberOfRecords];
        
        int numberOfFields;
        
        for (i = 0; i < numberOfRecords; i++) {            
        
            hs = sRec[i].getAllFields();
            numberOfFields = hs.size();
            sBizName[i] = new StandBusinessNamesImpl();
            
            oFields = hs.toArray();
            sFields = new String[numberOfFields];
                
            for (j = 0; j < numberOfFields; j++) {
            
                sFields[j] = (String) oFields[j];
                
                if (sFields[j].compareTo(StandBusinessNamesImpl.PRIMARY_NAME) == 0) {
                    sBizName[i].setPrimaryName(sRec[i].getValue(sFields[j]));
                    continue;
                    
                } else if (sFields[j].compareTo(StandBusinessNamesImpl.ORG_TYPE_KEY) == 0) {          
                    
                    sBizName[i].setOrgTypeKeyword(sRec[i].getValue(sFields[j]));
                    continue;
                                        
                } else if (sFields[j].compareTo(StandBusinessNamesImpl.ASSOC_TYPE_KEY) == 0) {             
                        
                    sBizName[i].setAssocTypeKeyword(sRec[i].getValue(sFields[j]));
                    continue;
                                                          
                } else if (sFields[j].compareTo(StandBusinessNamesImpl.INDUSTRY_TYPE_KEY) == 0) {                
                        
                    sBizName[i].setIndustryTypeKeyword(sRec[i].getValue(sFields[j]));
                    continue;
                                        
                } else if (sFields[j].compareTo(StandBusinessNamesImpl.ALIAS_LIST) == 0) {
   
                    sBizName[i].setAliasList((ArrayList) sRec[i].getValues(sFields[j]));
                    continue;
                                        
                } else if (sFields[j].compareTo(StandBusinessNamesImpl.URL_NAME) == 0) {                 
                        
                    sBizName[i].setUrl(sRec[i].getValue(sFields[j]));
                    continue;
                                        
                } else if (sFields[j].compareTo(StandBusinessNamesImpl.INDUSTRY_SECTOR_LIST) == 0) {                  

                    sBizName[i].setIndustrySectorList((ArrayList) sRec[i].getValues(sFields[j]));
                    continue;
                }           
            }
        }
        return sBizName;         
    }






    /**
     * Populate the StandPersonNames from the standardization results 
     * obtained from the Sbme engine
     * @param sRec a SbmeStandRecord object (person name)
     * @throws SbmeStandardizationException if the initialization failed
     */        
    public static StandBusinessNames returnStandObject(SbmeStandRecord sRec)
            throws SbmeStandardizationException {
        
        int i;
        int j;
        
        Object[] oFields;
        String[] sFields;
        Set hs;
       
        StandBusinessNamesImpl sBizName = new StandBusinessNamesImpl();
        
        int numberOfFields;
             
        
        hs = sRec.getAllFields();
        numberOfFields = hs.size();

        
        oFields = hs.toArray();
        sFields = new String[numberOfFields];
            
        for (j = 0; j < numberOfFields; j++) {
        
            sFields[j] = (String) oFields[j];
            

            if (sFields[j].compareTo(StandBusinessNamesImpl.PRIMARY_NAME) == 0) {
                sBizName.setPrimaryName(sRec.getValue(sFields[j]));
                continue;
                    
            } else if (sFields[j].compareTo(StandBusinessNamesImpl.ORG_TYPE_KEY) == 0) {             
                    
                sBizName.setOrgTypeKeyword(sRec.getValue(sFields[j]));
                continue;
                                        
            } else if (sFields[j].compareTo(StandBusinessNamesImpl.ASSOC_TYPE_KEY) == 0) {             
                        
                sBizName.setAssocTypeKeyword(sRec.getValue(sFields[j]));
                continue;
                                                          
            } else if (sFields[j].compareTo(StandBusinessNamesImpl.INDUSTRY_TYPE_KEY) == 0) {                
                        
                sBizName.setIndustryTypeKeyword(sRec.getValue(sFields[j]));
                continue;
                                        
            } else if (sFields[j].compareTo(StandBusinessNamesImpl.ALIAS_LIST) == 0) {
                        
                sBizName.setAliasList((ArrayList) sRec.getValues(sFields[j]));
                continue;
                                        
            } else if (sFields[j].compareTo(StandBusinessNamesImpl.URL_NAME) == 0) {                 
                        
                sBizName.setUrl(sRec.getValue(sFields[j]));
                continue;
                                        
            } else if (sFields[j].compareTo(StandBusinessNamesImpl.INDUSTRY_SECTOR_LIST) == 0) {                    
                        
                sBizName.setIndustrySectorList((ArrayList) sRec.getValues(sFields[j]));
                continue;
            }           
        }
        return sBizName;         
    }
    

    /**
     * Input the data from the StandPersonNames into the SbmeStandRecord 
     * standardization object
     * @param sRec an array of SbmeStandRecord objects (person name)
     * @throws SbmeStandardizationException if the initialization failed
     */        
    public static SbmeStandRecord inputStandObject(StandBusinessNames sRec)
            throws SbmeStandardizationException {
        
        int i;
        int j;
        
        Object[] oFields;
        String[] sFields;
        Set hs;
        
        SbmeStandRecord sBizName = new SbmeBusinessName();
        
        int numberOfFields;
             
        
        hs = sRec.getAllFields();
        numberOfFields = hs.size();
        
        oFields = hs.toArray();
        sFields = new String[numberOfFields];
            
        for (j = 0; j < numberOfFields; j++) {
        
            sFields[j] = (String) oFields[j];
            
            if (sFields[j].compareTo(StandBusinessNamesImpl.PRIMARY_NAME) == 0) {
                
                sBizName.setValue(sFields[j], sRec.getPrimaryName());
                continue;
                    
            } else if (sFields[j].compareTo(StandBusinessNamesImpl.ORG_TYPE_KEY) == 0) {
                    
                sBizName.setValue(sFields[j], sRec.getOrgTypeKeyword());
                continue;
         
            } else if (sFields[j].compareTo(StandBusinessNamesImpl.ASSOC_TYPE_KEY) == 0) {
                    
                sBizName.setValue(sFields[j], sRec.getAssocTypeKeyword());
                continue;
                
            } else if (sFields[j].compareTo(StandBusinessNamesImpl.INDUSTRY_TYPE_KEY) == 0) {
                    
                sBizName.setValue(sFields[j],sRec.getIndustryTypeKeyword());
                continue;
         
            } else if (sFields[j].compareTo(StandBusinessNamesImpl.ALIAS_LIST) == 0) {
                    
                sBizName.setValues(sFields[j], sRec.getAliasList());
                continue;

            } else if (sFields[j].compareTo(StandBusinessNamesImpl.URL_NAME) == 0) {
                    
                sBizName.setValue(sFields[j],sRec.getUrl());
                continue;
         
            } else if (sFields[j].compareTo(StandBusinessNamesImpl.INDUSTRY_SECTOR_LIST) == 0) {
                    
                sBizName.setValues(sFields[j], sRec.getIndustrySectorList());
                continue;
            }                                     
        }     
        return sBizName;         
    }       
}