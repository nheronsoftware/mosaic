/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbmeapi.impl;
import java.util.HashMap;
import java.util.Set;

import com.stc.sbmeapi.StandAddresses;

/**
 * Expose all the methods needed to manipulate person names
 *
 * @author Sofiane O
 */
public class StandAddressesImpl
    implements StandAddresses {
    
    /** House number */
    public static final String HOUSE_NUMBER = "HouseNumber";
    /** Second House number */
    public static final String SECOND_HOUSE_NUMBER = "SecondHouseNumber";
    /** Original Street name */
    public static final String ORIG_STREET_NAME = "OrigStreetName";
    /** Original Second Street name */
    public static final String ORIG_SECOND_STREET_NAME = "OrigSecondStreetName";
    /** Matching Street name */
    public static final String MATCH_STREET_NAME = "MatchStreetName";
    /** Street Name Prefix Direction */
    public static final String STREET_NAME_PREF_DIRECTION = "StreetNamePrefDirection";
    /** Street Name Prefix Type */
    public static final String STREET_NAME_PREF_TYPE = "StreetNamePrefType";
    /** Street Name suffix Direction */
    public static final String STREET_NAME_SUF_DIRECTION = "StreetNameSufDirection";
    /**  Street Name suffix Type */
    public static final String STREET_NAME_SUF_TYPE = "StreetNameSufType";
    /**  Second Street Name suffix Type */
    public static final String SECOND_STREET_NAME_SUF_TYPE = "SecondStreetNameSufType";
    /**  within structure descriptor */
    public static final String WITHIN_STRUCT_DESCRIPT = "WithinStructDescript";   
    /**  within structure identifier */
    public static final String WITHIN_STRUCT_IDENTIF = "WithinStructIdentif";          
    /**  rural route descriptor */
    public static final String RURAL_ROUTE_DESCRIPT = "RuralRouteDescript";
    /**  rural route descriptor */
    public static final String RURAL_ROUTE_IDENTIF = "RuralRouteIdentif";    
    /**  box descriptor */
    public static final String BOX_DESCRIPT = "BoxDescript";    
    /**  box identifier */
    public static final String BOX_IDENTIF = "BoxIdentif";    
    /**  State */
    public static final String STATE = "State";
    /**  City */
    public static final String CITY = "City";
    /**  Zip */
    public static final String ZIP = "Zip";      
    
    
    /**  property description prefix - directional */
    public static final String PROP_DES_PREF_DIRECTION = "PropDesPrefDirection";  
    /**  property description prefix - type */
    public static final String PROP_DES_PREF_TYPE = "PropDesPrefType";
    /**  Original property description name */
    public static final String ORIG_PROPERTY_NAME = "OrigPropertyName";   
    /**  matching property description name */
    public static final String MATCH_PROPERTY_NAME = "MatchPropertyName";  
    /**  property description suffix - type */
    public static final String PROPERTY_SUF_TYPE = "PropertySufType";
    /**  property description suffix - directional */
    public static final String PROPERTY_SUF_DIRECTION = "PropertySufDirection";
    /**  structure descriptor */
    public static final String STRUCTURE_DESCRIPT = "CenterDescript"; 
    /**  structure identifier */
    public static final String STRUCTURE_IDENTIF = "CenterIdentif";
          
    
        
    /* Holds keys-values of the different fields */
    private HashMap addressFieldName;      

    /**
     * Public constructor
     */
    public StandAddressesImpl() {
        addressFieldName = new HashMap();
    }


    /**
     * Getters methods that provide the values associated with each field
     * @return a street name string
     */
    public String getHouseNumber() {
        return (String) addressFieldName.get(HOUSE_NUMBER);
    }
    
    /**
     * Setters methods that update the values associated with each field
     * @param value the street name
     */
    public void setHouseNumber(String value) {
        addressFieldName.put(HOUSE_NUMBER, value);
    }

    /**
     * Getters methods that provide the values associated with each field
     * @return a street name string
     */
    public String getSecondHouseNumber() {
        return (String) addressFieldName.get(SECOND_HOUSE_NUMBER);
    }
    
    /**
     * Setters methods that update the values associated with each field
     * @param value the street name
     */
    public void setSecondHouseNumber(String value) {
        addressFieldName.put(SECOND_HOUSE_NUMBER, value);
    }
    

    /**
     * @return a street house name
     */
    public String getOrigStreetName() {
        return (String) addressFieldName.get(ORIG_STREET_NAME);
    }
    
    
    /**
     * Sets the value of the street house name
     * @param value the street house name
     */
    public void setOrigStreetName(String value) {
        addressFieldName.put(ORIG_STREET_NAME, value);
    }
    

    /**
     * @return a street house name
     */
    public String getOrigSecondStreetName() {
        return (String) addressFieldName.get(ORIG_SECOND_STREET_NAME);
    }
    
    
    /**
     * Sets the value of the street house name
     * @param value the street house name
     */
    public void setOrigSecondStreetName(String value) {
        addressFieldName.put(ORIG_SECOND_STREET_NAME, value);
    }
    
    /**
     * @return a street house name
     */
    public String getMatchStreetName() {
        return (String) addressFieldName.get(MATCH_STREET_NAME);
    }
    
    
    /**
     * Sets the value of the match (standard version) house name
     * @param value the street match house name
     */
    public void setMatchStreetName(String value) {
        addressFieldName.put(MATCH_STREET_NAME, value);
    }
    

    /**
     * @return a street prefix direction
     */
    public String getStreetNamePrefDir() {
        return (String) addressFieldName.get(STREET_NAME_PREF_DIRECTION);
    }    
    
    /**
     * Sets the value of the street prefix direction
     * @param value the street prefix direction
     */
    public void setStreetNamePrefDir(String value) {
        addressFieldName.put(STREET_NAME_PREF_DIRECTION, value);
    }
 
    
    /**
     * @return a street suffix direction
     */
    public String getStreetNameSufDir() {
        return (String) addressFieldName.get(STREET_NAME_SUF_DIRECTION);
    }
    
    
    /**
     * Sets the value of the street suffix direction
     * @param value the street suffix direction
     */
    public void setStreetNameSufDir(String value) {
        addressFieldName.put(STREET_NAME_SUF_DIRECTION, value);
    }

    
    /**
     * @return a street prefix type
     */
    public String getStreetNamePrefType() {
        return (String) addressFieldName.get(STREET_NAME_PREF_TYPE);
    }
    
    
    /**
     * Sets the value of the street prefix type
     * @param value the street prefix type
     */
    public void setStreetNamePrefType(String value) {
        addressFieldName.put(STREET_NAME_PREF_TYPE, value);
    }
 
    
    /**
     * @return a street suffix type
     */
    public String getStreetNameSufType() {
        return (String) addressFieldName.get(STREET_NAME_SUF_TYPE);
    }
    
    /**
     * @return a street suffix type
     */
    public String getSecondStreetNameSufType() {
        return (String) addressFieldName.get(SECOND_STREET_NAME_SUF_TYPE);
    }
    
    /**
     * Sets the value of the second street suffix type
     * @param value the second street suffix type
     */
    public void setStreetNameSufType(String value) {
        addressFieldName.put(STREET_NAME_SUF_TYPE, value);
    }

    /**
     * Sets the value of the second street suffix type
     * @param value the second street suffix type
     */
    public void setSecondStreetNameSufType(String value) {
        addressFieldName.put(SECOND_STREET_NAME_SUF_TYPE, value);
    }
    
    /**
     * @return a street within-structure descriptor (i.e., appt, Level...)
     */
    public String getWithinStructDescript() {
         return (String) addressFieldName.get(WITHIN_STRUCT_DESCRIPT);
    }
    
    
    /**
     * Sets the value of the street within-structure descriptor
     * @param value the street within-structure descriptor
     */
    public void setWithinStructDescript(String value) {
        addressFieldName.put(WITHIN_STRUCT_DESCRIPT, value);
    }

    
    /**
     * @return a street within-structure identifier
     */
    public String getWithinStructIdentif() {
        return (String) addressFieldName.get(WITHIN_STRUCT_IDENTIF);
    }
    
    
    /**
     * Sets the value of the street within-structure identifier
     * @param value the street within-structure identifier
     */
    public void setWithinStructIdentif(String value) {
        addressFieldName.put(WITHIN_STRUCT_IDENTIF, value);
    }
    

    /**
     * @return a street rural route descriptor
     */
    public String getRuralRouteDescript() {
        return (String) addressFieldName.get(RURAL_ROUTE_DESCRIPT);
    }
    
    
    /**
     * Sets the value of the street rural route descriptor
     * @param value the street rural route descriptor
     */
    public void setRuralRouteDescript(String value) {
        addressFieldName.put(RURAL_ROUTE_DESCRIPT, value);
    }


    /**
     * @return a street rural route identifier
     */
    public String getRuralRouteIdentif() {
        return (String) addressFieldName.get(RURAL_ROUTE_IDENTIF);
    }
    
    
    /**
     * Sets the value of the street rural route identifier
     * @param value the street rural route identifier
     */
    public void setRuralRouteIdentif(String value) {
        addressFieldName.put(RURAL_ROUTE_IDENTIF, value);
    }
    
    
    /**
     * @return a P.O. Box descriptor
     */
    public String getBoxDescript() {
        return (String) addressFieldName.get(BOX_DESCRIPT);
    }
    
    
    /**
     * Sets the value of the P.O. Box descriptor
     * @param value the P.O. Box descriptor
     */
    public void setBoxDescript(String value) {
        addressFieldName.put(BOX_DESCRIPT, value);
    }
    
    
    /**
     * @return a P.O. Box identifier
     */
    public String getBoxIdentif() {
        return (String) addressFieldName.get(BOX_IDENTIF);
    }
    
    
    /**
     * Sets the value of the P.O. Box identifier
     * @param value the street P.O. Box identifier
     */
    public void setBoxIdentif(String value) {
        addressFieldName.put(BOX_IDENTIF, value);
    }
    
 
    /**
     * @return a state name or abbreviation
     */
    public String getStateName() {
        return (String) addressFieldName.get(STATE);
    }
    
    
    /**
     * Sets the value of state name or abbreviation
     * @param value the state name or abbreviation
     */
    public void setStateName(String value) {
        addressFieldName.put(STATE, value);
    }
    
 
    /**
     * @return a zip code
     */
    public String getZipCode() {
        return (String) addressFieldName.get(ZIP);
    }
    
    
    /**
     * Sets the value of the zip code
     * @param value the zip code
     */
    public void setZipCode(String value) {
        addressFieldName.put(ZIP, value);
    }
 
    
    /**
     * @return a city name
     */
    public String getCityName() {
        return (String) addressFieldName.get(CITY);
    }
    
    
    /**
     * Sets the value of the city name
     * @param value the city name
     */
    public void setCityName(String value) {
        addressFieldName.put(CITY, value);  
    }
        
    /**
     * @return a city name
     */
    public String getPropDesPrefDirection() {
        return (String) addressFieldName.get(PROP_DES_PREF_DIRECTION);
    }
    
    
    /**
     * Sets the value of the city name
     * @param value the city name
     */
    public void setPropDesPrefDirection(String value) {
        addressFieldName.put(PROP_DES_PREF_DIRECTION, value);  
    }        
        
    /**
     * @return a city name
     */
    public String getPropDesPrefType() {
        return (String) addressFieldName.get(PROP_DES_PREF_TYPE);
    }
    
    
    /**
     * Sets the value of the city name
     * @param value the city name
     */
    public void setPropDesPrefType(String value) {
        addressFieldName.put(PROP_DES_PREF_TYPE, value);  
    }        
            
    /**
     * @return a city name
     */
    public String getOrigPropertyName() {
        return (String) addressFieldName.get(ORIG_PROPERTY_NAME);
    }   
    
    /**
     * Sets the value of the city name
     * @param value the city name
     */
    public void setOrigPropertyName(String value) {
        addressFieldName.put(ORIG_PROPERTY_NAME, value);  
    }        

    /**
     * @return a city name
     */
    public String getMatchPropertyName() {
        return (String) addressFieldName.get(MATCH_PROPERTY_NAME);
    }
        
    /**
     * Sets the value of the city name
     * @param value the city name
     */
    public void setMatchPropertyName(String value) {
        addressFieldName.put(MATCH_PROPERTY_NAME, value);  
    }        



    
    /**
     * @return a city name
     */
    public String getPropertySufType() {
        return (String) addressFieldName.get(PROPERTY_SUF_TYPE);
    }
        
    /**
     * Sets the value of the city name
     * @param value the city name
     */
    public void setPropertySufType(String value) {
        addressFieldName.put(PROPERTY_SUF_TYPE, value);  
    }        
    
    /**
     * @return a city name
     */
    public String getPropertySufDirection() {
        return (String) addressFieldName.get(PROPERTY_SUF_DIRECTION);
    }
        
    /**
     * Sets the value of the city name
     * @param value the city name
     */
    public void setPropertySufDirection(String value) {
        addressFieldName.put(PROPERTY_SUF_DIRECTION, value);  
    }        

    /**
     * @return a city name
     */
    public String getStructureDescript() {
        return (String) addressFieldName.get(STRUCTURE_DESCRIPT);
    }
        
    /**
     * Sets the value of the city name
     * @param value the city name
     */
    public void setStructureDescript(String value) {
        addressFieldName.put(STRUCTURE_DESCRIPT, value);  
    }        
    
    /**
     * @return a city name
     */
    public String getStructureIdentif() {
        return (String) addressFieldName.get(STRUCTURE_IDENTIF);
    }
        
    /**
     * Sets the value of the city name
     * @param value the city name
     */
    public void setStructureIdentif(String value) {
        addressFieldName.put(STRUCTURE_IDENTIF, value);  
    }        
       
    /**
     *
     * @return personFieldName.keySet() the set of all the object's keys
     */
    public Set getAllFields() {
        return addressFieldName.keySet();
    }      
               
}
