/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbmeapi;

import java.util.Set;

/**
 * Expose all the methods needed to manipulate person names
 *
 * @author Sofiane O
 */
public interface StandPersonNames {

    /**
     *
     * @return the first name field
     */
    String getFirstName();

 
    /**
     *
     * @return the last name field
     */
    String getLastName();

    
    /**
     *
     * @return the middle name field
     */
    String getMiddleName();

    
    /**
     *
     * @return the title field
     */
    String getTitle();
    
    
    /**
     *
     * @return the generational suffix field (i.e., Jr, Sr...)
     */
    String getGenSuffix();
    
    
    /**
     *
     * @return the person's gender field
     */
    String getGender();
 
 
    /**
     *
     * @return the occupational suffix field (i.e., Doctor, Lawyer...)
     */
    String getOccupSuffix();
    /**
     *
     * @return the last name prefix field
     */
    public String getLastPrefix();
 
    /**
     *
     * @return the business or any related field(s)
     */
    public String getBizOrRelated();
 
    
    /**
     *
     * @return any unknown type(s)
     */
    public String getUnknownType();
        
        
    /**
     *
     * @param value the first name field
     */
    void setFirstName(String value);
    
    
    /**
     *
     * @param value the last name field
     */
    void setLastName(String value);
    
    
    /**
     *
     * @param value the middle name field
     */
    void setMiddleName(String value);
    
    
    /**
     *
     * @param value the title field
     */
    void setTitle(String value);
    
    
    /**
     *
     * @param value the generational suffix field
     */
    void setGenSuffix(String value);
    
    
    /**
     *
     * @param value the person's gender field
     */
    void setGender(String value);
    
    
    /**
     *
     * @param value the occupational suffix field
     */
    void setOccupSuffix(String value);
    /**
     *
     * @param value the last name prefix
     */
    void setLastPrefix(String value);
    
    /**
     *
     * @param value the business or related term(s)
     */
    public void setBizOrRelated(String value);
  
    
    /**
     *
     * @param value the unknown field(s)
     */
    public void setUnknownType(String value);
 
 
    /**
     *
     * @return personFieldName.keySet() the set of all the object's keys
     */
    public Set getAllFields();      
}
