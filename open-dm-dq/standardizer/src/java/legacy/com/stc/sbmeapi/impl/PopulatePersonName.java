/* *************************************************************************
 *
 *          Copyright (c) 2002, SeeBeyond Technology Corporation,
 *          All Rights Reserved
 *
 *          This program, and all the routines referenced herein,
 *          are the proprietary properties and trade secrets of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 *          Except as provided for by license agreement, this
 *          program shall not be duplicated, used, or disclosed
 *          without  written consent signed by an officer of
 *          SEEBEYOND TECHNOLOGY CORPORATION.
 *
 ***************************************************************************/
package com.stc.sbmeapi.impl;

import java.util.Set;

import com.stc.sbme.api.SbmePersonName;
import com.stc.sbme.api.SbmeStandRecord;
import com.stc.sbme.api.SbmeStandardizationException;
import com.stc.sbmeapi.StandPersonNames;


/**
 *
 */
public class PopulatePersonName {

    
    /**
     * Populate the StandPersonNames from the standardization results 
     * obtained from the Sbme engine
     * @param sRec an array of SbmeStandRecord objects (person name)
     * @throws SbmeStandardizationException if the initialization failed
     */        
    public static StandPersonNames[] returnStandObject(SbmeStandRecord[] sRec)
            throws SbmeStandardizationException {
        
        int i;
        int j;
        
        Object[] oFields;
        String[] sFields;
        Set hs;
        int numberOfRecords = sRec.length;        
        StandPersonNames[] sPerson = new StandPersonNamesImpl[numberOfRecords];
        
        int numberOfFields;
        
        for (i = 0; i < numberOfRecords; i++) {            
        
            hs = sRec[i].getAllFields();
            numberOfFields = hs.size();
            sPerson[i] = new StandPersonNamesImpl();
            
            oFields = hs.toArray();
            sFields = new String[numberOfFields];
                
            for (j = 0; j < numberOfFields; j++) {
            
                sFields[j] = (String) oFields[j];
                
                if (sFields[j].compareTo(StandPersonNamesImpl.FIRSTNAME) == 0) {
                    sPerson[i].setFirstName(sRec[i].getValue(sFields[j]));
                    continue;
                    
                } else if (sFields[j].compareTo(StandPersonNamesImpl.MIDDLENAME) == 0) {
                    sPerson[i].setMiddleName(sRec[i].getValue(sFields[j]));
                    continue;
                                        
                } else if (sFields[j].compareTo(StandPersonNamesImpl.LASTNAME) == 0) {  
                    sPerson[i].setLastName(sRec[i].getValue(sFields[j]));
                    continue;
                                                          
                } else if (sFields[j].compareTo(StandPersonNamesImpl.TITLE) == 0) {
                    sPerson[i].setTitle(sRec[i].getValue(sFields[j]));
                    continue;
                                        
                } else if (sFields[j].compareTo(StandPersonNamesImpl.GENSUFFIX) == 0) {
                    sPerson[i].setGenSuffix(sRec[i].getValue(sFields[j]));
                    continue;
                                        
                } else if (sFields[j].compareTo(StandPersonNamesImpl.OCCUPSUFFIX) == 0) {
                    sPerson[i].setOccupSuffix(sRec[i].getValue(sFields[j]));
                    continue;
                } else if (sFields[j].compareTo(StandPersonNamesImpl.LASTNAMEPREFIX) == 0) {
                    sPerson[i].setLastPrefix(sRec[i].getValue(sFields[j]));
                    continue;
                                        
                } else if (sFields[j].compareTo(StandPersonNamesImpl.GENDER) == 0) {
                    sPerson[i].setGender(sRec[i].getValue(sFields[j]));
                    continue;
                                        
                } else if (sFields[j].compareTo(StandPersonNamesImpl.UNKNOWNTYPE) == 0) {
                    sPerson[i].setUnknownType(sRec[i].getValue(sFields[j]));
                    continue;
                                        
                } else if (sFields[j].compareTo(StandPersonNamesImpl.BUSINESSORRELATED) == 0) { 
                    sPerson[i].setBizOrRelated(sRec[i].getValue(sFields[j]));
                    continue;                    
                }  
            }
        }
        return sPerson;         
    }


    /**
     * Populate the StandPersonNames from the standardization results 
     * obtained from the Sbme engine
     * @param sRec a SbmeStandRecord object (person name)
     * @throws SbmeStandardizationException if the initialization failed
     */        
    public static StandPersonNames returnStandObject(SbmeStandRecord sRec)
            throws SbmeStandardizationException {
        
        int i;
        int j;
        
        Object[] oFields;
        String[] sFields;
        Set hs;
       
        StandPersonNamesImpl sPerson = new StandPersonNamesImpl();
        
        int numberOfFields;
             
        
        hs = sRec.getAllFields();
        numberOfFields = hs.size();

        
        oFields = hs.toArray();
        sFields = new String[numberOfFields];
            
        for (j = 0; j < numberOfFields; j++) {
        
            sFields[j] = (String) oFields[j];
            
            if (sFields[j].compareTo(StandPersonNamesImpl.FIRSTNAME) == 0) {
                sPerson.setFirstName(sRec.getValue(sFields[j]));
                continue;
                
            } else if (sFields[j].compareTo(StandPersonNamesImpl.MIDDLENAME) == 0) {
                sPerson.setMiddleName(sRec.getValue(sFields[j]));
                continue;
                                    
            } else if (sFields[j].compareTo(StandPersonNamesImpl.LASTNAME) == 0) {  
                sPerson.setLastName(sRec.getValue(sFields[j]));
                continue;
                                                      
            } else if (sFields[j].compareTo(StandPersonNamesImpl.TITLE) == 0) {
                sPerson.setTitle(sRec.getValue(sFields[j]));
                continue;
                                    
            } else if (sFields[j].compareTo(StandPersonNamesImpl.GENSUFFIX) == 0) {
                sPerson.setGenSuffix(sRec.getValue(sFields[j]));
                continue;
                                    
            } else if (sFields[j].compareTo(StandPersonNamesImpl.OCCUPSUFFIX) == 0) {
                sPerson.setOccupSuffix(sRec.getValue(sFields[j]));
                continue;
            } else if (sFields[j].compareTo(StandPersonNamesImpl.LASTNAMEPREFIX) == 0) {
                    sPerson.setLastPrefix(sRec.getValue(sFields[j]));
                    continue;
                                    
            } else if (sFields[j].compareTo(StandPersonNamesImpl.GENDER) == 0) {
                sPerson.setGender(sRec.getValue(sFields[j]));
                continue;
                                    
            } else if (sFields[j].compareTo(StandPersonNamesImpl.UNKNOWNTYPE) == 0) {
                sPerson.setUnknownType(sRec.getValue(sFields[j]));
                continue;
                                    
            } else if (sFields[j].compareTo(StandPersonNamesImpl.BUSINESSORRELATED) == 0) { 
                sPerson.setBizOrRelated(sRec.getValue(sFields[j]));
                continue;                    
            }  
        }  
        return sPerson;         
    }
    


    /**
     * Input the data from the StandPersonNames into the SbmeStandRecord 
     * standardization object
     * @param sRec an array of SbmeStandRecord objects (person name)
     * @throws SbmeStandardizationException if the initialization failed
     */        
    public static SbmeStandRecord inputStandObject(StandPersonNames sRec)
            throws SbmeStandardizationException {
        
        int i;
        int j;

        Object[] oFields;
        String[] sFields;
        Set hs;
        
        SbmeStandRecord sPerson = new SbmePersonName();
        
        int numberOfFields;
             
        
        hs = sRec.getAllFields();
        numberOfFields = hs.size();
        
        oFields = hs.toArray();
        sFields = new String[numberOfFields];
            
        for (j = 0; j < numberOfFields; j++) {
        
            sFields[j] = (String) oFields[j];
            
            if (sFields[j].compareTo(StandPersonNamesImpl.FIRSTNAME) == 0) {
                sPerson.setValue(sFields[j], sRec.getFirstName());          
                continue;
                
            } else if (sFields[j].compareTo(StandPersonNamesImpl.MIDDLENAME) == 0) {
                sPerson.setValue(sFields[j], sRec.getMiddleName());
                continue;
            } else if (sFields[j].compareTo(StandPersonNamesImpl.LASTNAMEPREFIX) == 0) {
                    sPerson.setValue(sFields[j], sRec.getLastPrefix());
                    continue;
                                    
            } else if (sFields[j].compareTo(StandPersonNamesImpl.LASTNAME) == 0) {  
                sPerson.setValue(sFields[j], sRec.getLastName());
                continue;
                                                      
            } else if (sFields[j].compareTo(StandPersonNamesImpl.TITLE) == 0) {
                sPerson.setValue(sFields[j], sRec.getTitle());
                continue;
                                    
            } else if (sFields[j].compareTo(StandPersonNamesImpl.GENSUFFIX) == 0) {
                sPerson.setValue(sFields[j], sRec.getGenSuffix());
                continue;
                                    
            } else if (sFields[j].compareTo(StandPersonNamesImpl.OCCUPSUFFIX) == 0) {
                sPerson.setValue(sFields[j], sRec.getOccupSuffix());
                continue;
                                    
            } else if (sFields[j].compareTo(StandPersonNamesImpl.GENDER) == 0) {
                sPerson.setValue(sFields[j], sRec.getGender());
                continue;
            }                                          
        }          
        return sPerson;         
    }       
}