/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.clue;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.ObjectOutputStream;
import java.io.Reader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import com.sun.inti.components.record.DefaultRecordPopulator;
import com.sun.inti.components.record.DefaultRecordSource;
import com.sun.inti.components.record.LineReader;
import com.sun.inti.components.record.PropertyDescriptor;
import com.sun.inti.components.record.Record;
import com.sun.inti.components.record.RecordPopulator;
import com.sun.inti.components.record.RecordSource;
import com.sun.inti.components.string.format.BooleanFormatter;
import com.sun.inti.components.string.tokenize.FixedFieldTokenizer;
import com.sun.inti.components.string.tokenize.FixedFieldTokenizer.FieldDescriptor;
import com.sun.mdm.sbme.normalizer.ClueWord.ClueWordListFormatter;

/**
 * Loads the clue words table and sets the corresponding variables.
 * 
 */
/**
 *
 *  @author Ricardo Rocha (ricardo.rocha@sun.com)
 */
public class ClueRegistry<I extends Enum<I>> {
    private Map<String, Clue<I>> clues = new HashMap<String, Clue<I>>();

    public ClueRegistry(final Map<String, Clue<I>> clues) {
        this.clues = clues;
    }

    /**
     * Search for the clue word inside the string str
     * 
     * @param str
     *            the input string
     * @return a ClueTable instance
     */
    public Clue<I> lookup(final String str) {
        return this.clues.get(str);
    } 
    
    public static void main(String[] args) throws Exception {
    	if (args.length != 4) {
    		usage();
    	}
    	
        String inputFileName = args[0];
        File inputFile = new File(inputFileName);
        if (!inputFile.canRead()) {
            usage("Can't find input file: " + inputFileName);
        }
        
        String outputFileName = args[1];
        File outputFile = new File(outputFileName);
        outputFile.getParentFile().mkdirs();
        if (!outputFile.getParentFile().canWrite()) {
            usage("Can't open output file: " + outputFileName);
        }
        
        Class<? extends Enum<?>> enumType = null;
        try {
    		 // @SuppressWarnings("unchecked")
        	enumType = (Class<? extends Enum<?>>) Class.forName(args[2]);
        } catch (Exception e) {
        	usage("Can't load class: " + args[2]);
        }

        FixedFieldTokenizer clueRegistryTokenizer = new FixedFieldTokenizer();
        FieldDescriptor[] clueRegistryFieldDescriptors = getFieldDescriptor(args[3]);
        
        clueRegistryTokenizer.setFieldDescriptors(clueRegistryFieldDescriptors);
        PropertyDescriptor[] clueRegistryPropertyDescriptors = getPropertyDescriptor(enumType, args[3]);
        
        RecordPopulator[] recordPopulators = new RecordPopulator[] {
                new DefaultRecordPopulator(clueRegistryTokenizer, clueRegistryPropertyDescriptors),
        };
        
        Reader reader = new FileReader(args[0]);
        Iterator<String> lineReader = new LineReader(reader, true);
        
        RecordSource recordSource = new DefaultRecordSource(recordPopulators, lineReader);
        
        Record record;
        
    	Map<String, Clue<?>> clues = new LinkedHashMap<String, Clue<?>>();
        while ((record = recordSource.nextRecord()) != null) {
        	Clue<?> clue = (Clue<?>) PropertyDescriptor.populate(Clue.class, record);
        	clues.put(clue.getName(), clue);
        }

        FileOutputStream fos = new FileOutputStream(outputFile);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(clues);
        oos.flush();
        oos.close();
    }

	private static PropertyDescriptor[] getPropertyDescriptor(Class<? extends Enum<?>> enumType, String variant) {
		PropertyDescriptor[] clueRegistryPropertyDescriptors = null;
		
		if (!variant.equals("fr")) {
			clueRegistryPropertyDescriptors = new PropertyDescriptor[] {
                new PropertyDescriptor("name", null, null),
                new PropertyDescriptor("translation", null, null),
                new PropertyDescriptor("words", new ClueWordListFormatter(enumType, true), null),
                new PropertyDescriptor("translationExpanded", new BooleanFormatter("*", " "), null),
			};
		} else {
			clueRegistryPropertyDescriptors = new PropertyDescriptor[] {
                new PropertyDescriptor("name", null, null),
                new PropertyDescriptor("translation", null, null),
                new PropertyDescriptor("words", new ClueWordListFormatter(enumType, true), null),
			};
		}
		return clueRegistryPropertyDescriptors;
	}

	private static FieldDescriptor[] getFieldDescriptor(String variant) {
		FieldDescriptor[] clueRegistryFieldDescriptors = null;
		
		if (!variant.equals("fr")) {
			clueRegistryFieldDescriptors = new FieldDescriptor[] {
                new FieldDescriptor(0, 24, true), // Name
                new FieldDescriptor(24, 15, true), // Translation
                new FieldDescriptor(40, 45, false), // Words
                new FieldDescriptor(87, 1, false), // Translation expanded
			};
		} else {
			clueRegistryFieldDescriptors = new FieldDescriptor[] {
				new FieldDescriptor(0, 34, true), // Name
				new FieldDescriptor(34, 14, true), // Translation
				new FieldDescriptor(50, 20, false), // Words
			};
		}
		return clueRegistryFieldDescriptors;
	}
    
    private static void usage(String message) {
    	System.err.println(message);
    	usage();
    } 
    
    private static void usage() {
		System.err.println("Usage: java " + ClueRegistry.class.getName() + " inputFileName outputFileName enumerationClassName");
		System.exit(1);
    }
}
