package com.sun.mdm.sbme.engine;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.jar.JarFile;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import com.sun.inti.components.classloading.FileClassLoaderFactory;
import com.sun.mdm.sbme.DataTypeDescriptor;
import com.sun.mdm.sbme.Descriptor;
import com.sun.mdm.sbme.StandardizationEngine;
import com.sun.mdm.sbme.StandardizerIntrospector;
import com.sun.mdm.sbme.VariantDescriptor;
import com.sun.mdm.sbme.util.StandardizerUtils;

public class DefaultStandardizerIntrospector implements StandardizerIntrospector {
	public static final String REPOSITORY_RESOURCE_LOCATION = "META-INF/mdm/standardizer/repositoryImage.zip";

	private File repositoryDirectory;
	private StandardizationEngine engine;
	private FileClassLoaderFactory classLoaderFactory;
	
	private static final Logger logger = Logger.getLogger(DefaultStandardizerIntrospector.class.getName());

	public DefaultStandardizerIntrospector() throws Exception {
		this.classLoaderFactory = (FileClassLoaderFactory) StandardizerUtils.loadService(FileClassLoaderFactory.class);
		this.engine = new DefaultStandardizationEngine(classLoaderFactory);
	}

	public DataTypeDescriptor[] setRepository(File repositoryDirectory) throws Exception {
		if (!repositoryDirectory.isDirectory()) {
			throw new IllegalArgumentException("Not a directory: " + repositoryDirectory.getAbsolutePath());
		}

		this.repositoryDirectory = repositoryDirectory;
		this.engine =  new DefaultStandardizationEngine();
		this.engine.initialize(this.repositoryDirectory);
		this.engine.start();
		
		return this.dataTypes();
	}
	
	public DataTypeDescriptor[] importDirectory(File jarDirectory) throws Exception {
		if (!jarDirectory.isDirectory()) {
			throw new IllegalArgumentException("Not a directory: " + jarDirectory.getAbsolutePath());
		}

		File[] files = jarDirectory.listFiles(new FileFilter() {
			public boolean accept(File file) {
				return file.getName().endsWith(".jar");
			}
		});
		
		final List<JarFile> dataTypeJarFiles = new LinkedList<JarFile>();
		final List<JarFile> variantJarFiles = new LinkedList<JarFile>();
		for (File file: files) {
			try {
				JarFile jarFile = new JarFile(file);
				if (jarFile.getEntry(DefaultStandardizationEngine.DATA_TYPE_DESCRIPTOR_FILENAME) != null) {
					dataTypeJarFiles.add(jarFile);
				}
				if (jarFile.getEntry(DefaultStandardizationEngine.VARIANT_DESCRIPTOR_FILENAME) != null) {
					variantJarFiles.add(jarFile);
				}
			} catch (Exception e) {
				logger.warning("Cant create jar file: " + file.getAbsolutePath());
			}
		}

		for (JarFile jarFile: dataTypeJarFiles) {
			this.engine.deploy(jarFile);
		}
		
		for (JarFile jarFile: variantJarFiles) {
			this.engine.deploy(jarFile);
		}
		
		return this.dataTypes();
	}
	
	public DataTypeDescriptor[] dataTypes() throws Exception {
		List<DataTypeDescriptor> dataTypeDescriptors = new LinkedList<DataTypeDescriptor>();
		for (String dataTypeName: this.engine.dataTypeNames()) {
			dataTypeDescriptors.add(this.engine.getDataTypeDescriptor(dataTypeName));
		}

		return dataTypeDescriptors.toArray(new DataTypeDescriptor[dataTypeDescriptors.size()]);
	}
	
	public DataTypeDescriptor getDataType(String dataTypeName) throws Exception {
		return this.engine.getDataTypeDescriptor(dataTypeName);
	}

	public VariantDescriptor getVariant(String dataTypeName, String variantName) throws Exception {
		return this.engine.getDataTypeDescriptor(dataTypeName).getVariant(variantName);
	}

	public Descriptor importJar(JarFile jarFile) throws Exception {
		return this.engine.deploy(jarFile);
	}

	public DataTypeDescriptor importDataType(JarFile jarFile) throws Exception {
		return this.engine.deployDataType(jarFile);
	}

	public VariantDescriptor importVariant(JarFile jarFile) throws Exception {
		return this.engine.deployVariant(jarFile);
	}
	
	public DataTypeDescriptor removeDataType(String dataTypeName) throws Exception {
		return this.engine.undeployDataType(dataTypeName);
	}

	public VariantDescriptor removeVariant(String dataTypeName, String variantName) throws Exception {
		return this.engine.undeployVariant(dataTypeName, variantName);
	}

	public void takeSnapshot(File resourceJarDirectory) throws Exception {
		resourceJarDirectory.mkdirs();
		File zipDestination = new File(resourceJarDirectory, REPOSITORY_RESOURCE_LOCATION);
		zipDestination.getParentFile().mkdirs();
		ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(zipDestination));
		takeSnapshot(this.repositoryDirectory, zos);
		zos.close();
	}
	private void takeSnapshot(File file, ZipOutputStream zos) throws Exception {
		if (file.isDirectory()) {
			for (File childFile: file.listFiles()) {
				takeSnapshot(childFile, zos);
			}
		} else {
			String baseName = file.getAbsolutePath().substring(this.repositoryDirectory.getAbsolutePath().length() + 1).replace('\\', '/');
			logger.info("Adding zip entry: " + baseName);
			ZipEntry zipEntry = new ZipEntry(baseName);
			zos.putNextEntry(zipEntry);
			int cnt;
			byte[] buffer = new byte[4096];
			FileInputStream fis = new FileInputStream(file);
			while ((cnt = fis.read(buffer)) != -1) {
				zos.write(buffer, 0, cnt);
			}
		}
	}
	
	public void close() throws Exception {
		try { this.engine.stop(); } catch (Exception e) {}
	}
}
