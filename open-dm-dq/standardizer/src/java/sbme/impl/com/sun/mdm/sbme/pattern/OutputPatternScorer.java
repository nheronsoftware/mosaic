package com.sun.mdm.sbme.pattern;

import java.util.List;

public interface OutputPatternScorer<I extends Enum<I>, O extends Enum<O>> {
	public int calculateScore(List<? extends OutputPattern<I, O>> outputPatterns); 
}
