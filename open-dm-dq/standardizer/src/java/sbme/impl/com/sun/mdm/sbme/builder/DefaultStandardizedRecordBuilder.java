/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.builder;

import java.util.List;

import com.sun.inti.components.record.Record;
import com.sun.mdm.sbme.MdmStandardizationException;
import com.sun.mdm.sbme.StandardizedRecord;
import com.sun.mdm.sbme.builder.postprocessor.StandardizedRecordBuilderPostprocessor;
import com.sun.mdm.sbme.builder.preprocessor.StandardizedRecordBuilderPreprocessor;
import com.sun.mdm.sbme.builder.property.StandardizedRecordPropertyBuilder;
import com.sun.mdm.sbme.normalizer.NormalizedToken;
import com.sun.mdm.sbme.pattern.OutputPattern;

/**
 *
 *  @author Ricardo Rocha (ricardo.rocha@sun.com)
 */
public class DefaultStandardizedRecordBuilder<I extends Enum<I>, O extends Enum<O>> implements StandardizedRecordBuilder<I, O> {
	private StandardizedRecordFactory standardizedRecordFactory;
    private List<StandardizedRecordBuilderPreprocessor<I, O>> preprocessors;
    private List<OutputTokenTypeHandler<I, O>> outputTokenTypeHandlers;
    private List<StandardizedRecordBuilderPostprocessor<O>> postprocessors;
    
    public StandardizedRecord buildRecord(final List<NormalizedToken<I>> normalizedTokens, final List<? extends OutputPattern<I, O>> outputPatterns) throws MdmStandardizationException {
        Record context = new Record();
        
        if (preprocessors != null) {
            for (StandardizedRecordBuilderPreprocessor<I, O> preprocessor: preprocessors) {
                preprocessor.preprocess(normalizedTokens, outputPatterns, context);
            }
        }

        final StandardizedRecord standardizedRecord = this.standardizedRecordFactory.newStandardizedRecord();

        for (OutputPattern<I, O> outputPattern: outputPatterns) {
            // NOTE inputTokenTypes and addressOutputTokenTypes have the same length
            final List<I> inputTokenTypes = outputPattern.getInputTokenTypes();
            final List<O> outputTokenTypes = outputPattern.getOutputTokenTypes();
            
            final int lastIndex = outputPattern.getEnd() - outputPattern.getBegin() + 1;
            for (int i = 0; i < lastIndex; i++) {
                final I inputTokenType = inputTokenTypes.get(i);
                final O outputTokenType = outputTokenTypes.get(i);

                final int tokenIndex = outputPattern.getBegin() + i - 1;
                final NormalizedToken<I> normalizedToken = normalizedTokens.get(tokenIndex);

                boolean handled = false;
                for (OutputTokenTypeHandler<I, O> outputTokenTypeHandler: this.outputTokenTypeHandlers) {
                    for (O candidateOutputTokenType: outputTokenTypeHandler.getOutputTokenTypes()) {
                        if (candidateOutputTokenType == outputTokenType) {
                            handled = true;
                            StandardizedRecordPropertyBuilder<I, O> builder = outputTokenTypeHandler.getStandardizedRecordPropertyBuilder();
                            builder.buildProperty(standardizedRecord, normalizedToken, inputTokenType, outputTokenType, context);
                            break;
                        }
                    }
                    if (handled) {
                        break;
                    }
                }

                if (!handled) {
                    throw new MdmStandardizationException("No handler specified for output token type: " + outputTokenType);
                }
            }
        }
        
        if (postprocessors != null) {
            for (StandardizedRecordBuilderPostprocessor<O> postprocessor: postprocessors) {
                postprocessor.postprocess(standardizedRecord, context);
            }
        }

        // FIXME Remove after decoupling test targets from record representation
        if (standardizedRecord instanceof OutputPatternAware) {
            @SuppressWarnings("unchecked")
            OutputPattern<?, O>[] outputPatternArray = outputPatterns.toArray(new OutputPattern[outputPatterns.size()]);
            ((OutputPatternAware) standardizedRecord).setOutputPatterns(outputPatternArray);
        }

        return standardizedRecord;
    }

	public StandardizedRecordFactory getStandardizedRecordFactory() {
		return standardizedRecordFactory;
	}

	public void setStandardizedRecordFactory(StandardizedRecordFactory standardizedRecordFactory) {
		this.standardizedRecordFactory = standardizedRecordFactory;
	}

	public List<StandardizedRecordBuilderPreprocessor<I, O>> getPreprocessors() {
		return preprocessors;
	}

	public void setPreprocessors(List<StandardizedRecordBuilderPreprocessor<I, O>> preprocessors) {
		this.preprocessors = preprocessors;
	}

	public List<OutputTokenTypeHandler<I, O>> getOutputTokenTypeHandlers() {
		return outputTokenTypeHandlers;
	}

	public void setOutputTokenTypeHandlers(List<OutputTokenTypeHandler<I, O>> outputTokenTypeHandlers) {
		this.outputTokenTypeHandlers = outputTokenTypeHandlers;
	}

	public List<StandardizedRecordBuilderPostprocessor<O>> getPostprocessors() {
		return postprocessors;
	}

	public void setPostprocessors(List<StandardizedRecordBuilderPostprocessor<O>> postprocessors) {
		this.postprocessors = postprocessors;
	}
}


