/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.normalizer;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.sun.inti.components.record.Record;
import com.sun.mdm.sbme.MdmStandardizationException;
import com.sun.mdm.sbme.clue.Clue;
import com.sun.mdm.sbme.clue.ClueRegistry;
import com.sun.mdm.sbme.normalizer.populator.ClueWordPopulator;
import com.sun.mdm.sbme.normalizer.postprocessor.NormalizationPostprocessor;
import com.sun.mdm.sbme.normalizer.validator.ClueValidator;
import com.sun.mdm.sbme.parser.Tokenization;

/**
 * Main class to process any request to match candidate to reference records
 * 
 */
/**
 *
 *  @author Ricardo Rocha (ricardo.rocha@sun.com)
 */
public class DefaultNormalizer<I extends Enum<I>> implements Normalizer<I> {
    private ClueRegistry<I> clueRegistry;

    private List<ClueValidator<I>> clueValidators;

    private List<ClueWordPopulator<I>> clueWordPopulators;

    private NormalizationPostprocessor<I> normalizationPostprocessor;

    public List<NormalizedToken<I>> normalize(final Tokenization tokenization) {
        final Record contextRecord = new Record();
        final List<NormalizedToken<I>> normalizedTokens = new ArrayList<NormalizedToken<I>>();

        int endIndex = tokenization.size();
        while (endIndex > 0) {
            int startIndex;
            Clue<I> clue = null;
            String image = null;

            for (startIndex = 0; startIndex < endIndex; startIndex++) {
            	image = tokenization.concatenateTokens(startIndex, endIndex);
                // TODO Is it possible to make this search faster by using a sequence-based tree structure? 
            	clue = this.clueRegistry.lookup(image);

                if (clue != null) {
                    if (!this.validate(clue, tokenization, startIndex, endIndex)) {
                        continue;
                    }

                    break;
                }
            }

            // No clue found, decrement start index
            if (startIndex == endIndex) {
            	startIndex--;
            }

            final NormalizedToken<I> normalizedToken = new NormalizedToken<I>(image, startIndex, endIndex);
            normalizedTokens.add(normalizedToken);

            // NOTE: Some populators ignore non-null clues
            int count = this.populate(normalizedTokens,
            						  clue,
            						  tokenization,
            						  startIndex,
            						  endIndex,
            						  contextRecord);
            
            // Decrement index by number of tokens processed by populator
            endIndex -= count;
        }

        /* Reverse the token array (tokens were loaded in reverse order) */
        for (int i = normalizedTokens.size() / 2, j = i - (normalizedTokens.size() + 1) % 2;
             i < normalizedTokens.size();
             i++, j--)
        {
            final NormalizedToken<I> swap = normalizedTokens.get(i);
            normalizedTokens.set(i, normalizedTokens.get(j));
            normalizedTokens.set(j, swap);
        }

        if (this.normalizationPostprocessor != null) {
            this.normalizationPostprocessor.postprocess(normalizedTokens, tokenization);
        }

        return normalizedTokens;
    }
    
    private boolean validate(Clue<I> clue, Tokenization tokenization, int startIndex, int endIndex) {
    	if (this.clueValidators != null) {
    		for (ClueValidator<I> clueValidator: this.clueValidators) {
    			if (!clueValidator.validate(clue, tokenization, startIndex, endIndex)) {
    				return false;
    			}
    		}
    	}
    	return true;
    }

    private int populate(final List<NormalizedToken<I>> normalizedTokens, Clue<I> clue, Tokenization tokenization, int startIndex, int endIndex, final Record contextRecord) {
        int count = 0;
        for (ClueWordPopulator<I> populator: this.clueWordPopulators) {
            // This must decrement tokenizationView.lastIndex
        	count = populator.populate(normalizedTokens, clue, tokenization, startIndex, endIndex, contextRecord);
            if (count > 0) {
            	return count;
            }
        }
    	throw new MdmStandardizationException("No populator handled normalized token");
    }
    
    public DefaultNormalizer<I> add(ClueValidator<I> clueValidator) {
    	if (this.clueValidators == null) {
    		this.clueValidators = new LinkedList<ClueValidator<I>>();
    	}
    	this.clueValidators.add(clueValidator);
    	return this;
    }

    public List<ClueWordPopulator<I>> getClueWordTokenPopulators() {
		return clueWordPopulators;
	}

	public void setClueWordTokenPopulators(List<ClueWordPopulator<I>> clueWordPopulators) {
		this.clueWordPopulators = clueWordPopulators;
	}

	public NormalizationPostprocessor<I> getNormalizationPostprocessor() {
        return this.normalizationPostprocessor;
    }

    public void setNormalizationPostprocessor(final NormalizationPostprocessor<I> normalizationPostprocessor) {
        this.normalizationPostprocessor = normalizationPostprocessor;
    }

	public ClueRegistry<I> getClueRegistry() {
		return clueRegistry;
	}

	public void setClueRegistry(ClueRegistry<I> clueRegistry) {
		this.clueRegistry = clueRegistry;
	}

	public List<ClueValidator<I>> getClueValidators() {
		return clueValidators;
	}

	public void setClueValidators(List<ClueValidator<I>> clueValidators) {
		this.clueValidators = clueValidators;
	}
}
