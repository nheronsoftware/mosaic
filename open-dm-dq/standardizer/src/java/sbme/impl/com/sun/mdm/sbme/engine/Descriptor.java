package com.sun.mdm.sbme.engine;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.List;

public abstract class Descriptor {
	public static final String LIB_DIRECTORY = "lib";
	public static final String CONFIG_DIRECTORY = "config";

	public static final String JAR_EXTENSION = ".jar";
	public static final String ZIP_EXTENSION = ".zip";

	private static final File[] EMPTY_FILE_ARRAY = new File[0];

	private String name;
	private String description;
	private ClassLoader classLoader;
	private File[] jarFiles = EMPTY_FILE_ARRAY;
	private File[] configurationFiles = EMPTY_FILE_ARRAY;
	
	@Override
	public String toString() {
		return "Name: " + this.name + ", " +
			   "Description: " + this.description;
	}

	protected void setLibDirectory(File libDirectory) {
		if (libDirectory.isDirectory()) {
			this.jarFiles = libDirectory.listFiles(new FileFilter() {
				public boolean accept(File file) {
					return file.getName().endsWith(JAR_EXTENSION) ||
						   file.getName().endsWith(ZIP_EXTENSION);
				}
			});
		}
	}
	
	protected void setResourceDirectory(File resourceDirectory) {
		if (resourceDirectory.isDirectory()) {
			List<File> files = new ArrayList<File>();
			getFiles(resourceDirectory, files);
			this.configurationFiles = files.toArray(new File[files.size()]);
		}
	}
	private void getFiles(File file, List<File> filenames) {
		if (file.isDirectory()) {
			for (File childFile: file.listFiles())  {
				getFiles(childFile, filenames);
			}
		} else {
			filenames.add(file);
		}
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public File[] getJarFilenames() {
		return this.jarFiles;
	}
	
	public File[] getConfigurationFilenames() {
		return this.configurationFiles;
	}
	
	public File[] getJarFiles() {
		return this.jarFiles;
	}
	
	public File[] getResourceFiles() {
		return this.configurationFiles;
	}

	public ClassLoader getClassLoader() {
		return classLoader;
	}

	public void setClassLoader(ClassLoader classLoader) {
		this.classLoader = classLoader;
	}
}
