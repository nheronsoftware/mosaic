/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.normalizer.populator;

import java.util.List;

import com.sun.mdm.sbme.normalizer.ClueWord;
import com.sun.mdm.sbme.normalizer.NormalizedToken;

/**
 *
 *  @author Ricardo Rocha (ricardo.rocha@sun.com)
 */
public abstract class AbstractPopulator<I extends Enum<I>> implements ClueWordPopulator<I> {
    protected void addClueWord(final List<NormalizedToken<I>> tokens, final ClueWord<I> clueWord) {
        tokens.get(tokens.size() - 1).addClueWord(clueWord);
    }

    protected void addClueWord(final List<NormalizedToken<I>> tokens, final int id, final I type) {
        tokens.get(tokens.size() - 1).addClueWord(new ClueWord<I>(id, type));
    }

    protected void addClueWord(final List<NormalizedToken<I>> tokens, final I type) {
        tokens.get(tokens.size() - 1).addClueWord(new ClueWord<I>(type));
    }

    protected void setImage(final List<NormalizedToken<I>> tokens, final String image) {
        tokens.get(tokens.size() - 1).setImage(image);
    }
}
