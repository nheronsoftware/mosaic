package com.sun.mdm.sbme.pattern;

import java.util.List;

public interface OutputPatternBuilder<I extends Enum<I>, O extends Enum<O>> {
	public List<? extends OutputPattern<I, O>> buildOutputPatterns(List<I> inputTokenTypes);
}
