package com.sun.mdm.sbme.engine;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.sun.mdm.sbme.DataTypeDescriptor;
import com.sun.mdm.sbme.VariantDescriptor;
import com.sun.mdm.sbme.util.Aliased;

public class DataTypeDescriptorImpl extends Descriptor implements DataTypeDescriptor {
	private Class<Enum<?>> fieldEnumerationClass;
	private Map<String, VariantDescriptorImpl> variantMap;
	private String[] fieldNames;
	
	public VariantDescriptor[] variants() {
		VariantDescriptor[] variantDescriptors = new VariantDescriptor[getVariantMap().size()];
		Iterator<String> it = getVariantMap().keySet().iterator();
		for (int i = 0; it.hasNext(); i++) {
			variantDescriptors[i] = getVariantMap().get(it.next());
		}
		return variantDescriptors;
	}
	
	public String toString() {
		return "Name: " + this.getName() + ",\n" +
			   "Description: " + this.getDescription() + "\n" +
			   "Token enumeration class: " + this.fieldEnumerationClass.getName() + "\n" +
			   "tokenNames: " + this.getFieldNames() + "\n" +
			   "Variants: " + this.getVariantMap();
	}
	
	public String[] getFieldNames() {
		return this.fieldNames;
	}
	
	public void setFieldNames(String[] fieldNames) {
		this.fieldNames = fieldNames;
	}
	
	public Set<String> variantNames() {
		return Collections.unmodifiableSet(this.getVariantMap().keySet());
	}
	
	public VariantDescriptorImpl getVariant(String variantId) {
		VariantDescriptorImpl variantDescriptor = this.getVariantMap().get(variantId);
		if (variantDescriptor == null) {
			throw new IllegalArgumentException("No such variant: " + variantId);
		}
		return variantDescriptor;
	}
	
	protected void addVariant(VariantDescriptorImpl variantDescriptor) {
		this.getVariantMap().put(variantDescriptor.getName(), variantDescriptor);
	}
	
	protected void removeVariant(String variantName) {
		this.getVariantMap().remove(variantName);
	}

	public Class<Enum<?>> getFieldEnumerationClass() {
		return fieldEnumerationClass;
	}
	public void setFieldEnumerationClass(Class<Enum<?>> fieldEnumerationClass) {
		this.fieldEnumerationClass = fieldEnumerationClass;
		boolean isAliased = Aliased.class.isAssignableFrom(this.fieldEnumerationClass);
		
		Enum<?>[] constants = this.fieldEnumerationClass.getEnumConstants();
		List<String> tokenList = new ArrayList<String>(constants.length);
		for (Enum<?> constant: constants) {
			if (isAliased) {
				tokenList.add(((Aliased) constant).getAlias());
			} else {
				tokenList.add(constant.toString());
			}
		}
		this.fieldNames = tokenList.toArray(new String[tokenList.size()]);
	}
	public Map<String, VariantDescriptorImpl> getVariantMap() {
		if (this.variantMap == null) {
			this.variantMap = new LinkedHashMap<String, VariantDescriptorImpl>();
		}
		return variantMap;
	}
	public void setVariantMap(Map<String, VariantDescriptorImpl> variantMap) {
		this.variantMap = variantMap;
	}
}
