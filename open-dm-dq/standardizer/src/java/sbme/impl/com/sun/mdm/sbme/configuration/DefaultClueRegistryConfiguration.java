package com.sun.mdm.sbme.configuration;

import com.sun.inti.components.record.PropertyDescriptor;
import com.sun.inti.components.string.format.BooleanFormatter;
import com.sun.inti.components.string.tokenize.FixedFieldTokenizer.FieldDescriptor;
import com.sun.mdm.sbme.normalizer.ClueWord.ClueWordListFormatter;

public abstract class DefaultClueRegistryConfiguration extends ClueRegistryConfiguration {
	@Override
	protected FieldDescriptor[] getFieldDescriptors() {
		return new FieldDescriptor[] {
                new FieldDescriptor(0, 24, true), // Name
                new FieldDescriptor(24, 15, true), // Translation
                new FieldDescriptor(40, 45, false), // Words
                new FieldDescriptor(87, 1, false), // Translation expanded
			};
	}

	@Override
	protected PropertyDescriptor[] getPropertyDescriptors() {
		return new PropertyDescriptor[] {
                new PropertyDescriptor("name", null, null),
                new PropertyDescriptor("translation", null, null),
                new PropertyDescriptor("words", new ClueWordListFormatter(this.getTokenTypeClass(), true), null),
                new PropertyDescriptor("translationExpanded", new BooleanFormatter("*", " "), null),
			};
	}
	
	protected abstract Class<?extends Enum<?>> getTokenTypeClass();
}
