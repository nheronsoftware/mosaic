package com.sun.mdm.sbme.pattern;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.logging.Logger;

import com.sun.mdm.sbme.MdmStandardizationException;
import com.sun.mdm.sbme.normalizer.ClueWord;
import com.sun.mdm.sbme.normalizer.NormalizedToken;

public class DefaultPatternFinder<I extends Enum<I>, O extends Enum<O>> implements PatternFinder<I, O> {
	private OutputPatternBuilder<I, O> outputPatternBuilder;
	private OutputPatternScorer<I, O> outputPatternScorer;
    private OutputPatternPostprocessor<I, O>[] postprocessors;

    private final int maxScore = 90;

    private final Logger logger = Logger.getLogger(this.getClass().getName());

	public List<? extends OutputPattern<I, O>> findPatterns(List<NormalizedToken<I>> tokens) throws MdmStandardizationException {
        int highestScore = Integer.MIN_VALUE;

        List<?extends OutputPattern<I, O>> patternsFound = null;
        for (final List<I> inputTokenTypes : buildInputPatterns(tokens)) {
            final List<?extends OutputPattern<I, O>> candidatePatterns = this.outputPatternBuilder.buildOutputPatterns(inputTokenTypes);

            if (candidatePatterns.size() == 0) {
                this.logger.severe("No such pattern found: " + inputTokenTypes);
                throw new MdmStandardizationException("No such pattern found: " + inputTokenTypes);
            }

            final int score = this.outputPatternScorer.calculateScore(candidatePatterns);

            // Save the highest scoring pattern
            if (score > highestScore) {
                highestScore = score;
                patternsFound = candidatePatterns;
            }

            // TODO Determine and document where score limit 90 comes from
            if (score >= this.maxScore) {
                break;
            }
        }
        
        if (patternsFound == null) {
            this.logger.severe("No output patterns found");
            throw new MdmStandardizationException("No output patterns found");
        }
        
        if (this.postprocessors != null) {
        	for (OutputPatternPostprocessor<I, O> postprocessor: this.postprocessors) {
        		postprocessor.postprocess(patternsFound);
        	}
        }

        return patternsFound;
	}

    private List<List<I>> buildInputPatterns(final List<NormalizedToken<I>> tokens) {
        final List<List<I>> inputPatterns = new ArrayList<List<I>>();
        populateInputPatterns(tokens, 0, new Stack<I>(), inputPatterns);
        return inputPatterns;
    }

    private  void populateInputPatterns(final List<NormalizedToken<I>> tokens, final int index, final Stack<I> patternTypes, final List<List<I>> inputPatterns) {
        if (index == tokens.size()) {
            inputPatterns.add(new ArrayList<I>(patternTypes));
        } else {
            for (final ClueWord<I> clueWord : tokens.get(index).getClueWords()) {
                patternTypes.push(clueWord.getType());
                populateInputPatterns(tokens, index + 1, patternTypes, inputPatterns);
                patternTypes.pop();
            }
        }
    }

	public OutputPatternBuilder<I, O> getOutputPatternBuilder() {
		return outputPatternBuilder;
	}

	public void setOutputPatternBuilder(OutputPatternBuilder<I, O> outputPatternBuilder) {
		this.outputPatternBuilder = outputPatternBuilder;
	}

	public OutputPatternScorer<I, O> getOutputPatternScorer() {
		return outputPatternScorer;
	}

	public void setOutputPatternScorer(OutputPatternScorer<I, O> outputPatternScorer) {
		this.outputPatternScorer = outputPatternScorer;
	}

	public int getMaxScore() {
		return maxScore;
	}

	public OutputPatternPostprocessor<I, O>[] getPostprocessors() {
		return postprocessors;
	}

	public void setPostprocessors(OutputPatternPostprocessor<I, O>[] postprocessors) {
		this.postprocessors = postprocessors;
	}
}
