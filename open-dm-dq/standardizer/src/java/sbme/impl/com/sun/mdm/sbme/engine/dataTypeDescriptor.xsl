<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
				xmlns:exsl="http://exslt.org/common">
                
	<xsl:output method="xml"
				indent="yes"
				doctype-public="-//SPRING//DTD BEAN 2.0//EN"
				doctype-system="http://www.springframework.org/dtd/spring-beans-2.0.dtd"/>
				
	<xsl:template match="dataType">
		<beans>
			<bean id="dataType" class="com.sun.mdm.sbme.engine.DataTypeDescriptorImpl">
				<property name="name">
					<value>
						<xsl:value-of select="@name"/>
					</value>
				</property>
				<xsl:if test="description">
					<property name="description">
						<value>
							<xsl:value-of select="normalize-space(string(description))"/>
						</value>
					</property>
				</xsl:if>
				<xsl:choose>
					<xsl:when test="@fieldEnumerationClass">
					
					</xsl:when>
					<xsl:otherwise>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:choose>
					<xsl:when test="@fieldEnumerationClass">
						<property name="fieldEnumerationClass">
							<value>
                                <xsl:value-of select="@fieldEnumerationClass"/>
							</value>
						</property>
					</xsl:when>
					<xsl:otherwise>
						<property name="fieldNames">
							<list>
								<xsl:for-each select="outputField">
									<value>
										<xsl:value-of select="@name"/>
									</value>
								</xsl:for-each>
							</list>
						</property>
					</xsl:otherwise>
				</xsl:choose>
			</bean>
		</beans>
	</xsl:template>
</xsl:stylesheet>
