package com.sun.mdm.sbme.builder;

import com.sun.mdm.sbme.pattern.OutputPattern;

// FIXME Remove after testing against legacy version
public interface OutputPatternAware {
	public void setOutputPatterns(final OutputPattern<?, ?>[] outputPatterns);
}
