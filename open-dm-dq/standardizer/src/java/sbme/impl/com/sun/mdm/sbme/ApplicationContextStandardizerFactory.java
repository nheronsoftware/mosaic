package com.sun.mdm.sbme;

public abstract class ApplicationContextStandardizerFactory implements StandardizerFactory {
	private final ApplicationContextFactory factory;
	
	public ApplicationContextStandardizerFactory() {
		String resourceFormat = "/META-INF/mdm/sbme/%s/standardizerBeans%s.xml";
		if (this.getVariant() == null) {
			resourceFormat = "/META-INF/mdm/sbme/standardizerBeans%s.xml";
		}
		
		Thread currentThread = Thread.currentThread();
		ClassLoader contextClassLoader = currentThread.getContextClassLoader();
		try {
			currentThread.setContextClassLoader(this.getClass().getClassLoader());
			this.factory = new ApplicationContextFactory(this.getType(), this.getVariant(), resourceFormat, "standardizer");
		} catch (Throwable t) {
			throw new RuntimeException("Error creating application context: " + t, t);
		} finally {
			currentThread.setContextClassLoader(contextClassLoader);
		}
	}
	
	protected abstract String getType();
	protected abstract String getVariant();

	public Standardizer newStandardizer() {
		return (Standardizer) this.factory.getBean();
	}
}
