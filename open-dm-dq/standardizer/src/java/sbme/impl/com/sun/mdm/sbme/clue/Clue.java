/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.clue;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.sun.mdm.sbme.normalizer.ClueWord;

/**
 *
 *  @author Ricardo Rocha (ricardo.rocha@sun.com)
 */
public class Clue<I extends Enum<I>> implements Serializable {
    private String name;
    private String translation;
    private boolean translationExpanded;
    private List<ClueWord<I>> words = new ArrayList<ClueWord<I>>();
    
    @Override
    public String toString() {
    	return name + " (" + translation + "): " + translationExpanded + ". " + words;
    }

    @Override
    public boolean equals(final Object o) {
    	@SuppressWarnings("unchecked")
    	boolean isEqual = this.name.equals(((Clue<I>) o).name);
        return isEqual;
    }

    @Override
    public int hashCode() {
        return this.name.hashCode();
    }

    public String getName() {
        if (this.name == null) {
            this.name = "";
        }
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getTranslation() {
        if (this.translation == null) {
            this.translation = "";
        }
        return this.translation;
    }

    public void setTranslation(final String translation) {
        this.translation = translation;
    }

    public boolean isTranslationExpanded() {
        return this.translationExpanded;
    }

    public void setTranslationExpanded(final boolean translationExpanded) {
        this.translationExpanded = translationExpanded;
    }

    public List<ClueWord<I>> getWords() {
        return this.words;
    }

    public void setWords(final List<ClueWord<I>> words) {
        this.words = words;
    }
}
