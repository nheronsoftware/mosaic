package com.sun.mdm.sbme;

public interface StandardizerFactory {
	public Standardizer newStandardizer();
}
