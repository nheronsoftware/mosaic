package com.sun.mdm.sbme;

import java.io.File;

public interface Descriptor {
	public String getName();
	public String getDescription();
	
	public File[] getJarFiles();
	public File[] getResourceFiles();
}
