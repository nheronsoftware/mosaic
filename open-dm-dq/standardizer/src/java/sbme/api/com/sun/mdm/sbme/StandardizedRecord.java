package com.sun.mdm.sbme;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.stc.sbme.api.SbmeStandRecord;

public abstract class StandardizedRecord {
    private Map<String, String> properties = new HashMap<String, String>();
    
    public abstract String getType();
    
    public Set<String> propertyNames() {
    	return this.properties.keySet();
    }

    public String get(String propertyName) {
    	return this.properties.get(propertyName);
    }

    public String get(String propertyName, String defaultValue) {
    	String propertyValue = this.properties.get(propertyName);
    	if (propertyValue == null) {
    		if (defaultValue != null) {
        		propertyValue = defaultValue;
        		this.properties.put(propertyName, propertyValue);
    		}
    	}
    	return propertyValue;
    }

    public void set(String propertyName, String propertyValue) {
    	this.properties.put(propertyName, propertyValue);
    }
    
    @Override
	public String toString() {
    	return this.getType() + ": " + this.properties;
    }

    // FIXME Remove after decoupling test targets from record representation
    public abstract String getSignature();
    
    // FIXME Remove after fixing SbmeStandardizerAdapter to use new StandardizedRecordImpl
    public abstract SbmeStandRecord toSbmeStandRecord();
}
