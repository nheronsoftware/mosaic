package com.sun.mdm.sbme;

import java.io.File;
import java.util.Set;
import java.util.jar.JarFile;


public interface StandardizationEngine {
	public void initialize(File repositoryDirectory);

	public void start() throws Exception;

	public Descriptor deploy(JarFile jarFile) throws Exception;

	public DataTypeDescriptor deployDataType(JarFile jarFile) throws Exception;

	public VariantDescriptor deployVariant(JarFile jarFile) throws Exception;

	public DataTypeDescriptor undeployDataType(String dataTypeName) throws Exception;

	public VariantDescriptor undeployVariant(String dataTypeName, String variantName) throws Exception;

	public void stop() throws Exception;

	public Set<String> dataTypeNames();

	public DataTypeDescriptor getDataTypeDescriptor(String dataTypeName) throws Exception;

	public Standardizer getStandardizer(String dataTypeName, String variantName) throws Exception;
}