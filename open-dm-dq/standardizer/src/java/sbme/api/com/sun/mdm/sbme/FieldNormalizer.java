package com.sun.mdm.sbme;

public interface FieldNormalizer {
	public String normalize(String fieldName, String fieldValue);
}
