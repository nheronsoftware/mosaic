package com.sun.mdm.sbme.test;

import java.io.File;
import java.util.jar.JarFile;
import java.util.logging.Logger;

import com.sun.mdm.sbme.DataTypeDescriptor;
import com.sun.mdm.sbme.StandardizerIntrospector;
import com.sun.mdm.sbme.VariantDescriptor;
import com.sun.mdm.sbme.util.StandardizerUtils;

public class DefaultStandardizationIntrospectorTester {
	private static final Logger logger = Logger.getLogger(DefaultStandardizationIntrospectorTester.class.getName());
	
	public static void main(String args[]) throws Exception {
		StandardizerIntrospector introspector = StandardizerUtils.getStandardizerIntrospector();
		
		//Simulates the eView project
		File projectRepository = new File(System.getProperty("java.io.tmpdir") + File.separator + "testRepository");
		logger.info("Creating repository: " + projectRepository.getAbsolutePath());
		StandardizerUtils.delete(projectRepository);
		projectRepository.mkdirs();
		logger.info("Setting repository: " + projectRepository.getAbsolutePath());
		introspector.setRepository(projectRepository);
		
		//Simulates the eView repository (<netbeans>/soa1/modules/ext/eview/standardizer/deployment),
		//which would contain the default data types (i.e. mdm-address-deployment.jar, mdm-fr-address-deployment.jar)
		File deploymentRepository = new File(System.getProperty("java.io.tmpdir") + File.separator + "deployment");
		StandardizerUtils.delete(deploymentRepository);
		deploymentRepository.mkdirs();
		logger.info("Created deployment repository: " + deploymentRepository.getAbsolutePath());
		StandardizerUtils.copy(new File("dist/sbme-address-deployment.zip"), new File(deploymentRepository, "sbme-address-deployment.zip"));
		StandardizerUtils.copy(new File("dist/sbme-fr-address-deployment.zip"), new File(deploymentRepository, "sbme-fr-address-deployment.zip"));
		StandardizerUtils.copy(new File("dist/sbme-businessname-deployment.zip"), new File(deploymentRepository, "sbme-businessname-deployment.zip"));
		StandardizerUtils.copy(new File("dist/mdm-personname-deployment.zip"), new File(deploymentRepository, "mdm-personname-deployment.zip"));

		logger.info("Deploying address...");
		introspector.importDataType(new JarFile(new File(deploymentRepository, "sbme-address-deployment.zip")));
		logger.info("Deploying French...");
		introspector.importVariant(new JarFile(new File(deploymentRepository, "sbme-fr-address-deployment.zip")));
		
		logger.info("Deploying business name...");
		introspector.importDataType(new JarFile(new File(deploymentRepository, "sbme-businessname-deployment.zip")));
		
		logger.info("Deploying person name...");
		introspector.importDataType(new JarFile(new File(deploymentRepository, "mdm-personname-deployment.zip")));
		
		logger.info("Importing directory: " + deploymentRepository);
		DataTypeDescriptor[] dataTypeDescriptors = introspector.importDirectory(deploymentRepository);
		for (DataTypeDescriptor dataTypeDescriptor: dataTypeDescriptors) {
			System.out.println("Data type name: " + dataTypeDescriptor.getName());
			VariantDescriptor[] variantDescriptors = dataTypeDescriptor.variants();
			for (VariantDescriptor variantDescriptor: variantDescriptors) {
				System.out.println("\tVariant name: " + variantDescriptor.getName() + " (of data type " + variantDescriptor.getDataTypeName() + ")");
			}
			System.out.println("Data type field names: ");
			for (String fieldName: dataTypeDescriptor.getFieldNames()) {
				System.out.println("\t" + fieldName);
			}
			
		}
		introspector.takeSnapshot(new File(System.getProperty("java.io.tmpdir")));
	}
}
