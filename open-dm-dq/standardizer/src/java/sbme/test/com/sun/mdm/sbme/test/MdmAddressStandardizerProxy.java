/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.test;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.stc.sbme.api.SbmeStandRecord;
import com.sun.mdm.sbme.MdmStandardizationException;
import com.sun.mdm.sbme.StandardizedRecord;
import com.sun.mdm.sbme.StandardizerFactory;
import com.sun.mdm.sbme.datatype.address.variant.au.AUStandardizerFactory;
import com.sun.mdm.sbme.datatype.address.variant.fr.FRStandardizerFactory;
import com.sun.mdm.sbme.datatype.address.variant.uk.UKStandardizerFactory;
import com.sun.mdm.sbme.datatype.address.variant.us.USStandardizerFactory;

public class MdmAddressStandardizerProxy implements StandardizerProxy {
    private static final Map<String, StandardizerFactory> standardizerFactories = new HashMap<String, StandardizerFactory>();
    static {
    	standardizerFactories.put("US", new USStandardizerFactory());
    	standardizerFactories.put("UK", new UKStandardizerFactory());
    	standardizerFactories.put("AU", new AUStandardizerFactory());
    	standardizerFactories.put("FR", new FRStandardizerFactory());
    }

    public StandardizedRecord standardize(final String record, final String domain) throws Exception {
        try {
        	return standardizerFactories.get(domain.toUpperCase()).newStandardizer().standardize(record);
        } catch (final MdmStandardizationException mse) {
            throw new TestException(mse.getMessage(), mse);
        } catch (final Exception e) {
            throw e;
        }
    }

    public String getSignature(final Object source) {
        return ((StandardizedRecord) source).getSignature();
    }

    public Map<String, String> getFields(final Object source) {
        final Map<String, String> fields = new LinkedHashMap<String, String>();
    	SbmeStandRecord record = ((StandardizedRecord) source).toSbmeStandRecord();
    	
    	for (Object fieldName: record.getAllFields()) {
    		fields.put(fieldName.toString(), record.getValue(fieldName.toString()));
    	}

        return fields;
    }
}
