package com.sun.mdm.sbme.test;

import java.util.Set;

import com.stc.sbme.api.SbmeStandRecord;
import com.sun.inti.components.util.ClassUtils;
import com.sun.mdm.sbme.StandardizedRecord;
import com.sun.mdm.sbme.Standardizer;
import com.sun.mdm.sbme.StandardizerRegistry;

public class DefaultStandardizerRegistryTester {

	public static void main(String args[]) throws Exception {
    	StandardizerRegistry registry = (StandardizerRegistry) ClassUtils.loadServiceClass(StandardizerRegistry.class);
		
		Standardizer standardizer = registry.lookup("address", "us");
		
		System.out.println("1001 LINCOLN STREET B 202");
		
		StandardizedRecord record = standardizer.standardize("1001 LINCOLN STREET B 202");
		SbmeStandRecord sbmeRecord = record.toSbmeStandRecord();
		System.out.println(record);
		
		Set<String> fields = sbmeRecord.getAllFields();
		for (String field: fields) {
			System.out.println(field + ": " + sbmeRecord.getValue(field));
			if (!sbmeRecord.getValue(field).equals(record.get(field))) {
				throw new Exception("FAILURE! Value mismatch for field (" + field + "). Sbme value (" + sbmeRecord.getValue(field) + ") does not match (" + record.get(field) + ").");
			}
		}
	}
}
