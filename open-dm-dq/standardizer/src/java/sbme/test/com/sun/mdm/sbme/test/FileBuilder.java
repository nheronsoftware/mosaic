/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

/**
 * 
 */
package com.sun.mdm.sbme.test;

import java.io.PrintWriter;
import java.util.Map;

public abstract class FileBuilder {
    protected PrintWriter out;
    protected PrintWriter err;

    public void init(final PrintWriter output, final PrintWriter error) throws Exception {
        this.out = output;
        this.err = error;
        this.begin();
    }

    protected void begin() throws Exception {
    }

    public Object parseRecord(final String record) throws Exception {
        return record;
    }

    public String getString(final Object parsedRecord) throws Exception {
        return (String) parsedRecord;
    }

    public abstract void processRecord(int recordNumber, Map<String, String> standardizedFields, Object parsedRecord, String signature) throws Exception;

    public void end() throws Exception {
    }
}