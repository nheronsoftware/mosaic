/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.test;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.sun.mdm.sbme.MdmStandardizationException;
import com.sun.mdm.sbme.StandardizedRecord;
import com.sun.mdm.sbme.StandardizerFactory;
import com.sun.mdm.sbme.datatype.businessname.BusinessName;
import com.sun.mdm.sbme.datatype.businessname.registry.BeanBusinessStandardizerRegistry;
import com.sun.mdm.sbme.datatype.businessname.registry.BusinessStandardizerRegistry;
import com.sun.mdm.sbme.datatype.businessname.variant.generic.GenericStandardizerFactory;

public class MdmBusinessNameStandardizerProxy implements StandardizerProxy {
    private final BusinessStandardizerRegistry businessNameStandardizerRegistry = new BeanBusinessStandardizerRegistry();
    private static final Map<String, StandardizerFactory> standardizerFactories = new HashMap<String, StandardizerFactory>();
    static {
    	standardizerFactories.put("GENERIC", new GenericStandardizerFactory());
    }

    public StandardizedRecord standardize(final String record, final String domain) throws Exception {
        try {
            if (domain.toUpperCase().equals("GENERIC")) {
            	return standardizerFactories.get(domain.toUpperCase()).newStandardizer().standardize(record);
            } else {
            	return businessNameStandardizerRegistry.lookup(domain.toUpperCase()).standardize(record);            	
            }
        } catch (final MdmStandardizationException mse) {
            throw new TestException(mse.getMessage(), mse);
        } catch (final Exception e) {
            throw e;
        }
    }
    
    public String getSignature(final Object source) {
        return ((BusinessName) source).getSignature().trim();
    }

    public Map<String, String> getFields(final Object source) {
        final BusinessName businessName = (BusinessName) source;
        final Map<String, String> fields = new HashMap<String, String>();
        @SuppressWarnings("unchecked")
        Set<String> allFields = businessName.propertyNames();
        for (final String name : allFields) {
            fields.put(name, businessName.get(name));
        }
        return fields;
    }
}
