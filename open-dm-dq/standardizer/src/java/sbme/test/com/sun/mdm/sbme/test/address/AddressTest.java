package com.sun.mdm.sbme.test.address;

import java.io.PrintWriter;
import java.io.StringWriter;

import junit.framework.TestCase;
import bmsi.util.DiffPrint;

import com.sun.mdm.sbme.test.SignatureFileBuilder;
import com.sun.mdm.sbme.test.TestFileBuilder;

public class AddressTest extends TestCase {

    private String variants;
//    @Before
    public void setUp() throws Exception {
        this.variants = "US,UK,FR,AU";
        System.out.println("setUp()");
        
        SignatureFileBuilder signatureBuilder = new SignatureFileBuilder();
        signatureBuilder.buildSignatureFiles("com.sun.mdm.sbme.test.MdmAddressStandardizerProxy",
                this.variants,
                "matches",
                "signature-mdm",
                "signature-mdm-err",
                false);
        
        TestFileBuilder testBuilder = new TestFileBuilder();
        testBuilder.buildFiles("com.sun.mdm.sbme.test.MdmAddressStandardizerProxy",
                this.variants,
                "signature-mdm",
                "test-mdm",
                "test-mdm-err",
                false);
    }
    
//    @Test
    public void testDiffSignatureFiles() throws Exception {
        System.out.println("testDiffSignatureFiles()");
        String[] variants = this.variants.split(",");
        
        for (String variant: variants) {
            String[] myStrings = new String[3];
            variant = variant.toLowerCase();
            myStrings[0] = "-c";
            myStrings[1] = "test-sbme-" + variant + ".txt";
            myStrings[2] = "test-mdm-" + variant + ".txt";
                        
            StringWriter myStringWriter = new StringWriter();
            PrintWriter  myWriter       = new PrintWriter(myStringWriter);
            DiffPrint    myDiffPrint    = new DiffPrint();
            myDiffPrint.doWork(myStrings, myWriter);
    
            myWriter.flush();
            myWriter.close();
            String myDiff = myStringWriter.toString();
    
            if (myDiff.length() > 0) {
                System.out.println("Differences found: " + myDiff);
            }
            assertTrue(myDiff.length() == 0);
        }
    }
}
