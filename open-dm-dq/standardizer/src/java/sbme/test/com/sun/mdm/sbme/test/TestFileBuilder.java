/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

public class TestFileBuilder extends FileBuilder {
    private static final Logger logger = Logger.getLogger(TestFileBuilder.class.getName());

    @Override
    public Object parseRecord(final String record) throws Exception {
        final String[] fields = record.split("\t");
        if (fields.length != 2) {
            logger.warning("Field count mismatch, expected 2, got " + fields.length);
            return null;
        }
        return fields;
    }

    @Override
    public String getString(final Object parsedRecord) throws Exception {
        final String[] fields = (String[]) parsedRecord;
        return fields[0];
    }

    @Override
    public void processRecord(final int recordNumber, final Map<String, String> standardizedFields, final Object parsedRecord, final String signature) throws Exception {
        final String[] recordFields = (String[]) parsedRecord;
        final String string = recordFields[0];
        final String expectedSignature = recordFields[1];

        if (!signature.equals(expectedSignature)) {
            final String messageText = recordNumber + "\t" + string + "\t" + "InputPattern signature mismatch, " + "expected '" + expectedSignature + "', got '" + signature + "'";
            logger.warning(messageText);
            this.err.println(messageText);
        }

        this.out.println(string + "\t" + signature);
        List<String> fields = new ArrayList<String>(standardizedFields.keySet());
        Collections.sort(fields);
        for (final String fieldName : fields) {
            final String fieldValue = standardizedFields.get(fieldName);
            if (fieldValue != null) {
                this.out.println(fieldName + "\t" + fieldValue);
            }
        }
        this.out.println();
    }
    
    public void buildFiles(String standardizerClassName,
                           String variantCodes,
                           String signatureFilePrefix,
                           String testFilePrefix,
                           String errorFilePrefix,
                           boolean force) throws Exception {
        
        final FileBuildingFramework builder = new FileBuildingFramework(standardizerClassName,
                                                                        variantCodes,
                                                                        signatureFilePrefix,
                                                                        testFilePrefix,
                                                                        errorFilePrefix,
                                                                        force,
                                                                        new TestFileBuilder());
        builder.buildFiles();
    }

    public static void main(final String[] args) throws Exception {
        final FileBuildingFramework builder = new FileBuildingFramework(args, new TestFileBuilder());
        builder.buildFiles();
    }
}
