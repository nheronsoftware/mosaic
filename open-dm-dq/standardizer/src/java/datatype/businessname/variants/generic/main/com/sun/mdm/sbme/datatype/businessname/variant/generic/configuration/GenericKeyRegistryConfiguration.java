package com.sun.mdm.sbme.datatype.businessname.variant.generic.configuration;

import java.util.List;
import java.util.Map;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Bean;
import com.sun.mdm.sbme.datatype.businessname.BusinessNameIndustryType;
import com.sun.mdm.sbme.datatype.businessname.CityStateType;
import com.sun.mdm.sbme.datatype.businessname.configuration.KeyRegistryConfiguration;
import com.sun.mdm.sbme.datatype.businessname.normalizer.AliasTypeRegistry;

@Configuration
/**
 * Configuration class responsible for loading all required data
 * into their respective components.
 * 
 * @author mdm team
 */
public class GenericKeyRegistryConfiguration extends KeyRegistryConfiguration {

    @Bean
    public List<String> adjectiveTypes() {
        return super.adjectiveTypes();
    }

    @Bean
    public AliasTypeRegistry aliasTypeRegistry() {
        return super.aliasTypeRegistry();
    }

    @Bean
    public Map<String, String> associations() {
        return super.associations();
    }

    @Bean
    public List<String> generalTerms() {
        return super.generalTerms();
    }

    @Bean
    public Map<String, List<CityStateType>> cityStateNames() {
        return super.cityStateNames();
    }

    @Bean
    public Map<String, String> formerNames() {
        return super.formerNames();
    }

    @Bean
    public Map<String, String> primaryNames() {
        return super.primaryNames();
    }

    @Bean
    public Map<String, String> mergerNames() {
        return super.mergerNames();
    }

    @Bean
    public Map<String, String> newNames() {
        return super.newNames();
    }

    @Bean
    public List<String> countryNationalities() {
        return super.countryNationalities();
    }

    @Bean
    public List<String> countryNames() {
        return super.countryNames();
    }

    @Bean
    public Map<String, String> industryCatagoryCodes() {
        return super.industryCatagoryCodes();
    }

    @Bean
    public Map<String, BusinessNameIndustryType> industryRegistry() {
        return super.industryRegistry();
    }

    @Bean
    public Map<String, String> organizations() {
        return super.organizations();
    }

    @Bean
    public List<String> connectorTokens() {
        return super.connectorTokens();
    }

    public String getVariantName() {
        return "generic";
    }
}
