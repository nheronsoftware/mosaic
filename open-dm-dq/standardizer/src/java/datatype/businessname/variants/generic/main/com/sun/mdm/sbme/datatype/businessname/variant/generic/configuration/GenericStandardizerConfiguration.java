package com.sun.mdm.sbme.datatype.businessname.variant.generic.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Bean;
import org.springframework.beans.factory.annotation.Autowired;
import com.sun.mdm.sbme.Standardizer;
import com.sun.mdm.sbme.datatype.businessname.DefaultBusinessNameStandardizer;
import com.sun.mdm.sbme.datatype.businessname.builder.BusinessNameBuilder;
import com.sun.mdm.sbme.datatype.businessname.normalizer.BusinessNameNormalizer;
import com.sun.mdm.sbme.datatype.businessname.parser.BusinessNameParser;
import com.sun.mdm.sbme.datatype.businessname.pattern.BusinessNamePatternFinder;

@Configuration
/**
 * Configuration class responsible constructing a <code>Standardizer</code>.
 * 
 * @author mdm team
 */
public class GenericStandardizerConfiguration {

    @Bean
    /**
     * A <code>Standardizer</code> configured with components necessary
     * to standardize a free form business name.
     * 
     * @return a configured <code>Standardizer</code> capable of standardizing
     * free form business names
     */
    public Standardizer standardizer() {
        DefaultBusinessNameStandardizer standardizer = new DefaultBusinessNameStandardizer();

        standardizer.setNormalizer(normalizer);
        standardizer.setParser(parser);
        standardizer.setPatternFinder(patternFinder);
        standardizer.setNameBuilder(nameBuilder);

        return standardizer;
    }
    @Autowired
    public BusinessNameNormalizer normalizer;
    @Autowired
    public BusinessNameParser parser;
    @Autowired
    public BusinessNamePatternFinder patternFinder;
    @Autowired
    public BusinessNameBuilder nameBuilder;
}
