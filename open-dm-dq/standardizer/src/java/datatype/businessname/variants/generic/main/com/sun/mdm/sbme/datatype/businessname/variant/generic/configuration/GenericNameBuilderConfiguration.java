package com.sun.mdm.sbme.datatype.businessname.variant.generic.configuration;

import java.util.ArrayList;
import java.util.List;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Bean;
import com.sun.inti.components.string.match.OrCompositeStringMatcher;
import com.sun.inti.components.string.match.StringMatcher;
import com.sun.mdm.sbme.datatype.businessname.InputTokenType;
import com.sun.mdm.sbme.datatype.businessname.builder.BusinessNameBuilder;
import com.sun.mdm.sbme.datatype.businessname.builder.CompoundInputTokenTypeProcessor;
import com.sun.mdm.sbme.datatype.businessname.builder.DefaultBusinessNameBuilder;
import com.sun.mdm.sbme.datatype.businessname.builder.DefaultInputTokenTypeProcessor;
import com.sun.mdm.sbme.datatype.businessname.builder.InputTokenTypeProcessor;
import com.sun.mdm.sbme.datatype.businessname.matcher.InputTokenTypeMatcher;

@Configuration
/**
 * Configuration class responsible for constructing a <code>BusinessNameBuilder</code>.
 * 
 * @author mdm team
 */
public class GenericNameBuilderConfiguration {

    private DefaultBusinessNameBuilder nameBuilder;

    @Bean
    /**
     * A <code>BusinessNameBuilder</code> configured with a set of <code>InputTokenTypeMatcher's</code>
     * and a set of <code>InuptTokenTypeProcessor's</code>.
     *
     * @return a <code>DefaultBusinessNameBuilder</code>
     */
    public BusinessNameBuilder nameBuilder() {
        this.nameBuilder = new DefaultBusinessNameBuilder();

        List<InputTokenTypeProcessor> processors = new ArrayList<InputTokenTypeProcessor>();

        processors.add(new DefaultInputTokenTypeProcessor(new InputTokenTypeMatcher(InputTokenType.PNT), InputTokenType.PNT));
        processors.add(new DefaultInputTokenTypeProcessor(new InputTokenTypeMatcher(InputTokenType.ORT), InputTokenType.ORT));
        processors.add(new DefaultInputTokenTypeProcessor(new InputTokenTypeMatcher(InputTokenType.AST), InputTokenType.AST));
        processors.add(new DefaultInputTokenTypeProcessor(new InputTokenTypeMatcher(InputTokenType.IDT), InputTokenType.IDT));
        processors.add(new DefaultInputTokenTypeProcessor(new InputTokenTypeMatcher(InputTokenType.URL), InputTokenType.URL));
        processors.add(new DefaultInputTokenTypeProcessor(new InputTokenTypeMatcher(InputTokenType.ALT), InputTokenType.PNT));

        List<StringMatcher> matchers = new ArrayList<StringMatcher>();
        matchers.add(new InputTokenTypeMatcher(InputTokenType.NF));
        matchers.add(new InputTokenTypeMatcher(InputTokenType.NF_NF));
        processors.add(new DefaultInputTokenTypeProcessor(new OrCompositeStringMatcher(matchers), InputTokenType.NF));

        this.nameBuilder.setProcessor(new CompoundInputTokenTypeProcessor(processors));
        return this.nameBuilder;
    }
}
