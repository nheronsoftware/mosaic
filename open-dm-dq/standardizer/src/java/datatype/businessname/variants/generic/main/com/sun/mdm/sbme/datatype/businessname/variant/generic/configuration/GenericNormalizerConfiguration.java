package com.sun.mdm.sbme.datatype.businessname.variant.generic.configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Bean;
import org.springframework.beans.factory.annotation.Autowired;
import com.sun.inti.components.string.transform.DiacriticalMarkStringTransformer;
import com.sun.mdm.sbme.datatype.businessname.BusinessNameIndustryType;
import com.sun.mdm.sbme.datatype.businessname.CityStateType;
import com.sun.mdm.sbme.datatype.businessname.InputTokenType;
import com.sun.mdm.sbme.datatype.businessname.matcher.AliasTypeTokenMatcher;
import com.sun.mdm.sbme.datatype.businessname.matcher.AmpersandTokenMatcher;
import com.sun.mdm.sbme.datatype.businessname.matcher.CityStateTokenMatcher;
import com.sun.mdm.sbme.datatype.businessname.matcher.CompoundTokenMatcher;
import com.sun.mdm.sbme.datatype.businessname.matcher.ConnectorTokenMatcher;
import com.sun.mdm.sbme.datatype.businessname.matcher.GlueTokenMatcher;
import com.sun.mdm.sbme.datatype.businessname.matcher.ListBasedTokenMatcher;
import com.sun.mdm.sbme.datatype.businessname.matcher.MapBasedTokenMatcher;
import com.sun.mdm.sbme.datatype.businessname.matcher.MergerNameTokenMatcher;
import com.sun.mdm.sbme.datatype.businessname.matcher.MergerTokenMatcher;
import com.sun.mdm.sbme.datatype.businessname.matcher.MultiCharacterOrLetterTokenMatcher;
import com.sun.mdm.sbme.datatype.businessname.matcher.NullInputTokenTypeMatcher;
import com.sun.mdm.sbme.datatype.businessname.matcher.SingleCharacterTokenMatcher;
import com.sun.mdm.sbme.datatype.businessname.matcher.StringBasedTokenMatcher;
import com.sun.mdm.sbme.datatype.businessname.matcher.TokenMatcher;
import com.sun.mdm.sbme.datatype.businessname.normalizer.AliasTypeRegistry;
import com.sun.mdm.sbme.datatype.businessname.normalizer.BusinessNameNormalizer;
import com.sun.mdm.sbme.datatype.businessname.normalizer.DefaultBusinessNameNormalizer;
import com.sun.mdm.sbme.datatype.businessname.normalizer.handler.CompoundTokenHandler;
import com.sun.mdm.sbme.datatype.businessname.normalizer.handler.ConditionalTokenHandler;
import com.sun.mdm.sbme.datatype.businessname.normalizer.handler.IndustrySectorTokenHandler;
import com.sun.mdm.sbme.datatype.businessname.normalizer.handler.TokenHandler;
import com.sun.mdm.sbme.datatype.businessname.normalizer.handler.TokenHandlerPair;
import com.sun.mdm.sbme.datatype.businessname.processor.AliasTypeTokenProcessor;
import com.sun.mdm.sbme.datatype.businessname.processor.AssociationTokenProcessor;
import com.sun.mdm.sbme.datatype.businessname.processor.GlueTokenProcessor;
import com.sun.mdm.sbme.datatype.businessname.processor.IndustryTypeTokenProcessor;
import com.sun.mdm.sbme.datatype.businessname.processor.InputTokenTypeProcessor;
import com.sun.mdm.sbme.datatype.businessname.processor.MergerNameTokenProcessor;
import com.sun.mdm.sbme.datatype.businessname.processor.MergerTokenProcessor;
import com.sun.mdm.sbme.datatype.businessname.processor.OrganizationTokenProcessor;
import com.sun.mdm.sbme.datatype.businessname.processor.PrimaryMergerNameTokenProcessor;

@Configuration
/**
 * Configuration class responsible for constructing a <code>BusinessNameNormalizer</code>.
 * 
 * @author mdm team
 */
public class GenericNormalizerConfiguration {

    @Bean
    /**
     * A <code>BusinessNameNormalizer</code> configured with a set of
     * <code>TokenHandler's</code>.
     *
     * @return a configured <code>BusinessNameNormalizer</code>
     */
    public BusinessNameNormalizer normalizer() {
        DefaultBusinessNameNormalizer normalizer = new DefaultBusinessNameNormalizer();
        CompoundTokenHandler compoundTokenHandler = new CompoundTokenHandler();
        List<TokenHandler> handlers = new ArrayList<TokenHandler>();

        ConditionalTokenHandler conditionalTokenHandler = new ConditionalTokenHandler();
        conditionalTokenHandler.setMaxImageSize(2);
        conditionalTokenHandler.setTransformer(diacriticalMarkStringTransformer);

        conditionalTokenHandler.setPairs(getPairs());

        IndustrySectorTokenHandler industrySectorTokenHandler = new IndustrySectorTokenHandler();
        industrySectorTokenHandler.setIndustryCategoryCodes(industryCatagoryCodes);

        handlers.add(conditionalTokenHandler);
        handlers.add(industrySectorTokenHandler);

        compoundTokenHandler.setHandlers(handlers);
        normalizer.setTokenHandler(compoundTokenHandler);

        return normalizer;
    }

    private List<TokenHandlerPair> getPairs() {
        List<TokenHandlerPair> pairs = new ArrayList<TokenHandlerPair>();

        pairs.add(new TokenHandlerPair(new CompoundTokenMatcher(new ArrayListExtended<TokenMatcher>().addExtended(new MultiCharacterOrLetterTokenMatcher()).
                addExtended(new ConnectorTokenMatcher(connectorTokens))), new InputTokenTypeProcessor(InputTokenType.CTT)));

        pairs.add(new TokenHandlerPair(new CompoundTokenMatcher(new ArrayListExtended<TokenMatcher>().addExtended(new MultiCharacterOrLetterTokenMatcher()).
                addExtended(new StringBasedTokenMatcher("AND"))), new InputTokenTypeProcessor(InputTokenType.GLU)));

        TokenHandlerPair pair = new TokenHandlerPair(new CompoundTokenMatcher(new ArrayListExtended<TokenMatcher>().addExtended(new MultiCharacterOrLetterTokenMatcher()).
                addExtended(new MapBasedTokenMatcher(primaryNames))),
                new PrimaryMergerNameTokenProcessor(primaryNames, newNames, aliasTypeRegistry, diacriticalMarkStringTransformer));
        pair.setMultiTokenProcessor(true);
        pairs.add(pair);

        pair = new TokenHandlerPair(new CompoundTokenMatcher(new ArrayListExtended<TokenMatcher>().addExtended(new MultiCharacterOrLetterTokenMatcher()).
                addExtended(new MergerNameTokenMatcher(mergerNames))),
                new MergerNameTokenProcessor(mergerNames, aliasTypeRegistry, newNames, diacriticalMarkStringTransformer));
        pair.setMultiTokenProcessor(true);
        pairs.add(pair);

        pairs.add(new TokenHandlerPair(new CompoundTokenMatcher(new ArrayListExtended<TokenMatcher>().addExtended(new MultiCharacterOrLetterTokenMatcher()).
                addExtended(new ListBasedTokenMatcher(generalTerms))), new InputTokenTypeProcessor(InputTokenType.BCT)));

        pairs.add(new TokenHandlerPair(new CompoundTokenMatcher(new ArrayListExtended<TokenMatcher>().addExtended(new MultiCharacterOrLetterTokenMatcher()).
                addExtended(new AliasTypeTokenMatcher(aliasTypeRegistry))), new AliasTypeTokenProcessor(aliasTypeRegistry, diacriticalMarkStringTransformer)));

        pairs.add(new TokenHandlerPair(new CompoundTokenMatcher(new ArrayListExtended<TokenMatcher>().addExtended(new MultiCharacterOrLetterTokenMatcher()).
                addExtended(new ListBasedTokenMatcher(countryNames))), new InputTokenTypeProcessor(InputTokenType.CNT)));

        pairs.add(new TokenHandlerPair(new CompoundTokenMatcher(new ArrayListExtended<TokenMatcher>().addExtended(new MultiCharacterOrLetterTokenMatcher()).
                addExtended(new ListBasedTokenMatcher(countryNationalities))), new InputTokenTypeProcessor(InputTokenType.NAT)));

        pair = new TokenHandlerPair(new CompoundTokenMatcher(new ArrayListExtended<TokenMatcher>().addExtended(new MultiCharacterOrLetterTokenMatcher()).
                addExtended(new CityStateTokenMatcher(cityStateNames))),
                new InputTokenTypeProcessor(InputTokenType.CST));
        pair.setMultiTokenProcessor(true);
        pairs.add(pair);

        pair = new TokenHandlerPair(new CompoundTokenMatcher(new ArrayListExtended<TokenMatcher>().addExtended(new MultiCharacterOrLetterTokenMatcher()).
                addExtended(new MapBasedTokenMatcher(industryRegistry))),
                new IndustryTypeTokenProcessor(industryRegistry, adjectiveTypes, diacriticalMarkStringTransformer));
        pair.setMultiTokenProcessor(true);
        pairs.add(pair);

        pairs.add(new TokenHandlerPair(new CompoundTokenMatcher(new ArrayListExtended<TokenMatcher>().addExtended(new MultiCharacterOrLetterTokenMatcher()).
                addExtended(new ListBasedTokenMatcher(adjectiveTypes))), new InputTokenTypeProcessor(InputTokenType.AJT)));

        pairs.add(new TokenHandlerPair(new CompoundTokenMatcher(new ArrayListExtended<TokenMatcher>().addExtended(new MultiCharacterOrLetterTokenMatcher()).
                addExtended(new MapBasedTokenMatcher(associations))), new AssociationTokenProcessor(associations)));

        pair = new TokenHandlerPair(new CompoundTokenMatcher(new ArrayListExtended<TokenMatcher>().addExtended(new MultiCharacterOrLetterTokenMatcher()).
                addExtended(new MapBasedTokenMatcher(organizations))), new OrganizationTokenProcessor(organizations));
        pair.setMultiTokenProcessor(true);
        pairs.add(pair);

        pairs.add(new TokenHandlerPair(new CompoundTokenMatcher(new ArrayListExtended<TokenMatcher>().addExtended(new MultiCharacterOrLetterTokenMatcher()).
                addExtended(new MergerTokenMatcher())), new MergerTokenProcessor(primaryNames, mergerNames)));

        pairs.add(new TokenHandlerPair(new CompoundTokenMatcher(new ArrayListExtended<TokenMatcher>().addExtended(new MultiCharacterOrLetterTokenMatcher()).
                addExtended(new GlueTokenMatcher())), new GlueTokenProcessor()));

        pair = new TokenHandlerPair(new CompoundTokenMatcher(new ArrayListExtended<TokenMatcher>().addExtended(new SingleCharacterTokenMatcher()).
                addExtended(new AmpersandTokenMatcher(associations))), new AssociationTokenProcessor(associations));
        pair.setMultiTokenProcessor(true);
        pairs.add(pair);

        pairs.add(new TokenHandlerPair(new CompoundTokenMatcher(new ArrayListExtended<TokenMatcher>().addExtended(new SingleCharacterTokenMatcher()).
                addExtended(new NullInputTokenTypeMatcher())), new InputTokenTypeProcessor(InputTokenType.NFC)));

        pairs.add(new TokenHandlerPair(new CompoundTokenMatcher(new ArrayListExtended<TokenMatcher>().addExtended(new MultiCharacterOrLetterTokenMatcher()).
                addExtended(new NullInputTokenTypeMatcher())), new InputTokenTypeProcessor(InputTokenType.NF)));

        return pairs;
    }

    private class ArrayListExtended<E> extends ArrayList<E> {

        private static final long serialVersionUID = 1L;

        public ArrayListExtended<E> addExtended(E o) {
            this.add(o);
            return this;
        }
    }
    @Autowired
    public DiacriticalMarkStringTransformer diacriticalMarkStringTransformer;
    @Resource(name = "adjectiveTypes")
    public List<String> adjectiveTypes;
    @Autowired
    public AliasTypeRegistry aliasTypeRegistry;
    @Resource(name = "associations")
    public Map<String, String> associations;
    @Resource(name = "generalTerms")
    public List<String> generalTerms;
    @Resource(name = "cityStateNames")
    public Map<String, List<CityStateType>> cityStateNames;
    @Resource(name = "formerNames")
    public Map<String, String> formerNames;
    @Resource(name = "primaryNames")
    public Map<String, String> primaryNames;
    @Resource(name = "mergerNames")
    public Map<String, String> mergerNames;
    @Resource(name = "newNames")
    public Map<String, String> newNames;
    @Resource(name = "countryNationalities")
    public List<String> countryNationalities;
    @Resource(name = "countryNames")
    public List<String> countryNames;
    @Resource(name = "industryCatagoryCodes")
    public Map<String, String> industryCatagoryCodes;
    @Resource(name = "industryRegistry")
    public Map<String, BusinessNameIndustryType> industryRegistry;
    @Resource(name = "organizations")
    public Map<String, String> organizations;
    @Resource(name = "connectorTokens")
    public List<String> connectorTokens;
}
