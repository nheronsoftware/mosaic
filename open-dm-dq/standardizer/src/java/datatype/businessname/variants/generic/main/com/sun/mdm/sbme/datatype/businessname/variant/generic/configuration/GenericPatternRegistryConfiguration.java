package com.sun.mdm.sbme.datatype.businessname.variant.generic.configuration;

import java.util.Map;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Bean;
import com.sun.mdm.sbme.datatype.businessname.Pattern;
import com.sun.mdm.sbme.datatype.businessname.configuration.PatternRegistryConfiguration;

@Configuration
/**
 * Configuration class responsible for loading the pattern registry and making
 * it available to other components.
 * 
 * @author Shant Gharibi (shant.gharibi@sun.com)
 */
public class GenericPatternRegistryConfiguration extends PatternRegistryConfiguration {

    @Override
    @Bean
    /**
     * Return the loaded map of patterns.
     *
     * @return a map of patterns
     */
    public Map<String, Pattern> patternRegistry() {
        return super.patternRegistry();
    }

    @Override
    /**
     * The variant name of the business name data type.
     *
     * @return the variant name
     */
    protected String getVariantName() {
        return "generic";
    }
}
