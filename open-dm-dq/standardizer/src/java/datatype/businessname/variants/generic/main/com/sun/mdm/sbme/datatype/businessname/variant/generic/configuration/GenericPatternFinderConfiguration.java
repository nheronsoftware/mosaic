package com.sun.mdm.sbme.datatype.businessname.variant.generic.configuration;

import java.util.Map;
import javax.annotation.Resource;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Bean;
import org.springframework.beans.factory.annotation.Autowired;
import com.sun.mdm.sbme.datatype.businessname.Pattern;
import com.sun.mdm.sbme.datatype.businessname.pattern.BusinessNamePatternFinder;
import com.sun.mdm.sbme.datatype.businessname.pattern.DefaultBusinessNamePatternFinder;

@Configuration
/**
 * Configuration class responsible for constructing a <code>BusinessNamePatternFinder</code>.
 * 
 * @author Shant Gharibi (shant.gharibi@sun.com)
 */
public class GenericPatternFinderConfiguration {

    @Bean
    /**
     * A <code>BusinessNamePatternFidner</code> configured with map
     * consisting of <code>Pattern's</code>.
     * 
     * @return a configured <code>BusinessNamePatternFinder</code>
     */
    public BusinessNamePatternFinder patternFinder() {
        return new DefaultBusinessNamePatternFinder(patternRegistry);
    }
    
    @Resource(name = "patternRegistry")
    public Map<String, Pattern> patternRegistry;
}
