/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.businessname.matcher;

import static com.sun.mdm.sbme.datatype.businessname.Flag.Ampersand;
import static com.sun.mdm.sbme.datatype.businessname.InputTokenType.GLU;

import java.util.Map;

import com.sun.mdm.sbme.datatype.businessname.Token;
import com.sun.mdm.sbme.datatype.businessname.normalizer.NormalizedField;

/**
 * Responsible for matching tokens against ampersand or other connector tokens.
 *
 * @author Shant Gharibi (shant.gharibi@sun.com)
 */
public class AmpersandTokenMatcher implements TokenMatcher {

	/**
	 * The map of associations to match against.
	 * 
	 */
    private Map<String, String> associations;

    /**
     * The empty constructor.
     * 
     */
    public AmpersandTokenMatcher() {}
    
    /**
     * Construct an <code>AmpersandTokenMatcher</code> based on a map of associations.
     * 
     * @param associations the map of associations
     */
    public AmpersandTokenMatcher(Map<String, String> associations) {
    	this.associations = associations;
    }
    
    /**
     * Matches the token if it contains an ampersand and also appears in the map of associations.
     * 
     * @param token token
     * @param string the pre-processed string
     * @param normalizedField the normalized field
     * 
     * @return true if token contains ampersand and string appears in the map of associations, false otherwise
     */
    public boolean matches(final Token token, final String string, final NormalizedField normalizedFields) {
    	if (token.getFlag(Ampersand)) {
            token.setInputTokenType(GLU);
            return this.associations.containsKey(string);
        }

        return false;
    }

    /**
     * Sets the map of associations.
     * 
     * @param associations the map of associations
     */
    public void setAssociations(final Map<String, String> associations) {
        this.associations = associations;
    }

}
