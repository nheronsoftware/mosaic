package com.sun.mdm.sbme.datatype.businessname.configuration;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import net.java.hulp.i18n.LocalizedString;

import com.sun.inti.components.record.DefaultRecordPopulator;
import com.sun.inti.components.record.DefaultRecordSource;
import com.sun.inti.components.record.LineReader;
import com.sun.inti.components.record.PropertyDescriptor;
import com.sun.inti.components.record.Record;
import com.sun.inti.components.record.RecordPopulator;
import com.sun.inti.components.record.RecordSource;
import com.sun.inti.components.string.format.EnumerationFormatter;
import com.sun.inti.components.string.tokenize.FixedFieldTokenizer;
import com.sun.inti.components.string.tokenize.FixedFieldTokenizer.FieldDescriptor;
import com.sun.mdm.sbme.datatype.businessname.BusinessNameIndustryType;
import com.sun.mdm.sbme.datatype.businessname.CityStateType;
import com.sun.mdm.sbme.datatype.businessname.InputTokenType;
import com.sun.mdm.sbme.datatype.businessname.Localizer;
import com.sun.mdm.sbme.datatype.businessname.CityStateType.EntityType;
import com.sun.mdm.sbme.datatype.businessname.normalizer.AliasTypeRegistry;

/**
 * Configuration class which reads in data files and populates the appropiate structures.
 *
 * @author Shant Gharibi (shant.gharibi@sun.com)
 */
public abstract class KeyRegistryConfiguration {

	private static net.java.hulp.i18n.Logger logger;
	private static Localizer localizer;
	
	static {
		logger = net.java.hulp.i18n.Logger.getLogger(InputTokenType.class);
		localizer = Localizer.get();
	}
	
	/**
	 * Read in the adjective types data file and populates a list.
	 * 
	 * @return the list of adjective types
	 */
	public List<String> adjectiveTypes() {
    	FixedFieldTokenizer firstTokenizer = new FixedFieldTokenizer();
        FieldDescriptor[] firstFieldDescriptors = new FieldDescriptor[] {
                new FieldDescriptor(0, 100, true), // adjectives
        };
        
        firstTokenizer.setFieldDescriptors(firstFieldDescriptors);
        PropertyDescriptor[] firstPropertyDescriptors = new PropertyDescriptor[] {
                new PropertyDescriptor("adjective", null, null),
        };
        
        RecordPopulator[] recordPopulators = new RecordPopulator[] {
                new DefaultRecordPopulator(firstTokenizer, firstPropertyDescriptors),   
        };
        
        String dataFile = "META-INF/mdm/sbme/businessname/" + this.getVariantName() + "/bizAdjectivesTypeKeys.dat";
        InputStream is = getClass().getClassLoader().getResourceAsStream(dataFile);
        
        if (is == null) {
    		LocalizedString message = localizer.x("STD002: Could not load data file: {0}", dataFile);
    		logger.severe(message); 
        	throw new IllegalArgumentException(message.toString());
        }

        Reader reader = new InputStreamReader(is);
        Iterator<String> lineReader = new LineReader(reader, true);
        
        RecordSource recordSource = new DefaultRecordSource(recordPopulators, lineReader);
        
        Record record;
        List<String> adjectives = new ArrayList<String>();
        while ((record = recordSource.nextRecord()) != null) {
                @SuppressWarnings("unchecked")
                String adjective = (String) record.get("adjective");
                adjectives.add(adjective.trim());
        }
        
        return adjectives;
	}
	
	/**
	 * Read in the alias type registry data and create a <code>AliasTypeRegistry</code>.
	 * 
	 * @return a populated <code>AliasTypeRegistry</code>
	 */
	public AliasTypeRegistry aliasTypeRegistry() {
		FixedFieldTokenizer firstTokenizer = new FixedFieldTokenizer();
        FieldDescriptor[] firstFieldDescriptors = new FieldDescriptor[] {
                new FieldDescriptor(0, 29, true), // alias
                new FieldDescriptor(30, 100, true), // normalized name
        };
        
        firstTokenizer.setFieldDescriptors(firstFieldDescriptors);
        PropertyDescriptor[] firstPropertyDescriptors = new PropertyDescriptor[] {
                new PropertyDescriptor("alias", null, null),
                new PropertyDescriptor("normalizedName", null, null),
        };
        
        RecordPopulator[] recordPopulators = new RecordPopulator[] {
                new DefaultRecordPopulator(firstTokenizer, firstPropertyDescriptors),   
        };
        
        String dataFile = "META-INF/mdm/sbme/businessname/" + this.getVariantName() + "/bizAliasTypeKeys.dat";
        InputStream is = getClass().getClassLoader().getResourceAsStream(dataFile);
        
        if (is == null) {
    		LocalizedString message = localizer.x("STD002: Could not load data file: {0}", dataFile);
    		logger.severe(message); 
        	throw new IllegalArgumentException(message.toString());
        }

        Reader reader = new InputStreamReader(is);
        Iterator<String> lineReader = new LineReader(reader, true);
        
        RecordSource recordSource = new DefaultRecordSource(recordPopulators, lineReader);
        
        Record record;
        Map<String, List<Record>> recordMap = new LinkedHashMap<String, List<Record>>();
        while ((record = recordSource.nextRecord()) != null) {
                @SuppressWarnings("unchecked")
                String alias = (String) record.get("alias");               
                List<Record> recordList = recordMap.get(alias);
                if (recordList == null) {
                    recordList = new LinkedList<Record>();
                    recordMap.put(alias, recordList);
                }
                recordList.add(record);
        }

        AliasTypeRegistry aliasTypeRegistry = new AliasTypeRegistry();
        for (String key: recordMap.keySet()) {
            List<Record> recordList = recordMap.get(key);
            for (Record candidateRecord: recordList) {
	            @SuppressWarnings("unchecked")
	            String alias = (String) candidateRecord.get("alias");
	            String normalizedName = (String) candidateRecord.get("normalizedName");
	            aliasTypeRegistry.add(alias, normalizedName);
            }          
        }
        
        return aliasTypeRegistry;
	}
	
	/**
	 * Read in the associations data file and populate a map.
	 * 
	 * @return a map populated with the associations data
	 */
	public Map<String, String> associations() {
    	FixedFieldTokenizer firstTokenizer = new FixedFieldTokenizer();
        FieldDescriptor[] firstFieldDescriptors = new FieldDescriptor[] {
                new FieldDescriptor(0, 19, true), // association name
                new FieldDescriptor(20, 50, true), // association value
        };
        
        firstTokenizer.setFieldDescriptors(firstFieldDescriptors);
        PropertyDescriptor[] firstPropertyDescriptors = new PropertyDescriptor[] {
                new PropertyDescriptor("associationName", null, null),
                new PropertyDescriptor("associationValue", null, null),
        };
        
        RecordPopulator[] recordPopulators = new RecordPopulator[] {
                new DefaultRecordPopulator(firstTokenizer, firstPropertyDescriptors),   
        };
        
        String dataFile = "META-INF/mdm/sbme/businessname/" + this.getVariantName() + "/bizAssociationTypeKeys.dat";
        InputStream is = getClass().getClassLoader().getResourceAsStream(dataFile);
        
        if (is == null) {
    		LocalizedString message = localizer.x("STD002: Could not load data file: {0}", dataFile);
    		logger.severe(message); 
        	throw new IllegalArgumentException(message.toString());
        }

        Reader reader = new InputStreamReader(is);
        Iterator<String> lineReader = new LineReader(reader, true);
        
        RecordSource recordSource = new DefaultRecordSource(recordPopulators, lineReader);
        
        Record record;
        Map<String, List<Record>> recordMap = new LinkedHashMap<String, List<Record>>();
        while ((record = recordSource.nextRecord()) != null) {
                @SuppressWarnings("unchecked")
                String alias = (String) record.get("associationName");
                List<Record> recordList = recordMap.get(alias);
                if (recordList == null) {
                    recordList = new LinkedList<Record>();
                    recordMap.put(alias, recordList);
                }
                recordList.add(record);
        }

        Map<String, String> associations = new HashMap<String, String>();
        for (String key: recordMap.keySet()) {
            List<Record> recordList = recordMap.get(key);
            for (Record candidateRecord: recordList) {
                record = candidateRecord;
                if ("".equals(record.getString("usageFlag"))) {
                    break;
                }
            }
            @SuppressWarnings("unchecked")
            String associationName = (String) record.get("associationName");
            String associationValue = (String) record.get("associationValue");
            associations.put(associationName, associationValue);          
        }
        
        return associations;		
	}
	
	/**
	 * Read in the general terms data file and populate a list.
	 * 
	 * @return a list containing the general terms
	 */
	public List<String> generalTerms() {
    	FixedFieldTokenizer firstTokenizer = new FixedFieldTokenizer();
        FieldDescriptor[] firstFieldDescriptors = new FieldDescriptor[] {
                new FieldDescriptor(0, 100, true), // general term
        };
        
        firstTokenizer.setFieldDescriptors(firstFieldDescriptors);
        PropertyDescriptor[] firstPropertyDescriptors = new PropertyDescriptor[] {
                new PropertyDescriptor("generalTerm", null, null),
        };
        
        RecordPopulator[] recordPopulators = new RecordPopulator[] {
                new DefaultRecordPopulator(firstTokenizer, firstPropertyDescriptors),   
        };
        
        String dataFile = "META-INF/mdm/sbme/businessname/" + this.getVariantName() + "/bizBusinessGeneralTerms.dat";
        InputStream is = getClass().getClassLoader().getResourceAsStream(dataFile);
        
        if (is == null) {
    		LocalizedString message = localizer.x("STD002: Could not load data file: {0}", dataFile);
    		logger.severe(message); 
        	throw new IllegalArgumentException(message.toString());
        }

        Reader reader = new InputStreamReader(is);
        Iterator<String> lineReader = new LineReader(reader, true);
        
        RecordSource recordSource = new DefaultRecordSource(recordPopulators, lineReader);
        
        Record record;
        List<String> generalTerms = new ArrayList<String>();
        while ((record = recordSource.nextRecord()) != null) {
                @SuppressWarnings("unchecked")
                String generalTerm = (String) record.get("generalTerm");
                generalTerms.add(generalTerm);
        }
        
        return generalTerms;
	}
	
	/**
	 * Read in the city/state names and populate a map with the information.
	 * 
	 * @return a map containing the city/state names
	 */
	public Map<String, List<CityStateType>> cityStateNames() {
    	FixedFieldTokenizer firstTokenizer = new FixedFieldTokenizer();
        FieldDescriptor[] firstFieldDescriptors = new FieldDescriptor[] {
                new FieldDescriptor(0, 24, true), // city
                new FieldDescriptor(25, 4, true), // type
                new FieldDescriptor(30, 2, true), // country
        };
        
        firstTokenizer.setFieldDescriptors(firstFieldDescriptors);
        PropertyDescriptor[] firstPropertyDescriptors = new PropertyDescriptor[] {
                new PropertyDescriptor("name", null, null),
                new PropertyDescriptor("type", new EnumerationFormatter(EntityType.class), null),
                new PropertyDescriptor("countryCode", null, null),
        };
        
        RecordPopulator[] recordPopulators = new RecordPopulator[] {
                new DefaultRecordPopulator(firstTokenizer, firstPropertyDescriptors),   
        };
        
        String dataFile = "META-INF/mdm/sbme/businessname/" + this.getVariantName() + "/bizCityOrStateTypeKeys.dat";
        InputStream is = getClass().getClassLoader().getResourceAsStream(dataFile);
        
        if (is == null) {
    		LocalizedString message = localizer.x("STD002: Could not load data file: {0}", dataFile);
    		logger.severe(message); 
        	throw new IllegalArgumentException(message.toString());
        }

        Reader reader = new InputStreamReader(is);
        Iterator<String> lineReader = new LineReader(reader, true);
        
        RecordSource recordSource = new DefaultRecordSource(recordPopulators, lineReader);
        
        Record record;
        Map<String, List<Record>> recordMap = new LinkedHashMap<String, List<Record>>();
        while ((record = recordSource.nextRecord()) != null) {
                @SuppressWarnings("unchecked")
                String name = (String) record.get("name");
                List<Record> recordList = recordMap.get(name);
                if (recordList == null) {
                    recordList = new LinkedList<Record>();
                    recordMap.put(name, recordList);
                }
                recordList.add(record);
        }

        Map<String, List<CityStateType>> types = new HashMap<String, List<CityStateType>>();
        for (String key: recordMap.keySet()) {
            List<Record> recordList = recordMap.get(key);
            for (Record candidateRecord: recordList) {
	            @SuppressWarnings("unchecked")
	            String city = (String) candidateRecord.get("name");
	            EntityType type = (EntityType) candidateRecord.get("type");
	            String countryCode = (String) candidateRecord.get("countryCode");
           
	            CityStateType entry = new CityStateType(city, type, countryCode);
	            List<CityStateType> typeList = types.get(city);
	            if (typeList == null) {
	            	typeList = new ArrayList<CityStateType>();
	            	types.put(city, typeList);
	            }
	            typeList.add(entry);
            }
        }
        
        return types;		
	}
	
	/**
	 * Read in the company former names data file and populate a map.
	 * 
	 * @return a map containing company former names
	 */
	public Map<String, String> formerNames() {
    	FixedFieldTokenizer firstTokenizer = new FixedFieldTokenizer();
        FieldDescriptor[] firstFieldDescriptors = new FieldDescriptor[] {
                new FieldDescriptor(0, 39, true), // former name
                new FieldDescriptor(40, 50, true), // new name
        };
        
        firstTokenizer.setFieldDescriptors(firstFieldDescriptors);
        PropertyDescriptor[] firstPropertyDescriptors = new PropertyDescriptor[] {
                new PropertyDescriptor("formerName", null, null),
                new PropertyDescriptor("newName", null, null),
        };
        
        RecordPopulator[] recordPopulators = new RecordPopulator[] {
                new DefaultRecordPopulator(firstTokenizer, firstPropertyDescriptors),   
        };
        
        String dataFile = "META-INF/mdm/sbme/businessname/" + this.getVariantName() + "/bizCompanyFormerNames.dat";
        InputStream is = getClass().getClassLoader().getResourceAsStream(dataFile);
        
        if (is == null) {
    		LocalizedString message = localizer.x("STD002: Could not load data file: {0}", dataFile);
    		logger.severe(message); 
        	throw new IllegalArgumentException(message.toString());
        }

        Reader reader = new InputStreamReader(is);
        Iterator<String> lineReader = new LineReader(reader, true);
        
        RecordSource recordSource = new DefaultRecordSource(recordPopulators, lineReader);
        
        Record record;
        Map<String, List<Record>> recordMap = new LinkedHashMap<String, List<Record>>();
        while ((record = recordSource.nextRecord()) != null) {
                @SuppressWarnings("unchecked")
                String formerName = (String) record.get("formerName");
                List<Record> recordList = recordMap.get(formerName);
                if (recordList == null) {
                    recordList = new LinkedList<Record>();
                    recordMap.put(formerName, recordList);
                }
                recordList.add(record);
        }

        Map<String, String> formerNames = new HashMap<String, String>();
        for (String key: recordMap.keySet()) {
            List<Record> recordList = recordMap.get(key);
            for (Record candidateRecord: recordList) {
	            @SuppressWarnings("unchecked")
	            String formerName = (String) candidateRecord.get("formerName");
	            String newName = (String) candidateRecord.get("newName");
           
	            formerNames.put(formerName, newName);	            
            }
        }
        
        return formerNames;			
	}
	
	/**
	 * Read in the company primary names data file and populate a map.
	 * 
	 * @return a map containing company primary names
	 */
	public Map<String, String> primaryNames() {
    	FixedFieldTokenizer firstTokenizer = new FixedFieldTokenizer();
        FieldDescriptor[] firstFieldDescriptors = new FieldDescriptor[] {
                new FieldDescriptor(0, 49, true), // primary name
                new FieldDescriptor(50, 50, true), // industry sector
        };
        
        firstTokenizer.setFieldDescriptors(firstFieldDescriptors);
        PropertyDescriptor[] firstPropertyDescriptors = new PropertyDescriptor[] {
                new PropertyDescriptor("primaryName", null, null),
                new PropertyDescriptor("industrySector", null, null),
        };
        
        RecordPopulator[] recordPopulators = new RecordPopulator[] {
                new DefaultRecordPopulator(firstTokenizer, firstPropertyDescriptors),   
        };
        
        String dataFile = "META-INF/mdm/sbme/businessname/" + this.getVariantName() + "/bizCompanyPrimaryNames.dat";
        InputStream is = getClass().getClassLoader().getResourceAsStream(dataFile);
        
        if (is == null) {
    		LocalizedString message = localizer.x("STD002: Could not load data file: {0}", dataFile);
    		logger.severe(message); 
        	throw new IllegalArgumentException(message.toString());
        }

        Reader reader = new InputStreamReader(is);
        Iterator<String> lineReader = new LineReader(reader, true);

        RecordSource recordSource = new DefaultRecordSource(recordPopulators, lineReader);
        
        Record record;
        Map<String, List<Record>> recordMap = new LinkedHashMap<String, List<Record>>();
        while ((record = recordSource.nextRecord()) != null) {
                @SuppressWarnings("unchecked")
                String primaryName = (String) record.get("primaryName");
                List<Record> recordList = recordMap.get(primaryName);
                if (recordList == null) {
                    recordList = new LinkedList<Record>();
                    recordMap.put(primaryName, recordList);
                }
                recordList.add(record);
        }

        Map<String, String> primaryNames = new HashMap<String, String>();
        for (String key: recordMap.keySet()) {
            List<Record> recordList = recordMap.get(key);
            for (Record candidateRecord: recordList) {
	            @SuppressWarnings("unchecked")
	            String primaryName = (String) candidateRecord.get("primaryName");
	            String industrySector = (String) candidateRecord.get("industrySector");
          
	            primaryNames.put(primaryName, industrySector);	            
            }
        }
        
        return primaryNames;			
	}
	
	/**
	 * Read in company merger names data file and populate a map.
	 * 
	 * @return a map containing company merger names
	 */
	public Map<String, String> mergerNames() {
    	FixedFieldTokenizer firstTokenizer = new FixedFieldTokenizer();
        FieldDescriptor[] firstFieldDescriptors = new FieldDescriptor[] {
                new FieldDescriptor(0, 49, true), // merger name
                new FieldDescriptor(50, 50, true), // industry sector
        };
        
        firstTokenizer.setFieldDescriptors(firstFieldDescriptors);
        PropertyDescriptor[] firstPropertyDescriptors = new PropertyDescriptor[] {
                new PropertyDescriptor("mergerName", null, null),
                new PropertyDescriptor("industrySector", null, null),
        };
        
        RecordPopulator[] recordPopulators = new RecordPopulator[] {
                new DefaultRecordPopulator(firstTokenizer, firstPropertyDescriptors),   
        };
        
        String dataFile = "META-INF/mdm/sbme/businessname/" + this.getVariantName() + "/bizCompanyMergerNames.dat";
        InputStream is = getClass().getClassLoader().getResourceAsStream(dataFile);
        
        if (is == null) {
    		LocalizedString message = localizer.x("STD002: Could not load data file: {0}", dataFile);
    		logger.severe(message); 
        	throw new IllegalArgumentException(message.toString());
        }

        Reader reader = new InputStreamReader(is);
        Iterator<String> lineReader = new LineReader(reader, true);

        RecordSource recordSource = new DefaultRecordSource(recordPopulators, lineReader);
        
        Record record;
        Map<String, List<Record>> recordMap = new LinkedHashMap<String, List<Record>>();
        while ((record = recordSource.nextRecord()) != null) {
                @SuppressWarnings("unchecked")
                String mergerName = (String) record.get("mergerName");
                List<Record> recordList = recordMap.get(mergerName);
                if (recordList == null) {
                    recordList = new LinkedList<Record>();
                    recordMap.put(mergerName, recordList);
                }
                recordList.add(record);
        }

        Map<String, String> mergerNames = new HashMap<String, String>();
        for (String key: recordMap.keySet()) {
            List<Record> recordList = recordMap.get(key);
            for (Record candidateRecord: recordList) {
	            @SuppressWarnings("unchecked")
	            String mergerName = (String) candidateRecord.get("mergerName");
	            String industrySector = (String) candidateRecord.get("industrySector");
          
	            mergerNames.put(mergerName, industrySector);	            
            }
        }
        
        return mergerNames;		
	}
	
	/**
	 * Read in company new names data file and populate a map.
	 * 
	 * @return a map containing company new names
	 */
	public Map<String, String> newNames() {
    	FixedFieldTokenizer firstTokenizer = new FixedFieldTokenizer();
        FieldDescriptor[] firstFieldDescriptors = new FieldDescriptor[] {
                new FieldDescriptor(0, 39, true), // former name
                new FieldDescriptor(40, 50, true), // new name
        };
        
        firstTokenizer.setFieldDescriptors(firstFieldDescriptors);
        PropertyDescriptor[] firstPropertyDescriptors = new PropertyDescriptor[] {
                new PropertyDescriptor("formerName", null, null),
                new PropertyDescriptor("newName", null, null),
        };
        
        RecordPopulator[] recordPopulators = new RecordPopulator[] {
                new DefaultRecordPopulator(firstTokenizer, firstPropertyDescriptors),   
        };
        
        String dataFile = "META-INF/mdm/sbme/businessname/" + this.getVariantName() + "/bizCompanyFormerNames.dat";
        InputStream is = getClass().getClassLoader().getResourceAsStream(dataFile);
        
        if (is == null) {
    		LocalizedString message = localizer.x("STD002: Could not load data file: {0}", dataFile);
    		logger.severe(message); 
        	throw new IllegalArgumentException(message.toString());
        }

        Reader reader = new InputStreamReader(is);
        Iterator<String> lineReader = new LineReader(reader, true);
        
        RecordSource recordSource = new DefaultRecordSource(recordPopulators, lineReader);
        
        Record record;
        Map<String, List<Record>> recordMap = new LinkedHashMap<String, List<Record>>();
        while ((record = recordSource.nextRecord()) != null) {
                @SuppressWarnings("unchecked")
                String newName = (String) record.get("newName");
                List<Record> recordList = recordMap.get(newName);
                if (recordList == null) {
                    recordList = new LinkedList<Record>();
                    recordMap.put(newName, recordList);
                }
                recordList.add(record);
        }

        Map<String, String> newNames = new HashMap<String, String>();
        for (String key: recordMap.keySet()) {
            List<Record> recordList = recordMap.get(key);
            for (Record candidateRecord: recordList) {
	            @SuppressWarnings("unchecked")
	            String newName = (String) candidateRecord.get("newName");
	            String formerName = (String) candidateRecord.get("formerName");
           
	            newNames.put(formerName, newName);	            
            }
        }
        
        return newNames;			
	}
	
	/**
	 * Read in country names data file and populate a list.
	 * 
	 * @return a list containing country names
	 */
	public List<String> countryNames() {
    	FixedFieldTokenizer firstTokenizer = new FixedFieldTokenizer();
        FieldDescriptor[] firstFieldDescriptors = new FieldDescriptor[] {
                new FieldDescriptor(0, 29, true), // country name
                new FieldDescriptor(30, 4, true), // country code
                new FieldDescriptor(35, 50, true), // nationality
        };
        
        firstTokenizer.setFieldDescriptors(firstFieldDescriptors);
        PropertyDescriptor[] firstPropertyDescriptors = new PropertyDescriptor[] {
                new PropertyDescriptor("countryName", null, null),
                new PropertyDescriptor("countryCode", null, null),
                new PropertyDescriptor("nationality", null, null),
        };
        
        RecordPopulator[] recordPopulators = new RecordPopulator[] {
                new DefaultRecordPopulator(firstTokenizer, firstPropertyDescriptors),   
        };
        
        String dataFile = "META-INF/mdm/sbme/businessname/" + this.getVariantName() + "/bizCountryTypeKeys.dat";
        InputStream is = getClass().getClassLoader().getResourceAsStream(dataFile);
        
        if (is == null) {
    		LocalizedString message = localizer.x("STD002: Could not load data file: {0}", dataFile);
    		logger.severe(message); 
        	throw new IllegalArgumentException(message.toString());
        }

        Reader reader = new InputStreamReader(is);
        Iterator<String> lineReader = new LineReader(reader, true);
        
        RecordSource recordSource = new DefaultRecordSource(recordPopulators, lineReader);
        
        Record record;
        Map<String, List<Record>> recordMap = new LinkedHashMap<String, List<Record>>();
        while ((record = recordSource.nextRecord()) != null) {
                @SuppressWarnings("unchecked")
                String countryName = (String) record.get("countryName");
                List<Record> recordList = recordMap.get(countryName);
                if (recordList == null) {
                    recordList = new LinkedList<Record>();
                    recordMap.put(countryName, recordList);
                }
                recordList.add(record);
        }

        List<String> names = new ArrayList<String>();
        for (String key: recordMap.keySet()) {
            List<Record> recordList = recordMap.get(key);
            for (Record candidateRecord: recordList) {
	            @SuppressWarnings("unchecked")
	            String countryName = (String) candidateRecord.get("countryName");
				names.add(countryName);
            }
        }
        
        return names;		
	}
	
	/**
	 * Read in a country nationalities data file and populate a list.
	 *  
	 * @return a list containing country nationalities
	 */
	public List<String> countryNationalities() {
    	FixedFieldTokenizer firstTokenizer = new FixedFieldTokenizer();
        FieldDescriptor[] firstFieldDescriptors = new FieldDescriptor[] {
                new FieldDescriptor(0, 29, true), // country name
                new FieldDescriptor(30, 4, true), // country code
                new FieldDescriptor(35, 50, true), // nationality
        };
        
        firstTokenizer.setFieldDescriptors(firstFieldDescriptors);
        PropertyDescriptor[] firstPropertyDescriptors = new PropertyDescriptor[] {
                new PropertyDescriptor("countryName", null, null),
                new PropertyDescriptor("countryCode", null, null),
                new PropertyDescriptor("nationality", null, null),
        };
        
        RecordPopulator[] recordPopulators = new RecordPopulator[] {
                new DefaultRecordPopulator(firstTokenizer, firstPropertyDescriptors),   
        };
        
        String dataFile = "META-INF/mdm/sbme/businessname/" + this.getVariantName() + "/bizCountryTypeKeys.dat";
        InputStream is = getClass().getClassLoader().getResourceAsStream(dataFile);
        
        if (is == null) {
    		LocalizedString message = localizer.x("STD002: Could not load data file: {0}", dataFile);
    		logger.severe(message); 
        	throw new IllegalArgumentException(message.toString());
        }

        Reader reader = new InputStreamReader(is);
        Iterator<String> lineReader = new LineReader(reader, true);
        
        RecordSource recordSource = new DefaultRecordSource(recordPopulators, lineReader);
        
        Record record;
        Map<String, List<Record>> recordMap = new LinkedHashMap<String, List<Record>>();
        while ((record = recordSource.nextRecord()) != null) {
                @SuppressWarnings("unchecked")
                String nationality = (String) record.get("nationality");
                List<Record> recordList = recordMap.get(nationality);
                if (recordList == null) {
                    recordList = new LinkedList<Record>();
                    recordMap.put(nationality, recordList);
                }
                recordList.add(record);
        }

        List<String> nationalities = new ArrayList<String>();
        for (String key: recordMap.keySet()) {
            List<Record> recordList = recordMap.get(key);
            for (Record candidateRecord: recordList) {
	            @SuppressWarnings("unchecked")
	            String nationality = (String) candidateRecord.get("nationality");
				nationalities.add(nationality);
            }
        }
        
        return nationalities;		
	}
	
	/**
	 * Read in a industry category codes data file and populate a map.
	 * 
	 * @return a map containing industry category codes
	 */
	public Map<String, String> industryCatagoryCodes() {
		FixedFieldTokenizer firstTokenizer = new FixedFieldTokenizer();
        FieldDescriptor[] firstFieldDescriptors = new FieldDescriptor[] {
                new FieldDescriptor(0, 14, true), // category code
                new FieldDescriptor(15, 150, true), // category name
        };
        
        firstTokenizer.setFieldDescriptors(firstFieldDescriptors);
        PropertyDescriptor[] firstPropertyDescriptors = new PropertyDescriptor[] {
                new PropertyDescriptor("categoryCode", null, null),
                new PropertyDescriptor("categoryName", null, null),
        };
        
        RecordPopulator[] recordPopulators = new RecordPopulator[] {
                new DefaultRecordPopulator(firstTokenizer, firstPropertyDescriptors),   
        };
        
        String dataFile = "META-INF/mdm/sbme/businessname/" + this.getVariantName() + "/bizIndustryCategoriesCode.dat";
        InputStream is = getClass().getClassLoader().getResourceAsStream(dataFile);
        
        if (is == null) {
    		LocalizedString message = localizer.x("STD002: Could not load data file: {0}", dataFile);
    		logger.severe(message); 
        	throw new IllegalArgumentException(message.toString());
        }

        Reader reader = new InputStreamReader(is);
        Iterator<String> lineReader = new LineReader(reader, true);
        
        RecordSource recordSource = new DefaultRecordSource(recordPopulators, lineReader);
        
        Record record;
        Map<String, List<Record>> recordMap = new LinkedHashMap<String, List<Record>>();
        while ((record = recordSource.nextRecord()) != null) {
                @SuppressWarnings("unchecked")
                String categoryCode = (String) record.get("categoryCode");
                List<Record> recordList = recordMap.get(categoryCode);
                if (recordList == null) {
                    recordList = new LinkedList<Record>();
                    recordMap.put(categoryCode, recordList);
                }
                recordList.add(record);
        }

        Map<String, String> categories = new HashMap<String, String>();
        for (String key: recordMap.keySet()) {
            List<Record> recordList = recordMap.get(key);
            for (Record candidateRecord: recordList) {
	            @SuppressWarnings("unchecked")
	            String categoryCode = (String) candidateRecord.get("categoryCode");
	            String categoryName = (String) candidateRecord.get("categoryName");
         
	            categories.put(categoryCode, categoryName);	            
            }
        }
        
        return categories;		
	}
	
	/**
	 * Read in an industry registry data file and populate a map.
	 * 
	 * @return a map containing information about different types of industries
	 */
	public Map<String, BusinessNameIndustryType> industryRegistry() {
		FixedFieldTokenizer firstTokenizer = new FixedFieldTokenizer();
        FieldDescriptor[] firstFieldDescriptors = new FieldDescriptor[] {
                new FieldDescriptor(0, 19, true), // industry field
                new FieldDescriptor(20, 19, true), // normalized industry field
                new FieldDescriptor(40, 150, true), // industry sector
        };
        
        firstTokenizer.setFieldDescriptors(firstFieldDescriptors);
        PropertyDescriptor[] firstPropertyDescriptors = new PropertyDescriptor[] {
                new PropertyDescriptor("industryField", null, null),
                new PropertyDescriptor("normalizedIndustryField", null, null),
                new PropertyDescriptor("industrySector", null, null),
        };
        
        RecordPopulator[] recordPopulators = new RecordPopulator[] {
                new DefaultRecordPopulator(firstTokenizer, firstPropertyDescriptors),   
        };
        
        String dataFile = "META-INF/mdm/sbme/businessname/" + this.getVariantName() + "/bizIndustryTypeKeys.dat";
        InputStream is = getClass().getClassLoader().getResourceAsStream(dataFile);
        
        if (is == null) {
    		LocalizedString message = localizer.x("STD002: Could not load data file: {0}", dataFile);
    		logger.severe(message); 
        	throw new IllegalArgumentException(message.toString());
        }

        Reader reader = new InputStreamReader(is);
        Iterator<String> lineReader = new LineReader(reader, true);
        
        RecordSource recordSource = new DefaultRecordSource(recordPopulators, lineReader);
        
        Record record;
        Map<String, List<Record>> recordMap = new LinkedHashMap<String, List<Record>>();
        while ((record = recordSource.nextRecord()) != null) {
                @SuppressWarnings("unchecked")
                String industryField = (String) record.get("industryField");
                List<Record> recordList = recordMap.get(industryField);
                if (recordList == null) {
                    recordList = new LinkedList<Record>();
                    recordMap.put(industryField, recordList);
                }
                recordList.add(record);
        }

        Map<String, BusinessNameIndustryType> industryRegistry = new HashMap<String, BusinessNameIndustryType>();
        for (String key: recordMap.keySet()) {
            List<Record> recordList = recordMap.get(key);
            for (Record candidateRecord: recordList) {
	            @SuppressWarnings("unchecked")
	            String industryField = (String) candidateRecord.get("industryField");
	            String normalizedIndustryField = (String) candidateRecord.get("normalizedIndustryField");
	            String industrySector = (String) candidateRecord.get("industrySector");      
	            List<String> sectorCodes = new ArrayList<String>();
	            for(String sectorCode: industrySector.split("\\s+")) {
	            	sectorCodes.add(sectorCode);
	            }
	            industryRegistry.put(industryField, new BusinessNameIndustryType(industryField, normalizedIndustryField, sectorCodes));	            
            }
        }
        
        return industryRegistry;		
	}
	
	/**
	 * Read in a data file containing information about organizations and populate a map.
	 * 
	 * @return a map containing information about organizations
	 */
	public Map<String, String> organizations() {
		FixedFieldTokenizer firstTokenizer = new FixedFieldTokenizer();
        FieldDescriptor[] firstFieldDescriptors = new FieldDescriptor[] {
                new FieldDescriptor(0, 19, true), // organization name
                new FieldDescriptor(20, 150, true), // organization value
        };
        
        firstTokenizer.setFieldDescriptors(firstFieldDescriptors);
        PropertyDescriptor[] firstPropertyDescriptors = new PropertyDescriptor[] {
                new PropertyDescriptor("name", null, null),
                new PropertyDescriptor("value", null, null),
        };
        
        RecordPopulator[] recordPopulators = new RecordPopulator[] {
                new DefaultRecordPopulator(firstTokenizer, firstPropertyDescriptors),   
        };
        
        String dataFile = "META-INF/mdm/sbme/businessname/" + this.getVariantName() + "/bizOrganizationTypeKeys.dat";
        InputStream is = getClass().getClassLoader().getResourceAsStream(dataFile);
        
        if (is == null) {
    		LocalizedString message = localizer.x("STD002: Could not load data file: {0}", dataFile);
    		logger.severe(message); 
        	throw new IllegalArgumentException(message.toString());
        }

        Reader reader = new InputStreamReader(is);
        Iterator<String> lineReader = new LineReader(reader, true);
        
        RecordSource recordSource = new DefaultRecordSource(recordPopulators, lineReader);
        
        Record record;
        Map<String, List<Record>> recordMap = new LinkedHashMap<String, List<Record>>();
        while ((record = recordSource.nextRecord()) != null) {
                @SuppressWarnings("unchecked")
                String name = (String) record.get("name");
                List<Record> recordList = recordMap.get(name);
                if (recordList == null) {
                    recordList = new LinkedList<Record>();
                    recordMap.put(name, recordList);
                }
                recordList.add(record);
        }

        Map<String, String> organizations = new HashMap<String, String>();
        for (String key: recordMap.keySet()) {
            List<Record> recordList = recordMap.get(key);
            for (Record candidateRecord: recordList) {
	            @SuppressWarnings("unchecked")
	            String name = (String) candidateRecord.get("name");
	            String value = (String) candidateRecord.get("value");

	            organizations.put(name, value);	            
            }
        }
        
        return organizations;			
	}
	
	/**
	 * Read in a data file contain connector tokens and populate a list.
	 * 
	 * @return a list containing connector tokens
	 */
	public List<String> connectorTokens() {
		FixedFieldTokenizer firstTokenizer = new FixedFieldTokenizer();
        FieldDescriptor[] firstFieldDescriptors = new FieldDescriptor[] {
                new FieldDescriptor(0, 50, true), // connector tokens
        };
        
        firstTokenizer.setFieldDescriptors(firstFieldDescriptors);
        PropertyDescriptor[] firstPropertyDescriptors = new PropertyDescriptor[] {
                new PropertyDescriptor("connectorToken", null, null),
        };
        
        RecordPopulator[] recordPopulators = new RecordPopulator[] {
                new DefaultRecordPopulator(firstTokenizer, firstPropertyDescriptors),   
        };
        
        String dataFile = "META-INF/mdm/sbme/businessname/" + this.getVariantName() + "/bizConnectorTokens.dat";
        InputStream is = getClass().getClassLoader().getResourceAsStream(dataFile);
        
        if (is == null) {
    		LocalizedString message = localizer.x("STD002: Could not load data file: {0}", dataFile);
    		logger.severe(message); 
        	throw new IllegalArgumentException(message.toString());
        }

        Reader reader = new InputStreamReader(is);
        Iterator<String> lineReader = new LineReader(reader, true);
        
        RecordSource recordSource = new DefaultRecordSource(recordPopulators, lineReader);
        
        Record record;
        Map<String, List<Record>> recordMap = new LinkedHashMap<String, List<Record>>();
        while ((record = recordSource.nextRecord()) != null) {
                @SuppressWarnings("unchecked")
                String name = (String) record.get("connectorToken");
                List<Record> recordList = recordMap.get(name);
                if (recordList == null) {
                    recordList = new LinkedList<Record>();
                    recordMap.put(name, recordList);
                }
                recordList.add(record);
        }

        List<String> connectorTokens = new ArrayList<String>();
        for (String key: recordMap.keySet()) {
            List<Record> recordList = recordMap.get(key);
            for (Record candidateRecord: recordList) {
	            @SuppressWarnings("unchecked")
	            String connectorToken = (String) candidateRecord.get("connectorToken");

	            connectorTokens.add(connectorToken);	            
            }
        }
        
        return connectorTokens;		
	}
	
	/**
	 * Returns the variant name.
	 * 
	 * @return the variant name
	 */
	public abstract String getVariantName();
}
