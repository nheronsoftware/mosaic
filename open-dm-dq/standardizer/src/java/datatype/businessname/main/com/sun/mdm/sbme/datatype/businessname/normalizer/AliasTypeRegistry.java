/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.businessname.normalizer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A registry consisting of alias types.
 *
 * @author Shant Gharibi (shant.gharibi@sun.com)
 */
public class AliasTypeRegistry {

    private Map<String, List<String>> aliasMap = new HashMap<String, List<String>>();
    private Map<String, String> nameMap = new HashMap<String, String>();

    /**
     * The empty constructor, if used, setter methods have to be used to populate this object.
     * 
     */
    public AliasTypeRegistry() {}
    
    /**
     * Construct a new <code>AliasTypeRegistry</code>.
     * 
     * @param aliasMap a map of aliases
     */
    public AliasTypeRegistry(final Map<String, List<String>> aliasMap) {
    	if (aliasMap != null && aliasMap.size() > 0) {
    		this.aliasMap = aliasMap;
    		this.nameMap = this.getNameMap(aliasMap);
    	}
    }

    /**
     * Checks to see if the given alias exists in the registry.
     * 
     * @param alias the alias name to check
     * @return true if exists in registry, false otherwise
     */
    public boolean containsAlias(final String alias) {	
        return this.aliasMap.containsKey(alias);
    }

    /**
     * Returns a list of names based on an alias.
     * 
     * @param alias the alias
     * @return list of names associated with the given alias
     */
    public List<String> getActualName(final String alias) {
        return this.aliasMap.get(alias);
    }

    /**
     * Sets a map of aliases and the associated names with the alias.
     * 
     * @param aliasMap the map containing aliases and names associated 
     * with the alias.
     */
    public void setAliasMap(final Map<String, List<String>> aliasMap) {
        this.aliasMap = aliasMap;
        this.nameMap = this.getNameMap(aliasMap);
    }

    /**
     * Checks to see if the registry contains the given name.
     * 
     * @param name the name to check
     * @return true if registry contains the given name, false otherwise
     */
    public boolean containsName(final String name) {
        return this.nameMap.containsKey(name);
    }

    /**
     * Returns the alias associated with a name.
     * 
     * @param name the name to get an alias for
     * @return the alias for the given name
     */
    public String getAliasByName(final String name) {
        return this.nameMap.get(name);
    }

    /**
     * Given a map whose key is an alias and value is a list of names, returns
     * a name map whose key is a name and value is an alias.
     * 
     * @param map a map whose key is an alias and value is a list of name
     * @return a map whose key is a name and value is an alias
     */
    private Map<String, String> getNameMap(final Map<String, List<String>> map) {
        final Map<String, String> nameMap = new HashMap<String, String>();

        for (final String key : map.keySet()) {
            final List<String> valueList = map.get(key);
            for (final String value : valueList) {
                nameMap.put(value, key);
            }
        }

        return nameMap;
    }
    
    /**
     * Associates a name with an alias.
     * 
     * @param key the alias to be associated with
     * @param value the name to associate to the alias
     */
    public void add(String key, String value) {
    	List<String> aliasList = this.aliasMap.get(key);
    	if (aliasList == null) {
    		aliasList = new ArrayList<String>();
    		this.aliasMap.put(key, aliasList);
    	}
    	
    	this.nameMap.put(value, key);
    	aliasList.add(value);   	
    }

}
