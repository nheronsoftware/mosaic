/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.businessname.parser;

import java.util.List;

import com.sun.mdm.sbme.MdmStandardizationException;
import com.sun.mdm.sbme.datatype.businessname.Token;

public interface BusinessNameParser {

    /**
     * Breaks down the record into its basic components including any special
     * characters
     * 
     * @param rec
     *            the string to be parsed
     * @return the parsed fields
     * @throws MdmStandardizationException
     *             a generic standardization exception
     */
    public List<Token> parse(String rec) throws MdmStandardizationException;

}