/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.businessname.matcher;

import java.util.Map;

import com.sun.mdm.sbme.datatype.businessname.Token;
import com.sun.mdm.sbme.datatype.businessname.normalizer.NormalizedField;

/**
 * Responsible for matching tokens against a map of merger names.
 *
 * @author Shant Gharibi (shant.gharibi@sun.com)
 */
public class MergerNameTokenMatcher implements TokenMatcher {

	/**
	 * The map of merger names to match against.
	 * 
	 */
    private Map<String, String> mergerNames;
    
    /**
     * The empty constructor.
     * 
     */
    public MergerNameTokenMatcher() {}
    
    /**
     * Construct a <code>MergerNameTokenMatcher</code> based on a map of merger names.
     * 
     * @param mergreNames the map consisting of merger names
     */
    public MergerNameTokenMatcher(Map<String, String> mergreNames) {
    	this.mergerNames = mergreNames;
    }

    /**
     * Match the given string against the configured map of merger names.
     * 
     * @param token token
     * @param string the pre-processed string to match
     * @param normalizedField the normalized field
     * 
     * @return true if given string appears in the map of merger names, false otherwise.
     */
    public boolean matches(final Token token, final String string, final NormalizedField normalizedField) {
        if (!normalizedField.isPrimaryName()) {
            return this.mergerNames.containsKey(string);
        }
        return false;
    }

    /**
     * Sets the map of merger names to match against.
     * 
     * @param mergerNames the map consisting of merger names
     */
    public void setMergerNames(final Map<String, String> mergerNames) {
        this.mergerNames = mergerNames;
    }
}
