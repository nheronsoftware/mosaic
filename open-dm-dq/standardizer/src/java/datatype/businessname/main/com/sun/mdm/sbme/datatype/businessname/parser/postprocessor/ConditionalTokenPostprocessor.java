/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.businessname.parser.postprocessor;

import com.sun.inti.components.string.match.StringMatcher;
import com.sun.mdm.sbme.datatype.businessname.Token;

/**
 * A <code>TokenPostprocessor</code> which only invokes a
 * <code>TokenPostprocessor</code> if the <code>StringMatcher</code>
 * component is able to match the token.
 *
 * @author Shant Gharibi (shant.gharibi@sun.com)
 */
public class ConditionalTokenPostprocessor implements TokenPostprocessor {

	/**
	 * Used to match against the token.
	 * 
	 */
    private StringMatcher matcher;

    /**
     * Processes token if the <code>StringMatcher</code> matched against the token.
     * 
     */
    private TokenPostprocessor processor;

    /**
     * The empty constructor.  Have to use setter methods to configure components.
     * 
     */
    public ConditionalTokenPostprocessor() {}
    
    /**
     * Construct a new <code>ConditionalTokenPostprocessor</code> with a <code>StringMatcher</code>
     * and <code>TokenPostprocessor</code>.
     * 
     * @param matcher the <code>StringMatcher</code> to register
     * @param processor the <code>TokenPostprocessor</code> to register
     */
    public ConditionalTokenPostprocessor(final StringMatcher matcher, final TokenPostprocessor processor) {
    	this.matcher = matcher;
    	this.processor = processor;
    }
    
    /**
     * If the <code>StringMatcher<code> matches the token, then do postprocessing.
     * 
     * @param token the token to match against and postprocess
     */
    public void postprocess(final Token token) {
        if (this.matcher.matches(token.getImage())) {
            this.processor.postprocess(token);
        }

    }

    /**
     * Registers the <code>StringMatcher</code> component.
     * 
     * @param matcher the <code>StringMatcher</code> to register
     */
    public void setMatcher(final StringMatcher matcher) {
        this.matcher = matcher;
    }

    /**
     * Registers the <code>TokenPostprocessor</code> component.
     * 
     * @param processor the <code>TokenProcessprocessor</code> to register
     */
    public void setProcessor(final TokenPostprocessor processor) {
        this.processor = processor;
    }
}
