/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.businessname.parser.postprocessor;

import java.util.List;

import com.sun.mdm.sbme.datatype.businessname.Token;

/**
 * A <code>TokenPostprocessor</code> component containing a list of 
 * <code>TokenPostprocessors</code>.
 *
 * @author Shant Gharibi (shant.gharibi@sun.com)
 */
public class CompoundTokenPostprocessor implements TokenPostprocessor {

	/**
	 * The list of <code>TokenPostprocessors</code>.
	 * 
	 */
    private List<TokenPostprocessor> postprocessors;

    /**
     * Goes through each <code>TokenPostprocessor</code>, invoking its
     * <code>postprocessor(Token)</code> method.
     * 
     * @param token the token to apply postprocessing to
     */
    public void postprocess(final Token token) {
        for (final TokenPostprocessor processor : this.postprocessors) {
            processor.postprocess(token);
        }
    }

    /**
     * Registers a list of <code>TokenPostprocessors</code> to this component.
     * 
     * @param postprocessors the list of <code>TokenPostprocessors</code>
     */
    public void setPostprocessors(final List<TokenPostprocessor> postprocessors) {
        this.postprocessors = postprocessors;
    }
}
