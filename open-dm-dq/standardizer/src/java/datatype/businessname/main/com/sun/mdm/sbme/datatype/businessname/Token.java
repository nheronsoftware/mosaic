/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.businessname;

import java.util.HashMap;
import java.util.Map;

/**
 * Contains a segment of the free form string, known as the image, and an input token type which identifies
 * the image.
 *
 * @author Shant Gharibi (shant.gharibi@sun.com)
 */
public class Token {

    private String image;

    private InputTokenType inputTokenType;
    
    private Map<Flag, Boolean> flags = new HashMap<Flag, Boolean>();

    /**
     * Create a token based on the image.
     * 
     * @param image a fragment of the original free form string
     */
    public Token(final String image) {
        this.image = image;
    }

    /**
     * Returns the image.
     * 
     * @return the image
     */
    public String getImage() {
        return this.image;
    }

    /**
     * Sets the image for this token.
     * 
     * @param image the image to set
     */
    public void setImage(final String image) {
        this.image = image;
    }

    /**
     * Returns the input token type which identifies the image contained
     * in this token.
     * 
     * @return the input token type representation
     */
    public InputTokenType getInputTokenType() {
        return this.inputTokenType;
    }

    /**
     * Sets the input token type which represents the image contained
     * in this token.
     * 
     * @param inputTokenType the input token type representing the image
     */
    public void setInputTokenType(final InputTokenType inputTokenType) {
        this.inputTokenType = inputTokenType;
    }
    
    /**
     * A flag tagging this token.
     * 
     * @param flag flag value
     * @param value true/false
     */
    public void setFlag(Flag flag, boolean value) {
    	flags.put(flag, value);
    }
    
    /**
     * Returns if the flag has been tagged or not.
     * 
     * @param flag the flag to check
     * @return true if flag has been tagged
     */
    public boolean getFlag(Flag flag) {
    	if (flags.containsKey(flag)) {
    		return flags.get(flag);
    	}
    	
    	return false;
    }

}
