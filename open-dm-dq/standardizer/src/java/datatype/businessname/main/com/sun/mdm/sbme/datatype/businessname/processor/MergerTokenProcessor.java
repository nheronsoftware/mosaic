/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.businessname.processor;

import static com.sun.mdm.sbme.datatype.businessname.InputTokenType.NF_NF;
import static com.sun.mdm.sbme.datatype.businessname.InputTokenType.NF_PN;
import static com.sun.mdm.sbme.datatype.businessname.InputTokenType.PNT;
import static com.sun.mdm.sbme.datatype.businessname.InputTokenType.PN_NF;
import static com.sun.mdm.sbme.datatype.businessname.InputTokenType.PN_PN;

import java.util.Map;
import java.util.StringTokenizer;

import com.sun.mdm.sbme.datatype.businessname.InputTokenType;
import com.sun.mdm.sbme.datatype.businessname.Token;
import com.sun.mdm.sbme.datatype.businessname.normalizer.NormalizedField;

/**
 * Component used to do transformations on tokens containing merger types.
 *
 * @author Shant Gharibi (shant.gharibi@sun.com)
 */
public class MergerTokenProcessor implements TokenProcessor {

	/**
	 * A map containing primary business names.
	 * 
	 */
    private Map<String, String> primaryNames;

    /**
     * A map containing merged business names.
     * 
     */
    private Map<String, String> mergerNames;

    /**
     * The empty constructor.  Use setter methods to register components.
     * 
     */
    public MergerTokenProcessor() {}
    
    /**
     * Constructs a new <code>MergerTokenProcessor</code> configured with a map of primary business names
     * and a map of merged business names.
     * 
     * @param primaryNames the map of primary business names
     * @param mergerNames the map of merged business names
     */
    public MergerTokenProcessor(Map<String, String> primaryNames, Map<String, String> mergerNames) {
    	this.primaryNames = primaryNames;
    	this.mergerNames = mergerNames;
    }
    
    /**
     * Performs transformations on the token and sets flags as appropriate.
     * 
     * @param token a fragment of a business name
     * @param tokenImage the pre-processed and transformed image of the token
     * @param normailizedField the normalized field used to store normalized data 
     */
    public void process(final Token token, final String tokenImage, final NormalizedField normalizedField) {
        // First search for the types of merged tokens
        token.setInputTokenType(this.searchForMergedTokenTypes(tokenImage));
        token.setImage(tokenImage);
    }

    /**
     * Searches the map of merged business names to see if the token is a merged company or not.
     * 
     * @param token the token to search for
     * @return an <code>InputTokenType</code> which described the kind of the token
     */
    private InputTokenType searchForMergedTokenTypes(String token) {
        /*
         * Before searching inside the token for business names, we first search
         * for the entire token inside the table of business primary names with
         * full.
         */
        if (this.mergerNames.containsKey(token)) {
            return PNT;
        }

        /*
         * Parse the string and search for special characters that are
         * considered as delimiters
         */
        final StringTokenizer stok = new StringTokenizer(token, "/");

        /* Count the number of fields inside the string */
        final int numberOfSubtokens = stok.countTokens();
        InputTokenType inputTokenType = null;
        for (int i = 0; i < numberOfSubtokens; i++) {
            // search for the different sub-tokens
            token = stok.nextToken();
            /* Search for tokens in the list of words */
            if (this.primaryNames.containsKey(token)) {
                // search for the second sub-tokens
                if (stok.hasMoreTokens()) {
                    token = stok.nextToken();
                    i++;
                }

                /* Search for tokens in the list of words */
                if (this.primaryNames.containsKey(token)) {
                    if (inputTokenType == null) {
                        inputTokenType = PN_PN;
                    }
                    continue;
                } else {
                    if (inputTokenType == null) {
                        inputTokenType = PN_NF;
                    }
                }
            } else {
                // search for the second subtokens
                if (stok.hasMoreTokens()) {
                    token = stok.nextToken();
                    i++;
                }

                /* Search for tokens in the list of location-related words */
                if (this.primaryNames.containsKey(token)) {
                    if (inputTokenType == null) {
                        inputTokenType = NF_PN;
                    }
                    continue;
                } else {
                    if (inputTokenType == null) {
                        inputTokenType = NF_NF;
                    }
                }
            }
        }
        return inputTokenType;
    }

    /**
     * Registers a map of primary names used by this processor.
     * 
     * @param primaryNames a map of primary business names
     */
    public void setPrimaryNames(final Map<String, String> primaryNames) {
        this.primaryNames = primaryNames;
    }

    /**
     * Registers a map of merger names used by this processor.
     * 
     * @param mergerNames a map of merged business names
     */
    public void setMergerNames(final Map<String, String> mergerNames) {
        this.mergerNames = mergerNames;
    }
}
