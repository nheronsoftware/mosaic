/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.businessname.normalizer.handler;

import java.util.List;
import java.util.Map;

import com.sun.mdm.sbme.datatype.businessname.Token;
import com.sun.mdm.sbme.datatype.businessname.normalizer.NormalizedField;

/**
 * Specialized <code>TokenHandler</code> implementation dealing with industry sectors.
 *
 * @author Shant Gharibi (shant.gharibi@sun.com)
 */
public class IndustrySectorTokenHandler implements TokenHandler {

	/**
	 * Map of industry category codes.
	 * 
	 */
    private Map<String, String> industryCategoryCodes;
    
    /**
     * Converts industry sector codes to a meaningful textual representation.
     * 
     * @param tokens list of tokens
     * @param normalizedField the normalized field
     */
	public void handle(List<Token> tokens, NormalizedField normalizedField) {
		List<String> industrySectorList = normalizedField.getIndustrySectorList();
		
		int len = industrySectorList.size();
        // make sure that every element in the industrySectorList is unique
        for (int i = 0; i < len - 1; i++) {
            for (int k = i + 1; k < len; k++) {
                if (industrySectorList.get(i).equals(industrySectorList.get(k))) {
                    industrySectorList.remove(k);
                    len--;
                }
            }
        }       
       
        for (int i = 0; i < len; i++) {
            // Convert the industry sector code to a meaningful text representation
            final String code = industrySectorList.get(i);
            if (this.industryCategoryCodes.containsKey(code)) {
                industrySectorList.set(i, this.industryCategoryCodes.get(code));
            }
        }

	}

	/**
	 * Sets the industry category codes to lookup against.
	 * 
	 * @param industryCategoryCodes the map of industry category codes
	 */
	public void setIndustryCategoryCodes(Map<String, String> industryCategoryCodes) {
		this.industryCategoryCodes = industryCategoryCodes;
	}

}
