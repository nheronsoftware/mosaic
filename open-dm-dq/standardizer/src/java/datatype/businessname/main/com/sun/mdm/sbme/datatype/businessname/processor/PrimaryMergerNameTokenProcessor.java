/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.businessname.processor;

import static com.sun.mdm.sbme.datatype.businessname.InputTokenType.PNT;
import static com.sun.mdm.sbme.datatype.businessname.normalizer.AliasType.CompanyAcronym;
import static com.sun.mdm.sbme.datatype.businessname.normalizer.AliasType.CompanyFormerName;

import java.util.Map;

import com.sun.inti.components.string.transform.StringTransformer;
import com.sun.mdm.sbme.datatype.businessname.Token;
import com.sun.mdm.sbme.datatype.businessname.normalizer.AliasTypeRegistry;
import com.sun.mdm.sbme.datatype.businessname.normalizer.NormalizedField;

/**
 * Component used to perform transformation on primary or merger business names.
 *
 * @author Shant Gharibi (shant.gharibi@sun.com)
 */
public class PrimaryMergerNameTokenProcessor implements TokenProcessor {

    private Map<String, String> primaryNames;
    private Map<String, String> newNames;
    private AliasTypeRegistry aliasTypeRegistry;
    private StringTransformer transformer;

    /**
     * The empty constructor.  Must use setter methods to register components.
     * 
     */
    public PrimaryMergerNameTokenProcessor() {}
    
    /**
     * Constructs a new <code>PrimaryMergerNameTokenProcessor</code> configured with a map
     * of primary names, new names, an alias type registry, and a string transformer.
     * 
     * @param primaryNames the map of primary names data
     * @param newNames the map of new names data
     * @param aliasTypeRegistry the registry of alias types
     * @param transformer the string transformer
     */
    public PrimaryMergerNameTokenProcessor(Map<String, String> primaryNames, Map<String, String> newNames,
    		AliasTypeRegistry aliasTypeRegistry, StringTransformer transformer) {
    	this.primaryNames = primaryNames;
    	this.newNames = newNames;
    	this.aliasTypeRegistry = aliasTypeRegistry;
    	this.transformer = transformer;
    }
    
    /**
     * First transforms the token image according to the configured <code>StringTransformer</code>. Then
     * searches for a corresponding acronym for the transformed image as the alias, and then search
     * for the new name of the transformed image.
     * 
     * @param token a fragment of the business name component containing a primary name or merger name
     * @param tokenImage a pre-processed image of the token
     * @param normalizedField the normalized field used to store normalized values
     */
    public void process(final Token token, final String tokenImage, final NormalizedField normalizedField) {
        final String transformedImage = transformer.transform(tokenImage);
        normalizedField.getIndustrySectorList().add(this.primaryNames.get(transformedImage));

        // Search for any corresponding acronym
        if (this.aliasTypeRegistry.containsName(transformedImage)) {
        	normalizedField.addAlias(CompanyAcronym, this.aliasTypeRegistry.getAliasByName(transformedImage));
            normalizedField.setIncompleteAlias(true);
        }

        // Search for the company former name
        if (this.newNames.containsKey(transformedImage)) {
        	normalizedField.addAlias(CompanyFormerName, this.newNames.get(transformedImage));
            normalizedField.setIncompleteAlias(true);
        }

        normalizedField.setPrimaryName(true);
        token.setInputTokenType(PNT);
        token.setImage(tokenImage);
    }

    /**
     * Registers a map of primary names data.
     * 
     * @param primaryNames the map of primary names
     */
    public void setPrimaryNames(final Map<String, String> primaryNames) {
        this.primaryNames = primaryNames;
    }

    /**
     * Registers a map of new company names (either through a name change or merge).
     * 
     * @param newNames the map of new company names
     */
    public void setNewNames(final Map<String, String> newNames) {
        this.newNames = newNames;
    }

    /**
     * Register a registry of business name alias'.
     * 
     * @param aliasTypeRegistry an alias registry
     */
    public void setAliasTypes(final AliasTypeRegistry aliasTypeRegistry) {
        this.aliasTypeRegistry = aliasTypeRegistry;
    }

    /**
     * Registers a <code>StringTransformer</code> used to transformer the token image
     * before any further processing.
     * 
     * @param transformer the <code>StringTransformer</code>
     */
    public void setTransformer(StringTransformer transformer) {
        this.transformer = transformer;
    }
}
