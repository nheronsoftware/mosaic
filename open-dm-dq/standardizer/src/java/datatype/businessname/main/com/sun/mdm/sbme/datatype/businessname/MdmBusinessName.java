/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.businessname;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * The MdmBusinessName represents and manipulates any business name record that
 * has been broken down into its basic components
 * 
 */
public class MdmBusinessName {
    // Added for capturing pattern signature
    private String signature;

    public String getSignature() {
        return this.signature;
    }

    public void setSignature(final String signature) {
        this.signature = signature;
    }

    /** Defines the primary name associated with a business entity */
    public static final String PRIMARY_NAME = "PrimaryName";

    /** Defines the organization type of the business entity */
    public static final String ORG_TYPE_KEY = "OrgTypeKeyword";

    /** Defines the association type of the business entity */
    public static final String ASSOC_TYPE_KEY = "AssocTypeKeyword";

    /** Defines the association type of the business entity */
    public static final String INDUSTRY_TYPE_KEY = "IndustryTypeKeyword";

    /** Defines the association type of the business entity */
    public static final String ALIAS_LIST = "AliasList";

    /** Defines the association type of the business entity */
    public static final String URL_NAME = "Url";

    /** Defines the association type of the business entity */
    public static final String INDUSTRY_SECTOR_LIST = "IndustrySectorList";

    /** Defines the association type of the business entity */
    public static final String NOT_FOUND_FIELDS = "NotFoundFields";
    
    /** Defines the not found types */
    public static final String NOT_FOUND_TYPE = "NotFoundType";

    /**
     * Used in the standardizationEngine class to identify the type of records
     */
    public static final String BUSINESS_NAME = "BusinessName";

    /* Holds keys-values of the different fields */
    private final HashMap businessFieldName;

    /* Holds keys-values of the status of the different fields */
    private final HashMap businessFieldStatus;

    /* Holds keys-values of the precision of the different records */
    private final HashMap businessRecordAccuracy;

    /**
     * Public constructor
     */
    public MdmBusinessName() {
        this.businessFieldName = new HashMap();
        this.businessFieldStatus = new HashMap();
        this.businessRecordAccuracy = new HashMap();
    }

    /**
     * This method identify the type of this class's objects (BusinessName)
     * 
     * @return a string
     */
    public String getType() {
        return BUSINESS_NAME;
    }

    /**
     * This getter method provides the value associated with primary name
     * 
     * @return a company primary name
     */
    public String getPrimaryName() {
        return (String) this.businessFieldName.get(PRIMARY_NAME);
    }

    /**
     * This method sets the value associated with primary name
     * 
     * @param value
     *            the company primary name
     */
    public void setPrimaryName(final String value) {
        this.businessFieldName.put(PRIMARY_NAME, value);
    }

    /**
     * This getter method provides the value associated with organization type
     * key
     * 
     * @return an organization type key
     */
    public String getOrgTypeKeyword() {
        return (String) this.businessFieldName.get(ORG_TYPE_KEY);
    }

    /**
     * This method sets the value associated with organization type key
     * 
     * @param value
     *            of the organization type key
     */
    public void setOrgTypeKeyword(final String value) {
        this.businessFieldName.put(ORG_TYPE_KEY, value);
    }

    /**
     * This getter method provides the value associated with association type
     * key
     * 
     * @return an association type key
     */
    public String getAssocTypeKeyword() {
        return (String) this.businessFieldName.get(ASSOC_TYPE_KEY);
    }

    /**
     * This method sets the value associated with association type key
     * 
     * @param value
     *            of the company association type key
     */
    public void setAssocTypeKeyword(final String value) {
        this.businessFieldName.put(ASSOC_TYPE_KEY, value);
    }

    /**
     * This getter method provides the value associated with the alias list
     * 
     * @return a list of alternative names
     */
    public List getAliasList() {
        return (ArrayList) this.businessFieldName.get(ALIAS_LIST);
    }

    /**
     * This method sets the value associated with the alias list
     * 
     * @param values
     *            list of the company's alternative names
     */
    public void setAliasList(final List values) {
        this.businessFieldName.put(ALIAS_LIST, values);
    }

    /**
     * This getter method provides the value associated with the industry type
     * key
     * 
     * @return a company industry type key
     */
    public String getIndustryTypeKeyword() {
        return (String) this.businessFieldName.get(INDUSTRY_TYPE_KEY);
    }

    /**
     * This method sets the value associated with the industry type key
     * 
     * @param value
     *            of the company industry type key
     */
    public void setIndustryTypeKeyword(final String value) {
        this.businessFieldName.put(INDUSTRY_TYPE_KEY, value);
    }

    /**
     * This getter method provides the value associated with the url name
     * 
     * @return a company name as url
     */
    public String getUrl() {
        return (String) this.businessFieldName.get(URL_NAME);
    }

    /**
     * This method sets the value associated with the url name
     * 
     * @param value
     *            the company url name
     */
    public void setUrl(final String value) {
        this.businessFieldName.put(URL_NAME, value);
    }

    /**
     * This getter method provides the value associated with the industry
     * sector(s)
     * 
     * @return a list of industry sector(s)
     */
    public List getIndustrySectorList() {
        return (ArrayList) this.businessFieldName.get(INDUSTRY_SECTOR_LIST);
    }

    /**
     * This method sets the value associated with the industry sector(s)
     * 
     * @param values
     *            list of the company's alternative names
     */
    public void setIndustrySectorList(final List values) {
        this.businessFieldName.put(INDUSTRY_SECTOR_LIST, values);
    }

    /**
     * Getters methods that provide the values associated with each field
     * 
     * @return a company type
     */
    public String getNotFoundFields() {
        return (String) this.businessFieldName.get(NOT_FOUND_FIELDS);
    }

    /**
     * Setters methods that update the values associated with each field
     * 
     * @param value
     *            the company name
     */
    public void setNotFoundFields(final String value) {
        this.businessFieldName.put(NOT_FOUND_FIELDS, value);
    }

    /**
     * Generic getter method for fields' names.
     * 
     * @param key
     *            the field
     * @return the associated value
     */
    public String getValue(final String key) {
        if (this.businessFieldName.get(key) == null) {
            return "";
        }
        if (this.businessFieldName.get(key) instanceof ArrayList) {
            return (String) this.getValues(key).get(0);
        }
        return (String) this.businessFieldName.get(key);
    }

    /**
     * Generic setter method for fields' names.
     * 
     * @param key
     *            the field
     * @param value
     *            the field's value
     */
    public void setValue(final String key, final String value) {
        this.businessFieldName.put(key, value);
    }

    /**
     * Generic getter method for fields' names.
     * 
     * @param key
     *            the field's type
     * @return a string array
     */
    public List getValues(final String key) {
        if (this.businessFieldName.get(key) instanceof String) {
            final List<String> ar = new ArrayList<String>();
            ar.clear();
            ar.add((String) this.businessFieldName.get(key));
            return ar;
        }
        return (ArrayList) this.businessFieldName.get(key);
    }

    /**
     * Generic setter method for fields' names.
     * 
     * @param key
     *            the generic key
     * @param values
     *            the corresponding array of values
     */
    public void setValues(final String key, final List values) {
        this.businessFieldName.put(key, values);
    }

    /**
     * Generic getter method for fields' status.
     * 
     * @param key
     *            the status' field
     * @return a status value
     */
    public String getStatusValue(final String key) {
        return (String) this.businessFieldStatus.get(key);
    }

    /**
     * Generic setter method for fields' status.
     * 
     * @param key
     *            the status' field
     * @param value
     *            the status value
     */
    public void setStatusValue(final String key, final String value) {
        this.businessFieldStatus.put(key, value);
    }

    /**
     * This method returns all the fields in a particular PersonName record as a
     * Setinterface (use HashMap to implement it).
     * 
     * @return a Set grouping all the fields
     */
    public Set getAllFields() {
        return this.businessFieldName.keySet();
    }

    /**
     * This method returns all the statuses values associated with the fields in
     * a given PersonName record as a Collection.
     * 
     * @return a Collection of status values
     */
    public Collection getAllStatusValues() {
        return this.businessFieldStatus.values();
    }

    /**
     * This method returns all the names associated with the fields in a given
     * PersonName record as a Collection.
     * 
     * @return a Collection of fields' values
     */
    public Collection getAllNameValues() {
        return this.businessFieldName.values();
    }

}
