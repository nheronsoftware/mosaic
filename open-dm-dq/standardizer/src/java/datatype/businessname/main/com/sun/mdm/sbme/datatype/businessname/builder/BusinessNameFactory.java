package com.sun.mdm.sbme.datatype.businessname.builder;

import com.sun.mdm.sbme.StandardizedRecord;
import com.sun.mdm.sbme.builder.StandardizedRecordFactory;
import com.sun.mdm.sbme.datatype.businessname.BusinessName;

/**
 * Returns a standardized record.
 *
 * @author Shant Gharibi (shant.gharibi@sun.com)
 */
public class BusinessNameFactory implements StandardizedRecordFactory {
	
	/**
	 * Return a new instance of a business name standardized record.
	 * 
	 * @return an empty standardized record
	 */
	public StandardizedRecord newStandardizedRecord() {
		return new BusinessName();
	}
}
