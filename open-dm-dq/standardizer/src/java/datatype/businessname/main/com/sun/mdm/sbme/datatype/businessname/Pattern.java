/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.businessname;

/**
 * Encapsulates the input and output pattern for a given record.
 *
 * @author Shant Gharibi (shant.gharibi@sun.com)
 */
public class Pattern {

    private int patternCount;
    private String inputPattern;
    private String outputPattern;

    /**
     * Constructs a new <code>Pattern</code> object configured with a pattern count,
     * an input pattern, and an output pattern.
     * 
     * @param patternCount the number of elements in the input pattern
     * @param inputPattern the input pattern
     * @param outputPattern the output pattern
     */
    public Pattern(final int patternCount, final String inputPattern, final String outputPattern) {
        this.patternCount = patternCount;
        this.inputPattern = inputPattern;
        this.outputPattern = outputPattern;
    }

    /**
     * Number of elements contained in the input pattern.
     * 
     * @return the number of elements in the pattern
     */
    public int getPatternCount() {
        return this.patternCount;
    }

    /**
     * Set the number of elements in the input pattern.
     * 
     * @param patternCount the number of elements in the input pattern.
     */
    public void setPatternCount(final int patternCount) {
        this.patternCount = patternCount;
    }

    /**
     * Returns the input pattern.
     * 
     * @return the input pattern
     */
    public String getInputPattern() {
        return this.inputPattern;
    }

    /**
     * Sets the input pattern.
     * 
     * @param inputPattern the input pattern
     */
    public void setInputPattern(final String inputPattern) {
        this.inputPattern = inputPattern;
    }

    /**
     * Gets the output pattern.
     * 
     * @return the output pattern
     */
    public String getOutputPattern() {
        return this.outputPattern;
    }

    /**
     * Sets the output pattern.
     * 
     * @param outputPattern the output pattern
     */
    public void setOutputPattern(final String outputPattern) {
        this.outputPattern = outputPattern;
    }

}
