package com.sun.mdm.sbme.datatype.businessname.builder;

import java.util.List;

import com.sun.mdm.sbme.StandardizedRecord;
import com.sun.mdm.sbme.datatype.businessname.Pattern;
import com.sun.mdm.sbme.datatype.businessname.Token;
import com.sun.mdm.sbme.datatype.businessname.normalizer.NormalizedField;

/**
 * 
 *
 * @author Shant Gharibi (shant.gharibi@sun.com)
 */
public interface BusinessNameBuilder {

    /**
     * Creates a standardized record which contains the standardized elements of the free form business name string.
     * 
     * @param pattern includes input and output pattern
     * @param tokens list of tokens
     * @param normalizedField object which contains the normalized fields
     * @return a StandardizedRecord which holds the standardized business
     *         entity
     */
	public StandardizedRecord buildStandardizedRecord(Pattern pattern, List<Token> tokens, NormalizedField normalizedField);
}
