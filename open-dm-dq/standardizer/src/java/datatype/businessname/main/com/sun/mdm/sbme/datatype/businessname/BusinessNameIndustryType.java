/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.businessname;

import java.util.List;

public class BusinessNameIndustryType {

    private String name;
    private String standardName;
    private List<String> sectorCodes;

    /**
     * Create a new <code>BusinessNameIndustryType</code> given a name, standardized name, and
     * industry sector codes.
     * 
     * @param name name
     * @param standardName standardized name
     * @param sectorCodes a list of industry sector codes
     */
    public BusinessNameIndustryType(final String name, final String standardName, final List<String> sectorCodes) {
        this.name = name;
        this.standardName = standardName;
        this.sectorCodes = sectorCodes;
    }

    /**
     * Returns the name.
     * 
     * @return name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Set name.
     * 
     * @param name name
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Returns the standardized name.
     * 
     * @return the standardized name
     */
    public String getStandardName() {
        return this.standardName;
    }

    /**
     * Sets the standardized name.
     * 
     * @param standardName the standardized name
     */
    public void setStandardName(final String standardName) {
        this.standardName = standardName;
    }

    /**
     * Returns the industry sector codes.
     * 
     * @return a list of industry sector codes
     */
    public List<String> getSectorCodes() {
        return this.sectorCodes;
    }

    /**
     * Sets the industry sector codes.
     * 
     * @param sectorCodes a list of industry sector codes
     */
    public void setSectorCodes(final List<String> sectorCodes) {
        this.sectorCodes = sectorCodes;
    }
}
