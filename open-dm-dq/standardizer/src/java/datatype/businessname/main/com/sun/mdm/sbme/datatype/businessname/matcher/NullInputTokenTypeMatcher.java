/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.businessname.matcher;

import com.sun.mdm.sbme.datatype.businessname.Token;
import com.sun.mdm.sbme.datatype.businessname.normalizer.NormalizedField;

/**
 * Responsible for matching tokens against null.
 *
 * @author Shant Gharibi (shant.gharibi@sun.com)
 */
public class NullInputTokenTypeMatcher implements TokenMatcher {

	/**
	 * Matches the token type against null.
	 * 
	 * @param token token
	 * @param string the pre-processed string
	 * @param normalizedField the normalized field
	 * 
	 * @return true if token's <code>InputTokenType</code> is null, false otherwise
	 */
    public boolean matches(final Token token, final String string, final NormalizedField normalizedFields) {
        return token.getInputTokenType() == null;
    }

}
