/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.businessname.normalizer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class NormalizedField {

	/**
	 * List of industry sector codes.
	 */
    private List<String> industrySectorList;

    /**
     * Flag indicating whether or not an alias is complete.
     */
    private boolean incompleteAlias;

    /**
     * Flag indicating whether or not field is a primary name.
     */
    private boolean primaryName;
    
    /**
     * A map representing alias types and its corresponding aliases.
     */
    private Map<AliasType, List<String>> aliasListType;

    /**
     * Creates a new <code>NormalizedField</code>.
     */
    public NormalizedField() {
        this.industrySectorList = new ArrayList<String>();
        this.aliasListType = new HashMap<AliasType, List<String>>();
    }
    
    /**
     * Returns a list of industry sector codes.
     * 
     * @return a list of industry sector codes
     */
    public List<String> getIndustrySectorList() {
        return this.industrySectorList;
    }

    /**
     * Tests for an incomplete alias.
     * 
     * @return true if alias is incomplete, false otherwise
     */
    public boolean isIncompleteAlias() {
        return this.incompleteAlias;
    }
    
    /**
     * Set the incomplete alias field.
     * 
     * @param incompleteAlias true if alias is incomplete, false otherwise 
     */
    public void setIncompleteAlias(final boolean incompleteAlias) {
        this.incompleteAlias = incompleteAlias;
    }

    /**
     * Tests if field is a primary name.
     * 
     * @return true if field is a primary name, false otherwise
     */
    public boolean isPrimaryName() {
        return this.primaryName;
    }

    /**
     * Set the primary name.
     * 
     * @param primaryName true if field is a primary name, false otherwise.
     */
    public void setPrimaryName(final boolean primaryName) {
        this.primaryName = primaryName;
    }
	
    /**
     * Adds an alias to the map of aliases based on <code>AliasType</code>.
     * 
     * @param aliasField the alias type to be included in
     * @param alias the alias to add
     */
	public void addAlias(AliasType aliasField, String alias) {
		List<String> aliasList = this.aliasListType.get(aliasField);
		if (aliasList == null) {
			aliasList = new ArrayList<String>();
			this.aliasListType.put(aliasField, aliasList);
		}
		
		aliasList.add(alias);
	}
	
	/**
	 * Get an alias associated with an <code>AliasType</code>.
	 * 
	 * @param aliasField
	 * @return
	 */
	public String getAlias(AliasType aliasField) {
		//FIXME Currently returning the first alias. How should we choose which alias to return?
		return this.aliasListType.get(aliasField).get(0);
	}
	
	/**
	 * Tests whether the alias type exists.
	 * 
	 * @param aliasField the alias type
	 * @return true if alias type already exists, false otherwise
	 */
	public boolean containsAlias(AliasType aliasField) {
		return this.aliasListType.containsKey(aliasField);
	}

}
