/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.businessname.parser.postprocessor;

import static com.sun.mdm.sbme.datatype.businessname.Flag.Glue;
import static com.sun.mdm.sbme.datatype.businessname.InputTokenType.SEP;
import static com.sun.mdm.sbme.datatype.businessname.InputTokenType.SEP_GLC;

import com.sun.mdm.sbme.datatype.businessname.Token;

/**
 * Processes a token containing a comma.
 *
 * @author Shant Gharibi (shant.gharibi@sun.com)
 */
public class CommaTokenPostprocessor implements TokenPostprocessor {

	/**
	 * Comma was seen in the token, flag the token as a <code>Flag.Glue</code>
	 * type flag.
	 * 
	 * @param token token containing comma
	 */
    public void postprocess(final Token token) {
        /*
         * It must be a one-character string (previously, we isolated the
         * commas)
         */
        // A glue type flag
        token.setFlag(Glue, true);

        if (token.getImage().length() == 1) {
            token.setInputTokenType(SEP);
            if (!Character.isLetter(token.getImage().charAt(0))) {
                token.setInputTokenType(SEP_GLC);
            }
        }
    }

}
