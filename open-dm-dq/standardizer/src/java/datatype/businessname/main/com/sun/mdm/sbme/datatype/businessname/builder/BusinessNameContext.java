package com.sun.mdm.sbme.datatype.businessname.builder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.sun.mdm.sbme.datatype.businessname.InputTokenType;

/**
 * Context class which contains values based on InputTokenTypes.
 *
 * @author Shant Gharibi (shant.gharibi@sun.com)
 */
public class BusinessNameContext {
    
    private Map<InputTokenType, List<String>> contextMap;
    
    /**
     * Default constructor, initializes underlying map.
     * 
     */
    public BusinessNameContext() {
        contextMap = new HashMap<InputTokenType, List<String>>();
    }
    
    /**
     * Add a value to the context.
     * 
     * @param key the key as an InputTokenType
     * @param value the value as a String
     */
    public void addValue(InputTokenType key, String value) {     
        List<String> propertyList = contextMap.get(key);
        if (propertyList == null) {
            propertyList = new ArrayList<String>();
            contextMap.put(key, propertyList);
        }
        
        propertyList.add(value);
    }
    
    /**
     * Returns the value associated with an InputTokenType.
     * 
     * @param key the InputTokenType
     * @return the value associated with the InputTokenType
     */
    public String getValue(InputTokenType key) {
        String value = "";
        
        List<String> nameList = contextMap.get(key);
        
        if (nameList != null) {
            for (String name: contextMap.get(key)) {
                value += name + " ";
            }
        }
        
        return value;
    }
    
    /**
     * Returns a set of InputTokenType keys.
     * 
     * @return a set of InputTokenType keys
     */
    public Set<InputTokenType> getKeys() {
    	return this.contextMap.keySet();
    }

}
