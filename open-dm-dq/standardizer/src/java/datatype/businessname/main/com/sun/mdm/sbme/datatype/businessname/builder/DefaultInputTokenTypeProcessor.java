package com.sun.mdm.sbme.datatype.businessname.builder;

import com.sun.inti.components.string.match.StringMatcher;
import com.sun.mdm.sbme.datatype.businessname.InputTokenType;

/**
 * A default implementation of an <code>InputTokenTypeProcessor</code>.
 *
 * @author Ricardo Rocha (ricardo.rocha@sun.com)
 */
public class DefaultInputTokenTypeProcessor implements InputTokenTypeProcessor {

    private StringMatcher matcher;
    
    private InputTokenType inputTokenType;

    /**
     * The default construct.
     */
    public DefaultInputTokenTypeProcessor() {}
    
    /**
     * Construct a <code>DefaultInputTokenTypeProcessor</code with a matcher and input token type.
     *  
     * @param matcher the <code>StringMatcher</code> to be used
     * @param inputTokenType the <code>InputTokenType</code> to match against
     */
    public DefaultInputTokenTypeProcessor(StringMatcher matcher, InputTokenType inputTokenType) {
    	this.matcher = matcher;
    	this.inputTokenType = inputTokenType;
    }
    
    /**
     * Processes a token if the matcher matches the token.
     *
     * @param token the image after pre-processing and cleansing
     * @param image the raw image
     * @param context context which holds values
     * @return true if the matcher matched the token
     */
    public boolean process(String token, String image, BusinessNameContext context) {
        if (matcher.matches(token)) {
            context.addValue(inputTokenType, image);
            
            return true;
        }
        
        return false;
    }

    /**
     * Set the matcher.
     * 
     * @param matcher the <code>StringMatcher</code> to set
     */
    public void setMatcher(StringMatcher matcher) {
        this.matcher = matcher;
    }

    /**
     * Set the input token type.
     * 
     * @param inputTokenType the <code>InputTokenType</code> to set
     */
    public void setInputTokenType(InputTokenType inputTokenType) {
        this.inputTokenType = inputTokenType;
    }

}
