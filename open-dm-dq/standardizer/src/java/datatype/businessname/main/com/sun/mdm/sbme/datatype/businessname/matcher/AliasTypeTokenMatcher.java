/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.businessname.matcher;

import com.sun.mdm.sbme.datatype.businessname.Token;
import com.sun.mdm.sbme.datatype.businessname.normalizer.AliasTypeRegistry;
import com.sun.mdm.sbme.datatype.businessname.normalizer.NormalizedField;

/**
 * Responsible for matching tokens against the <code>AliasTypeRegistry</code>.
 *
 * @author Shant Gharibi (shant.gharibi@sun.com)
 */
public class AliasTypeTokenMatcher implements TokenMatcher {

	/**
	 * The <code>AliasTypeRegistry</code> to match against.
	 * 
	 */
    private AliasTypeRegistry aliasTypeRegistry;
    
    /**
     * The empty constructor.
     * 
     */
    public AliasTypeTokenMatcher() {}
    
    /**
     * Construct an <code>AliasTypeTokenMatcher</code> based on an <code>AliasTypeRegistry</code>.
     * 
     * @param aliasTypeRegistry the alias type registry
     */
    public AliasTypeTokenMatcher(AliasTypeRegistry aliasTypeRegistry) {
    	this.aliasTypeRegistry = aliasTypeRegistry;
    }

    /**
     * Checks to see if the <code>AliasTypeRegistry</code> contains the given string.
     * 
     * @param token token
     * @param string pre-processed string
     * @param normalizedField the normalized field
     * 
     * @return true if <code>AliasTypeRegistry</code> contains the given string, false otherwise
     */
    public boolean matches(final Token token, final String string, final NormalizedField normalizedField) {
        if (!normalizedField.isPrimaryName()) {
            return this.aliasTypeRegistry.containsAlias(string);
        }
        return false;
    }

    /**
     * Sets the <code>AliasTypeRegistry</code>.
     * 
     * @param aliasTypeRegistry the alias type registry
     */
    public void setAliasTypes(final AliasTypeRegistry aliasTypeRegistry) {
        this.aliasTypeRegistry = aliasTypeRegistry;
    }
}
