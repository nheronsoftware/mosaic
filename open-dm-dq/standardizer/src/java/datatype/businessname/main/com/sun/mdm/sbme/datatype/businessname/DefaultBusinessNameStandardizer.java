/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.businessname;

import java.util.List;

import net.java.hulp.i18n.LocalizedString;

import com.sun.mdm.sbme.MdmStandardizationException;
import com.sun.mdm.sbme.StandardizedRecord;
import com.sun.mdm.sbme.Standardizer;
import com.sun.mdm.sbme.datatype.businessname.builder.BusinessNameBuilder;
import com.sun.mdm.sbme.datatype.businessname.normalizer.BusinessNameNormalizer;
import com.sun.mdm.sbme.datatype.businessname.normalizer.NormalizedField;
import com.sun.mdm.sbme.datatype.businessname.parser.BusinessNameParser;
import com.sun.mdm.sbme.datatype.businessname.pattern.BusinessNamePatternFinder;

/**
 * Processes any request for standardizing a generic free form
 * business name presented in the form of a string
 * 
 *  @author Shant Gharibi (shant.gharibi@sun.com)
 */
public class DefaultBusinessNameStandardizer implements Standardizer {

    /* Performs the parsing process */
    private BusinessNameParser parser;
    
    /* Performs the normalization process */
    private BusinessNameNormalizer normalizer;

    /* Performs pattern(s) identifications */
    private BusinessNamePatternFinder patternFinder;
    
    /* Performs name building */
    private BusinessNameBuilder nameBuilder;

    /**
     * Standardizes a free form business name into a standardized record.
     * 
     * @param record the string to standardize
     * @return a StandardizedRecord containing the standardization of the free form business name
     */
    public StandardizedRecord standardize(final String record) throws MdmStandardizationException {

        if (record == null || record.length() == 0) {
            // Jump to the next record if the record is empty or null
            return new BusinessName();
        }

        /*
         * Start first by parsing the string into tokens. We input a string, and
         * return an array of strings 'fieldList' which hold the fields.
         */
        final List<Token> tokens = this.parser.parse(record);

        /*
         * We then normalize the different fields. We send in the parsed fields
         * and return back the normalized ones. We keep the same number of
         * fields, but we replace them with their normalized forms if found, or
         * as an empty string if the field is to be removed. Also we define the
         * fieldType inside this method.
         */
        final NormalizedField normalizedField = this.normalizer.normalize(tokens);

        /*
         * Finally, identify the best pattern (the order in the arrangement of
         * the fields) for the record, and also the fields to be returned.
         */
        final Pattern pattern = this.patternFinder.getBestPatterns(tokens);

        return this.nameBuilder.buildStandardizedRecord(pattern, tokens, normalizedField);
    }

    /**
     * Sets the normalizer to be used by the standardizer.
     * 
     * @param normalizer the normalizer to set
     */
    public void setNormalizer(final BusinessNameNormalizer normalizer) {
        this.normalizer = normalizer;
    }

    /**
     * Sets the parser to be used by the standardizer.
     * 
     * @param parser the parser to set
     */
    public void setParser(final BusinessNameParser parser) {
        this.parser = parser;
    }

    /**
     * Sets the pattern finder to be used by the standardizer.
     * 
     * @param patternFinder the pattern finder to set
     */
    public void setPatternFinder(final BusinessNamePatternFinder patternFinder) {
        this.patternFinder = patternFinder;
    }

    /**
     * Sets the name builder to be used by the standardizer to build a business name entity.
     * 
     * @param nameBuilder the name builder to set
     */
    public void setNameBuilder(BusinessNameBuilder nameBuilder) {
        this.nameBuilder = nameBuilder;
    }
}