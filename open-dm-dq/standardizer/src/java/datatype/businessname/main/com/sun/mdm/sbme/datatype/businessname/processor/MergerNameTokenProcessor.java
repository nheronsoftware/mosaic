/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.businessname.processor;

import static com.sun.mdm.sbme.datatype.businessname.Flag.Merger;
import static com.sun.mdm.sbme.datatype.businessname.InputTokenType.PN_PN;
import static com.sun.mdm.sbme.datatype.businessname.normalizer.AliasType.CompanyAcronym;
import static com.sun.mdm.sbme.datatype.businessname.normalizer.AliasType.CompanyFormerName;

import java.util.Map;

import com.sun.inti.components.string.transform.StringTransformer;
import com.sun.mdm.sbme.datatype.businessname.Token;
import com.sun.mdm.sbme.datatype.businessname.normalizer.AliasTypeRegistry;
import com.sun.mdm.sbme.datatype.businessname.normalizer.NormalizedField;

/**
 * Component used to do transformations on tokens containing merger types.
 *
 * @author Shant Gharibi (shant.gharibi@sun.com)
 */
public class MergerNameTokenProcessor implements TokenProcessor {

	/**
	 * A map of merged company names used by the processor.
	 * 
	 */
    private Map<String, String> mergerNames;

    /**
     * A registry of alias' used by the processor.
     * 
     */
    private AliasTypeRegistry aliasTypeRegistry;

    /**
     * A map of new company names used by the processor.
     * 
     */
    private Map<String, String> newNames;
    
    /**
     * A <code>StringTransformer</code> used by the processor to do transformations
     * on a fragment of the business name (token).
     * 
     */
    private StringTransformer transformer;
    
    /**
     * The empty constructor.  Setter methods must be used to register components.
     * 
     */
    public MergerNameTokenProcessor() {}
    
    /**
     * Constructs a new <code>MergerNameTokenProcessor</code> configured with a map of merged business
     * names, a registry of alias', a map of new business names, and a <code>StringTransformer</code>.
     * 
     * @param mergerNames a map of merged business names
     * @param aliasTypeRegistry a registry of alias'
     * @param newNames a map of new business names
     * @param transformer a <code>StringTransformer</code> used for transformations on the token
     */
    public MergerNameTokenProcessor(Map<String, String> mergerNames, AliasTypeRegistry aliasTypeRegistry, Map<String, String> newNames,
    		StringTransformer transformer) {
    	this.mergerNames = mergerNames;
    	this.aliasTypeRegistry = aliasTypeRegistry;
    	this.newNames = newNames;
    	this.transformer = transformer;
    }

    /**
     * First transforms the token image according to the configured <code>StringTransformer</code>. Then
     * searches for a corresponding acronym for the transformed image as the alias, and then search
     * for the former name of the transformed image.
     * 
     * @param token a fragment of the business name component containing a primary name or merger name
     * @param tokenImage a pre-processed image of the token
     * @param normalizedField the normalized field used to store normalized values
     */
    public void process(final Token token, final String tokenImage, final NormalizedField normalizedField) {
        final String transformedImage = transformer.transform(tokenImage);
        normalizedField.getIndustrySectorList().add(this.mergerNames.get(transformedImage));

        // Search for any corresponding acronym
        if (this.aliasTypeRegistry.containsName(transformedImage)) {        	
        	normalizedField.addAlias(CompanyAcronym, this.aliasTypeRegistry.getAliasByName(transformedImage));
            normalizedField.setIncompleteAlias(true);
        }

        // Search for the company former name
        if (this.newNames.containsKey(transformedImage)) {
        	normalizedField.addAlias(CompanyFormerName, this.newNames.get(transformedImage));
            normalizedField.setIncompleteAlias(true);
        }

        token.setFlag(Merger, true);

        token.setInputTokenType(PN_PN);
        token.setImage(tokenImage);
    }

    /**
     * Registers a map of merged business names used by this component.
     * 
     * @param mergerNames a map of merged business names
     */
    public void setMergerNames(final Map<String, String> mergerNames) {
        this.mergerNames = mergerNames;
    }

    /**
     * Registers a registry of alias' used by this component.
     * 
     * @param aliasTypeRegistry a registry of alias'
     */
    public void setAliasTypes(final AliasTypeRegistry aliasTypeRegistry) {
        this.aliasTypeRegistry = aliasTypeRegistry;
    }

    /**
     * Registers a map of new business names used by this component.
     * 
     * @param newNames a map of new business names
     */
    public void setNewNames(final Map<String, String> newNames) {
        this.newNames = newNames;
    }

    /**
     * Sets a <code>StringTransformer</code> used by this component.
     * 
     * @param transformer a <code>StringTransformer</code>
     */
    public void setTransformer(StringTransformer transformer) {
        this.transformer = transformer;
    }
}
