/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.businessname.parser.postprocessor;

import static com.sun.mdm.sbme.datatype.businessname.Flag.Glue;
import static com.sun.mdm.sbme.datatype.businessname.InputTokenType.SEP;
import static com.sun.mdm.sbme.datatype.businessname.InputTokenType.SEP_GLD;

import com.sun.mdm.sbme.datatype.businessname.Token;

/**
 * <code>TokenPostprocessor</code> which handles tokens containing a dash character.
 *
 * @author Shant Gharibi (shant.gharibi@sun.com)
 */
public class DashTokenPostprocessor implements TokenPostprocessor {

	/**
	 * Set appropriate flags if the token contains a dash character.
	 * 
	 * @param token the token containing a dash
	 */
    public void postprocess(final Token token) {
        final String image = token.getImage();

        /* Case where we have a one-character token */
        if (image.length() == 1 && (image.charAt(0) == '-' || image.charAt(0) == '�')) {
            // A glue flag
            token.setFlag(Glue, true);

            return;
        }

        /* Case where we have a two-character token */
        if (image.length() == 2 && image.charAt(0) == '-' && image.charAt(1) == '-') {
            // A separator
            token.setInputTokenType(SEP);
            return;
        }

        // case where the token is more than one character
        for (int i = 0; i < image.length(); i++) {
            if (image.charAt(i) == '-') {
                if (i > 0 && i < image.length() - 2 && Character.isLetterOrDigit(image.charAt(i + 1)) && Character.isLetterOrDigit(image.charAt(i - 1))) {
                    /* If the dash is in the middle of the field */
                	token.setFlag(Glue, true);
                    return;
                }
            }
        }

        if (token.getImage().length() == 1 || !Character.isLetter(token.getImage().charAt(0))) {
            token.setInputTokenType(SEP_GLD);
        }
    }

}
