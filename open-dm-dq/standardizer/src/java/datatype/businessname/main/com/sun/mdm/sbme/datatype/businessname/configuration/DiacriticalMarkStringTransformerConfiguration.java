package com.sun.mdm.sbme.datatype.businessname.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Bean;
import com.sun.inti.components.string.transform.DiacriticalMarkStringTransformer;

@Configuration
/**
 * A spring framework bean configuration class to retrieve a <code>DiacriticalMarkStringTransformer</code>.
 * 
 */
public class DiacriticalMarkStringTransformerConfiguration {

    @Bean
    /**
     * Retrieves a <code>DiacriticalMarkStringTransformer</code>.
     *
     * @return a <code>DiacriticalMarkStringTransformer</code>
     */
    public DiacriticalMarkStringTransformer diacriticalMarkStringTransformer() {
        return new DiacriticalMarkStringTransformer();
    }
}
