/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.businessname.processor;

import static com.sun.mdm.sbme.datatype.businessname.Flag.Merger;
import static com.sun.mdm.sbme.datatype.businessname.InputTokenType.IDT;
import static com.sun.mdm.sbme.datatype.businessname.InputTokenType.IDT_AJT;

import java.util.List;
import java.util.Map;

import com.sun.inti.components.string.transform.StringTransformer;
import com.sun.mdm.sbme.datatype.businessname.BusinessNameIndustryType;
import com.sun.mdm.sbme.datatype.businessname.Token;
import com.sun.mdm.sbme.datatype.businessname.normalizer.NormalizedField;

/**
 * Component used to do transformations on tokens containing industry type.
 *
 * @author Shant Gharibi (shant.gharibi@sun.com)
 */
public class IndustryTypeTokenProcessor implements TokenProcessor {

	/**
	 * A map of <code>BusinessNameIndustryType's</code> used to lookup a token industry type.
	 * 
	 */
    private Map<String, BusinessNameIndustryType> industryRegistry;

    /**
     * A list of defined adjectives used to identify adjectives in a business name.
     * 
     */
    private List<String> adjectiveTypes;
    
    /**
     * A string transformer used for transformations or cleansing of the token image.
     * 
     */
    private StringTransformer transformer;

    /**
     * Constructs a new <code>IndustryTypeTokenProcessor</code>. If using the empty constructor,
     * setter method must be used to configure this component.
     * 
     */
    public IndustryTypeTokenProcessor() {}
    
    /**
     * Constructs a new <code>IndustryTypeTokenProcessor</code> configured with a business name
     * industry type map, a list of adjectives, and a string transformer.
     * 
     * @param industryRegistry a map of industry types
     * @param adjectiveTypes a list of configured adjectives
     * @param transformer a string transformer
     */
    public IndustryTypeTokenProcessor(Map<String, BusinessNameIndustryType> industryRegistry, 
    		                          List<String> adjectiveTypes,
    		                          StringTransformer transformer) {
    	this.industryRegistry = industryRegistry;
    	this.adjectiveTypes = adjectiveTypes;
    	this.transformer = transformer;
    }
    
    /**
     * After transforming the token image with a <code>StringTransformer</code>, lookup the
     * industry type and associate a industry category code with the industry.  If the industry
     * type overlaps with an adjective, use a type that accounts for both.
     * 
     * @param token the token containing the industry type
     * @param tokenImage the pre-processed and possibly transformed token image
     * @param normalizedField the normalized field
     */
    public void process(final Token token, final String tokenImage, final NormalizedField normalizedField) {

        final BusinessNameIndustryType industryTypeField = this.industryRegistry.get(transformer.transform(tokenImage));

        // Read the list of industry categories associated with the field
        if (!normalizedField.isPrimaryName() && !token.getFlag(Merger)) {
            for (final String sectorCode : industryTypeField.getSectorCodes()) {
                normalizedField.getIndustrySectorList().add(0, sectorCode);
            }
        }

        normalizedField.setIncompleteAlias(true);
        // If there is an overlapping between the two different types
        // (industry and adjective), then use a type that account for both of
        // them
        if (this.adjectiveTypes.contains(token.getImage())) {
            token.setInputTokenType(IDT_AJT);
        } else {
            token.setInputTokenType(IDT);
        }

        token.setImage(tokenImage);
        if (!industryTypeField.getStandardName().equals("0")) {
            token.setImage(industryTypeField.getStandardName());
        }
    }

    /**
     * Registers a map of industry types for use by this processor.
     * 
     * @param industryRegistry a map of industry types
     */
    public void setIndustryRegistry(final Map<String, BusinessNameIndustryType> industryRegistry) {
        this.industryRegistry = industryRegistry;
    }

    /**
     * Registers a list of configured adjectives for use by this processor.
     * 
     * @param adjectiveTypes a list of adjectives
     */
    public void setAdjectiveTypes(final List<String> adjectiveTypes) {
        this.adjectiveTypes = adjectiveTypes;
    }

    /**
     * Registers a <code>StringTransformer</code> used by this processor to transform
     * the token image.
     * 
     * @param transformer a <code>StringTransformer</code>
     */
	public void setTransformer(StringTransformer transformer) {
		this.transformer = transformer;
	}
}
