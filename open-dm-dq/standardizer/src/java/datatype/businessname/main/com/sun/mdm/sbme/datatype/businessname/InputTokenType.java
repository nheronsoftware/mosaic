/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.businessname;

import java.util.HashMap;
import java.util.Map;

import net.java.hulp.i18n.LocalizedString;

import com.sun.mdm.sbme.MdmStandardizationException;

/**
 * Enumeration class containing all the possible input token types for business names.
 *
 * @author Shant Gharibi (shant.gharibi@sun.com)
 */
public enum InputTokenType {
    ALT("ALT"),
    AST("AST"),
    CTT("CTT"),
    NFG("NFG"),
    IDT_AJT("IDT-AJT"),
    IDT("IDT"),
    PN_PN("PN-PN"),
    PNT("PNT"),
    PN_NF("PN-NF"),
    NF_NF("NF-NF"),
    NF_PN("NF-PN"),
    ORT("ORT"),
    GLU("GLU"),
    SEP("SEP"),
    SEP_GLC("SEP-GLC"),
    SEP_GLD("SEP-GLD"),
    URL("URL"),
    GLU_MRG("GLU-MRG"),
    BCT("BCT"),
    CNT("CNT"),
    NAT("NAT"),
    CST("CST"),
    AJT("AJT"),
    NFC("NFC"),
    NF("NF"),
    DEL("DEL"),
    AND("AND")
    ;
    
    private final String type;
    
    private static final Map<String, InputTokenType> typeMap = new HashMap<String, InputTokenType>();
    static {
        for (final InputTokenType type : InputTokenType.values()) {
            typeMap.put(type.toString(), type);
        }
    }
    
    /**
     * Construct an InputTokenType
     * 
     * @param type the string representation of the input token type
     */
    InputTokenType(final String type) {
        this.type = type;
    }
    
    @Override
	public String toString(){
        return this.type;
    }
    
    /**
     * Return an InputTokenType based on its string representation.
     * 
     * @param type the input token type as a string
     * @return the InputTokenType enumeration class based on the input string
     * @throws MdmStandardizationException if no such input token type exists
     */
    public static InputTokenType getImageType(final String type) throws MdmStandardizationException {
        if (!typeMap.containsKey(type)) {
    		net.java.hulp.i18n.Logger sLog = net.java.hulp.i18n.Logger.getLogger(InputTokenType.class);
    		Localizer localizer = Localizer.get();
    		LocalizedString message = localizer.x("STD001: No such input token type: {0}", type);
    		sLog.severe(message); 
            throw new MdmStandardizationException(message);
        }

        return typeMap.get(type);
    }
 
}
