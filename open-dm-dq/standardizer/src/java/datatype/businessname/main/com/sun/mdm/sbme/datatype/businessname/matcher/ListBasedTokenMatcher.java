/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.businessname.matcher;

import java.util.List;

import com.sun.mdm.sbme.datatype.businessname.Token;
import com.sun.mdm.sbme.datatype.businessname.normalizer.NormalizedField;

/**
 * Responsible for matching tokens against a configured list.
 *
 * @author Shant Gharibi (shant.gharibi@sun.com)
 */
public class ListBasedTokenMatcher implements TokenMatcher {

	/**
	 * The list to match against.
	 * 
	 */
    private List<String> list;

    /**
     * The empty constructor.
     * 
     */
    public ListBasedTokenMatcher() {}
    
    /**
     * Construct a <code>ListBasedTokenMatcher</code> based off of a list.
     * 
     * @param list the list to match against
     */
    public ListBasedTokenMatcher(List<String> list) {
    	this.list = list;
    }
    
    /**
     * Matches the given string against the configured list.
     * 
     * @param token token
     * @param string the pre-processed string
     * @param normalizedField the normalized field
     * 
     * @return true if given string appears in the configured list, false otherwise
     */
    public boolean matches(final Token token, final String string, final NormalizedField normalizedFields) {
        return this.list.contains(string);
    }

    /**
     * Sets the list to match against.
     * 
     * @param list the list to match against
     */
    public void setList(final List<String> list) {
        this.list = list;
    }

}
