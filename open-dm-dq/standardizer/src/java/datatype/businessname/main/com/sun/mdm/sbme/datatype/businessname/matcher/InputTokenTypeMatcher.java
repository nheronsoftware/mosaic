package com.sun.mdm.sbme.datatype.businessname.matcher;

import com.sun.inti.components.string.match.StringMatcher;
import com.sun.mdm.sbme.datatype.businessname.InputTokenType;

/**
 * Responsible for matching tokens against an <code>InputTokenType</code>.
 *
 * @author Shant Gharibi (shant.gharibi@sun.com)
 */
public class InputTokenTypeMatcher implements StringMatcher {

	/**
	 * The <code>InputTokenType</code> to match against.
	 * 
	 */
    private InputTokenType type;
    
    /**
     * The empty constructor.
     * 
     */
    public InputTokenTypeMatcher() {}
    
    /**
     * Construct an <code>InputTokenTypeMatcher</code> based on a <code>InputTokenType</code>.
     * 
     * @param type the <code>InputTokenType</code>
     */
    public InputTokenTypeMatcher(InputTokenType type) {
    	this.type = type;
    }
    
    /**
     * Matches given string against configured <code>InputTokenType</code>.
     * 
     * @param the string to match
     * 
     * @return true if given string equals the configured <code>InputTokenType's</code> toString() method, false otherwise
     */
    public boolean matches(String string) {
        return type.toString().equals(string);
    }

    /**
     * Sets the <code>InputTokenType</code> to matches against.
     * 
     * @param type the <code>InputTokenType</code>
     */
    public void setType(InputTokenType type) {
        this.type = type;
    }

}
