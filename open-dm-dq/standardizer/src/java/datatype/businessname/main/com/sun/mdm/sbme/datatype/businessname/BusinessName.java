/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.businessname;

import com.stc.sbme.api.SbmeStandRecord;
import com.stc.sbme.api.SbmeStandRecordFactory;
import com.sun.mdm.sbme.StandardizedRecord;
import com.sun.mdm.sbme.datatype.businessname.normalizer.AliasType;
import com.sun.mdm.sbme.pattern.OutputPattern;

/**
 *  Represents a standardized business name record.
 *  
 *  @author Shant Gharibi (shant.gharibi@sun.com)
 */
public class BusinessName extends StandardizedRecord  {

	private String signature;
	
	@Override
	/**
	 * Returns the type of this standardized record.
	 * 
	 * @return a string representing the type of this standardized record
	 */
	public String getType() { 
		return "BusinessName"; 
	}
	
    @Override
    /**
     * Returns the signature of this standardized record.
     * 
     * @return a string representing the signature of this standardized record.
     */
	public String getSignature() {
    	return this.signature;
    }
    
    /**
     * Sets the signature of this standardized record.
     * 
     * @param signature the signature to set
     */
    public void setSignature(String signature) {
    	this.signature = signature;
    }
    
    // FIXME Remove after testing against legacy version
    private OutputPattern<?, ?>[] outputPatterns;
    
    /**
     * Return the output pattern associated with this standardized record.
     * 
     * @return the output pattern
     */
    public OutputPattern<?, ?>[] getOutputPatterns() {
        return this.outputPatterns;
    }
    
    /**
     * Sets the output pattern.
     * 
     * @param outputPatterns an array consisting of the output pattern
     */
    public void setOutputPatterns(final OutputPattern<?, ?>[] outputPatterns) {
        this.outputPatterns = outputPatterns;
    }

    private static final String[] mdmProperties = new String[] {
    	MdmBusinessName.PRIMARY_NAME,
    	MdmBusinessName.ORG_TYPE_KEY,
    	MdmBusinessName.ASSOC_TYPE_KEY,
    	MdmBusinessName.INDUSTRY_TYPE_KEY,
    	MdmBusinessName.URL_NAME,
    	MdmBusinessName.PRIMARY_NAME,
    	MdmBusinessName.NOT_FOUND_TYPE,
    	MdmBusinessName.INDUSTRY_SECTOR_LIST,
    	AliasType.CompanyAcronym.toString()
    };
    
	@Override
	/**
	 * Converts to an SbmeStandRecord for backward compatibility.
	 * 
	 * @return an SbmeStandRecord containing name/value pairs
	 */
	public SbmeStandRecord toSbmeStandRecord() {
		SbmeStandRecord record = SbmeStandRecordFactory.getInstance("BusinessName");
        for (String propertyName: mdmProperties) {
            String propertyValue = this.get(propertyName);
            if (propertyValue != null && propertyValue.length() > 0) {
                record.setValue(propertyName, propertyValue);
            }
        }
		
		return record;
	}
}
