package com.sun.mdm.sbme.datatype.address.builder.property;

import static com.sun.mdm.sbme.datatype.address.builder.CombinationOption.SPACE;

import com.sun.inti.components.record.Record;
import com.sun.mdm.sbme.datatype.address.Address;
import com.sun.mdm.sbme.datatype.address.pattern.InputTokenType;
import com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType;
import com.sun.mdm.sbme.normalizer.NormalizedToken;

public class ContextAddressPropertyBuilder extends BaseAddressPropertyBuilder {
	private String contextPropertyName;

    protected void buildProperty(Address address, NormalizedToken<InputTokenType> normalizedToken, InputTokenType inputTokenType, OutputTokenType outputTokenType, Record context) {
        if (this.abbreviationOption == null) {
            this.abbreviationOption = getLocale().getAbbreviationOption();
        }
        
        if (this.combinationOption == null) {
            this.combinationOption = SPACE;
        }
        
        final int clueWordId = normalizedToken.getClueWordId(inputTokenType);
        this.loadField(context.getStringBuilder(this.contextPropertyName), normalizedToken.getImage(), clueWordId, inputTokenType, this.abbreviationOption, this.combinationOption);
    }

    public String getContextPropertyName() {
        return contextPropertyName;
    }

    public void setContextPropertyName(String contextPropertyName) {
        this.contextPropertyName = contextPropertyName;
    }
}
