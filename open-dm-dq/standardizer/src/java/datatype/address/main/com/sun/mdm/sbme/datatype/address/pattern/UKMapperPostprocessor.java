package com.sun.mdm.sbme.datatype.address.pattern;

import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.STREET_NAME;

import java.util.List;

import com.sun.mdm.sbme.pattern.OutputPattern;
import com.sun.mdm.sbme.pattern.OutputPatternPostprocessor;

public class UKMapperPostprocessor implements OutputPatternPostprocessor<InputTokenType, OutputTokenType> {
	public void postprocess(List<? extends OutputPattern<InputTokenType, OutputTokenType>> patternsFound) {
        for (OutputPattern<InputTokenType, OutputTokenType> pattern: patternsFound) {
            processPattern((AddressOutputPattern) pattern);
        }
    }
    
    private static void processPattern(AddressOutputPattern pattern) {
        for (int i = 0; i < pattern.getOutputTokenTypes().size(); i++) {
            switch (pattern.getOutputTokenType(i)) {
                case EXTRA_INFORMATION:
                    pattern.setOutputTokenType(i, STREET_NAME);
                    break;
                case STREET_NAME:
                    i++;
                    break;
                default:
                    return;
            }
        }
    }
}
