/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.address.pattern;

import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.COMMON_WORD;
import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.GENERIC_WORD;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.CONJUNCTION;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.ObjectOutputStream;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.sun.inti.components.record.DefaultRecordPopulator;
import com.sun.inti.components.record.DefaultRecordSource;
import com.sun.inti.components.record.LineReader;
import com.sun.inti.components.record.PropertyDescriptor;
import com.sun.inti.components.record.Record;
import com.sun.inti.components.record.RecordPopulator;
import com.sun.inti.components.record.RecordSource;
import com.sun.inti.components.string.format.BooleanFormatter;
import com.sun.inti.components.string.format.EnumerationFormatter;
import com.sun.inti.components.string.format.EnumerationListFormatter;
import com.sun.inti.components.string.format.IntegerFormatter;
import com.sun.inti.components.string.tokenize.FixedFieldTokenizer;
import com.sun.inti.components.string.tokenize.FixedFieldTokenizer.FieldDescriptor;
import com.sun.mdm.sbme.clue.ClueRegistry;

/**
 *
 */
public class PatternRegistry {
    private Map<List<InputTokenType>, AddressPattern> patternMap = new HashMap<List<InputTokenType>, AddressPattern>();

    public PatternRegistry() {}
    
    public PatternRegistry(Map<List<InputTokenType>, AddressPattern> patternMap) {
    	this.patternMap = patternMap;
    }
    
    public AddressPattern lookup(final List<InputTokenType> candidatePatternTypes) {
        // Search for NL (null) tokens
        boolean commonWordFound = false;
        final List<InputTokenType> significantTokens = new ArrayList<InputTokenType>();
        for (InputTokenType type: candidatePatternTypes) {
            if (type == COMMON_WORD) {
                commonWordFound = true;
            } else {
                significantTokens.add(type);
            }
        }

        // If all tokens are NLs, then return
        if (significantTokens.size() == 0) {
            return null;
        }

        // Collapse consecutive AUs
        int wordCount = -1;
        boolean previousGenericWord = false;
        boolean hasContiguousGenericWords = false;
        List<InputTokenType> searchTokens = new ArrayList<InputTokenType>();
        final int[] genericWordOccurrenceCount = new int[significantTokens.size()];
        for (InputTokenType type: significantTokens) {
            if (previousGenericWord && type == GENERIC_WORD) {
                hasContiguousGenericWords = true;
                genericWordOccurrenceCount[wordCount]++;
                continue;
            }
            
            wordCount++;

            searchTokens.add(type);

            previousGenericWord = type == GENERIC_WORD;
        }

        // pattern string FOUND
        final AddressPattern addressPattern = this.patternMap.get(searchTokens);
        if (addressPattern != null) {
            List<OutputTokenType> outputTokens = addressPattern.getOutputTokenTypes();
            if (hasContiguousGenericWords) {
                outputTokens = new ArrayList<OutputTokenType>();
                for (int i = 0; i <= wordCount; i++) {
                    for (int j = 0; j <= genericWordOccurrenceCount[i]; j++) {
                        outputTokens.add(addressPattern.getOutputTokenTypes().get(i));
                    }
                }
            }

            // Reload NLs if any were sent
            List<OutputTokenType> resultOutputTokens = outputTokens;
            if (commonWordFound) {
                int count = 0;
                resultOutputTokens = new ArrayList<OutputTokenType>();
                for (int i = 0; i < candidatePatternTypes.size(); i++) {
                    if (candidatePatternTypes.get(i) == COMMON_WORD) {
                        resultOutputTokens.add(CONJUNCTION);
                    } else {
                        resultOutputTokens.add(outputTokens.get(count++));
                    }
                }
            }

            return new AddressPattern(addressPattern, candidatePatternTypes, resultOutputTokens);
        }

        return null;
    }

    public void setPatternMap(final Map<List<InputTokenType>, AddressPattern> patternMap) {
        this.patternMap = patternMap;
    }
	
    public static void main(String[] args) throws Exception {
    	if (args.length != 2) {
    		usage();
    	}
    	
    	String inputFileName = args[0];
    	File inputFile = new File(inputFileName);
    	if (!inputFile.canRead()) {
    		usage("Can't find input file: " + inputFileName);
    	}
    	
    	String outputFileName = args[1];
    	File outputFile = new File(outputFileName);
        outputFile.getParentFile().mkdirs();
    	if (!outputFile.getParentFile().canWrite()) {
    		usage("Can't open output file: " + outputFileName);
    	}

    	FixedFieldTokenizer firstTokenizer = new FixedFieldTokenizer();
        FieldDescriptor[] firstFieldDescriptors = new FieldDescriptor[] {
                new FieldDescriptor(0, 36, true), // Input token types
                new FieldDescriptor(37, 35, true), // Example
        };
        firstTokenizer.setFieldDescriptors(firstFieldDescriptors);
        PropertyDescriptor[] firstPropertyDescriptors = new PropertyDescriptor[] {
                new PropertyDescriptor("inputTokenTypes", new EnumerationListFormatter(InputTokenType.class, true), null),
                new PropertyDescriptor("example", null, null),
        };
        
        FixedFieldTokenizer secondTokenizer = new FixedFieldTokenizer();
        FieldDescriptor[] secondFieldDescriptors = new FieldDescriptor[] {
                new FieldDescriptor(0, 36, true), // Output token types
                new FieldDescriptor(37, 1, true), // Pattern class
                new FieldDescriptor(38, 1, true), // Scoring option
                new FieldDescriptor(39, 3, true), // Priority
                new FieldDescriptor(57, 1, true), // Usage flag
                new FieldDescriptor(58, 1, true), // Exclude option
        };
        secondTokenizer.setFieldDescriptors(secondFieldDescriptors);
        PropertyDescriptor[] secondPropertyDescriptors = new PropertyDescriptor[] {
                new PropertyDescriptor("outputTokenTypes", new EnumerationListFormatter(OutputTokenType.class, true), null),  
                new PropertyDescriptor("patternClass", new EnumerationFormatter(AddressPatternClass.class, true), null),  
                new PropertyDescriptor("scoringOption", new EnumerationFormatter(AddressScoringOption.class, true), null),   
                new PropertyDescriptor("priority", new IntegerFormatter(), null),   
                new PropertyDescriptor("usageFlag", null, ""),
                new PropertyDescriptor("exclude", new BooleanFormatter("X", ""), ""),
        };
        
        RecordPopulator[] recordPopulators = new RecordPopulator[] {
                new DefaultRecordPopulator(firstTokenizer, firstPropertyDescriptors),   
                new DefaultRecordPopulator(secondTokenizer, secondPropertyDescriptors), 
        };
        
        Reader reader = new FileReader(args[0]);
        Iterator<String> lineReader = new LineReader(reader, true);
        
        RecordSource recordSource = new DefaultRecordSource(recordPopulators, lineReader);
        
        Record record;
        Map<List<InputTokenType>, List<Record>> recordMap = new LinkedHashMap<List<InputTokenType>, List<Record>>();
        while ((record = recordSource.nextRecord()) != null) {
            if (!record.getBoolean("exclude")) {
                @SuppressWarnings("unchecked")
                List<InputTokenType> inputTokenTypes = (List<InputTokenType>) record.get("inputTokenTypes");
                List<Record> recordList = recordMap.get(inputTokenTypes);
                if (recordList == null) {
                    recordList = new LinkedList<Record>();
                    recordMap.put(inputTokenTypes, recordList);
                }
                recordList.add(record);
            }
        }

        Map<List<InputTokenType>, AddressPattern> patternMap = new LinkedHashMap<List<InputTokenType>, AddressPattern>();
        for (List<InputTokenType> inputTokenTypes: recordMap.keySet()) {
            List<Record> recordList = recordMap.get(inputTokenTypes);
            for (Record candidateRecord: recordList) {
                record = candidateRecord;
                if ("".equals(record.getString("usageFlag"))) {
                    break;
                }
            }
            @SuppressWarnings("unchecked")
            List<OutputTokenType> outputTokenTypes = (List<OutputTokenType>) record.get("outputTokenTypes");
            int priority = record.getInt("priority");
            AddressPatternClass addressPatternClass = (AddressPatternClass) record.get("patternClass");
            AddressScoringOption addressScoringOption = (AddressScoringOption) record.get("scoringOption");

            AddressPattern addressPattern = new AddressPattern(inputTokenTypes, outputTokenTypes, priority, addressPatternClass, addressScoringOption);
            patternMap.put(inputTokenTypes, addressPattern);
        }

        FileOutputStream fos = new FileOutputStream(outputFile);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(patternMap);
        oos.flush();
        oos.close();
    }
    
    private static void usage(String message) {
    	System.err.println(message);
    	usage();
    } 
    
    private static void usage() {
		System.err.println("Usage: java " + ClueRegistry.class.getName() + " inputFileName outputFileName enumerationClassName");
		System.exit(1);
    }
}
