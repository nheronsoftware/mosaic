package com.sun.mdm.sbme.datatype.address.builder.postprocessor;

import static com.sun.mdm.sbme.datatype.address.Address.MATCHING_STREET_NAME;
import static com.sun.mdm.sbme.datatype.address.Address.ORIGINAL_STREET_NAME;
import static com.sun.mdm.sbme.datatype.address.Address.STORAGE_STREET_NAME;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.STREET_NAME_EXTENSION_INDEX;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.STREET_NAME_PREFIX_DIRECTION;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.STREET_NAME_PREFIX_TYPE;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.STREET_NAME_SUFFIX_DIRECTION;

import com.sun.inti.components.record.Record;
import com.sun.mdm.sbme.datatype.address.Address;

public class EnglishStreetNameAddressPostprocessor extends AddressPostprocessor {
	/*
     * If the usage flag is C (commercial only), and only the street name field
     * was loaded, then move ALL_FIELDS to the building's name. Also, check for
     * numerics and ordinal numbers in the street name field and expand if
     * necessary.
     */
    protected void postprocess(Address address, Record context) {
    	String matchingStreetName = address.get(MATCHING_STREET_NAME);
    	String originalStreetName = address.get(ORIGINAL_STREET_NAME);
    	String storageStreetName = address.get(STORAGE_STREET_NAME);
    	String streetNameExtensionIndex = address.get(STREET_NAME_EXTENSION_INDEX);
    	String streetNamePrefixDirection = address.get(STREET_NAME_PREFIX_DIRECTION);
    	String streetNamePrefixType = address.get(STREET_NAME_PREFIX_TYPE);
    	String streetNameSuffixDirection = address.get(STREET_NAME_SUFFIX_DIRECTION);

        if (matchingStreetName != null && (originalStreetName == null || !matchingStreetName.equalsIgnoreCase(originalStreetName))) {
            if (getLocale().isCardinalPointAbbreviation(matchingStreetName)) {
            	matchingStreetName = getLocale().getCardinalPointName(matchingStreetName);
                address.set(MATCHING_STREET_NAME, matchingStreetName);
            }
        }

        // Check for spelled out OT's in the storage and matching names
        if (originalStreetName != null && this.isOrdinalTypeValue(originalStreetName)) {
        	storageStreetName = matchingStreetName;
        	address.set(STORAGE_STREET_NAME, storageStreetName);
        }

        // Check for ALL_FIELDS numeric in the street name. If so, add its
        // proper OT. NOTE: this is only valid if there is no
        // prefix type (this is to prevent "123 st hwy 4" from being
        // butchered), or if a UTAH address is present (PD and SD both
        // exists), or if no extension is present (EX must be blank: 123 old 5).
        if (streetNamePrefixType == null &&
            streetNameExtensionIndex == null &&
            streetNamePrefixDirection == null ||
            streetNameSuffixDirection == null)
        {
            if (originalStreetName != null && isAllDigits(originalStreetName)) {
            	address.set(ORIGINAL_STREET_NAME, originalStreetName + matchNumberWithOrdinalType(originalStreetName));
            }

            if (matchingStreetName != null && isAllDigits(matchingStreetName)) {
            	matchingStreetName += matchNumberWithOrdinalType(matchingStreetName);
                address.set(MATCHING_STREET_NAME, matchingStreetName);
            }

            if (storageStreetName != null && isAllDigits(storageStreetName)) {
            	address.set(STORAGE_STREET_NAME, storageStreetName + matchNumberWithOrdinalType(storageStreetName));
            }
        }
    }
    
    /*
     * Checks strings for "spelled out" ordinal types, eg. "fifty second" is
     * changed to its numeric ordinal type: 52nd.
     */
    // FIXME This method is ineffective for languages other than English
    private boolean isOrdinalTypeValue(final String inputStreetName) {
        final String[][] tens;
        final String[][] units;

            tens = new String[][] {
                {"TWENTY", "2"},
                {"THIRTY", "3"},
                {"FORTY", "4"},
                {"FIFTY", "5"},
                {"SIXTY", "6"},
                {"SEVENTY", "7"},
                {"EIGHTY", "8"},
                {"NINETY", "9"}
            };
            units = new String[][] {
                {"SECOND", "2nd"},
                {"THIRD", "3rd"},
                {"FOURTH", "4th"},
                {"FIFTH", "5th"},
                {"SIXTH", "6th"},
                {"SEVENTH", "7th"},
                {"EIGHTH", "8th"},
                {"NINTH", "9th"}
            };

        // Change FIFTY SECOND to 52nd, THIRTY FIRST to 31st, etc.
        for (int i = 0; i < tens.length; i++) {
            if (inputStreetName.startsWith(tens[i][0])) {
                if (inputStreetName.length() <= tens[i][0].length()) {
                    continue;
                }

                int pos = tens[i][0].length();
                if (inputStreetName.charAt(pos - 1) == '-') { // French
                    pos++;
                }
                String mUnits = inputStreetName.substring(pos);
                for (int j = 0; j < units.length; j++) {
                    if (mUnits.startsWith(units[j][0])) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    /**
     * Receives a number (char) and match that number with its valid Ordinal
     * Type and return it, UNLESS it's not a match and the OT is ST or RD
     * (example: "65st" wont be corrected). EX: 62 is matched properly with
     * "ND".
     * 
     *            aaa
     *            aaa
     */
    private static String matchNumberWithOrdinalType(final String num) {
        final StringBuilder ord = new StringBuilder();

        char lastDigit;
        char sec2LastDigit;
        String th = "th";
        String nd = "nd";
        String st = "st";
        String rd = "rd";

        lastDigit = num.charAt(num.length() - 1);

        sec2LastDigit = num.trim().length() > 1 ? num.charAt(num.length() - 2) : ' ';

        /*
         * Valid combos: ST ND RD TH -- -- -- -- 1 2 3 4-20 21 22 23 24-30 31 32
         * 33 34-40 . . . . . . . . 101 102 103 104-120 121 122 123 124-130 . . . . . . . .
         */
        if (lastDigit == '1' && sec2LastDigit != '1') {

            copy(ord, st);

        } else if (lastDigit == '2' && sec2LastDigit != '1') {

            copy(ord, nd);

        } else if (lastDigit == '3' && sec2LastDigit != '1') {

            copy(ord, rd);

        } else {

            copy(ord, th);
        }
        
        return ord.toString();
    }
}
