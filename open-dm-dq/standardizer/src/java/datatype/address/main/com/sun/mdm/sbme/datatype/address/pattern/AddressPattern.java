/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common
 * Development and Distribution License ("CDDL")(the "License"). You
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html
 * or mural/license.txt. See the License for the specific language
 * governing permissions and limitations under the License. *
 * When distributing Covered Code, include this CDDL Header Notice
 * in each file and include the License file at mural/license.txt.
 * If applicable, add the following below the CDDL Header, with the
 * fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */

package com.sun.mdm.sbme.datatype.address.pattern;

import java.util.List;

import com.sun.inti.components.util.Abbreviated;
import com.sun.mdm.sbme.pattern.AbstractPattern;

/**
 * 
 */
public class AddressPattern extends AbstractPattern<InputTokenType, OutputTokenType> {
	private static final String SEPARATOR = ":";

	private int priority;
	
	private AddressPatternClass addressPatternClass;

	private AddressScoringOption addressScoringOption;
	
	public AddressPattern(final List<InputTokenType> inputTokenTypes,
				   		  final List<OutputTokenType> outputTokenTypes,
				   		  int priority,
				   		  AddressPatternClass addressPatternClass,
				   		  AddressScoringOption addressScoringOption)
	{
		super(inputTokenTypes, outputTokenTypes);
		this.set(priority, addressPatternClass, addressScoringOption);
	}

	public AddressPattern(AddressPattern other) {
		super(other.getInputTokenTypes(), other.getOutputTokenTypes());
		this.set(other.priority, other.addressPatternClass, other.addressScoringOption);
	}

	public AddressPattern(AddressPattern other, List<InputTokenType> inputTokenTypes, List<OutputTokenType> outputTokenTypes) {
		super(inputTokenTypes, outputTokenTypes);
		this.set(other.priority, other.addressPatternClass, other.addressScoringOption);
	}

	private void set(int priority,
						  AddressPatternClass addressPatternClass,
                          AddressScoringOption addressScoringOption)
	{
		this.priority = priority;
        this.addressScoringOption = addressScoringOption;
		this.addressPatternClass = addressPatternClass;
	}

	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append(buildString(this.getInputTokenTypes()));
		builder.append(SEPARATOR);
		builder.append(buildString(this.getOutputTokenTypes()));
		return builder.toString();
	}

	public static StringBuilder buildString(List<? extends Abbreviated> list) {
		StringBuilder builder = new StringBuilder();
		if (list.size() > 0) {
			for (Abbreviated o : list) {
				builder.append(o.getAbbreviation());
				builder.append(' ');
			}
			builder.setLength(builder.length() - 1);
		}
		return builder;
	}

	public int getPriority() {
		return this.priority;
	}

    public void setPriority(final int priority) {
        this.priority = priority;
    }

	public AddressScoringOption getScoringOption() {
		return this.addressScoringOption;
	}

	public void setScoringOption(AddressScoringOption addressScoringOption) {
		this.addressScoringOption = addressScoringOption;
	}

    public AddressPatternClass getPatternClass() {
        return addressPatternClass;
    }

    public void setPatternClass(AddressPatternClass addressPatternClass) {
        this.addressPatternClass = addressPatternClass;
    }
}
