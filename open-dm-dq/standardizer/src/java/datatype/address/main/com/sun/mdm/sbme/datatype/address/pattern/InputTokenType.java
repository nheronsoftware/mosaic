/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.address.pattern;

import com.sun.inti.components.util.Abbreviated;


/**
 *
 */
public enum InputTokenType implements Abbreviated {
    ALPHA_NUM("AN"),
    ALPHA_ONE("A1"),
    ALPHA_TWO("A2"),
    AMPERSAND("AM"),
    BUILDING_PROPERTY("BP"),
    BUILDING_UNIT("BU"),
    COMMON_WORD("NL"),
    DIGIT("D1"),
    EXTENSION("EX"),
    EXTRA_INFORMATION("EI"),
    GENERIC_WORD("AU"),
    HIGHWAY_ROUTE("HR"),
    LEADING_DASH("DA"),
    MILE_POST("MP"),
    NUMERIC_FRACTION("FC"),
    NUMERIC_VALUE("NU"),
    ORDINAL_TYPE("OT"),
    POST_OFFICE_BOX("BX"),
    PREFIX_TYPE("PT"),
    RURAL_ROUTE("RR"),
    STATE_ABBREVIATION("SA"),
    STREET_DIRECTION("DR"),
    STREET_TYPE("TY"),
    STRUCTURE_DESCRIPTOR("WD"),
    STRUCTURE_IDENTIFIER("WI"),
    
    // FIXME
    UNSPECIFIED(""),
    
    OTHER_1P("1P"),
    OTHER_A3("A3"),
    OTHER_BI("BI"),
    OTHER_BN("BN"),
    OTHER_BS("BS"),
    OTHER_BT("BT"),
    OTHER_CN("CN"),
    OTHER_DB("DB"),
    OTHER_DM("DM"),
    OTHER_EN("EN"),
    OTHER_HN("HN"),
    OTHER_HS("HS"),
    OTHER_NA("NA"),
    OTHER_NB("NB"),
    OTHER_PD("PD"),
    OTHER_SD("SD"),
    OTHER_ST("ST"),
    OTHER_TB("TB"),
    OTHER_XN("XN"),
    
    // FIXME These values actually correspond to pattern classes, not pattern tokens.
    BUILDING_CLASS("B*"),
    HOUSE_CLASS("H*"),
    MOSTLY_NUMERIC_CLASS("N*"),
    POST_OFFICE_BOX_CLASS("P*"),
    RURAL_ROUTE_CLASS("R*"),
    STREET_TYPE_CLASS("T*"),
    UNIT_WITHIN_STRUCTURE_CLASS("W*")
    ;

    private final String abbreviation;
    
    // FIXME This is a shameless hack
    private boolean allowsAveraging = false;
    private boolean disallowsAveraging = false;

    InputTokenType(final String abbreviation) {
        this.abbreviation = abbreviation;
        if (this.abbreviation.length() > 0) {
            this.allowsAveraging = this.abbreviation.charAt(1) == '*'; 
            this.disallowsAveraging = this.abbreviation.charAt(1) == '+'; 
        }
    }
    
    public String getAbbreviation() {
    	return this.abbreviation;
    }

    public boolean allowsAveraging() {
        return allowsAveraging;
    }

    public boolean disallowsAveraging() {
        return disallowsAveraging;
    }
}
