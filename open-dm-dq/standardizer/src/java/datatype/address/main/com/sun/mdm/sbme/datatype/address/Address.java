/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.address;

import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.EXTRA_INFORMATION;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.HOUSE_NUMBER;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.HOUSE_NUMBER_PREFIX;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.HOUSE_NUMBER_SUFFIX;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.POST_OFFICE_BOX_DESCRIPTOR;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.POST_OFFICE_BOX_IDENTIFIER;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.PROPERTY_PREFIX_DIRECTION;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.PROPERTY_SUFFIX_DIRECTION;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.PROPERTY_TYPE_PREFIX;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.PROPERTY_TYPE_SUFFIX;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.RURAL_ROUTE_DESCRIPTOR;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.RURAL_ROUTE_IDENTIFIER;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.SECOND_HOUSE_NUMBER;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.SECOND_HOUSE_NUMBER_PREFIX;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.STREET_NAME_EXTENSION_INDEX;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.STREET_NAME_PREFIX_DIRECTION;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.STREET_NAME_PREFIX_TYPE;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.STREET_NAME_SUFFIX_DIRECTION;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.STREET_NAME_SUFFIX_TYPE;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.STRUCTURE_DESCRIPTOR;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.STRUCTURE_IDENTIFIER;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.WITHIN_STRUCTURE_DESCRIPTOR;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.WITHIN_STRUCTURE_IDENTIFIER;

import com.stc.sbme.api.SbmeStandRecord;
import com.stc.sbme.api.SbmeStandRecordFactory;
import com.sun.mdm.sbme.StandardizedRecord;
import com.sun.mdm.sbme.builder.OutputPatternAware;
import com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType;
import com.sun.mdm.sbme.pattern.OutputPattern;

/**
 *
 */
public class Address extends StandardizedRecord /* FIXME */ implements OutputPatternAware {
    public static final String MATCHING_PROPERTY_NAME = "MatchPropertyName";
    public static final String MATCHING_STREET_NAME = "MatchStreetName";
    public static final String ORIGINAL_PROPERTY_NAME = "OrigPropertyName";
    public static final String ORIGINAL_SECOND_STREET_NAME = "OrigSecondStreetName";
    public static final String ORIGINAL_STREET_NAME = "OrigStreetName";
    public static final String STORAGE_STREET_NAME = "StorStreetName"; // FIXME This field is ignored!!
    public static final String SECOND_STREET_NAME_SUFFIX_TYPE = "SecondStreetNameSufType";
    
    public String get(OutputTokenType outputTokenType) {
    	return this.get(outputTokenType.getAlias());
    }
    
    public String get(OutputTokenType outputTokenType, String defaultValue) {
    	return this.get(outputTokenType.getAlias(), defaultValue);
    }
    
    public void set(OutputTokenType outputTokenType, String propertyValue) {
    	this.set(outputTokenType.getAlias(), propertyValue);
    }

	public String getType() { return "Address"; }
    private String signature;
	public String getSignature() {
    	if (signature == null) {
    		StringBuilder builder = new StringBuilder();
    		OutputPattern<?, ?>[] outputPatterns = this.getOutputPatterns();
    		for (int i= 0; i < outputPatterns.length; i++) {
    			if (i > 0) {
    				builder.append('|');
    			}
    			builder.append(outputPatterns[i]);
    		}
    		this.signature = builder.toString();
    	}
    	return this.signature;
    }
    
    // FIXME Remove after testing against legacy version
    private OutputPattern<?, ?>[] outputPatterns;
    public OutputPattern<?, ?>[] getOutputPatterns() {
        return this.outputPatterns;
    }
    public void setOutputPatterns(final OutputPattern<?, ?>[] outputPatterns) {
        this.outputPatterns = outputPatterns;
    }

    private static final String[] mdmProperties = new String[] {
        HOUSE_NUMBER.getAlias(),
        HOUSE_NUMBER_PREFIX.getAlias(),
        HOUSE_NUMBER_SUFFIX.getAlias(),
        POST_OFFICE_BOX_DESCRIPTOR.getAlias(),
        POST_OFFICE_BOX_IDENTIFIER.getAlias(),
        PROPERTY_PREFIX_DIRECTION.getAlias(),
        PROPERTY_SUFFIX_DIRECTION.getAlias(),
        PROPERTY_TYPE_PREFIX.getAlias(),
        PROPERTY_TYPE_SUFFIX.getAlias(),
        RURAL_ROUTE_DESCRIPTOR.getAlias(),
        RURAL_ROUTE_IDENTIFIER.getAlias(),
        SECOND_HOUSE_NUMBER.getAlias(),
        SECOND_HOUSE_NUMBER_PREFIX.getAlias(),
        SECOND_STREET_NAME_SUFFIX_TYPE,
        STREET_NAME_EXTENSION_INDEX.getAlias(),
        STREET_NAME_PREFIX_DIRECTION.getAlias(),
        STREET_NAME_PREFIX_TYPE.getAlias(),
        STREET_NAME_SUFFIX_DIRECTION.getAlias(),
        STREET_NAME_SUFFIX_TYPE.getAlias(),
        STRUCTURE_DESCRIPTOR.getAlias(),
        STRUCTURE_IDENTIFIER.getAlias(),
        WITHIN_STRUCTURE_DESCRIPTOR.getAlias(),
        WITHIN_STRUCTURE_IDENTIFIER.getAlias(),

        MATCHING_PROPERTY_NAME,
        MATCHING_STREET_NAME,
        ORIGINAL_PROPERTY_NAME,
        ORIGINAL_SECOND_STREET_NAME,
        ORIGINAL_STREET_NAME,
        
        // FIXME This field should be included!
        // STORAGE_STREET_NAME
    };
	public SbmeStandRecord toSbmeStandRecord() {
        boolean propertySet = false;
		SbmeStandRecord record = SbmeStandRecordFactory.getInstance("Address");
        for (String propertyName: mdmProperties) {
            String propertyValue = this.get(propertyName);
            if (propertyValue != null && propertyValue.length() > 0) {
                propertySet = true;
                record.setValue(propertyName, propertyValue);
            }
        }

        String propertyValue = this.get(EXTRA_INFORMATION.getAlias());
        if (propertyValue != null && propertyValue.length() > 0) {
            if (propertySet) {
            	record.setValue(EXTRA_INFORMATION.getAlias(), propertyValue);
            } else {
            	record.setValue(ORIGINAL_PROPERTY_NAME, propertyValue.toString());
            	record.setValue(MATCHING_PROPERTY_NAME, propertyValue.toString());
            }
        }
		
		return record;
	}
}
