package com.sun.mdm.sbme.datatype.address.builder.property;

import static com.sun.mdm.sbme.datatype.address.Address.MATCHING_STREET_NAME;
import static com.sun.mdm.sbme.datatype.address.Address.ORIGINAL_STREET_NAME;
import static com.sun.mdm.sbme.datatype.address.Address.STORAGE_STREET_NAME;
import static com.sun.mdm.sbme.datatype.address.builder.AbbreviationOption.EXPANDED;
import static com.sun.mdm.sbme.datatype.address.builder.AbbreviationOption.USPS_ABBREVIATION;
import static com.sun.mdm.sbme.datatype.address.builder.CombinationOption.SPACE;
import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.COMMON_WORD;
import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.STREET_TYPE;

import com.sun.inti.components.record.Record;
import com.sun.mdm.sbme.MdmStandardizationException;
import com.sun.mdm.sbme.clue.Clue;
import com.sun.mdm.sbme.datatype.address.Address;
import com.sun.mdm.sbme.datatype.address.builder.AbbreviationOption;
import com.sun.mdm.sbme.datatype.address.pattern.InputTokenType;
import com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType;
import com.sun.mdm.sbme.normalizer.NormalizedToken;

public class StreetNameAddressPropertyBuilder extends AddressPropertyBuilder {
    protected void buildProperty(Address address, NormalizedToken<InputTokenType> normalizedToken, InputTokenType inputTokenType, OutputTokenType outputTokenType, Record context) {
        final int clueWordId = normalizedToken.getClueWordId(inputTokenType);
        this.loadField(address, ORIGINAL_STREET_NAME, normalizedToken.getImage(), 0, inputTokenType, getLocale().getAbbreviationOption(), SPACE);

        /*
         * If the abbreviation option is 'E' (use expanded matching
         * names), then loadStorageField will copy the storage name
         * to the matching name: eg, we want "fifth" ave to have a
         * matching name of "5th"
         */
        if (!this.loadStorageField(address, clueWordId, normalizedToken, getLocale().getAbbreviationOption())) {
            this.loadField(address, STORAGE_STREET_NAME, normalizedToken.getImage(), 0, inputTokenType, getLocale().getAbbreviationOption(), SPACE);
        }

        // Make sure "st barnabas" has a matching name of "saint
        // barnabas" for type 'E'
        if (inputTokenType == STREET_TYPE) {
            // FIXME The following checks are specific to the English language and to the US locale!
            if ("ST".equalsIgnoreCase(normalizedToken.getImage())) {
                this.loadField(address, MATCHING_STREET_NAME, "Saint", clueWordId, inputTokenType, USPS_ABBREVIATION, SPACE);
            } else if ("DR".equalsIgnoreCase(normalizedToken.getImage())) {
                this.loadField(address, MATCHING_STREET_NAME, "Dr", clueWordId, inputTokenType, USPS_ABBREVIATION, SPACE);
            } else {
                this.loadField(address, MATCHING_STREET_NAME, normalizedToken.getImage(), clueWordId, inputTokenType, USPS_ABBREVIATION, SPACE);
            }
        } else {
            // If the abbreviation option is 'E', then all matching
            // names now need the FULL names:
            // springhill --> springhill
            // Spring hill --> Spr Hl
            // Before, these two names wouldn't match
            // springhill --> springhill
            // Spring hill --> Spring Hill
            // Now they can.
            if (inputTokenType != COMMON_WORD/* || context.getBoolean("hasStreetName")*/) {
                this.loadField(address, MATCHING_STREET_NAME, normalizedToken.getImage(), clueWordId, inputTokenType, USPS_ABBREVIATION, SPACE);
            }
        }
        
//        context.set("hasStreetName", true);
    }

    /*
     * Used only when loading the Storage name field. It searches the clue table
     * for the image and checks the storage flag: If it is '*', then the name is
     * loaded from the standard abbreviation field, and the function returns
     * "true", otherwise "false".
     */
    private boolean loadStorageField(final Address address,
                                     final int clueWordId,
                                     final NormalizedToken<InputTokenType> normalizedToken,
                                     final AbbreviationOption abbreviationOption)
        throws MdmStandardizationException
    {
    	StringBuilder field = new StringBuilder();
    	
    	try {
            // If the clue word id for the token is 0, then a separate
            // storage name can't exist, hence, return FALSE.
            if (clueWordId == 0) {
                return false;
            }

            final Clue<InputTokenType> clue = getClueRegistry().lookup(normalizedToken.getImage());
            if (clue == null) {
                logger.warning("ERROR:  clue word entry wasn't found for input image: " + normalizedToken.getImage());
                throw new MdmStandardizationException("ERROR:  clue word entry wasnt found for input image: " + normalizedToken.getImage());
            }

            if (clue.isTranslationExpanded()) {
                field.append(' ');
                field.append(clue.getTranslation());

                if (abbreviationOption == EXPANDED) {
                    normalizedToken.setImage(clue.getTranslation());
                }
                return true;
            }
            return false;
    	} finally {
    		address.set(STORAGE_STREET_NAME, field.toString());
    	}
    }
}
