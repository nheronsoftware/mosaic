/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.address.locale;

import com.sun.inti.components.string.match.CharSequencePositionMatcher;

/**
 *
 */
public class QuoteAbbreviationMatcher implements CharSequencePositionMatcher {
    private static final char[] apostrophes = new char[] { 39, // '\u0027': '
            '\u0060', // `
            '\u2018', // �
            '\u2019', // �
    };

    public boolean matches(final CharSequence sequence, final int index) {
        if (index == 0 || index == sequence.length() - 1) {
            return false;
        }
        for (final char apostrophe : apostrophes) {
            if (sequence.charAt(index) == apostrophe) {
                return Character.isLetter(sequence.charAt(index - 1)) && Character.isLetter(sequence.charAt(index + 1));
            }
        }
        return false;
    }
}
