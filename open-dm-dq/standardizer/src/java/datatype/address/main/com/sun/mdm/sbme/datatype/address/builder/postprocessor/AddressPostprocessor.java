package com.sun.mdm.sbme.datatype.address.builder.postprocessor;

import com.sun.inti.components.record.Record;
import com.sun.mdm.sbme.StandardizedRecord;
import com.sun.mdm.sbme.builder.postprocessor.StandardizedRecordBuilderPostprocessor;
import com.sun.mdm.sbme.datatype.address.Address;
import com.sun.mdm.sbme.datatype.address.builder.AddressBuildingComponent;
import com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType;

public abstract class AddressPostprocessor extends AddressBuildingComponent implements StandardizedRecordBuilderPostprocessor<OutputTokenType> {
	public void postprocess(StandardizedRecord standardizedRecord, Record context) {
    	this.postprocess((Address) standardizedRecord, context);
    }
    
    protected abstract void postprocess(Address address, Record context);

    protected static boolean isAllDigits(final StringBuilder str) {
        return isAllDigits(str.toString());
    }

    protected static boolean isAllDigits(final String str) {
        final int len = str.length();
        for (int i = 0; i < len; i++) {
            if (!Character.isDigit(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    /**
     * Copy the string finS into the original string intSb
     * 
     *            the original string
     *            the replacement string
     */
    protected static StringBuilder copy(final StringBuilder intSb, final String finS) {
        intSb.setLength(0);
        return intSb.append(finS);
    }
    /**
     * Copy the string finSb into the original string intSb
     * 
     *            the original string
     *            the replacement string
     *             an exception
     */
    protected static StringBuilder copy(final StringBuilder intSb, final StringBuilder finSb) {
        intSb.setLength(0);
        return intSb.append(finSb);

    }
}
