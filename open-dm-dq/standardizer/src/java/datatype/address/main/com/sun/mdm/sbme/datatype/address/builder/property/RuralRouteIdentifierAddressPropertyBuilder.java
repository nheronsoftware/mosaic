package com.sun.mdm.sbme.datatype.address.builder.property;

import static com.sun.mdm.sbme.datatype.address.builder.CombinationOption.DASH;
import static com.sun.mdm.sbme.datatype.address.builder.CombinationOption.SPACE;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.EXTRA_INFORMATION;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.RURAL_ROUTE_IDENTIFIER;

import com.sun.inti.components.record.Record;
import com.sun.mdm.sbme.datatype.address.Address;
import com.sun.mdm.sbme.datatype.address.pattern.InputTokenType;
import com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType;
import com.sun.mdm.sbme.normalizer.NormalizedToken;

public class RuralRouteIdentifierAddressPropertyBuilder extends AddressPropertyBuilder {
    protected void buildProperty(Address address, NormalizedToken<InputTokenType> normalizedToken, InputTokenType inputTokenType, OutputTokenType outputTokenType, Record context) {
        this.checkForSpelledOutNumber(normalizedToken);
        final String[] fractionParts = fractionWithinNumber(normalizedToken);

        final int clueWordId = normalizedToken.getClueWordId(inputTokenType);

        if (fractionParts == null) {
            this.loadField(address, RURAL_ROUTE_IDENTIFIER, normalizedToken.getImage(), clueWordId, inputTokenType, getLocale().getAbbreviationOption(), DASH);
        } else {
            this.loadField(address, EXTRA_INFORMATION, fractionParts[1], 0, inputTokenType, getLocale().getAbbreviationOption(), SPACE);
            this.loadField(address, RURAL_ROUTE_IDENTIFIER, fractionParts[0], clueWordId, inputTokenType, getLocale().getAbbreviationOption(), DASH);
        }
    }
}
