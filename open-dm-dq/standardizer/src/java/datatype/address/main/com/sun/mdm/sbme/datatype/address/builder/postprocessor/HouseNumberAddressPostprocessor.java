package com.sun.mdm.sbme.datatype.address.builder.postprocessor;

import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.HOUSE_NUMBER;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.SECOND_HOUSE_NUMBER;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.SECOND_HOUSE_NUMBER_PREFIX;

import java.util.regex.Pattern;

import com.sun.inti.components.record.Record;
import com.sun.mdm.sbme.datatype.address.Address;

public class HouseNumberAddressPostprocessor extends AddressPostprocessor {
    private static final Pattern leadingZerosPattern = Pattern.compile("^0+");

	/*
     * Checks the PrimaryHouseNumber field to see if multiple numbers have been
     * added; if so, it splits them up into PrimaryHouseNumber and
     * SecondaryHouseNumber. EX: 101 120 main st --> "101" & "120". Also, if H1
     * and H2 are present and P2 is blank, then add '-' to P2. This will handle
     * the QUEENS style addresses. Also, check for H1 = "0".
     */
    protected void postprocess(Address address, Record context) {
    	int blankPosition = -1;
    	String houseNumber = address.get(HOUSE_NUMBER);
    	if (houseNumber != null) {
    		blankPosition = houseNumber.indexOf(' ');
    	}

        if (blankPosition != -1) {
            // If you found a second house number , then return it after
            // first eliminating it from the original number
        	address.set(SECOND_HOUSE_NUMBER, houseNumber.substring(blankPosition + 1));
        }
        
        if (houseNumber != null) {
        	String secondHouseNumber = address.get(SECOND_HOUSE_NUMBER);
        	String secondHouseNumberPrefix = address.get(SECOND_HOUSE_NUMBER_PREFIX);
            if (secondHouseNumber != null && secondHouseNumberPrefix == null) {
            	address.set(SECOND_HOUSE_NUMBER_PREFIX, "-");
            }

            if (houseNumber.length() > 1 && houseNumber.charAt(0) == '0') {
                address.set(HOUSE_NUMBER, leadingZerosPattern.matcher(houseNumber).replaceFirst(""));
            }
        }
    }
}
