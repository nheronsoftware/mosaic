package com.sun.mdm.sbme.datatype.address.builder;

import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.BUILDING_UNIT;
import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.STREET_TYPE;

import com.sun.mdm.sbme.datatype.address.pattern.InputTokenType;

public class EnglishInputTokenTypeMapper implements InputTokenTypeMapper {

	public InputTokenType map(InputTokenType inputTokenType) {
		
		switch (inputTokenType) {
			case PREFIX_TYPE:
				return STREET_TYPE;
			case BUILDING_PROPERTY:
				return BUILDING_UNIT;
			case HIGHWAY_ROUTE:
				return STREET_TYPE;
			default:
				return inputTokenType;
		}
	}
}
