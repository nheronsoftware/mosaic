package com.sun.mdm.sbme.datatype.address.builder.property;

import static com.sun.mdm.sbme.datatype.address.Address.MATCHING_PROPERTY_NAME;
import static com.sun.mdm.sbme.datatype.address.Address.ORIGINAL_PROPERTY_NAME;
import static com.sun.mdm.sbme.datatype.address.builder.AbbreviationOption.STANDARD_ABBREVIATION;
import static com.sun.mdm.sbme.datatype.address.builder.CombinationOption.SPACE;
import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.COMMON_WORD;
import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.STREET_TYPE;

import com.sun.inti.components.record.Record;
import com.sun.mdm.sbme.datatype.address.Address;
import com.sun.mdm.sbme.datatype.address.pattern.InputTokenType;
import com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType;
import com.sun.mdm.sbme.normalizer.NormalizedToken;

public class PropertyBuildingNameAddressPropertyBuilder extends AddressPropertyBuilder {
    protected void buildProperty(Address address, NormalizedToken<InputTokenType> normalizedToken, InputTokenType inputTokenType, OutputTokenType outputTokenType, Record context) {
        final int clueWordId = normalizedToken.getClueWordId(inputTokenType);
        this.loadField(address, ORIGINAL_PROPERTY_NAME, normalizedToken.getImage(), 0, inputTokenType, getLocale().getAbbreviationOption(), SPACE);

        if (inputTokenType == STREET_TYPE) {
            this.loadField(address, MATCHING_PROPERTY_NAME, normalizedToken.getImage(), 0, inputTokenType, getLocale().getAbbreviationOption(), SPACE);
        } else {
            if (inputTokenType != COMMON_WORD) {
                this.loadField(address, MATCHING_PROPERTY_NAME, normalizedToken.getImage(), clueWordId, inputTokenType, STANDARD_ABBREVIATION, SPACE);
            }
        }
    }
}
