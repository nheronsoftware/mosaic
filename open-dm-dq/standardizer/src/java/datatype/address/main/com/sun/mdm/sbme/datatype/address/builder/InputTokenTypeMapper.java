package com.sun.mdm.sbme.datatype.address.builder;

import com.sun.mdm.sbme.datatype.address.pattern.InputTokenType;

public interface InputTokenTypeMapper {
	
	public InputTokenType map(InputTokenType inputTokenType);

}
