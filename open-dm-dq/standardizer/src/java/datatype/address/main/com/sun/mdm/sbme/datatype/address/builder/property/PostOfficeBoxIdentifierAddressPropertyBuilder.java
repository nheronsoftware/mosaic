package com.sun.mdm.sbme.datatype.address.builder.property;

import static com.sun.mdm.sbme.datatype.address.builder.CombinationOption.DASH;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.POST_OFFICE_BOX_IDENTIFIER;

import com.sun.inti.components.record.Record;
import com.sun.mdm.sbme.datatype.address.Address;
import com.sun.mdm.sbme.datatype.address.pattern.InputTokenType;
import com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType;
import com.sun.mdm.sbme.normalizer.NormalizedToken;

public class PostOfficeBoxIdentifierAddressPropertyBuilder extends AddressPropertyBuilder {
    protected void buildProperty(Address address, NormalizedToken<InputTokenType> normalizedToken, InputTokenType inputTokenType, OutputTokenType outputTokenType, Record context) {
        final int clueWordId = normalizedToken.getClueWordId(inputTokenType);
        
        this.checkForSpelledOutNumber(normalizedToken);
        this.loadField(address, POST_OFFICE_BOX_IDENTIFIER, normalizedToken.getImage(), clueWordId, inputTokenType, getLocale().getAbbreviationOption(), DASH);
    }
}
