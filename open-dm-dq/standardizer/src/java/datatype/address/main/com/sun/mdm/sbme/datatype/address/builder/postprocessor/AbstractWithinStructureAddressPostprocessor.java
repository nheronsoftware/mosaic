package com.sun.mdm.sbme.datatype.address.builder.postprocessor;


public abstract class AbstractWithinStructureAddressPostprocessor extends AddressPostprocessor {
	protected static String searchStandIden(final StringBuilder inputIdentifier) {
	    final String FLOOR;
	    final String LEVEL;
	    final String NUMBERED_FLOOR;
	    final String NUMBERED_LEVEL;
	    final String THIRD_SUFFIX;
	    final String SECOND_SUFFIX;
	    final String ORDINAL_SUFFIX;
	    final String FIRST_SUFFIX;
	    final String SIDE;
	    final String BASEMENT;
	    final String INTERIOR;
	    final String REAR;
	    final String BOTTOM;
	
	    final String MILIEU;
	    final String LOWER;
	    final String UPPER;
	    final String FRONT;
	    final String DOWN;
	
	    final String LEFT;
	    final String RIGHT;
	    final String LOBBY;
	    final String OFFICE;
	    final String TOP;
	    final String PENTHOUSE;
	    final String UP;
	
	    FLOOR = "Etg";
	    NUMBERED_FLOOR = "Etg-";
	    LEVEL = "Niv";
	    NUMBERED_LEVEL = "Niv-";
	    SECOND_SUFFIX = "EME";
	    THIRD_SUFFIX = "EME";
	    FIRST_SUFFIX = "ER";
	    ORDINAL_SUFFIX = "ERE";
	    SIDE = "C�t";
	    BASEMENT = "SSOL";
	    INTERIOR = "Inte";
	    REAR = "ANNX";
	    BOTTOM = "BASSE";
	    MILIEU = "Milieu";
	    LOWER = "BAS";
	    UPPER = "HTE";
	    FRONT = "Face";
	    DOWN = "BASSE";
	    LEFT = "Gauche";
	    RIGHT = "DRTE";
	    LOBBY = "Lbby";
	    OFFICE = "Bure";
	    TOP = "Ht";
	    PENTHOUSE = "TERR";
	    UP = "HTS";
	    
	    // FIXME What does "*" mean in a within structure identifier?
	    /* Make sure that the '*' is at least at index 2. SUBJECT TO REVIEW */
	    int strIdx = inputIdentifier.toString().indexOf('*');
	    if (strIdx < 2) {
	        return inputIdentifier.toString();
	    }
	
	    // Determine length of input string and the first position of an
	    // ASTERISK
	    // .. 23nd*Fl*5 --> *Fl*5 ..
	    StringBuilder star = strchr(inputIdentifier, '*');
	    if (star == null) {
	        // Single character sent
	        return inputIdentifier.toString();
	    }
	
	    int lenStr;
	    int lenStar;
	    int lenFirst;
	    int len2star;
	    int midlen;
	    StringBuilder star2;
	    final StringBuilder temp = new StringBuilder();
	    boolean floor = inputIdentifier.toString().startsWith(FLOOR);
	    boolean level = inputIdentifier.toString().startsWith(LEVEL);
	    if (floor || level) {
	        star2 = strchr(star, 1, '*');
	        if (star2 == null) {
	            star2 = strchr(star, '*');
	            len2star = star2.length();
	            copyLength(temp, star2, 1, len2star - 1);
	            temp.setCharAt(len2star - 1, '*');
	
	            if (floor) {
	                copy(temp, len2star, FLOOR);
	            }
	            if (level) {
	                copy(temp, len2star, LEVEL);
	            }
	            copy(inputIdentifier, temp);
	        } else {
	            star2 = strchr(star, 1, '*');
	            if (star2 != null) {
	                len2star = star2.length();
	                lenStar = star.length();
	                lenStr = inputIdentifier.length();
	                midlen = lenStar - len2star;
	                lenFirst = lenStr - lenStar + 1;
	
	                copyLength(temp, star, 1, midlen);
	                copyLength(temp, midlen, inputIdentifier, lenFirst);
	                copyLength(temp, midlen + lenFirst, star2, 1, 0);
	                copy(inputIdentifier, temp);
	            }
	        }
	    }
	
	    int digit;
	    int end;
	    int lenFl;
	    // Flips the input: 3*Fl4th ----->> to 4th*Fl*3
	    floor = star.substring(1).startsWith(FLOOR);
	    level = star.substring(1).startsWith(LEVEL);
	    star2 = strchr(star, 1, '*');
	    if (compare(SECOND_SUFFIX, inputIdentifier, strIdx - 2, 2) != 0 &&
	        compare(THIRD_SUFFIX, inputIdentifier, strIdx - 2, 2) != 0 &&
	        compare(ORDINAL_SUFFIX, inputIdentifier, strIdx - 2, 2) != 0 &&
	        compare(FIRST_SUFFIX, inputIdentifier, strIdx - 2, 2) != 0)
	    {
	        if (floor) {
	            if ((digit = Character.digit(star.charAt(4), 10)) != 0) {
	                lenStar = star.length();
	                lenStr = inputIdentifier.length();
	                lenFirst = lenStr - lenStar;
	                lenFl = lenStr - lenFirst - 4;
	
	                copyLength(temp, star, 4, lenFl);
	                temp.setCharAt(lenFl, '*');
	                copyLength(temp, lenFl + 1, star, 1, 2);
	                end = lenFl + 3;
	                temp.setCharAt(end, '*');
	                copyLength(temp, end + 1, inputIdentifier, lenFirst);
	                copy(inputIdentifier, temp);
	            }
	        } else if (level) {
	            if ((digit = Character.digit(star.charAt(5), 10)) != 0) {
	                lenStar = star.length();
	                lenStr = inputIdentifier.length();
	                lenFirst = lenStr - lenStar;
	                lenFl = lenStr - lenFirst - 5;
	
	                copyLength(temp, star, 5, lenFl);
	                temp.setCharAt(lenFl, '*');
	                copyLength(temp, lenFl + 1, star, 1, 2);
	                end = lenFl + 3;
	                temp.setCharAt(end, '*');
	                copyLength(temp, end + 1, inputIdentifier, lenFirst);
	                copy(inputIdentifier, temp);
	            }
	        }
	    }
	
	    // 3nd*Fl*5 --> *Fl*5
	    star = strchr(inputIdentifier, '*');
	    lenStr = inputIdentifier.length();
	    // Eg: Length of *Fl*5 .
	    lenStar = star.length();
	    lenFirst = lenStr - lenStar;
	
	    final StringBuilder outputIdentifier = new StringBuilder();
	
	    // If "Side" is contained in input string, handle separately
	    if (compare(SECOND_SUFFIX, inputIdentifier, strIdx - 2, 2) != 0 &&
	        compare(THIRD_SUFFIX, inputIdentifier, strIdx - 2, 2) != 0 &&
	        compare(ORDINAL_SUFFIX, inputIdentifier, strIdx - 2, 2) != 0 &&
	        compare(FIRST_SUFFIX, inputIdentifier, strIdx - 2, 2) != 0)
	    {
	        // FIXME These lengths don't apply to the French case!!
	        if ((digit = Character.digit(star.charAt(1), 10)) != 0
	                && (compare(SIDE, inputIdentifier.toString(), 4) == 0 ||
	                    compare(BASEMENT, inputIdentifier.toString(), 4) == 0 ||
	                    compare(INTERIOR, inputIdentifier.toString(), 4) == 0 ||
	                    compare(REAR, inputIdentifier.toString(), 4) == 0 ||
	                    compare(BOTTOM, inputIdentifier.toString(), 4) == 0 ||
	                    compare(MILIEU, inputIdentifier.toString(), 4) == 0 ||
	                    compare(LOWER, inputIdentifier.toString(), 4) == 0 ||
	                    compare(UPPER, inputIdentifier.toString(), 4) == 0 ||
	                    compare(FRONT, inputIdentifier.toString(), 4) == 0 ||
	                    compare(DOWN, inputIdentifier.toString(), 4) == 0 ||
	                    compare(LEFT, inputIdentifier.toString(), 4) == 0 ||
	                    compare(RIGHT, inputIdentifier.toString(), 4) == 0 ||
	                    compare(LOBBY, inputIdentifier.toString(), 4) == 0 ||
	                    compare(OFFICE, inputIdentifier.toString(), 3) == 0 ||
	                    compare(TOP, inputIdentifier.toString(), 3) == 0 ||
	                    compare(PENTHOUSE, inputIdentifier.toString(), 2) == 0 ||
	                    compare(UP, inputIdentifier .toString(), 2) == 0))
	        {
	            copy(outputIdentifier, star.substring(1));
	            copyLength(outputIdentifier, lenStar - 1, inputIdentifier, lenFirst);
	            outputIdentifier.setLength(lenStr - 1);
	
	            return outputIdentifier.toString();
	        } else if ((digit = Character.digit(inputIdentifier.charAt(0), 10)) != 0
	                && (compare(SIDE, star, 1, 4) == 0 ||
	                    compare(BASEMENT, star, 1, 4) == 0 ||
	                    compare(INTERIOR, star, 1, 4) == 0 ||
	                    compare(REAR, star, 1, 4) == 0 ||
	                    compare(BOTTOM, star, 1, 4) == 0 ||
	                    compare(MILIEU, star, 1, 4) == 0 ||
	                    compare(LOWER, star, 1, 4) == 0 ||
	                    compare(UPPER, star, 1, 4) == 0 ||
	                    compare(FRONT, star, 1, 4) == 0 ||
	                    compare(DOWN, star, 1, 4) == 0 ||
	                    compare(LEFT, star, 1, 4) == 0 ||
	                    compare(RIGHT, star, 1, 4) == 0 ||
	                    compare(LOBBY, star, 1, 4) == 0 ||
	                    compare(OFFICE, star, 1, 3) == 0 ||
	                    compare(TOP, star, 1, 3) == 0 ||
	                    compare(PENTHOUSE, star, 1, 2) == 0 ||
	                    compare(UP, star, 1, 2) == 0))
	        {
	            copy(outputIdentifier, inputIdentifier);
	            // *star = '-';
	            copy(outputIdentifier, lenFirst, star, 1);
	            outputIdentifier.setLength(lenStr - 1);
	            // System.out.println(str + "|"+outstr);
	            return outputIdentifier.toString();
	        }
	    }
	
	    // Search for second ASTERISK
	    int lenBefstar;
	    star2 = strchr(star, 1, '*');
	    if (star2 != null) {
	        floor = star2.substring(1).startsWith(FLOOR);
	        level = star2.substring(1).startsWith(LEVEL);
	
	        if (floor || level) {
	            star2.setCharAt(3, '*');
	            lenBefstar = lenStr - lenStar;
	            copyLength(star2, 4, inputIdentifier, lenBefstar);
	            copyLength(inputIdentifier, star, 1, lenStr);
	            inputIdentifier.setLength(lenStr);
	
	            // REinitialize
	            star = strchr(inputIdentifier, '*');
	            lenStr = inputIdentifier.length();
	            lenStar = star.length();
	            star2 = strchr(star, 1, '*');
	        }
	    }
	
	    // Look for the following types : LOWR*BOTM ----> BOTM-LOWR
	    // UP*BSMT ----> BSMT-UP; UP*TOP ----> TOP-UP
	    // Skip any which contain "RD" etc. or digits.
	    if (lenStr >= 5 &&
	        star2 == null &&
	        lenStar >= 3 &&
	        compare(SECOND_SUFFIX, inputIdentifier, strIdx - 2, 2) != 0 &&
	        compare(THIRD_SUFFIX, inputIdentifier, strIdx - 2, 2) != 0 &&
	        compare(ORDINAL_SUFFIX, inputIdentifier, strIdx - 2, 2) != 0 &&
	        compare(FIRST_SUFFIX, inputIdentifier, strIdx - 2, 2) != 0 &&
	        (digit = Character.digit(star.charAt(1), 10)) == 0 &&
	        (digit = Character.digit(inputIdentifier.charAt(0), 10)) == 0)
	    {
	        lenBefstar = lenStr - lenStar;
	        // Put in alphabetic order by flipping the before portion of the
	        // ASTERISK and the portion after the ASTERISK.
	        if (inputIdentifier.charAt(0) > star.charAt(1)) {
	            copyLength(outputIdentifier, star, 1, lenStr);
	            copyLength(outputIdentifier, lenStar - 1, "-", 1);
	            copyLength(outputIdentifier, lenStar, inputIdentifier, lenBefstar);
	            outputIdentifier.setLength(lenStr);
	            // System.out.println(str + "|"+outstr);
	            return outputIdentifier.toString();
	        } else {
	            // No need to flip, just put a '-' in
	            star.setCharAt(0, '-');
	            copy(outputIdentifier, inputIdentifier);
	            outputIdentifier.setLength(lenStr);
	            // System.out.println(str + "|"+outstr);
	            return outputIdentifier.toString();
	        }
	    }
	
	    // Check for Input: 2*G or 2*4 Output: 2G or 2-4
	    int befStar;
	    int aftStar;
	    int chr2Cnt = 0;
	    int dig1Cnt = 0;
	    int dig2Cnt = 0;
	    int chr1Cnt = 0;
	    befStar = lenStr - lenStar;
	    aftStar = lenStr - befStar - 1;
	    star2 = strchr(star, 1, '*');
	    if (star2 != null) {
	        aftStar = aftStar - star2.length();
	    }
	
	    for (int i = 0; i < befStar; i++) {
	        if ((digit = Character.digit(inputIdentifier.charAt(i), 10)) == 0) {
	            // Count number of characters
	            chr1Cnt++;
	        } else {
	            // Count number of digits
	            dig1Cnt++;
	        }
	    }
	
	    for (int i = 1; i <= aftStar; i++) {
	        if ((digit = Character.digit(star.charAt(i), 10)) == 0) {
	            // Count of characters after '*'
	            chr2Cnt++;
	        } else {
	            dig2Cnt++;
	        }
	    }
	
	    int digit1;
	    int digit2;
	    boolean iSkip = false;
	    if (chr1Cnt == befStar && (lenStr < 3 || star.charAt(1) == '&')) {
	        if (chr2Cnt == aftStar && star.charAt(1) == '&') {
	            // Both characters, put a DASH
	            star.setCharAt(0, '-');
	            copy(outputIdentifier, inputIdentifier);
	        } else if (dig2Cnt == aftStar || star.charAt(1) == '&') {
	            // Both different,put togther
	            copyLength(outputIdentifier, inputIdentifier, befStar);
	            copy(outputIdentifier, befStar, star, 1);
	        }
	    } else if (dig1Cnt == befStar && (lenStr < 3 || star.charAt(1) == '&')) {
	        if (dig2Cnt == aftStar && star.charAt(1) == '&') {
	            star.setCharAt(0, '-');
	            copy(outputIdentifier, inputIdentifier);
	        } else if (chr2Cnt == aftStar || star.charAt(1) == '&') {
	            copyLength(outputIdentifier, inputIdentifier, befStar);
	            copy(outputIdentifier, befStar, star, 1);
	        }
	    } else {
	        // Check for Input: 3G*1 ---> Output: 3G1 or
	        // Input: 3G*b ---> Output: 3G-b
	        if (compare(SECOND_SUFFIX, inputIdentifier, strIdx - 2, 2) == 0 ||
	            compare(THIRD_SUFFIX, inputIdentifier, strIdx - 2, 2) == 0 ||
	            compare(ORDINAL_SUFFIX, inputIdentifier, strIdx - 2, 2) == 0 ||
	            compare(FIRST_SUFFIX, inputIdentifier, strIdx - 2, 2) == 0)
	        {
	            iSkip = true;
	        } else {
	            digit1 = Character.digit(star.charAt(1), 10);
	            digit2 = Character.digit(star.charAt(1), 10);
	
	            if (digit1 != 0 && digit2 == 0 || digit1 == 0 && digit2 != 0) {
	                copyLength(outputIdentifier, inputIdentifier, befStar);
	                copy(outputIdentifier, befStar, star, 1);
	            } else {
	                star.setCharAt(0, '-');
	                copy(outputIdentifier, inputIdentifier);
	            }
	        }
	    }
	    
	    int len;
	    boolean and = false;
	    boolean iMore = true;
	    if (!iSkip) {
	        while (iMore) {
	            iMore = false;
	
	            if (star.charAt(1) == '&') {
	                and = true;
	            }
	            star = strchr(star, 1, '*');
	
	            if (star != null) {
	                len = outputIdentifier.length() - star.length();
	
	                if ((digit = Character.digit(star.charAt(1), 10)) == 0) {
	                    if (dig2Cnt == aftStar || and) {
	                        copy(star, star, 1);
	                        copy(outputIdentifier, len, star);
	                    } else if (chr2Cnt == aftStar) {
	                        star.setCharAt(0, '-');
	                        copy(outputIdentifier, len, star);
	                    }
	                } else {
	                    if (dig2Cnt == aftStar) {
	                        star.setCharAt(0, '-');
	                        copy(outputIdentifier, len, star);
	                    } else if (chr2Cnt == aftStar) {
	                        copy(star, star, 1);
	                        copy(outputIdentifier, len, star);
	                    }
	                }
	                iMore = true;
	                continue;
	            } else {
	                // System.out.println(str + "|"+outstr);
	                return outputIdentifier.toString();
	            }
	        }
	    }
	
	
	    int numlen;
	    boolean iCheck = false;
	    // Check for a digit in the first character
	    if ((digit = Character.digit(inputIdentifier.charAt(0), 10)) != 0) {
	
	        // Check for Fl or Lvl contained in the string after the first star.
	        floor = star.substring(1).startsWith(FLOOR);
	        level = star.substring(1).startsWith(LEVEL);
	
	        if (floor || level) {
	            if (compare(SECOND_SUFFIX, inputIdentifier, strIdx - 2, 2) == 0 ||
	                compare(THIRD_SUFFIX, inputIdentifier, strIdx - 2, 2) == 0 ||
	                compare(ORDINAL_SUFFIX, inputIdentifier, strIdx - 2, 2) == 0 ||
	                compare(FIRST_SUFFIX, inputIdentifier, strIdx - 2, 2) == 0)
	            {
	                // Copy the digits before the st, nd, th, rd to the output
	                // string.
	                // Numlen is the number of digits before the ND, ST, TH, RD.
	                numlen = lenStr - lenStar - 2;
	
	                for (int i = numlen - 1; i >= 0; i--) {
	                    outputIdentifier.setCharAt(i, inputIdentifier.charAt(i));
	                }
	
	                // Check for a second ASTERISK. Eg: 22nd*Fl*G
	                star = strchr(star, 1, '*');
	                // Only one ASTERISK. Input: 2nd*Fl ---> Output: 2Fl
	                if (star == null) {
	                    if (floor) {
	                        copyLength(outputIdentifier, numlen, FLOOR, 2);
	                        outputIdentifier.setLength(numlen + 2);
	                    }
	
	                    if (level) {
	                        copyLength(outputIdentifier, numlen, LEVEL, 3);
	                        outputIdentifier.setLength(numlen + 3);
	                    }
	                    iCheck = true;
	                }
	
	                if (!iCheck) {
	                    len2star = star.substring(1).length();
	                    // Determine if first two characters after the ASTERISK
	                    // are digits or
	                    // letters.
	                    digit = Character.digit(star.charAt(1), 10);
	
	                    if (star.charAt(2) != '*') {
	                        digit2 = Character.digit(star.charAt(2), 10);
	                    } else {
	                        // Shift over on character if a ASTERISK is in the
	                        // star+2
	                        // position. Example: star = *2*3
	                        digit2 = Character.digit(star.charAt(3), 10);
	
	                        if (digit2 != 0 && digit == 0 || digit2 == 0 && digit != 0) {
	                            for (int i = 2; i <= len2star; i++) {
	                                star.setCharAt(i, star.charAt(i + 1));
	                            }
	                            star.setLength(len2star);
	                            len2star--;
	                        }
	                    }
	
	                    // First or first and second characters are letters.
	                    // Input: 2nd*Fl*EG ---> Output: 2EG
	                    if (digit == 0 && digit2 == 0 && len2star < 3 || digit == 0 && len2star == 1 || digit == 0 && star2 != null && len2star < 3) {
	
	                        copy(outputIdentifier, numlen, star, 1);
	
	                    } else if (digit != 0 && digit2 != 0 && len2star < 3 || digit != 0 && len2star == 1 || digit != 0 && star2 != null && len2star < 3) {
	                        // First or first and second characters are digits.
	                        // Input: 2nd*Fl*34 ---> Output: 2-34
	                        outputIdentifier.setCharAt(numlen, '-');
	                        copy(outputIdentifier, numlen + 1, star, 1);
	                    } else {
	                        // First and second characters after ASTERISK are
	                        // mixed.
	                        // Input: 2nd*Fl*2E ----> Output: 2Fl-2E or
	                        // Input: 2nd*Fl*Rear ----> Output: 2Fl-Rear
	                        if (floor) {
	                            copyLength(outputIdentifier, numlen, NUMBERED_FLOOR, 3);
	                            copy(outputIdentifier, numlen + 3, star, 1);
	                        }
	
	                        if (level) {
	                            copyLength(outputIdentifier, numlen, NUMBERED_LEVEL, 4);
	                            copy(outputIdentifier, numlen + 4, star, 1);
	                        }
	                    }
	                }
	            }
	        } else {
	            // First character is a digit, but no Fl or Lvl in the field.
	            // Input: 2nd*Rear ---> Output: 2Fl-Rear
	            if (lenStar > 3) {
	                int i = 0;
	                while (digit != 0) {
	                    outputIdentifier.setCharAt(i, inputIdentifier.charAt(i));
	                    i++;
	                    digit = Character.digit(inputIdentifier.charAt(i), 10);
	                }
	
	                copyLength(outputIdentifier, i, NUMBERED_FLOOR, 3);
	                copy(outputIdentifier, i + 3, star, 1);
	            }
	        }
	    }
	    // Search for more asterisks and append onto the string
	
	    // Before searching for another ASTERISK, check that previous star
	    // did not contain an ampersand
	    star = strchr(outputIdentifier, '*');
	    int prevDigit;
	
	    if (star != null) {
	        len = outputIdentifier.length() - star.length();
	        star.setCharAt(0, '-');
	        len++;
	        // .. Increment len to include '-'
	        int i = 0;
	
	        while (star.charAt(1 + i) != '*' && star.length() > 1 + i) {
	            outputIdentifier.setCharAt(len + i, star.charAt(1 + i));
	            i++;
	        }
	        star = strchr(star, 1, '*');
	
	        while (star != null) {
	            len = outputIdentifier.length() - star.length();
	            prevDigit = Character.digit(outputIdentifier.charAt(len - 1), 10);
	            digit = Character.digit(star.charAt(1), 10);
	
	            // Two consecutive letters or two consecutive digits.
	            if (prevDigit == 0 && digit == 0 || prevDigit != 0 && digit != 0) {
	                outputIdentifier.setCharAt(len, '-');
	                i = 1;
	
	                while (star.charAt(i) != '*' && star.length() > i) {
	                    outputIdentifier.setCharAt(len + i, star.charAt(i));
	                    outputIdentifier.setLength(len + 1 + i);
	                    i++;
	                }
	            } else {
	                i = 0;
	                while (star.charAt(i + 1) != '*' && star.length() > 1 + i) {
	                    outputIdentifier.setCharAt(len + i, star.charAt(1 + i));
	                    outputIdentifier.setLength(len + 1 + i);
	                    i++;
	                }
	            }
	            star = strchr(star, 1, '*');
	        }
	    }
	    // System.out.println(str + "|"+outstr);
	    return outputIdentifier.toString();
	}

	/**
	 * 
	 *            the input string
	 *            the output string
	 */
	protected static StringBuilder strchr(final StringBuilder sb, final char cha) {
	    // If the string sb is null return a null object
	    if (sb == null) {
	        return null;
	    }
	
	    // The length of the string
	    final int iLen = sb.length();
	    int i;
	
	    // Search for the character cha
	    for (i = 0; i < iLen; i++) {
	        if (sb.charAt(i) == cha) {
	            return sb.delete(0, i);
	        }
	    }
	    return null;
	}

	protected static StringBuilder strchr(final StringBuilder sb, final int ind, final char cha) {
	    // If the string sb is null return a null object
	    if (sb == null) {
	        return null;
	    }
	
	    // The length of the string
	    final int iLen = sb.length();
	    int i;
	
	    // Search for the character cha
	    for (i = ind; i < iLen; i++) {
	        if (sb.charAt(i) == cha) {
	            return sb.delete(0, i);
	        }
	    }
	
	    return null;
	}

	protected static int compare(final String intS, final StringBuilder finSb, final int index, final int len) {
	
	    if (intS == null || finSb == null) {
	        return 2;
	    }
	
	    final int len1 = intS.length();
	    final int len2 = finSb.length();
	
	    String s1;
	    String s2;
	
	    // First check if the index is within the string itself and
	    // keep only the jneeded substring
	    if (index < len2) {
	        s2 = finSb.substring(index);
	    } else {
	        s2 = finSb.toString();
	    }
	
	    // Then compare the length of the strings with the length over which we
	    // compare them
	    if (len1 < len) {
	        s1 = intS;
	    } else {
	        s1 = intS.substring(0, len);
	    }
	
	    if (s2.length() > len) {
	        s2 = s2.substring(0, len);
	    }
	
	    if (s1.compareTo(s2) > 0) {
	        return 1;
	
	    } else if (s1.compareTo(s2) < 0) {
	        return -1;
	
	    } else {
	        return 0;
	    }
	}

	/**
	 * Copy the substring finSb, starting at index index, into the original
	 * string intSb over length 'length'.
	 * 
	 *            the original string
	 *            the replacement string
	 *            the start index of the substring of the replacement string
	 *            the length of the substring of the replacement string
	 *             an exception
	 */
	protected static void copyLength(final StringBuilder intSb, final StringBuilder finSb, final int index, final int length) throws NullPointerException {
	
	    final int len2 = finSb.length();
	    int len1;
	    int i;
	
	    intSb.setLength(0);
	
	    // First, extract the substring from intSb
	    if (index < len2) {
	        intSb.append(finSb.substring(index));
	    }
	
	    len1 = intSb.length();
	
	    /* Handle case when finSb is null or smaller than the expected length */
	    if (len1 < length) {
	
	        // Append the finSb string
	        if (index >= len2) {
	            intSb.append(finSb);
	        }
	
	        // Fill the string with a length-long whitespace characters
	        for (i = 0; i < length - len1; i++) {
	            intSb.append(" ");
	        }
	        return;
	    }
	
	    intSb.setLength(length);
	}

	/**
	 * Copy the string finSb into the original string intS starting at index ind
	 * 
	 *            the original string
	 *            the start index of the original string
	 *            the replacement string
	 */
	protected static StringBuilder copy(final StringBuilder intSb, final int ind, final String finS) {
	
	    final int len1 = intSb.length();
	
	    // First check if the index ind is within the string range itself and
	    // keep only the needed substring
	    if (ind < len1) {
	        intSb.setLength(ind);
	        return intSb.append(finS);
	
	    } else {
	        return intSb.append(finS);
	    }
	}

	/**
	 * Copy the string finSb into the original string intSb starting at index
	 * ind and over length 'length'.
	 * 
	 *            the original string
	 *            the start index of the original string
	 *            the replacement string
	 *            the length of the substring of the replacement string
	 */
	protected static StringBuilder copyLength(final StringBuilder intSb, final int ind, final StringBuilder finSb, final int length) {
	
	    final int len2 = finSb.length();
	    int i;
	
	    intSb.setLength(ind);
	
	    // Handle case when finSb length is less than the defined length
	    if (len2 < length) {
	
	        // Append the finSb string
	        intSb.append(finSb);
	
	        // Fill the string with a length-long whitespace characters
	        for (i = 0; i < length - len2; i++) {
	            intSb.append(" ");
	        }
	        return intSb;
	    }
	
	    return intSb.replace(ind, ind + length, finSb.substring(0, length));
	}

	/**
	 * Copy the substring finSb, starting at index index, into the original
	 * string intSb, starting at index ind2, over length 'length'.
	 * 
	 *            the original string
	 *            the start index of the substring of the original string
	 *            the replacement string
	 *            the start index of the substring of the replacement string
	 *            the length of the substring of the replacement string
	 */
	protected static StringBuilder copyLength(final StringBuilder intSb, final int ind1, final StringBuilder finSb, final int ind2, final int length) {
	
	    int len1;
	    final int len2 = finSb.length();
	    int i;
	
	    intSb.setLength(ind1);
	
	    // And, extract the substring from finSb
	    if (ind2 < len2) {
	        intSb.append(finSb.substring(ind2));
	    }
	
	    len1 = intSb.length();
	
	    // Handle case when finSb is null or smaller than the expected length
	    if (len1 < length) {
	
	        // Fill the string with a length-long whitespace characters
	        for (i = 0; i < length - len1; i++) {
	            intSb.append(" ");
	        }
	        return intSb;
	    }
	
	    return intSb.delete(length, len1);
	}

	protected static int compare(final String intS, final String finS, final int len) {
	
	    if (intS == null || finS == null) {
	        return 2;
	    }
	
	    final int len1 = intS.length();
	    final int len2 = finS.length();
	
	    String s1;
	    String s2;
	
	    if (len1 < len) {
	        s1 = intS;
	    } else {
	        s1 = intS.substring(0, len);
	    }
	
	    if (len2 < len) {
	        s2 = finS;
	
	    } else {
	        s2 = finS.substring(0, len);
	    }
	
	    if (s1.compareTo(s2) > 0) {
	        return 1;
	
	    } else if (s1.compareTo(s2) < 0) {
	        return -1;
	
	    } else {
	        return 0;
	    }
	}

	/**
	 * Copy a substring of finSb, starting at index ind2 to the end of the
	 * string, into the original string intSa, starting at index ind1. The
	 * returned string is the concatenation of a substring of the original
	 * string, from index inde1, and a substring of the second string, starting
	 * at index ind2.
	 * 
	 *            the original string
	 *            the start index of the original string
	 *            the replacement string
	 *            the start index of the second string
	 */
	protected static StringBuilder copy(final StringBuilder intSb, final int ind1, final StringBuilder finSb, final int ind2) {
	
	    // String s;
	
	    final int len1 = intSb.length();
	    final int len2 = finSb.length();
	
	    if (ind1 < len1) {
	        intSb.setLength(ind1);
	    }
	
	    if (ind2 < len2) {
	        // s = finSb.substring(ind2, len2);
	        return intSb.append(finSb.substring(ind2, len2));
	    } else {
	        return intSb.append(finSb);
	    }
	}

	/**
	 * Copy the string finS into the original string intSb starting at index ind
	 * and over length 'length'.
	 * 
	 *            the original string
	 *            the start index of the original string
	 *            the replacement string
	 *            the length of the substring of the replacement string
	 *             an exception
	 */
	protected static StringBuilder copyLength(final StringBuilder intSb, final int ind, final String finS, final int length) throws IndexOutOfBoundsException {
	    final int len2 = finS.length();
	    int i;
	
	    // Handle case when finSb length is less than the defined length
	    if (len2 < length) {
	
	        // Append the finSb string
	        intSb.append(finS);
	
	        // Fill the string with a length-long whitespace characters
	        for (i = 0; i < length - len2; i++) {
	            intSb.append(" ");
	        }
	        return intSb;
	    }
	
	    intSb.replace(ind, ind + length, finS.substring(0, length));
	
	    return intSb;
	
	}

	/**
	 * Copy the substring of finSb over a length 'length' into the original
	 * string intSb
	 * 
	 *            the original string
	 *            the replacement string
	 *            the length of the substring of the replacement string
	 */
	protected static StringBuilder copyLength(final StringBuilder intSb, final StringBuilder finSb, final int length) {
	
	    final int len = finSb.length();
	    int i;
	
	    // Free the initial string
	    intSb.setLength(0);
	
	    /* Handle case when finSb length is less than the defined length */
	    if (len < length) {
	
	        // Append the finSb string
	        intSb.append(finSb);
	
	        // Fill the string with a length-long whitespace characters
	        for (i = 0; i < length - len; i++) {
	            intSb.append(" ");
	        }
	        return intSb;
	    }
	
	    return intSb.append(finSb.substring(0, length));
	}

	/**
	 * Copy the substring of finSb, starting at index ind, into the original
	 * string intSa
	 * 
	 *            the original string
	 *            the replacement string
	 *            aaaa
	 *             an exception
	 */
	protected static StringBuilder copy(final StringBuilder intSb, final StringBuilder finSb, final int ind) throws NullPointerException {
	
	    final int len = finSb.length();
	    intSb.setLength(0);
	
	    if (ind < len) {
	        return intSb.append(finSb.substring(ind, len));
	
	    } else {
	        return intSb.append(finSb);
	    }
	}

	/**
	 * Copy the string finSb into the original string intSb starting at index
	 * ind
	 * 
	 *            the original string
	 *            the replacement string
	 *            aaaa
	 *             an exception
	 */
	protected static StringBuilder copy(final StringBuilder intSb, final int ind, final StringBuilder finSb) throws NullPointerException {
	
	    final int len1 = intSb.length();
	
	    // First, check if the index ind is within the string range itself and
	    // keep only the needed substring
	    if (ind < len1) {
	
	        intSb.setLength(ind);
	        return intSb.append(finSb);
	
	    } else {
	        return intSb.append(finSb);
	    }
	}
}
