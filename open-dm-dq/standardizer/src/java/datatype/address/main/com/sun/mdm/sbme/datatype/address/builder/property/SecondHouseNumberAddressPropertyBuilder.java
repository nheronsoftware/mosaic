/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 
package com.sun.mdm.sbme.datatype.address.builder.property;

import static com.sun.mdm.sbme.datatype.address.builder.CombinationOption.SPACE;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.HOUSE_NUMBER_SUFFIX;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.SECOND_HOUSE_NUMBER;

import com.sun.inti.components.record.Record;
import com.sun.mdm.sbme.datatype.address.Address;
import com.sun.mdm.sbme.datatype.address.pattern.InputTokenType;
import com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType;
import com.sun.mdm.sbme.normalizer.NormalizedToken;

/**
 *
 */
public class SecondHouseNumberAddressPropertyBuilder extends AddressPropertyBuilder {
    protected void buildProperty(Address address, NormalizedToken<InputTokenType> normalizedToken, InputTokenType inputTokenType, OutputTokenType outputTokenType, Record context) {
        this.checkForSpelledOutNumber(normalizedToken);
        final String[] fractionParts = fractionWithinNumber(normalizedToken);

        final int clueWordId = normalizedToken.getClueWordId(inputTokenType);

        if (fractionParts == null) {
            this.loadField(address, SECOND_HOUSE_NUMBER, normalizedToken.getImage(), clueWordId, inputTokenType, getLocale().getAbbreviationOption(), SPACE);
        } else {
            this.loadField(address, HOUSE_NUMBER_SUFFIX, fractionParts[1], 0, inputTokenType, getLocale().getAbbreviationOption(), SPACE);
            this.loadField(address, SECOND_HOUSE_NUMBER, fractionParts[0], clueWordId, inputTokenType, getLocale().getAbbreviationOption(), SPACE);
        }
    }
}
