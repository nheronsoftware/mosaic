/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.address.normalizer.validator;

import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.EXTENSION;
import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.HIGHWAY_ROUTE;
import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.STATE_ABBREVIATION;
import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.STREET_TYPE;

import java.util.regex.Pattern;

import com.sun.mdm.sbme.clue.Clue;
import com.sun.mdm.sbme.clue.ClueRegistry;
import com.sun.mdm.sbme.datatype.address.locale.AddressLocale;
import com.sun.mdm.sbme.datatype.address.pattern.InputTokenType;
import com.sun.mdm.sbme.normalizer.ClueWord;
import com.sun.mdm.sbme.normalizer.validator.ClueValidator;
import com.sun.mdm.sbme.parser.Tokenization;

/**
 *
 */
// FIXME This validator seems to apply to US only
public class HighwayRoadComboClueValidator implements ClueValidator<InputTokenType> {
    private AddressLocale locale;
    private ClueRegistry<InputTokenType> clueRegistry;

    public HighwayRoadComboClueValidator() {}
    
    public HighwayRoadComboClueValidator(AddressLocale locale, ClueRegistry<InputTokenType> clueRegistry) {
    	this.locale = locale;
    	this.clueRegistry = clueRegistry;
    }
    
    // TODO A table of clue word types is needed
    private static final int COUNTY_HIGHWAY = 67;
    private static final int COUNTY_ROAD = 69;
    private static final int FARM_ROAD = 99;
    private static final int STATE_HIGHWAY = 257;
    private static final int STATE_ROAD = 258;
    private static final int TOWNSHIP_HIGHWAY = 272;
    private static final int TOWNSHIP_ROAD = 273;
    public final static int TOWNSHIP_ROAD_LOWER_LIMIT = 320;
    private static final int[] clueWordIds = {
        COUNTY_HIGHWAY,
        COUNTY_ROAD,
        FARM_ROAD,
        STATE_HIGHWAY,
        STATE_ROAD,
        TOWNSHIP_HIGHWAY,
        TOWNSHIP_ROAD
    };

    public boolean validate(final Clue<InputTokenType> clue, final Tokenization tokenization, final int startIndex, final int endIndex) {
        return !this.unsuitableHighwayRoadcombo(clue, tokenization, startIndex, endIndex);
    }

    private final static String STREET_SYMBOL = "ST ";
    public boolean unsuitableHighwayRoadcombo(final Clue<InputTokenType> clue, final Tokenization tokenization, final int startIndex, int endIndex) {
        if (startIndex > 0 && clue.getName().startsWith(STREET_SYMBOL)) {
            final String previousWord = tokenization.getToken(startIndex - 1).getImage();
            final Clue<InputTokenType> previousClue = this.clueRegistry.lookup(previousWord);

            if (previousClue != null) {
                if (previousClue.getWords().get(0).getType() == EXTENSION) {
                    return true;
                }
                for (final ClueWord<InputTokenType> clueWord : previousClue.getWords()) {
                    if (clueWord.getType() == STREET_TYPE || clueWord.getType() == STATE_ABBREVIATION) {
                        return false;
                    }
                }
            }

            if (containsLetter(previousWord)) {
                return true;
            }
        }

        if (isRoadOrHighway(clue)) {
            if (endIndex == tokenization.size()) {
                return true;
            }

            final String nextWord = tokenization.getToken(endIndex).getImage();

            return !allDigits(nextWord) && nonBlankTrailingChars(nextWord) && !twoAlphas(nextWord);
        }

        return false;
    }

    private static final Pattern containsLetter = Pattern.compile("\\p{Alpha}");

    private static boolean containsLetter(final CharSequence word) {
        return containsLetter.matcher(word).find();
    }

    private static final Pattern allDigits = Pattern.compile("^\\d+$");

    private static boolean allDigits(final CharSequence word) {
        return allDigits.matcher(word).find();
    }

    private static final Pattern nonBlankTrailingChars = Pattern.compile("\\S{2}\\s*$");

    private static boolean nonBlankTrailingChars(final CharSequence word) {
        return nonBlankTrailingChars.matcher(word).find();
    }

    private static final Pattern twoAlphas = Pattern.compile("^\\p{Alpha}{2}$");

    private static boolean twoAlphas(final CharSequence word) {
        return twoAlphas.matcher(word).find();
    }

    // FIXME Method roadOrHighway is highly specific to US!
    private static boolean isRoadOrHighway(final Clue<InputTokenType> clue) {
        if (clue.getWords().get(0).getType() == HIGHWAY_ROUTE) {
            if (clue.getWords().get(0).getId() >= TOWNSHIP_ROAD_LOWER_LIMIT) {
                return true;
            }
            
            for (final int element : clueWordIds) {
                if (clue.getWords().get(0).getId() == element) {
                    return true;
                }
            }
        }

        return false;
    }

    public AddressLocale getLocale() {
        return this.locale;
    }

    public void setLocale(final AddressLocale locale) {
        this.locale = locale;
    }

    public ClueRegistry<InputTokenType> getClueRegistry() {
        return this.clueRegistry;
    }

    public void setClueRegistry(final ClueRegistry<InputTokenType> clueRegistry) {
        this.clueRegistry = clueRegistry;
    }
}
