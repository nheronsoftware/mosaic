package com.sun.mdm.sbme.datatype.address.builder.postprocessor;

import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.STRUCTURE_IDENTIFIER;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.WITHIN_STRUCTURE_IDENTIFIER;

import com.sun.inti.components.record.Record;
import com.sun.mdm.sbme.datatype.address.Address;

public class BuildingNumberAddressPostprocessor extends AddressPostprocessor {
	/*
     * Checks if any of the following fields has values: 1. Building Number
     * Prefix 2. Building Number 3. Building Number Suffix If yes, ALL_FIELDS 3 of them
     * must be moved because these fields are no longer valid. If the
     * WithinStructureIdentifier field is blank, then put them there, otherwise
     * put them in the new StructureIdentifier field.
     */
    protected void postprocess(Address address, Record context) {
        String buildingNumberPrefix = context.getString("buildingNumberPrefix");
        String buildingNumber = context.getString("buildingNumber");
        String buildingNumberSuffix = context.getString("buildingNumberSuffix");
        
        if (buildingNumberPrefix.length() == 0 && buildingNumber.length() == 0 && buildingNumberSuffix.length() == 0) {
            return;
        }

        final StringBuilder field = new StringBuilder();
        field.append(buildingNumberPrefix);
        field.append(buildingNumber);
        field.append(buildingNumberSuffix);

        if (address.get(WITHIN_STRUCTURE_IDENTIFIER) == null) {
        	address.set(WITHIN_STRUCTURE_IDENTIFIER, field.toString());
        } else {
        	address.set(STRUCTURE_IDENTIFIER, field.toString());
        }
    }
}
