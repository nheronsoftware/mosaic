/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.address.pattern;

import java.util.HashMap;
import java.util.Map;

import com.sun.mdm.sbme.MdmStandardizationException;

/**
 *
 */
public enum PatternType {
    HOUSE("H"),
    BUILDING("B"),
    UNIT_WITHIN_STRUCTURE("W"),
    STREET_TYPE_DIRECTION("T"),
    RURAL_ROUTE("R"),
    POST_OFFICE_BOX("P"),
    NUMERIC("N");

    private final String symbol;

    PatternType(final String symbol) {
        this.symbol = symbol;
    }

    public String toString() {
        return this.symbol;
    }

    public boolean isTypeOf(final String type) {
        return this.toString().equals(type);
    }

    private static final Map<String, InputTokenType> valueMap = new HashMap<String, InputTokenType>();
    static {
        for (final InputTokenType value : InputTokenType.values()) {
            valueMap.put(value.toString(), value);
        }
    }

    public static InputTokenType getType(final String symbol) throws MdmStandardizationException {
        if (!valueMap.containsKey(symbol)) {
            throw new MdmStandardizationException("No such pattern type symbol: " + symbol);
        }
        return valueMap.get(symbol);
    }
}
