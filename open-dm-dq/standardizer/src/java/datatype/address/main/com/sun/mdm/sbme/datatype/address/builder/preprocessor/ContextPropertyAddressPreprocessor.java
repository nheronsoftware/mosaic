package com.sun.mdm.sbme.datatype.address.builder.preprocessor;

import java.util.List;

import com.sun.inti.components.record.Record;
import com.sun.mdm.sbme.builder.preprocessor.StandardizedRecordBuilderPreprocessor;
import com.sun.mdm.sbme.normalizer.NormalizedToken;
import com.sun.mdm.sbme.pattern.OutputPattern;

public class ContextPropertyAddressPreprocessor<I extends Enum<I>, O extends Enum<O>> implements StandardizedRecordBuilderPreprocessor<I, O> {
    private String propertyName;
    
    public ContextPropertyAddressPreprocessor() {}
    
    public ContextPropertyAddressPreprocessor(String propertyName) {
    	this.propertyName = propertyName;
    }
    
    public void preprocess(List<NormalizedToken<I>> normalizedTokens, List<? extends OutputPattern<I, O>> addressOutputPatterns, Record context) {
        context.set(this.propertyName, new StringBuilder());
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }
}
