/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.address.pattern;

import com.sun.inti.components.util.Abbreviated;
import com.sun.mdm.sbme.util.Aliased;


/**
 *
 */
public enum OutputTokenType implements Aliased, Abbreviated {
    EXTRA_INFORMATION("EI", "ExtraInfo"), // Address property: extraInformation
    HOUSE_NUMBER_PREFIX("P1", "HouseNumberPrefix"), // Address property: houseNumberPrefix
    HOUSE_NUMBER_SUFFIX("HS", "HouseNumberSuffix"), // Address property: houseNumberSuffix
    POST_OFFICE_BOX_DESCRIPTOR("BX", "BoxDescript"), // Address property: postOfficeBoxDescriptor
    POST_OFFICE_BOX_IDENTIFIER("XN", "BoxIdentif"), // Address property: postOfficeBoxIdentifier
    PROPERTY_PREFIX_DIRECTION("DB", "PropDesPrefDirection"), // Address property: propertyPrefixDirection
    PROPERTY_SUFFIX_DIRECTION("BD", "PropDesSufDirection"), // Address property: propertySuffixDirection
    PROPERTY_TYPE_PREFIX("TB", "PropDesPrefType"), // Address property: propertyTypePrefix
    PROPERTY_TYPE_SUFFIX("BT", "PropDesSufType"), // Address property: propertyTypeSuffix
    RURAL_ROUTE_DESCRIPTOR("RR", "RuralRouteDescript"), // Address property: ruralRouteDescriptor
    SECOND_HOUSE_NUMBER_PREFIX("P2", "SecondHouseNumberPrefix"), // Address property: secondHouseNumberPrefix
    SECOND_STREET_NAME("N2", "OrigSecondStreetName"), // Address property: originalSecondStreetName
    SECOND_STREET_NAME_SUFFIX_TYPE_S2("S2", "SecondStreetNameSufType"), // Address property: secondStreetNameSuffixType
    SECOND_STREET_NAME_SUFFIX_TYPE_T2("T2", "SecondStreetNameSufType"), // Address property: secondStreetNameSuffixType
    STREET_NAME("NA", "StreetName"), // Address properties: originalStreetName, storageStreetName, matchingStreetName, 
    STREET_NAME_EXTENSION_INDEX("EX", "StreetNameExtensionIndex"), // Address property: streetNameExtensionIndex
    STREET_NAME_PREFIX_DIRECTION("PD", "StreetNamePrefDirection"), // Address property: streetNamePrefixDirection
    STRUCTURE_DESCRIPTOR("BY", "CenterDescript"), // Address property: structureDescriptor
    STRUCTURE_IDENTIFIER("BI", "CenterIdentif"), // Address property: structureIdentifier
    WITHIN_STRUCTURE_DESCRIPTOR("WD", "WithinStructDescript"), // Address property: withinStructureDescriptor
    WITHIN_STRUCTURE_IDENTIFIER("WI", "WithinStructIdentif"), // Address property: withinStructureIdentifier

    BUILDING_NUMBER("NB", "BuildingNumber"), // Address properties: withinStructureIdentifier, structureIdentifier
    BUILDING_NUMBER_PREFIX("1P", "BuildingNumberPrefix"), // Address properties: withinStructureIdentifier, structureIdentifier
    BUILDING_NUMBER_SUFFIX("BS", "BuildingNumberSuffix"), // Address properties: withinStructureIdentifier, structureIdentifier
    FIRST_HOUSE_NUMBER("H1", "FirstHouseNumber"), // Address properties: houseNumber, houseNumberSuffix
    HOUSE_NUMBER("HN", "HouseNumber"), // Address properties: houseNumber, houseNumberSuffix 
    PROPERTY_BUILDING_NAME("BN", "PropertyBuildingName"), // Address properties: originalPropertyName, matchingPropertyName
    RURAL_ROUTE_IDENTIFIER("RN", "RuralRouteIdentif"), // Address properties: ruralRouteIdentifier, extraInformation
    SECOND_HOUSE_NUMBER("H2", "SecondHouseNumber"), // Address properties: secondHouseNumber, houseNumberSuffix
    STREET_NAME_PREFIX_TYPE("PT", "StreetNamePrefType"), // Address properties: streetNameExtensionIndex, streetNamePrefixType, originalStreetName, storageStreetName, matchingStreetName
    STREET_NAME_SUFFIX_DIRECTION("SD", "StreetNameSufDirection"), // Address properties: wihtinStructureIdentifier, streetNameSuffixDirection
    STREET_NAME_SUFFIX_TYPE("ST", "StreetNameSufType"), // Address property: streetNameSUffixType
    
    CONJUNCTION("NL", "Conjunction"), // No matching address property, used in output pattern finding
    SECOND_BUILDING_NUMBER_PREFIX("2P", "SecondBuildingNumberPrefix"), // No matching address property, used only by pattern configuration files

    OTHER_B1("B1", "OtherB1"),
    OTHER_B2("B2", "OtherB2"),
    OTHER_BE("BE", "OtherBE"),

    // FIXME These values actually correspond to pattern types, not token types.
    BUILDING_CLASS("B*", "BuildingClass"),
    HOUSE_CLASS("H*", "HouseClass"),
    MOSTLY_NUMERIC_CLASS("N*", "MostlyNumericClass"),
    POST_OFFICE_BOX_CLASS("P*", "PostOfficeBoxClass"),
    RURAL_ROUTE_CLASS("R*", "RuralRouteClass"),
    STREET_TYPE_CLASS("T*", "StreetTypeClass"),
    UNIT_WITHIN_STRUCTURE_CLASS("W*", "UnitWithinStructureClass")
    ;

    private final String alias;
    private final String abbreviation;

    OutputTokenType(final String abbreviation, final String alias) {
        this.alias = alias;
        this.abbreviation = abbreviation;
    }
    
    public String getAlias() {
    	return this.alias;
    }
    
    public String getAbbreviation() {
    	return this.abbreviation;
    }
}
