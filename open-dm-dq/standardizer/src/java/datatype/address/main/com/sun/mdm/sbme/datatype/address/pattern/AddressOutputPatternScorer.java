package com.sun.mdm.sbme.datatype.address.pattern;

import static com.sun.mdm.sbme.datatype.address.pattern.AddressScoringOption.NO_AVERAGE;
import static com.sun.mdm.sbme.datatype.address.pattern.AddressScoringOption.UNMATCHED;
import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.GENERIC_WORD;

import java.util.List;

import com.sun.mdm.sbme.pattern.OutputPattern;
import com.sun.mdm.sbme.pattern.OutputPatternScorer;

public class AddressOutputPatternScorer implements OutputPatternScorer<InputTokenType, OutputTokenType> {
    private double patternWeight;
    private double unmatchedPatternFactor;

    /*
     * Determines a score for a pattern based on the following calculation:
     * score = avg. priority of all sub-positions found (minus) total number of
     * unmatched tokens x PT, where PT = 0 for EI's embedded in positions and PT =
     * 20 for singular EI's.
     */
	public int calculateScore(List<?extends OutputPattern<InputTokenType, OutputTokenType>> patterns) {
        if (patterns.size() == 0) {
            return 0;
        }
        
        double weightSum = 0.0;
        double priorityTotal = 0.0;

        int genericWordCount = 0;
        int unamtchedPatternCount = 0;
        boolean averageDisallowed = false;
        for (OutputPattern<InputTokenType, OutputTokenType> aPattern: patterns) {
        	AddressOutputPattern pattern = (AddressOutputPattern) aPattern;
            // Don't average positions where pattern type = 'EI', but tally
            // the number of unmatched positions (AUs gets tallied only once)
            // D.C. Allow AU type unmatched positions to be counted three times
            // D.C. This tends to give higher scores to position combinations
            // D.C. that account for more tokens. TODO: Make this configurable?
            if (pattern.getScoringOption() == UNMATCHED) {
                if (pattern.getInputTokenTypes().size() == 1 && // Always true
                    pattern.getInputTokenTypes().get(0) == GENERIC_WORD)
                {
                    if (genericWordCount < 3) {
                        unamtchedPatternCount++;
                    }
                    genericWordCount++;
                } else {
                    unamtchedPatternCount++;
                }

                continue;
            }

            // Don't average in the priority for the current sub-pattern if
            // a "+" pattern type has already been averaged.
            if (pattern.getScoringOption() == NO_AVERAGE) {
                if (averageDisallowed) {
                    continue;
                }
                averageDisallowed = true;
            }

            priorityTotal += pattern.getPriority() * this.patternWeight;

            weightSum += this.patternWeight;
        }

        return (int) (priorityTotal / weightSum + 0.5 - unamtchedPatternCount * this.unmatchedPatternFactor);
	}

    public void setUnmatchedPatternFactor(final double unmatchPts) {
        this.unmatchedPatternFactor = unmatchPts;
    }

    public void setPatternWeight(final double patternWeight) {
      this.patternWeight = patternWeight;
    }
}
