package com.sun.mdm.sbme.datatype.address.builder.postprocessor;

import static com.sun.mdm.sbme.datatype.address.builder.AbbreviationOption.STANDARD_ABBREVIATION;
import static com.sun.mdm.sbme.datatype.address.builder.CombinationOption.SPACE;
import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.STRUCTURE_DESCRIPTOR;
import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.UNSPECIFIED;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.EXTRA_INFORMATION;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.HOUSE_NUMBER;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.WITHIN_STRUCTURE_DESCRIPTOR;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.WITHIN_STRUCTURE_IDENTIFIER;

import com.sun.inti.components.record.Record;
import com.sun.mdm.sbme.datatype.address.Address;

public class AUWithinStructureAddressPostprocessor extends AbstractWithinStructureAddressPostprocessor {
	/*
     * Places a "#" in the wsd field if wsd is blank and wsi is non-blank. Also,
     * the wsi field is squashed if needed. If no wsi exists and wsd != blank,
     * then copy wsd to wsi.
     */
    protected void postprocess(Address address, Record context) {
    	String withinStructureDescriptor = address.get(WITHIN_STRUCTURE_DESCRIPTOR);
    	
    	StringBuilder wsi;
    	String withinStructureIdentifier = address.get(WITHIN_STRUCTURE_IDENTIFIER);
    	if (withinStructureIdentifier == null) {
    		wsi = new StringBuilder();
    	} else {
    		wsi = new StringBuilder(withinStructureIdentifier);
    	}
    	
        if (wsi.length() != 0) {
            if (dashFound(wsi) && address.get(HOUSE_NUMBER) == null) {
                address.set(HOUSE_NUMBER, wsi.toString());
                
                wsi = new StringBuilder();
                withinStructureIdentifier = wsi.toString();
                address.set(WITHIN_STRUCTURE_IDENTIFIER, withinStructureIdentifier);
            }

            if (withinStructureDescriptor == null) {
                // FIXME What do "G", "UG" and "LG" mean as structure identifier?
                if ("G".equals(withinStructureIdentifier) ||
                    "UG".equals(withinStructureIdentifier) ||
                    "LG".equals(withinStructureIdentifier))
                {
                    // FIXME What does "Fl"  mean as structure descriptor?
                    this.loadField(address, WITHIN_STRUCTURE_DESCRIPTOR, "Fl", 0, UNSPECIFIED, getLocale().getAbbreviationOption(), SPACE);
                } else {
                    // FIXME What does "U"  mean as structure descriptor?
                    this.loadField(address, WITHIN_STRUCTURE_DESCRIPTOR, "U", 0, UNSPECIFIED, getLocale().getAbbreviationOption(), SPACE);
                }
            }

            address.set(WITHIN_STRUCTURE_IDENTIFIER, searchStandIden(wsi));
        } else if (withinStructureDescriptor != null) {
            // FIXME What does "#" mean as within structure descriptor?
            if (withinStructureDescriptor.length() > 0 && withinStructureDescriptor.charAt(0) == '#') {
                this.loadField(address, EXTRA_INFORMATION, "#", 0, STRUCTURE_DESCRIPTOR, STANDARD_ABBREVIATION, SPACE);
                // FIXME What does "-" mean as within structure descriptor?
                address.set(WITHIN_STRUCTURE_DESCRIPTOR, "-");
            } else if ("G".equals(withinStructureDescriptor) ||
                       "UG".equals(withinStructureDescriptor) ||
                       "UL".equals(withinStructureDescriptor))
            {
                address.set(WITHIN_STRUCTURE_IDENTIFIER, withinStructureDescriptor);
                address.set(WITHIN_STRUCTURE_DESCRIPTOR, "Fl");
            } else {
                address.set(WITHIN_STRUCTURE_IDENTIFIER, "-");
            }
        }
    }
    
    private static boolean dashFound(final StringBuilder str) {
        int i;
        final int len = str.length();

        for (i = 0; i < len; i++) {
            if (str.charAt(i) == '-' || str.charAt(i) == '?') {
                return true;
            }
        }
        return false;
    }
}
