/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.address.pattern;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.sun.mdm.sbme.pattern.OutputPattern;

/**
 *
 */
public class AddressOutputPattern extends AddressPattern implements OutputPattern<InputTokenType, OutputTokenType> {
    private int begin;
    private int end;

    public AddressOutputPattern(final List<InputTokenType> inputTokenTypes,
                         final List<OutputTokenType> outputTokenTypes,
                         final int priority,
                         AddressPatternClass addressPatternClass,
                         AddressScoringOption addressScoringOption,
                         final int begin,
                         final int end)
    {
        super(inputTokenTypes, outputTokenTypes, priority, addressPatternClass, addressScoringOption);
        this.begin = begin;
        this.end = end;
    }

    public AddressOutputPattern(final AddressPattern addressPattern,
                         final int begin,
                         final int end)
    {
        super(addressPattern);
        this.begin = begin;
        this.end = end;
    }
    
    public static List<AddressOutputPattern> sortPatterns(List<AddressOutputPattern> patterns) {
        final List<AddressOutputPattern> result = new ArrayList<AddressOutputPattern>(patterns);
        Collections.sort(result, new Comparator<AddressOutputPattern>() {
           public int compare(AddressOutputPattern left, AddressOutputPattern right) {
               return left.begin - right.begin;
           }
        });
        return result;
    }

    public int getBegin() {
        return this.begin;
    }

    public int getEnd() {
        return this.end;
    }
}
