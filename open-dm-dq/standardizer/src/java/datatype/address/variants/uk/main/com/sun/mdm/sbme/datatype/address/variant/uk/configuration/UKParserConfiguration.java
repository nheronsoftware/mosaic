package com.sun.mdm.sbme.datatype.address.variant.uk.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Bean;
import com.sun.mdm.sbme.datatype.address.locale.AddressLocale;
import com.sun.mdm.sbme.datatype.address.parser.AddressParser;
import com.sun.mdm.sbme.parser.Parser;

@Configuration
public abstract class UKParserConfiguration {

    @Bean
    public Parser parser() {
        return new AddressParser(locale());
    }

    @Bean
    public abstract AddressLocale locale();
}
