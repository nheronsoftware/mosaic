package com.sun.mdm.sbme.datatype.address.variant.uk.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Bean;
import com.sun.mdm.sbme.datatype.address.builder.MasterClueRegistry;
import com.sun.mdm.sbme.datatype.address.configuration.EnglishMasterClueRegistryConfiguration;

@Configuration
public class UKMasterClueRegistryConfiguration extends EnglishMasterClueRegistryConfiguration {

    @Override
    @Bean
    public MasterClueRegistry masterClueRegistry() {
        return super.masterClueRegistry();
    }

    @Override
    protected String getVariantName() {
        return "uk";
    }
}
