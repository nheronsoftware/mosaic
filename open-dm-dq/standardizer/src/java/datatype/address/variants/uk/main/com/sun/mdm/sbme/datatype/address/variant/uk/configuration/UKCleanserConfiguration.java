package com.sun.mdm.sbme.datatype.address.variant.uk.configuration;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Bean;
import com.sun.inti.components.string.transform.CompoundStringTransformer;
import com.sun.inti.components.string.transform.SimpleRegexStringTransformer;
import com.sun.inti.components.string.transform.StringTransformer;
import com.sun.inti.components.string.transform.TrimStringTransformer;
import com.sun.inti.components.string.transform.UpperCaseStringTransformer;

@Configuration
public class UKCleanserConfiguration {

    @Bean
    public StringTransformer cleanser() {
        return new CompoundStringTransformer().add(new TrimStringTransformer()).
                add(new UpperCaseStringTransformer()).
                add(new SimpleRegexStringTransformer("\\|", " ", true)).
                add(new SimpleRegexStringTransformer("[.]+", "")).
                add(new SimpleRegexStringTransformer("[,]+", " ")).
                add(new SimpleRegexStringTransformer("\\s+", " ")).
                add(specialCharTransformer()).
                add(new SimpleRegexStringTransformer("#([0-9A-Z])", "# $1")).
                add(new SimpleRegexStringTransformer("(\\w)([-\\u2013])\\s+(\\d)", "$1$2$3")).
                add(new SimpleRegexStringTransformer("(\\d)\\s+([-\\u2013])(\\d)", "$1$2$3")).
                add(dashTransformer()).
                add(new SimpleRegexStringTransformer("[^\\w/\\u0027\\u0060\\u2018\\u2019]+$", ""));
    }

    private StringTransformer specialCharTransformer() {
        return new StringTransformer() {

            private final char[] specialChars = new char[]{
                ' ',
                '\u0023',
                '\u0026',
                39, // '\u0027',
                '\u002d',
                '\u002f',
                '\u0060',
                '\u007c',
                '\u2018',
                '\u2019',
                '\u2013'
            };
            private final char[] apostrophes = new char[]{
                39, // '\u0027' // '
                '\u0060', // `
                '\u2018', // �
                '\u2019', // �
            };

            public String transform(String string) {
                char[] chars = string.toCharArray();
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < chars.length; i++) {
                    if (Character.isLetterOrDigit(chars[i]) || isSpecial(chars[i])) {
                        builder.append(chars[i]);
                    } else {
                        if (hasApostrophe(chars, i)) {
                            builder.append(chars[i]);
                        }
                    }
                }
                return builder.toString();
            }

            private boolean isSpecial(char c) {
                for (char sc : specialChars) {
                    if (c == sc) {
                        return true;
                    }
                }
                return false;
            }

            private boolean hasApostrophe(final char[] chars, final int index) {
                if (index == 0 || index == chars.length - 1) {
                    return false;
                }
                for (final char apostrophe : apostrophes) {
                    if (chars[index] == apostrophe) {
                        return Character.isLetter(chars[index - 1]) && Character.isLetter(chars[index + 1]);
                    }
                }
                return false;
            }
        };
    }

    private StringTransformer dashTransformer() {
        return new StringTransformer() {

            Pattern dashPattern = Pattern.compile("\\s[-\\u2013]\\s");
            Pattern nonAlphaPattern = Pattern.compile("\\A[^\\p{Alpha}]");
            Pattern digitDashPattern = Pattern.compile("(\\d)\\s([-\\u2013])\\s(\\d)");
            Pattern spaceDashPattern = Pattern.compile("\\s[-\\u2013]");
            Pattern alphaDashPattern = Pattern.compile("(\\p{Alpha})\\s[-\\u2013]\\s(\\p{Alpha})");

            public String transform(String string) {
                if (this.dashPattern.matcher(string).find()) {
                    if (this.nonAlphaPattern.matcher(string).find()) {
                        Matcher digitDashMatcher = this.digitDashPattern.matcher(string);
                        if (digitDashMatcher.find()) {
                            string = digitDashMatcher.replaceAll("$1$2$3");
                        } else {
                            string = this.spaceDashPattern.matcher(string).replaceAll("");
                        }
                    } else {
                        // FIXME This substitution should produce only one space
                        string = this.alphaDashPattern.matcher(string).replaceAll("$1  $2");
                    }
                }
                return string;
            }
        };
    }
}
