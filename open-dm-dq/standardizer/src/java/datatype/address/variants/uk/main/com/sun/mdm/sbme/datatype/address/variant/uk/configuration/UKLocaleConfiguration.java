package com.sun.mdm.sbme.datatype.address.variant.uk.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Bean;
import com.sun.mdm.sbme.datatype.address.locale.AddressLocale;
import com.sun.mdm.sbme.datatype.address.locale.english.UKAddressLocale;

@Configuration
public class UKLocaleConfiguration {

    @Bean
    public AddressLocale locale() {
        return new UKAddressLocale();
    }
}
