package com.sun.mdm.sbme.datatype.address.variant.fr.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Bean;

import com.sun.mdm.sbme.datatype.address.locale.AddressLocale;
import com.sun.mdm.sbme.datatype.address.variant.fr.FRAddressLocale;

@Configuration
public class FRLocaleConfiguration {

    @Bean
    public AddressLocale locale() {
        return new FRAddressLocale();
    }
}
