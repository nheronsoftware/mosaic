package com.sun.mdm.sbme.datatype.address.variant.fr;

import static com.sun.mdm.sbme.datatype.address.pattern.InputTokenType.STREET_TYPE;

import com.sun.mdm.sbme.datatype.address.builder.InputTokenTypeMapper;
import com.sun.mdm.sbme.datatype.address.pattern.InputTokenType;

public class FrenchInputTokenTypeMapper implements InputTokenTypeMapper {

	public InputTokenType map(InputTokenType inputTokenType) {
		
		switch (inputTokenType) {
			case PREFIX_TYPE:
				return STREET_TYPE;
			default:
				return inputTokenType;
		}
	}
}
