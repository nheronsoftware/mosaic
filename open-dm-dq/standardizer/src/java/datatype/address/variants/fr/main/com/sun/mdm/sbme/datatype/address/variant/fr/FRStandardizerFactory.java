package com.sun.mdm.sbme.datatype.address.variant.fr;

import com.sun.mdm.sbme.AnnotationContextStandardizerFactory;

public class FRStandardizerFactory extends AnnotationContextStandardizerFactory {

    @Override
    protected String[] getConfigurationLocations() {
        return new String[]{"com.sun.mdm.sbme.configuration",
                    "com.sun.mdm.sbme.datatype.address.configuration",
                    "com.sun.mdm.sbme.datatype.address.variant.fr.configuration",
                  /*"/com/sun/mdm/sbme/configuration/*Configuration.class",
                    "/com/sun/mdm/sbme/datatype/address/configuration/*Configuration.class",
                    "/com/sun/mdm/sbme/datatype/address/variant/fr/configuration/*Configuration.class",*/};
    }
}
