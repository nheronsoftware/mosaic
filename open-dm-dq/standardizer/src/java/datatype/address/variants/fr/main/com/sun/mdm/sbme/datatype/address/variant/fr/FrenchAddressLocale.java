/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.mdm.sbme.datatype.address.variant.fr;

import com.sun.mdm.sbme.datatype.address.locale.AbstractAddressLocale;

/**
 *
 *  @author ricardo.rocha@sun.com jlin@netgensoftware.com
 */
public abstract class FrenchAddressLocale extends AbstractAddressLocale {
    private final static String[] ordinalPrefixes = new String[] { "1ER", "�ME", "EME", "E ", };
    public boolean isOrdinalPrefix(final String string) {
        for (final String element : ordinalPrefixes) {
            if (string.startsWith(element)) {
                return true;
            }
        }
        return false;
    }

    // FIXME Ordinal suffix seems to be missing for French...
    public String getOrdinalSuffix(final String number) {
        return "";
    }

    private static final String[] cardinalPoints = { "N", "S", "E", "O" };

    public final String[] getCardinalPoints() {
        return cardinalPoints;
    }
    
    public String getCardinalPointName(String point) {
        if ("N".equalsIgnoreCase(point)) {
            return "Nord";
        }
        if ("S".equalsIgnoreCase(point)) {
            return "Sud";
        }
        if ("E".equalsIgnoreCase(point)) {
            return "Est";
        }
        if ("O".equalsIgnoreCase(point)) {
            return "Ouest";
        }
        
        throw new IllegalArgumentException("Invalid cardinal point: '" + point + "'");
    }
}
