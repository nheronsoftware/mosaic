package com.sun.mdm.sbme.datatype.address.variant.fr.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Bean;
import com.sun.mdm.sbme.datatype.address.configuration.PatternRegistryConfiguration;
import com.sun.mdm.sbme.datatype.address.pattern.PatternRegistry;

@Configuration
public class FRPatternRegistryConfiguration extends PatternRegistryConfiguration {

    @Override
    @Bean
    public PatternRegistry patternRegistry() {
        return super.patternRegistry();
    }

    @Override
    protected String getVariantName() {
        return "fr";
    }
}
