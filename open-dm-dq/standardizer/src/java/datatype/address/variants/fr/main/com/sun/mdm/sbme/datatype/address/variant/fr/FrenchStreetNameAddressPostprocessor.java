package com.sun.mdm.sbme.datatype.address.variant.fr;

import static com.sun.mdm.sbme.datatype.address.Address.MATCHING_STREET_NAME;
import static com.sun.mdm.sbme.datatype.address.Address.ORIGINAL_STREET_NAME;
import static com.sun.mdm.sbme.datatype.address.Address.STORAGE_STREET_NAME;

import com.sun.inti.components.record.Record;
import com.sun.mdm.sbme.datatype.address.Address;
import com.sun.mdm.sbme.datatype.address.builder.postprocessor.AddressPostprocessor;

public class FrenchStreetNameAddressPostprocessor extends AddressPostprocessor {
	/*
     * If the usage flag is C (commercial only), and only the street name field
     * was loaded, then move ALL_FIELDS to the building's name. Also, check for
     * numerics and ordinal numbers in the street name field and expand if
     * necessary.
     */
    @Override
    protected void postprocess(Address address, Record context) {
    	String originalStreetName = address.get(ORIGINAL_STREET_NAME);
    	String matchingStreetName = address.get(MATCHING_STREET_NAME);
    	String storageStreetName = address.get(STORAGE_STREET_NAME);

        if (matchingStreetName != null && !matchingStreetName.equalsIgnoreCase(originalStreetName)) {
            if (getLocale().isCardinalPointAbbreviation(matchingStreetName)) {
            	matchingStreetName = getLocale().getCardinalPointName(matchingStreetName);
                address.set(MATCHING_STREET_NAME, matchingStreetName);
            }
        }

        // Check for spelled out OT's in the storage and matching names
        if (originalStreetName != null && this.isOrdinalTypeValue(originalStreetName)) {
        	storageStreetName = matchingStreetName;
        	address.set(STORAGE_STREET_NAME, storageStreetName);
        }
    }
    
    /*
     * Checks strings for "spelled out" ordinal types, eg. "fifty second" is
     * changed to its numeric ordinal type: 52nd.
     */
    // FIXME This method is ineffective for languages other than English
    private boolean isOrdinalTypeValue(final String inputStreetName) {
        final String[][] tens;
        final String[][] units;
        
            tens = new String[][] {
                {"VINGT", "2"},
                {"TRENTE", "3"},
                {"QUARANTE", "4"},
                {"CINQUANTE", "5"},
                {"SOIXANTE", "6"},
                {"SOIXANTE-DIX", "7"},
                {"QUATRE-VINGT", "8"},
                {"QUATRE-VINGT=DIX", "9"}
            };
            units = new String[][] {
                {"PREMIER", "1er"},
                {"DEUXIEME", "2eme"},
                {"TROISIEME", "3eme"},
                {"QUATRIEME", "4eme"},
                {"CINQUIEME", "5eme"},
                {"SIXIEME", "6eme"},
                {"SEPTIEME", "7eme"},
                {"HUITIEME", "8eme"},
                {"NEUVIEME", "9eme"}
            };

        // Change FIFTY SECOND to 52nd, THIRTY FIRST to 31st, etc.
        for (int i = 0; i < tens.length; i++) {
            if (inputStreetName.startsWith(tens[i][0])) {
                if (inputStreetName.length() <= tens[i][0].length()) {
                    continue;
                }

                int pos = tens[i][0].length();
                if (inputStreetName.charAt(pos - 1) == '-') { // French
                    pos++;
                }
                String mUnits = inputStreetName.substring(pos);
                for (int j = 0; j < units.length; j++) {
                    if (mUnits.startsWith(units[j][0])) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
