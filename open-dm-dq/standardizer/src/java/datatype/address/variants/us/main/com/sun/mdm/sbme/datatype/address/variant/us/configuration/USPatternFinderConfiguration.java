package com.sun.mdm.sbme.datatype.address.variant.us.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Bean;
import org.springframework.beans.factory.annotation.Autowired;
import com.sun.mdm.sbme.datatype.address.pattern.AddressOutputPatternBuilder;
import com.sun.mdm.sbme.datatype.address.pattern.AddressOutputPatternScorer;
import com.sun.mdm.sbme.datatype.address.pattern.InputTokenType;
import com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType;
import com.sun.mdm.sbme.datatype.address.pattern.PatternRegistry;
import com.sun.mdm.sbme.pattern.DefaultPatternFinder;
import com.sun.mdm.sbme.pattern.OutputPatternPostprocessor;
import com.sun.mdm.sbme.pattern.PatternFinder;

@Configuration
public class USPatternFinderConfiguration {

    @Bean
    public PatternFinder<InputTokenType, OutputTokenType> patternFinder() {
        DefaultPatternFinder<InputTokenType, OutputTokenType> patternFinder = new DefaultPatternFinder<InputTokenType, OutputTokenType>();
        patternFinder.setOutputPatternBuilder(new AddressOutputPatternBuilder(patternRegistry));
        patternFinder.setOutputPatternScorer(addressOutputPatternScorer());
        patternFinder.setPostprocessors(postprocessors());
        return patternFinder;
    }
    @Autowired
    private PatternRegistry patternRegistry;

    private AddressOutputPatternScorer addressOutputPatternScorer() {
        AddressOutputPatternScorer addressPatternScorer = new AddressOutputPatternScorer();
        addressPatternScorer.setUnmatchedPatternFactor(20d);
        addressPatternScorer.setPatternWeight(0.4d);
        return addressPatternScorer;
    }

    private OutputPatternPostprocessor<InputTokenType, OutputTokenType>[] postprocessors() {
        return null;
    }
}
