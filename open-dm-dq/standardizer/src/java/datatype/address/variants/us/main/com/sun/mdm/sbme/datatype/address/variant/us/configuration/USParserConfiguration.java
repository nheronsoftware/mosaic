package com.sun.mdm.sbme.datatype.address.variant.us.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Bean;
import org.springframework.beans.factory.annotation.Autowired;
import com.sun.mdm.sbme.datatype.address.locale.AddressLocale;
import com.sun.mdm.sbme.datatype.address.parser.AddressParser;
import com.sun.mdm.sbme.parser.Parser;

@Configuration
public class USParserConfiguration {

    @Bean
    public Parser parser() {
        return new AddressParser(locale);
    }
    @Autowired
    private AddressLocale locale;
}
