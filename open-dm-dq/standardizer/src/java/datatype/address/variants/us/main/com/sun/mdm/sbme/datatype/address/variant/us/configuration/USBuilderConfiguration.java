package com.sun.mdm.sbme.datatype.address.variant.us.configuration;

import static com.sun.mdm.sbme.datatype.address.builder.AbbreviationOption.FULL_NAME;
import static com.sun.mdm.sbme.datatype.address.builder.AbbreviationOption.STANDARD_ABBREVIATION;
import static com.sun.mdm.sbme.datatype.address.builder.AbbreviationOption.USPS_ABBREVIATION;
import static com.sun.mdm.sbme.datatype.address.builder.CombinationOption.SPACE;
import static com.sun.mdm.sbme.datatype.address.builder.CombinationOption.SQUASH;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.BUILDING_NUMBER;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.BUILDING_NUMBER_PREFIX;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.BUILDING_NUMBER_SUFFIX;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.CONJUNCTION;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.EXTRA_INFORMATION;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.FIRST_HOUSE_NUMBER;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.HOUSE_NUMBER;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.HOUSE_NUMBER_PREFIX;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.HOUSE_NUMBER_SUFFIX;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.POST_OFFICE_BOX_DESCRIPTOR;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.POST_OFFICE_BOX_IDENTIFIER;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.PROPERTY_BUILDING_NAME;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.PROPERTY_PREFIX_DIRECTION;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.PROPERTY_SUFFIX_DIRECTION;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.PROPERTY_TYPE_PREFIX;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.PROPERTY_TYPE_SUFFIX;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.RURAL_ROUTE_DESCRIPTOR;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.RURAL_ROUTE_IDENTIFIER;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.SECOND_HOUSE_NUMBER;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.SECOND_HOUSE_NUMBER_PREFIX;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.SECOND_STREET_NAME;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.SECOND_STREET_NAME_SUFFIX_TYPE_S2;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.SECOND_STREET_NAME_SUFFIX_TYPE_T2;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.STREET_NAME;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.STREET_NAME_EXTENSION_INDEX;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.STREET_NAME_PREFIX_DIRECTION;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.STREET_NAME_PREFIX_TYPE;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.STREET_NAME_SUFFIX_DIRECTION;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.STREET_NAME_SUFFIX_TYPE;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.STRUCTURE_DESCRIPTOR;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.STRUCTURE_IDENTIFIER;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.WITHIN_STRUCTURE_DESCRIPTOR;
import static com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType.WITHIN_STRUCTURE_IDENTIFIER;

import java.util.LinkedList;
import java.util.List;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Bean;
import com.sun.mdm.sbme.builder.DefaultStandardizedRecordBuilder;
import com.sun.mdm.sbme.builder.OutputTokenTypeHandler;
import com.sun.mdm.sbme.builder.StandardizedRecordBuilder;
import com.sun.mdm.sbme.builder.postprocessor.StandardizedRecordBuilderPostprocessor;
import com.sun.mdm.sbme.builder.preprocessor.StandardizedRecordBuilderPreprocessor;
import com.sun.mdm.sbme.datatype.address.builder.AddressBuildingComponent;
import com.sun.mdm.sbme.datatype.address.builder.AddressFactory;
import com.sun.mdm.sbme.datatype.address.builder.postprocessor.BuildingNumberAddressPostprocessor;
import com.sun.mdm.sbme.datatype.address.builder.postprocessor.EnglishDirectionalSuffixAddressPostprocessor;
import com.sun.mdm.sbme.datatype.address.builder.postprocessor.EnglishPostOfficeBoxAddressPostprocessor;
import com.sun.mdm.sbme.datatype.address.builder.postprocessor.EnglishStreetNameAddressPostprocessor;
import com.sun.mdm.sbme.datatype.address.builder.postprocessor.EnglishWithinStructureAddressPostprocessor;
import com.sun.mdm.sbme.datatype.address.builder.postprocessor.HouseNumberAddressPostprocessor;
import com.sun.mdm.sbme.datatype.address.builder.preprocessor.ContextPropertyAddressPreprocessor;
import com.sun.mdm.sbme.datatype.address.builder.preprocessor.NumericFieldAddressPreprocessor;
import com.sun.mdm.sbme.datatype.address.builder.preprocessor.StreetNamePrefixAddressPreprocessor;
import com.sun.mdm.sbme.datatype.address.builder.property.BuildingNumberAddressPropertyBuilder;
import com.sun.mdm.sbme.datatype.address.builder.property.ExtraInformationAddressPropertyBuilder;
import com.sun.mdm.sbme.datatype.address.builder.property.FirstHouseNumberAddressPropertyBuilder;
import com.sun.mdm.sbme.datatype.address.builder.property.NullAddressPropertyBuilder;
import com.sun.mdm.sbme.datatype.address.builder.property.OriginalSecondStreetNameAddressPropertyBuilder;
import com.sun.mdm.sbme.datatype.address.builder.property.PostOfficeBoxIdentifierAddressPropertyBuilder;
import com.sun.mdm.sbme.datatype.address.builder.property.PropertyBuildingNameAddressPropertyBuilder;
import com.sun.mdm.sbme.datatype.address.builder.property.RuralRouteIdentifierAddressPropertyBuilder;
import com.sun.mdm.sbme.datatype.address.builder.property.SecondHouseNumberAddressPropertyBuilder;
import com.sun.mdm.sbme.datatype.address.builder.property.SpelledNumberAddressPropertyBuilder;
import com.sun.mdm.sbme.datatype.address.builder.property.StreetNameAddressPropertyBuilder;
import com.sun.mdm.sbme.datatype.address.builder.property.StreetNamePrefixTypeAddressPropertyBuilder;
import com.sun.mdm.sbme.datatype.address.builder.property.StreetNameSuffixDirectionAddressPropertyBuilder;
import com.sun.mdm.sbme.datatype.address.configuration.BuilderConfiguration;
import com.sun.mdm.sbme.datatype.address.pattern.InputTokenType;
import com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType;

@Configuration
public class USBuilderConfiguration extends BuilderConfiguration {

    @Bean
    public StandardizedRecordBuilder<InputTokenType, OutputTokenType> builder() {
        DefaultStandardizedRecordBuilder<InputTokenType, OutputTokenType> builder = new DefaultStandardizedRecordBuilder<InputTokenType, OutputTokenType>();
        builder.setStandardizedRecordFactory(new AddressFactory());
        builder.setPreprocessors(preprocessors());
        builder.setOutputTokenTypeHandlers(outputTokenTypeHandlers());
        builder.setPostprocessors(postprocessors());
        return builder;
    }

    private List<StandardizedRecordBuilderPreprocessor<InputTokenType, OutputTokenType>> preprocessors() {
        List<StandardizedRecordBuilderPreprocessor<InputTokenType, OutputTokenType>> preprocessors = new LinkedList<StandardizedRecordBuilderPreprocessor<InputTokenType, OutputTokenType>>();
        preprocessors.add(new NumericFieldAddressPreprocessor());
        preprocessors.add(new StreetNamePrefixAddressPreprocessor());
        preprocessors.add(new ContextPropertyAddressPreprocessor<InputTokenType, OutputTokenType>("buildingNumber"));
        preprocessors.add(new ContextPropertyAddressPreprocessor<InputTokenType, OutputTokenType>("buildingNumberPrefix"));
        preprocessors.add(new ContextPropertyAddressPreprocessor<InputTokenType, OutputTokenType>("buildingNumberSuffix"));
        return preprocessors;
    }

    private List<OutputTokenTypeHandler<InputTokenType, OutputTokenType>> outputTokenTypeHandlers() {
        List<OutputTokenTypeHandler<InputTokenType, OutputTokenType>> handlers = new LinkedList<OutputTokenTypeHandler<InputTokenType, OutputTokenType>>();

        handlers.add(handler(new FirstHouseNumberAddressPropertyBuilder(), FIRST_HOUSE_NUMBER,
                HOUSE_NUMBER));
        handlers.add(handler(new SecondHouseNumberAddressPropertyBuilder(), SECOND_HOUSE_NUMBER));
        handlers.add(simpleHandler(STANDARD_ABBREVIATION, SPACE, "HouseNumberPrefix", HOUSE_NUMBER_PREFIX));
        handlers.add(simpleHandler(STANDARD_ABBREVIATION, SPACE, "SecondHouseNumberPrefix", SECOND_HOUSE_NUMBER_PREFIX));
        handlers.add(simpleHandler("HouseNumberSuffix", HOUSE_NUMBER_SUFFIX));
        handlers.add(simpleHandler("StreetNamePrefDirection", STREET_NAME_PREFIX_DIRECTION));
        handlers.add(handler(new StreetNamePrefixTypeAddressPropertyBuilder(), STREET_NAME_PREFIX_TYPE));
        handlers.add(handler(new StreetNameAddressPropertyBuilder(), STREET_NAME));
        handlers.add(simpleHandler("StreetNameSufType", STREET_NAME_SUFFIX_TYPE));
        handlers.add(simpleHandler(new StreetNameSuffixDirectionAddressPropertyBuilder(), "StreetNameSufDirection", STREET_NAME_SUFFIX_DIRECTION));
        handlers.add(simpleHandler("StreetNameExtensionIndex", STREET_NAME_EXTENSION_INDEX));
        handlers.add(simpleHandler(USPS_ABBREVIATION, "WithinStructDescript", WITHIN_STRUCTURE_DESCRIPTOR));
        handlers.add(simpleHandler(new SpelledNumberAddressPropertyBuilder(), "WithinStructIdentif", WITHIN_STRUCTURE_IDENTIFIER));
        handlers.add(contextHandler(new BuildingNumberAddressPropertyBuilder(), SQUASH, "buildingNumber", BUILDING_NUMBER));
        handlers.add(contextHandler(STANDARD_ABBREVIATION, "buildingNumberPrefix", BUILDING_NUMBER_PREFIX));
        handlers.add(contextHandler("buildingNumberSuffix", BUILDING_NUMBER_SUFFIX));
        handlers.add(simpleHandler("PropDesPrefDirection", PROPERTY_PREFIX_DIRECTION));
        handlers.add(simpleHandler("PropDesPrefType", PROPERTY_TYPE_PREFIX));
        handlers.add(handler(new PropertyBuildingNameAddressPropertyBuilder(), PROPERTY_BUILDING_NAME));
        handlers.add(simpleHandler("PropDesSufType", PROPERTY_TYPE_SUFFIX));
        handlers.add(simpleHandler("PropDesSufDirection", PROPERTY_SUFFIX_DIRECTION));
        handlers.add(simpleHandler(FULL_NAME, "CenterDescript", STRUCTURE_DESCRIPTOR));
        handlers.add(simpleHandler(FULL_NAME, "CenterIdentif", STRUCTURE_IDENTIFIER));
        handlers.add(simpleHandler("RuralRouteDescript", RURAL_ROUTE_DESCRIPTOR));
        handlers.add(handler(new RuralRouteIdentifierAddressPropertyBuilder(), RURAL_ROUTE_IDENTIFIER));
        handlers.add(simpleHandler(STANDARD_ABBREVIATION, "BoxDescript", POST_OFFICE_BOX_DESCRIPTOR));
        handlers.add(handler(new PostOfficeBoxIdentifierAddressPropertyBuilder(), POST_OFFICE_BOX_IDENTIFIER));
        handlers.add(handler(new ExtraInformationAddressPropertyBuilder(), EXTRA_INFORMATION));
        handlers.add(handler(new NullAddressPropertyBuilder(), CONJUNCTION));
        handlers.add(simpleHandler("SecondStreetNameSufType", SECOND_STREET_NAME_SUFFIX_TYPE_S2,
                SECOND_STREET_NAME_SUFFIX_TYPE_T2));
        handlers.add(handler(new OriginalSecondStreetNameAddressPropertyBuilder(), SECOND_STREET_NAME));

        return handlers;
    }

    private List<StandardizedRecordBuilderPostprocessor<OutputTokenType>> postprocessors() {
        List<StandardizedRecordBuilderPostprocessor<OutputTokenType>> postprocessors = new LinkedList<StandardizedRecordBuilderPostprocessor<OutputTokenType>>();
        postprocessors.add(postprocessor(new EnglishPostOfficeBoxAddressPostprocessor()));
        postprocessors.add(postprocessor(new HouseNumberAddressPostprocessor()));
        postprocessors.add(postprocessor(new BuildingNumberAddressPostprocessor()));
        postprocessors.add(postprocessor(new EnglishDirectionalSuffixAddressPostprocessor()));
        postprocessors.add(postprocessor(new EnglishWithinStructureAddressPostprocessor()));
        postprocessors.add(postprocessor(new EnglishStreetNameAddressPostprocessor()));
        return postprocessors;
    }

    private StandardizedRecordBuilderPostprocessor<OutputTokenType> postprocessor(StandardizedRecordBuilderPostprocessor<OutputTokenType> postprocessor) {
        AddressBuildingComponent component = (AddressBuildingComponent) postprocessor;
        component.setLocale(this.locale);
        component.setClueRegistry(this.clueRegistry);
        component.setMasterClueRegistry(this.masterClueRegistry);
        return postprocessor;
    }
}
