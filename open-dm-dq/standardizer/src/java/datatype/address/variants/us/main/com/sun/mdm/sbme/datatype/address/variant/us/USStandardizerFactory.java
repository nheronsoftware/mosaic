package com.sun.mdm.sbme.datatype.address.variant.us;

import com.sun.mdm.sbme.AnnotationContextStandardizerFactory;

public class USStandardizerFactory extends AnnotationContextStandardizerFactory {

    @Override
    public String[] getConfigurationLocations() {
        return new String[]{"com.sun.mdm.sbme.configuration",
                            "com.sun.mdm.sbme.datatype.address.configuration",
                            "com.sun.mdm.sbme.datatype.address.variant.us.configuration",
                          /*"/com/sun/mdm/sbme/configuration/*Configuration.class",
                            "/com/sun/mdm/sbme/datatype/address/configuration/*Configuration.class",
                            "/com/sun/mdm/sbme/datatype/address/variant/us/configuration/*Configuration.class",*/
                            };
    }
}
