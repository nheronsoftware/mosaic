package com.sun.mdm.sbme.datatype.address.variant.au.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.ApplicationContext;
import com.sun.inti.components.string.transform.StringTransformer;
import com.sun.mdm.sbme.DefaultStandardizer;
import com.sun.mdm.sbme.Standardizer;
import com.sun.mdm.sbme.builder.StandardizedRecordBuilder;
import com.sun.mdm.sbme.datatype.address.pattern.InputTokenType;
import com.sun.mdm.sbme.datatype.address.pattern.OutputTokenType;
import com.sun.mdm.sbme.normalizer.Normalizer;
import com.sun.mdm.sbme.parser.Parser;
import com.sun.mdm.sbme.pattern.PatternFinder;

@Configuration
public abstract class AUStandardizerConfiguration {

    @Bean
    public Standardizer standardizer() {
        DefaultStandardizer<InputTokenType, OutputTokenType> standardizer = new DefaultStandardizer<InputTokenType, OutputTokenType>();

        standardizer.setCleanser(cleanser());
        standardizer.setParser(parser());
        standardizer.setNormalizer(normalizer());
        standardizer.setPatternFinder(patternFinder());
        standardizer.setBuilder(builder());

        standardizer.setStandardizationType("Address");
        standardizer.setStandardizationProperty(OutputTokenType.EXTRA_INFORMATION.getAlias());

        return standardizer;
    }

    public static void main(String[] args) throws Exception {
        //ApplicationContext xmlContext = new FileSystemXmlApplicationContext(args[0]);
        AnnotationConfigApplicationContext annotationContext = new AnnotationConfigApplicationContext("/com/sun/mdm/sbme/datatype/address/configuration/au/*Configuration.class");
        annotationContext.getBean("standardizer");
    }

    @Bean
    public abstract StringTransformer cleanser();

    @Bean
    public abstract Parser parser();

    @Bean
    public abstract Normalizer<InputTokenType> normalizer();

    @Bean
    public abstract PatternFinder<InputTokenType, OutputTokenType> patternFinder();

    @Bean
    public abstract StandardizedRecordBuilder<InputTokenType, OutputTokenType> builder();
}
