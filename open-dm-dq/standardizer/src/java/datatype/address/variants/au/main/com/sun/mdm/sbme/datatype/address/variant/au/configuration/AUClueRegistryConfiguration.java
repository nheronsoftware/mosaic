package com.sun.mdm.sbme.datatype.address.variant.au.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Bean;
import com.sun.mdm.sbme.clue.ClueRegistry;
import com.sun.mdm.sbme.datatype.address.configuration.AddressClueRegistryConfiguration;
import com.sun.mdm.sbme.datatype.address.pattern.InputTokenType;

@Configuration
public class AUClueRegistryConfiguration extends AddressClueRegistryConfiguration {

    @Bean
    public ClueRegistry<InputTokenType> clueRegistry() throws Exception {
        return new ClueRegistry(super.getClues());
    }

    @Override
    protected String getVariantName() {
        return "au";
    }
}
