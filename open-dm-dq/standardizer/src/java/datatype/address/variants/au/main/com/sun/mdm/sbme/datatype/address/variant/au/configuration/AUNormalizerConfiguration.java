package com.sun.mdm.sbme.datatype.address.variant.au.configuration;

import java.util.LinkedList;
import java.util.List;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Bean;
import com.sun.mdm.sbme.clue.ClueRegistry;
import com.sun.mdm.sbme.datatype.address.locale.AddressLocale;
import com.sun.mdm.sbme.datatype.address.normalizer.populator.DefaultNonNullCluePopulator;
import com.sun.mdm.sbme.datatype.address.normalizer.populator.DefaultNullCluePopulator;
import com.sun.mdm.sbme.datatype.address.normalizer.populator.EnglishOrdinalPopulator;
import com.sun.mdm.sbme.datatype.address.normalizer.populator.NumericComboPopulator;
import com.sun.mdm.sbme.datatype.address.normalizer.postprocessor.DefaultNormalizationPostprocessor;
import com.sun.mdm.sbme.datatype.address.pattern.InputTokenType;
import com.sun.mdm.sbme.normalizer.DefaultNormalizer;
import com.sun.mdm.sbme.normalizer.Normalizer;
import com.sun.mdm.sbme.normalizer.populator.ClueWordPopulator;

@Configuration
public abstract class AUNormalizerConfiguration {

    @Bean
    public Normalizer<InputTokenType> normalizer() {
        DefaultNormalizer<InputTokenType> normalizer = new DefaultNormalizer<InputTokenType>();
        normalizer.setClueRegistry(clueRegistry());
        normalizer.setClueWordTokenPopulators(cluePopulators());
        normalizer.setNormalizationPostprocessor(new DefaultNormalizationPostprocessor());
        return normalizer;
    }

    @Bean
    public abstract ClueRegistry<InputTokenType> clueRegistry();

    @Bean
    public abstract AddressLocale locale();

    private List<ClueWordPopulator<InputTokenType>> cluePopulators() {
        List<ClueWordPopulator<InputTokenType>> cluePopulators = new LinkedList<ClueWordPopulator<InputTokenType>>();
        cluePopulators.add(new DefaultNullCluePopulator());
        cluePopulators.add(new EnglishOrdinalPopulator());
        cluePopulators.add(new NumericComboPopulator(locale()));
        cluePopulators.add(new DefaultNonNullCluePopulator());
        return cluePopulators;
    }
}
