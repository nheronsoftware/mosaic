package com.sun.mdm.sbme.datatype.address.variant.au.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Bean;
import com.sun.mdm.sbme.datatype.address.locale.AddressLocale;
import com.sun.mdm.sbme.datatype.address.locale.english.AUAddressLocale;

@Configuration
public class AULocaleConfiguration {

    @Bean
    public AddressLocale locale() {
        return new AUAddressLocale();
    }
}
