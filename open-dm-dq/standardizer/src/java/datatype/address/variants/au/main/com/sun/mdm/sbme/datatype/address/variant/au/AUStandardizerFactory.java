package com.sun.mdm.sbme.datatype.address.variant.au;

import com.sun.mdm.sbme.AnnotationContextStandardizerFactory;

public class AUStandardizerFactory extends AnnotationContextStandardizerFactory {

    @Override
    protected String[] getConfigurationLocations() {
        return new String[]{"com.sun.mdm.sbme.configuration",
                    "com.sun.mdm.sbme.datatype.address.configuration",
                    "com.sun.mdm.sbme.datatype.address.variant.au.configuration",
                  /*"/com/sun/mdm/sbme/configuration/*Configuration.class",
                    "/com/sun/mdm/sbme/datatype/address/configuration/*Configuration.class",
                    "/com/sun/mdm/sbme/datatype/address/variant/au/configuration/*Configuration.class",*/};
    }
}
