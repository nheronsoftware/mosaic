/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
*/ 
package com.sun.mdm.standardizer.introspector;

import java.io.File;

/**
 * This interface exposes the properties of a standardization deployment unit
 * including descriptive and filesystem-level information.
 *
 * @author Ricardo Rocha (ricardo.rocha@sun.com)
 */
public interface Descriptor {
    /**
     * Return the descriptor's name
     * @return the descriptor's name
     */
    public String getName();
    
    /**
     * Return the descriptor's description
     * @return the descriptor's description
     */
    public String getDescription();
    
    /**
     * Return the array of filenames accessible throught his descriptor
     * @return the array of filenames accessible throught his descriptor
     */
    public String[] getResourceFilenames();
    
    /**
     * Return the <code>java.io.File</code> corresponding to the given
     * <code>name</code>
     * 
     * @param name The filename
     * @return The <code>File</code> object associated with the given name
     * @throws IntrospectionException if the named file does not exist or cannot be read/written
     */
    public File getResourceFile(String name) throws IntrospectionException;
}
