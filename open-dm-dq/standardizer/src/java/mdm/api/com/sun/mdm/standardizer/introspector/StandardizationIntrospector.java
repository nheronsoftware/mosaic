/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
*/ 
package com.sun.mdm.standardizer.introspector;

import java.io.File;
import java.util.zip.ZipFile;

import com.sun.inti.container.ContainerException;

/**
 * This interface exposes the services and lifecycle events applicable to the
 * standardization introspector.
 *
 * @author Ricardo Rocha (ricardo.rocha@sun.com)
 */
public interface StandardizationIntrospector {
    /**
     * Set the repository file to be used for standardization introspection. This can be an
     * empty directory to be populated by means of the <code>deploy</code> methods. If it is
     * an existing directory then it may contain already deployed data types and variants
     * that will be introspected by this method.
     * 
     * @param repositoryDirectory The directory containing the data types and variants for this introspector
     * @throws IntrospectionException If the given directory cannot be created or accessed or 
     *                                a [re]deployment error occurs during introspection
     */
    public void setRepositoryDirectory(File repositoryDirectory) throws IntrospectionException;
    
    /**
     * Deploy a zip file containing either a data type or a variant deployment unit. The returned value
     * can be tested by means of <code>instanceof</code> to determine which of the two cases applies.
     * 
     * @param deploymentFile The zip file containing the deployment unit
     * @return The descriptor (either <code>DataTypeDescriptor</code> or
     *         <code>VariantDescriptor</code>) built from the deployment file contents
     * @throws IntrospectionException If the deployment file contents or structure are invalid
     *                                or if the deployment descriptors are missing or invalid
     */
    public Descriptor deploy(ZipFile deploymentFile)  throws IntrospectionException;
    
    /**
     * Deploy a data type from a deployment file.
     * 
     * @param deploymentFile The zip file containing the data type deployment unit
     * @return The data type descriptor built from the introspected directory
     * @throws IntrospectionException If the zip file contents or structure are invalid
     *                                or if any of the contained deployment descriptors
     *                                are missing or invalid
     */
    public DataTypeDescriptor deployDataType(ZipFile deploymentFile) throws IntrospectionException;
    
    /**
     * Deploye a variant from a deployment file.
     * 
     * @param dataTypeName The name of the data type under which to deploy the variant 
     * @param deploymentFile The zip file containing the deployment unit
     * @return The variant descriptor built from the introspected directory
     * @throws IntrospectionException If the zip file contents or structure are invalid
     *                                or if any of the contained deployment descriptors
     *                                are missing or invalid
     */
    public VariantDescriptor deployVariant(String dataTypeName, ZipFile deploymentFile) throws IntrospectionException;
    
    /**
     * Return the array of data type descriptors currently deployed onto the introspector
     * @return the array of data type descriptors currently deployed onto the introspector
     */
    public DataTypeDescriptor[] getDataTypes();
    
    /**
     * Return a specific data type descriptor given the data type name
     * 
     * @param dataTypeName the name of the data type
     * @return the data type corresponding to the given name
     * @throws IntrospectionException if no data type with the given name can be found
     */
    public DataTypeDescriptor getDataType(String dataTypeName) throws IntrospectionException;
    
    /**
     * Remove a complete data type from the instrospector's deployment directory. If the
     * data type contains one or more deployed variants they will be recursively deleted.
     * 
     * @param dataTypeName The name of the data type to undeploy
     * @throws IntrospectionException If no data type exists for the given name
     */
    public void undeploy(String dataTypeName) throws IntrospectionException;
    
    /**
     * Remove a variant givern its data type and variant names.
     * 
     * @param dataTypeName The name of the data type containing the variant
     * @param variantName The name of the variant to remove
     * @throws IntrospectionException If either the data type or variant names correspond
     *                                to previously deployed units
     */
    public void undeploy(String dataTypeName, String variantName) throws IntrospectionException;
    
    /**
     * Create a zip file on the given destination that contains the entirety of this
     * introspector's repository directory. The given file must be either non-existent
     * or it must point to a regular file. Intermediate directories will be created
     * as needed.
     * 
     * @param zipDestination the file to write the zipped image to
     * @throws ContainerException If the destination is a directory or its file cannot be written to
     */
    public void takeSnapshot(File zipDestination) throws ContainerException;
}
