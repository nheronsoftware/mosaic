/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
*/ 
package com.sun.mdm.standardizer.introspector;

import com.sun.mdm.standardizer.StandardizationException;

/**
 * This class specializes <code>StandardizationException</code> to describe
 * error conditions detected by the standardization introspector.
 *
 * @author Ricardo Rocha (ricardo.rocha@sun.com)
 */
public class IntrospectionException extends StandardizationException {
    /**
     * The serialization id.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The message-based constructor
     * 
     * @param message the error message
     */
    public IntrospectionException(String message) {
        super(message);
    }

    /**
     * The message/exception constructor.
     * @param message the error message
     * @param cause the source exception
     */
    public IntrospectionException(String message, Throwable cause) {
        super(message, cause);
    }
}
