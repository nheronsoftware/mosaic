/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
*/ 
package com.sun.mdm.standardizer;

import java.util.List;

/**
 * This interface exposes the basic operation performed by a standardizer: parse an input string and
 * produce a list of standardized records reflecting its structure and contents.
 * 
 * <i>Standardization</i> is an operation whereby an input string is parsed and its constituents are
 * identified so that they are recognized as belonging to an <i>input field type</i> and are assigned
 * to an <i>output field type</i> depending on their occurrence position within the input string.
 *
 * @author Ricardo Rocha (ricardo.rocha@sun.com)
 */
public interface Standardizer  {
    /**
     * Standardize the given input string and produce a list of standardized records expressing its
     * structure. Note that a list of standardized records -rather than a single record- is returned
     * to account for the cases where more than one occurrence of the record's type is present in the
     * string. This happens when multiple logical records occur in the string separated by conjunctions
     * such as <i>and/or</i> or <i>aka</i>.
     *   
     * @param record The input string to be standardized
     * @return The list of standardized records extracted from the input string
     */
	public List<StandardizedRecord> standardize(String record);
}
