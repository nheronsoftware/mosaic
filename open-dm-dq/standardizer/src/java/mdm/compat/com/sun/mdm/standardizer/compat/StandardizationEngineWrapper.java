/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
*/ 
package com.sun.mdm.standardizer.compat;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.java.hulp.i18n.Logger;

import com.stc.sbme.api.SbmeStandRecord;
import com.sun.inti.components.util.ClassUtils;
import com.sun.mdm.standardizer.Localizer;
import com.sun.mdm.standardizer.StandardizationEngine;
import com.sun.mdm.standardizer.StandardizedRecord;
 
/**
 * Compatibility class that exposes the legacy <code>stc_sbme</code> standardization engine interface.
 * This class wraps a <code>com.sun.mdm.standardizer.StandardizedRecord</code> but exposes the
 * interface defined by <code>com.stc.sbme.api.SbmeStandardizationEngine</code>.
 */
public class StandardizationEngineWrapper {
    /**
     * The logger for this class.
     */
    public static final Logger logger = Logger.getLogger(StandardizationEngineWrapper.class.getName());
    /**
     * The standardization localizer.
     */
    private static final Localizer localizer = Localizer.getInstance();

    /**
     * The wrapped standardization engine.
     */
    private static StandardizationEngine standardizationEngine;
    private static final int SHUTDOWN = 0;
    private static final int INITIALIZED = 1;
    private static int state = SHUTDOWN;

    /**
     * Default standardization engine wrapper constructor. This constructor loads its wrapped
     * standardization engine instance using Java's standard service loading mechanism.
     * <br/>
     * NOTE: In order to provide backwards compatibility with JDK1.5 this implementation uses
     * the <code>ClassUtils.loadDefaultService(Class<?>)</code>method instead of
     * <code>java.util.ServiceLoader</code>. 
     * 
     * @throws IllegalStateException If standardization engine loading fails because of configuration
     *                               problems such as resource file <code>com.sun.mdm.standardizer.StandardizationEngine</code>
     *                               not being present under resource directory <code>META-INF/services</code>.
     */
    public StandardizationEngineWrapper() {
        try {
            if (standardizationEngine == null) {
                logger.info(localizer.x("STD-122: About to load standardization engine"));
                // NOTE For >= JDK 1.6:  Use ServiceLoader<StandardizationEngine>
                standardizationEngine = ClassUtils.loadDefaultService(StandardizationEngine.class);

                logger.info(localizer.x("STD-123: Done loading standardization engine"));
            }
        } catch (Exception e) {
            logger.severe(localizer.x("STD-124: Error loading standardization engine service: {0}", e));
            throw new IllegalStateException("Error loading standardization engine service: " + e, e);
        }
    }

    /**
     * Initialize the wrapped standardization engine instance.
     */
    public synchronized void initialize() {
        if (state == SHUTDOWN) {
            logger.info(localizer.x("STD-125: Initializing standardization engine"));
            standardizationEngine.initialize();
            state = INITIALIZED;
        }
    }
    
    /**
     * Standardize an array of records qualified by type and domain. All three arrays must have the
     * same length. This implementation standardizes each value in turn using its wrapped standardization
     * engine and creates an array of standardized record wrappers that expose the legacy <code>SbmeStandRecord</code>
     * interface.
     * 
     * @param typeNames The array of standardization data type names
     * @param value The array of input strings to be standardized
     * @param domain The array of standardization domain (i.e. variant) names
     * 
     * @return An array  of standardized records
     */
    public SbmeStandRecord[][] standardize(String[] typeNames, String[] value, String[] domain) {
        SbmeStandRecord[][] result = new SbmeStandRecord[typeNames.length][];
        for (int i = 0; i < typeNames.length; i++) {
            List<StandardizedRecord> standardizedRecords = standardizationEngine.standardize(typeNames[i], domain[i], value[i]);
            result[i] = new SbmeStandRecord[standardizedRecords.size()];
            for (int j = 0; j < result[i].length; j++) {
                result[i][j] = new StandardizedRecordWrapper(standardizedRecords.get(j));
            }
        }
        return result;            
    }

    /**
     * Normalize an array of standardized objects each belonging to a given domain. All fields
     * populated in the incoming standardized records are tried in turn. Those for which no
     * normalization is defined will be returned unchanged as per the general <code>Normalizer</code>
     * interface.
     * <br/>
     * This implementation is non-destructive: a new instance of <code>SbmeStandRecord</code> is
     * created that is a shallow copy of the corresponding input record.
     * 
     * @param standObjs The array of standardized objects
     * @param domain The array of domains
     * 
     * @return 
     */
    @SuppressWarnings("unchecked")
    public SbmeStandRecord[] normalize(SbmeStandRecord[] standObjs, String[] domain) {
        SbmeStandRecord[] records = new SbmeStandRecord[standObjs.length];
        for (int i = 0; i < standObjs.length; i++) {
            records[i] = new DefaultSbmeStandRecord(standObjs[i]);
            for (String fieldName: (Set<String>) records[i].getAllFields()) {
                records[i].setValue(fieldName, standardizationEngine.normalize(records[i].getType(), domain[i], fieldName, records[i].getValue(fieldName)));
            }
        }
        return records;
    }    
    
    /**
     * Shutdown the wrapped standardization engine.
     */
    public synchronized void shutdown() {
        if (state == INITIALIZED) {
            logger.info(localizer.x("STD-126: Shutting down standardization engine"));
            standardizationEngine.shutdown();
            state = SHUTDOWN;
        }
    }
    
    /**
     * Compatibility class that exposes the legacy <code>com.stc.sbme.api.SbmeStandRecord</code>
     * interface.
     *
     * @author Ricardo Rocha (ricardo.rocha@sun.com)
     */
    private static class DefaultSbmeStandRecord implements SbmeStandRecord {
        /**
         * The standardization data type for this record. This symbol is customarily capitalized
         * as in <code>Address</code> or <code>PersonName</code>.
         */
        private String type;
        
        /**
         * The field map underlying the record contents.
         */
        private Map<String, String> fields;
        
        /**
         * The shallow-copy record constructor. This constructor creates a shallow copy of its
         * input standardized record.
         * 
         * @param source The input record being duplicated
         */
        @SuppressWarnings("unchecked")
        public DefaultSbmeStandRecord(SbmeStandRecord source) {
            this.type = source.getType();
            this.fields = new HashMap<String, String>();
            for (String fieldName: (Set<String>) source.getAllFields()) {
                fields.put(fieldName, source.getValue(fieldName));
            }
        }
        
        /**
         * Return the names of all populated fields.
         * @see <code>com.stc.sbme.api.SbmeStandRecord.getAllFields()</code>
         */
        @SuppressWarnings("unchecked")
        public Set getAllFields() {
            return fields.keySet();
        }

        /**
         * Return the status of the given field. This implementation throws an
         * <code>UnsupportedOperationException</code> as the actual semantics of this
         * method is undefined in the legacy interface.
         * 
         * @throws UnsupportedOperationException
         * 
         * @see <code>com.stc.sbme.api.SbmeStandRecord.getStatusValue(String key)</code>
         */
        public String getStatusValue(String key) {
            throw new UnsupportedOperationException("Not implemented: getStatusValue(String key)");
        }

        /**
         * Return the standardization data type of this record.
         */
        public String getType() {
            return type;
        }

        /**
         * Returns the value of the field associated with the given name.
         */
        public String getValue(String name) {
            return fields.get(name);
        }

        /**
         * Return the values of the given field. This implementation throws an
         * <code>UnsupportedOperationException</code> as the actual semantics of this
         * method is undefined in the legacy interface.
         * 
         * @throws UnsupportedOperationException
         * 
         * @see <code>com.stc.sbme.api.SbmeStandRecord.getValues(String key)</code>
         */
        @SuppressWarnings("unchecked")
        public List getValues(String key) {
            throw new UnsupportedOperationException("Not implemented: getValues(String key)");
        }

        /**
         * Return the status values of the given data type and field name. This implementation
         * throws an<code>UnsupportedOperationException</code> as the actual semantics of this
         * method is undefined in the legacy interface.
         * 
         * @throws UnsupportedOperationException
         * 
         * @see <code>com.stc.sbme.api.SbmeStandRecord.setStatusValue(String typeName, String value)</code>
         */
        public void setStatusValue(String typeName, String value) {
            throw new UnsupportedOperationException("Not implemented: setStatusValue(String key, String value)");
        }

        /**
         * Set the string value of a given field.
         * 
         * @see <code>com.stc.sbme.api.SbmeStandRecord.setValue(String key, String value)</code>
         */
        public void setValue(String key, String value) {
            fields.put(key, value);
        }

        /**
         * Return the field values of the given data type and field name. This implementation
         * throws an<code>UnsupportedOperationException</code> as the actual semantics of this
         * method is undefined in the legacy interface.
         * 
         * @throws UnsupportedOperationException
         * 
         * @see <code>com.stc.sbme.api.SbmeStandRecord.setValues(String typeName, List value)</code>
         */
        @SuppressWarnings("unchecked")
        public void setValues(String typeName, List value) {
            throw new UnsupportedOperationException("Not implemented: setValues(String key, List value)");
        }
    }
}
