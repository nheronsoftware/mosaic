/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
*/ 
package com.sun.mdm.standardizer.compat;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.stc.sbme.api.SbmeStandRecord;
import com.sun.mdm.standardizer.StandardizedRecord;
import com.sun.mdm.standardizer.StandardizedRecord.StandardizedField;

/**
 * Compatibility class that wraps a <code>com.sun.mdm.standardizer.StandardizedRecord</code>
 * while exposing the legacy <code>com.stc.sbme.api.SbmeStandRecord</code> interface.
 *
 * @author Ricardo Rocha (ricardo.rocha@sun.com)
 */
/**
 *
 * @author Ricardo Rocha (ricardo.rocha@sun.com)
 */
public class StandardizedRecordWrapper implements SbmeStandRecord {
    /**
     * The parameter name for standardization data type.
     */
    public static final String DATA_TYPE = "dataType";
    
    /**
     * The parameter name for standardization variant.
     */
    public static final String VARIANT_TYPE = "variantType";
    
    /**
     * The wrapped standardization record instance.
     */
    private StandardizedRecord record;

    /**
     * The base constructor taking an instance of <code>com.sun.mdm.standardizer.StandardizedRecord</code>.
     * 
     * @param record The wrapped standardized record instance
     */
    public StandardizedRecordWrapper(StandardizedRecord record) {
        if (record == null) {
            throw new NullPointerException("Null record passed to standardized record wrapper");
        }
        this.record = record;
    }
    
    /**
     * Return the string representation of this record.
     * 
     * @return The string representation of the wrapped standardized record.
     */
    public String toString() {
        return record.toString();
    }
    
    /**
     * Return the set of all populated field names. Note that this is not the exhaustive
     * list of all possible field types defined by the record's data type but, instead,
     * the list of only those output fields which were populated during standardization.
     * 
     * @return The set of all populated field names
     */
    public Set getAllFields() {
        return record.getFieldNames();
    }

    /**
     * Unimplemented operation.
     * 
     * @throws UnsupportedOperationException 
     */
    public String getStatusValue(String key) {
        throw new UnsupportedOperationException("No equivalence for getStatusValue()");
    }

    /**
     * Return the name of the data type to which this standardized record belongs.
     * 
     * @return The data type name
     */
    public String getType() {
        if (record.getParameters() != null) {
            return (String) record.getParameters().get(DATA_TYPE);
        }
        throw new IllegalArgumentException("No type defined for standardized record");
    }

    /**
     * Return the value of the field given by <code>key</code>
     * 
     * @return The field value corresponding to <code>key</code>
     */
    public String getValue(String key) {
        return record.getFieldValue(key);
    }

    /**
     * Return the list of individual tokens corresponding to the field named by
     * <code>key</code>.
     * 
     *  @return The list of tokens for the given field
     */
    public List getValues(String key) {
        List<StandardizedField> fieldValues = record.getFields(key);
        List<String> stringValues = new ArrayList<String>(fieldValues.size());
        for (StandardizedField standardizedField: fieldValues) {
            StringBuilder sb = new StringBuilder();
            for (String token: standardizedField.getTokens()) {
                sb.append(token);
                sb.append(' ');
            }
            if (sb.length() > 1) {
                sb.setLength(sb.length() - 1);
            }
            stringValues.add(sb.toString());
        }
        return stringValues;
    }

    /**
     * Unimplemented operation.
     * 
     * @throws UnsupportedOperationException 
     */
    public void setStatusValue(String key, String value) {
        throw new UnsupportedOperationException("No equivalence for setStatusValue()");
    }

    /**
     * Unimplemented operation.
     * 
     * @throws UnsupportedOperationException 
     */
    public void setValue(String key, String value) {
        throw new UnsupportedOperationException("No equivalence for setValue()");
    }

    /**
     * Unimplemented operation.
     * 
     * @throws UnsupportedOperationException 
     */
    public void setValues(String key, List value) {
        throw new UnsupportedOperationException("No equivalence for setValues()");
    }
}