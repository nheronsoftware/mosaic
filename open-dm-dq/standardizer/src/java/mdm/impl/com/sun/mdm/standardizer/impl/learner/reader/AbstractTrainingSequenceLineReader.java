package com.sun.mdm.standardizer.impl.learner.reader;

import java.util.Map;

import com.sun.mdm.standardizer.impl.InputSymbol;
import com.sun.mdm.standardizer.impl.learner.reader.LineReaderTrainingSequenceIterator.TrainingSequenceLineReader;

public abstract class AbstractTrainingSequenceLineReader implements TrainingSequenceLineReader {
    private Map<String, InputSymbol> inputSymbols;
    
    public AbstractTrainingSequenceLineReader() {}
    
    public AbstractTrainingSequenceLineReader(Map<String, InputSymbol> inputSymbols) {
        this.inputSymbols = inputSymbols;
    }

    public Map<String, InputSymbol> getInputSymbols() {
        return inputSymbols;
    }

    public void setInputSymbols(Map<String, InputSymbol> inputSymbols) {
        this.inputSymbols = inputSymbols;
    }
}
