package com.sun.mdm.standardizer.impl.introspector;

import java.io.File;
import java.util.List;

import net.java.hulp.i18n.Logger;

import com.sun.inti.container.ContainerException;
import com.sun.inti.container.ServiceInstanceDescriptor;
import com.sun.inti.container.ServiceTypeDescriptor;
import com.sun.mdm.standardizer.Localizer;
import com.sun.mdm.standardizer.introspector.DataTypeDescriptor;
import com.sun.mdm.standardizer.introspector.IntrospectionException;
import com.sun.mdm.standardizer.introspector.VariantDescriptor;

public class DataTypeDescriptorImpl implements DataTypeDescriptor {
    private static final Logger logger = Logger.getLogger(DataTypeDescriptorImpl.class.getName());
	private static final Localizer localizer = Localizer.getInstance();
    
    protected static final String FIELDS_PARAMETER_NAME = "fields";

    private ServiceTypeDescriptor descriptor;
    
    public DataTypeDescriptorImpl(ServiceTypeDescriptor descriptor) {
        this.descriptor = descriptor;
    }
    
    public String getName() {
        return descriptor.getName();
    }

    public String getDescription() {
        return descriptor.getDescription();
    }

    public String[] getResourceFilenames() {
        return descriptor.getResourceFilenames();
    }

    public File getResourceFile(String name) throws IntrospectionException {
        try {
            return descriptor.getResourceFile(name);
        } catch (ContainerException ce) {
            logger.warn(localizer.x("STD-101: Container error getting resource file '{0}': {1}", name, ce));
            throw new IntrospectionException("Container error getting resource file '" + name + "': " + ce, ce);
        }
    }

    private String[] fieldNames;
    public String[] getFieldNames() {
        if (fieldNames == null) {
            @SuppressWarnings("unchecked")
            List<String> fieldNameList = (List<String>) descriptor.getParameters().get(FIELDS_PARAMETER_NAME);
            if (fieldNameList != null) {
            	fieldNames = fieldNameList.toArray(new String[fieldNameList.size()]);
            }
        }
        return fieldNames;
    }

    public VariantDescriptor[] getVariants() {
        ServiceInstanceDescriptor[] instanceDescriptors = descriptor.getInstances();
        VariantDescriptor[] variantDescriptors = new VariantDescriptor[instanceDescriptors.length];
        for (int i = 0; i < instanceDescriptors.length; i++) {
            variantDescriptors[i] = new VariantDescriptorImpl(instanceDescriptors[i]);
            
        }
        return variantDescriptors;
    }
}
