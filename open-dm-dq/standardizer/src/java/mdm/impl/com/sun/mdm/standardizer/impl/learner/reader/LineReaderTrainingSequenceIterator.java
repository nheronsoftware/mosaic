package com.sun.mdm.standardizer.impl.learner.reader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;

import com.sun.mdm.standardizer.impl.learner.TrainingSequence;

public class LineReaderTrainingSequenceIterator implements Iterator<TrainingSequence> {
    public interface TrainingSequenceLineReader {
        public TrainingSequence fromLine(String line);
    }

    private BufferedReader in;
    private String currentLine;
    private TrainingSequenceLineReader trainingSequenceLineReader;
    
    public LineReaderTrainingSequenceIterator(InputStream is, TrainingSequenceLineReader trainingSequenceLineReader) throws IOException {
        this.in = new BufferedReader(new InputStreamReader(is));
        this.trainingSequenceLineReader = trainingSequenceLineReader;
        
        currentLine = in.readLine();
    }

    public boolean hasNext() {
        return currentLine != null;
    }

    public TrainingSequence next() {
        TrainingSequence trainingSequence = trainingSequenceLineReader.fromLine(currentLine);
        advanceLine();
        return trainingSequence;
    }

    public void remove() {
        throw new UnsupportedOperationException("Can't remove training sequence");
    }
    
    private void advanceLine(){
        do {
            try {
                currentLine = in.readLine();
            } catch (IOException e) {
                throw new IllegalStateException("Error reading line: " + e, e);
            }
        } while (currentLine != null && currentLine.startsWith("#"));
    }
}
