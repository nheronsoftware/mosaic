package com.sun.mdm.standardizer.impl.learner;

import java.util.List;

import com.sun.mdm.standardizer.impl.InputSymbol;

/**
 * This auxiliary class represents an input training sequence understood as a list of
 * one or more input symbols (the <code>sequence</code>) and the associated number of times
 * it occurred in the training file (the <code>count</code>).
 *
 * @author Ricardo Rocha (ricardo.rocha@sun.com)
 */
public class TrainingSequence {
    /**
     * The distinct list of input symbols observed in the input training file
     */
    private List<InputSymbol> sequence;
    /**
     * The number of times the <code>sequence</code> has been observed in the input
     * training file.
     */
    private int count;

    public TrainingSequence(List<InputSymbol> sequence, int count) {
        this.setSequence(sequence);
        this.setCount(count);
    }

    public void setSequence(List<InputSymbol> sequence) {
        this.sequence = sequence;
    }

    public List<InputSymbol> getSequence() {
        return sequence;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getCount() {
        return count;
    }
}