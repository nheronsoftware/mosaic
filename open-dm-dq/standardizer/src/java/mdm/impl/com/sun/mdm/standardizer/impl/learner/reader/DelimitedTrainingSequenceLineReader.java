package com.sun.mdm.standardizer.impl.learner.reader;

import java.util.Map;
import java.util.regex.Pattern;

import com.sun.mdm.standardizer.impl.InputSymbol;
import com.sun.mdm.standardizer.impl.learner.TrainingSequence;

public abstract class DelimitedTrainingSequenceLineReader extends AbstractTrainingSequenceLineReader {
    private Pattern separator;
    
    public DelimitedTrainingSequenceLineReader() {}
    
    public DelimitedTrainingSequenceLineReader(Map<String, InputSymbol> inputSymbols, Pattern separator) {
        super(inputSymbols);
        this.separator = separator;
    }
    
    public TrainingSequence fromLine(String line) {
        return fromLine(separator.split(line));
    }
    
    protected abstract TrainingSequence fromLine(String[] fields);
}
