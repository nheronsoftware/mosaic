package com.sun.mdm.standardizer.impl.introspector;

import java.io.File;

import net.java.hulp.i18n.Logger;

import com.sun.inti.container.ContainerException;
import com.sun.inti.container.ServiceInstanceDescriptor;
import com.sun.mdm.standardizer.Localizer;
import com.sun.mdm.standardizer.introspector.IntrospectionException;
import com.sun.mdm.standardizer.introspector.VariantDescriptor;

public class VariantDescriptorImpl implements VariantDescriptor {
    private static final Logger logger = Logger.getLogger(VariantDescriptorImpl.class.getName());
	private static final Localizer localizer = Localizer.getInstance();

    private ServiceInstanceDescriptor descriptor;
    
    public String getTypeName() {
        return (String) descriptor.getParameters().get("dataType");
//        // Shameless kludge to appease eView design time
//        return ((ServiceInstance) descriptor).getBaseDirectory().getParentFile().getParentFile().getName();
    }
    
    public String getVariantType() {
        return (String) descriptor.getParameters().get("variantType");
    }

    public VariantDescriptorImpl(ServiceInstanceDescriptor descriptor) {
        this.descriptor = descriptor;
    }
    
    public String getName() {
        return descriptor.getName();
    }

    public String getDescription() {
        return descriptor.getDescription();
    }

    public File getResourceFile(String name) throws IntrospectionException {
        try {
            return descriptor.getResourceFile(name);
        } catch (ContainerException e) {
            logger.warn(localizer.x("STD-111: Error getting resource file '{0}: {1}", name, e));
            throw new IntrospectionException("Error getting resource file '" + name + "': " + e, e);
        }
    }

    public String[] getResourceFilenames() {
        return descriptor.getResourceFilenames();
    }
}
