package com.sun.mdm.standardizer.impl;

public abstract class Symbol {
	private String name;

	public Symbol() {}

	public Symbol(String name) {
		this.name = name;
	}

	public String toString() {
		return name;
	}

	public int hashCode() {
		return name.hashCode();
	}

	public boolean equals(Symbol that) {
		return name.equals(that.name);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
