/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
*/ 
package com.sun.mdm.standardizer.impl;

import java.util.ArrayList;
import java.util.List;

import com.sun.inti.components.string.concatenate.SeparatorBasedStringConcatenator;
import com.sun.inti.components.string.concatenate.StringConcatenator;

/**
 * This class represents the output token types assigned during the string parsing phase
 * of standardization.<br/>
 * 
 * <code>OutputSymbol</code>s are associated with the <code><a href="StateTransition.html">StateTransition</a></code>
 * selected by observing a given <code><a href="InputSymbol.html">InputSymbol</a></code> while in a given
 * <code><a href="State.html">State</a></code>.<br/>
 * 
 * Also, a <code>OutputSymbol</code> has two instances of {@link StringConcatenator}'s; one for concatenating adjacent
 * tokens of the same output type (<code>tokenConcatenator</code>) and another (<code>OccurrenceConcatenator</code>
 *  for further concatenating tokens concatenadres by the token concatenator. The latter takes care of the case
 *  where the same output type appears more than once in a normalized record but in non-consecutive positions.
 * strings resulting from the application 
 *
 * @author Ricardo Rocha (ricardo.rocha@sun.com)
 */
public class OutputSymbol extends Symbol {
    /**
     * The default concatenator to use when concatenating contiguous tokens matched by the same input field.
     * This component uses a whitespace as concatenating string.
     */
	private static final StringConcatenator defaultTokenConcatenator = new SeparatorBasedStringConcatenator(" ");
	/**
	 * The default concatenator to use when concatenating non-contiguous groups of tokens matched at different
	 * possitions within the input record string
     * This component uses a comma as concatenating string.
	 */
	private static final StringConcatenator defaultOccurrenceConcatenator = new SeparatorBasedStringConcatenator(", ");

	/**
	 * The token concatenator to use when concatenating contiguous tokens matched by the same input field
	 */
	private StringConcatenator tokenConcatenator = defaultTokenConcatenator;
	/**
     * The concatenator to use when concatenating non-contiguous groups of tokens matched at different
     * possitions within the input record string
	 */
	private StringConcatenator occurrenceConcatenator = defaultOccurrenceConcatenator;

	/**
	 * The default, empty constructor.
	 */
	public OutputSymbol() {}

	/**
	 * The complete construtor.
	 * 
	 * @param tokenConcatenator The concatenator to be used in concatenating adjacent token (instance concatenation)
	 * @param occurrenceConcatenator The concatenator to used in concatenating instance concatenations
	 */
	public OutputSymbol(StringConcatenator tokenConcatenator, StringConcatenator occurrenceConcatenator) {
		this.tokenConcatenator = tokenConcatenator;
		this.occurrenceConcatenator = occurrenceConcatenator;
	}

	/**
	 * Convenience method to concatenate a list of string in accordance to the concatenator configred for
	 * this output type.
	 * 
	 * @param tokens List list of strings to concatenated
	 * @return The concatenated string
	 */
	public String concatenateTokens(List<String> tokens) {
		return tokenConcatenator.concatenate(tokens);
	}

	/**
	 * Cbnvenience metid to concatenate lists of lists of string (as they are recognized by the standardizer).
	 * 
	 * @param tokens The list of lists of strings to be concatenated
	 * @return The concatenated string
	 */
	public String concatenateOccurrences(List<List<String>> tokens) {
	    List<String> concatenatedTokens = new ArrayList<String>();
	    for (List<String> tokenList: tokens) {
	        concatenatedTokens.add(concatenateTokens(tokenList));
	    }
		return occurrenceConcatenator.concatenate(concatenatedTokens);
	}

	/**
	 * Return the token concatenartor
	 * @return the token concatenartor
	 */
	public StringConcatenator getTokenConcatenator() {
		return tokenConcatenator;
	}

	/**
	 * Set the token concatenartor
	 * @param tokenConcatenator the token concatenartor
	 */
	public void setTokenConcatenator(StringConcatenator tokenConcatenator) {
		this.tokenConcatenator = tokenConcatenator;
	}

	/**
	 * Return the occurrence concatenator
	 * @return the occurrence concatenator
	 */
	public StringConcatenator getOccurrenceConcatenator() {
		return occurrenceConcatenator;
	}

	/**
	 * Set the occurrence concatenator
	 * @param occurrenceConcatenator the occurrence concatenator
	 */
	public void setOccurrenceConcatenator(StringConcatenator occurrenceConcatenator) {
		this.occurrenceConcatenator = occurrenceConcatenator;
	}
}
