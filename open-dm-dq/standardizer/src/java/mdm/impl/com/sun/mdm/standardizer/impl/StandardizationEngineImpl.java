package com.sun.mdm.standardizer.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import net.java.hulp.i18n.Logger;

import com.sun.inti.components.util.ClassUtils;
import com.sun.inti.components.util.IOUtils;
import com.sun.inti.container.Container;
import com.sun.inti.container.ContainerException;
import com.sun.mdm.standardizer.Localizer;
import com.sun.mdm.standardizer.Normalizer;
import com.sun.mdm.standardizer.StandardizationEngine;
import com.sun.mdm.standardizer.StandardizationException;
import com.sun.mdm.standardizer.StandardizedRecord;
import com.sun.mdm.standardizer.Standardizer;

public class StandardizationEngineImpl implements StandardizationEngine {
    private static final Logger logger = Logger.getLogger(StandardizationEngineImpl.class.getName());
	private static final Localizer localizer = Localizer.getInstance();

    private static final String REPOSITORY_NAME_PROPERTY = "repositoryName";
    private static final String STANDARDIZER_COMPONENT_NAME = "standardizer";
    private static final String NORMALIZER_COMPONENT_NAME = "normalizer";
    private static final String REPOSITORY_RESOURCE_NAME = "com/sun/mdm/standardizer/repositoryImage.zip";
    private static final String PROPERTIES_RESOURCE_NAME = "com/sun/mdm/standardizer/standardizationEngine.properties";
    
    private Container container;
    private File repositoryDirectory;

    public StandardizationEngineImpl() throws StandardizationException {
        try {
            // Fail fast
            getRepositoryInputStream().close();
            
            String repositoryName = null;
            InputStream is = getClass().getClassLoader().getResourceAsStream(PROPERTIES_RESOURCE_NAME);
            if (is == null) {
                repositoryName = temporaryName();
            } else {
                Properties properties = new Properties();
                try {
                    properties.load(is);
                } catch (IOException ioe) {
                    logger.severe(localizer.x("STD-113: Error loading standardization engine properties file: {0}", ioe));
                    throw new StandardizationException("Error loading standardization engine properties file: " + ioe, ioe);
                }
                
                repositoryName = properties.getProperty(REPOSITORY_NAME_PROPERTY);
                if (repositoryName == null) {
                    repositoryName = temporaryName();
                }
            }

            repositoryDirectory = new File(new File(System.getProperty("java.io.tmpdir")), repositoryName);
            repositoryDirectory.mkdirs();
            
            if (!(repositoryDirectory.isDirectory() && repositoryDirectory.canRead() && repositoryDirectory.canWrite())) {
                logger.severe(localizer.x("STD-114: Not a readable/writable directory: {0}", repositoryDirectory.getAbsolutePath()));
                throw new StandardizationException("Not a readable/writable directory: " + repositoryDirectory.getAbsolutePath());
            }
        } catch (StandardizationException e) {
            throw e;
        } catch (IOException ioe) {
            logger.severe(localizer.x("STD-115: Error opening/loading resource file: {0}", ioe));
            throw new StandardizationException("Error opening/loading resource file: " + ioe, ioe);
        }
    }
    
    public void initialize() throws StandardizationException {
        try {
            if (container == null) {
                container = ClassUtils.loadDefaultService(Container.class);
                IOUtils.extract(getRepositoryInputStream(), repositoryDirectory);
            }
            container.initialize(repositoryDirectory);
        } catch (ContainerException ce) {
            logger.severe(localizer.x("STD-116: Error initializing container: {0}", ce));
            throw new StandardizationException("Error initializing container: " + ce, ce);
        } catch (IOException ioe) {
            logger.severe(localizer.x("STD-117: Error extracting repository image: {0}", ioe));
            throw new StandardizationException("Error extracting repository image: " + ioe);
        }
    }

    public List<StandardizedRecord> standardize(String typeName, String variantName, String record) throws StandardizationException {
        try {
            Standardizer standardizer = (Standardizer) container.getComponent(typeName, variantName, STANDARDIZER_COMPONENT_NAME);
            return standardizer.standardize(record);
        } catch (ContainerException ce) {
            logger.severe(localizer.x("STD-118: Error retrieving standardizer: {0}", ce));
            throw new StandardizationException("Error retrieving standardizer: " + ce, ce);
        }
    }

    public String normalize(String typeName, String variantName, String fieldName, String fieldValue) throws StandardizationException {
        try {
            Normalizer normalizer = (Normalizer) container.getComponent(typeName, variantName, NORMALIZER_COMPONENT_NAME);
            return normalizer.normalize(fieldName, fieldValue);
        } catch (ContainerException ce) {
            logger.severe(localizer.x("STD-119: Error retrieving normalizer: {0}", ce));
            throw new StandardizationException("Error retrieving normalizer: " + ce, ce);
        }
    }

    public void shutdown() throws StandardizationException {
        try {
            container.shutdown();
        } catch (ContainerException ce) {
            logger.severe(localizer.x("STD-120: Error shutting down container: {0}", ce));
            throw new StandardizationException("Error shutting down container: " + ce, ce);
        }
    }
    
    private InputStream getRepositoryInputStream() throws FileNotFoundException {
        InputStream is = getClass().getClassLoader().getResourceAsStream(REPOSITORY_RESOURCE_NAME);
        if (is == null) {
            throw new FileNotFoundException("No such resource: " + REPOSITORY_RESOURCE_NAME);
        }
        return is;
    }

    private static synchronized String temporaryName() {
        return "standardizationRepository_" + System.currentTimeMillis();
    }
}
