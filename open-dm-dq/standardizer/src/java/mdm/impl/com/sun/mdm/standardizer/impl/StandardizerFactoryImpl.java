package com.sun.mdm.standardizer.impl;

import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.Writer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.core.io.DescriptiveResource;
import org.w3c.dom.Document;

import com.sun.inti.components.util.WorkingDirectoryAware;
import com.sun.mdm.standardizer.StandardizerFactory;

public class StandardizerFactoryImpl implements StandardizerFactory, WorkingDirectoryAware {
	private static Templates templates;
	private static final String STATE_STYLESHEET = "standardizer.xsl";
	public static final String STANDARDIZER_BEAN_NAME = "standardizer";
	public static final String WORKING_DIRECTORY_NAME_PARAMETER = "workingDirectory";
	static {
		InputStream is = StandardizerFactoryImpl.class.getResourceAsStream(STATE_STYLESHEET);
		if (is == null) {
			throw new RuntimeException("Can't locate resource: " + STATE_STYLESHEET);
		}
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Source source = new StreamSource(is);
		try {
			templates = transformerFactory.newTemplates(source);
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
			throw new RuntimeException("Error loading " + STATE_STYLESHEET + " stylesheet: " + e, e);
		}
	}

	private File workingDirectory;
	private String configurationFilename;
	private String configurationBeansFilename;
	
	public StandardizerFactoryImpl() {}
	
	public StandardizerFactoryImpl(File workingDirectory, String configurationFilename, String configurationBeansFilename) {
		this.setWorkingDirectory(workingDirectory);
		this.setConfigurationFilename(configurationFilename);
		this.setConfigurationBeansFilename(configurationBeansFilename);
	}

	private StandardizerImpl standardizer;
	
	public StandardizerImpl newStandardizer() {
		if (standardizer == null) {
			try {
				Writer out = null;
				if (configurationBeansFilename != null) {
					out = new FileWriter(new File(workingDirectory, configurationBeansFilename));
				}

				standardizer = buildStandardizer(new File(workingDirectory, configurationFilename).toURI().toString(), out, workingDirectory.getAbsolutePath());

				if (out != null) {
					out.flush();
					out.close();
				}
			} catch (Exception e) {
				throw new RuntimeException("Error creating standarizer: " + e, e);
			}
		}
		return standardizer;
	}

	public StandardizerImpl buildStandardizer(String uri, Writer out, String workingDirectoryName) throws Exception {
		ApplicationContext context = buildApplicationContext(uri, out, workingDirectoryName);
		return (StandardizerImpl) context.getBean(STANDARDIZER_BEAN_NAME);
	}

	private ApplicationContext buildApplicationContext(String uri, Writer writer, String workingDirectoryName) throws Exception {
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
		Document document = documentBuilder.newDocument();

		Source source = new StreamSource(uri);
		Result result = new DOMResult(document);
		Transformer transformer = templates.newTransformer();
		transformer.setParameter(WORKING_DIRECTORY_NAME_PARAMETER, workingDirectoryName + File.separator);
		transformer.transform(source, result);

		if (writer != null) {
			TransformerFactory.newInstance().newTransformer().transform(new DOMSource(document), new StreamResult(writer));
		}

		Thread currentThread = Thread.currentThread();
		ClassLoader contextClassLoader = currentThread.getContextClassLoader();
		try {
			currentThread.setContextClassLoader(getClass().getClassLoader());
			GenericApplicationContext context = new GenericApplicationContext();
			XmlBeanDefinitionReader definitionReader = new XmlBeanDefinitionReader(context);
			definitionReader.registerBeanDefinitions(document, new DescriptiveResource(uri));

			return context;
		} finally {
			currentThread.setContextClassLoader(contextClassLoader);
		}
	}

	public void setWorkingDirectory(File workingDirectory) {
		this.workingDirectory = workingDirectory;
	}

	public void setConfigurationFilename(String configurationFilename) {
		this.configurationFilename = configurationFilename;
	}

	public void setConfigurationBeansFilename(String configurationBeansFilename) {
		this.configurationBeansFilename = configurationBeansFilename;
	}
}
