/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
*/ 
package com.sun.mdm.standardizer;

import java.util.regex.Pattern;

import net.java.hulp.i18n.LocalizationSupport;

/**
 * This class provides <a href="https://hulp.dev.java.net/">Hulp</a> logging support to
 * standardization classes.
 *
 * @author Ricardo Rocha (ricardo.rocha@sun.com)
 */
public class Localizer extends LocalizationSupport {
    /**
     * The default prefix used by the Master Data Management's Data Quality modules
     */
    private static final String DEFAULT_PREFIX = "MDM-DQ-";
    /**
     * The default resource base filename where bundled messages are found
     */
    private static final String DEFAULT_BUNDLE_NAME = "msgs";
    /**
     * The fault regular expression used to identify the message code (group 1) and the message text( group 3)
     */
    private static final Pattern DEFAULT_PATTERN = Pattern.compile("([A-Z][-A-Z0-9]+)\\s*:.*");
    
    /**
     * The singleton instance.
     */
    private static final Localizer instance = new Localizer();
    
    /**
     * Return the singleton instance.
     * @return the singleton instance
     */
    public static Localizer getInstance() {
        return instance;
    }
    
    /**
     * The private default constructor
     */
    private Localizer() {
        super(DEFAULT_PATTERN, DEFAULT_PREFIX, DEFAULT_BUNDLE_NAME);
    }
}
