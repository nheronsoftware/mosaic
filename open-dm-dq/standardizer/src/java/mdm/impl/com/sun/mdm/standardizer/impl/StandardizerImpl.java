package com.sun.mdm.standardizer.impl;


import static com.sun.mdm.standardizer.impl.InputSymbol.EOF;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.logging.Level;

import net.java.hulp.i18n.Logger;

import com.sun.inti.components.string.tokenize.Tokenizer;
import com.sun.inti.components.string.transform.StringTransformer;
import com.sun.inti.components.util.Parametrizable;
import com.sun.mdm.standardizer.Localizer;
import com.sun.mdm.standardizer.StandardizedRecord;
import com.sun.mdm.standardizer.Standardizer;
import com.sun.mdm.standardizer.impl.InputSymbol.Match;

// TODO Add enrichers/postprocessors that extend standardized record contents
//      Example: firstName conjunction firstName lastName -> firstName lastName conjunction fisrtName lastName
// TODO Evaluate multiplication-based accumulated probability evaluation
public class StandardizerImpl implements Standardizer, Parametrizable, Comparator<List<Field>> {
	private static final Logger logger = Logger.getLogger(StandardizerImpl.class.getName());
	private static final Localizer localizer = Localizer.getInstance();

	private Tokenizer tokenizer;
	private StringTransformer recordCleanser;
    private State initialState;
    private InputSymbol conjunctionInputSymbol;
    private InputSymbol extraInformationInputSymbol;
    private OutputSymbol conjunctionOutputSymbol;
    private OutputSymbol extraInformationOutputSymbol;
	
	private Map<String, Object> parameters;

	public StandardizerImpl() {}
	
	public List<StandardizedRecord> standardize(final String record) {
	    String cleansedRecord = record;
		if (recordCleanser != null) {
		    cleansedRecord = recordCleanser.transform(record);
			if (cleansedRecord == null || cleansedRecord.length() == 0) {
			    return new ArrayList<StandardizedRecord>();
			}
		}

		List<String> tokens = tokenizer.tokenize(cleansedRecord);

		List<List<Field>> patterns = new PatternBuilder(tokens).buildPatterns();
		if (patterns.size() == 0) {
		    System.err.println(record);
		    logger.warn(localizer.x("STD-121: No pattern was built for record '{0}'", record));
            return new ArrayList<StandardizedRecord>();
		}
		
		Collections.sort(patterns, this);
		if (logger.isFine()) {
		    for (List<Field> fields: patterns) {
		        logger.fine(localizer.x(fields.toString() + ": " + computeProbability(fields)));
		    }
		}
		List<Field> matchedPattern = patterns.get(0);
        
		List<StandardizedRecord> standardizedRecords = splitOnConjunction(matchedPattern);

		return standardizedRecords;
	}
	
	public void setParameters(Map<String, Object> parameters) {
	    this.parameters = parameters;
	}
    
    public int compare(List<Field> first, List<Field> second) {
        int firstEofCount = eofCount(first);
        int secondEofCount = eofCount(second);
        if (firstEofCount > secondEofCount) {
            return -1;
        }
        if (firstEofCount < secondEofCount) {
            return 1;
        }
        double firstProbability = computeProbability(first);
        double secondProbability = computeProbability(second); 
        if (firstProbability > secondProbability) {
            return -1;
        }
        if (firstProbability < secondProbability) {
            return 1;
        }
        int firstLength = computeLength(first);
        int secondLength = computeLength(second);
        if (firstLength > secondLength) {
            return -1;
        }
        if (firstLength < secondLength) {
            return 1;
        }
        return 0;
    }
    private static int eofCount(List<Field> fields) {
        int count = 0;
        for (Field field: fields) {
            if (field.getInputSymbol() == EOF) {
                count++;
            }
        }
        return count;
    }
	
	private class PatternBuilder {
	    private List<String> tokens;
	    private Stack<Field> patternStack;
	    private List<List<Field>> patterns;
	    
        public PatternBuilder(List<String> tokens) {
            this.tokens = tokens;
            this.patternStack = new Stack<Field>();
            this.patterns = new ArrayList<List<Field>>();
        }
        
        private List<List<Field>> buildPatterns() {
            buildPatterns(0, initialState);
            return patterns;
        }
        
        private void addPattern(List<Field> newPattern) { // TODO: Should be a Set
        	for (List<Field> pattern: patterns) {
        		if (equals(newPattern, pattern)) {
                	return;
        		}
        	}
        	patterns.add(newPattern);
        }
        private boolean equals(List<Field> first, List<Field> second) { // TODO: Should be a set
        	if (first.size() != second.size()) {
        		return false;
        	}
        	for (int i = 0; i < first.size(); i++) {
        		if (!first.get(i).equals(second.get(i))) {
        			return false;
        		}
        	}
        	return true;
        }

        private void buildPatterns(int offset, State state) {
            if (offset == tokens.size()) {
            	List<Field> pattern = new ArrayList<Field>(patternStack);
            	if (state.expectedInputSymbols().contains(EOF)) {
            		pattern.add(new Field(state.accept(EOF).getProbability()));
            	}
                addPattern(pattern);
            } else {
                for (InputSymbol inputSymbol: state.expectedInputSymbols()) {
                    if (inputSymbol != EOF) {
                    	tryInputSymbol(inputSymbol, offset, state);
                    }
                }
            }
        }

        private void tryInputSymbol(InputSymbol inputSymbol, int offset, State state) {
            Match match = inputSymbol.match(tokens, offset);
            if (match != null) {
                StateTransition stateTransition = state.accept(inputSymbol);
                processTransition(inputSymbol, match, stateTransition, offset, state);
            } else {
                if (computeProbability(patternStack) > 0d) {
                    List<Field> pattern = new ArrayList<Field>(patternStack);
                    if (extraInformationOutputSymbol != null) {
                        pattern.add(new Field(tokens.subList(offset, tokens.size()), extraInformationInputSymbol, extraInformationOutputSymbol, 0d));
                    }
                    addPattern(pattern);
                }
            }
        }

        private void processTransition(InputSymbol inputSymbol,
                                        Match match,
                                        StateTransition stateTransition,
                                        int offset,
                                        State state)
        {
            OutputSymbol outputSymbol = stateTransition.getOutputSymbol();
            if (outputSymbol != null) {
                patternStack.push(new Field(match.getTokens(),
                                            inputSymbol,
                                            outputSymbol,
                                            stateTransition.getProbability() * match.getFactor()));
                if (logger.isFine()) {
                    logger.fine(localizer.x(
                            "Tokens: " + match.getTokens() +
                            ". inputSymbol: " + inputSymbol.getName() +
                            ":, probability: " + (stateTransition.getProbability() * match.getFactor()) +
                            ", stateTransition.getProbability(): " + stateTransition.getProbability() +
                            ", match.getFactor(): " + match.getFactor()
                            ));
                }
            }

            buildPatterns(offset + match.getLength(), stateTransition.getNextState());

            if (outputSymbol != null) {
                patternStack.pop();
            }
        }
	}

	// TODO: Allow for pluggable probabily calculations?
	private static double computeProbability(Iterable<Field> pattern) {
//        double probability = 1d;
//        for (Field field : pattern) {
//            if (field.getProbability() != 0d) {
//                probability *= field.getProbability();
//            }
//        }
//        return 1 / probability;
	    
	    double count = 0;
	    double accumulatedProbability = 1d;
	    for (Field field: pattern) {
	        if (field.getProbability() != 0d) {
	            count++;
	            accumulatedProbability *= field.getProbability();
	        }
	    }
	    return Math.pow(accumulatedProbability, 1 / count);
        
//        double probability = 0d;
//        for (Field field : pattern) {
//            probability += field.getProbability();
//        }
//        return probability;
	}
	
	private static int computeLength(Iterable<Field> pattern) {
		int length = 0;
		for (Field field: pattern) {
			if (field.getProbability() > 0) {
				length++;
			}
		}
		return length;
	}

	private List<StandardizedRecord> splitOnConjunction(List<Field> pattern) {
		int start = 0;
		List<StandardizedRecord> standardizedRecords = new ArrayList<StandardizedRecord>();
        if (conjunctionInputSymbol != null) {
            for (int i = 0; i < pattern.size(); i++) {
                if (pattern.get(i).getInputSymbol() != null && pattern.get(i).getInputSymbol().equals(conjunctionInputSymbol)) {
                    standardizedRecords.add(new StandardizedRecordImpl(pattern.subList(start, i + 1), parameters));
                    start = i + 1;
                }
            }
        }
		if (start < pattern.size()) {
			standardizedRecords.add(new StandardizedRecordImpl(pattern.subList(start, pattern.size()), parameters));
		}

		return standardizedRecords;
	}

	public void setLoggerLevel(Level level) { logger.setLevel(level); }
	public State getInitialState() { return initialState; }
	public void setInitialState(State initialState) { this.initialState = initialState; }
	public Tokenizer getTokenizer() { return tokenizer; }
	public void setTokenizer(Tokenizer tokenizer) { this.tokenizer = tokenizer; }
	public StringTransformer getRecordCleanser() { return recordCleanser; }
	public void setRecordCleanser(StringTransformer recordCleanser) { this.recordCleanser = recordCleanser; }
	public OutputSymbol getExtraInformationOutputSymbol() { return extraInformationOutputSymbol; }
	public void setExtraInformationOutputSymbol(OutputSymbol extraInformationSymbol) { this.extraInformationOutputSymbol = extraInformationSymbol; }
	public InputSymbol getConjunctionInputSymbol() { return conjunctionInputSymbol; }
	public void setConjunctionInputSymbol(InputSymbol conjunctionSymbol) { this.conjunctionInputSymbol = conjunctionSymbol; }
    public InputSymbol getExtraInformationInputSymbol() { return extraInformationInputSymbol;}
    public void setExtraInformationInputSymbol(InputSymbol extraInformationInputSymbol) { this.extraInformationInputSymbol = extraInformationInputSymbol; }
    public OutputSymbol getConjunctionOutputSymbol() { return conjunctionOutputSymbol; }
    public void setConjunctionOutputSymbol(OutputSymbol conjunctionOutputSymbol) { this.conjunctionOutputSymbol = conjunctionOutputSymbol; }
}
