package com.sun.mdm.standardizer.test;

import com.stc.sbme.api.SbmeStandRecord;
import com.sun.mdm.standardizer.compat.StandardizationEngineWrapper;

public class StandardizationEngineWrapperTester {
	public static void main(String[] args) throws Exception {
		StandardizationEngineWrapper engine = new StandardizationEngineWrapper();
		engine.initialize();
		
		SbmeStandRecord record = null;
		
		record = engine.standardize(new String[]{"Address"}, new String[]{"1001 LINCOLN STREET B 202"}, new String[]{"US"})[0][0];
		System.out.println(record);
		record = engine.standardize(new String[]{"Address"}, new String[]{"1010 OLD HIGHWAY 12 MILE RD"}, new String[]{"US"})[0][0];
		System.out.println(record);
        
        record = engine.standardize(new String[]{"Address"}, new String[]{"1 rue Jean Jaures, BP 27"}, new String[]{"FR"})[0][0];
        System.out.println(record);
        record = engine.standardize(new String[]{"Address"}, new String[]{"10 r du vieux march� aux vins"}, new String[]{"FR"})[0][0];
        System.out.println(record);
        
        record = engine.standardize(new String[]{"PhoneNumber"}, new String[]{"+1(626)391-9217"}, new String[]{"Generic"})[0][0];
        System.out.println(record);
		
		engine.shutdown();
	}
}
