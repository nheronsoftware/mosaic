package com.sun.mdm.standardizer.test;

import java.io.File;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

import com.sun.inti.components.util.ClassUtils;
import com.sun.inti.components.util.IOUtils;
import com.sun.mdm.standardizer.StandardizationEngine;
import com.sun.mdm.standardizer.StandardizedRecord;

public class StandardizationEngineTester {
    private final static Logger logger = Logger.getLogger(StandardizationEngineTester.class.getName());
    
    public static void main(String[] args) throws Exception {
//        Properties properties = new Properties();
        ClassLoader classLoader = StandardizationEngine.class.getClassLoader();
//        properties.load(classLoader.getResourceAsStream("com/sun/mdm/standardizer/standardizationEngine.properties"));
//        String repositoryName = properties.getProperty("repositoryName");
//        File repositoryDirectory = new File(new File(System.getProperty("java.io.tmpdir")), repositoryName);
//        IOUtils.recursiveDelete(repositoryDirectory);
//        IOUtils.extract(classLoader.getResourceAsStream("com/sun/mdm/standardizer/repositoryImage.zip"), repositoryDirectory);
        StandardizationEngine engine = ClassUtils.loadDefaultService(StandardizationEngine.class);
        engine.initialize();

        List<StandardizedRecord> records;
        
        records = engine.standardize("Address", "US", "1001 Arcadia Ave #12");
        logger.info("US address: " + records.toString());
        logger.info("Parameters" + records.get(0).getParameters().toString());
        
        records = engine.standardize("BusinessName", "US", "Sun Microsystems");
        logger.info("US business name: " + records.toString());
        logger.info("Parameters" + records.get(0).getParameters().toString());
        
        records = engine.standardize("PhoneNumber", "Generic", "+1 (626) 391-9217 X. 123");
        logger.info("Phone number:: " + records.toString());
//        
//        logger.info("First name normalization: " + engine.normalize("PersonName", "US", "FirstName", "Bobby"));
                
        records = engine.standardize("PersonName", "US", "ALBERT GUTASY, JR AND/OR RICARDO ROCHA III, ESQ.");
        logger.info("US person name: " + records.toString());
        
//        records = engine.standardize("Address", "AU", "28 Garden Grv");
//        logger.info("AU address: " + records.toString());
//        logger.info("Parameters" + records.get(0).getParameters().toString());
        
        String normalizedFirstName = engine.normalize("PersonName", "US", "FirstName", "Mary Sue");
        logger.info("Normalized first name: " + normalizedFirstName);
        
        normalizedFirstName = engine.normalize("PersonName", "US", "FirstName", "O'Malley");
        logger.info("Normalized first name: " + normalizedFirstName);
        
        normalizedFirstName = engine.normalize("PersonName", "US", "FirstName", "Billy-Bob");
        logger.info("Normalized first name: " + normalizedFirstName);

        String normalizedLastName = engine.normalize("PersonName", "US", "LastName", "St. James");
        logger.info("Normalized last name: " + normalizedLastName);

        engine.shutdown();
    }
}
