package com.sun.mdm.standardizer.test;

import static java.util.logging.Level.FINE;
import static java.util.logging.Level.FINER;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.util.List;
import java.util.logging.Logger;

import com.sun.inti.components.component.BeanComponentManagerFactory;
import com.sun.inti.components.io.OutputStreamSource;
import com.sun.inti.components.url.ResourceURLSource;
import com.sun.mdm.standardizer.StandardizedRecord;
import com.sun.mdm.standardizer.Standardizer;
import com.sun.mdm.standardizer.StandardizedRecord.StandardizedField;

public class StandardizerTester {
    private static final Logger logger = Logger.getLogger(StandardizerTester.class.getName());

    public static void main(final String[] args) throws Exception {
        logger.setLevel(FINE);
        
        final String basename = "person-names";
        final File directory = new File("data/test/personname");
        final ClassLoader classLoader = StandardizerTester.class.getClassLoader();
        BeanComponentManagerFactory factory = new BeanComponentManagerFactory();
        factory.setUrlSource(new ResourceURLSource("standardizer.xml"));
        factory.setStylesheetURL(classLoader.getResource("com/sun/mdm/standardizer/impl/standardizer.xsl"));
        factory.setOutputStreamSource(new OutputStreamSource() {
            public OutputStream getOutputStream() throws IOException {
                return new FileOutputStream(new File(directory, basename + ".xml"));
            }
        });
        Standardizer standardizer = (Standardizer) factory.newInstance(classLoader).getComponent("standardizer");
        testStandardizer(standardizer, directory, basename);
    }

    private static void testStandardizer(Standardizer standardizer, File directory, String basename) throws Exception {
        final String inputFilename = basename + "-in.txt";
        final String outputFilename = basename + "-out.txt";
        final String errorFilename = basename + "-err.txt";

        final Reader reader = new FileReader(new File(directory, inputFilename));

        String record;
        final BufferedReader in = new BufferedReader(reader);
        final PrintWriter out = new PrintWriter(new FileWriter(new File(directory, outputFilename)), true);
        final PrintWriter err = new PrintWriter(new FileWriter(new File(directory, errorFilename)), true);

        int recordNumber = 0;
        long start = System.currentTimeMillis();
        while ((record = in.readLine()) != null) {
            recordNumber++;
            record = record.replaceAll("\\s*#.*", "").trim();
            if (record.length() > 0) {
                try {
                    List<StandardizedRecord> standardizedRecords = standardizer.standardize(record);

                    boolean firstField = true;
                    out.print(standardizedRecords + "|" + record + "|");
                    for (StandardizedRecord standardizedRecord: standardizedRecords) {
                        for (String fieldName: standardizedRecord.getFieldNames()) {
                            for (StandardizedField field: standardizedRecord.getFields(fieldName)) {
                                if (firstField) {
                                    firstField = false;
                                } else {
                                    out.print(" ");
                                }
                                // TODO: Supress extraInformation
                                out.print(field.getInputSymbol());
                            }
                        }
                    }
                    out.println();

                    if (logger.isLoggable(FINER)) {
                        logger.finer(record + " -> " + standardizedRecords);
                    }
                } catch (Exception e) {
                    e.printStackTrace(err);
                    err.println("Record number " + recordNumber + ": " + e.getMessage() + ". Record: '" + record + "'");
                    logger.warning("Record number " + recordNumber + ": " + e.getMessage() + ". Record: '" + record + "'");
                }
            }
        }

        in.close();
        out.flush(); out.close();
        err.flush(); err.close();

        long end = System.currentTimeMillis();
        long elapsedSeconds = (end - start) / 1000;
        System.err.println("Processed " + recordNumber + " records in " + elapsedSeconds + " seconds (" + (recordNumber / (double) elapsedSeconds) + " rec/sec)");
    }
}
