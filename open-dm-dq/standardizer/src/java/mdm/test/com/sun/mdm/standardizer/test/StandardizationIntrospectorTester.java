package com.sun.mdm.standardizer.test;

import java.io.File;
import java.util.logging.Logger;
import java.util.zip.ZipFile;

import com.sun.inti.components.util.ClassUtils;
import com.sun.inti.components.util.IOUtils;
import com.sun.mdm.standardizer.introspector.DataTypeDescriptor;
import com.sun.mdm.standardizer.introspector.StandardizationIntrospector;
import com.sun.mdm.standardizer.introspector.VariantDescriptor;

public class StandardizationIntrospectorTester {
    private final static Logger logger = Logger.getLogger(StandardizationIntrospectorTester.class.getName());
    
    public static void main(String[] args) throws Exception {
        File repositoryDirectory = new File(new File(System.getProperty("java.io.tmpdir")), "introspectorTesterRepository");
        IOUtils.recursiveDelete(repositoryDirectory);
        StandardizationIntrospector introspector = ClassUtils.loadDefaultService(StandardizationIntrospector.class);
        introspector.setRepositoryDirectory(repositoryDirectory);

        logger.info("Deploying 'Address' data type");
        introspector.deployDataType(new ZipFile("dist/deployment/Address.zip"));
        dump(introspector);

        logger.info("Deploying 'FR' variant of 'Address' data type");
        introspector.deployVariant("Address", new ZipFile("dist/deployment/Address/FR.zip"));
        dump(introspector);

        logger.info("Deploying 'PersonName' data type");
        introspector.deployDataType(new ZipFile("dist/deployment/PersonName.zip"));
        dump(introspector);

        logger.info("Undeploying variant 'UK' of data type 'Address'");
        introspector.undeploy("Address", "UK");
        dump(introspector);

        IOUtils.recursiveDelete(repositoryDirectory);
    }
    
    private static void dump(StandardizationIntrospector introspector) {
        for (DataTypeDescriptor dataTypeDescriptor: introspector.getDataTypes()) {
            logger.info("Data type: " + dataTypeDescriptor.getName());
            for (VariantDescriptor variantDescriptor: dataTypeDescriptor.getVariants()) {
                logger.info("    Variant: " + variantDescriptor.getName());
                for (String filename: variantDescriptor.getResourceFilenames()) {
                    logger.info("        Filename: " + filename);
                }
            }
        }
    }
}
