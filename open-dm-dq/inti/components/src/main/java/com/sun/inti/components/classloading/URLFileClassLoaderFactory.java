package com.sun.inti.components.classloading;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;

public class URLFileClassLoaderFactory implements FileClassLoaderFactory {
	public ClassLoader newClassLoader(File[] files, ClassLoader parentClassLoader) throws ClassLoadingException {
		try {
            URL[] urls = new URL[files.length];
            for (int i = 0; i < files.length; i++) {
            	urls[i] = files[i].toURI().toURL();
            }
            return new URLClassLoader(urls, parentClassLoader);
        } catch (Exception e) {
            throw new ClassLoadingException("Error creating class loader: " + e, e);
        }
	}
	
	public void close(ClassLoader classLoader) {}
}
