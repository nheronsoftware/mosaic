package com.sun.inti.components.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class FileInputStreamSource implements InputStreamSource {
    private File file;
    
    public FileInputStreamSource() {}
    public FileInputStreamSource(String name) { this(new File(name)); }
    public FileInputStreamSource(File file) { this.file = file; }
    
    public InputStream getInputStream() throws IOException {
        return new FileInputStream(file);
    }
    
    public File getFile() {
        return file;
    }
    public void setFile(File file) {
        this.file = file;
    }
}
