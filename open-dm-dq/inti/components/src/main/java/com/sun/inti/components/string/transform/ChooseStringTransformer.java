/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.inti.components.string.transform;

import java.util.LinkedList;
import java.util.List;

import com.sun.inti.components.string.match.CharSequenceMatcher;

public class ChooseStringTransformer implements StringTransformer {
    private List<WhenHandler> handlers;
    private StringTransformer otherwiseTransformer;

    public static class WhenHandler {
        private final CharSequenceMatcher matcher;
        private final StringTransformer transformer;

        public WhenHandler(final CharSequenceMatcher matcher, final StringTransformer transformer) {
            this.matcher = matcher;
            this.transformer = transformer;
        }
    }
    
    public ChooseStringTransformer() {}

    public ChooseStringTransformer(final StringTransformer otherwiseTransformer) {
        this.otherwiseTransformer = otherwiseTransformer;
    }

    public ChooseStringTransformer(final List<WhenHandler> handlers) {
        this(handlers, null);
    }

    public ChooseStringTransformer(final List<WhenHandler> handlers, final StringTransformer otherwiseTransformer) {
        this.handlers = handlers;
        this.otherwiseTransformer = otherwiseTransformer;
    }

    public String transform(final String string) {
        for (final WhenHandler handler : this.handlers) {
            if (handler.matcher.matches(string)) {
                return handler.transformer.transform(string);
            }
        }
        if (this.otherwiseTransformer != null) {
            return this.otherwiseTransformer.transform(string);
        }
        return string;
    }
    
    public ChooseStringTransformer add(CharSequenceMatcher matcher, StringTransformer transformer) {
    	if (this.handlers == null) {
    		this.handlers = new LinkedList<WhenHandler>();
    	}
    	this.handlers.add(new WhenHandler(matcher, transformer));
    	return this;
    }

	public List<WhenHandler> getHandlers() {
		return handlers;
	}

	public void setHandlers(List<WhenHandler> handlers) {
		this.handlers = handlers;
	}

	public StringTransformer getOtherwiseTransformer() {
		return otherwiseTransformer;
	}

	public void setOtherwiseTransformer(StringTransformer otherwiseTransformer) {
		this.otherwiseTransformer = otherwiseTransformer;
	}
}
