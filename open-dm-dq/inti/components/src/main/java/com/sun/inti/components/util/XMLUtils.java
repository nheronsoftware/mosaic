package com.sun.inti.components.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class XMLUtils {
    private static final Logger logger = Logger.getLogger(XMLUtils.class.getName());

    @SuppressWarnings("unused")
    private static final DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();

    public static Templates buildTemplates(URL stylesheetURL) {
        try {
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            
            Source stylesheetSource = new StreamSource(stylesheetURL.toURI().toString());

            Templates templates = transformerFactory.newTemplates(stylesheetSource);
            
            return templates;
        } catch (Exception e) {
            logger.warning("Error loading/compiling stylesheet '" + stylesheetURL + "': " + e);
            throw new IllegalArgumentException("Error loading/compiling stylesheet '" + stylesheetURL + "': " + e, e);
        }
    }
    
    public static Document parse(InputStream is) throws SAXException, IOException, ParserConfigurationException {
        return documentBuilderFactory.newDocumentBuilder().parse(is);
    }
}
