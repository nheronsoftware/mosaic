package com.sun.inti.components.string.transform;

public class DiacriticalMarkStringTransformer implements StringTransformer {

    private static CharTransformer transformer = new DiacriticalMarkCharTransformer();
    
    public String transform(String string) {
        final int len = string.length();
        String transformedString = new String(string);

        for (int i = 0; i < len; i++) {
            final char ch = transformedString.charAt(i);
            transformedString = transformedString.replace(ch, transformer.transform(ch));
        }
        return transformedString;    
    }

}
