package com.sun.inti.components.component;

import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.URL;
import java.util.Map;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;

import org.apache.xml.serializer.DOMSerializer;
import org.apache.xml.serializer.Method;
import org.apache.xml.serializer.OutputPropertiesFactory;
import org.apache.xml.serializer.Serializer;
import org.apache.xml.serializer.SerializerFactory;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.core.io.DescriptiveResource;
import org.w3c.dom.Document;

import com.sun.inti.components.io.OutputStreamSource;
import com.sun.inti.components.url.URLSource;
import com.sun.inti.components.util.ClassLoaderAware;
import com.sun.inti.components.util.LogUtils;
import com.sun.inti.components.util.XMLUtils;

public class BeanComponentManagerFactory extends AbstractComponentManagerFactory  {
    private static final Logger logger = Logger.getLogger(BeanComponentManagerFactory.class.getName());
    
    private static final DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
    
    private Templates templates;
    private URLSource urlSource;
    private OutputStreamSource outputStreamSource;
    private Serializer serializer;

    public ComponentManager newInstance(ClassLoader classLoader, Map<String, Object> parameters) throws ComponentException {
        try {
            if (urlSource instanceof ClassLoaderAware) {
                ((ClassLoaderAware) urlSource).setClassLoader(classLoader);
            }
            
            URL configurationURL = urlSource.getURL();
            if (configurationURL == null) {
                throw new NullPointerException("Can't find URL");
            }

            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document inputDocument = documentBuilder.parse(configurationURL.toURI().toString());
            Source source = new DOMSource(inputDocument);

            if (templates != null) {
                Document outputDocument = documentBuilder.newDocument();
                Result result = new DOMResult(outputDocument);
                Transformer transformer = templates.newTransformer();
                if (parameters != null) {
                	for (String parameterName: parameters.keySet()) {
                		Object parameterValue = parameters.get(parameterName);
                		if (parameterValue != null) {
                			transformer.setParameter(parameterName, parameterValue);
                		}
                	}
                }
                transformer.transform(source, result);
                
                inputDocument = outputDocument;
                
                if (outputStreamSource != null) {
                    if (serializer == null) {
                        java.util.Properties props = OutputPropertiesFactory.getDefaultMethodProperties(Method.XML);
                        props.setProperty("indent", "yes");
                        serializer = SerializerFactory.getSerializer(props);
                    }
                    
                    OutputStream os = outputStreamSource.getOutputStream();
                    Writer out = new OutputStreamWriter(os);
                    serializer.setWriter(out);
                    DOMSerializer domSerializer = serializer.asDOMSerializer();
                    domSerializer.serialize(inputDocument);
                    out.flush();
                    out.close();

                    serializer.reset();
                }
            }

            GenericApplicationContext context = null;
            ClassLoader threadClassLoader = Thread.currentThread().getContextClassLoader();
            try {
                Thread.currentThread().setContextClassLoader(classLoader);
                context = new GenericApplicationContext();
                XmlBeanDefinitionReader reader = new XmlBeanDefinitionReader(context);
                reader.registerBeanDefinitions(inputDocument, new DescriptiveResource(configurationURL.toURI().toString()));

                return new BeansComponentManager(context);
            } finally {
                Thread.currentThread().setContextClassLoader(threadClassLoader);
            }
        } catch (Exception e) {
            logger.warning("Error processing XML configuration file: " + e);
            throw new ComponentException("Error processing XML configuration file: " + e, e);
        }
    }

    private class BeansComponentManager implements ComponentManager {
        private ApplicationContext context;
        
        private BeansComponentManager(ApplicationContext context) {
            this.context = context;
        }
        
        public Object getComponent(String componentName) throws ComponentException {
            try {
                return context.getBean(componentName);
            } catch (Exception e) {
                logger.severe("Error getting bean '" + componentName + "': " + e);
                logger.severe(LogUtils.toString(e));
                logger.severe("Classpath: " + System.getProperty("java.class.path"));
                throw new ComponentException("Error getting bean '" + componentName + "': " + e, e);
            }
        }
    }
    
    public void setStylesheetURL(URL stylesheetURL) {
        templates = XMLUtils.buildTemplates(stylesheetURL);
    }

    public Templates getTemplates() {
        return templates;
    }

    public void setTemplates(Templates templates) {
        this.templates = templates;
    }

    public URLSource getUrlSource() {
        return urlSource;
    }

    public void setUrlSource(URLSource urlSource) {
        this.urlSource = urlSource;
    }

    public OutputStreamSource getOutputStreamSource() {
        return outputStreamSource;
    }

    public void setOutputStreamSource(OutputStreamSource outputStreamSource) {
        this.outputStreamSource = outputStreamSource;
    }
}
