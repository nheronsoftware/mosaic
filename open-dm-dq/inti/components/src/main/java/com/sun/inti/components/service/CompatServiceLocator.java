package com.sun.inti.components.service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.logging.Logger;

public class CompatServiceLocator extends AbstractServiceLocator {
    private static final Logger logger = Logger.getLogger(CompatServiceLocator.class.getName());
    
    public CompatServiceLocator() {}
    public CompatServiceLocator(ServiceMatcher serviceMatcher) { super(serviceMatcher); }

    protected Iterator<?> loadServices(final Class<?> serviceClass, final ClassLoader classLoader) {
        return new Iterator<Object>() {
            private String serviceQName;
            private BufferedReader in;
            private Object instance;
            
            {
                serviceQName = serviceClass.getName();
                InputStream is = classLoader.getResourceAsStream("META-INF/services/" + serviceQName);
                if (is == null) {
                    logger.warning("Can't open: META-INF/services/" + serviceQName);
                    throw new RuntimeException("Can't open: META-INF/services/" + serviceQName);
                }
                in = new BufferedReader(new InputStreamReader(is));
                
                nextInstance();
            }

            public boolean hasNext() {
                return instance != null;
            }

            public Object next() {
                Object currentInstance = instance;
                nextInstance();
                return currentInstance;
            }

            public void remove() {
                throw new UnsupportedOperationException("Can't remove");
            }
            
            private void nextInstance() {
                try {
                    String line;
                    instance = null;
                    while ((line = in.readLine()) != null) {
                        line = line.replaceAll("#.*", "").trim();
                        if (line.length() > 0) {
                            Class<?> clazz = classLoader.loadClass(line);
                            instance = clazz.newInstance();
                            return;
                        }
                    }
                } catch (Exception e) {
                    instance = null;
                    logger.warning("Error loading service '" + serviceQName + "': " + e);
                    throw new RuntimeException("Error loading service '" + serviceQName + "': " + e, e);
                } finally {
                    try {
                        if (instance == null) {
                            in.close();
                        }
                    } catch (Exception x) {}
                }
            }
        };
    }
}
