package com.sun.inti.components.string.transform;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class RegexBasedStringTransformer implements StringTransformer {
    private List<Substitution> substitutions;
    
    public static class Substitution {
        private Pattern pattern;
        private String replacement;
        private boolean global;
        
        public Substitution(Pattern pattern, String replacement) {
            this(pattern, replacement, false);
        }
        
        public Substitution(Pattern pattern, String replacement, boolean global) {
            this.pattern = pattern;
            this.replacement = replacement;
            this.global = global;
        }
    }
    
    public RegexBasedStringTransformer() {}
    
    public RegexBasedStringTransformer(List<Substitution> substitutions) {
        this.substitutions = substitutions;
    }
    
    public String transform(String term) {
        for (Substitution substitution: substitutions) {
            Matcher matcher = substitution.pattern.matcher(term); 
            if (matcher.find()) {
                if (substitution.global) {
                    return matcher.replaceAll(substitution.replacement);
                }
                return matcher.replaceFirst(substitution.replacement);
            }
        }
        return term;
    }

    public List<Substitution> getSubstitutions() { return substitutions; }
    public void setSubstitutions(List<Substitution> substitutions) { this.substitutions = substitutions; }
}
