/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.inti.components.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.List;

import com.sun.inti.components.record.Record;

public class InterfaceImplementor {
    private ClassLoader classLoader;
    private List<InterfaceDescriptor> descriptors;

    private Class<?>[] interfaces;

    private int objectCount = 0;
    private final List<Record> records = new ArrayList<Record>();

    private static final Object[] emptyArgs = new Object[0];

    public InterfaceImplementor() {
    }

    public InterfaceImplementor(final ClassLoader classLoader, final List<InterfaceDescriptor> descriptors) {
        this.classLoader = classLoader;
        this.descriptors = descriptors;

        this.interfaces = new Class[descriptors.size()];
        for (int i = 0; i < this.interfaces.length; i++) {
            this.interfaces[i] = descriptors.get(i).getInterfaceClass();
        }
    }

    public InterfaceImplementor(final List<InterfaceDescriptor> descriptors) {
        this(Thread.currentThread().getContextClassLoader(), descriptors);
    }

    public Class<?>[] getInterfaces() {
        return this.interfaces;
    }

    public Object newInstance() {
        final InvocationHandler handler = new InvocationHandler() {
            private Integer id = new Integer(InterfaceImplementor.this.objectCount++);

            {
                InterfaceImplementor.this.records.add(new Record());
            }

            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                if (args == null) {
                    args = InterfaceImplementor.emptyArgs;
                }

                if ("hashCode".equals(method.getName()) && args.length == 0) {
                    return this.id.hashCode();
                }

                if ("toString".equals(method.getName()) && args.length == 0) {
                    return this.id.toString();
                }

                if ("equals".equals(method.getName()) && args.length == 1) {
                    return this == args[0];
                }

                for (InterfaceDescriptor interfaceDescriptor : InterfaceImplementor.this.descriptors) {
                    if (interfaceDescriptor.interfaceClass.equals(method.getDeclaringClass())) {
                        for (MethodDescriptor methodDescriptor : interfaceDescriptor.methodDescriptors) {
                            if (method.getName().equals(methodDescriptor.getName())) {
                                if (args.length == methodDescriptor.argumentDescriptors.size()) {
                                    int i;
                                    for (i = 0; i < args.length; i++) {
                                        Class<?> argumentType = methodDescriptor.argumentDescriptors.get(i).getType();
                                        // TODO Verify usage of isAssignableFrom
                                        if (!(args[i] == null || args[i].getClass().isAssignableFrom(argumentType))) {
                                            break;
                                        }
                                    }
                                    if (i == args.length) {
                                        Record record = InterfaceImplementor.this.records.get(this.id.intValue());
                                        return methodDescriptor.execute(args, record);
                                    }
                                }
                            }
                        }
                    }
                }

                return null;
            }
        };

        return Proxy.newProxyInstance(this.classLoader, this.interfaces, handler);
    }
}
