package com.sun.inti.components.component;

import java.net.URL;
import java.util.Map;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;

import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.core.io.DescriptiveResource;
import org.w3c.dom.Document;


public class BeanComponentManager implements ComponentManager {
    private static final Logger logger = Logger.getLogger(BeanComponentManager.class.getName());
    
    private ApplicationContext context;
    
    public BeanComponentManager(URL configurationURL, URL stylesheetURL, ClassLoader classLoader, Map<String, Object> parameters) throws ComponentException {
        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();

            Document inputDocument = documentBuilder.parse(configurationURL.toURI().toString());

            if (stylesheetURL != null) {
                Source stylesheetSource = new StreamSource(stylesheetURL.toURI().toString());
                TransformerFactory transformerFactory = TransformerFactory.newInstance();

                Transformer transformer = transformerFactory.newTransformer(stylesheetSource);
                if (parameters != null) {
                    for (String parameterName: parameters.keySet()) {
                        Object parameterValue = parameters.get(parameterName);
                        if (parameterValue != null) {
                            transformer.setParameter(parameterName, parameterValue);
                        }
                    }
                }
                
                Document outputDocument = documentBuilder.newDocument();
                Source source = new DOMSource(inputDocument);
                Result result = new DOMResult(outputDocument);
                transformer.transform(source, result);
                
                inputDocument = outputDocument;
                
                // transformerFactory.newTransformer().transform(new DOMSource(inputDocument), new javax.xml.transform.stream.StreamResult(System.out));
            }

            ClassLoader threadClassLoader = Thread.currentThread().getContextClassLoader();
            GenericApplicationContext genericApplicationContext = new GenericApplicationContext();
            try {
                if (classLoader == null) {
                    classLoader = getClass().getClassLoader();
                }
                
                Thread.currentThread().setContextClassLoader(classLoader);
                genericApplicationContext = new GenericApplicationContext();
                XmlBeanDefinitionReader reader = new XmlBeanDefinitionReader(genericApplicationContext);
                reader.registerBeanDefinitions(inputDocument, new DescriptiveResource(configurationURL.toURI().toString()));

                this.context = genericApplicationContext;
            } finally {
                Thread.currentThread().setContextClassLoader(threadClassLoader);
            }
        } catch (Exception e) {
            logger.warning("Error processing XML configuration file: " + e);
            throw new ComponentException("Error processing XML configuration file: " + e, e);
        }

    }
    
    public Object getComponent(String componentName) throws ComponentException {
        return context.getBean(componentName);
    }
}
