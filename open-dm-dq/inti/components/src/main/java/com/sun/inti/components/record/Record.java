/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [propertyName of copyright owner]"
*/ 

package com.sun.inti.components.record;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class Record {
    private Map<String, Object> properties;

    public Record() {}
    
    public Record(Map<String, Object> properties) {
        this.properties = properties;
    }
    
    public Set<String> propertyNames() {
        return this.getProperties().keySet();
    }
    
    public boolean hasProperty(final String propertyName) {
        return this.getProperties().containsKey(propertyName);
    }
    
    @Override
	public String toString() {
    	return this.getProperties().toString();
    }
    
    @Override
    public boolean equals(Object o) {
        if (o == null) { return false; }
        if (!(o instanceof Record)) { return false; }
        Record that = (Record) o;
        if (that.properties == null) { return false; }
        return this.properties.equals(that.properties);
    }

    public Object get(final String propertyName) {
        return this.getProperties().get(propertyName);
    }

    public void set(final String propertyName, final Object value) {
        this.getProperties().put(propertyName, value);
    }
    
    public void set(final String propertyName, final String value) {
        this.getProperties().put(propertyName, value);
    }
    
    public String getString(final String propertyName) {
        Object value = this.getProperties().get(propertyName);
        if (value == null) {
            return null;
        }
        return value.toString();
    }

    public byte getByte(final String propertyName) {
        return (Byte) this.getProperties().get(propertyName);
    }

    public void set(final String propertyName, final byte b) {
        this.getProperties().put(propertyName, b);
    }

    public Byte getAsByte(final String propertyName) {
        return (Byte) this.getProperties().get(propertyName);
    }

    public void set(final String propertyName, final Byte b) {
        this.getProperties().put(propertyName, b);
    }

    public char getChar(final String propertyName) {
        return (Character) this.getProperties().get(propertyName);
    }

    public void set(final String propertyName, final char c) {
        this.getProperties().put(propertyName, c);
    }

    public Character getAsCharacter(final String propertyName) {
        return (Character) this.getProperties().get(propertyName);
    }

    public void set(final String propertyName, final Character c) {
        this.getProperties().put(propertyName, c);
    }

    public boolean getBoolean(final String propertyName) {
        final Boolean b = (Boolean) this.getProperties().get(propertyName);
        if (b == null) {
            return false;
        }
        return b;
    }

    public void set(final String propertyName, final boolean b) {
        this.getProperties().put(propertyName, b);
    }

    public Boolean getAsBoolean(final String propertyName) {
        return (Boolean) this.getProperties().get(propertyName);
    }

    public void set(final String propertyName, Boolean b) {
        if (b == null) {
            b = false;
        }
        this.getProperties().put(propertyName, b);
    }

    public void set(final String propertyName, final int i) {
        this.getProperties().put(propertyName, i);
    }

    public int getInt(final String propertyName) {
        return (Integer) this.getProperties().get(propertyName);
    }

    public void set(final String propertyName, final Integer i) {
        this.getProperties().put(propertyName, i);
    }

    public Integer getAsInteger(final String propertyName) {
        return (Integer) this.getProperties().get(propertyName);
    }

    public void set(final String propertyName, final short s) {
        this.getProperties().put(propertyName, s);
    }

    public short getShort(final String propertyName) {
        return (Short) this.getProperties().get(propertyName);
    }

    public void set(final String propertyName, final Short s) {
        this.getProperties().put(propertyName, s);
    }

    public Short getAsShort(final String propertyName) {
        return (Short) this.getProperties().get(propertyName);
    }

    public void set(final String propertyName, final long l) {
        this.getProperties().put(propertyName, l);
    }

    public long getLong(final String propertyName) {
        return (Long) this.getProperties().get(propertyName);
    }

    public void set(final String propertyName, final Long l) {
        this.getProperties().put(propertyName, l);
    }

    public Long getAsLong(final String propertyName) {
        return (Long) this.getProperties().get(propertyName);
    }

    public void set(final String propertyName, final float f) {
        this.getProperties().put(propertyName, f);
    }

    public float getFloat(final String propertyName) {
        return (Float) this.getProperties().get(propertyName);
    }

    public void set(final String propertyName, final Float f) {
        this.getProperties().put(propertyName, f);
    }

    public Float getAsFloat(final String propertyName) {
        return (Float) this.getProperties().get(propertyName);
    }

    public void set(final String propertyName, final double d) {
        this.getProperties().put(propertyName, d);
    }

    public double getDouble(final String propertyName) {
        return (Double) this.getProperties().get(propertyName);
    }

    public void set(final String propertyName, final Double d) {
        this.getProperties().put(propertyName, d);
    }

    public Double getAsDouble(final String propertyName) {
        return (Double) this.getProperties().get(propertyName);
    }

    public void set(final String propertyName, final BigDecimal bigDecimal) {
        this.getProperties().put(propertyName, bigDecimal);
    }

    public void setDecimal(final String propertyName, final String string) {
        this.getProperties().put(propertyName, new BigDecimal(string));
    }

    public void setDecimal(final String propertyName, final String source, final DecimalFormat format) throws ParseException {
        this.getProperties().put(propertyName, new BigDecimal(format.parse(source).toString()));
    }

    public void setDecimal(final String propertyName, final String source, final String formatMask) throws ParseException {
        this.setDecimal(propertyName, source, new DecimalFormat(formatMask));
    }

    public BigDecimal getDecimal(final String propertyName) {
        return (BigDecimal) this.getProperties().get(propertyName);
    }

    public void set(final String propertyName, final Date date) {
        this.getProperties().put(propertyName, date);
    }

    public void set(final String propertyName, final String source, final DateFormat format) throws ParseException {
        this.getProperties().put(propertyName, format.parse(source));
    }

    public void setDate(final String propertyName, final String source, final String formatMask) throws ParseException {
        this.set(propertyName, source, new SimpleDateFormat(formatMask));
    }

    public Date getDate(final String propertyName) {
        return (Date) this.getProperties().get(propertyName);
    }
    
    public void set(final String propertyName, final StringBuilder stringBuilder) {
        this.getProperties().put(propertyName, stringBuilder);
    }
    
    public StringBuilder getStringBuilder(final String propertyName) {
        return (StringBuilder) this.getProperties().get(propertyName);
    }
    
    private Map<String, Object> getProperties() {
        if (properties == null) {
            properties = new LinkedHashMap<String, Object>();
        }
        return properties;
    }
}
