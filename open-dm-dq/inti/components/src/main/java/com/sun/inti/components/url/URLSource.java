package com.sun.inti.components.url;

import java.io.IOException;
import java.net.URL;

public interface URLSource {
    public URL getURL() throws IOException;
}
