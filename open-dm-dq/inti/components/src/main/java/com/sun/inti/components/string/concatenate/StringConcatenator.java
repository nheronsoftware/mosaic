package com.sun.inti.components.string.concatenate;

import java.util.List;

public interface StringConcatenator {
	public String concatenate(List<String> parts);
}
