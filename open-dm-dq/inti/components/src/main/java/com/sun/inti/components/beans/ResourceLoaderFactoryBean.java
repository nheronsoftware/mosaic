package com.sun.inti.components.beans;

import org.springframework.beans.factory.BeanClassLoaderAware;
import org.springframework.beans.factory.FactoryBean;

public class ResourceLoaderFactoryBean implements FactoryBean, BeanClassLoaderAware {
	private String resourceName;
	private ClassLoader classLoader;
	
	public Object getObject() throws Exception {
		return classLoader.getResource(resourceName);
	}

	public Class<?> getObjectType() {
		return null;
	}

	public boolean isSingleton() {
		return false;
	}

	public void setBeanClassLoader(ClassLoader classLoader) {
		this.classLoader = classLoader;
	}

	public String getResourceName() {
		return resourceName;
	}

	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}
}
