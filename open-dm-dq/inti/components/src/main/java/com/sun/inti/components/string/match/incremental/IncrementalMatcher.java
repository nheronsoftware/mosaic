package com.sun.inti.components.string.match.incremental;

import java.util.Iterator;

public interface IncrementalMatcher {
	public int match(Iterator<String> tokens);
}
