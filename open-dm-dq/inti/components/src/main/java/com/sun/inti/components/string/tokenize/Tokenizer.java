package com.sun.inti.components.string.tokenize;

import java.util.List;

public interface Tokenizer {
	public List<String> tokenize(String string);
}
