package com.sun.inti.components.logging;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

public class BasicFormatter extends Formatter {
    private static final String FORMAT_MASK = "hh:mm:ss";
    private final DateFormat format = new SimpleDateFormat(FORMAT_MASK);
    
    @Override
    public String format(LogRecord record) {
        final StringBuilder builder = new StringBuilder();
        String className = record.getSourceClassName();
        className = className.substring(className.lastIndexOf('.') + 1);
        builder.append(className);
        builder.append('.');
        builder.append(record.getSourceMethodName());
        builder.append(" [");
        builder.append(format.format(new Date()));
        builder.append("]: ");
        builder.append(record.getMessage());
        builder.append('\n');
        return builder.toString();
    }
}
