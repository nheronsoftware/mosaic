package com.sun.inti.components.component;

public class ComponentException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public ComponentException(String message, Throwable cause) {
        super(message, cause);
    }

    public ComponentException(String message) {
        super(message);
    }

    public ComponentException(Throwable cause) {
        super(cause);
    }
}
