/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.inti.components.string.transform;

import java.util.LinkedList;
import java.util.List;

public class CompoundStringTransformer implements StringTransformer {

    private List<StringTransformer> transformers;

    private boolean stopOnEmpty = false;

    public CompoundStringTransformer() {

    }

    public CompoundStringTransformer(final List<StringTransformer> transformers, final boolean stopOnEmpty) {
        this.transformers = transformers;
        this.stopOnEmpty = stopOnEmpty;
    }

    public CompoundStringTransformer(final List<StringTransformer> transformers) {
        this(transformers, true);
    }

    public String transform(String string) {
        for (final StringTransformer transformer : this.transformers) {
            string = transformer.transform(string);
            if (this.stopOnEmpty && (string == null || string.length() == 0)) {
                return string;
            }
        }

        return string;
    }
    
    public CompoundStringTransformer add(StringTransformer transformer) {
    	if (this.transformers == null) {
    		this.transformers = new LinkedList<StringTransformer>();
    	}
    	this.transformers.add(transformer);
    	return this;
    }

    public List<StringTransformer> getTransformers() {
        return this.transformers;
    }

    public void setTransformers(final List<StringTransformer> transformers) {
        this.transformers = transformers;
    }

}
