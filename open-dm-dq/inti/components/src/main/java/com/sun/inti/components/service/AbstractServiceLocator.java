package com.sun.inti.components.service;

import java.util.Iterator;
import java.util.logging.Logger;

public abstract class AbstractServiceLocator implements ServiceLocator {
    private static final Logger logger = Logger.getLogger(AbstractServiceLocator.class.getName());
    
    protected ServiceMatcher serviceMatcher;
    
    public AbstractServiceLocator() {}
    
    public AbstractServiceLocator(ServiceMatcher serviceMatcher) {
        this.serviceMatcher = serviceMatcher;
    }

    public Object locateService(Class<?> serviceClass) throws ServiceLocationException {
        return locateService(serviceClass, serviceClass.getClassLoader());
    }

    public Object locateService(Class<?> serviceClass, ClassLoader classLoader) {
        Iterator<?> iterator = loadServices(serviceClass, classLoader);
        while (iterator.hasNext()) {
            Object service = iterator.next();
            if (serviceMatcher == null || serviceMatcher.matches(service)) {
                return service;
            }
        }
        
        logger.warning("No suitable implementation for service '" + serviceClass.getName() + "'");
        throw new RuntimeException("No suitable implementation for service '" + serviceClass.getName() + "'");
    }
    
    protected abstract Iterator<?> loadServices(Class<?> serviceClass, ClassLoader classLoader);
}
