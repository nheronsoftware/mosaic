package com.sun.inti.components.string.format;

public abstract class AbstractFormatter implements Formatter {
	public String format(Object object) {
		if (object == null) {
			return null;
		}
		return object.toString();
	}
}
