package com.sun.inti.components.string.transform;

public interface CharTransformer {
    public char transform(char c);
}
