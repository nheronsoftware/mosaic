/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.inti.components.string.transform;

import java.util.regex.Pattern;

public class SimpleRegexStringTransformer extends NonNullStringTransformer {
    private Pattern pattern;
    private String replacement;
    private boolean global;

    public SimpleRegexStringTransformer() {
    }

    public SimpleRegexStringTransformer(final String regex, final String replacement) {
    	this(Pattern.compile(regex), replacement, true);
    }

    public SimpleRegexStringTransformer(final String regex, final String replacement, final boolean global) {
    	this(Pattern.compile(regex), replacement, global);
    }

    public SimpleRegexStringTransformer(final Pattern pattern, final String replacement) {
    	this(pattern, replacement, true);
    }

    public SimpleRegexStringTransformer(final Pattern pattern, final String replacement, final boolean global) {
        this.pattern = pattern;
        this.replacement = replacement;
        this.global = global;
    }

    @Override
    public String doTransform(final String string) {
        if (this.global) {
            return this.pattern.matcher(string).replaceAll(this.replacement);
        } else {
            return this.pattern.matcher(string).replaceFirst(this.replacement);
        }
    }

    public boolean isGlobal() {
        return this.global;
    }

    public void setGlobal(final boolean global) {
        this.global = global;
    }

    public Pattern getPattern() {
        return this.pattern;
    }

    public void setPattern(final Pattern pattern) {
        this.pattern = pattern;
    }

    public String getReplacement() {
        return this.replacement;
    }

    public void setReplacement(final String replacement) {
        this.replacement = replacement;
    }

}
