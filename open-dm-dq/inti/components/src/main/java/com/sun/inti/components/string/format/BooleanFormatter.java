package com.sun.inti.components.string.format;

public class BooleanFormatter implements Formatter {
	private String trueString = "true";
	private String falseString = "false";
	
	public BooleanFormatter() {}
	
	public BooleanFormatter(String trueString, String falseString) {
		this.trueString = trueString;
		this.falseString = falseString;
	}
	
	public String format(Object object) {
		if (object == null) {
			return this.falseString;
		}
		boolean value = (Boolean) object;
		if (value) {
			return this.trueString;
		}
		return this.falseString;
	}

	public Object parse(String string) {
		if (string == null) {
			return false;
		}
		if (this.trueString.equals(string)) {
			return true;
		}
		return false;
	}
}
