/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.inti.components.beans;

import java.util.List;

import org.springframework.beans.factory.FactoryBean;

import com.sun.inti.components.proxy.InterfaceDescriptor;
import com.sun.inti.components.proxy.InterfaceImplementor;

public class InterfaceImplementorFactoryBean implements FactoryBean {
    private boolean singleton;
    private InterfaceImplementor interfaceImplementor;

    private Object instance;
    private Class<?> objectType = null;

    public InterfaceImplementorFactoryBean() {
    }

    public InterfaceImplementorFactoryBean(final InterfaceImplementor interfaceImplementor, final boolean singleton) {
        this.interfaceImplementor = interfaceImplementor;
        this.singleton = singleton;
    }

    public InterfaceImplementorFactoryBean(final ClassLoader classLoader, final List<InterfaceDescriptor> descriptors, final boolean singleton) {
        this(new InterfaceImplementor(classLoader, descriptors), singleton);
    }

    public InterfaceImplementorFactoryBean(final List<InterfaceDescriptor> descriptors, final boolean singleton) {
        this(Thread.currentThread().getContextClassLoader(), descriptors, singleton);
    }

    public InterfaceImplementorFactoryBean(final List<InterfaceDescriptor> descriptors) {
        this(Thread.currentThread().getContextClassLoader(), descriptors, false);
    }

    public Object getObject() throws Exception {
        if (this.singleton) {
            if (this.instance == null) {
                this.instance = this.newInstance();
            }
            return this.instance;

        }
        return this.newInstance();
    }

    public Class<?> getObjectType() {
        if (this.objectType == null && this.interfaceImplementor.getInterfaces().length == 1) {
            this.objectType = this.interfaceImplementor.getInterfaces()[0];
        }
        return this.objectType;
    }

    public boolean isSingleton() {
        return this.singleton;
    }

    private Object newInstance() {
        return this.interfaceImplementor.newInstance();
    }
}
