package com.sun.inti.components.string.match;

public class FixedStringMatcher implements StringMatcher {
    private String matchedString;
    
    public FixedStringMatcher(String matchedString) {
        this.matchedString = matchedString;
    }

    public boolean matches(String string) {
        if (matchedString == null) {
            return string == null;
        }
        
        return matchedString.equals(string);
    }
}
