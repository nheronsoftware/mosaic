/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

/**
 * 
 */
package com.sun.inti.components.proxy;

import java.util.List;

public class InterfaceDescriptor {
    Class<?> interfaceClass;
    List<MethodDescriptor> methodDescriptors;

    public InterfaceDescriptor() {
    }

    public InterfaceDescriptor(final Class<?> interfaceClass, final List<MethodDescriptor> methodDescriptors) {
        this.interfaceClass = interfaceClass;
        this.methodDescriptors = methodDescriptors;
    }

    public Class<?> getInterfaceClass() {
        return this.interfaceClass;
    }

    public void setInterfaceClass(final Class<?> interfaceClass) {
        this.interfaceClass = interfaceClass;
    }

    public List<MethodDescriptor> getMethodDescriptors() {
        return this.methodDescriptors;
    }

    public void setMethodDescriptors(final List<MethodDescriptor> methodDescriptors) {
        this.methodDescriptors = methodDescriptors;
    }
}