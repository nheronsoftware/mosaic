/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.inti.components.record;

import java.lang.reflect.Method;

import com.sun.inti.components.string.format.Formatter;

public class PropertyDescriptor {
	private String name;
	private Formatter formatter;
	private Object defaultValue;
	
	public PropertyDescriptor() {}
	
	public PropertyDescriptor(String name, Formatter formatter, Object defaultValue) {
		this.name = name;
		this.formatter = formatter;
		this.defaultValue = defaultValue;
	}
	
	public Object getValue(String string) {
		Object propertyValue = string;
		if (this.formatter != null) {
			propertyValue = this.formatter.parse(string);
		}
		if (propertyValue == null) {
			propertyValue = this.defaultValue;
		}
		return propertyValue;
	}
	
	public static Object populate(Class<?> clazz, Record record) throws Exception {
		Object object = clazz.newInstance();
		populate(object, record);
		return object;
	}
	
	public static void populate(Object object, Record record) {
		if (object != null) {
			for (String propertyName: record.propertyNames()) {
				try {
					java.beans.PropertyDescriptor propertyDescriptor = new java.beans.PropertyDescriptor(propertyName, object.getClass());
					Method writeMethod = propertyDescriptor.getWriteMethod();
					writeMethod.invoke(object, record.get(propertyName));
				} catch (Exception e) {}
			}
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Formatter getFormatter() {
		return formatter;
	}

	public void setFormatter(Formatter formatter) {
		this.formatter = formatter;
	}

	public Object getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(Object defaultValue) {
		this.defaultValue = defaultValue;
	}
}