package com.sun.inti.components.classloading;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.JarURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Logger;

public class JarCache {
	private final Logger logger = Logger.getLogger(JarCache.class.getName());

	Map<String, JarFile> openJarFiles = new HashMap<String, JarFile>();
	Map<String, String> resourceIndex = Collections.synchronizedMap(new HashMap<String, String>());
	URLStreamHandler urlStreamHandler = new JarStreamHandlerImpl();

	public static class JarFileImpl extends JarFile {
		private final Logger logger = Logger.getLogger(JarFileImpl.class.getName());

		public JarFileImpl(final String name) throws IOException {
			super(name);
		}

		@Override
		public void close() {
			// don't close: in case somebody does this:

			// JarURLConnection c =
			// (JarURLConnection)getResource("jar:file:blah.jar").openConnection();
			// c.getJarFile().close();

		}

		public void closeImpl() throws IOException {
			logger.info("Closing jar file implementation '" + getName() + "'");
			super.close();
		}

		@Override
		protected void finalize() throws IOException {
			this.closeImpl();
		}
	}

	public class JarURLConnectionImpl extends JarURLConnection {
		JarFile jarFile;

		public JarURLConnectionImpl(final URL url) throws MalformedURLException {
			super(url);
		}

		@Override
		public JarFile getJarFile() throws IOException {
			if (this.jarFile == null) {
				final URL jarFileURL = this.getJarFileURL();
				synchronized (JarCache.this.openJarFiles) {
					this.jarFile = JarCache.this.openJarFiles.get(jarFileURL.getPath());
					if (this.jarFile == null) {
						this.jarFile = new JarFileImpl(jarFileURL.getPath());
						JarCache.this.openJarFiles.put(jarFileURL.getPath(), this.jarFile);
					}
				}
			}
			return this.jarFile;
		}

		@Override
		public InputStream getInputStream() throws IOException {
			return this.getJarFile().getInputStream(this.getJarFile().getEntry(this.getEntryName()));
		}

		@Override
		public void connect() throws IOException {
			// jarFileURLConnection = jarFileURL.openConnection();
		}
	}

	class JarStreamHandlerImpl extends URLStreamHandler {
		@Override
		protected URLConnection openConnection(final URL url) throws IOException {
			return new JarURLConnectionImpl(url);
		}
	}

	public URLStreamHandler getURLStreamHandler() {
		return this.urlStreamHandler;
	}

	public URL createURL(final URL jarFileURL, final String entry) throws MalformedURLException {
		final URL url = new URL("jar", null, -1, jarFileURL.toString() + "!/" + (entry == null ? "" : entry), this.getURLStreamHandler());
		return url;
	}

	public void close() {
		logger.info("Closing jar cache");
		synchronized (this.openJarFiles) {
			for (final JarFile jarFile : this.openJarFiles.values()) {
				try {
					logger.info("Closing jar: " + jarFile.getName());
					((JarFileImpl) jarFile).closeImpl();
				} catch (final IOException e) {
					logger.warning("Error closing jar file '" + jarFile.getName() + "': " + e);
					e.printStackTrace(); // FIXME
				}
			}
			this.resourceIndex.clear();
			this.openJarFiles.clear();
		}
	}

	public InputStream getResourceAsStream(String fileName) {
		fileName = fileName.replace('\\', '/');
		if (fileName.startsWith("/")) {
			fileName = fileName.substring(1);
		} else if (fileName.startsWith("./")) {
			fileName = fileName.substring(2);
		}
		final String key = this.resourceIndex.get(fileName);
		synchronized (this.openJarFiles) {
			if (key != null) {
				final JarFile jarFile = this.openJarFiles.get(key);
				if (jarFile != null) {
					try {
						final JarEntry entry = jarFile.getJarEntry(fileName);
						return jarFile.getInputStream(entry);
					} catch (final IOException exc) {
						throw new RuntimeException(exc);
					}
				}
			}
			for (final Map.Entry<String, JarFile> e : this.openJarFiles.entrySet()) {
				final JarFile jarFile = e.getValue();
				final JarEntry entry = jarFile.getJarEntry(fileName);
				if (entry != null) {
					try {
						this.resourceIndex.put(fileName, e.getKey());
						return jarFile.getInputStream(entry);
					} catch (final IOException exc) {
						throw new RuntimeException(exc);
					}
				}
			}
		}
		return null;
	}

	public URL findResource(String fileName) {
		fileName = fileName.replace('\\', '/');
		if (fileName.startsWith("/")) {
			fileName = fileName.substring(1);
		} else if (fileName.startsWith("./")) {
			fileName = fileName.substring(2);
		}
		final String key = this.resourceIndex.get(fileName);
		synchronized (this.openJarFiles) {
			if (key != null) {
				final JarFile jarFile = this.openJarFiles.get(key);
				if (jarFile != null) {
					try {
						return this.createURL(new File(jarFile.getName()).toURI().toURL(), fileName);
					} catch (final MalformedURLException exc) {
						throw new RuntimeException(exc);
					}
				}
			}
			for (final Map.Entry<String, JarFile> e : this.openJarFiles.entrySet()) {
				final JarFile jarFile = e.getValue();
				final JarEntry entry = jarFile.getJarEntry(fileName);
				if (entry != null) {
					try {
						this.resourceIndex.put(fileName, e.getKey());
						return this.createURL(new File(jarFile.getName()).toURI().toURL(), fileName);
					} catch (final MalformedURLException exc) {
						throw new RuntimeException(exc);
					}
				}
			}
		}
		return null;
	}

	public Vector<URL> findResources(String fileName) {
		fileName = fileName.replace('\\', '/');
		if (fileName.startsWith("/")) {
			fileName = fileName.substring(1);
		} else if (fileName.startsWith("./")) {
			fileName = fileName.substring(2);
		}
		final Vector<URL> result = new Vector<URL>();
		synchronized (this.openJarFiles) {
			for (final JarFile jarFile : this.openJarFiles.values()) {
				final JarEntry entry = jarFile.getJarEntry(fileName);
				if (entry != null) {
					try {
						result.add(this.createURL(new File(jarFile.getName()).toURI().toURL(), fileName));
					} catch (final MalformedURLException exc) {
						throw new RuntimeException(exc);
					}

				}
			}
		}
		return result;
	}

	public void addJar(final String fileName) throws IOException {
		try {
			this.addJar(new JarFileImpl(fileName));
		} catch (final IOException e) {
			throw new RuntimeException("jar file: " + fileName, e);
		}
	}

	public void addJar(final JarFile file) throws IOException {
		final String name = file.getName();
		final File f = new File(name);
		final URL url = f.toURI().toURL();
		synchronized (this.openJarFiles) {
			final JarFileImpl prev = (JarFileImpl) this.openJarFiles.get(url.getPath());
			if (prev != null) {
				prev.closeImpl();
			}
			this.openJarFiles.put(url.getPath(), new JarFileImpl(file.getName()));
		}
	}

	public URL[] getURLs() {
		final List<URL> list = new LinkedList<URL>();
		synchronized (this.openJarFiles) {
			for (final JarFile jarFile : this.openJarFiles.values()) {
				final String fileName = jarFile.getName();
				try {
					list.add(this.createURL(new File(fileName).toURI().toURL(), null));
				} catch (final MalformedURLException exc) {
					throw new RuntimeException(exc);
				}
			}
		}
		final URL[] urls = new URL[list.size()];
		list.toArray(urls);
		return urls;
	}
}
