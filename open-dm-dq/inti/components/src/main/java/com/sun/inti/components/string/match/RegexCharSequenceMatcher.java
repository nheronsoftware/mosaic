/*
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
*
* Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
*
* The contents of this file are subject to the terms of the Common
* Development and Distribution License ("CDDL")(the "License"). You
* may not use this file except in compliance with the License.
*
* You can obtain a copy of the License at
* https://open-esb.dev.java.net/public/CDDLv1.0.html
* or mural/license.txt. See the License for the specific language
* governing permissions and limitations under the License. *
* When distributing Covered Code, include this CDDL Header Notice
* in each file and include the License file at mural/license.txt.
* If applicable, add the following below the CDDL Header, with the
* fields enclosed by brackets [] replaced by your own identifying
* information: "Portions Copyrighted [year] [name of copyright owner]"
*/ 

package com.sun.inti.components.string.match;

import java.util.regex.Pattern;

public class RegexCharSequenceMatcher implements CharSequenceMatcher {
    private final Pattern pattern;

    public RegexCharSequenceMatcher(final String regex) {
    	this(Pattern.compile(regex));
    }
    
    public RegexCharSequenceMatcher(final Pattern pattern) {
        this.pattern = pattern;
    }

    public boolean matches(final CharSequence sequence) {
        return this.pattern.matcher(sequence).find();
    }
}
