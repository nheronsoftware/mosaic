package com.sun.inti.components.url;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class FileURLSource implements URLSource {
    private URL url;
    
    public FileURLSource() {}
    
    public FileURLSource(File file) throws MalformedURLException {
        setFile(file);
    }

    public URL getURL() throws IOException {
        return url;
    }

    public void setFile(File file) throws MalformedURLException {
        this.url = file.toURI().toURL();
    }
}
