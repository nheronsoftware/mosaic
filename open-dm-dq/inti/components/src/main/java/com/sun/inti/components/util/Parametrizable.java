package com.sun.inti.components.util;

import java.util.Map;

public interface Parametrizable {
    public void setParameters(Map<String, Object> parameters);
}
