package com.sun.inti.components.util;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Logger;

import com.sun.inti.components.component.BeanComponentManagerFactory;
import com.sun.inti.components.component.ComponentException;
import com.sun.inti.components.component.ComponentManager;
import com.sun.inti.components.url.FileURLSource;
import com.sun.inti.components.url.ResourceURLSource;

public class ComponentUtils {
    private static final Logger logger = Logger.getLogger(ComponentUtils.class.getName());

    public static ComponentManager getComponentManager(String configurationResourceName, URL stylesheetURL, ClassLoader classLoader) {
        BeanComponentManagerFactory factory = new BeanComponentManagerFactory();
        factory.setUrlSource(new ResourceURLSource(configurationResourceName));
        factory.setStylesheetURL(stylesheetURL);
        ComponentManager componentManager = factory.newInstance(classLoader);
        return componentManager;
    }

    public static ComponentManager getComponentManager(File configurationFile, URL stylesheetURL, ClassLoader classLoader) {
        BeanComponentManagerFactory factory = new BeanComponentManagerFactory();
        try {
            factory.setUrlSource(new FileURLSource(configurationFile));
        } catch (MalformedURLException e) {
            logger.warning("Malformed URL: '" + configurationFile.getAbsolutePath() + "': " + e);
            throw new ComponentException("Malformed URL: '" + configurationFile.getAbsolutePath() + "': " + e);
        }
        factory.setStylesheetURL(stylesheetURL);
        ComponentManager componentManager = factory.newInstance(classLoader);
        return componentManager;
    }
}
