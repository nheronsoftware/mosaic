package com.sun.inti.components.string.match.incremental;

import java.util.List;
import java.util.regex.Pattern;

public abstract class AbstractRegexIncrementalMatcher implements IncrementalMatcher {
    protected List<RegexMatcher> regexMatchers;
    
    // TODO Add 'global' flag to control find/matches 
    public static class RegexMatcher {
        Pattern pattern;
        List<Pattern> exceptions;
        
        public RegexMatcher(Pattern pattern) {
            this(pattern, null);
        }
        
        public RegexMatcher(Pattern pattern, List<Pattern> exceptions) {
            this.pattern = pattern;
            this.exceptions = exceptions;
        }
    }
    
    public AbstractRegexIncrementalMatcher() {}
    public AbstractRegexIncrementalMatcher(List<RegexMatcher> matchers) { this.regexMatchers = matchers; }
    
    // TODO Provide a string representation showing exceptions
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (RegexMatcher regexMatcher: regexMatchers) {
            sb.append(regexMatcher.pattern);
            sb.append(' ');
        }
        sb.setLength(sb.length() - 1);
        return sb.toString();
    }

    public List<RegexMatcher> getRegexMatchers() { return regexMatchers; }
    public void setRegexMatchers(List<RegexMatcher> regexMatchers) { this.regexMatchers = regexMatchers; }
}
