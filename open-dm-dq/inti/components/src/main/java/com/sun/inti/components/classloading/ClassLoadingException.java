package com.sun.inti.components.classloading;

public class ClassLoadingException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public ClassLoadingException(String message) {
        super(message);
    }

    public ClassLoadingException(Throwable cause) {
        super(cause);
    }

    public ClassLoadingException(String message, Throwable cause) {
        super(message, cause);
    }
}
