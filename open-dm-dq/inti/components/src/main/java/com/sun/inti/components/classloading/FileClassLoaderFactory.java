package com.sun.inti.components.classloading;

import java.io.File;

public interface FileClassLoaderFactory {
	public ClassLoader newClassLoader(File[] files, ClassLoader parentClassLoader) throws ClassLoadingException;
	public void close(ClassLoader classLoader) throws ClassLoadingException;
}
