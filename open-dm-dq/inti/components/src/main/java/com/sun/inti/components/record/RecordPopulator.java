package com.sun.inti.components.record;


public interface RecordPopulator {
	public void populateRecord(Record record, String recordString);
}
