package com.sun.inti.components.io;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class URLInputStreamSource implements InputStreamSource {
    private URL url;
    
    public URLInputStreamSource() {}
    
    public URLInputStreamSource(URL url) { this.url = url; }
    
    public InputStream getInputStream() throws IOException {
        return url.openStream();
    }

    public URL getUrl() {
        return url;
    }

    public void setUrl(URL url) {
        this.url = url;
    }
}
