package com.sun.inti.components.string.transform;

public class TransliterationStringTransformer implements StringTransformer {
    private char[] source;
    private char[] replacement;
    
    public TransliterationStringTransformer(String sourceString, String replacementString) {
        this.source = sourceString.toCharArray();
        this.replacement = replacementString.toCharArray();
    }
    
    public TransliterationStringTransformer(char[] source, char[] replacement) {
        this.source = source;
        this.replacement = replacement;
    }
    
    public String transform(String string) {
        StringBuilder sb = new StringBuilder();
        for (char ch: string.toCharArray()) {
            for (int i = 0; i < source.length; i++) {
            	if (i > replacement.length) {
            		ch = '\0';
            		break;
            	}
                if (ch == source[i]) {
                   ch = replacement[i];
                   break;
                }
            }
            if (ch != '\0') {
                sb.append(ch);
            }
        }
        return sb.toString();
    }

}
