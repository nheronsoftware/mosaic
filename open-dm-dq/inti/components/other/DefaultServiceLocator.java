package com.sun.inti.components.service;

import java.util.Iterator;
import java.util.ServiceLoader;

public class DefaultServiceLocator extends AbstractServiceLocator {
    public DefaultServiceLocator() {}
    public DefaultServiceLocator(ServiceMatcher serviceMatcher) { super(serviceMatcher); }
    
    protected Iterator<?> loadServices(Class<?> serviceClass, ClassLoader classLoader) {
        return ServiceLoader.load(serviceClass, classLoader).iterator();
    }
}
