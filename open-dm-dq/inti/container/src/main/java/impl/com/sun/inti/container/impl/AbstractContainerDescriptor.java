package com.sun.inti.container.impl;

import static com.sun.inti.container.impl.ContainerConstants.RESOURCE_DIRECTORY_NAME;

import java.io.File;
import java.io.FileFilter;
import java.util.Collections;
import java.util.Map;

import com.sun.inti.components.util.IOUtils;
import com.sun.inti.container.ContainerDescriptor;
import com.sun.inti.container.ContainerException;

public abstract class AbstractContainerDescriptor implements ContainerDescriptor {
    private String name;
    private String description;
    private File baseDirectory;
    private Map<String, Object> parameters;
    
    public String[] getResourceFilenames() {
        File[] files = IOUtils.recursiveListFiles(getResourceDirectory(), new FileFilter() {
        	private String[] excludedNames = {
        		"CVS", "META-INF"	
        	};
            public boolean accept(File file) {
                return file.isFile() && !isExcluded(file.getPath());
            }
            private boolean isExcluded(String name) {
            	for (String excludedName: excludedNames) {
            		if (name.contains(excludedName)) {
            			return true;
            		}
            	}
            	return false;
            }
        });
        
        int pos = getResourceDirectory().getPath().length();
        String[] filenames = new String[files.length];
        for (int i = 0; i < filenames.length; i++) {
            filenames[i] = files[i].getPath().substring(pos + 1).replace(File.separatorChar, '/');
        }

        return filenames;
    }

    public File getResourceFile(String filename) throws ContainerException {
        File file = new File( getResourceDirectory(), filename);
        if (!file.isFile()) {
            throw new ContainerException("No such resource file: '" + filename + "'");
        }
        return file;
    }
    
    private File getResourceDirectory() {
        return new File(baseDirectory, RESOURCE_DIRECTORY_NAME);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public File getBaseDirectory() {
        return baseDirectory;
    }

    public void setBaseDirectory(File baseDirectory) {
        this.baseDirectory = baseDirectory;
    }
    @SuppressWarnings("unchecked")
    public Map<String, Object> getParameters() {
        if (parameters == null) {
            return Collections.EMPTY_MAP;
        }
        return Collections.unmodifiableMap(parameters);
    }
    
    public void setParameters(Map<String, Object> parameters) {
        this.parameters = parameters;
    }
}
