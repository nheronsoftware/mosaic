package com.sun.inti.container.impl.introspector;
import static com.sun.inti.container.impl.ContainerConstants.SERVICE_INSTANCE_COMPONENT_NAME;
import static com.sun.inti.container.impl.ContainerConstants.SERVICE_INSTANCE_FILENAME;
import static com.sun.inti.container.impl.ContainerConstants.SERVICE_INSTANCE_STYLESHEET;
import static com.sun.inti.container.impl.ContainerConstants.SERVICE_TYPE_COMPONENT_NAME;
import static com.sun.inti.container.impl.ContainerConstants.SERVICE_TYPE_FILENAME;
import static com.sun.inti.container.impl.ContainerConstants.SERVICE_TYPE_STYLESHEET;
import static com.sun.inti.container.impl.ContainerImpl.directoryFilter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

import org.w3c.dom.Document;

import com.sun.inti.components.component.ComponentManager;
import com.sun.inti.components.util.ComponentUtils;
import com.sun.inti.components.util.IOUtils;
import com.sun.inti.components.util.XMLUtils;
import com.sun.inti.container.ContainerDescriptor;
import com.sun.inti.container.ContainerException;
import com.sun.inti.container.ContainerIntrospector;
import com.sun.inti.container.ServiceInstanceDescriptor;
import com.sun.inti.container.ServiceTypeDescriptor;

public class ContainerIntrospectorImpl implements ContainerIntrospector {
    private static final Logger logger = Logger.getLogger(ContainerIntrospectorImpl.class.getName());

    private File repositoryDirectory;
    private Map<String, ServiceTypeDescriptorImpl> serviceTypes = new LinkedHashMap<String, ServiceTypeDescriptorImpl>();
    
    private static final URL SERVICE_TYPE_STYLESHEET_NAME = ContainerIntrospectorImpl.class.getResource(SERVICE_TYPE_STYLESHEET);
    private static final URL SERVICE_INSTANCE_STYLESHEET_NAME = ContainerIntrospectorImpl.class.getResource(SERVICE_INSTANCE_STYLESHEET);

    public ServiceTypeDescriptor[] setRepository(File repositoryDirectory) throws ContainerException {
    	logger.info("Setting repository: '" + repositoryDirectory.getAbsolutePath() + "'");
        this.repositoryDirectory = repositoryDirectory;
        repositoryDirectory.mkdirs();
        
        for (File typeDirectory: repositoryDirectory.listFiles(directoryFilter)) {
        	deployTypeDirectory(typeDirectory);
        }

        return serviceTypes.values().toArray(new ServiceTypeDescriptor[serviceTypes.size()]);
    }

    public ServiceTypeDescriptor[] importDirectory(File directory) throws ContainerException {
    	List<ServiceTypeDescriptor> descriptors = new ArrayList<ServiceTypeDescriptor>();
    	for (File typeDirectory: directory.listFiles(directoryFilter)) {
        	descriptors.add(deployTypeDirectory(typeDirectory));
    	}
        return descriptors.toArray(new ServiceTypeDescriptor[descriptors.size()]);
    }

    public ServiceInstanceDescriptor[] importDirectory(String typeName, File directory) throws ContainerException {
    	ServiceTypeDescriptorImpl deploymentType = getType(typeName);
    	List<ServiceInstanceDescriptor> descriptors = new ArrayList<ServiceInstanceDescriptor>();
    	for (File typeDirectory: directory.listFiles(directoryFilter)) {
        	descriptors.add(deployInstanceDirectory(typeDirectory, deploymentType));
    	}
        return descriptors.toArray(new ServiceInstanceDescriptor[descriptors.size()]);
    }

    public ContainerDescriptor importFile(ZipFile deploymentFile) throws ContainerException {
        ZipEntry entry = deploymentFile.getEntry(SERVICE_TYPE_FILENAME);
        if (entry != null) {
            return importServiceType(deploymentFile);
        }
        
        entry = deploymentFile.getEntry(SERVICE_INSTANCE_FILENAME);
        if (entry != null) {
            Document document;
            InputStream is = null;
            String typeName = null;
            try {
                is = deploymentFile.getInputStream(entry);
                document = XMLUtils.parse(is);
                typeName = document.getDocumentElement().getAttribute("type");
            } catch (Exception e) {
                throw new ContainerException("Error parsing instance descriptor: " + e, e);
            } finally {
                try {
                    if (is != null) {
                        is.close();
                    }
                } catch (Exception e ){}                
            }
            if ("".equals(typeName)) {
                throw new ContainerException("Undefined service type for instance " + entry.getName());
            }
            return importServiceInstance(typeName, deploymentFile);
        }
        
        throw new ContainerException("Invalid deployment file, does not contain either service or instance descriptor file");
    }

    public ServiceTypeDescriptor[] getServiceTypes() {
        return serviceTypes.values().toArray(new ServiceTypeDescriptor[serviceTypes.size()]);
    }

    public ServiceTypeDescriptor getServiceType(String typeName) throws ContainerException {
        return getType(typeName);
    }
    
    public ServiceTypeDescriptor importServiceType(ZipFile deploymentFile) throws ContainerException {
    	String typeName = IOUtils.basename(deploymentFile.getName());
    	File typeDirectory = new File(repositoryDirectory, typeName);
    	try {
			IOUtils.extract(deploymentFile, typeDirectory);
		} catch (IOException e) {
			throw new ContainerException("Error extracting deployment file: " + e, e);
		}
        return deployTypeDirectory(typeDirectory);
    }

    public ServiceInstanceDescriptor importServiceInstance(String typeName, ZipFile deploymentFile) throws ContainerException {
    	ServiceTypeDescriptorImpl deploymentType = getType(typeName);
    	String instanceName = IOUtils.basename(deploymentFile.getName());
    	File instanceDirectory = new File(deploymentType.getDeploymentDirectory(), instanceName);
    	try {
			IOUtils.extract(deploymentFile, instanceDirectory);
		} catch (IOException e) {
			throw new ContainerException("Error extracting deployment file: " + e, e);
		}
        return deployInstanceDirectory(instanceDirectory, deploymentType);
    }

    public void removeInstance(String typeName, String instanceName) throws ContainerException {
    	ServiceTypeDescriptorImpl deploymentType = getType(typeName);
    	File instanceDirectory = new File(deploymentType.getDeploymentDirectory(), instanceName);
    	IOUtils.recursiveDelete(instanceDirectory);
    	deploymentType.removeInstance(instanceName);
    }

    public void removeType(String typeName) throws ContainerException {
    	ServiceTypeDescriptorImpl deploymentType = getType(typeName);
    	File typeDirectory = new File(repositoryDirectory, deploymentType.getName());
    	IOUtils.recursiveDelete(typeDirectory);
        serviceTypes.remove(typeName);
    }

    public void takeSnapshot(File zipDestination) throws ContainerException {
    	try {
			zipDestination.getParentFile().mkdirs();
			ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(zipDestination));
			takeSnapshot(repositoryDirectory, zos);
			zos.close();
		} catch (Exception e) {
			logger.warning("Error creating repository snapshot: " + e);
			throw new ContainerException("Error creating repository snapshot: " + e, e);
		}
	}
	private void takeSnapshot(File file, ZipOutputStream zos) throws Exception {
		if (file.isDirectory()) {
			for (File childFile: file.listFiles()) {
				takeSnapshot(childFile, zos);
			}
		} else {
			int pathLength = repositoryDirectory.getAbsolutePath().length();
			String baseName = file.getAbsolutePath().substring(pathLength + 1).replace(File.pathSeparatorChar, '/');
			ZipEntry zipEntry = new ZipEntry(baseName);
			zos.putNextEntry(zipEntry);
			int cnt;
			byte[] buffer = new byte[4096];
			FileInputStream fis = new FileInputStream(file);
			while ((cnt = fis.read(buffer)) != -1) {
				zos.write(buffer, 0, cnt);
			}
		}
	}

    private ServiceTypeDescriptorImpl deployTypeDirectory(File typeDirectory) throws ContainerException {
        File descriptorFile = new File(typeDirectory, SERVICE_TYPE_FILENAME);
        if (!descriptorFile.isFile()) {
        	throw new ContainerException("Missing type deployment descriptor file ");
        }
        ComponentManager componentManager = ComponentUtils.getComponentManager(descriptorFile, SERVICE_TYPE_STYLESHEET_NAME, getClass().getClassLoader());
        ServiceTypeDescriptorImpl deploymentType = (ServiceTypeDescriptorImpl) componentManager.getComponent(SERVICE_TYPE_COMPONENT_NAME);
        deploymentType.setName(typeDirectory.getName());
        deploymentType.setBaseDirectory(typeDirectory);

        for (File instanceDirectory: deploymentType.getDeploymentDirectory().listFiles(directoryFilter)) {
        	deployInstanceDirectory(instanceDirectory, deploymentType);
        }
        
        serviceTypes.put(deploymentType.getName(), deploymentType);
        
        logger.info("Successfully deployed type '" + deploymentType.getName() + "'");
        return deploymentType;
    }
    
    private ServiceInstanceDescriptorImpl deployInstanceDirectory(File instanceDirectory, ServiceTypeDescriptorImpl deploymentType) throws ContainerException {
    	ServiceInstanceDescriptorImpl deploymentInstance = null;
        File descriptorFile = new File(instanceDirectory, SERVICE_INSTANCE_FILENAME);
        if (descriptorFile.isFile()) {
            ComponentManager componentManager = ComponentUtils.getComponentManager(descriptorFile, SERVICE_INSTANCE_STYLESHEET_NAME, getClass().getClassLoader());
            deploymentInstance = (ServiceInstanceDescriptorImpl) componentManager.getComponent(SERVICE_INSTANCE_COMPONENT_NAME);
        } else {
        	deploymentInstance = new ServiceInstanceDescriptorImpl();
        }
        
        deploymentInstance.setName(instanceDirectory.getName());
        deploymentInstance.setBaseDirectory(instanceDirectory);
        
        deploymentType.addInstance(deploymentInstance);
        
        return deploymentInstance;
    }
    
    private ServiceTypeDescriptorImpl getType(String typeName) throws ContainerException {
    	ServiceTypeDescriptorImpl deploymentType = serviceTypes.get(typeName);
    	if (deploymentType == null) {
    		logger.warning("No such deployment type: '" + typeName + "'");
    		throw new ContainerException("No such deployment type: '" + typeName + "'");
    	}
    	return deploymentType;
    }
}
