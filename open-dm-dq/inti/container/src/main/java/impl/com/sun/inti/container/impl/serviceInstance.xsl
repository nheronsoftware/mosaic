<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
                
	<xsl:output method="xml"
				indent="yes"
				doctype-public="-//SPRING//DTD BEAN 2.0//EN"
				doctype-system="http://www.springframework.org/dtd/spring-beans-2.0.dtd"/>
				
	<xsl:template match="serviceInstance">
		<beans>
			<bean id="serviceInstance" class="com.sun.inti.container.impl.ServiceInstance">
				<property name="componentManagerFactory">
					<xsl:apply-templates select="componentManagerFactory"/>
				</property>
				<xsl:if test="description">
					<property name="description">
						<value>
							<xsl:value-of select="normalize-space(string(description))"/>
						</value>
					</property>
				</xsl:if>
				<xsl:if test="parameter">
					<property name="parameters">
						<map type="java.util.LinkedHashMap">
							<xsl:for-each select="parameter">
								<entry>
									<key>
										<value>
											<xsl:value-of select="@name"/>
										</value>
									</key>
									<xsl:choose>
										<xsl:when test="@value">
											<value>
												<xsl:if test="@type">
													<xsl:attribute name="type">
														<xsl:value-of select="@type"/>
													</xsl:attribute>
												</xsl:if>
												<xsl:value-of select="@value"/>
											</value>
										</xsl:when>
										<xsl:when test="value">
											<value>
												<xsl:if test="value/@type">
													<xsl:attribute name="type">
														<xsl:value-of select="value/@type"/>
													</xsl:attribute>
												</xsl:if>
												<xsl:value-of select="string(value)"/>
											</value>
										</xsl:when>
										<xsl:otherwise>
											<xsl:apply-templates/>
										</xsl:otherwise>
									</xsl:choose>
								</entry>
							</xsl:for-each>
						</map>
					</property>
				</xsl:if>
			</bean>
		</beans>
	</xsl:template>

	<xsl:template match="componentManagerFactory">
		<bean>
			<xsl:apply-templates select="@*|node()"/>
		</bean>
	</xsl:template>

	<xsl:template match="@*|node()"  priority="-1">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
</xsl:stylesheet>
