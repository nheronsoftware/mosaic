package com.sun.inti.container.impl;

import static com.sun.inti.container.impl.ContainerConstants.INSTANCE_DIRECTORY_NAME;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Logger;

import com.sun.inti.container.ContainerException;
import com.sun.inti.container.ServiceInstanceDescriptor;
import com.sun.inti.container.ServiceTypeDescriptor;

public class ServiceType extends ServiceContainerDescriptor implements ServiceTypeDescriptor {
    private static final Logger logger = Logger.getLogger(ServiceType.class.getName());

    private String defaultInstanceName;
    private Map<String, ServiceInstance> serviceInstances = new LinkedHashMap<String, ServiceInstance>();
    
    public ServiceInstance[] getInstances() {
        return serviceInstances.values().toArray(new ServiceInstance[serviceInstances.size()]);
    }
    
    public ServiceInstanceDescriptor getDefaultInstance() {
        return serviceInstances.get(defaultInstanceName);
    }
    
    protected void addInstance(ServiceInstance serviceInstance) {
        logger.info("Adding instance '" + serviceInstance.getName() + "': " + serviceInstance.getDescription());
        serviceInstances.put(serviceInstance.getName(), serviceInstance);
    }
    
    protected void removeInstance(String instanceName) {
        logger.info("Removing instance '" + instanceName + "'");
        serviceInstances.remove(instanceName);
    }
    
    public ServiceInstance getInstance(String instanceName) throws ContainerException {
        ServiceInstance serviceInstance = serviceInstances.get(instanceName);
        if (serviceInstance == null) {
            serviceInstance = serviceInstances.get(defaultInstanceName);
            if (serviceInstance == null) {
                logger.warning("No such instance: '" + instanceName + "' for type '" + getName() + "'");
                throw new ContainerException("No such instance: '" + instanceName + "' for type '" + getName() + "'");
            }
        }
        return serviceInstance;
    }
    
    public File getDeploymentDirectory() {
    	return new File(getBaseDirectory(), INSTANCE_DIRECTORY_NAME);
    }

    public String getDefaultInstanceName() {
        return defaultInstanceName;
    }

    public void setDefaultInstanceName(String defaultInstanceName) {
        this.defaultInstanceName = defaultInstanceName;
    }
}
