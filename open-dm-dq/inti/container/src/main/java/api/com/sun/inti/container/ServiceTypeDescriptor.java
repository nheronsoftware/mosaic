package com.sun.inti.container;

public interface ServiceTypeDescriptor extends ContainerDescriptor {
    public ServiceInstanceDescriptor[] getInstances();
    public ServiceInstanceDescriptor getDefaultInstance();
    public ServiceInstanceDescriptor getInstance(String name) throws ContainerException;
}
