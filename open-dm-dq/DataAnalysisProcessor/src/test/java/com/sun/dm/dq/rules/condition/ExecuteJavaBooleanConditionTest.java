/*
 * ExecuteJavaBooleanConditionTest.java
 * JUnit based test
 *
 * Created on October 13, 2007, 12:40 AM
 */

package com.sun.dm.dq.rules.condition;

import com.sun.dm.dq.process.DataObjectCreator;
import com.sun.dm.dq.schema.Parameters;
import junit.framework.*;
import com.sun.dm.dq.rules.data.DataObjectHandler;
import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import com.sun.dm.dq.schema.IfStepType;
import com.sun.dm.dq.schema.Parameter;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class ExecuteJavaBooleanConditionTest extends TestCase {
    
    DataObjectHandler doHandler;
    String ePath ;
    IfStepType.Condition.ExecuteJavaBoolean  executeJavaXmlObj;
    ExecuteJavaBooleanCondition instance ;
    ConditionValue result ;
    
    public ExecuteJavaBooleanConditionTest(String testName) {
        super(testName);
        ePath = "Person.FirstName";
    }
    
    protected void setUp() throws Exception {
        super.setUp();
        DataObjectCreator doCreate = new DataObjectCreator();
        doHandler = doCreate.createDataObjectHandler();
        executeJavaXmlObj= new IfStepType.Condition.ExecuteJavaBoolean();
        executeJavaXmlObj.setFieldName(ePath);
    }
    
    protected void tearDown() throws Exception {
        super.tearDown();
    }
    
    /**
     * Test of getConditionValue method, of class com.sun.di.rules.condition.ExecuteJavaBooleanCondition.
     */
    public void testGetConditionValue() throws ProcessXMLDataException {
        System.out.println("getConditionValue");
       //Test For True Condition
        String strPassed="My First Name";
        executeJavaXmlObj.setClassName("testcls");
        executeJavaXmlObj.setJarFilePath( System.getProperty("user.dir") + "/src/test/java/resources/testConditionClass.jar");
       // System.out.println(System.getProperty("class.path"));
       // System.out.println(System.getProperty("user.dir") + "\\test\\resources");
        executeJavaXmlObj.setMethodName("show");
        com.sun.dm.dq.schema.Parameters params = new Parameters();
        params.addParameter(createParam("str","java.lang.String",strPassed));
        
        
        executeJavaXmlObj.setParameters(params);
        instance = new ExecuteJavaBooleanCondition(executeJavaXmlObj);
        
        result = instance.getConditionValue(doHandler);
        
        assertNotNull(result);
        assertEquals(result.getClass(),TrueCondition.class );
        //Test For FAlse Condition
        strPassed="First Name";
        params = new Parameters();
        params.addParameter(createParam("str","java.lang.String",strPassed));
        
        executeJavaXmlObj.setParameters(params);
        instance = new ExecuteJavaBooleanCondition(executeJavaXmlObj);
        
        result = instance.getConditionValue(doHandler);
        
        assertNotNull(result);
        assertEquals(result.getClass(),FalseCondition.class );
        
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    private com.sun.dm.dq.schema.Parameter createParam(String name,String type,String value) {
        com.sun.dm.dq.schema.Parameter param = new Parameter();
        param.setParameterName(name);
        param.setParameterType(type);
        param.setParameterValue(value);
        return param;
    }
    
}
