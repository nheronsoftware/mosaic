/*
 * MatchFromFileRuleTest.java
 * JUnit based test
 *
 * Created on October 10, 2007, 9:10 PM
 */

package com.sun.dm.dq.rules;

import com.sun.dm.dq.process.DataObjectCreator;
import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import junit.framework.*;
import com.sun.dm.dq.rules.data.DataObjectHandler;
import com.sun.dm.dq.rules.output.RuleErrorObject;
import com.sun.dm.dq.schema.CleansingRuleType;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class MatchFromFileRuleTest extends TestCase {
    
    DataObjectHandler doHandler;
    String ePath ;
    RuleErrorObject expResult = null;
    CleansingRuleType.RuleList.Rule.MatchFromFile  matchFromFileXmlObj;
    MatchFromFileRule instance ;
    RuleErrorObject result ;
    String strDelimiter,strFilePath ;
    
    public MatchFromFileRuleTest(String testName) {
        super(testName);
        ePath = "Person.FirstName";
    }
    
    protected void setUp() throws Exception {
        super.setUp();
        DataObjectCreator doCreate = new DataObjectCreator();
        doHandler = doCreate.createDataObjectHandler();
        matchFromFileXmlObj= new CleansingRuleType.RuleList.Rule.MatchFromFile();
        matchFromFileXmlObj.setFieldName(ePath);
        
    }
    
    protected void tearDown() throws Exception {
        super.tearDown();
    }
    
    /**
     * Test of execute method, of class com.sun.di.rules.MatchFromFileRule.
     */
    public void testExecute() throws ProcessXMLDataException {
        System.out.println("execute");
        
        strDelimiter = ";";
        strFilePath = System.getProperty("user.dir") + "/src/test/java/resources/matchFromFile.txt";
        
        matchFromFileXmlObj.setDelimiter(strDelimiter);
        matchFromFileXmlObj.setExact(true);
        matchFromFileXmlObj.setFilePath(strFilePath);
        
        instance = new MatchFromFileRule(matchFromFileXmlObj);
        
        result = instance.execute(doHandler);
        assertNotNull( result);
        
        // TODO review the generated test code and remove the default call to fail.
        // fail("The test case is a prototype.");
    }
    
}
