/*
 * ExecuteJavaRuleTest.java
 * JUnit based test
 *
 * Created on October 10, 2007, 9:10 PM
 */

package com.sun.dm.dq.rules;

import com.sun.dm.dq.process.DataObjectCreator;
import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import junit.framework.*;
import com.sun.dm.dq.rules.data.DataObjectHandler;
import com.sun.dm.dq.rules.exception.DataObjectHandlerException;
import com.sun.dm.dq.rules.output.RuleErrorObject;
import com.sun.dm.dq.schema.CleansingRuleType;
import com.sun.dm.dq.schema.Parameter;
import com.sun.dm.dq.schema.Parameters;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class ExecuteJavaRuleTest extends TestCase {
    
    DataObjectHandler doHandler;
    String ePath ;
    RuleErrorObject expResult = null;
    CleansingRuleType.RuleList.Rule.ExecuteJava  executeJavaXmlObj;
    ExecuteJavaRule instance ;
    RuleErrorObject result ;
    
    public ExecuteJavaRuleTest(String testName) {
        super(testName);
        ePath = "Person.FirstName";
    }

    protected void setUp() throws Exception {
        
        super.setUp();
        DataObjectCreator doCreate = new DataObjectCreator();
        doHandler = doCreate.createDataObjectHandler();
        executeJavaXmlObj= new CleansingRuleType.RuleList.Rule.ExecuteJava();
        executeJavaXmlObj.setFieldName(ePath);
    }

    protected void tearDown() throws Exception {
    }

    /**
     * Test of execute method, of class com.sun.di.rules.ExecuteJavaRule.
     */
    public void testExecute() throws ProcessXMLDataException, DataObjectHandlerException {
        System.out.println("execute");
        String strPassed="Changed My First Name Using ExecuteJavaRule";
        executeJavaXmlObj.setClassName("testcls");
        executeJavaXmlObj.setJarFilePath(System.getProperty("user.dir") + "/src/test/java/resources/testClass.jar");
        executeJavaXmlObj.setMethodName("show");
        com.sun.dm.dq.schema.Parameters params = new Parameters();
        params.addParameter(createParam("str","java.lang.String",strPassed));
        
        
        executeJavaXmlObj.setParameters(params);
         instance = new ExecuteJavaRule(executeJavaXmlObj);
        
        RuleErrorObject expResult = null;
        RuleErrorObject result = instance.execute(doHandler);
        
        assertNull(result);
        String strOutPut = doHandler.getFieldValue(ePath);
        System.out.println(strOutPut);
        System.out.println(strPassed);
        assertEquals(strPassed,strOutPut);
        
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }
    private com.sun.dm.dq.schema.Parameter createParam(String name,String type,String value) {
        com.sun.dm.dq.schema.Parameter param = new Parameter();
        param.setParameterName(name);
        param.setParameterType(type);
        param.setParameterValue(value);
        return param;
    }
    
}
