/*
 * RangeRuleTest.java
 * JUnit based test
 *
 * Created on October 10, 2007, 9:11 PM
 */

package com.sun.dm.dq.rules;

import com.sun.dm.dq.process.DataObjectCreator;
import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import junit.framework.*;
import com.sun.dm.dq.rules.data.DataObjectHandler;
import com.sun.dm.dq.rules.output.RuleErrorObject;
import com.sun.dm.dq.schema.ProfilingRuleType;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class RangeRuleTest extends TestCase {
    
    DataObjectHandler doHandler;
    String ePath ;
    RuleErrorObject expResult = null;
    ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.Range  rangeXmlObj;
    RangeRule instance ;
    RuleErrorObject result ;
    
    
    public RangeRuleTest(String testName) {
        super(testName);
        ePath = "Person.Age";
    }

    protected void setUp() throws Exception {
        super.setUp();
        DataObjectCreator doCreate = new DataObjectCreator();
        doHandler = doCreate.createDataObjectHandler();
        rangeXmlObj= new ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.Range();
        rangeXmlObj.setFieldName(ePath);
    }

    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test of execute method, of class com.sun.di.rules.RangeRule.
     */
    public void testExecute() throws ProcessXMLDataException {
        System.out.println("execute");
        
        rangeXmlObj.setMax(35);
        rangeXmlObj.setMin(15);
        //actual age is 30
       
         instance = new RangeRule(rangeXmlObj);
        
       
        result = instance.execute(doHandler);
        assertNull(result);
        
        rangeXmlObj.setMax(25);
        rangeXmlObj.setMin(45);
        instance = new RangeRule(rangeXmlObj);
        result = instance.execute(doHandler);
        assertNotNull(result);
        
        rangeXmlObj.setMax(35);
        rangeXmlObj.setMin(31);
        instance = new RangeRule(rangeXmlObj);
        result = instance.execute(doHandler);
        assertNotNull(result);
        
        rangeXmlObj.setMax(25);
        rangeXmlObj.setMin(20);
        instance = new RangeRule(rangeXmlObj);
        result = instance.execute(doHandler);
        assertNotNull(result);
        
        // TODO review the generated test code and remove the default call to fail.
       // fail("The test case is a prototype.");
    }
    
}
