/*
 * DataLengthConditionTest.java
 * JUnit based test
 *
 * Created on October 13, 2007, 12:40 AM
 */

package com.sun.dm.dq.rules.condition;

import com.sun.dm.dq.process.DataObjectCreator;
import com.sun.dm.dq.rules.output.RuleErrorObject;
import junit.framework.*;
import com.sun.dm.dq.rules.data.DataObjectHandler;
import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import com.sun.dm.dq.schema.IfStepType;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class DataLengthConditionTest extends TestCase {
    
    DataObjectHandler doHandler;
    String ePath ;
    RuleErrorObject expResult = null;
    IfStepType.Condition.DataLength dataLengthXmlObj;
    DataLengthCondition instance ;
    ConditionValue result ;
    
    public DataLengthConditionTest(String testName) {
        super(testName);
        ePath = "Person.Age";
    }

    protected void setUp() throws Exception {
        super.setUp();
        DataObjectCreator doCreate = new DataObjectCreator();
        doHandler = doCreate.createDataObjectHandler();
        dataLengthXmlObj= new IfStepType.Condition.DataLength();
        dataLengthXmlObj.setFieldName(ePath);
    }

    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test of getConditionValue method, of class com.sun.di.rules.condition.DataLengthCondition.
     */
    public void testGetConditionValue() throws ProcessXMLDataException {
        System.out.println("getConditionValue");
        short i =20;
        dataLengthXmlObj.setLen(i);
        dataLengthXmlObj.setMore(true);
         instance = new DataLengthCondition(dataLengthXmlObj);
        
      
         result = instance.getConditionValue(doHandler);
        assertNotNull( result);
        
        // TODO review the generated test code and remove the default call to fail.
       // fail("The test case is a prototype.");
    }
    
}
