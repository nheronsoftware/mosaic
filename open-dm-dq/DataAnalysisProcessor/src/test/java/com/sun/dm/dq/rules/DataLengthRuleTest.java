/*
 * DataLengthRuleTest.java
 * JUnit based test
 *
 * Created on October 10, 2007, 9:09 PM
 */

package com.sun.dm.dq.rules;

import com.sun.dm.dq.process.DataObjectCreator;
import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import junit.framework.*;
import com.sun.dm.dq.rules.data.DataObjectHandler;
import com.sun.dm.dq.rules.exception.DataObjectHandlerException;
import com.sun.dm.dq.rules.output.RuleErrorObject;
import com.sun.dm.dq.schema.ProfilingRuleType;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class DataLengthRuleTest extends TestCase {
    DataObjectHandler doHandler;
    short len ;
    int actualLength;
    String ePath ;
    RuleErrorObject expResult = null;
    ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.DataLength  datalengthXmlObj;
    DataLengthRule instance ;
    RuleErrorObject result ;
    
    public DataLengthRuleTest(String testName) {
        super(testName);
        len = 3;
        
        ePath = "Person.FirstName";
    }
    
    protected void setUp() throws Exception {
        super.setUp();
        DataObjectCreator doCreate = new DataObjectCreator();
        doHandler = doCreate.createDataObjectHandler();
        actualLength = doHandler.getFieldValue(ePath).length();
        System.out.println("Actual Field length :: " + actualLength);
        datalengthXmlObj =  new ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.DataLength();
        datalengthXmlObj.setFieldName(ePath);
        datalengthXmlObj.setLen(len);
    }
    
    protected void tearDown() throws Exception {
        super.tearDown();
    }
    
    /**
     * Test of execute method, of class com.sun.di.rules.DataLengthRule.
     */
    public void testMoreThan() throws ProcessXMLDataException  {
        System.out.println("execute");
        
        
        
        datalengthXmlObj.setMore(true);
        instance = new DataLengthRule(datalengthXmlObj);
        
        
        result = instance.execute(doHandler);
        // Test for setMore = true
        if (actualLength >= len) {
            assertNull(result);
        } else {
            assertNotNull(result);
        }
        
        
        //assertEquals(expResult, result);
        
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    public void testLessThan() throws DataObjectHandlerException, ProcessXMLDataException {
        System.out.println("execute");
        
        
        datalengthXmlObj.setMore(false);
        instance = new DataLengthRule(datalengthXmlObj);
        result = instance.execute(doHandler);
        // Test for setMore = false
        if (actualLength < len) {
            assertNull(result);
        } else {
            assertNotNull(result);
        }
        
        //assertEquals(expResult, result);
        
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}
