/*
 * EqualsConditionTest.java
 * JUnit based test
 *
 * Created on October 13, 2007, 12:40 AM
 */

package com.sun.dm.dq.rules.condition;

import com.sun.dm.dq.process.DataObjectCreator;
import junit.framework.*;
import com.sun.dm.dq.rules.data.DataObjectHandler;
import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import com.sun.dm.dq.schema.IfStepType;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class EqualsConditionTest extends TestCase {
    
    DataObjectHandler doHandler;
    String ePath ;
    IfStepType.Condition.Equals  equalsXmlObj;
    EqualsCondition instance ;
    ConditionValue result ;
    
    public EqualsConditionTest(String testName) {
        super(testName);
        ePath = "Person.FirstName";
    }

    protected void setUp() throws Exception {
        super.setUp();
        DataObjectCreator doCreate = new DataObjectCreator();
        doHandler = doCreate.createDataObjectHandler();
        equalsXmlObj= new IfStepType.Condition.Equals();
        equalsXmlObj.setFieldName(ePath);
    }

    protected void tearDown() throws Exception {
        super.tearDown();
        
    }

    /**
     * Test of getConditionValue method, of class com.sun.di.rules.condition.EqualsCondition.
     */
    public void testGetConditionValue() throws ProcessXMLDataException {
        System.out.println("getConditionValue");
        
        equalsXmlObj.setValue2("My First Name");
        equalsXmlObj.setExact(true);
         instance = new EqualsCondition(equalsXmlObj);
        
         result = instance.getConditionValue(doHandler);
        assertEquals(result.getClass(), TrueCondition.class);
        
        equalsXmlObj.setValue2("mY first Name");
        equalsXmlObj.setExact(false);
         instance = new EqualsCondition(equalsXmlObj);
        
         result = instance.getConditionValue(doHandler);
        assertEquals(result.getClass(), TrueCondition.class);
        
        equalsXmlObj.setValue2("mY first Name");
        equalsXmlObj.setExact(true);
         instance = new EqualsCondition(equalsXmlObj);
        
         result = instance.getConditionValue(doHandler);
        assertEquals(result.getClass(), FalseCondition.class);
        
        equalsXmlObj.setValue2("My First");
        equalsXmlObj.setExact(false);
         instance = new EqualsCondition(equalsXmlObj);
        
         result = instance.getConditionValue(doHandler);
        assertEquals(result.getClass(), FalseCondition.class);
        
        // TODO review the generated test code and remove the default call to fail.
       // fail("The test case is a prototype.");
    }
    
}
