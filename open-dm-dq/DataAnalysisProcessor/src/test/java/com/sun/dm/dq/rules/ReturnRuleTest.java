/*
 * ReturnRuleTest.java
 * JUnit based test
 *
 * Created on October 10, 2007, 9:11 PM
 */

package com.sun.dm.dq.rules;

import com.sun.dm.dq.process.DataObjectCreator;
import junit.framework.*;
import com.sun.dm.dq.rules.data.DataObjectHandler;
import com.sun.dm.dq.rules.output.RuleErrorObject;
import com.sun.dm.dq.schema.ProfilingRuleType;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class ReturnRuleTest extends TestCase {
    
    DataObjectHandler doHandler;
    String ePath ;
    RuleErrorObject expResult = null;
    ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.Return returnXmlObj;
    ReturnRule instance ;
    RuleErrorObject result ;
    
    public ReturnRuleTest(String testName) {
        super(testName);
    }

    protected void setUp() throws Exception {
        super.setUp();
        DataObjectCreator doCreate = new DataObjectCreator();
        doHandler = doCreate.createDataObjectHandler();
        returnXmlObj= new ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.Return();
        returnXmlObj.setFieldName(ePath);
    }

    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test of execute method, of class com.sun.di.rules.ReturnRule.
     */
    public void testExecute() {
        System.out.println("execute");
        
        
        instance = new ReturnRule(returnXmlObj);
        
         result = instance.execute(doHandler);
        assertNull(result);
        
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}
