/*
 * PatternReplaceRuleTest.java
 * JUnit based test
 *
 * Created on October 10, 2007, 9:11 PM
 */

package com.sun.dm.dq.rules;

import com.sun.dm.dq.process.DataObjectCreator;
import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import junit.framework.*;
import com.sun.dm.dq.rules.data.DataObjectHandler;
import com.sun.dm.dq.rules.exception.DataObjectHandlerException;
import com.sun.dm.dq.rules.output.RuleErrorObject;
import com.sun.dm.dq.schema.CleansingRuleType;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class PatternReplaceRuleTest extends TestCase {
    
    DataObjectHandler doHandler;
    String ePath ;
    RuleErrorObject expResult = null;
    CleansingRuleType.RuleList.Rule.PatternReplace  patternReplaceXmlObj;
    PatternReplaceRule instance ;
    RuleErrorObject result ;
    
    public PatternReplaceRuleTest(String testName) {
        super(testName);
        ePath = "Person.FirstName";
    }

    protected void setUp() throws Exception {
        super.setUp();
        DataObjectCreator doCreate = new DataObjectCreator();
        doHandler = doCreate.createDataObjectHandler();
        patternReplaceXmlObj= new CleansingRuleType.RuleList.Rule.PatternReplace();
        patternReplaceXmlObj.setFieldName(ePath);
    }

    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test of execute method, of class com.sun.di.rules.PatternReplaceRule.
     */
    public void testExecute() throws ProcessXMLDataException, DataObjectHandlerException {
        System.out.println("execute");
        String strMatchPattern,strReplace;
        
        strMatchPattern = "My";
        strReplace="Your";
        patternReplaceXmlObj.setMatchPattern(strMatchPattern);
        patternReplaceXmlObj.setReplace(strReplace);
         instance = new PatternReplaceRule(patternReplaceXmlObj);
        
        RuleErrorObject expResult = null;
         result = instance.execute(doHandler);
         System.out.println("Replaced value :: " + doHandler.getFieldValue(ePath)) ;
        assertNull(result);
        
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}
