/*
 * TruncateRuleTest.java
 * JUnit based test
 *
 * Created on October 10, 2007, 9:11 PM
 */

package com.sun.dm.dq.rules;

import com.sun.dm.dq.process.DataObjectCreator;
import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import com.sun.mdm.index.dataobject.DataObject;
import junit.framework.*;
import com.sun.dm.dq.rules.data.DataObjectHandler;
import com.sun.dm.dq.rules.exception.DataObjectHandlerException;
import com.sun.dm.dq.rules.output.RuleErrorObject;
import com.sun.dm.dq.schema.CleansingRuleType;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class TruncateRuleTest extends TestCase {
    
    DataObjectHandler doHandler;
    String ePath ;
    RuleErrorObject expResult = null;
    CleansingRuleType.RuleList.Rule.Truncate  truncateXmlObj;
    TruncateRule instance ;
    RuleErrorObject result ;
    DataObject dObj;
    
    public TruncateRuleTest(String testName) {
        super(testName);
        ePath = "Person.FirstName";
    }

    protected void setUp() throws Exception {
        super.setUp();
        DataObjectCreator doCreate = new DataObjectCreator();
        doHandler = doCreate.createDataObjectHandler();
        String fname = "This is a test Name for Truncate Testing ";
        truncateXmlObj= new CleansingRuleType.RuleList.Rule.Truncate();
        truncateXmlObj.setFieldName(ePath);
        dObj = doCreate.createDataObject(fname + fname + fname +fname +fname + fname,"sName","21","M","10/12/2007");
        doHandler.setParent(dObj);
    }

    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test of execute method, of class com.sun.di.rules.TruncateRule.
     */
    public void testExecute() throws ProcessXMLDataException, DataObjectHandlerException {
        System.out.println("execute");
        
        
        instance = new TruncateRule(truncateXmlObj);
        String oldValue = doHandler.getFieldValue(ePath);
        System.out.println(doHandler.getFieldValue(ePath));
       
        result = instance.execute(doHandler);
        
        String updateValue = doHandler.getFieldValue(ePath);
        
         System.out.println(updateValue);
        
        assertFalse(oldValue.compareTo(updateValue) == 0);
        
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}
