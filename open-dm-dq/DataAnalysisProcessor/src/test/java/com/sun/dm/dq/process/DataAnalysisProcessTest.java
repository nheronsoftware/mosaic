/*
 * DataAnalysisProcessTest.java
 * JUnit based test
 *
 * Created on October 9, 2007, 5:26 PM
 */

package com.sun.dm.dq.process;

import com.sun.dm.dq.rules.exception.DBHandlerException;
import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import junit.framework.*;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class DataAnalysisProcessTest extends TestCase {
    protected DataAnalysisProcess instance = null;
    
    public DataAnalysisProcessTest(String testName) {
        super(testName);
    }
    
    protected void setUp() throws Exception {
        super.setUp();
        instance = new DataAnalysisProcess(System.getProperty("user.dir") + "/src/test/java/resources/TestDataAnalysisConfig.xml");
        
    }
    
    protected void tearDown() throws Exception {
        super.tearDown();
    }
    
    /**
     * Test of getCleansingProcess method, of class com.sun.di.process.DataAnalysisProcess.
     */
    public void testGetCleansingProcess() throws ProcessXMLDataException {
        System.out.println("getCleansingProcess");
        
       CleansingProcess cp = instance.getCleansingProcess();
        assertNotNull( cp);
        
        // TODO review the generated test code and remove the default call to fail.
        // fail("The test case is a prototype.");
    }
    
    /**
     * Test of getProfilingProcess method, of class com.sun.di.process.DataAnalysisProcess.
     */
    public void testGetProfilingProcess() throws ProcessXMLDataException, DBHandlerException {
        System.out.println("getProfilingProcess");
       // ProfilingProcess pp = instance.getProfilingProcess();
       // assertNotNull( pp);
        
        // ProfilingProcess expResult = null;
        // pp = instance.getProfilingProcess();
        //  assertEquals(expResult, pp);
        
        // TODO review the generated test code and remove the default call to fail.
        // fail("The test case is a prototype.");
    }
    
    
}
