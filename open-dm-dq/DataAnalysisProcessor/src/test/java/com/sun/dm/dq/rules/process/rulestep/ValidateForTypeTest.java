/*
 * ValidateForTypeTest.java
 * JUnit based test
 *
 * Created on June 02, 2010, 9:47 AM
 */

package com.sun.dm.dq.rules.process.rulestep;

import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import junit.framework.*;
import com.sun.dm.dq.rules.data.DataObjectHandler;
import com.sun.dm.dq.rules.output.RuleErrorObject;
import com.sun.mdm.index.dataobject.ChildType;
import com.sun.mdm.index.dataobject.DataObject;
import java.util.ArrayList;

/**
 *
 * @author graeme.williams@oracle.com
 */
public class ValidateForTypeTest extends TestCase {
    
    DataObjectHandler doHandler;
    
    public ValidateForTypeTest(String testName) {
        super(testName);
    }
    
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    protected void tearDown() throws Exception {
    }
    
    /**
     * Test of execute method, of class com.sun.di.rules.DateRangeRule.
     */
    public void testExecute() throws ProcessXMLDataException {
        System.out.println("execute");
        doHandler = new DataObjectHandler(System.getProperty("user.dir") + "/src/test/java/resources/objectdef.xml");        
		ValidateForType vft = new ValidateForType(doHandler.getFieldsAll());
		ArrayList<RuleErrorObject> errObj = null;
		
        System.out.println("1st Test - valid data");
        doHandler.setParent(createDataObject("My First Name","My Last Name","30","M","10/31/2005"));
		errObj = vft.execute(doHandler);
		System.out.println("Output size = "  + errObj.size());
		for (RuleErrorObject reo: errObj){
			System.out.println("Rule Error = [" + reo.getRuleValue() + "][" + reo.getFailReason() + "]");
		}
		assertTrue(errObj.size() == 0);
		
        System.out.println("2nd Test - invalid date");
		doHandler.setParent(createDataObject("My First Name","My Last Name","30","M","31/10/2005"));
		errObj = vft.execute(doHandler);
		System.out.println("Output size = "  + errObj.size());
		for (RuleErrorObject reo: errObj){
			System.out.println("Rule Error = [" + reo.getRuleValue() + "][" + reo.getFailReason() + "]");
		}
		assertTrue(errObj.size() != 0);
		
        System.out.println("3rd Test - invalid int");
        doHandler.setParent(createDataObject("My First Name","My Last Name","NO AGE","M","10/31/2005"));
		errObj = vft.execute(doHandler);
		System.out.println("Output size = "  + errObj.size());
		for (RuleErrorObject reo: errObj){
			System.out.println("Rule Error = [" + reo.getRuleValue() + "][" + reo.getFailReason() + "]");
		}
		assertTrue(errObj.size() != 0);
		
        System.out.println("4th Test - invalid char");
        doHandler.setParent(createDataObject("My First Name","My Last Name","30","Male","10/31/2005"));
		errObj = vft.execute(doHandler);
		System.out.println("Output size = "  + errObj.size());
		for (RuleErrorObject reo: errObj){
			System.out.println("Rule Error = [" + reo.getRuleValue() + "][" + reo.getFailReason() + "]");
		}
		assertTrue(errObj.size() != 0);
    }
	
    private DataObject createDataObject(String fName,String sName,String sAge,String sGender,String sDOB) {
        
        DataObject person = new DataObject();
        
        DataObject address1 = new DataObject();
        DataObject address2 = new DataObject();
        ChildType addressCT = new ChildType();
        addressCT.addChild(address1);
        addressCT.addChild(address2);
        
        person.add(0,null);
        person.add(0,null);
        person.add(0,null);
        person.add(0,null);
        person.add(0,null);
        
        person.addChildType(addressCT);
        
        person.addFieldValue(fName);
        person.addFieldValue(sName);
        person.addFieldValue(sAge);
        person.addFieldValue(sGender);
        person.addFieldValue(sDOB);
        
        
        address1.addFieldValue("Duarte");
        address1.addFieldValue("91010");
        
        address2.addFieldValue("Monrovia");
        address2.addFieldValue("91016");
        
        return person;
    }
}
