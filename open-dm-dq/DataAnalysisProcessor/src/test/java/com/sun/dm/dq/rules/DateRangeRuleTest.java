/*
 * DateRangeRuleTest.java
 * JUnit based test
 *
 * Created on October 10, 2007, 9:09 PM
 */

package com.sun.dm.dq.rules;

import com.sun.dm.dq.process.DataObjectCreator;
import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import junit.framework.*;
import com.sun.dm.dq.rules.data.DataObjectHandler;
import com.sun.dm.dq.rules.output.RuleErrorObject;
import com.sun.dm.dq.schema.ProfilingRuleType;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class DateRangeRuleTest extends TestCase {
    
    DataObjectHandler doHandler;
    String ePath ;
    RuleErrorObject expResult = null;
    ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.DateRange  dateRangeXmlObj;
    DateRangeRule instance ;
    RuleErrorObject result ;
    
    private String actualDate;
    
    public DateRangeRuleTest(String testName) {
        super(testName);
        ePath = "Person.DOB";
    }
    
    protected void setUp() throws Exception {
        super.setUp();
        DataObjectCreator doCreate = new DataObjectCreator();
        doHandler = doCreate.createDataObjectHandler();
        actualDate = doHandler.getFieldValue(ePath);
        dateRangeXmlObj= new ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.DateRange();
        dateRangeXmlObj.setFieldName(ePath);
        
    }
    
    protected void tearDown() throws Exception {
    }
    
    /**
     * Test of execute method, of class com.sun.di.rules.DateRangeRule.
     */
    public void testExecute() throws ProcessXMLDataException {
        System.out.println("execute");
     
        // Case 1 : result shud be NULL
        String minDate = "09/21/2003";
        String maxDate = "12/21/2007";
        //Actual Date = 10/11/2005
        
        result = PrepareExecute(minDate,maxDate);
        
        assertNull(result);
        
        // Case 2 : result shud NOT NULL ( Minimum date is more than actual date)
         minDate = "09/21/2006";
         maxDate = "12/21/2007";
        //Actual Date = 10/11/2005
        
        result = PrepareExecute(minDate,maxDate);
        
        assertNotNull(result);
        
        // Case 3 : result shud NOT NULL ( Max date is less than actual date)
         minDate = "09/21/2003";
         maxDate = "12/21/2004";
        //Actual Date = 10/11/2005
        
        result = PrepareExecute(minDate,maxDate);
        
        assertNotNull(result);
        
        // Case 4 : result shud be NULL ( Max date is more than actual date)
         minDate = null;
         maxDate = "12/21/2007";
        //Actual Date = 10/11/2005
        
        result = PrepareExecute(minDate,maxDate);
        
        assertNull(result);
        
        // Case 5 : result shud be NULL ( Min date is less than actual date)
         minDate = "12/21/2003";
         maxDate = null;
        //Actual Date = 10/11/2005
        
        result = PrepareExecute(minDate,maxDate);
        
        assertNull(result);
        
        // TODO review the generated test code and remove the default call to fail.
        // fail("The test case is a prototype.");
    }
    private RuleErrorObject PrepareExecute(String min,String max) throws ProcessXMLDataException {
        System.out.println("execute");
        
        
        System.out.println("Actual Date = " + actualDate );
        System.out.println("Min Date = " + min );
        System.out.println("Max Date = " + max );
        
        
        dateRangeXmlObj.setMax(max);
        dateRangeXmlObj.setMin(min);
        
        instance = new DateRangeRule(dateRangeXmlObj);
        
        return instance.execute(doHandler);
        
    }
}
