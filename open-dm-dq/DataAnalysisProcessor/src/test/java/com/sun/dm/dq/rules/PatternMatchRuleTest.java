/*
 * PatternMatchRuleTest.java
 * JUnit based test
 *
 * Created on October 10, 2007, 9:10 PM
 */

package com.sun.dm.dq.rules;

import com.sun.dm.dq.process.DataObjectCreator;
import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import junit.framework.*;
import com.sun.dm.dq.rules.data.DataObjectHandler;
import com.sun.dm.dq.rules.output.RuleErrorObject;
import com.sun.dm.dq.schema.ProfilingRuleType;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class PatternMatchRuleTest extends TestCase {
    
    DataObjectHandler doHandler;
    String ePath ;
    RuleErrorObject expResult = null;
    ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.PatternMatch  patternMatchXmlObj;
    PatternMatchRule instance ;
    RuleErrorObject result ;
    
    
    public PatternMatchRuleTest(String testName) {
        super(testName);
        ePath = "Person.FirstName";
    }
    
    protected void setUp() throws Exception {
        super.setUp();
        DataObjectCreator doCreate = new DataObjectCreator();
        doHandler = doCreate.createDataObjectHandler();
        patternMatchXmlObj= new ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.PatternMatch();
        patternMatchXmlObj.setFieldName(ePath);
    }
    
    protected void tearDown() throws Exception {
        super.tearDown();
    }
    
    /**
     * Test of execute method, of class com.sun.di.rules.PatternMatchRule.
     */
    public void testExecute() throws ProcessXMLDataException {
        System.out.println("execute");
        patternMatchXmlObj.setMatchPattern("My First Name");
        patternMatchXmlObj.setFound(true);
        instance = new PatternMatchRule(patternMatchXmlObj);
        
        RuleErrorObject expResult = null;
        result = instance.execute(doHandler);
        assertNull( result);
        
        patternMatchXmlObj.setFound(false);
        instance = new PatternMatchRule(patternMatchXmlObj);
        
        
        result = instance.execute(doHandler);
        assertNotNull( result);
        
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}
