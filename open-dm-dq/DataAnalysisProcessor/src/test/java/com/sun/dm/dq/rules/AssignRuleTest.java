/*
 * AssignRuleTest.java
 * JUnit based test
 *
 * Created on October 9, 2007, 7:16 PM
 */

package com.sun.dm.dq.rules;

import com.sun.dm.dq.process.DataObjectCreator;
import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import junit.framework.*;
import com.sun.dm.dq.rules.data.DataObjectHandler;
import com.sun.dm.dq.rules.exception.DataObjectHandlerException;
import com.sun.dm.dq.rules.output.RuleErrorObject;
import com.sun.dm.dq.schema.CleansingRuleType;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class AssignRuleTest extends TestCase {
    DataObjectHandler doHandler;
    public AssignRuleTest(String testName) {
        super(testName); 
        
    }

    protected void setUp() throws Exception {
        super.setUp();
        DataObjectCreator doCreate = new DataObjectCreator();
        doHandler = doCreate.createDataObjectHandler();
    }

    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test of execute method, of class com.sun.di.rules.AssignRule.
     */
    public void testExecute() throws DataObjectHandlerException, ProcessXMLDataException {
        System.out.println("execute");
        String strAssign = "My First Name is Changed";
        String ePath = "Person.FirstName" ;
        CleansingRuleType.RuleList.Rule.Assign assignXmlObj = new CleansingRuleType.RuleList.Rule.Assign();
       
        assignXmlObj.setValue(strAssign);
        assignXmlObj.setFieldName(ePath);
        DataObjectHandler dataObjHandler = null;
        AssignRule instance = null;
        
        instance = new AssignRule(assignXmlObj);
        RuleErrorObject expResult = null;
        System.out.println("Initial Value :: " + doHandler.getFieldValue(ePath));
        RuleErrorObject result = instance.execute(doHandler);
        
        String resultStr;
       
            resultStr = doHandler.getFieldValue(ePath);
            System.out.println("Final Updated Value :: " + resultStr);
       
        
       // assertEquals(strAssign, resultStr);
        
        // TODO review the generated test code and remove the default call to fail.
       // fail("The test case is a prototype.");
    }
    
}
