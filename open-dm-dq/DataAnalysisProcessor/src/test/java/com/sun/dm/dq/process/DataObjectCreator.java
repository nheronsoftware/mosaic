/*
 * DataObjectCreator.java
 *
 * Created on October 10, 2007, 9:34 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.sun.dm.dq.process;

import com.sun.dm.dq.rules.data.DataObjectHandler;
import com.sun.mdm.index.dataobject.ChildType;
import com.sun.mdm.index.dataobject.DataObject;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class DataObjectCreator {
    
    /** Creates a new instance of DataObjectCreator */
    public DataObjectCreator() {
    }
    public  DataObjectHandler createDataObjectHandler() {
        
        DataObjectHandler doHandler = new DataObjectHandler(System.getProperty("user.dir") + "/src/test/java/resources/objectdef.xml");
        doHandler.setParent(createDataObject("My First Name","My Last Name","30","M","10/11/2005"));
        return doHandler;
    }
    public  DataObject createDataObject(String fName,String sName,String sAge,String sGender,String sDOB) {
        
        // firstname|lastname#$city|zip#$phoneno|phoneCompany
        
        DataObject person = new DataObject();
        
        DataObject address1 = new DataObject();
        DataObject address2 = new DataObject();
        ChildType addressCT = new ChildType();
        addressCT.addChild(address1);
        addressCT.addChild(address2);
        
       // DataObject phone = new DataObject();
       // ChildType phoneCT = new ChildType();
        //phoneCT.addChild(phone);
        person.add(0,null);
        person.add(0,null);
        person.add(0,null);
        person.add(0,null);
        person.add(0,null);
        
        
        
        
        person.addChildType(addressCT);
        //person.addChildType(phoneCT);
        
        person.addFieldValue(fName);
        person.addFieldValue(sName);
        person.addFieldValue(sAge);
        person.addFieldValue(sGender);
        person.addFieldValue(sDOB);
        
        
        address1.addFieldValue("Duarte");
        address1.addFieldValue("91010");
        
        address2.addFieldValue("Monrovia");
        address2.addFieldValue("91016");
        
       // phone.addFieldValue("626-999-9999");
       // phone.addFieldValue("charter");
        
       // DataObject phoneChild = new DataObject();
       // ChildType phoneChildCT = new ChildType();
       // phoneChildCT.addChild(phoneChild);
       // phone.addChildType(phoneChildCT);
        
       // phoneChild.addFieldValue("Verizon");
       // phoneChild.addFieldValue("Texas");
        
        return person;
        
    }
    
}
