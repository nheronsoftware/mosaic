/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html
 * or mural/license.txt. See the License for the specific language 
 * governing permissions and limitations under the License.  
 *
 * When distributing Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at mural/license.txt.
 * If applicable, add the following below the CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.dm.dq.rules;

import com.sun.dm.dq.rules.data.DataObjectHandler;
import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import com.sun.dm.dq.rules.output.RuleErrorObject;
import com.sun.dm.dq.rules.process.rulestep.RuleStep;
import com.sun.dm.dq.schema.CleansingRuleType;
import java.util.ArrayList;
import java.util.ListIterator;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class ValidateDBFieldRule extends RuleStep {

    ArrayList<RuleStep> validateFieldList;

    public ValidateDBFieldRule(CleansingRuleType.RuleList.Rule.ValidateDBField objXmlValidateDB) throws ProcessXMLDataException {

        processXMLObject(objXmlValidateDB);
    }

    public RuleErrorObject execute(DataObjectHandler dataObjHandler) {

        ListIterator rlListItr = validateFieldList.listIterator();
        RuleErrorObject ruleErr = null;
        String strErrors = "";
        while (rlListItr.hasNext()) {
            RuleStep varRuleStep = (RuleStep) rlListItr.next();
            if (varRuleStep != null) {
                ruleErr = varRuleStep.execute(dataObjHandler);
                if (ruleErr != null) {
                    strErrors = strErrors + ruleErr.getFailReason() + ";";
                }
            }
        }
        if (strErrors.trim() == "") {
            return null;
        } else {
            return new RuleErrorObject("", " ValidateDBField", "", "", "ERR004: " + strErrors, false);
        }

    }

    private void processXMLObject(CleansingRuleType.RuleList.Rule.ValidateDBField objXmlValidateDB) throws ProcessXMLDataException {

        processXMLObject((ArrayList<CleansingRuleType.RuleList.Rule.ValidateDBField.Field>) objXmlValidateDB.getField());

    }
    /*
     *         @XmlElement(name = "istrue", type = IfStepType.Condition.Istrue.class),
    @XmlElement(name = "not", type = IfStepType.Condition.Not.class),
    @XmlElement(name = "or", type = IfStepType.Condition.Or.class),
    @XmlElement(name = "and", type = IfStepType.Condition.And.class),
    @XmlElement(name = "dataLength", type = IfStepType.Condition.DataLength.class),
    @XmlElement(name = "matches", type = IfStepType.Condition.Matches.class),
    @XmlElement(name = "equals", type = IfStepType.Condition.Equals.class)
     */

    private void processXMLObject(ArrayList<CleansingRuleType.RuleList.Rule.ValidateDBField.Field> objValidateList) throws ProcessXMLDataException {

        CleansingRuleType.RuleList.Rule.ValidateDBField.Field fldValue = null;
        String strAction;

        validateFieldList = new ArrayList<RuleStep>();
        ListIterator rlListItr = objValidateList.listIterator();

        while (rlListItr.hasNext()) {

            fldValue = (CleansingRuleType.RuleList.Rule.ValidateDBField.Field) rlListItr.next();
            if (fldValue != null) {
                strAction = fldValue.getAction();

                if (strAction.compareToIgnoreCase("truncate") == 0) {
                    validateFieldList.add(new TruncateRule(fldValue));

                } else if (strAction.compareToIgnoreCase("reject") == 0) {
                    validateFieldList.add(new RejectFieldRule(fldValue));

                }
            }
        }

    }
}
