/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html
 * or mural/license.txt. See the License for the specific language 
 * governing permissions and limitations under the License.  
 *
 * When distributing Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at mural/license.txt.
 * If applicable, add the following below the CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.dm.dq.rules;

import com.sun.dm.dq.rules.data.DataObjectHandler;
import com.sun.dm.dq.rules.exception.DataObjectHandlerException;
import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import com.sun.dm.dq.rules.output.RuleErrorObject;
import com.sun.dm.dq.rules.process.rulestep.RuleStep;
import com.sun.dm.dq.rules.variablelist.VarListProcess;
import com.sun.dm.dq.schema.CleansingRuleType;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class AssignRule extends RuleStep {

    private String ePath;
    private String setValue;
    
    public AssignRule(CleansingRuleType.RuleList.Rule.Assign assignXmlObj) throws ProcessXMLDataException {

        processXMLObject(assignXmlObj);

    }

    public RuleErrorObject execute(DataObjectHandler dataObjHandler) {
        try {
            // If Exception then return RuleErrorObject
            // else return NULL ...
            
            dataObjHandler.setFieldValue(ePath, setValue);

        } catch (DataObjectHandlerException ex) {
            return new RuleErrorObject("EPath :: " + ePath, " Assign", "", setValue, "ERR001:" + ex.getLocalizedMessage(), true);
        }
        return null;
    }

    private void processXMLObject(CleansingRuleType.RuleList.Rule.Assign xmlObj) throws ProcessXMLDataException {

        ePath = VarListProcess.getVarValue(xmlObj.getFieldName());
        setValue = xmlObj.getValue();
        
    }
}
