/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html
 * or mural/license.txt. See the License for the specific language 
 * governing permissions and limitations under the License.  
 *
 * When distributing Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at mural/license.txt.
 * If applicable, add the following below the CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */

package com.sun.dm.dq.rules.process.ruleprocessor;

import com.sun.dm.dq.rules.data.DataObjectHandler;
import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import com.sun.dm.dq.rules.output.RuleErrorObject;
import com.sun.dm.dq.rules.process.rulestep.CleansingRuleImpl;
import com.sun.dm.dq.rules.process.rulestep.ProfilingRuleImpl;
import com.sun.dm.dq.rules.process.rulestep.RuleStep;
import com.sun.dm.dq.schema.CleansingRuleType;
import com.sun.dm.dq.schema.ProfilingRuleType;
import java.util.ArrayList;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class ThenProcess extends RuleStep {
    
    private CleansingRuleType.RuleList.Rule.If.Then objThen;
    private CleansingRuleImpl clensingRule;
    private ProfilingRuleImpl profilingRule;
    /**
     * Creates a new instance of ThenProcess
     */
    
    public ThenProcess(CleansingRuleType.RuleList.Rule.If.Then objXmlData) throws ProcessXMLDataException {
        
        processXMLObject(objXmlData);
    }
    public ThenProcess(ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.If.Then objXmlData) throws ProcessXMLDataException {
        
        processXMLObject(objXmlData);
    }
    private void processXMLObject(CleansingRuleType.RuleList.Rule.If.Then objXmlThen) throws ProcessXMLDataException {
        
        
        clensingRule = new  CleansingRuleImpl((ArrayList<Object>) objXmlThen.getCleansingRuleStepGroup());
    }
    private void processXMLObject(ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.If.Then objXmlThen) throws ProcessXMLDataException {
        
        
        profilingRule = new  ProfilingRuleImpl((ArrayList<Object>) objXmlThen.getProfilerRuleStepGroup());
    }
    public RuleErrorObject execute(DataObjectHandler dataObjHandler) {
        // Only one RuleStep is allowed in Then or Else statement.
        ArrayList<RuleErrorObject> ruleErr = null;
        
        if(clensingRule != null) {
            ruleErr = clensingRule.execute(dataObjHandler);
        } else if(profilingRule != null) {
            profilingRule.execute(dataObjHandler);
        }
        if (ruleErr != null) {
            if (ruleErr.size() > 0) {
                return ruleErr.get(0);
            }
        }
        return null;
    }
    
}
