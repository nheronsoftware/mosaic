/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html
 * or mural/license.txt. See the License for the specific language 
 * governing permissions and limitations under the License.  
 *
 * When distributing Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at mural/license.txt.
 * If applicable, add the following below the CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.dm.dq.rules.condition;

import com.sun.dm.dq.rules.data.DataObjectHandler;
import com.sun.dm.dq.rules.exception.DataObjectHandlerException;
import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import com.sun.dm.dq.rules.variablelist.VarListProcess;
import com.sun.dm.dq.schema.IfStepType;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class MatchesCondition extends ConditionClass {

    private String ePath;
    private String strPattern;
    private Pattern pattern;

    public MatchesCondition(IfStepType.Condition.Matches objMatches) throws ProcessXMLDataException {
        processXMLObject(objMatches);
    }

    public ConditionValue getConditionValue(DataObjectHandler dataObjHandler) {
        String value;
        try {
            value = dataObjHandler.getFieldValue(ePath);

            if (value != null) {
                Matcher matcher = pattern.matcher(value);
                boolean matchFound = matcher.find();
                if (matchFound == true) {
                    return new TrueCondition();
                } else if (matchFound == false) {
                    return new FalseCondition();
                }
            }
        } catch (DataObjectHandlerException ex) {
            return new ErrorCondition();
        }
        return new ErrorCondition();
    }

    private void processXMLObject(IfStepType.Condition.Matches objMatches) throws ProcessXMLDataException {

        ePath = VarListProcess.getVarValue(objMatches.getFieldName());
        strPattern = objMatches.getPattern();
        pattern = Pattern.compile(strPattern);

        try {
            pattern = Pattern.compile(strPattern);
            if (pattern == null) {
                throw new ProcessXMLDataException("String Pattern " + strPattern + " is incorrect");
            }
        } catch (PatternSyntaxException e) {
            throw new ProcessXMLDataException(e);
        }

    }
}
