/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html
 * or mural/license.txt. See the License for the specific language 
 * governing permissions and limitations under the License.  
 *
 * When distributing Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at mural/license.txt.
 * If applicable, add the following below the CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */

package com.sun.dm.dq.rules.data.ObjectDefModifier;

import com.sun.mdm.index.dataobject.objectdef.ObjectDefinition;
import com.sun.mdm.index.dataobject.objectdef.Field;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class ObjectDefModifier {
    
    /*
     * Creates a new instance of ObjectDefModifier */
    public static ObjectDefinition getModifiedDataObject(ObjectDefinition objDef) {
        if (objDef== null) return null ;
        
        objDef.addField(0,getNewField("USR"));
        objDef.addField(0,getNewField("UPDATEDATE"));
        objDef.addField(0,getNewField("LID"));
        objDef.addField(0,getNewField("SYSTEMCODE"));
        objDef.addField(0,getNewField("GID"));
        
        return objDef;
    }
    private static Field getNewField(String name) {
        Field fldtmp = new Field();
        fldtmp.setCodeModule("");
        fldtmp.setType("string");
        fldtmp.setSize(30);
        fldtmp.setUpdateable(true);
        fldtmp.setRequired(false);
        fldtmp.setName(name);
        fldtmp.setKeyType(false);
        return fldtmp;
    }
    
}
