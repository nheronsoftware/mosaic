/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html
 * or mural/license.txt. See the License for the specific language 
 * governing permissions and limitations under the License.  
 *
 * When distributing Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at mural/license.txt.
 * If applicable, add the following below the CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.dm.dq.rules.condition;

import com.sun.dm.dq.rules.data.DataObjectHandler;
import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import com.sun.dm.dq.schema.IfStepType;
import java.util.ArrayList;
import java.util.ListIterator;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class OrCondition extends ConditionClass {

    private ArrayList<Object> objOrXmlList;
    private ArrayList<Object> objOrStepList;

    public OrCondition(IfStepType.Condition.Or objOr) throws ProcessXMLDataException {
        processXMLObject(objOr);
    }

    public ConditionValue getConditionValue(DataObjectHandler dataObjHandler) {
        ListIterator rlListItr = objOrStepList.listIterator();
        ConditionValue conditionVal;
        ConditionClass varConditionClass;

        while (rlListItr.hasNext()) {
            varConditionClass = (ConditionClass) rlListItr.next();

            if (varConditionClass != null) {
                conditionVal = varConditionClass.getConditionValue(dataObjHandler);
                if (conditionVal.getClass() == TrueCondition.class) {
                    return conditionVal;
                }
                if (conditionVal.getClass() == ErrorCondition.class) {
                    return conditionVal;
                }
            }
        }

        return new FalseCondition();
    }

    private void processXMLObject(IfStepType.Condition.Or objOr) throws ProcessXMLDataException {

        processXMLObject((ArrayList<Object>) objOr.getBooleanOperatorGroup());

    }
    /*
     *         @XmlElement(name = "istrue", type = IfStepType.Condition.Istrue.class),
    @XmlElement(name = "not", type = IfStepType.Condition.Not.class),
    @XmlElement(name = "or", type = IfStepType.Condition.Or.class),
    @XmlElement(name = "and", type = IfStepType.Condition.And.class),
    @XmlElement(name = "dataLength", type = IfStepType.Condition.DataLength.class),
    @XmlElement(name = "matches", type = IfStepType.Condition.Matches.class),
    @XmlElement(name = "equals", type = IfStepType.Condition.Equals.class)
     */

    private void processXMLObject(ArrayList<Object> objOrSteps) throws ProcessXMLDataException {


        objOrXmlList = new ArrayList<Object>();
        objOrStepList = new ArrayList<Object>();
        objOrXmlList = objOrSteps;

        ListIterator rlListItr = objOrXmlList.listIterator();
        while (rlListItr.hasNext()) {

            Object var = rlListItr.next();

            if (var.getClass() == IfStepType.Condition.Isnull.class) {
                objOrStepList.add(new IsNullCondition((IfStepType.Condition.Isnull) var));

            } else if (var.getClass() == IfStepType.Condition.And.class) {
                objOrStepList.add(new AndCondition((IfStepType.Condition.And) var));

            } else if (var.getClass() == IfStepType.Condition.DataLength.class) {
                objOrStepList.add(new DataLengthCondition((IfStepType.Condition.DataLength) var));

            } else if (var.getClass() == IfStepType.Condition.Equals.class) {
                objOrStepList.add(new EqualsCondition((IfStepType.Condition.Equals) var));

            } else if (var.getClass() == IfStepType.Condition.Matches.class) {
                objOrStepList.add(new MatchesCondition((IfStepType.Condition.Matches) var));

            } else if (var.getClass() == IfStepType.Condition.Not.class) {
                objOrStepList.add(new NotCondition((IfStepType.Condition.Not) var));

            } else if (var.getClass() == IfStepType.Condition.Or.class) {
                objOrStepList.add(new OrCondition((IfStepType.Condition.Or) var));

            }

        }

    }
}
