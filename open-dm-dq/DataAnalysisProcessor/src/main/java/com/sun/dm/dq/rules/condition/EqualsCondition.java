/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html
 * or mural/license.txt. See the License for the specific language 
 * governing permissions and limitations under the License.  
 *
 * When distributing Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at mural/license.txt.
 * If applicable, add the following below the CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.dm.dq.rules.condition;

import com.sun.dm.dq.rules.data.DataObjectHandler;
import com.sun.dm.dq.rules.exception.DataObjectHandlerException;
import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import com.sun.dm.dq.rules.variablelist.VarListProcess;
import com.sun.dm.dq.schema.IfStepType;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class EqualsCondition extends ConditionClass {

    private String cmpValue;
    private String ePath;
    private boolean isExact;

    public EqualsCondition(IfStepType.Condition.Equals objEquals) throws ProcessXMLDataException {
        processXMLObject(objEquals);
    }

    public ConditionValue getConditionValue(DataObjectHandler dataObjHandler) {
        String strValue;
        int cmp;
        try {
            strValue = dataObjHandler.getFieldValue(ePath);

            if (strValue != null) {
                if (isExact) {
                    cmp = strValue.compareTo(cmpValue);
                } else {
                    cmp = strValue.compareToIgnoreCase(cmpValue);
                }
                if (cmp == 0) {
                    return new TrueCondition();
                } else {
                    return new FalseCondition();
                }
            }
        } catch (DataObjectHandlerException ex) {
            return new ErrorCondition();
        }
        return new FalseCondition();
    }

    private void processXMLObject(IfStepType.Condition.Equals objEquals) throws ProcessXMLDataException {

        ePath = VarListProcess.getVarValue(objEquals.getFieldName());
        cmpValue = objEquals.getValue2();
        isExact = objEquals.isExact();

    }
}
