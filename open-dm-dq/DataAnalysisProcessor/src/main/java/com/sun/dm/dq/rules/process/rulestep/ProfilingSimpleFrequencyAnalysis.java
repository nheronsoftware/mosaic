/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html
 * or mural/license.txt. See the License for the specific language 
 * governing permissions and limitations under the License.  
 *
 * When distributing Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at mural/license.txt.
 * If applicable, add the following below the CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.dm.dq.rules.process.rulestep;

import com.sun.dm.dq.rules.data.DBHandler;
import com.sun.dm.dq.rules.exception.DBHandlerException;
import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import com.sun.dm.dq.rules.exception.ProfilerRuleException;
import com.sun.dm.dq.rules.variablelist.VarListProcess;
import com.sun.dm.dq.schema.FieldListVarType.Field;
import com.sun.dm.dq.schema.ProfilingRuleType;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ListIterator;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class ProfilingSimpleFrequencyAnalysis extends RuleStep implements ProfilerReport {

    
    private ArrayList<Field> simpleFrqList;
    private String sortOrder;
    private int threshHold;
    private boolean threshHoldMore;
    private boolean sortOrderIncreasing;
    private String tblName;
    private HashMap<String, String> mapSimpleFRQ;
    private PreparedStatement prepStmntSimple;
    private String[] fieldsSimpleFRQ;
    private static int objCount = 0;

    public ProfilingSimpleFrequencyAnalysis(ProfilingRuleType.SimpleFrequencyAnalysis objXmlSimpleFrequencyAnalysis, DBHandler dbHandler) throws ProcessXMLDataException {
        objCount = objCount + 1;
        tblName = "PROFILE_SIMPLE_FRQ" + "_" + String.valueOf(objCount);
        processXMLObject(objXmlSimpleFrequencyAnalysis, dbHandler);

    }

    private void processXMLObject(ProfilingRuleType.SimpleFrequencyAnalysis objXmlSimpleFrequencyAnalysis, DBHandler dbHandler) throws ProcessXMLDataException {

        simpleFrqList = new ArrayList<Field>();
        try {
            if (objXmlSimpleFrequencyAnalysis.getSortorder() != null) {
                this.sortOrder = VarListProcess.getVarValue(objXmlSimpleFrequencyAnalysis.getSortorder().getFieldName());
                this.sortOrderIncreasing = objXmlSimpleFrequencyAnalysis.getSortorder().isIncreasing();
            }
            if (objXmlSimpleFrequencyAnalysis.getThreshold() != null) {

                this.threshHold = objXmlSimpleFrequencyAnalysis.getThreshold().getValue();
                this.threshHoldMore = objXmlSimpleFrequencyAnalysis.getThreshold().isMore();
            }
            if (objXmlSimpleFrequencyAnalysis.getFields() != null) {
                ListIterator rlListItr = objXmlSimpleFrequencyAnalysis.getFields().getField().listIterator();

                while (rlListItr.hasNext()) {
                    Field varFld = (Field) rlListItr.next();
                    varFld.setFieldName(VarListProcess.getVarValue(varFld.getFieldName()));
                    if (varFld != null) {
                        simpleFrqList.add(varFld);
                    }
                }

                dbHandler.createSimpleFrequencyTable(this.tblName, simpleFrqList);
                this.mapSimpleFRQ = dbHandler.getMapSimpleFRQ();
                this.prepStmntSimple = dbHandler.getPrepStmntSimple();
                this.fieldsSimpleFRQ = dbHandler.getFieldsSimpleFRQ();

            }
        } catch (DBHandlerException ex) {
            throw new ProcessXMLDataException(ex);
        }
    }

    public void execute(DBHandler dbHandler) throws ProfilerRuleException {
        try {

            dbHandler.insertSimpleFRQ(this.fieldsSimpleFRQ, this.prepStmntSimple, this.mapSimpleFRQ);
        } catch (DBHandlerException ex) {

            throw new ProfilerRuleException(ex);

        }
    }

    public void getReport(DBHandler dbHandler) throws DBHandlerException {

        dbHandler.writeReportSimpleFRQ("SF", this.tblName,this.mapSimpleFRQ, this.threshHold, this.threshHoldMore, this.sortOrderIncreasing, this.sortOrder);

    }
}
