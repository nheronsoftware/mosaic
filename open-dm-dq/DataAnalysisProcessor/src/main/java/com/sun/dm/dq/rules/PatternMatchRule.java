/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html
 * or mural/license.txt. See the License for the specific language 
 * governing permissions and limitations under the License.  
 *
 * When distributing Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at mural/license.txt.
 * If applicable, add the following below the CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.dm.dq.rules;

import com.sun.dm.dq.rules.data.DataObjectHandler;
import com.sun.dm.dq.rules.exception.DataObjectHandlerException;
import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import com.sun.dm.dq.rules.output.RuleErrorObject;
import com.sun.dm.dq.rules.process.rulestep.RuleStep;
import com.sun.dm.dq.rules.variablelist.VarListProcess;
import com.sun.dm.dq.schema.ProfilingRuleType;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class PatternMatchRule extends RuleStep {

    private String matchPattern;
    private boolean isFound;
    private String ePath;
    private Pattern pattern;

    /** Creates a new instance of PatternMatchRule */
    public PatternMatchRule(ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.PatternMatch objXmlMatchFromFile) throws ProcessXMLDataException {
        processXMLObject(objXmlMatchFromFile);
    }

    public RuleErrorObject execute(DataObjectHandler dataObjHandler) {

        String value = "";
        try {
            value = dataObjHandler.getFieldValue(ePath);

            if (value != null) {
                Matcher matcher = pattern.matcher(value);
                boolean matchFound = matcher.matches();
                if (isFound == true) {
                    if (matchFound == false) {
                        return new RuleErrorObject("EPath :: " + ePath, " PatternMatch", "" + matchPattern, value, "ERR008: String pattern of the value does not match", false);
                    }

                } else {
                    if (matchFound == true) {
                        return new RuleErrorObject("EPath :: " + ePath, " PatternMatch", "" + matchPattern, value, "ERR009: String pattern of the value matched", false);
                    }
                }
            }

        } catch (DataObjectHandlerException ex) {
            return new RuleErrorObject("EPath :: " + ePath, "PatternMatch", "" + matchPattern, value, ex.getMessage(), true);
        }

        return null;
    }

    private void processXMLObject(ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.PatternMatch objXmlMatchFromFile)
            throws ProcessXMLDataException {

        matchPattern = objXmlMatchFromFile.getMatchPattern();
        ePath = VarListProcess.getVarValue(objXmlMatchFromFile.getFieldName());
        isFound = objXmlMatchFromFile.isFound();

        try {
            pattern = Pattern.compile(matchPattern);
            if (pattern == null) {
                throw new ProcessXMLDataException("String Pattern " + matchPattern + " is incorrect");
            }
        } catch (PatternSyntaxException e) {
            throw new ProcessXMLDataException(e);
        }
    }
}
