/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html
 * or mural/license.txt. See the License for the specific language 
 * governing permissions and limitations under the License.  
 *
 * When distributing Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at mural/license.txt.
 * If applicable, add the following below the CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.dm.dq.rules;

import com.sun.dm.dq.rules.data.DataObjectHandler;
import com.sun.dm.dq.rules.exception.DataObjectHandlerException;
import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import com.sun.dm.dq.rules.output.RuleErrorObject;
import com.sun.dm.dq.rules.process.rulestep.RuleStep;
import com.sun.dm.dq.rules.variablelist.VarListProcess;
import com.sun.dm.dq.schema.ProfilingRuleType;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class DataLengthRule extends RuleStep {

    short length;
    String ePath;
    boolean isMore;

    /** Creates a new instance of assignRule */
    public DataLengthRule(ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.DataLength objXml) throws ProcessXMLDataException {

        processXMLObject(objXml);
    }

    public RuleErrorObject execute(DataObjectHandler dataObjHandler) {
        /*
         *RuleErrorObject(String ruleStepName,String ruleValue,String passedValue,String failReason,boolean sysError)
         */
        String value = "";
        try {
            value = dataObjHandler.getFieldValue(ePath);

            if (value != null) {
                if ((value.length() >= length) == isMore) {
                    return null;
                } else {
                    return new RuleErrorObject("EPath :: " + ePath, "DataLength", "" + length, value, "ERR002: The value is more", false);
                }
            } else {
                return new RuleErrorObject("EPath :: " + ePath, "DataLength", "" + length, "NULL", "ERR003: The Value is Null", false);
            }
        } catch (DataObjectHandlerException ex) {
            return new RuleErrorObject("EPath :: " + ePath, "DataLength", "" + length, value, "ERR004: " + ex.getLocalizedMessage(), true);
        }
    }

    private void processXMLObject(ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.DataLength objXml) throws ProcessXMLDataException {

        length = objXml.getLen();
        ePath = VarListProcess.getVarValue(objXml.getFieldName());
        isMore = objXml.isMore();

    }
}
