/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html
 * or mural/license.txt. See the License for the specific language 
 * governing permissions and limitations under the License.  
 *
 * When distributing Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at mural/license.txt.
 * If applicable, add the following below the CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.dm.dq.process;

import com.sun.dm.dq.rules.data.DBHandler;
import com.sun.dm.dq.rules.data.DataReader.CustomDataReader;
import com.sun.dm.dq.rules.data.DataReader.DataObjectDataReader;
import com.sun.dm.dq.rules.exception.DBHandlerException;
import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import com.sun.dm.dq.rules.exception.ProfilerRuleException;
import com.sun.dm.dq.rules.process.rulestep.ProfilingRulesImpl;
import com.sun.dm.dq.rules.variablelist.VarListProcess;
import com.sun.mdm.index.dataobject.DataObject;
import com.sun.dm.dq.schema.ProfilingRuleType;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class ProfilingProcess {

    private ProfilingRuleType profilingRule;
    private static ProfilingProcess profilingProcess;
    private String objDefFilePath;
    private String dbConnection;
    private String reportFilePath;
    private int profileSize;
    private int startFrom;
    private ProfilingRulesImpl profilingRulesImpl;
    private DBHandler dbHandler;
    private int recordCount;
    private String[] variablesUsed;
    private CustomDataReader dbReader;
    private DataObjectDataReader dataObjectdbReader;
    

    private ProfilingProcess(ProfilingRuleType objXml) throws ProcessXMLDataException, DBHandlerException {

        profilingRule = objXml;
        VarListProcess profilingVarList = null;

        Initialize();
        if (objXml.getVarList() != null) {
            profilingVarList = new VarListProcess(objXml);
        }

        dbHandler = new DBHandler(dbConnection, objDefFilePath, reportFilePath, profileSize, startFrom);

        processXMLObject(profilingRule, dbHandler);
        if (profilingVarList != null) {
            variablesUsed = profilingVarList.getVarList();
            profilingVarList.removeAll();
            profilingVarList = null;
        }

    }

    /**
     *
     * @param dataObject
     */
    public void executeRules(DataObject dataObject) throws ProfilerRuleException, DBHandlerException {
        int i =0;
        if (dataObject == null) {
            dbHandler.commitData();
        }

        if (recordCount > 5000) {
            recordCount = 0;
            dbHandler.commitData();
            i++;
        } else {
            recordCount++;
        }

        dbHandler.setDataObject(dataObject);
        profilingRulesImpl.executeRules(dbHandler);
        
    }

    public void generateReport() throws ProfilerRuleException, DBHandlerException {
        dbHandler.setRecordStamp();
        dbHandler.commitData();
        profilingRulesImpl.getReport(dbHandler);
        dbHandler.cleanTemps();
        
    }

    public DataObjectDataReader getDataObjectdbReader() {
        return dataObjectdbReader;
    }

    public CustomDataReader getDbReader() {
        return dbReader;
    }

    public String getObjectDefFilePath() throws ProfilerRuleException, DBHandlerException {
        return objDefFilePath;
    }

    public String getDBPath() throws ProfilerRuleException, DBHandlerException {
        return dbConnection;
    }

    public int getStartFrom() {
        return startFrom;
    }

    public int getProfileSize() {
        return profileSize;
    }

    public String[] getVariableList() {
        return variablesUsed;
    }

    static ProfilingProcess getProfilingProcess(ProfilingRuleType objXml) throws ProcessXMLDataException, DBHandlerException {

        profilingProcess = new ProfilingProcess(objXml);
        return profilingProcess;
    }

    private void Initialize() {

        objDefFilePath = profilingRule.getProfilerVariable().getObjectdefFilePath();
        reportFilePath = profilingRule.getProfilerVariable().getReportFilePath();
        profileSize = profilingRule.getProfilerVariable().getProfileSize();
        startFrom = profilingRule.getProfilerVariable().getStartFrom();
        dbConnection = profilingRule.getProfilerVariable().getDBconnection();
        recordCount = 0;
    }

    private void processXMLObject(ProfilingRuleType objXml, DBHandler dbHandler) throws ProcessXMLDataException {

        profilingRulesImpl = new ProfilingRulesImpl(profilingRule, dbHandler);

        if (objXml.getDatareader() != null) {
            dbReader = new CustomDataReader(objXml.getDatareader());
        } else {
            dbReader = null;
        }
        if (objXml.getDataObjectReader() != null) {
            dataObjectdbReader = new DataObjectDataReader(objXml.getDataObjectReader());
        } else {
            dataObjectdbReader = null;
        }

    }
}
