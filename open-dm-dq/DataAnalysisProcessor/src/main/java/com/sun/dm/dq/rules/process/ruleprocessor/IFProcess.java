/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html
 * or mural/license.txt. See the License for the specific language 
 * governing permissions and limitations under the License.  
 *
 * When distributing Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at mural/license.txt.
 * If applicable, add the following below the CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.dm.dq.rules.process.ruleprocessor;

import com.sun.dm.dq.rules.condition.ConditionValue;
import com.sun.dm.dq.rules.condition.FalseCondition;
import com.sun.dm.dq.rules.condition.TrueCondition;
import com.sun.dm.dq.rules.data.DataObjectHandler;
import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import com.sun.dm.dq.rules.output.RuleErrorObject;
import com.sun.dm.dq.rules.process.rulestep.RuleStep;
import com.sun.dm.dq.schema.CleansingRuleType;
import com.sun.dm.dq.schema.IfStepType;
import com.sun.dm.dq.schema.ProfilingRuleType;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class IFProcess extends RuleStep {

    private ConditionProcess conditionprocess;
    private ElseProcess elseprocess;
    private ThenProcess thenprocess;
    private IfStepType ifStepdatablock;
    private CleansingRuleType.RuleList.Rule.If ifdatablock;

    /**
     * Creates a new instance of IFProcess
     */
    public IFProcess(IfStepType ifdata) throws ProcessXMLDataException {

        processXMLObject(ifdata);
    }

    public IFProcess(CleansingRuleType.RuleList.Rule.If ifdata) throws ProcessXMLDataException {

        processXMLObject(ifdata);
    }

    public IFProcess(ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.If ifdata) throws ProcessXMLDataException {

        processXMLObject(ifdata);
    }

    private void processXMLObject(IfStepType objXmlIfStep) throws ProcessXMLDataException {
        conditionprocess = new ConditionProcess(objXmlIfStep.getCondition());
        thenprocess = null;
        elseprocess = null;


    }

    private void processXMLObject(CleansingRuleType.RuleList.Rule.If objXmlIf) throws ProcessXMLDataException {

        if (objXmlIf.getCondition() != null) {
            conditionprocess = new ConditionProcess(objXmlIf.getCondition());
        // thenprocess.execute();
        }
        if (objXmlIf.getThen() != null) {
            thenprocess = new ThenProcess(objXmlIf.getThen());
        // thenprocess.execute();
        }
        if (objXmlIf.getElse() != null) {
            elseprocess = new ElseProcess(objXmlIf.getElse());
        //elseprocess.execute();
        }

    }

    private void processXMLObject(ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.If objXmlIf) throws ProcessXMLDataException {

        if (objXmlIf.getCondition() != null) {
            conditionprocess = new ConditionProcess(objXmlIf.getCondition());
        // thenprocess.execute();
        }
        if (objXmlIf.getThen() != null) {
            thenprocess = new ThenProcess(objXmlIf.getThen());
        // thenprocess.execute();
        }
        if (objXmlIf.getElse() != null) {
            elseprocess = new ElseProcess(objXmlIf.getElse());
        //elseprocess.execute();
        }

    }

    public RuleErrorObject execute(DataObjectHandler dataObjHandler) {

        RuleErrorObject ruleErr = null;
        ConditionValue condVal = conditionprocess.getConditionValue(dataObjHandler);
        if (condVal == null) return null;
        if (condVal.getClass() == TrueCondition.class) {
            if (thenprocess != null) {
                ruleErr = thenprocess.execute(dataObjHandler);
            }
        // thenprocess.execute();
        } else if (condVal.getClass() == FalseCondition.class) {
            if (elseprocess != null) {
                ruleErr = elseprocess.execute(dataObjHandler);
            }
        //elseprocess.execute();
        }

        return ruleErr;

    }
}
