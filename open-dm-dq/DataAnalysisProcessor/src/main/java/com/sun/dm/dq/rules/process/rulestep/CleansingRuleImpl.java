/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html
 * or mural/license.txt. See the License for the specific language 
 * governing permissions and limitations under the License.  
 *
 * When distributing Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at mural/license.txt.
 * If applicable, add the following below the CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */

package com.sun.dm.dq.rules.process.rulestep;

import com.sun.dm.dq.rules.AssignRule;
import com.sun.dm.dq.rules.DataLengthRule;
import com.sun.dm.dq.rules.DateRangeRule;
import com.sun.dm.dq.rules.ExecuteJavaRule;
import com.sun.dm.dq.rules.MatchFromFileRule;
import com.sun.dm.dq.rules.PatternMatchRule;
import com.sun.dm.dq.rules.PatternReplaceRule;
import com.sun.dm.dq.rules.RaiseRule;
import com.sun.dm.dq.rules.RangeRule;
import com.sun.dm.dq.rules.RejectFieldRule;
import com.sun.dm.dq.rules.RejectObjectRule;
import com.sun.dm.dq.rules.ReplaceRule;
import com.sun.dm.dq.rules.ReturnRule;
import com.sun.dm.dq.rules.TruncateRule;
import com.sun.dm.dq.rules.ValidateDBFieldRule;
import com.sun.dm.dq.rules.data.DataObjectHandler;
import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import com.sun.dm.dq.rules.output.RuleErrorObject;
import com.sun.dm.dq.rules.process.ruleprocessor.IFProcess;
import com.sun.dm.dq.schema.CleansingRuleType.RuleList;
import com.sun.dm.dq.schema.CleansingRuleType.RuleList.Rule.Assign;
import com.sun.dm.dq.schema.CleansingRuleType.RuleList.Rule.ExecuteJava;
import com.sun.dm.dq.schema.CleansingRuleType.RuleList.Rule.If;
import com.sun.dm.dq.schema.CleansingRuleType.RuleList.Rule.MatchFromFile;
import com.sun.dm.dq.schema.CleansingRuleType.RuleList.Rule.PatternReplace;
import com.sun.dm.dq.schema.CleansingRuleType.RuleList.Rule.Reject;
import com.sun.dm.dq.schema.CleansingRuleType.RuleList.Rule.Replace;
import com.sun.dm.dq.schema.CleansingRuleType.RuleList.Rule.Truncate;
import com.sun.dm.dq.schema.CleansingRuleType.RuleList.Rule.ValidateDBField;
import com.sun.dm.dq.schema.IfStepType;
import com.sun.dm.dq.schema.ProfilingRuleType;
import com.sun.dm.dq.schema.ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.PatternMatch;
import com.sun.dm.dq.schema.ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.Raise;
import com.sun.dm.dq.schema.ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.Range;
import com.sun.dm.dq.schema.ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.Return;
import com.sun.dm.dq.schema.ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.DataLength;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;



/**
 *
 * @author abhijeet.gupta@sun.com
 */


public class CleansingRuleImpl implements Rule{
    
    RuleList.Rule ruleStepXmlObj;
    List<Object> ruleXmlList ;
    List<Object> ruleStepList ;
    /**
     * Creates a new instance of CleansingRuleImpl
     */
    public CleansingRuleImpl(RuleList.Rule objRuleSteps) throws ProcessXMLDataException {
        processXMLObject(objRuleSteps);
        
    }
    public CleansingRuleImpl(ArrayList<Object> objList) throws ProcessXMLDataException {
        processXMLObject(objList);
    }
    
    public ArrayList <RuleErrorObject> execute(DataObjectHandler dataObjHandler) {
        ListIterator rlListItr = ruleStepList.listIterator();
        ArrayList <RuleErrorObject> list = new ArrayList <RuleErrorObject>();
        RuleErrorObject ruleErr=null;
        while (rlListItr.hasNext()){
            RuleStep varRuleStep = (RuleStep) rlListItr.next();
            if (varRuleStep != null){
                ruleErr = varRuleStep.execute(dataObjHandler);
                if (ruleErr != null) {
                    list.add(ruleErr);
                }
            }
        }
        if (list.size() == 0) {
            return null;
        }
        return list;
    }
    
   
    
    /*
     *          @XmlElement(name = "truncate", type = CleansingRuleType.RuleList.Rule.Truncate.class),
                @XmlElement(name = "patternReplace", type = CleansingRuleType.RuleList.Rule.PatternReplace.class),
                @XmlElement(name = "matchFromFile", type = CleansingRuleType.RuleList.Rule.MatchFromFile.class),
                @XmlElement(name = "reject", type = CleansingRuleType.RuleList.Rule.Reject.class),
                @XmlElement(name = "validateDBField", type = CleansingRuleType.RuleList.Rule.ValidateDBField.class),
                @XmlElement(name = "replace", type = CleansingRuleType.RuleList.Rule.Replace.class),
                @XmlElement(name = "assign", type = CleansingRuleType.RuleList.Rule.Assign.class),
                @XmlElement(name = "raise", type = com.sun.schema.di.ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.Raise.class),
                @XmlElement(name = "range", type = com.sun.schema.di.ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.Range.class),
                @XmlElement(name = "if", type = CleansingRuleType.RuleList.Rule.If.class),
                @XmlElement(name = "patternMatch", type = com.sun.schema.di.ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.PatternMatch.class),
                @XmlElement(name = "executeJava", type = CleansingRuleType.RuleList.Rule.ExecuteJava.class),
                @XmlElement(name = "return", type = com.sun.schema.di.ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.Return.class)
     */
    private void processXMLObject(RuleList.Rule objRuleSteps) throws ProcessXMLDataException {
        
        processXMLObject((ArrayList<Object>) objRuleSteps.getCleansingRuleStepGroup());
        
    }
    
    private void processXMLObject(ArrayList<Object> objRuleSteps) throws ProcessXMLDataException {
        
        
        ruleXmlList = new ArrayList<Object>();
        ruleStepList = new ArrayList<Object>();
        ruleXmlList = objRuleSteps;
        
        ListIterator rlListItr = ruleXmlList.listIterator();
        while (rlListItr.hasNext()){
            Object var = rlListItr.next();
            if (var.getClass()  == If.class){
                ruleStepList.add(new IFProcess((If) var)) ;
                
            }  else if (var.getClass()  == IfStepType.class)  {
                ruleStepList.add(new IFProcess((IfStepType) var)) ;
                
            }  else if (var.getClass()  == Assign.class)  {
                ruleStepList.add(new AssignRule((Assign) var)) ;
                
            }  else if (var.getClass()  == ExecuteJava.class)  {
                ruleStepList.add(new ExecuteJavaRule((ExecuteJava) var)) ;
                
            }  else if (var.getClass()  == Replace.class)  {
                ruleStepList.add(new ReplaceRule((Replace) var)) ;
                
            }  else if (var.getClass()  == MatchFromFile.class)  {
                ruleStepList.add(new MatchFromFileRule((MatchFromFile) var)) ;
                
            }  else if (var.getClass()  == Reject.class)  {
                //ruleStepList.add(new RejectFieldRule((Reject) var)) ;
                ruleStepList.add(new RejectObjectRule((Reject) var)) ;
                
            }  else if (var.getClass()  == PatternReplace.class)  {
                ruleStepList.add(new PatternReplaceRule((PatternReplace) var)) ;
                
            }  else if (var.getClass()  == Truncate.class)  {
                ruleStepList.add(new TruncateRule((Truncate) var)) ;
                
            }  else if (var.getClass()  == ValidateDBField.class)  {
                ruleStepList.add(new ValidateDBFieldRule((ValidateDBField) var)) ;
                
            }  else if (var.getClass()  == Raise.class)  {
                ruleStepList.add(new RaiseRule((Raise) var)) ;
                
            }  else if (var.getClass()  == Range.class)  {
                ruleStepList.add(new RangeRule((Range) var)) ;
                
            }  else if (var.getClass()  == Return.class)  {
                ruleStepList.add(new ReturnRule((Return) var)) ;
                
            }  else if (var.getClass()  == PatternMatch.class)  {
                ruleStepList.add(new PatternMatchRule((PatternMatch) var)) ;
                
            } else if (var.getClass()  == DataLength.class)  {
                ruleStepList.add(new DataLengthRule((DataLength) var)) ;
                
            } else if (var.getClass()  == ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.DateRange.class)  {
                ruleStepList.add(new DateRangeRule((ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.DateRange) var)) ;
               
            }
        }
        
    }
}