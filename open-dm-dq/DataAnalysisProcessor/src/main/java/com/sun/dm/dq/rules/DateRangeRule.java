/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html
 * or mural/license.txt. See the License for the specific language 
 * governing permissions and limitations under the License.  
 *
 * When distributing Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at mural/license.txt.
 * If applicable, add the following below the CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.dm.dq.rules;

import com.sun.dm.dq.rules.data.DataObjectHandler;
import com.sun.dm.dq.rules.exception.DataObjectHandlerException;
import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import com.sun.dm.dq.rules.output.RuleErrorObject;
import com.sun.dm.dq.rules.process.rulestep.RuleStep;
import com.sun.dm.dq.rules.variablelist.VarListProcess;
import com.sun.dm.dq.schema.ProfilingRuleType;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class DateRangeRule extends RuleStep {

    private String strDateMax;
    private String strDateMin;
    private Date dateMax;
    private Date dateMin;
    private String ePath;

    public DateRangeRule(ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.DateRange rangeXmlObj) throws ProcessXMLDataException {

        processXMLObject(rangeXmlObj);

    }

    public RuleErrorObject execute(DataObjectHandler dataObjHandler) {
        Date dateVal;

        String value = "";
        try {

            value = dataObjHandler.getFieldValue(ePath);
            SimpleDateFormat df = new SimpleDateFormat(dataObjHandler.getDateFormat());
            dateVal = df.parse(value);

            if (strDateMin == null && strDateMax == null) {
                return null;

            } else if (strDateMin == null) {
		if (dateMax == null) {
		    dateMax = df.parse(strDateMax);
		}
                if (dateVal.compareTo(dateMax) > 0) {
                    return new RuleErrorObject("EPath :: " + ePath, "DateRange", 
                            dateVal.toString(), dateMax.toString(), "ERR005: Date is more than the maximum date", false);
                } else {
                    return null;
                }

            } else if (strDateMax == null) {
		if (dateMin == null) {
		   dateMin = df.parse(strDateMin);
		}
                if (dateVal.compareTo(dateMin) < 0) {
                    return new RuleErrorObject("EPath :: " + ePath, "DateRange", 
                            dateVal.toString(), dateMin.toString(), "ERR006: Date is less than the minimum date", false);
                } else {
                    return null;
                }
            } else {
		if (dateMax == null) {
		    dateMax = df.parse(strDateMax);
		}
		if (dateMin == null) {
		    dateMin = df.parse(strDateMin);
		}
                if (dateVal.compareTo(dateMin) < 0) {
                    return new RuleErrorObject("EPath :: " + ePath, "DateRange", 
                            dateVal.toString(), dateMin.toString(), "ERR006: Date is less than the minimum date", false);
                } else if (dateVal.compareTo(dateMax) > 0) {
                    return new RuleErrorObject("EPath :: " + ePath, "DateRange", dateVal.toString(), dateMax.toString(), "ERR005: Date is more than the maximum date", false);
                } else {
                    return null;
                }
            }

        } catch (DataObjectHandlerException ex) {
            return new RuleErrorObject("EPath :: " + ePath, "DateRange", "", "", ex.getLocalizedMessage(), true);
        } catch (ParseException ex) {
            return new RuleErrorObject("EPath :: " + ePath, "DateRange", "", "", ex.getLocalizedMessage(), true);
        }
    }

    private void processXMLObject(ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.DateRange rangeXmlObj)
            throws ProcessXMLDataException {
           String strTmp;
           strTmp = rangeXmlObj.getMin();

	   // Dates will be parsed when processing the first record to ensure correct date format is used
	   dateMin = null;
	   dateMax = null;

            if (strTmp == null) {
            strDateMin = null;
            } else if (strTmp.trim() == "") {
            strDateMin = null;
            } else {
            strDateMin = strTmp;
            }

            strTmp = null;
            strTmp = rangeXmlObj.getMax();

            if (strTmp == null) {
            strDateMax = null;
            } else if (strTmp.trim() == "") {
            strDateMax = null;
            } else {
            strDateMax = strTmp;
            }
            ePath = VarListProcess.getVarValue(rangeXmlObj.getFieldName());
    }
}
