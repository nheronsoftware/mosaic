/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html
 * or mural/license.txt. See the License for the specific language 
 * governing permissions and limitations under the License.  
 *
 * When distributing Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at mural/license.txt.
 * If applicable, add the following below the CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */

package com.sun.dm.dq.rules.process.ruleprocessor;

import com.sun.dm.dq.rules.condition.AndCondition;
import com.sun.dm.dq.rules.condition.ConditionClass;
import com.sun.dm.dq.rules.condition.ConditionValue;
import com.sun.dm.dq.rules.condition.DataLengthCondition;
import com.sun.dm.dq.rules.condition.EqualsCondition;
import com.sun.dm.dq.rules.condition.ExecuteJavaBooleanCondition;
import com.sun.dm.dq.rules.condition.FalseCondition;
import com.sun.dm.dq.rules.condition.IsNullCondition;
import com.sun.dm.dq.rules.condition.MatchesCondition;
import com.sun.dm.dq.rules.condition.NotCondition;
import com.sun.dm.dq.rules.condition.OrCondition;
import com.sun.dm.dq.rules.condition.TrueCondition;
import com.sun.dm.dq.rules.data.DataObjectHandler;
import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import com.sun.dm.dq.schema.IfStepType;
import java.util.ArrayList;
import java.util.ListIterator;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class ConditionProcess {
    
    IfStepType.Condition objXmlCondition;
    ArrayList<Object> conditionXmlList;
    /**
     * Creates a new instance of ConditionProcess
     */
    public ConditionProcess(IfStepType.Condition objXml) throws ProcessXMLDataException {
        
        processXMLObject(objXml);
    }
    public ConditionValue getConditionValue(DataObjectHandler dataObjHandler) {
        
        ListIterator rlListItr = conditionXmlList.listIterator();
        ConditionValue resultCondition=null;
        while (rlListItr.hasNext()){
            ConditionClass varCondition = (ConditionClass) rlListItr.next();
            if (varCondition != null){
                resultCondition = varCondition.getConditionValue(dataObjHandler);
               /* if(resultCondition.getClass() == FalseCondition.class) {
                    return resultCondition;
                }*/
                
            }
        }
        return resultCondition ;
    }
    private void processXMLObject(IfStepType.Condition objXmlCondition) throws ProcessXMLDataException {
         conditionXmlList = new ArrayList<Object>();
        if (objXmlCondition.getAnd() != null) {
            conditionXmlList.add(new AndCondition( objXmlCondition.getAnd()));
        }
        if (objXmlCondition.getNot() != null) {
            conditionXmlList.add(new NotCondition( objXmlCondition.getNot()));
        }
        if (objXmlCondition.getOr() != null) {
            conditionXmlList.add(new OrCondition( objXmlCondition.getOr()));
        }
        if (objXmlCondition.getDataLength() != null) {
            conditionXmlList.add(new DataLengthCondition( objXmlCondition.getDataLength()));
        }
        if (objXmlCondition.getEquals() != null) {
            conditionXmlList.add(new EqualsCondition( objXmlCondition.getEquals()));
        }
        if (objXmlCondition.getIsnull() != null) {
            conditionXmlList.add(new IsNullCondition( objXmlCondition.getIsnull()));
        }
        if (objXmlCondition.getMatches() != null) {
            conditionXmlList.add(new MatchesCondition( objXmlCondition.getMatches()));
        }
        if (objXmlCondition.getExecuteJavaBoolean() != null) {
            conditionXmlList.add(new ExecuteJavaBooleanCondition( objXmlCondition.getExecuteJavaBoolean()));
        } 
         
        
    }
    
}
