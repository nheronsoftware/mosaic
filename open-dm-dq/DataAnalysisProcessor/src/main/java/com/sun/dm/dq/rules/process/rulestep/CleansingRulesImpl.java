/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html
 * or mural/license.txt. See the License for the specific language 
 * governing permissions and limitations under the License.  
 *
 * When distributing Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at mural/license.txt.
 * If applicable, add the following below the CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */

package com.sun.dm.dq.rules.process.rulestep;

import com.sun.dm.dq.rules.data.DataObjectHandler;
import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import com.sun.dm.dq.rules.output.RuleErrorObject;
import com.sun.dm.dq.schema.CleansingRuleType;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class CleansingRulesImpl implements Rules {
    
    
    List<CleansingRuleImpl> ruleList ;
    
    /**
     * Creates a new instance of CleansingRulesImpl
     */
    public CleansingRulesImpl(CleansingRuleType.RuleList objRuleList) throws ProcessXMLDataException {
       
        processXMLObject(objRuleList);
        
        
        
    }
   
    
    public ArrayList<ArrayList<RuleErrorObject>> executeRules(DataObjectHandler dataObjHandler) {
        
        
        ListIterator rlListItr = ruleList.listIterator();
        CleansingRuleImpl tmpRuleImpl;
        ArrayList<ArrayList<RuleErrorObject>> rulesErr = new ArrayList<ArrayList<RuleErrorObject>>();
        ArrayList<RuleErrorObject> ruleErr;
        while (rlListItr.hasNext()){
            tmpRuleImpl = (CleansingRuleImpl)(rlListItr.next());
            if (tmpRuleImpl != null) {
                ruleErr = tmpRuleImpl.execute(dataObjHandler);
                if (ruleErr != null) {
                    rulesErr.add(ruleErr);
                }
            }
        }
        if (rulesErr.size()==0) {
            return null;
        }
        return rulesErr;
        
    }
    
    private void processXMLObject(CleansingRuleType.RuleList objRuleList) throws ProcessXMLDataException {
        List<CleansingRuleType.RuleList.Rule> ruleXmlList ;
        ruleXmlList = objRuleList.getRule();
        ListIterator rlListItr = ruleXmlList.listIterator();
        ruleList = new ArrayList<CleansingRuleImpl>();
        while (rlListItr.hasNext()){
            com.sun.dm.dq.schema.CleansingRuleType.RuleList.Rule varRule = (com.sun.dm.dq.schema.CleansingRuleType.RuleList.Rule) rlListItr.next();
            if (varRule != null){
                ruleList.add(new CleansingRuleImpl(varRule));
            }
            
        }
        
    }
    

    
}
