/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html
 * or mural/license.txt. See the License for the specific language 
 * governing permissions and limitations under the License.  
 *
 * When distributing Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at mural/license.txt.
 * If applicable, add the following below the CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.dm.dq.rules.data;

import com.sun.dm.dq.rules.exception.DBHandlerException;
import com.sun.dm.dq.rules.exception.DataObjectHandlerException;
import com.sun.mdm.index.dataobject.DataObject;
import com.sun.dm.dq.schema.FieldListVarType.Field;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class DBHandler {

    private String DBNAME = "TEMPDB";
    private String uri;
    private DataObjectHandler DOHandler;
    private Connection conn;
    private String stmtSimpleFRQ;
    private String stmtConstrainedFRQ;
    private String stmtPatternFRQ;
    private HashMap<String, String> mapSimpleFRQ;
    private HashMap<String, String> mapConstrainedFRQ;
    private HashMap<String, String> mapPatternFRQ;
    private String reportFilePath;
    private int profileSize;
    private int startFrom;
    private String recordCountStamp;
    private PreparedStatement prepStmntConstrained;
    private PreparedStatement prepStmntSimple;
    private PreparedStatement prepStmntPattern;
    private String[] fieldsSimpleFRQ;
    private String[] fieldsConstrainedFRQ;
    private String[] fieldsPatternFRQ;

    public DBHandler(String conStr, String objDefFilePath, String reportFilePath, int profileSize, int startFrom) throws DBHandlerException {
        try {

            this.reportFilePath = reportFilePath;
            this.profileSize = profileSize;
            this.startFrom = startFrom;

            DBNAME = "TEMPDB";
            Class.forName("org.axiondb.jdbc.AxionDriver");

            uri = "jdbc:axiondb:" + DBNAME + ":" + reportFilePath;
            DOHandler = new DataObjectHandler(objDefFilePath);

            conn = DriverManager.getConnection(uri);
            conn.setAutoCommit(false);

        } catch (ClassNotFoundException ex) {
            throw new DBHandlerException(ex);
        } catch (SQLException ex) {
            throw new DBHandlerException(ex);
        }
    }

    public void setDataObject(DataObject dataObject) {

        this.DOHandler.setParent(dataObject);

    }

    public void commitData() throws DBHandlerException {
        try {

            conn.commit();
        } catch (SQLException ex) {
            throw new DBHandlerException(ex);
        }

    }

    public DataObject getDataObject() {

        return this.DOHandler.getParent();

    }

    public DataObjectHandler getDataObjectHandler() {

        return this.DOHandler;

    }

    public void createSimpleFrequencyTable(String tblName, ArrayList<Field> fields) throws DBHandlerException {

        try {
            String fieldsStr = "";
            fieldsStr = prepareDBFieldString(fields);
            createDBTable(tblName, fieldsStr);
            fieldsStr = prepareInsertFieldString(fields);
            stmtSimpleFRQ = prepareInsertStatement(tblName, fieldsStr);

            //prepStmntSimple = conn.prepareStatement(stmtSimpleFRQ);
            mapSimpleFRQ = fieldsToMap(fields);
            fieldsSimpleFRQ = fieldsArray(fields);
        } catch (Exception ex) {
            throw new DBHandlerException(ex);
        }
    }

    public PreparedStatement getPrepStmntConstrained() throws DBHandlerException {
        try {
            return conn.prepareStatement(stmtConstrainedFRQ);

        } catch (SQLException ex) {

            throw new DBHandlerException(ex);
        }
    }

    public PreparedStatement getPrepStmntPattern() throws DBHandlerException {
        try {
            return conn.prepareStatement(stmtPatternFRQ);

        } catch (SQLException ex) {

            throw new DBHandlerException(ex);
        }
    }

    public PreparedStatement getPrepStmntSimple() throws DBHandlerException {
        try {
            return conn.prepareStatement(stmtSimpleFRQ);

        } catch (SQLException ex) {

            throw new DBHandlerException(ex);
        }

    }

    public String[] getFieldsConstrainedFRQ() {
        return fieldsConstrainedFRQ;
    }

    public String[] getFieldsPatternFRQ() {
        return fieldsPatternFRQ;
    }

    public String[] getFieldsSimpleFRQ() {
        return fieldsSimpleFRQ;
    }

    public HashMap<String, String> getMapConstrainedFRQ() {
        return mapConstrainedFRQ;
    }

    public HashMap<String, String> getMapPatternFRQ() {
        return mapPatternFRQ;
    }

    public HashMap<String, String> getMapSimpleFRQ() {
        return mapSimpleFRQ;
    }

    public String getStmtConstrainedFRQ() {
        return stmtConstrainedFRQ;
    }

    public String getStmtPatternFRQ() {
        return stmtPatternFRQ;
    }

    public String getStmtSimpleFRQ() {
        return stmtSimpleFRQ;
    }

    private String[] fieldsArray(ArrayList<Field> fields) {
        String[] arrString = null;
        Field varFld = null;
        if (fields != null) {
            arrString = new String[fields.size()];
            ListIterator rlListItr = fields.listIterator();
            int i = 0;
            while (rlListItr.hasNext()) {
                varFld = (Field) rlListItr.next();
                arrString[i] = varFld.getFieldName();
                i++;

            }

        }
        return arrString;

    }

    private HashMap<String, String> fieldsToMap(ArrayList<Field> fields) {
        HashMap<String, String> tmpMap = null;
        Field varFld = null;
        if (fields != null) {
            tmpMap = new HashMap<String, String>();
            ListIterator rlListItr = fields.listIterator();

            while (rlListItr.hasNext()) {
                varFld = (Field) rlListItr.next();
                tmpMap.put(varFld.getFieldName(), replaceStrings(varFld.getFieldName()).replace(".", "_"));

            }
        }
        return tmpMap;
    }

    private String replaceStrings(String str) {

        if (str != null) {

            str = str.replaceAll("\\[[0-9*]+\\]", "");
            str = str.replace("[", "");
            str = str.replace("]", "");

        }
        return str;
    }

    public void createConstraintFrequencyTable(String tblName, ArrayList<Field> fields) throws DBHandlerException {

        try {
            String fieldsStr = "";
            fieldsStr = prepareDBFieldString(fields);
            createDBTable(tblName, fieldsStr);
            fieldsStr = prepareInsertFieldString(fields);
            stmtConstrainedFRQ = prepareInsertStatement(tblName, fieldsStr);

            prepStmntConstrained = conn.prepareStatement(stmtConstrainedFRQ);
            mapConstrainedFRQ = fieldsToMap(fields);
            fieldsConstrainedFRQ = fieldsArray(fields);
        } catch (SQLException ex) {
            throw new DBHandlerException(ex);
        }
    }

    public void createPatternFrequencyTable(String tblName, ArrayList<Field> fields) throws DBHandlerException {

        try {

            String fieldsStr = "";
            fieldsStr = prepareDBFieldString(fields);
            createDBTable(tblName, fieldsStr);
            fieldsStr = prepareInsertFieldString(fields);
            stmtPatternFRQ = prepareInsertStatement(tblName, fieldsStr);

            prepStmntPattern = conn.prepareStatement(stmtPatternFRQ);
            mapPatternFRQ = fieldsToMap(fields);
            fieldsPatternFRQ = fieldsArray(fields);
        } catch (SQLException ex) {
            throw new DBHandlerException(ex);
        }

    }

    private String prepareInsertStatement(String tblName, String dbFieldsStr) throws DBHandlerException {

        String strStmnt = "";
        String param = "";
        String[] flds = dbFieldsStr.split(",");

        for (int i = 0; i < flds.length; i++) {
            param = param + "? ,";
        }
        if (param.endsWith(",")) {
            param = param.substring(0, param.length() - 1);
        }

        strStmnt = new String("INSERT INTO " + tblName +
                "( " + dbFieldsStr + " ) " +
                " values ( " + param + " )");

        return strStmnt;
    }

    private void createDBTable(String tblName, String dbFieldsStr) throws DBHandlerException {

        Statement stmt = null;

        String fieldsStr = "CREATE TABLE IF NOT EXISTS " + tblName + " (" + dbFieldsStr + ")";
        String deleteStr = "DELETE FROM  " + tblName;
        try {
            stmt = conn.createStatement();
            stmt.addBatch(fieldsStr);
            stmt.addBatch(deleteStr);
            stmt.executeBatch();
            conn.commit();

        } catch (SQLException ex) {
            throw new DBHandlerException(ex);

        } finally {
            try {
                stmt.close();
            } catch (Exception ex) {
                throw new DBHandlerException(ex);
            }

        }
    }

    public void insertRecord(String[] flds, PreparedStatement stmnt, HashMap map, boolean pattern) throws DBHandlerException {

        String strVal = "";
        List<String> list = null;

        PreparedStatement prepStmnt = stmnt;
        try {
            if (flds == null) {
                throw new DBHandlerException("Frequency Field values are not Set");
            }
            for (int i = 0; i < flds.length; i++) {

                String varKey = (String) flds[i];
                list = this.getDataObjectHandler().getFieldValueListTruncated(varKey);
                if (list == null || list.size() <= 0) {
                    return;
                }
                for (int j = 0; j < list.size(); j++) {
                    strVal = list.get(j);
                    if (strVal != null) {
                        if (pattern) {
                            strVal = strVal.replaceAll("[A-Za-z]", "A");
                            strVal = strVal.replaceAll("[0-9]", "N");
                        }
                    }
                    prepStmnt.setString(i + 1, strVal);
                    if (i + 1 == flds.length) {
                        prepStmnt.executeUpdate();
                    }
                }
            }

        } catch (SQLException ex) {
            throw new DBHandlerException(ex);
        } catch (DataObjectHandlerException ex) {
            throw new DBHandlerException(ex);
        } finally {
            try {/* if (prepStmnt != null) prepStmnt.close(); */
            } catch (Exception ex) {
                throw new DBHandlerException(ex);
            }
        }

    }

    public void insertSimpleFRQ(String[] flds, PreparedStatement stmnt, HashMap map) throws DBHandlerException {

        insertRecord(flds, stmnt, map, false);

    }

    public void insertConstrainedFRQ(String[] flds, PreparedStatement stmnt, HashMap map) throws DBHandlerException {

        insertRecord(flds, stmnt, map, false);

    }

    public void insertPatternFRQ(String[] flds, PreparedStatement stmnt, HashMap map) throws DBHandlerException {

        insertRecord(flds, stmnt, map, true);


    }

    private String prepareDBFieldString(ArrayList<Field> fields) throws DBHandlerException {

        String fldStr = "";
        com.sun.mdm.index.dataobject.objectdef.Field tmpField = null;
        try {
            if (fields != null) {
                ListIterator rlListItr = fields.listIterator();

                while (rlListItr.hasNext()) {
                    Field varFld = (Field) rlListItr.next();

                    tmpField = DOHandler.getFieldObject(replaceStrings(varFld.getFieldName()));

                    if (tmpField != null) {

                        fldStr = fldStr + replaceStrings(varFld.getFieldName()).replace(".", "_") + " varchar(" + tmpField.getSize() + "),";

                    }
                }
                if (fldStr.endsWith(",")) {
                    fldStr = fldStr.substring(0, fldStr.length() - 1);
                }
            }
        } catch (DataObjectHandlerException ex) {
            throw new DBHandlerException(ex);
        }
        return fldStr;
    }

    private String prepareInsertFieldString(ArrayList<Field> fields) throws DBHandlerException {

        String fldStr = "";
        com.sun.mdm.index.dataobject.objectdef.Field tmpField = null;
        try {
            if (fields != null) {
                ListIterator rlListItr = fields.listIterator();

                while (rlListItr.hasNext()) {
                    Field varFld = (Field) rlListItr.next();
                    tmpField = DOHandler.getFieldObject(replaceStrings(varFld.getFieldName()));

                    if (tmpField != null) {

                        fldStr = fldStr + replaceStrings(varFld.getFieldName()).replace(".", "_") + ",";
                    }
                }
                if (fldStr.endsWith(",")) {
                    fldStr = fldStr.substring(0, fldStr.length() - 1);
                }
            }
        } catch (DataObjectHandlerException ex) {
            throw new DBHandlerException(ex);
        }
        return fldStr;
    }

    private String generateReport(String repType, String tblName, HashMap<String, String> fldMap, int threshold, boolean threshHoldMore, boolean increasingOrder, String sortingField) throws DBHandlerException {

        String fldStr = "", strQuery = "";
        String tmpFldString = "", tmpSelectFldString = "", tmpInsertFldNameString = "";
        ResultSet rSet = null;
        if (fldMap != null) {
            try {

                for (int i = 0; i < fldMap.size(); i++) {
                    tmpFldString = tmpFldString + "FLDVALUE_" + String.valueOf(i) + " varchar(32), ";

                }
                Iterator rlListItr = fldMap.keySet().iterator();

                Statement stmt = conn.createStatement();
                //stmt.executeUpdate("DROP TABLE TMP_PROFILE");
                //   conn.commit();
                //select PERSON_FIRSTNAME, count(*) frq from PROFILE_SIMPLE_FRQ group by PERSON_FIRSTNAME  having frq > 0 order by frq desc;
               /* stmt.addBatch("CREATE  EXTERNAL TABLE TMP_PROFILE (" + tmpFldString + " FRQ varchar(32)) " +
                "ORGANIZATION(LOADTYPE='delimited' filename='" + reportFilePath + "/TMP_PROFILE.csv" + "' FIELDDELIMITER='\t')");*/
                stmt.executeUpdate("CREATE  EXTERNAL TABLE TMP_PROFILE (" + tmpFldString + " FRQ varchar(32)) " +
                        "ORGANIZATION(LOADTYPE='delimited' filename='" + reportFilePath + "/TMP_PROFILE.csv" + "' FIELDDELIMITER='\t')");
                while (rlListItr.hasNext()) {
                    String key = (String) rlListItr.next();
                    String fldName = fldMap.get(key);
                    tmpSelectFldString = tmpSelectFldString + fldName + ",";
                    tmpInsertFldNameString = tmpInsertFldNameString + "'" + replaceStrings(key.toUpperCase()).replace(".", "_") + "',";
                }
                if (tmpSelectFldString.endsWith(",")) {
                    tmpSelectFldString = tmpSelectFldString.substring(0, tmpSelectFldString.length() - 1);
                }
                stmt.executeUpdate("INSERT INTO TMP_PROFILE VALUES(" + tmpInsertFldNameString + " 'FREQUENCY')");
                strQuery = "INSERT INTO TMP_PROFILE SELECT " + tmpSelectFldString +
                        ", COUNT(*) FREQUENCY FROM " + tblName + " GROUP BY " + tmpSelectFldString;
                if (threshold > 0) {
                    if (threshHoldMore) {
                        strQuery = strQuery + " HAVING FREQUENCY >= '" + threshold + "'";
                    } else {
                        strQuery = strQuery + " HAVING FREQUENCY < '" + threshold + "'";
                    }
                }
                if (sortingField == null || sortingField.trim().length() == 0) {
                    strQuery = strQuery + " ORDER BY FREQUENCY ";
                } else {
                    strQuery = strQuery + " ORDER BY " + replaceStrings(sortingField.toUpperCase()).replace(".", "_");
                }

                if (increasingOrder == false) {
                    strQuery = strQuery + " DESC ";
                }
                stmt.executeUpdate(strQuery);
                // stmt.executeBatch();
                conn.commit();

                writeReportFile(reportFilePath + "/TMP_PROFILE.csv", reportFilePath + "/" + repType + "_" + tblName + "_" + this.recordCountStamp + ".csv");
                stmt = null;
                stmt = conn.createStatement();
                stmt.executeUpdate("TRUNCATE TABLE TMP_PROFILE");
                stmt.executeUpdate("DROP TABLE TMP_PROFILE");
                conn.commit();


            } catch (SQLException ex) {
                throw new DBHandlerException(ex);
            }

        }
        return fldStr;
    }
    
    private String generateReport(String repType, String tblName, HashMap<String, String> fldMap, int threshold, boolean threshHoldMore, boolean increasingOrder, String sortingField, int topNPattern, boolean topNPatternMore) throws DBHandlerException {

        String fldStr = "", strQuery = "", strTopNPattern = "";
        String tmpFldString = "", tmpSelectFldString = "", tmpInsertFldNameString = "";
        ResultSet rSet = null;
        if (fldMap != null) {
            try {

                for (int i = 0; i < fldMap.size(); i++) {
                    tmpFldString = tmpFldString + "FLDVALUE_" + String.valueOf(i) + " varchar(32), ";

                }
                Iterator rlListItr = fldMap.keySet().iterator();

                Statement stmt = conn.createStatement();
                //stmt.executeUpdate("DROP TABLE TMP_PROFILE");
                //   conn.commit();
                //select PERSON_FIRSTNAME, count(*) frq from PROFILE_SIMPLE_FRQ group by PERSON_FIRSTNAME  having frq > 0 order by frq desc;
               /* stmt.addBatch("CREATE  EXTERNAL TABLE TMP_PROFILE (" + tmpFldString + " FRQ varchar(32)) " +
                "ORGANIZATION(LOADTYPE='delimited' filename='" + reportFilePath + "/TMP_PROFILE.csv" + "' FIELDDELIMITER='\t')");*/
                stmt.executeUpdate("CREATE  EXTERNAL TABLE TMP_PROFILE (" + tmpFldString + " FRQ varchar(32)) " +
                        "ORGANIZATION(LOADTYPE='delimited' filename='" + reportFilePath + "/TMP_PROFILE.csv" + "' FIELDDELIMITER='\t')");
                while (rlListItr.hasNext()) {
                    String key = (String) rlListItr.next();
                    String fldName = fldMap.get(key);
                    tmpSelectFldString = tmpSelectFldString + fldName + ",";
                    tmpInsertFldNameString = tmpInsertFldNameString + "'" + replaceStrings(key.toUpperCase()).replace(".", "_") + "',";
                }
                if (tmpSelectFldString.endsWith(",")) {
                    tmpSelectFldString = tmpSelectFldString.substring(0, tmpSelectFldString.length() - 1);
                }
                stmt.executeUpdate("INSERT INTO TMP_PROFILE VALUES(" + tmpInsertFldNameString + " 'FREQUENCY')");
                strQuery = "INSERT INTO TMP_PROFILE SELECT " + tmpSelectFldString +
                        ", COUNT(*) FREQUENCY FROM " + tblName + " GROUP BY " + tmpSelectFldString;
                if (threshold > 0) {
                    if (threshHoldMore) {
                        strQuery = strQuery + " HAVING FREQUENCY >= '" + threshold + "'";
                    } else {
                        strQuery = strQuery + " HAVING FREQUENCY < '" + threshold + "'";
                    }
                }
                if (sortingField == null || sortingField.trim().length() == 0) {
                    strQuery = strQuery + " ORDER BY FREQUENCY ";
                } else {
                    strQuery = strQuery + " ORDER BY " + replaceStrings(sortingField.toUpperCase()).replace(".", "_");
                }

                if (increasingOrder == false) {
                    strQuery = strQuery + " DESC ";
                }
                if (topNPatternMore == false) {
                    strQuery = strQuery + " LIMIT " + topNPattern + " OFFSET 0 ";
                    
                }
                stmt.executeUpdate(strQuery);
                // stmt.executeBatch();
                conn.commit();

                writeReportFile(reportFilePath + "/TMP_PROFILE.csv", reportFilePath + "/" + repType + "_" + tblName + "_" + this.recordCountStamp + ".csv");
                stmt = null;
                stmt = conn.createStatement();
                stmt.executeUpdate("TRUNCATE TABLE TMP_PROFILE");
                stmt.executeUpdate("DROP TABLE TMP_PROFILE");
                conn.commit();


            } catch (SQLException ex) {
                throw new DBHandlerException(ex);
            }

        }
        return fldStr;
    }

    public void cleanTemps() throws DBHandlerException {
        try {
            File f = new File(reportFilePath);
            File[] fls;
            File[] flsIn;
            String fName;
            fls = f.listFiles();
            // Statement stmt = conn.createStatement();
            //stmt.executeUpdate("DROP TABLE TMP_PROFILE");
            // conn.commit();
            conn.close();
            for (int i = 0; i < fls.length; i++) {
                if (fls[i] != null) {
                    fName = fls[i].getName().trim();
                    if (fName.equalsIgnoreCase("TMP_PROFILE.csv") || fName.equalsIgnoreCase("TEMPDB.VER") || fName.equalsIgnoreCase("TMP_PROFILE")) {
                        if (fls[i].isDirectory()) {
                            flsIn = fls[i].listFiles();
                            for (int j = 0; j < flsIn.length; j++) {
                                flsIn[j].delete();
                            }
                            fls[i].delete();
                        } else {
                            fls[i].delete();
                        }
                    }
                }
            }
        } catch (SQLException ex) {
        //throw new DBHandlerException(ex);
        }

    }

    public void writeReportSimpleFRQ(String repFileName, String tblName, HashMap<String, String> fldMap, int threshold, boolean threshHoldMore, boolean increasingOrder, String sortingField) throws DBHandlerException {

        try {
            if (prepStmntSimple != null) {
                prepStmntSimple.close();
            }

            generateReport(repFileName, tblName, fldMap, threshold, threshHoldMore, increasingOrder, sortingField);
            Statement stmt = conn.createStatement();
            stmt.executeUpdate("DROP TABLE " + tblName);
            conn.commit();
            stmt.close();
        } catch (SQLException ex) {
            throw new DBHandlerException(ex);
        }

    }

    public void writeReportConstrainedFRQ(String repFileName, String tblName, HashMap<String, String> fldMap, int threshold, boolean threshHoldMore, boolean increasingOrder, String sortingField) throws DBHandlerException {
        try {
            if (prepStmntConstrained != null) {
                prepStmntConstrained.close();
            }

            generateReport(repFileName, tblName, fldMap, threshold, threshHoldMore, increasingOrder, sortingField);
            Statement stmt = conn.createStatement();
            stmt.executeUpdate("DROP TABLE " + tblName);
            conn.commit();
            stmt.close();

        } catch (SQLException ex) {
            throw new DBHandlerException(ex);
        }


    }

    public void writeReportPatternFRQ(String repFileName, String tblName, HashMap<String, String> fldMap, int threshold, boolean threshHoldMore, boolean increasingOrder, String sortingField) throws DBHandlerException {

        try {
            if (prepStmntPattern != null) {
                prepStmntPattern.close();
            }

            generateReport(repFileName, tblName, fldMap, threshold, threshHoldMore, increasingOrder, sortingField);
            Statement stmt = conn.createStatement();
            stmt.executeUpdate("DROP TABLE " + tblName);
            conn.commit();
            stmt.close();

        } catch (SQLException ex) {
            throw new DBHandlerException(ex);
        }


    }
    public void writeReportPatternFRQ(String repFileName, String tblName, HashMap<String, String> fldMap, int threshold, boolean threshHoldMore, boolean increasingOrder, String sortingField,int topNPattern, boolean topNPatternMore) throws DBHandlerException {

        try {
            if (prepStmntPattern != null) {
                prepStmntPattern.close();
            }

            generateReport(repFileName, tblName, fldMap, threshold, threshHoldMore, increasingOrder, sortingField,topNPattern,topNPatternMore);
            Statement stmt = conn.createStatement();
            stmt.executeUpdate("DROP TABLE " + tblName);
            conn.commit();
            stmt.close();

        } catch (SQLException ex) {
            throw new DBHandlerException(ex);
        }


    }

    private void writeReportFile(String fromFile, String toFile) throws DBHandlerException {
        try {
            // Create channel on the source
            //File fromFileObj = new File(fromFile);

            FileChannel fromChannel = new FileInputStream(fromFile).getChannel();

            // Create channel on the destination
            FileChannel toChannel = new FileOutputStream(toFile).getChannel();

            // Copy file contents from source to destination

            toChannel.transferFrom(fromChannel, 0, fromChannel.size());

            // Close the channels

            fromChannel.close();
            toChannel.close();


        } catch (IOException ex) {
            throw new DBHandlerException(ex);
        }
    }

    public void setRecordStamp() {
        recordCountStamp = String.valueOf(startFrom) + "-" + String.valueOf(startFrom + profileSize - 1);
    }
}
