/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html
 * or mural/license.txt. See the License for the specific language 
 * governing permissions and limitations under the License.  
 *
 * When distributing Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at mural/license.txt.
 * If applicable, add the following below the CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.dm.dq.rules.output;

import com.sun.dm.dq.util.Localizer;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class RuleErrorObject {

    private String ruleStepName;
    private String ruleValue;
    private String passedValue;
    private String failReason;
    private String ePath;
    private transient final Localizer mLocalizer = Localizer.get();
    private boolean sysError;

    /**
     * Creates a new instance of RuleErrorObject
     *
     */
    public RuleErrorObject(String ePath, String ruleStepName, String ruleValue, String passedValue, String failReason, boolean sysError) {
        this.ruleStepName = ruleStepName;
        this.ruleValue = ruleValue;
        this.passedValue = passedValue;
        this.sysError = sysError;
        this.ePath = ePath;
        this.failReason = failReason;

    }

    public String getEPath() {
        return ePath;
    }

    public String getRuleStepName() {
        return ruleStepName;
    }

    public String getRuleValue() {
        return ruleValue;
    }

    public String getPassedValue() {
        return passedValue;
    }

    public String getFailReason() {
        if (this.sysError == false) {
            return mLocalizer.t(failReason);
        }
        return failReason;
    }

    public boolean isSysError() {
        return sysError;
    }
}
