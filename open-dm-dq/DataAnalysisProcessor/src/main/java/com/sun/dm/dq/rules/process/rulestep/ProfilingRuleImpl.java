/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html
 * or mural/license.txt. See the License for the specific language 
 * governing permissions and limitations under the License.  
 *
 * When distributing Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at mural/license.txt.
 * If applicable, add the following below the CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */

package com.sun.dm.dq.rules.process.rulestep;

import com.sun.dm.dq.rules.DataLengthRule;
import com.sun.dm.dq.rules.DateRangeRule;
import com.sun.dm.dq.rules.PatternMatchRule;
import com.sun.dm.dq.rules.RaiseRule;
import com.sun.dm.dq.rules.RangeRule;
import com.sun.dm.dq.rules.ReturnRule;
import com.sun.dm.dq.rules.data.DataObjectHandler;
import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import com.sun.dm.dq.rules.output.RuleErrorObject;
import com.sun.dm.dq.rules.process.ruleprocessor.IFProcess;
import com.sun.dm.dq.schema.CleansingRuleType;
import com.sun.dm.dq.schema.IfStepType;
import com.sun.dm.dq.schema.ProfilingRuleType;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class ProfilingRuleImpl implements Rule {
    
    CleansingRuleType.RuleList.Rule ruleStepXmlObj;
    List<Object> ruleXmlList ;
    List<Object> ruleStepList ;
    /**
     * Creates a new instance of CleansingRuleImpl
     */
    public ProfilingRuleImpl(ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule objRuleSteps) throws ProcessXMLDataException {
        processXMLObject(objRuleSteps);
        
    }
    public ProfilingRuleImpl(ArrayList<Object> objList) throws ProcessXMLDataException {
        processXMLObject(objList);
    }
    
    public ArrayList <RuleErrorObject> execute(DataObjectHandler dataObjHandler) {
        ListIterator rlListItr = ruleStepList.listIterator();
        ArrayList <RuleErrorObject> errObj = new ArrayList <RuleErrorObject> ();
        RuleErrorObject rErr;
        while (rlListItr.hasNext()){
            RuleStep varRuleStep = (RuleStep) rlListItr.next();
            if (varRuleStep != null){
               rErr = varRuleStep.execute(dataObjHandler);
               if (rErr != null) {
                  errObj.add(rErr); 
               }
            }
        }
        return errObj;
    }
    
   
    
    public void executeRuleStep() {
    }
    
    /*
     *          @XmlElement(name = "truncate", type = CleansingRuleType.RuleList.Rule.Truncate.class),
                @XmlElement(name = "patternReplace", type = CleansingRuleType.RuleList.Rule.PatternReplace.class),
                @XmlElement(name = "matchFromFile", type = CleansingRuleType.RuleList.Rule.MatchFromFile.class),
                @XmlElement(name = "reject", type = CleansingRuleType.RuleList.Rule.Reject.class),
                @XmlElement(name = "validateDBField", type = CleansingRuleType.RuleList.Rule.ValidateDBField.class),
                @XmlElement(name = "replace", type = CleansingRuleType.RuleList.Rule.Replace.class),
                @XmlElement(name = "assign", type = CleansingRuleType.RuleList.Rule.Assign.class),
                @XmlElement(name = "raise", type = com.sun.schema.di.ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.Raise.class),
                @XmlElement(name = "range", type = com.sun.schema.di.ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.Range.class),
                @XmlElement(name = "if", type = CleansingRuleType.RuleList.Rule.If.class),
                @XmlElement(name = "patternMatch", type = com.sun.schema.di.ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.PatternMatch.class),
                @XmlElement(name = "executeJava", type = CleansingRuleType.RuleList.Rule.ExecuteJava.class),
                @XmlElement(name = "return", type = com.sun.schema.di.ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.Return.class)
     */
    private void processXMLObject(ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule objRuleSteps) throws ProcessXMLDataException {
        
        processXMLObject((ArrayList<Object> )objRuleSteps.getProfilerRuleStepGroup());
        
    }
    /*
     *@XmlElement(name = "range", type = ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.Range.class),
                    @XmlElement(name = "return", type = ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.Return.class),
                    @XmlElement(name = "patternMatch", type = ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.PatternMatch.class),
                    @XmlElement(name = "if", type = ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.If.class),
                    @XmlElement(name = "raise", type = ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.Raise.class)
    */
    private void processXMLObject(ArrayList<Object> objRuleSteps) throws ProcessXMLDataException {
        
        
        ruleXmlList = new ArrayList<Object>();
        ruleStepList = new ArrayList<Object>();
        ruleXmlList = objRuleSteps;
        
        ListIterator rlListItr = ruleXmlList.listIterator();
        while (rlListItr.hasNext()){
            Object var =  rlListItr.next();
            if (var.getClass() == ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.If.class ){
                ruleStepList.add(new IFProcess((ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.If) var)) ;
                
            } else if(var.getClass() == IfStepType.class )  {
                ruleStepList.add(new IFProcess((IfStepType) var)) ;
                
            }  else if(var.getClass() == ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.DataLength.class )  {
                ruleStepList.add(new DataLengthRule((ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.DataLength) var)) ;
                
            } else if(var.getClass() == ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.PatternMatch.class )  {
                ruleStepList.add(new PatternMatchRule((ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.PatternMatch) var)) ;
                
            } else if(var.getClass() == ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.Range.class )  {
                ruleStepList.add(new RangeRule((ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.Range) var)) ;
                
            } else if(var.getClass() == ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.Raise.class )  {
                ruleStepList.add(new RaiseRule((ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.Raise) var)) ;
                
            } else if(var.getClass() == ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.Return.class )  {
                ruleStepList.add(new ReturnRule((ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.Return) var)) ;
                
            } 
            else if(var.getClass() == ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.DateRange.class )  {
                ruleStepList.add(new DateRangeRule((ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.DateRange) var)) ;
                
            } 
        }
        
    }
    
}
