/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html
 * or mural/license.txt. See the License for the specific language 
 * governing permissions and limitations under the License.  
 *
 * When distributing Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at mural/license.txt.
 * If applicable, add the following below the CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.dm.dq.rules;

import com.sun.dm.dq.rules.data.DataObjectHandler;
import com.sun.dm.dq.rules.exception.DataObjectHandlerException;
import com.sun.dm.dq.rules.exception.ProcessXMLDataException;
import com.sun.dm.dq.rules.output.RuleErrorObject;
import com.sun.dm.dq.rules.process.rulestep.RuleStep;
import com.sun.dm.dq.rules.variablelist.VarListProcess;
import com.sun.dm.dq.schema.ProfilingRuleType;

/**
 *
 * @author abhijeet.gupta@sun.com
 */
public class RangeRule extends RuleStep {

    private int max;
    private int min;
    private String ePath;

    public RangeRule(ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.Range rangeXmlObj) throws ProcessXMLDataException {
        processXMLObject(rangeXmlObj);
    }

    public RuleErrorObject execute(DataObjectHandler dataObjHandler) {

        int val = 0;
        String value = "";
        try {
            value = dataObjHandler.getFieldValue(ePath);
            val = Integer.valueOf(value);

            if (val > max) {
                return new RuleErrorObject("EPath :: " + ePath, " Range",
                        val + "", max + "", "ERR010: The value is more than the maximum limit", false);
            } else if (val < min) {
                return new RuleErrorObject("EPath :: " + ePath, " Range",
                        val + "", min + "", "ERR011: The value is less than the minimum limit", false);
            } else if (val > min && val < max) {
                return null;
            } else {
                return new RuleErrorObject("EPath :: " + ePath, " Range",
                        val + "", min + "", "ERR012: The value does not fall in the range ", false);
            }

        } catch (DataObjectHandlerException ex) {
            return new RuleErrorObject("EPath :: " + ePath, " Range",
                    val + "", min + "", ex.getLocalizedMessage(), true);
        }
    }

    private void processXMLObject(ProfilingRuleType.ConstrainedFrequencyAnalysis.RuleList.Rule.Range rangeXmlObj)
            throws ProcessXMLDataException {

        min = rangeXmlObj.getMin();
        ePath = VarListProcess.getVarValue(rangeXmlObj.getFieldName());
        max = rangeXmlObj.getMax();

    }
}
