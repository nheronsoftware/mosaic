//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.1.4-b02-fcs 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2007.11.22 at 01:35:53 PM IST 
//


package com.sun.dm.dq.schema;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for dbFieldBlockCallerType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="dbFieldBlockCallerType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="fieldList" type="{http://www.sun.com/schema/DI}fieldListType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "dbFieldBlockCallerType", propOrder = {
    "fieldList"
})
public class DbFieldBlockCallerType {

    @XmlElement(required = true)
    protected FieldListType fieldList;

    /**
     * Gets the value of the fieldList property.
     * 
     * @return
     *     possible object is
     *     {@link FieldListType }
     *     
     */
    public FieldListType getFieldList() {
        return fieldList;
    }

    /**
     * Sets the value of the fieldList property.
     * 
     * @param value
     *     allowed object is
     *     {@link FieldListType }
     *     
     */
    public void setFieldList(FieldListType value) {
        this.fieldList = value;
    }

}
