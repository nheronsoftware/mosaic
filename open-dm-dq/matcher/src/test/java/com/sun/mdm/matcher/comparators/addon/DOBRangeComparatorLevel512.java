/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.matcher.comparators.addon;

import java.util.Map;
import java.util.StringTokenizer;
import java.util.HashMap;

import com.sun.mdm.matcher.comparators.MatchComparator;
import com.sun.mdm.matcher.comparators.MatchComparatorException;

/**
 * This comparator compares the DOB years/months, and returns a total agreement 
 * if the DOB are separated by less than "firstThreshold" months, then, 
 * the agreement decreases linearly to reach a dynamic threshold. Then it becomes
 * total disagreement when the difference is equal or greater than "secondThreshold" months.
 * It uses the DataComparator as a dependency class to perform the date calculation.
 * Here, we add a data source access capability. 
 * 
 * This class comparator provides the fifth-level functionality of the match 
 * comparators plugin framework. It reads two strings, then performs a validation
 * step and compare the strings using external static parameters from the 
 * match configuration file, a dynamic variable parameter (real-time), rely
 * on another existing comparator to perform the date distance measure.
 * Finally, it access a data source to read related data.
 * 
 * @author souaguenouni
 */
public class DOBRangeComparatorLevel512 implements MatchComparator {

    // The variable that holds all the static parameters
    private Map<String, Map> params;   
    // The variable that holds all the additional real-time parameters
    private Map<String, String> argumentsRT = new HashMap(); 
    // Dependency class that performs the date-distance measure
    private Map<String, Map> dependClassList; 
    // The variable that holds data sources info
    private Map<String, Map> dataSources;       
    
    /**
     * Initialize the parameters and data sources info.
     * @param  params  provides all the parameters associated with a given match field using this match comparator
     * @param  dataSources  provides all the data sources info. associated with a given match field using this match comparator
     * @param  dependClassList  provides the list of all the dependency classes
     */
    public void initialize(Map<String, Map> params, Map<String, Map> dataSources, Map<String, Map> dependClassList) {
        this.params = params;    
        this.dependClassList = dependClassList;      
        this.dataSources = dataSources;
    }

    /**
     * A setter for real-time passed-in parameters
     *
     * @param  key  the key for use in a Map
     * @param  value the corresponding value for use in a Map
     */
    public void setRTParameters(String key, String value) {    
        this.argumentsRT.put(key, value);        
    }
    
     /**
     * Reads two strings and measure how close they are relying on an algorithm
     * that compare the proximity of the two strings (zero being very different and
     * one being identical)
     *
     * @param  recordA  Candidate's string record.
     * @param  recordB  Reference's string record.
     * @param context 
     * @return  a real number between zero and one that measures the degree of similarity.
     */
    public double compareFields(String recordA, String recordB, Map context) 
       throws MatchComparatorException {

        // The list of parameters
        Map<String, String> theParams = null;
        // Get the Date comparator instance
//        MatchComparator dateInstance = (MatchComparator) dependClassList.get("DC"); 
        // Set the instance parameters for the date comparator
//        dateInstance.setRTParameters("matchfield", "DC");        
        
        if (params != null) {
            theParams = params.get(context.get("fieldName"));
        }        
        
        // Test if the date format is valid (mm/dd/yyyy)
        validateDOB(recordA, recordB);   

        // Compare only year
//        double compare = dateInstance.compareFields(recordA, recordB, context);         
  
        // Read the associated static parameters (lower threshold and upper
        // threshold)
        int firstT = Integer.parseInt(theParams.get("firstThreshold"));
        int secondT = Integer.parseInt(theParams.get("secondThreshold"));
        int varT;
        // Look for third parameter (real-time)        
        if (argumentsRT.containsKey("variableThreshold")) {
            varT = Integer.parseInt(argumentsRT.get("variableThreshold"));
        } else {
            varT = secondT;
        }
        
        // Calculate the month-diff
        double deltaC = 0;//(secondT - firstT)*compare;
        // 
        if (deltaC < firstT) {
            return 1;
        } else if (deltaC < varT) {
            return (secondT - deltaC) / (secondT - firstT);
        } else if (deltaC < secondT) {
            return (secondT - varT) / (secondT - firstT);
        } else {
            return 0;
        }
    } 
    
    private void validateDOB(String recordA, String recordB) 
        throws MatchComparatorException {
        
        StringTokenizer stA = new StringTokenizer(recordA, "/");
        StringTokenizer stB = new StringTokenizer(recordB, "/");
        
        // Test le length
        if (stA.countTokens() != 3 || stB.countTokens() != 3) {
            throw new MatchComparatorException("The format should be mm/dd/yyyy");
        }
        
        while (stA.hasMoreElements() && stB.hasMoreElements()) {
            try {
                Integer.parseInt(stA.nextToken());
                Integer.parseInt(stB.nextToken());
            } catch (NumberFormatException ex) {
                throw new MatchComparatorException("The date should be all integers: mm dd yyyy");
            }
            
        }
    }
    
    /**
     * Close any related data sources streams
     */
    public void stop() {
        argumentsRT.clear();        
    }
    
}
