/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.matcher.comparators.base;

import com.sun.mdm.matcher.comparators.MatchComparatorException;
import java.io.IOException;
import java.text.ParseException;
import com.sun.mdm.matcher.api.*;
import com.sun.mdm.matcher.api.impl.MatchConfigFilesAccessImpl;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * JaroAdvancedStringComparatorTest unit testing
 * @author  sofiane Ouaguenouni
 * @version $Revision: 1.3 $
 */
public class JaroAdvancedStringComparatorTest extends TestCase {

    static double[][] resultWeightNew;
    static double[][] resultWeightNew2;
    static double[][] resultWeightNew3;
    static double[][] resultWeightNew4;
    static final double DELTA = 0.0001;
    
    static String[] matchFieldsIDs = {"StreetDir","StreetDir","StreetDir",     // u
                                       "StreetDir","StreetDir","StreetDir"};   
    static String[] matchFieldsIDs2 = {"FirstName","FirstName","FirstName",     // uf
                                       "FirstName","FirstName","FirstName"};   
    static String[] matchFieldsIDs3 = {"LastName","LastName","LastName",     // ul
                                       "LastName","LastName","LastName"};      
    static String[] matchFieldsIDs4 = {"HouseNumber","HouseNumber","HouseNumber",     // un
                                       "HouseNumber","HouseNumber","HouseNumber"};  
    
    static String[][] candRecArrayVals = {{"transforma", "transforma", "transforma", 
                                           "transforma", "transforma", "transforma"},
                                          {"Johansonn", "Johansonn", "Johansonn", 
                                           "Johansonn", "Johansonn", "Johansonn", "", null}};
    static String[][] refRecArrayVals = {{"trasnfma", "trinsforma", "trams", 
                                           "ransf", "transform", "tra"},
                                           {"Johanso", "Johasnonn", "Joahnsonn", 
                                           "Jhonasonn", "ansonn", "sonn", "", null}};        
     static String[][] candRecArrayVals2 = {{"Alberto", "Alberto", "Alberto", 
                                           "Alberto", "Alberto", "Alberto"},
                                          {"Johansonn", "Johansonn", "Johansonn", 
                                           "Johansonn", "Johansonn", "Johansonn", "", null}};
    static String[][] refRecArrayVals2 = {{"Alberto", "Alberta", "Ablerto", 
                                           "Abberto", "berto", "erto"},
                                           {"Johanso", "Johasnonn", "Joahnsonn", 
                                           "Jhonasonn", "ansonn", "sonn", "", null}};        
     static String[][] candRecArrayVals3 = {{"Smithsonian", "Smithsonian", "Smithsonian", 
                                           "Smithsonian", "Smithsonian", "Smithsonian"},
                                          {"Ouaguenouni", "Ouaguenouni", "Ouaguenouni", 
                                           "Ouaguenouni", "Ouaguenouni", "Ouaguenouni", "", null}};
    static String[][] refRecArrayVals3 = {{"Smithsoni", "Smithsoan", "Smihtsonian", 
                                           "Sthsonian", "Simtonian", "Simhysonian"},
                                           {"Ouaguenoun", "Ouaguenou", "Quagenouni", 
                                           "Ouagenoni", "Waguenoni", "Ouagenun", "", null}};           
    static String[][] candRecArrayVals4 = {{"3322", "3322", "3322", 
                                           "3322", "3322", "3322", "", null}};
    static String[][] refRecArrayVals4 = {{"3322", "2322", "3324", 
                                           "32217", "2363", "3340", "", null}};       
    
    private static double extectedWeight1 = -11.949999999999989;    
    private static double extectedWeight2 = -7.105427357601002E-15;
    private static double extectedWeight21 = 34.28571428571429;
    private static double extectedWeight22 = 33.33333333333333;
    private static double extectedWeight31 = 23.555555555555564;
    private static double extectedWeight32 = 13.94545454545458;
    private static double extectedWeight4 = -25.000000000000004;


    static int numberOfCandRec;
    static int numberOfRefRec;
    static int fieldsLen;
     
    static {
        resultWeightNew = new double[2][2];        
        resultWeightNew2 = new double[2][2];
        resultWeightNew3 = new double[2][2];
        resultWeightNew4 = new double[1][1];
        int i;
        int j;
    }    

    
    /** 
     * Creates new Bigram
     * @see junit.framework.TestCase
     */
    public JaroAdvancedStringComparatorTest(String name) {
        super(name);        
    }

    /** 
     * Set up the unit test
     * @see junit.framework.TestCase
     */    
    protected void setUp() {
        
    }

    /** 
     * Tear down the unit test
     * @see junit.framework.TestCase
     */
    protected void tearDown() {
        // cleanup code
    }

    /**
     * Test the Bigram class methods
     * 
     */
    public void testJaroAdvancedStringComparator() 
        throws MatchComparatorException, IOException,
               MatcherException, ParseException, InstantiationException {
        
        int i;
        int j;
        int k;
        String path = "match/";

        MatchConfigFilesAccess filesAccess = new MatchConfigFilesAccessImpl(path); 

        // The new engine
        MatchingEngine newME = new MatchingEngine(filesAccess);
        newME.upLoadConfigFile();      

	resultWeightNew = newME.matchWeight(matchFieldsIDs, candRecArrayVals, refRecArrayVals);        
        resultWeightNew2 = newME.matchWeight(matchFieldsIDs2, candRecArrayVals2, refRecArrayVals2);
        resultWeightNew3 = newME.matchWeight(matchFieldsIDs3, candRecArrayVals3, refRecArrayVals3);
        resultWeightNew4 = newME.matchWeight(matchFieldsIDs4, candRecArrayVals4, refRecArrayVals4);
                		
        assertEquals((float) resultWeightNew[0][0], (float) extectedWeight1, (float) DELTA);
        assertEquals((float) resultWeightNew[1][1], (float) extectedWeight2, (float) DELTA);
        assertEquals((float) resultWeightNew2[0][0], (float) extectedWeight21, (float) DELTA);
        assertEquals((float) resultWeightNew2[1][1], (float) extectedWeight22, (float) DELTA); 
        assertEquals((float) resultWeightNew3[0][0], (float) extectedWeight31, (float) DELTA);
        assertEquals((float) resultWeightNew3[1][1], (float) extectedWeight32, (float) DELTA); 
        assertEquals((float) resultWeightNew4[0][0], (float) extectedWeight4, (float) DELTA);   
    }
    
    /**
     * Main method needed to make a self runnable class
     * @param args The command line arguments
     */
    public static void main(String[] args) {
        junit.textui.TestRunner.run(new TestSuite(JaroAdvancedStringComparatorTest.class));
    }   
}
