/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.matcher.comparators.base;

import com.sun.mdm.matcher.comparators.MatchComparatorException;
import java.io.IOException;
import java.text.ParseException;
import com.sun.mdm.matcher.api.*;
import com.sun.mdm.matcher.api.impl.MatchConfigFilesAccessImpl;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * UnicodeStringComparator unit testing
 * @author  sofiane Ouaguenouni
 * @version $Revision: 1.3 $
 */
public class UnicodeStringComparatorTest extends TestCase {

    static double[][] resultWeightNew;
    static double[][] resultWeightOld;
    static final double DELTA = 0.0001;
    
    static String[] matchFieldsIDs = {"UniString","UniString","UniString",     // usu - de
                                   "UniString","UniString","UniString"};      

    static String[][] candRecArrayVals = {{"transforma", "transforma", "transforma", 
                                           "transforma", "transforma", "transforma", "", null}};
    static String[][] refRecArrayVals = {{"trasnfma", "trinsforma", "trams", 
                                           "ransf", "transform", "tra", "", null}};       
    
    private static double extectedWeight = -18.0;    

    
    static {
        resultWeightNew = new double[1][1];        

        int i;
        int j;
    }    

    
    /** 
     * Creates new Bigram
     * @see junit.framework.TestCase
     */
    public UnicodeStringComparatorTest(String name) {
        super(name);        
    }

    /** 
     * Set up the unit test
     * @see junit.framework.TestCase
     */    
    protected void setUp() {
        
    }

    /** 
     * Tear down the unit test
     * @see junit.framework.TestCase
     */
    protected void tearDown() {
        // cleanup code
    }

    /**
     * Test the Bigram class methods
     * 
     */
    public void testUnicodeStringComparator() 
        throws MatchComparatorException, IOException,
               MatcherException, ParseException, InstantiationException {
        
        int i;
        int j;
        int k;
        String path = "match/";

        MatchConfigFilesAccess filesAccess = new MatchConfigFilesAccessImpl(path); 

        // The new engine
        MatchingEngine newME = new MatchingEngine(filesAccess);
        newME.upLoadConfigFile();      
	resultWeightNew = newME.matchWeight(matchFieldsIDs, candRecArrayVals, refRecArrayVals);      
                		
        assertEquals((float) resultWeightNew[0][0], (float) extectedWeight, (float) DELTA);    
    }
    
    /**
     * Main method needed to make a self runnable class
     * @param args The command line arguments
     */
    public static void main(String[] args) {
        junit.textui.TestRunner.run(new TestSuite(UnicodeStringComparatorTest.class));
    }   
}
