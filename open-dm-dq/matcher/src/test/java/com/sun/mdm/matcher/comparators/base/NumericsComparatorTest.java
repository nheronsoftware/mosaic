/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.matcher.comparators.base;

import com.sun.mdm.matcher.comparators.MatchComparatorException;
import java.io.IOException;
import java.text.ParseException;
import com.sun.mdm.matcher.api.*;
import com.sun.mdm.matcher.api.impl.MatchConfigFilesAccessImpl;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * NumericsComparator unit testing
 * @author  sofiane Ouaguenouni
 * @version $Revision: 1.3 $
 */
public class NumericsComparatorTest extends TestCase {

    static double[][] resultWeight;
    static double[][] resultWeight2;
    static double[][] resultWeight3;
    static double[][] resultWeight4;
    static final double DELTA = 0.0001;
    
    static String[] matchFieldsIDs = {"Numeric","Numeric","Numeric",     // n - y 8
                                       "Numeric","Numeric","Numeric"};   
    static String[] matchFieldsIDs2 = {"Real","Real","Real",     // nR - n
                                       "Real","Real","Real"};  
    static String[] matchFieldsIDs3 = {"Integer","Integer","Integer",     // nI - n
                                       "Integer","Integer","Integer"};      
    static String[] matchFieldsIDs4 = {"Integer2","Integer2","Integer2",     // nI - y 6
                                       "Integer2","Integer2","Integer2"};            
      
    static String[][] candRecArrayVals = {{"333", "333", "333", 
                                           "333", "333", "333", "", null}};
    static String[][] refRecArrayVals = {{"328.66", "359.6", "337.9", 
                                           "324", "344.44", "340", "", null}};  
    
    static String[][] candRecArrayVals2 = {{"33", "33", "33", 
                                           "33", "33", "33", "", null}};
    static String[][] refRecArrayVals2 = {{"33", "35", "30", 
                                           "24", "44", "40", "", null}};     
         
    
    private static double expectedWeight = -34.18947368421043;
    private static double expectedWeight2 = -13.000000000000002;   
    private static double expectedWeight3 = 5.0;   
    private static double expectedWeight4 = -14.285714285714286;   


    
    static {
        resultWeight = new double[1][1];        
        resultWeight2 = new double[1][1];   
        resultWeight3 = new double[1][1];   
        resultWeight4 = new double[1][1];   
        int i;
        int j;
    }    

    
    /** 
     * Creates new Bigram
     * @see junit.framework.TestCase
     */
    public NumericsComparatorTest(String name) {
        super(name);        
    }

    /** 
     * Set up the unit test
     * @see junit.framework.TestCase
     */    
    protected void setUp() {
        
    }

    /** 
     * Tear down the unit test
     * @see junit.framework.TestCase
     */
    protected void tearDown() {
        // cleanup code
    }

    /**
     * Test the Bigram class methods
     * 
     */
    public void testNumericsComparator() 
        throws MatchComparatorException, IOException,
                MatcherException, ParseException, InstantiationException {
        
        int i;
        int j;
        int k;
        String path = "match/";

        MatchConfigFilesAccess filesAccess = new MatchConfigFilesAccessImpl(path); 

        // The new engine
        MatchingEngine newME = new MatchingEngine(filesAccess);
        newME.upLoadConfigFile();   
        
	resultWeight = newME.matchWeight(matchFieldsIDs, candRecArrayVals, refRecArrayVals);           
        resultWeight2 = newME.matchWeight(matchFieldsIDs2, candRecArrayVals, refRecArrayVals);    
        resultWeight3 = newME.matchWeight(matchFieldsIDs3, candRecArrayVals2, refRecArrayVals2);    
        resultWeight4 = newME.matchWeight(matchFieldsIDs4, candRecArrayVals2, refRecArrayVals2);
        
        assertEquals((float) resultWeight[0][0], (float) expectedWeight, (float) DELTA);  
        assertEquals((float) resultWeight2[0][0], (float) expectedWeight2, (float) DELTA); 
        assertEquals((float) resultWeight3[0][0], (float) expectedWeight3, (float) DELTA); 
        assertEquals((float) resultWeight4[0][0], (float) expectedWeight4, (float) DELTA); 
    }
    
    /**
     * Main method needed to make a self runnable class
     * @param args The command line arguments
     */
    public static void main(String[] args) {
        junit.textui.TestRunner.run(new TestSuite(NumericsComparatorTest.class));
    }   
}