/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.matcher.comparators.base;

import com.sun.mdm.matcher.comparators.MatchComparatorException;
import java.io.IOException;
import java.text.ParseException;
import com.sun.mdm.matcher.api.*;
import com.sun.mdm.matcher.api.impl.MatchConfigFilesAccessImpl;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * UnicodeSSNComparator unit testing
 * @author  sofiane Ouaguenouni
 * @version $Revision: 1.3 $
 */
public class CondensedSSNComparatorTest extends TestCase {

    static double[][] resultWeight;
    static double[][] resultWeight2;
    static double[][] resultWeight3;
    static double[][] resultWeight4;
    static final double DELTA = 0.0001;
    
    static String[] matchFieldsIDs = {"SSN","SSN","SSN",     // nS  - an
                                       "SSN","SSN","SSN"};          
    static String[] matchFieldsIDs2 = {"SSN2","SSN2","SSN2",     // nS  - 7 an
                                       "SSN2","SSN2","SSN2"};         
    static String[] matchFieldsIDs3 = {"SSN3","SSN3","SSN3",     // nS  - 9 nu 1 3 6
                                       "SSN3","SSN3","SSN3"};    
    static String[] matchFieldsIDs4 = {"SSN4","SSN4","SSN4",     // nS  - an c d 0
                                       "SSN4","SSN4","SSN4"};         
      
    static String[][] candRecArrayVals = {{"T27564564R", "T27564564R", "T27564564R", 
                                           "T27564564R", "T27564564R", "T27564564R", "", null}};
    static String[][] refRecArrayVals = {{"T27564564R", "T2754654R", "T2754564", 
                                           "546564", "7546564", "64564", "", null}};
    
    static String[][] candRecArrayVals2 = {{"u7115t6", "u7115t6", "u7115t6", 
                                           "u7115t6", "u7115t6", "u7115t6", "", null}};
    static String[][] refRecArrayVals2 = {{"u7115t6", "u7151t6", "u7115te", 
                                           "u71t63d", "7u115t6", "u11763", "", null}};        
 
    static String[][] candRecArrayVals3 = {{"427115663", "427115663", "427115663", 
                                           "427115663", "427115663", "427115663", "", null}};
    static String[][] refRecArrayVals3 = {{"427115663", "413116563", "4175666", 
                                           "32457565", "221761167", "2255466", "", null}};    
         
    
    private static double expectedWeight = -6.333333333333334;   
    private static double expectedWeight2 = 19.523809523809522;    
    private static double expectedWeight3 = -18.14814814814815;   
    private static double expectedWeight4 = 5.185185185185185;   


    
    static {
        resultWeight = new double[1][1];        
        resultWeight2 = new double[1][1];   
        resultWeight3 = new double[1][1];   
        resultWeight4 = new double[1][1];   
        int i;
        int j;
    }    

    
    /** 
     * Creates new Bigram
     * @see junit.framework.TestCase
     */
    public CondensedSSNComparatorTest(String name) {
        super(name);        
    }

    /** 
     * Set up the unit test
     * @see junit.framework.TestCase
     */    
    protected void setUp() {
        
    }

    /** 
     * Tear down the unit test
     * @see junit.framework.TestCase
     */
    protected void tearDown() {
        // cleanup code
    }

    /**
     * Test the Bigram class methods
     * 
     */
    public void testCondensedSSNComparator() 
        throws MatchComparatorException, IOException,
               MatcherException, ParseException, InstantiationException {
        
        int i;
        int j;
        int k;
        String path = "match/";

        MatchConfigFilesAccess filesAccess = new MatchConfigFilesAccessImpl(path); 

        // The new engine
        MatchingEngine newME = new MatchingEngine(filesAccess);
        newME.upLoadConfigFile();      
	resultWeight = newME.matchWeight(matchFieldsIDs, candRecArrayVals, refRecArrayVals);      
        resultWeight2 = newME.matchWeight(matchFieldsIDs2, candRecArrayVals2, refRecArrayVals2);    
        resultWeight3 = newME.matchWeight(matchFieldsIDs3, candRecArrayVals3, refRecArrayVals3);    
        resultWeight4 = newME.matchWeight(matchFieldsIDs4, candRecArrayVals3, refRecArrayVals3);
        
        assertEquals((float) resultWeight[0][0], (float) expectedWeight, (float) DELTA);  
        assertEquals((float) resultWeight2[0][0], (float) expectedWeight2, (float) DELTA); 
        assertEquals((float) resultWeight3[0][0], (float) expectedWeight3, (float) DELTA); 
        assertEquals((float) resultWeight4[0][0], (float) expectedWeight4, (float) DELTA); 
    }
    
    /**
     * Main method needed to make a self runnable class
     * @param args The command line arguments
     */
    public static void main(String[] args) {
        junit.textui.TestRunner.run(new TestSuite(CondensedSSNComparatorTest.class));
    }   
}
