/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.matcher.comparators.base;

import com.sun.mdm.matcher.comparators.MatchComparator;
import com.sun.mdm.matcher.comparators.MatchComparatorException;
import java.io.IOException;
import java.text.ParseException;
import com.sun.mdm.matcher.api.*;
import com.sun.mdm.matcher.api.impl.MatchConfigFilesAccessImpl;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * DateComparatorTest unit testing
 * @author  sofiane Ouaguenouni
 * @version $Revision: 1.3 $
 */
public class DateComparatorTest extends TestCase {

    static double[][] resultWeightYears;
    static double[][] resultWeightMonths;
    static double[][] resultWeightDays;
    static double[][] resultWeightHours;
    static double[][] resultWeightMinutes;
    static double[][] resultWeightSeconds;
    static final double DELTA = 0.0001;
    
    static String[] matchFieldsIDs = {"DateYear","DateYear","DateYear",     // dY - y 9 18
                                      "DateYear","DateYear","DateYear"};  
    static String[] matchFieldsIDs2 = {"DateMonths","DateMonths","DateMonths",     // dM - n
                                       "DateMonths","DateMonths","DateMonths"};      
    static String[] matchFieldsIDs3 = {"DateDays","DateDays","DateDays",     // dD - y 15 30
                                       "DateDays","DateDays","DateDays"};   
    static String[] matchFieldsIDs4 = {"DateHours","DateHours","DateHours",     // dH - y 30 60
                                       "DateHours","DateHours","DateHours"};       
    static String[] matchFieldsIDs5 = {"DateMinutes","DateMinutes","DateMinutes",    // dm - y 300 600
                                       "DateMinutes","DateMinutes","DateMinutes"};      
    static String[] matchFieldsIDs6 = {"DateSeconds","DateSeconds","DateSeconds",     // ds - y 75 60
                                       "DateSeconds","DateSeconds","DateSeconds"};            
      
    static String[][] candRecArrayVals = {{"20070000", "20070000", "20070000", 
                                           "20070000", "20070000", "20070000", "", null}};
    static String[][] refRecArrayVals = {{"20070000", "20090000", "20010000", 
                                           "19970000", "19900000", "20200000", "", null}};     
    
    static String[][] candRecArrayVals2 = {{"20070200", "20070200", "20070200", 
                                           "20070200", "20070200", "20070200", "", null}};
    static String[][] refRecArrayVals2 = {{"20070201", "20070015", "20070910", 
                                           "20071227", "20061019", "20060530", "", null}};   
 
    static String[][] candRecArrayVals3 = {{"20070200", "20070200", "20070200", 
                                           "20070200", "20070200", "20070200", "", null}};
    static String[][] refRecArrayVals3 = {{"20070201", "20070215", "20070210", 
                                           "20070227", "20070219", "20070230", "", null}};   
    
    static String[][] candRecArrayVals4 = {{"20070200:12", "20070200:12", "20070200:12", 
                                           "20070200:12", "20070200:12", "20070200:12", "", null}};
    static String[][] refRecArrayVals4 = {{"20070200:12", "20070200:16", "20070201:00", 
                                           "20070201:13", "20070201:22", "20070230:55", "", null}};     
    
    static String[][] candRecArrayVals5 = {{"20070200:12:33", "20070200:12:33", "20070200:12:33", 
                                           "20070200:12:33", "20070200:12:33", "20070200:12:33", "", null}};
    static String[][] refRecArrayVals5 = {{"20070200:12:22", "20070200:12:10", "20070200:13:25", 
                                           "20070200:13:55", "20070201:09:21", "20070230:12:12", "", null}};   
    
    static String[][] candRecArrayVals6 = {{"20070200:12:33:21", "20070200:12:33:21", "20070200:12:33:21", 
                                            "20070200:12:22:21", "20070200:12:10:21", "20070200:13:25:21", "", null}};
    static String[][] refRecArrayVals6 = {{"20070200:12:33:17", "20070200:12:33:01", "20070200:12:34:21",
                                           "20070200:13:32:21", "20070201:09:21:21", "20070230:12:12:21", "", null}};         
    
    private static double extectedWeight1 = -7.789473684210526;    
    private static double extectedWeight2 = 26.38888888888888;    
    private static double extectedWeight3 = -5.806451612903224;    
    private static double extectedWeight4 = 15.40983606557377;   
    private static double extectedWeight5 = 13.28162917839039;
    private static double extectedWeight6 = -25.987920621225193;



    static int numberOfCandRec;
    static int numberOfRefRec;
    static int fieldsLen;
     
    static {
        resultWeightYears = new double[1][1];        
        resultWeightMonths = new double[1][1];
        resultWeightDays = new double[1][1];
        resultWeightHours = new double[1][1];
        resultWeightMinutes = new double[1][1];
        resultWeightSeconds = new double[1][1];
        int i;
        int j;
    }    

    
    /** 
     * Creates new Bigram
     * @see junit.framework.TestCase
     */
    public DateComparatorTest(String name) {
        super(name);        
    }

    /** 
     * Set up the unit test
     * @see junit.framework.TestCase
     */    
    protected void setUp() {
        
    }

    /** 
     * Tear down the unit test
     * @see junit.framework.TestCase
     */
    protected void tearDown() {
        // cleanup code
    }

    /**
     * Test the Bigram class methods
     * 
     */
    public void testDateComparators() 
        throws MatchComparatorException, IOException,
               MatcherException, ParseException, InstantiationException {
        
        int i;
        int j;
        int k;
        String path = "match/";

        MatchConfigFilesAccess filesAccess = new MatchConfigFilesAccessImpl(path); 

        // The new engine
        MatchingEngine newME = new MatchingEngine(filesAccess);
        newME.upLoadConfigFile(); 
        
        MatchComparator codeClass = newME.getComparatorManager(newME).getComparatorInstance("ds");
        codeClass.setRTParameters("DateFormat", "yyyyMMdd");   
        
	resultWeightYears = newME.matchWeight(matchFieldsIDs, candRecArrayVals, refRecArrayVals);
        resultWeightMonths = newME.matchWeight(matchFieldsIDs2, candRecArrayVals2, refRecArrayVals2);
        resultWeightDays = newME.matchWeight(matchFieldsIDs3, candRecArrayVals3, refRecArrayVals3);
        //
        codeClass.setRTParameters("DateFormat", "yyyyMMdd:HH"); 
        resultWeightHours = newME.matchWeight(matchFieldsIDs4, candRecArrayVals4, refRecArrayVals4);
        //
        codeClass.setRTParameters("DateFormat", "yyyyMMdd:HH:mm"); 
        resultWeightMinutes = newME.matchWeight(matchFieldsIDs5, candRecArrayVals5, refRecArrayVals5);
        //
        codeClass.setRTParameters("DateFormat", "yyyyMMdd:HH:mm:ss"); 
        resultWeightSeconds = newME.matchWeight(matchFieldsIDs6, candRecArrayVals6, refRecArrayVals6);
                		
        assertEquals((float) resultWeightYears[0][0], (float) extectedWeight1, (float) DELTA);
        assertEquals((float) resultWeightMonths[0][0], (float) extectedWeight2, (float) DELTA);
        assertEquals((float) resultWeightDays[0][0], (float) extectedWeight3, (float) DELTA);
        assertEquals((float) resultWeightHours[0][0], (float) extectedWeight4, (float) DELTA);   
        assertEquals((float) resultWeightMinutes[0][0], (float) extectedWeight5, (float) DELTA);  
        assertEquals((float) resultWeightSeconds[0][0], (float) extectedWeight6, (float) DELTA);  
    }
    
    /**
     * Main method needed to make a self runnable class
     * @param args The command line arguments
     */
    public static void main(String[] args) {
        junit.textui.TestRunner.run(new TestSuite(DateComparatorTest.class));
    }   
}

