/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sun.mdm.matcher.comparators.validator;

import java.util.List;

/**
 * Defines the methods to be implemented by any data source
 * 
 * @author souaguenouni
 */
public interface DataSourcesProperties {

    /**
     * Provides the list of data sources associated with a given match field
     * 
     * @param codeName the match field's name 
     * @return the list of data sources
     */    
    List getDataSourcesList(String codeName);
    /**
     * Setter for the list of data sources associated with a given match field
     * 
     * @param index the index of the match field
     * @param codeName the match field's name associated with the data source
     * @param paths the data source path
     */        
    void setDataSourcesList(int index, String codeName, List paths);
    /**
     * Check if the data saource is already loaded
     * 
     * @param sourcePath the data source path
     * @return true if the data source is already loaded by another field
     */    
    boolean isDataSourceLoaded(String sourcePath);
    /**
     * Set the state if the data saource loading
     * 
     * @param sourcePath the data source path
     * @param status the loading status
     */       
    void setDataSourceLoaded(String sourcePath, boolean status);
    /**
     * Provides the object that holds the data sources info
     * 
     * @param sourcePath the data source path
     */        
    Object getDataSourceObject(String sourcePath);
    /**
     * Set the state if the data saource loading
     * 
     * @param sourcePath the data source path
     * @param obj the object that holds the data sources info
     */           
    void setDataSourceObject(String sourcePath, Object obj);
}
