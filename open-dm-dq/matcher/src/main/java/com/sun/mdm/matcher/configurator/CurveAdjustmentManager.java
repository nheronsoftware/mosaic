/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.matcher.configurator;

//import com.sun.mdm.matcher.util.ReadMatchConstantsValues;
import com.sun.mdm.matcher.api.MatcherException;
import java.util.ArrayList;
import java.util.Map;

/**
 * Manages the custom comparator-specific curve adjustment classes
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1 $
 */
public class CurveAdjustmentManager {

    private static final String C_PATH = "com.sun.mdm.matcher.configurator.curveadjustor";
    private static final String CURVE_ADJUSTOR = "CurveAdjustor";
    
    /**
     *
     *
     * @param matchVar an istance of the MatchVariables class
     * @param al the list of curve adjustment parameters defined in the matchConfigFile
     * @param i the match field index
     * @param curveAdjust the custom curve 
     * @throws MatcherException
     */
    static void adjustParameters(MatchVariables mVar, int option, int i)
        throws MatcherException {
        
        int j;
        int defSw = 0;
//        boolean defaultAdjustment = false;
        double[] hf = new double[4];
        String fullClassName1;
        CurveAdjustor curveAdjInstance = null;

        // Assign to hf[] the real values hold in ArrayList 'al' which represent
        // the custom curve-adjustment values defined in the config. 
        // The default values (if there is no data) are zeros.
//        for (j = 0; j < 4; j++) {            
//            hf[j] = ((Integer) al.get(j)).doubleValue();
//        }
        
        String codeName = mVar.comparatorType[i];
        String fullClassName = mVar.compMap.get(codeName);
        String className = fullClassName.substring(fullClassName.lastIndexOf('.'));      
          
        try {
            fullClassName1 = C_PATH+className+CURVE_ADJUSTOR;           
            Class comparatorClass = Class.forName(fullClassName1);
       
            if (comparatorClass != null) {
                curveAdjInstance = (CurveAdjustor) comparatorClass.newInstance(); 

                // Validate the comparators' parameters.
                hf = curveAdjInstance.processCurveAdjustment(codeName, option);
                // Add the data to the MatchVariables class
                mVar.adjCurv.put(codeName, hf.clone());
            }
        } catch (IllegalAccessException ex) {
        } catch (InstantiationException ex) {                       
        } catch (ClassNotFoundException ex) {          
        }      
    }        

}
