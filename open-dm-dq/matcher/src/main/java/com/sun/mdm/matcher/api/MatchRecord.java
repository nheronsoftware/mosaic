/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.matcher.api;

import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Set;

/**
 * Base class that defines the properties of any records that
 * need to be compared
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1 $
 */
public class MatchRecord {

    /*
     * A HashMap variable that holds the pairs of keys/values of
     * the matching fields
     */
    private HashMap matchFields; 

    /**
     * The default constructor
     */
    public MatchRecord() {
       matchFields = new HashMap();
    }
     
    /**
     * Returns a String representation of the value stored in the field
     *
     * @param  key the value associated with the matching field 
     * @return the value of the matching key
     */
    public String getMatchKeyValue(String key) {
        return (String) matchFields.get(key);
    }

    /**
     * Sets the value of the matching field specified by the 'key' argument
     *
     * @param key the matching field's name
     * @param value the corresponding matching value
     */
    public void setMatchKeyValue(String key, String value) {
        matchFields.put(key, value);
    }

    /**
     * Returns a list of values associated with one specifc matching field
     *
     * @param  key the value associated with the matching field 
     * @return an ArrayList of all the field's value
     */
    public List getMatchKeyValues(String key) {
        return (ArrayList) matchFields.get(key);
    }

    /**
     * Sets the multiple values associated with one matching field 
     *
     * @param key the match value
     * @param values an ArrayList instance representing all the values
     */
    public void setMatchKeyValues(String key, List values) {
        matchFields.put(key, values);
    }
    
    /**
     * Returns all the matching fields' names associated with one match object
     * 
     * 
     * @param matchObject the MatchRecord instance
     * @return the set of all the MatchRecord object's keys
     */
    public Set getAllMatchFields(MatchRecord matchObject) {
        return matchFields.keySet();
    }
}
