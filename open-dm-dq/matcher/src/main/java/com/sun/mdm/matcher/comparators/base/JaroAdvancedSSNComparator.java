/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.matcher.comparators.base;

import com.sun.mdm.matcher.comparators.MatchComparatorException;
import com.sun.mdm.matcher.util.CharactersErrorsHandling;
import com.sun.mdm.matcher.configurator.MatchVariables;
import com.sun.mdm.matcher.util.StringsManipulation;
import com.sun.mdm.matcher.comparators.MatchComparator;
import java.util.HashMap;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Map;

/**
 * The algorithm comprises the Jaro string comparator plus One additional enhancements
 * which makes use of the information from the prior characteristics.
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1 $
 */
public class JaroAdvancedSSNComparator implements MatchComparator {
    
    /*   */
    private static final String NULL50 = MatchVariables.NULL50;
    /*   */
    private static int[] indC = {0, 0, 0, 0, 0, 0};
    
    // The variable that holds all the parameters
    private Map<String, Map> params; 
    // The variable that holds all the additional real-time parameters
    private Map<String, String> argumentsRT = new HashMap();     

    /**
     * Initialize the parameters and data sources info.
     * @param  params  provides all the parameters associated with a given match field using this match comparator
     * @param  dataSources  provides all the data sources info. associated with a given match field using this match comparator
     * @param  dependClassList  provides the list of all the dependency classes
     */
    public void initialize(Map<String, Map> params, Map<String, Map> dataSources, Map<String, Map> dependClassList) {
        this.params = params;
    } 
    
    /**
     * A parameters setter
     */
    public void setRTParameters(String key, String value) {
        this.argumentsRT.put(key, value);
    }     
    
    /**
     * Close any related data sources streams
     */
    public void stop() {
        argumentsRT.clear();
    }  
    
    /**
     * Reads two strings and compute how similar they are relying on an algorithm
     * that compare the bigrams of the two strings.
     *
     * @param      recA   Candidate's string record.
     * @param      recB   Reference's string record.
     * @param      context   
     * @return     a real number that measures the similarity
     */
    public double compareFields(String recA, String recB, Map context) 
        throws MatchComparatorException {
        
        StringBuilder recAFlag = new StringBuilder();
        StringBuilder recBFlag = new StringBuilder();

        recA = recA.toUpperCase();
        recB = recB.toUpperCase();        
        StringBuilder sb = null;        
        int lenA;
        int lenB;
        int fixLen;
        char tempHold;

        double weight;
        double numSim;
        int minv;
        int searchRange;
        int lowLim;
        int hiLim;
        int numTrans;
        int numCom;
        int yl1;
        int numSimi = 0;
        int i;
        int j;
        int k;   
        
        String fieldName = argumentsRT.get("matchfield");
        if(fieldName == null) {
            fieldName = context.get("fieldName").toString();
        }       
        // The list of parameters
        Map<String, String> theParams = null;
        if (params != null) {
            theParams = params.get(fieldName);
        }
        
        lenA = recA.length();
        lenB = recB.length();

        // Before going any further, test if we are using a 'nS' and if the record 
        // is not in the list of invalid SSNs (e.g. XXXXXX or 9999999)

        // Modify the pattern so that it can handle exactly Length times 
        // any specified constraint, Length being the length of the records A and B
        if ((theParams != null) && theParams.containsKey("ssnLength")) {
            fixLen = Integer.parseInt(theParams.get("ssnLength")); 
        } else {
            fixLen = 0;
        }

        // First test if the SSN needs to have a fixed length
        if (fixLen > 0) {

            // Make sure that both records have the same axpected length
            if ((lenA != fixLen) || (lenB != fixLen)) {
                return 0.0;
            }
        }

        // Test also which type are the records (numeric or alphanumeric)
        // First, numeric
        if ((theParams != null) && theParams.get("recType").compareTo("nu") == 0) {

            // Make sure that both records are numerics
            for (i = 0; i < lenA; i++) {
                if (!Character.isDigit(recA.charAt(i))) {
                    return 0.0;
                }
            }

            for (i = 0; i < lenB; i++) {
                if (!Character.isDigit(recB.charAt(i))) {
                    return 0.0f;
                }
            }

        // First, alphanumeric
        } else if ((theParams != null) && theParams.get("recType").compareTo("an") == 0) {

            // Make sure that both records are numerics
            for (i = 0; i < lenA; i++) {
                if (!Character.isLetterOrDigit(recA.charAt(i))) {
                    return 0.0;
                }
            }

            for (i = 0; i < lenB; i++) {
                if (!Character.isLetterOrDigit(recB.charAt(i))) {
                    return 0.0;
                }
            }
        }

        if ((theParams != null) && theParams.containsKey("ssnList")) {
            String ssnList = theParams.get("ssnList");
            if(ssnList.trim().length() > 0) { 
                sb = new StringBuilder();
                //First extend the pattern to include at least min(A, B) number
                // of identical characters in the pattern
                sb.append(ssnList);
                sb.append("{").append(Math.min(lenA, lenB)).append(",}");
                Pattern pa = Pattern.compile(sb.toString());                                 
                Matcher matA;
                Matcher matB;


                /* Regular expression that catches empty strings */
                matA = pa.matcher(recA);
                matB = pa.matcher(recB);

                /* test if the record is empty or null */
                if (matA.lookingAt() || matB.lookingAt()) {
                    return 0.0;
                }
            }
        }
        
        // Determine which field is longer
        if (lenA > lenB) {
            searchRange = lenA;
            minv = lenB;
            
        } else {
            searchRange = lenB;
            minv = lenA;
        }
        
        // If the smaller field, in length, is empty (blanks), return a zero weight.
        // Need review since we have already considered this case earlier
        if (minv == 0) {
            return 0.0;
        }
        
        String nullSub50 = NULL50.substring(0, searchRange);
        
        // Blank out the flags
        StringsManipulation.strncat(recAFlag, nullSub50, searchRange);
        StringsManipulation.strncat(recBFlag, nullSub50, searchRange);
    
        // Is lower-side of the middle of the largest field
        searchRange = (searchRange >> 1) - 1;        
        
        // Looking only within the search range, count and flag the matched pairs.
        numCom = 0;
        // Index of last char in field from B-file
        yl1 = lenB - 1;
        
        // Loop over chars of the field from A-file
        for (i = 0; i < lenA; i++) {
            
            lowLim = (i >= searchRange) ? i - searchRange : 0;           
            hiLim = ((i + searchRange) <= yl1) ? (i + searchRange) : yl1;
            
            tempHold = recA.charAt(i);
            
            for (j = lowLim; j <= hiLim; j++) {
                
                if ((recBFlag.charAt(j) != '1')
                    && (recB.charAt(j) == tempHold)) {

                    recBFlag.setCharAt(j, '1');
                    recAFlag.setCharAt(i, '1');
                    numCom++;
                    break;
                }
            }
        }
        
        // If there are no characters in common, return zero agreement.
        // The definition of 'common' is that the agreeing characters must be
        // within 1/2 the length of the shorter string min (s1, s2).
        if (numCom == 0) {
            return 0.0;
        }
        
        // Count the number of transpositions. The definition of 'transposition' is
        // that the character from one string is out of order with the corresponding
        // common character from the other string (in the sens defined above).
        k = 0;
        numTrans = 0;
        
        // Loop over the A-file field chars to search for transpositions
        for (i = 0; i < lenA; i++) {
            
            // If the actual character has some corresponding commons, then
            // loop over the chars of the B-file field.
            if (recAFlag.charAt(i) == '1') {
                
                for (j = k; j < lenB; j++) {
                    
                    // If there is a common between indices (i,j), then break the
                    // inner loop and go for next i and start search from j+1.
                    if (recBFlag.charAt(j) == '1') {
                        
                        k = j + 1;
                        break;
                    }
                }
                
                // If char from A-field has no corresponding common from B-field,
                // increment the number of transposition
                if (recA.charAt(i) != recB.charAt(j)) {                    
                    
                    numTrans++;
                }
            }
        }
       
        // 1/2 the transpositions
        numTrans = numTrans >> 1;
        
        // First inhancement by McLaughlin (1993). Adjust for similarities in
        // nonmatched characters.
        // Similar characters might occur because of scanning errors ('1' versus 'l')
        // or keypunch ('V' versus 'C'). The #common (in Jaro) gets // increased by
        // 0.3 for each similar character
        numSimi = 0;
        
        // Authorize the use of similar characters weightings
        if (indC[1] == 0) {
            
            // Test if the min(str1, str2) is larger than the number of common
            if (minv > numCom) {
                
                for (i = 0; i < lenA; i++) {
                    
                    // Check if the character does not have a commom and is inRange.
                    // See def, at the end.
                    if ((recAFlag.charAt(i) == ' ') && inRange(recA.charAt(i))) {                        
                        
                        tempHold = recA.charAt(i);                        
                        
                        // Loop aver the corresponding B-field
                        for (j = 0; j < lenB; j++) {
                            
                            // Check also if the character does not have
                            // a commom and is inRange.
                            if ((recBFlag.charAt(j) == ' ') && inRange(recB.charAt(j))) {                                

                                // Test if the pair of chars is in the list of
                                // similar characters defined in array adjwt
                                if (CharactersErrorsHandling.correctCharactersErrors(tempHold, recB.charAt(j)) > 0) {                                    
                                    
                                    // Increment the number of similar pairs
                                    // Need review
                                    numSimi += CharactersErrorsHandling.correctCharactersErrors(tempHold, recB.charAt(j));                                    
                                    
                                    // Flag the B-field. How about the A-field?
                                    recBFlag.setCharAt(j, '2');
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
              
        // Update the number of commons by replacing them with:
        // #common + 0.3 * #similar
        numSim = ((double) numSimi) / 10.0 + (double) numCom;
                
        // Main weight computation.
        // Since the initial formula was: 1/3(#common/length(str1) + #common/
        // length(str2) + 1/2*(#transposition / #common).
        // The new formula (adding similar chars) is:
        // 1/3((#common + 0.3*simi)/length(str1) + (#common + 0.3*simi)/length(str2)
        // + 1/2*(#transposition / #common).
        weight = numSim / ((double) lenA) + numSim / ((double) lenB) 
                 + ((double) (numCom - numTrans)) / ((double) numCom);
          
        
        // Multiply by 1/3
        return weight / 3.0;
        
    }   
    
    // Need to be reviewed
    private static boolean inRange(char c) {
        return (((int) c > 0)  && ((int) c < 91)
        || ((int) c > 191)  && ((int) c < 256));
    }
    
}
