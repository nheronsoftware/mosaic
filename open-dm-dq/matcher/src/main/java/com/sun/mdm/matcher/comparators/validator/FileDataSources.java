/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sun.mdm.matcher.comparators.validator;

import java.util.List;
import java.util.Set;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;

/**
 * File-type data sources implemetation
 * 
 * @author souaguenouni
 */
public class FileDataSources implements DataSourcesProperties {
    
    // 
    private Map<Integer, List<String>> dataPath = new HashMap();
    private Map<Integer, String> indexCodeNane = new HashMap();
    private List<String> pathList = new ArrayList();
    private Map<String, Boolean> sourceLoaded = new HashMap();
    private Map<String, Object> srcObject = new HashMap();
    
    /**
     * Provides the list of data sources associated with a given match field
     * 
     * @param codeName the match field's name 
     * @return the list of data sources
     */        
    public List getDataSourcesList(String codeName) {
        //
        Set s = indexCodeNane.keySet();
        List<List<String>> pathL = new ArrayList();
        Iterator<Integer> it = s.iterator();
        while(it.hasNext()) {
            int index = it.next();   
            if (indexCodeNane.get(index).compareTo(codeName) == 0) {          
                pathL.add(dataPath.get(index));
            }
        }
        return pathL;
    }
    
    /**
     * Setter for the list of data sources associated with a given match field
     * 
     * @param index the index of the match field
     * @param codeName the match field's name associated with the data source
     * @param paths the data source path
     */            
    public void setDataSourcesList(int index, String codeName, List paths) {
        dataPath.put(index, paths);
        indexCodeNane.put(index, codeName);
        
        // Add all the sources paths
        Iterator<String> iter = paths.iterator();
        while(iter.hasNext()) {
            String next = iter.next();
            
            if (!pathList.contains(next)) {
                pathList.add(next);
            }
        }        
    }
    
    /**
     * Set the state if the data saource loading
     * 
     * @param sourcePath the data source path
     * @param status the loading status
     */           
    public void setDataSourceLoaded(String sourcePath, boolean status) {
        sourceLoaded.put(sourcePath, status);
    }   
    
    /**
     * Check if the data saource is already loaded
     * 
     * @param sourcePath the data source path
     * @return true if the data source is already loaded by another field
     */        
    public boolean isDataSourceLoaded(String sourcePath) { 
        if (sourceLoaded.containsKey(sourcePath)) {
            return sourceLoaded.get(sourcePath);
        } else {
            return false;
        }
    } 
    
    /**
     * Provides the object that holds the data sources info
     * 
     * @param sourcePath the data source path
     */            
    public Object getDataSourceObject(String sourcePath) {
        return srcObject.get(sourcePath);
    }  
    
    /**
     * Set the state if the data saource loading
     * 
     * @param sourcePath the data source path
     * @param obj the object that holds the data sources info
     */               
    public void setDataSourceObject(String sourcePath, Object obj) {
        srcObject.put(sourcePath, obj);
    }          
}
