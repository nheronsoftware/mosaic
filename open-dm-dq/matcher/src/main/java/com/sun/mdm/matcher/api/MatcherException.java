/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.matcher.api;

/**
 * The base class that handles exceptions related to the Matching Engine
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1 $
 */
public class MatcherException
    extends java.lang.Exception {
    
    /**
     * Constructs an <code>SbmeStandardizationException</code> with
     * no detail message.
     * @param t a throwable.
     */
    public MatcherException(Throwable t) {
        super(t);
    }
    
    /**
     * Constructs an <code>SbmeStandardizationException</code> with
     * a description message of the exception.
     *
     * @param  msg exception message
     * @param  t a throwable
     */
    public MatcherException(String msg, Throwable t) {
        super(msg, t);
    }
    
    /**
     * Constructs an <code>SbmeStandardizationException</code> with
     * a description message of the exception.
     *
     * @param  msg exception message
     */
    public MatcherException(String msg) {
        super(msg);
    }

}
