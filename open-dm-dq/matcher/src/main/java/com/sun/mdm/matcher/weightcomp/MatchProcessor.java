/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.matcher.weightcomp;

import com.sun.mdm.matcher.comparators.ComparatorsManager;
import com.sun.mdm.matcher.configurator.MatchConfigFile;
import com.sun.mdm.matcher.configurator.MatchVariables;
import java.text.ParseException;

import com.sun.mdm.matcher.api.MatchingEngine;
import com.sun.mdm.matcher.api.MatchRecord;
import com.sun.mdm.matcher.api.MatcherException;
import com.sun.mdm.matcher.util.StringsManipulation;

import net.java.hulp.i18n.LocalizationSupport;
import net.java.hulp.i18n.Logger;
import com.sun.mdm.matcher.util.Localizer;
import java.util.logging.Level;
import java.util.HashMap;
import java.util.Map;

/**
 * Main class to process any request to match candidate and reference records
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1 $
 */
public class MatchProcessor implements Cloneable {
    
    private int pat;  
    // Logging handlers
    private transient Logger mLogger = Logger.getLogger(this.getClass().getName());
    private transient Localizer mLocalizer = Localizer.get();     
       

    /**
     * Reads two strings and compute the individual and the global matching weight between them
     * 
     * @param fieldsIDs The types of fields
     * @param candRecArrayVals an array of reference records
     * @param refRecArrayVals an array of reference records
     * @param mEng an instance of the MatchingEngine class
     * @return a real number that measures the degree of match
     * @throws SbmeMatchingException an exception
     * @throws ParseException if a string parsing failed
     */
    public double[][][] performMatchDetails(String[] fieldsIDs, String[][] candRecArrayVals,
        String[][] refRecArrayVals, MatchingEngine mEng)
        throws MatcherException, ParseException {
        
        int i;
        int j;
        int k;
        
        int[] jIndex;
        int theIndx;
        
        int fieldsNumber;
        int blockArecNumber;
        int blockBrecNumber;
        
        double tempCompWeight;
        double wgtIncrement = 0.0;

        MatchVariables matchVar = mEng.getMatchVar(mEng);
        MatchConfigFile mConfig = mEng.getMatchConfig(mEng);
        // 
        Map context = new HashMap(); 
        
 
        /*
         * Get the number of matching records and associated fields
         */
        fieldsNumber = fieldsIDs.length;
        blockArecNumber = candRecArrayVals.length;
        blockBrecNumber = refRecArrayVals.length;

        if (mLogger.isLoggable(Level.FINE)) {
            mLogger.fine("Test if all the provided match types are valid");
        }
        
        // Make sure that all the matching fields are 
        // valid types in the matchConfigFile
        for (i = 0; i < fieldsNumber; i++) {
            if (!matchVar.fieldName.containsValue(fieldsIDs[i])) {
                throw new MatcherException("The match type: " + fieldsIDs[i]
                    + " is invalid! \n You need to define it within the matchConfigFile.cfg file");                      
            }
        }

        if (mLogger.isLoggable(Level.FINE)) {
            mLogger.fine("Start the matching process!");
        }
        
        // Define the number of indexes associated with matching variables
        jIndex = new int[fieldsNumber];
        
        /* Define a variable that holds the fields of the candidate records */
        String[] candRecVals = new String[fieldsNumber];
        double[][][] computedWeight = new double[fieldsNumber+1][blockArecNumber][blockBrecNumber];
        
        /* Define a variable that holds the fields of the reference records */
        String[] refRecVals = new String[fieldsNumber];
        // Reads the instance class of the match comparator
        ComparatorsManager comparMgr = mEng.getComparatorManager(mEng);
        
        // Returns the corresponding indexes of the fields in the config file
        jIndex = mConfig.getFieldIndices(fieldsIDs, mEng);
        
        for (i = 0; i < blockArecNumber; i++) {
            
            // Array of fields of record i in the candidate block
            candRecVals = candRecArrayVals[i];
            
            // Loop over a corresponding block in the corresponding file.
            for (k = 0; k < blockBrecNumber; k++) {
                
                tempCompWeight = 0.0;
                pat = 0;
                
                // Array of fields of record k in the candidate block
                refRecVals = refRecArrayVals[k];               

                // Loop over all the matching fields in the actual record 
                for (j = 0; j < fieldsNumber; j++) {
                    
                    computedWeight[j][i][k] = 0.0;
                    theIndx = jIndex[j];

                    // Call method weight() to calculate the weight associated
                    // with (j,k) pair.
                    wgtIncrement = WeightComputation.calculateWeight(fieldsIDs[j], candRecVals[j], refRecVals[j], 
                                                                     theIndx, mEng, comparMgr, context);
                   
                    // Individual weight for each field
                    computedWeight[j][i][k] = wgtIncrement;         
                    
                    // Add the calculated weight to the sum of weights for the pair
                    tempCompWeight += wgtIncrement;
                    
                } // end of loop over match fields

                // The total weight
                computedWeight[fieldsNumber][i][k] = tempCompWeight;                
            }
        }
        return computedWeight;
    }    
    /**
     * Reads two strings and compute the global matching weight between them
     * 
     * @param fieldsIDs The types of fields
     * @param candRecArrayVals an array of reference records
     * @param refRecArrayVals an array of reference records
     * @param mEng an instance of the MatchingEngine class
     * @return a real number that measures the degree of match
     * @throws SbmeMatchingException an exception
     * @throws ParseException if a string parsing failed
     */
    public double[][] performMatch(String[] fieldsIDs, String[][] candRecArrayVals,
        String[][] refRecArrayVals, MatchingEngine mEng)
        throws MatcherException, ParseException {
        
        int i;
        int j;
        int k;
        
        int[] jIndex;
        int theIndx;
        
        int fieldsNumber;
        int blockArecNumber;
        int blockBrecNumber;    
        double tempCompWeight;
        double wgtIncrement = 0.0;

        MatchVariables matchVar = mEng.getMatchVar(mEng);
        MatchConfigFile mConfig = mEng.getMatchConfig(mEng);
        // 
        Map<String, String> context = new HashMap(); 
        /*
         * Get the number of matching records and associated fields
         */
        fieldsNumber = fieldsIDs.length;
        blockArecNumber = candRecArrayVals.length;
        blockBrecNumber = refRecArrayVals.length;

        if (mLogger.isLoggable(Level.FINE)) {
            mLogger.fine("Test if all the provided match types are valid");
        }
        
        // Make sure that all the matching fields are 
        // valid types in the matchConfigFile
        for (i = 0; i < fieldsNumber; i++) {            
            if (!matchVar.fieldName.containsValue(fieldsIDs[i])) {
                throw new MatcherException("The match type: " + fieldsIDs[i]
                    + " is invalid! \n You need to define it within the matchConfigFile.cfg file");                      
            }
        }

        if (mLogger.isLoggable(Level.FINE)) {
            mLogger.fine("Start the matching process!");
        }
        
        // Define the number of indexes associated with matching variables
        jIndex = new int[fieldsNumber];
        
        /* Define a variable that holds the fields of the candidate records */
        String[] candRecVals = new String[fieldsNumber];
        double[][] computedWeight = new double[blockArecNumber][blockBrecNumber];
        
        /* Define a variable that holds the fields of the reference records */
        String[] refRecVals = new String[fieldsNumber];

        // Reads the instance class of the match comparator
        ComparatorsManager comparMgr = mEng.getComparatorManager(mEng);
        
        // Returns the corresponding indexes of the fields in the config file
        jIndex = mConfig.getFieldIndices(fieldsIDs, mEng);

        for (i = 0; i < blockArecNumber; i++) {
            // Array of fields of record i in the candidate block
            candRecVals = candRecArrayVals[i];
            
            // Loop over a corresponding block in the corresponding file.
            for (k = 0; k < blockBrecNumber; k++) {
                
                tempCompWeight = 0.0;
                pat = 0;           
                
                // Array of fields of record k in the candidate block
                refRecVals = refRecArrayVals[k];               

                // Loop over all the matching fields in the actual record 
                for (j = 0; j < fieldsNumber; j++) {
                    
                    computedWeight[i][k] = 0.0;
                    theIndx = jIndex[j];

                    // Call method weight() to calculate the weight associated
                    // with (j,k) pair.
                    wgtIncrement = WeightComputation.calculateWeight(fieldsIDs[j], candRecVals[j], refRecVals[j], 
                                                                     theIndx, mEng, comparMgr, context);
                    
                    // Add the calculated weight to the sum of weights for the pair
                    tempCompWeight += wgtIncrement;
                    
                } // end of loop over match fields

                // The total weight
                computedWeight[i][k] = tempCompWeight;  
            }
        }
        return computedWeight;
    }        
    
    
    /**
     * We can use downweight adjustment parameters associated with
     * data-specific matching variables. We have dw=1 (upweight adjustment
     * for transposed middle & first names, dw=3 (downweight for simultaneous
     * disagreement of first name, sex and age. dw=2, corresponds to both
     * upweight and downweight.
     *
     * @param fnVal
     * @param mdVal
     * @return matchVar
     */
    private static double downWeight(String[] fnVal, String[] mdVal, MatchVariables matchVar) {
        
        double dummy = 0.0;
        
        // Refers to first name
        int m1 = matchVar.m1;
        // Refers to middle name
        int m2 = matchVar.m2;
        
        // Upweight adjustment for transposed middle & first names
        if (matchVar.dw < 3) {
            
            // Compare first name's first character with middle name one character
            if (StringsManipulation.strncmp(fnVal[1], mdVal[0], 1) == 0) {
                
                // Reverse the fields' indices to double check
                if (StringsManipulation.strncmp(mdVal[1], fnVal[0], 1) == 0) {
                    
                    dummy += matchVar.fm1;
                    
                } else if (StringsManipulation.strncmp(mdVal[1], fnVal[0], 1) != 0) {
                    // If the result is different use a different parameter
                    
                    dummy += matchVar.fm2;
                }
                
            } else if (StringsManipulation.strncmp(fnVal[1], mdVal[0], 1) != 0) {
                // If it does not match, reverse the indices.
                
                if (StringsManipulation.strncmp(mdVal[1], fnVal[0], 1) == 0) {
                    
                    // Add the real value to this variable
                    dummy += matchVar.fm2;
                }
            }
        }
        
        // This part handles downweight for simultaneous disagreement of
        // first name, sex and age
        if (matchVar.dw > 1) {
            
            // NEED MORE INVESTIGATION
            if ((matchVar.dwi[matchVar.dw1] == 1)
            && (matchVar.dwi[matchVar.dw2] == 1)) {
                
                dummy -= ((matchVar.dwi[matchVar.dw3] == 1) ? matchVar.fw1 : matchVar.fw2);
                
            } else if ((matchVar.dwi[matchVar.dw1] == 1)
            && (matchVar.dwi[matchVar.dw3] == 1)) {
                
                dummy -= matchVar.fw3;
            }
        }
        return dummy;
    }


    /**
     * 
     * 
     * 
     * @param matchFieldsIDs the type of match field
     * @param candRecVals the candidate list of records
     * @param refRecVals the reference list of records
     * @param mEng an instance of the MatchingEngine class
     * @return a double
     * @throws SbmeMatchingException any generic exeption in the matching engine
     */
    public double performMatch(String[] matchFieldsIDs, String[] candRecVals,
        String[] refRecVals, MatchingEngine mEng)
        throws MatcherException {

        throw new MatcherException("This method is not functional \n"
            + " Use the generic method performMatch(String[] fieldID, String[][] candRecs,"
            + " String[][] refRecs, MatchingEngine mEng)");
    }

 
    /**
     * 
     * 
     * 
     * @param matchFieldsIDs the type of match field
     * @param candRecVals the values of the fields of candidate record
     * @param refRecArrayVals the values of the fields of an array of ref records
     * @param mEng an instance of the MatchingEngine class
     * @return an array of doubles
     * @throws SbmeMatchingException any generic exeption in the matching engine
     */
    public double[] performMatch(String[] matchFieldsIDs, String[] candRecVals,
        String[][] refRecArrayVals, MatchingEngine mEng)
        throws MatcherException {

            throw new MatcherException("This method is not functional \n"
                + " Use the generic method performMatch(String[] fieldID, String[][] candRecs,"
                + " String[][] refRecs, MatchingEngine mEng)");
    }

 
 
    
    /**
     * 
     * 
     * 
     * @param aStr a candidate object
     * @param bStr a reference object
     * @param mEng an instance of the MatchingEngine class
     * @return a double
     * @throws SbmeMatchingException any generic exeption in the matching engine
     */
    public double performMatch(MatchRecord aStr, MatchRecord bStr,
    MatchingEngine mEng)
        throws MatcherException {

        throw new MatcherException("This method is not functional \n"
            + " Use the generic method performMatch(String[] fieldID, String[][] candRecs,"
            + " String[][] refRecs, MatchingEngine mEng)");
    }


    /**
     * 
     * 
     * 
     * @param aStr a candidate object
     * @param bStr an array of reference objects
     * @param mEng an instance of the MatchingEngine class
     * @return an array of doubles
     * @throws SbmeMatchingException any generic exeption in the matching engine
     */
    public double[] performMatch(MatchRecord aStr, MatchRecord[] bStr,
    MatchingEngine mEng)
        throws MatcherException {

        throw new MatcherException("This method is not functional \n"
            + " Use the generic method performMatch(String[] fieldID, String[][] candRecs,"
            + " String[][] refRecs, MatchingEngine mEng)");
    }
    
    /**
     * 
     * 
     * 
     * @param aStr an array of candidate objects
     * @param bStr an array of reference objects
     * @param mEng an instance of the MatchingEngine class
     * @return a 2D array of doubles
     * @throws SbmeMatchingException any generic exeption in the matching engine
     */
    public double[][] performMatch(MatchRecord[] aStr, MatchRecord[] bStr,
    MatchingEngine mEng)
        throws MatcherException {

        throw new MatcherException("This method is not functional \n"
            + " Use the generic method performMatch(String[] fieldID, String[][] candRecs,"
            + " String[][] refRecs, MatchingEngine mEng)");
    }
    
    /**
     * 
     * 
     * 
     * @param str a record object
     * @param mEng an instance of the MatchingEngine class
     * @return a double
     * @throws SbmeMatchingException any generic exeption in the matching engine
     */
    public double[][] performMatch(MatchRecord[] str, MatchingEngine mEng)
        throws MatcherException {

        throw new MatcherException("This method is not functional \n"
            + " Use the generic method performMatch(String[] fieldID, String[][] candRecs,"
            + " String[][] refRecs, MatchingEngine mEng)");
    }
    
    /**
     * 
     * 
     * 
     * @param matchFieldsIDs an array of fields' types
     * @param str the string values of the fields
     * @param mEng an instance of the MatchingEngine class
     * @return a double
     * @throws SbmeMatchingException any generic exeption in the matching engine
     */
    public double[][] performMatch(String[] matchFieldsIDs, String[][] str,
    MatchingEngine mEng)
        throws MatcherException {

        throw new MatcherException("This method is not functional \n"
            + " Use the generic method performMatch(String[] fieldID, String[][] candRecs,"
            + " String[][] refRecs, MatchingEngine mEng)");
    }
}
