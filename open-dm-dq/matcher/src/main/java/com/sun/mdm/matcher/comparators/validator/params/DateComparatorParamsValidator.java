/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.matcher.comparators.validator.params;

import com.sun.mdm.matcher.comparators.validator.ParametersValidator;
import java.util.Map;
import com.sun.mdm.matcher.api.MatcherException;

import net.java.hulp.i18n.LocalizationSupport;
import net.java.hulp.i18n.Logger;
import com.sun.mdm.matcher.util.Localizer;
import java.util.logging.Level;

/**
 *
 * @author souaguenouni
 */
public class DateComparatorParamsValidator implements ParametersValidator {
    
    // Handles logging messages
    private transient final Logger mLogger = Logger.getLogger(this.getClass().getName());
    private transient final Localizer mLocalizer = Localizer.get();
    
    /**
     * Provides validation rules for the parameters associated with any given comparator
     */
    public void validateComparatorsParameters(Map<String, Object> params) 
        throws MatcherException {

        if (mLogger.isLoggable(Level.FINE)) {
            mLogger.fine("Reading the Date match comparator");
        }        

        // Read the switch that control the two different options on dates
        // (relative distance and string-type comparison that handle keypunch,
        // phonetic and transposition errors
        if (!((params.get("switch").toString().compareTo("y") == 0)
                || (params.get("switch").toString().compareTo("n") == 0))) {

            mLogger.severe(mLocalizer.x("MAT001: You must choose 'y' or 'n' only to select "+
                                        "the type of date comparator"));
                throw new MatcherException("You must choose 'y' or 'n' only to select the type"
                        + " of date comparator: 'y' for distance and 'n' for string-like comparisons");
 
        } else if (params.get("switch").toString().compareTo("n") == 0) {
            return;
        }

        // Read the ranges (right and left) over which a date will have a non null weight
        // Check that the number is positive
        if (Integer.parseInt(params.get("lLimit").toString()) < 0) {

            mLogger.severe(mLocalizer.x("MAT002: Use only positive numbers within the "+
                                        "date comparator's parameters"));
            throw new MatcherException("Use only positive numbers within the"
                                       + " date comparator's parameters");
        }

        if (Integer.parseInt(params.get("uLimit").toString()) < 0) {

            mLogger.severe(mLocalizer.x("MAT003: Use only positive numbers within the "+
                                        "date comparator's parameters"));
            throw new MatcherException("Use only positive numbers within the"
                                       + " date comparator's parameters");
        }  
    }
}
