/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.matcher.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.File;
import java.io.IOException;
import java.util.zip.ZipFile;
import java.util.zip.ZipEntry;
import java.util.Enumeration;
import java.io.InputStream;
import java.io.FileOutputStream;
import com.sun.mdm.matcher.api.MatcherException;

/**
 *
 * @author souaguenouni
 */
public class IOHelperClass {   
    
    /**
     * 
     * @param directoryFile
     *            aaaa
     * @return a BufferedReader
     * @throws MdmStandardizationException
     *             aaaa
     */
    public static BufferedReader openBufferedReader(final File file) 
        throws MatcherException {

        try {
            final FileInputStream fS = new FileInputStream(file);
            final InputStreamReader sR = new InputStreamReader(fS);
            final BufferedReader br = new BufferedReader(sR);
            return br;
        } catch (final FileNotFoundException fe) {
            throw new MatcherException("File not found ", fe);
        }
    }
    /**
     * 
     * @param directoryFile
     *            aaaa
     * @return a BufferedReader
     * @throws MdmStandardizationException
     *             aaaa
     */
    public static BufferedReader openBufferedReaderFromZip(String zipName, String fileName) 
        throws FileNotFoundException, IOException {    
        
        if(zipName == null) {
            return null;
        }
        ZipFile zf = new ZipFile(zipName);
        BufferedReader bf = null;                
        Enumeration<?extends ZipEntry> en = zf.entries();
                
        while (en.hasMoreElements()) {
            ZipEntry jarEntry = en.nextElement();
            if (!jarEntry.isDirectory()) {
                if (jarEntry.getName().matches(fileName)) { 
                    InputStream is = zf.getInputStream(jarEntry);
                    InputStreamReader isr = new InputStreamReader(is);
                    bf = new BufferedReader(isr);
                }
            }
        } 
        return bf;
    }
    
    /**
     * 
     * 
     * @param br aaaa
     * @return a boolean
     * @throws MatcherException aaaa
     */
    public static boolean feof(BufferedReader br)
        throws MatcherException {

        String s;

        try {
            br.mark(500);
            s = br.readLine();
            br.reset();
        } catch (IOException ie) {
            throw new MatcherException(" ", ie);
        }
        return (s == null);
    }

 
    /**
     * 
     * 
     * @param br  aaaa
     * @param sb aaaa
     * @param size aaaa
     * @return the size of the string
     * @throws SbmeMatchEngineException aaaa
     */
    public static Object fgets(BufferedReader br, StringBuffer sb, int size)
        throws MatcherException  {

        String s;

        try {
            s = br.readLine();
        } catch (IOException ie) {
            throw new MatcherException(" ", ie);
        }


        if (s == null) {
            return Boolean.FALSE;
        }

        sb.delete(0, sb.length());
        int k = s.length();

        if ((k < size) || (size == 0)) {
            sb.insert(0, s);

        } else {
            sb.insert(0, s.substring(0, size));
        }
        return sb;
    }    
}
