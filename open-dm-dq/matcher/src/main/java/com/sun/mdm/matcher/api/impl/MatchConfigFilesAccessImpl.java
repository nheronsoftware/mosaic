/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.matcher.api.impl;

import java.io.InputStream;
import com.sun.mdm.matcher.api.MatcherConfigurationException;
import com.sun.mdm.matcher.api.MatchConfigFilesAccess;
import com.sun.mdm.matcher.api.ConfigFilesAccess;

/**
 *
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1 $
 */
public class MatchConfigFilesAccessImpl
    implements MatchConfigFilesAccess {

    private final ConfigFilesAccess mFilesAccess;
    private static final String DEFAULT_MATCH_PATH = "match/"; 
    private static String matchPath = "";
    private static final String MATCH_CONFIG = "matchConfigFile.cfg";

    //Use class loader to get resource
    public MatchConfigFilesAccessImpl(String configPath) {
        mFilesAccess = null;
        if (configPath != null) {
            matchPath = configPath;
        } else {
            matchPath = DEFAULT_MATCH_PATH;
        }
    }
    
    public MatchConfigFilesAccessImpl(ConfigFilesAccess filesAccess) {
        mFilesAccess = filesAccess;  
    }
    
    private InputStream getResource(String resource)
    throws MatcherConfigurationException {
  
        InputStream stream = null;
        if (mFilesAccess == null) { 
            stream = this.getClass().getClassLoader().getResourceAsStream(matchPath + resource);
            if (stream == null) {
                stream = ClassLoader.getSystemResourceAsStream(matchPath + resource);
            }
        } else {            
            stream = mFilesAccess.getConfigFileAsStream(matchPath + resource);       
        }

        if (stream == null) {
            if (mFilesAccess == null) {
                stream = this.getClass().getClassLoader().getResourceAsStream("/" + resource);
                if (stream == null) {
                    stream = ClassLoader.getSystemResourceAsStream("/" + resource);
                }
            } else {
                stream = mFilesAccess.getConfigFileAsStream("/" + resource);
            }
        }
        
        if (stream == null) {
            throw new MatcherConfigurationException("Resource: " + resource + " not found");
        }
        return stream;
    }        

    /**
     * Provides the path to the resources files
     *
     * @return the corresponding full path of the resources
     */
    public String getConfigPath() {     
        return matchPath;
    }    
    
    /**
     * Provides a stream from the match confguration file
     *
     * @return the corresponding stream for the match confguration file
     * @throws SbmeConfigurationException a file related exception
     */
    public InputStream provideMatchConfigFile()
        throws MatcherConfigurationException {
        
        return getResource(MATCH_CONFIG);
    }
}
