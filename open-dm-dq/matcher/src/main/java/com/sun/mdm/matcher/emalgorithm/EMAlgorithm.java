/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.matcher.emalgorithm;

import com.sun.mdm.matcher.api.MatchingEngine;
import com.sun.mdm.matcher.configurator.MatchVariables;
//import com.sun.mdm.matcher.util.ReadMatchConstantsValues;

import net.java.hulp.i18n.LocalizationSupport;
import net.java.hulp.i18n.Logger;
import com.sun.mdm.matcher.util.Localizer;
import java.util.logging.Level;

/**
 * We use the EM Algorithm (for the latent k-class independent model)
 * for Weight Computation in the Fellegi-Sunter Model of Record
 * Linkage". In the case where we use a 3-class EM algorithm, it
 * means that we divide the set of pairs into (1) matches within the
 * same household, (2) nonmatches within the same household, and (3)
 * nonmatches that are not within the same household. The second and
 * third classes are collapsed into the class of nonmatches
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1 $
 */
public class EMAlgorithm {
    
        /* */
///    private static final int N_FIELDS = ReadMatchConstantsValues.getNFields();
     private static final int N_FIELDS = 40;
        /* */
//    private static final int MCELL = ReadMatchConstantsValues.getNumberPatt();
    private static final int MCELL = (int) Math.pow(2, 10);
        /* */
//    private static final int MCOMB = ReadMatchConstantsValues.getMComb();
     private static final int MCOMB = 2*20;
        /* */
    private static final int MCLS = 3; // ReadMatchConstantsValues.getMcls();
    
 
    // Logging handlers
    private transient Logger mLogger = Logger.getLogger(this.getClass().getName());
    private transient Localizer mLocalizer = Localizer.get();     
    
    /* */
    private int totalNumPatt;
        /* */
    private int numUniquePatt;
        /* */
    private int numIter;
    
        /* */
    private double prod;
        /* */
    private double numRec;
        /* */
    private boolean exitFlag = false;
        /* */
    private boolean newFlg;
        /* */
    private double[] proba = new double[MCLS];
        /* */
    private double[] freq = new double[MCELL];
    
    // change later the MCOMB
    private int[] ibasof = new int[MCOMB];
    
        /* */
    private double[] probl = new double[MCLS];
        /* */
    private double[] vecthh = new double[MCLS * MCOMB];
        /* */
    private double[] vectrh = new double[MCLS * MCOMB + 1];
        /* */
    private double[] vector = new double[MCLS * MCOMB + 1];
        /* */
    private int[] gamma = new int[MCELL * N_FIELDS];
        /* */
    private double[] prodl = new double[MCLS * MCELL];
        /* */
    private double[] summ = new double[MCLS * MCOMB];


    /**
     * Define a factory method that return an instance of the class
     * @return a EMAlgorithm object
     */
    public static EMAlgorithm getInstance() {
        return new EMAlgorithm();
    }
    
    /**
     * 
     * 
     * @param matchFieldsIDs the match field's key
     * @param mEng the corresponding instance ofMatchingEnginee class
     */
    public void computeEMProba(String[] matchFieldsIDs, MatchingEngine mEng) {
        
        int fieldsNumber;
        
        MatchVariables matchVar = mEng.getMatchVar(mEng);
        
        /*  * Get the number of matching records and associated fields */
        fieldsNumber = matchFieldsIDs.length;
        
        // Initialization step
        initialization(fieldsNumber, matchFieldsIDs, matchVar);
        
        // M probability computation
        calEMProbability(matchVar, fieldsNumber);
        
        // U probability computation
        compUProba(fieldsNumber, matchVar);
        
        // Update the agreement and disagreement weights
        updateWeights(fieldsNumber, matchVar);
    }
    
    /**
     * @param fieldsNumber
     * @param matchFieldsIDs
     * @param matchVar
     */
    private void initialization(int fieldsNumber, String[] matchFieldsIDs,
    MatchVariables matchVar) {
        
        int i;
        int j;
        int ll;
        int k;
        int kk;
        int n;
        int m;
        double t1;
        double t2;
        double[] initProbValue = new double[MCLS];
        
        // Check for missing data from file parmemb. All the  parameters
        // must be strinctly positive
        if ((fieldsNumber == 0) || (matchVar.degPrecision == 0)
        || (matchVar.proportion == 0)
        || (matchVar.nIteration == 0)) {
            
            // print to log file
        }
        
        // Initial guess for the first 2 probabilities
        proba[0] = 0.1;
        proba[1] = 0.3;
        
        // Case where the number of latent classes is more than 2.
        if (MCLS > 2) {
            
            // Initialize the probalilities higher than 2
            for (i = 2; i < MCLS; i++) {
                
                proba[i] = (1.0 - proba[0] - proba[1]) / (MCLS - 2);
            }
        // Adjust the second initial probability if the number of latent
        // classes is 2.            
        } else if (MCLS == 2) {
            proba[1] = 1.0 - proba[0];
        }
        
        // Check that the number of variables is less than the max allowed
        if (fieldsNumber > N_FIELDS) {           
            mLogger.severe(mLocalizer.x("MEG035: Insufficient storage, make N_FIELDS >= nmv"
                           +" nmv currently equal {0}",fieldsNumber));           
            exitFlag = true;
        }
        
        // Initialize the parameters
        n = 1;
        totalNumPatt = 1;
        numUniquePatt = 0;
        
        // Loop over all the matching variables
        for (i = 0; i < fieldsNumber; i++) {
            
            // Calculate all the possible patterns associated with any
            // given record, If ibase=2 and NMV=10, then it equals 1024
            totalNumPatt *= matchVar.fieldPatt[matchVar.jIndex[i]];
            
            // Calculate all the individual, different, patterns. If ibase=2
            // and NMV=10, then it equals 20.
            numUniquePatt += matchVar.fieldPatt[matchVar.jIndex[i]];
            
            // Initialize a variable that holds all the individual patterns up to
            // the ith index
            ibasof[i] = 1;
        }
        
        // Check that total number of patterns is less than the maximum allowed.
        // Write to error log file, and set exflag as true.
        if (totalNumPatt > MCELL) {
            
            mLogger.severe(mLocalizer.x("MEG036: Insufficient storage, make N_FIELDS >= nmv"
                          + "  nmv currently equal {0}", totalNumPatt));
            
            exitFlag = true;
        }
        
        // Test for numUniquePatt < MCOMB. If not, exflag = true
        if (numUniquePatt > MCOMB) {
            
            mLogger.severe(mLocalizer.x("MEG037: Insufficient storage, make MCOMB >= numUniquePatt"
                           + " totalNumPatt currently equal {0}", numUniquePatt));
            
            exitFlag = true;
        }
        
        // Populate the variable ibasof[]. If ibase=2 and NMV=10, then ibaseof[1]
        // equals 2, ibaseof[2]=4,..., ibaseof[NMV-1]=20.
        for (i = 1; i <= fieldsNumber; i++) {
            
            for (j = 1; j <= i; j++) {
                ibasof[i] += matchVar.fieldPatt[matchVar.jIndex[j - 1]];
            }
        }
        
        // Loop over the number of latent classes (generally 3)
        for (j = 0; j < MCLS; j++) {
            
            vectrh[MCLS * numUniquePatt + j] = proba[j];
        }
        
        // Read initial values of the marginal probabilities.
        for (i = 0; i < fieldsNumber; i++) {
            
            // If we use a yes/no agreement pattern, then ll=1 and
            // k=1,3,5,...,19.
            ll = matchVar.fieldPatt[matchVar.jIndex[i]] - 1;
            k = ibasof[i] - 1;
            
            initProbValue[0] = matchVar.Mproba[matchVar.jIndex[i]];
            initProbValue[1] = matchVar.Uproba[matchVar.jIndex[i]];
            initProbValue[2] = matchVar.fracVal[matchVar.jIndex[i]];
            
            for (kk = 0; kk < MCLS; kk++) {
                
                vectrh[k + ll + kk * numUniquePatt] = 1.0;
            }
            
            // Read data from "cntcb3.dat". If problems, write to log file.
            for (j = 0; j < ll; j++) {
                
                // Loop over the MCLS initial values associated with each field
                // In our case (3-class), there are 3 values. The m-probability,
                // the u-probability and
                for (kk = 0; kk < MCLS; kk++) {
                    
                    // Read the u and m probabilities, plus the value that defines
                    // the ratio of name match and nonmatch within the same address
                    vectrh[k + j + kk * numUniquePatt] = initProbValue[kk];
                }
                
                //
                for (kk = 0; kk < MCLS; kk++) {
                    
                    vectrh[k + ll + kk * numUniquePatt] -= vectrh[k + j + kk * numUniquePatt];
                    
                    if (vectrh[k + ll + kk * numUniquePatt] <= 0.0) {
                        
                        mLogger.fine("negative disagr prob for ith field, i =" + i);
                        mLogger.fine(" Field i =  " + matchFieldsIDs[i]);
                        
                        exitFlag = true;
                        return;
                    }
                }
            }
        }
        
        // This corresponds to the case where we have a one-class system.
        // Not practical
        if (j == 0) {
            mLogger.fine(" No convex constraints enabled/n");
        }
        
        // Read the frequency count for each of the totalNumPatt agreement pattern
        numRec = 0.0;
        
        for (i = 0; i < totalNumPatt; i++) {
            
            // Use double precision
            freq[i] = (double) matchVar.freqPatt[i];
            
            // Increment the total number of counts
            numRec += freq[i];
        }
    }
    
    /**
     * @param matchVar
     * @param fieldsNumber
     */
    private void calEMProbability(MatchVariables matchVar, int fieldsNumber) {
        
        newFlg = false;
        int kk;
        int ll;
        int i;
        
        int j;
        int k;
        int ii;
        
        double zlike;
        double zlikeh;
        double absDiff;
        double t1;
        
        // Loop
        for (i = 0; i < (MCLS * (numUniquePatt + 1)); i++) {
            vector[i] = vectrh[i];
        }
        
        // Call gammav() and write to "embi.trk"
        gammav(fieldsNumber, matchVar);
        
        for (i = 0; i < matchVar.nIteration; i++) {
            numIter = i + 1;
            
            // Get new probability estimate via the EM algorithm.
            vecmu(fieldsNumber, matchVar);
            
            zlike = 0.0;
            
            for (j = 0; j < totalNumPatt; j++) {
                newFlg = true;
                
                // Compute log likelihood.
                probT(j, fieldsNumber, matchVar);
                
                t1 = 0.0;
                
                for (ii = 0; ii < MCLS; ii++) {
                    t1 += probl[ii] * proba[ii];
                }
                
                if (t1 > 0.0) {
                    zlike += freq[j] * Math.log(t1);
                }
            }
            
            zlike /= numRec;
            
            // Test for convergence
            prod = 0.0;
            
            for (j = 0; j < (MCLS * (numUniquePatt + 1)); j++) {
                absDiff = Math.abs(vector[j] - vectrh[j]);
                
                if (absDiff > prod) {
                    prod = absDiff;
                }
            }
            
            // Initialization for next pass through loop
            for (j = 0; j < (MCLS * (numUniquePatt + 1)); j++) {
                vectrh[j] = vector[j];
            }
            
            for (j = 0; j < MCLS; j++) {
                proba[j] = vector[MCLS * numUniquePatt + j];
            }
            
            zlikeh = zlike;
            
            if (prod < matchVar.degPrecision) {
                break;
            }
        }
        
        if (!(prod < matchVar.degPrecision)) {
            mLogger.fine(" Max. number of iterations exceeded.");
        }
        return;
    }
    
    /**
     * This subroutine estimates the probability of agreement associated with each
     * field 'k', given a match and a nonmatch.
     *
     * @param fieldsNumber
     * @param matchVar
     */
    private void vecmu(int fieldsNumber, MatchVariables matchVar) {
        
        int kk;
        int i;
        int ll;
        int j;
        int jj;
        int ii;
        
        for (i = 0; i < (MCLS * (numUniquePatt + 1)); i++) {
            summ[i] = 0.0;
        }
        
        // Call
        probmu(fieldsNumber, matchVar);
        
        // Calculate the entire sum for a class
        for (i = 0; i < MCLS; i++) {
            
            kk = MCLS * numUniquePatt + i;
            
            for (j = 0; j < totalNumPatt; j++) {
                
                summ[kk] += prodl[j * MCLS + i] * freq[j];
            }
        }
        
        
        for (int k = 0; k < fieldsNumber; k++) {
            
            kk = matchVar.fieldPatt[matchVar.jIndex[k]];
            ll = ibasof[k] - 1;
            
            // Compute the sum of the appropriate subcategories needed for marginal
            // probabilities estimates.
            for (j = 0; j < totalNumPatt; j++) {
                
                jj = gamma[j * fieldsNumber + k];
                
                for (i = 0; i < MCLS; i++) {
                    
                    ii = i * numUniquePatt + ll + kk - jj - 1;
                    
                    summ[ii] += prodl[j * MCLS + i] * freq[j];
                }
            }
            
            for (i = 0; i < MCLS; i++) {
                
                for (ii = 0; ii < kk; ii++) {
                    
                    j = i * numUniquePatt + ll + ii;
                    
                    vector[j] = summ[j] / summ[MCLS * numUniquePatt + i];
                }
            }
        }
        
        for (i = 0; i < MCLS; i++) {
            
            proba[i] = summ[MCLS * numUniquePatt + i] / numRec;
            
            vector[MCLS * numUniquePatt + i] = proba[i];
        }
    }
    
    /**
     * Calculates the expected proportions, given the pattern and the
     * class-type, based on the existing probabilities.
     *
     * @param fieldsNumber
     * @param matchVar
     */
    private void probmu(int fieldsNumber, MatchVariables matchVar) {
        
        double t1;
        int i;
        int j;
        
        // probl[] holds the current estimates of the computed
        for (j = 0; j < totalNumPatt; j++) {
            
            probT(j, fieldsNumber, matchVar);
            
            t1 = 0.0;
            
            for (i = 0; i < MCLS; i++) {
                
                t1 += proba[i] * probl[i];
            }
            
            prodl[(j + 1) * MCLS - 1] = 1.0;
            
            for (i = 0; i < MCLS - 1; i++) {
                
                prodl[j * MCLS + i] = proba[i] * probl[i] / t1;
                
                prodl[(j + 1) * MCLS - 1] -= prodl[j * MCLS + i];
            }
        }
    }
    
    /**
     * Computes the probability of a pattern, given the class type and the current
     * marginal estimates.
     * @param j
     * @param fieldsNumber
     * @param matchVar
     */
    private void probT(int j, int fieldsNumber, MatchVariables matchVar) {
        
        int ll;
        int kk;
        int mm;
        int i;
        int jj;
        
        // Initialize the values
        for (i = 0; i < MCLS; i++) {
            probl[i] = 1.0;
        }
        
        for (i = 0; i < fieldsNumber; i++) {
            
            ll = ibasof[i];
            kk = matchVar.fieldPatt[matchVar.jIndex[i]];
            
            mm = ll + kk - gamma[j * fieldsNumber + i] - 2;
            
            for (jj = 0; jj < MCLS; jj++) {
                
                probl[jj] *= vectrh[jj * numUniquePatt + mm];
            }
        }
    }
    
    
    /**
     * Converts the integer 'I - 1' into the m-dimensional arbitrary-base equivalent.
     *
     * @param fieldsNumber
     * @param matchVar
     */
    private void gammav(int fieldsNumber, MatchVariables matchVar) {
        
        int k;
        int temp;
        int ib;
        int i;
        int j;
        
        for (i = 0; i < totalNumPatt; i++) {
            
            temp = i;
            
            for (j = 0; j < fieldsNumber; j++) {
                
                k = fieldsNumber - (j + 1);
                ib = matchVar.fieldPatt[matchVar.jIndex[k]];
                
                gamma[i * fieldsNumber + k] = temp - ib * (temp / ib);
                temp /= ib;
            }
        }
    }
    
    /**
     * @param fieldsNumber
     * @param matchVar
     */
    private void compUProba(int fieldsNumber, MatchVariables matchVar) {
        
        String blnk = new String();
        int i;
        int j;
        int jj;
        int kk;
        int ll;
        double uProb;
        double interm;
        
        for (j = 0; j < fieldsNumber; j++) {
            
            ll = matchVar.fieldPatt[matchVar.jIndex[j]] - 1;
            kk = ibasof[j] - 1;
            
            for (i = 0; i < ll; i++) {
                
                uProb = 0.0;
                
                for (jj = 1; jj < MCLS; jj++) {
                    
                    interm = proba[jj] * vector[kk + i + jj * numUniquePatt];
                    
                    uProb += interm;
                }
                
                uProb /= (1.0 - proba[0]);
                
                matchVar.Uproba[matchVar.jIndex[j]] = uProb;
                matchVar.Mproba[matchVar.jIndex[j]] = vector[kk + i];
            }
        }
    }
    
    
    /**
     * Updates the u and m probabilities and the agreement and 
     * disagreement weights
     *
     * @param PrintWriter initFile
     * @return void
     */
    private void updateWeights(int fieldsNumber, MatchVariables matchVar) {

        int j;

        for (j = 0; j < fieldsNumber; j++) {
        
            if (matchVar.Uproba[matchVar.jIndex[j]] < matchVar.minD) {

                matchVar.Uproba[matchVar.jIndex[j]] = matchVar.minD;

            } else if (matchVar.Uproba[matchVar.jIndex[j]] > matchVar.maxD) {

                matchVar.Uproba[matchVar.jIndex[j]] = matchVar.maxD;
            }

            if (matchVar.Mproba[matchVar.jIndex[j]] < matchVar.minD) {

                matchVar.Mproba[matchVar.jIndex[j]] = matchVar.minD;

            } else if (matchVar.Mproba[matchVar.jIndex[j]] > matchVar.maxD) {

                matchVar.Mproba[matchVar.jIndex[j]] = matchVar.maxD;
            }

            // Update the initial agreement and disagreement weight
            matchVar.agrWeight[j] = Math.log(matchVar.Mproba[j] / matchVar.Uproba[j]);

            matchVar.disWeight[j] = Math.log((1.0 - matchVar.Mproba[j]) /
                                    (1.0 - matchVar.Uproba[j]));
        }
    }
}
