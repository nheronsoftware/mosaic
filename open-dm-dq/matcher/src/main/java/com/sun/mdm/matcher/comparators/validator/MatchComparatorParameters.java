/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.matcher.comparators.validator;

import com.sun.mdm.matcher.configurator.MatchVariables;
import com.sun.mdm.matcher.api.MatcherException;

import java.util.Set;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator; 
import java.io.IOException;

import net.java.hulp.i18n.LocalizationSupport;
import net.java.hulp.i18n.Logger;
import com.sun.mdm.matcher.util.Localizer;
import java.util.logging.Level;

/**
 * Handles the lists of parameters validators and data sources handler custom classes
 *
 * @author souaguenouni
 */
public class MatchComparatorParameters {
    
    private static final String PARAMS_VALIDATOR = "ParamsValidator";
    private static final String P_PATH = "com.sun.mdm.matcher.comparators.validator.params";
    private static final String SOURCES_VALIDATOR = "SourcesHandler";
    private static final String S_PATH = "com.sun.mdm.matcher.comparators.validator.datasources";
    
    // Logging handlers
    private transient Logger mLogger = Logger.getLogger(this.getClass().getName());
    private transient Localizer mLocalizer = Localizer.get();      
    
    // Holds a map of the class name - parameters list
    Map<Integer, Map> params;
    // The match field name
    String matchFieldName;
    // The match fdield index
    int index;
    // 
    MatchVariables mVar;

    /**
     * Constructor
     * @param mVar the MatchVariables instance
     */
    public MatchComparatorParameters(MatchVariables mVar) {
        this.mVar = mVar;  
    }       
 
    
    /**
     * 
     * @throws MatcherException aaaa
     */
    public void handleParametersValidation() 
        throws MatcherException {
        //
        params = mVar.params;
        int index;   
        Iterator<Integer> iter = params.keySet().iterator();
        String fullClassName;
        String fullClassName1;
        String className;
        String codeName;
        ParametersValidator validatorInstance = null;
        
        while(iter.hasNext()) {
            index = iter.next().intValue();     
            codeName = mVar.comparatorType[index];
            fullClassName = mVar.compMap.get(codeName);
            // Case where the code is secondary one
            if (fullClassName == null) {
                fullClassName = mVar.compMap.get(mVar.secondaryCodes.get(codeName));
            }
            className = fullClassName.substring(fullClassName.lastIndexOf('.'));      
            
            try {
                fullClassName1 = P_PATH+className+PARAMS_VALIDATOR;
                Class comparatorClass = Class.forName(fullClassName1);
                validatorInstance = (ParametersValidator) comparatorClass.newInstance(); 
        
                // Validate the comparators' parameters.
                validatorInstance.validateComparatorsParameters(params.get(index));

            } catch (IllegalAccessException ex) {                
                mLogger.info(mLocalizer.x("MEG015: {0}", ex.getMessage())); 
            } catch (InstantiationException ex) {  
                if (mLogger.isLoggable(Level.FINE)) {
                    mLogger.fine("The optional Match Comparator parameters class was not defined for class"+fullClassName);
                }                       
            } catch (ClassNotFoundException ex) {
                if (mLogger.isLoggable(Level.FINE)) {
                    mLogger.fine("The optional Match Comparator parameters class was not defined for class"+fullClassName);
                }      
            }                
        }      
    }

    
    /**
     * @param the match field index
     * @throws MatcherException aaaa
     */
    public Map<String, Object> handleDataSourcesValidation() 
        throws MatcherException, IOException {

        String fullClassName;
        String classPathName;
        String className;        
        // Iterate over the different sets of datasources
        Set s = mVar.sources.keySet();
        Iterator<Integer> iter = s.iterator();
        
        DataSourcesHandler dataSourceInstance = null;
        Map<String, DataSourcesHandler> codeDSHandler = new HashMap();
        String codeName;
        int index;

        DataSourcesProperties dataPath = new FileDataSources();
        String path;
        Integer key;
        
        // Instantiate all the data sources classes handler.
        while(iter.hasNext()) {
            index = iter.next().intValue();
       
            codeName = mVar.comparatorType[index];   
            classPathName = mVar.compMap.get(codeName);
            className = classPathName.substring(classPathName.lastIndexOf('.')+1);
      
            try {
                fullClassName = S_PATH+"."+className+SOURCES_VALIDATOR;       
                Class comparatorClass = Class.forName(fullClassName);  

                if (comparatorClass != null) {
                    if (!codeDSHandler.containsKey(codeName)) {
                        dataSourceInstance = (DataSourcesHandler) comparatorClass.newInstance(); 
                        // Update the map code name - data source handler
                        codeDSHandler.put(codeName, dataSourceInstance);
                    }

                   // Update the dataPath
                    dataPath.setDataSourcesList(index, codeName, mVar.sources.get(index));
                }                    
            } catch (IllegalAccessException ex) {
                mLogger.warn(mLocalizer.x("MEG018: The match comparator data sources' class cannot be instantiated {0}", ex.getMessage())); 
            } catch (InstantiationException ex) {   
                mLogger.warn(mLocalizer.x("MEG019: The match comparator data sources' class is not found {0}", ex.getMessage()));
            } catch (ClassNotFoundException ex) {
            }     
        }

        // Handle validation
        Set keys = codeDSHandler.keySet();
        Iterator<String> sourceIter = keys.iterator();
        Map<String, Object> sourceHandlerList = new HashMap();
        
        while(sourceIter.hasNext()) {   
            String keyS = sourceIter.next();
          
            Object obj = codeDSHandler.get(keyS).handleComparatorsDataSources(dataPath);
            sourceHandlerList.put(keyS, obj); 
            // Update the data sources objects
            Map<String, Object> mp = (HashMap) obj;
            Iterator<String> it = mp.keySet().iterator();
            while (it.hasNext()) {
                String patho = it.next();           
                dataPath.setDataSourceObject(patho, mp.get(patho));
            }
        }       
        return sourceHandlerList;
    }
}
