/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.matcher.comparators;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Iterator;
import java.util.StringTokenizer;
import com.sun.mdm.matcher.comparators.configurator.ComparatorsConfigBean;
import com.sun.mdm.matcher.configurator.MatchVariables;
import com.sun.mdm.matcher.api.MatchingEngine;
        
/**
 *
 * @author souaguenouni
 */
public class ComparatorCodeProcessor {

    /**
     * @param codeClass a string representing a comparator code name or set of code names
     * @param option
     * @param meg an instance of the matching engine
     */
    public static String[] decodeComparatorsCode(String codeName, boolean option, MatchingEngine meg) 
            throws InstantiationException {
        
        // Check if the code has multiple code comparator
        String[] brk1 = codeName.split("\\[");
        String[] code = null;

        if (brk1.length > 1) {  
            // Remove the second bracket
            String[] brk = brk1[1].split("\\]");         
            String[] comma = brk[0].split(",");
            String[] dash = brk[0].split("-");
            int commaL = comma.length;
            int dashL = dash.length;
            int i;
            
            if (commaL > 1) {
                 
//                code = new String[commaL];
                code = new String[1];
                // Create all the basic comparator code
                for(i = 0; i < commaL; i++) {
                    // First, register the first code as the base one.
                    if (i == 0) {
                        code[i] = brk1[0]+comma[i].trim();
                    } else {
                        if (option) {
                            // Then, register all the secondary codes separately
                            // so that we can, later on, 'normalize' them to the base one
                            meg.getMatchVar(meg).secondaryCodes.put(brk1[0]+comma[i].trim(), code[0]);
                        }
                    }
                }

            } else if (dashL > 1) {
                // 
                int min = Integer.parseInt(dash[0]);
                int max = Integer.parseInt(dash[1]);
                code = new String[dashL];
                
                // Create all the basic comparator code
                for(i = min; i <= max; i++) {
                    // Separate out the different codes inside a given class
                    // First, register the first code as the base one.
                    if (i == min) {                    
                        code[i] = brk1[0]+i;
                    } else {
                        if (option) {                        
                            // Then, register all the secondary codes separately
                            // so that we can, later on, 'normalize' them to the base one
                            meg.getMatchVar(meg).secondaryCodes.put(brk1[0]+i, code[min]); 
                        }
                    }
                }             
            }               
        } else {
            code = new String[1];
            code[0] = codeName;
        } 
        return code;
    }
    
    /**
     * @param codeClass a string representing a set of coded code names
     */
    public static Map<String, Map<String, String>> decodeComparatorsCode(Map<String, String> codeClass) {
        
        //
        Map<String, Map<String, String>> totCodeList = new HashMap();
        Map theNewCodeClass = new HashMap();
        Map secondaryCodeNames = new HashMap();
        Set<String> codeList = codeClass.keySet();   
        String code;
    
        // length of the list
        Iterator iter = codeList.iterator();
        int i;
        
        while(iter.hasNext()) {
            code = iter.next().toString();

            // Check if the code has multiple code comparator
            String[] brk1 = code.split("\\[");
     
            if (brk1.length > 1) {  
                // Remove the second bracket
                String[] brk = brk1[1].split("\\]");         
                String[] comma = brk[0].split(",");
                String[] dash = brk[0].split("-");
                int commaL = comma.length;
                int dashL = dash.length;
                String codeZ = "";
                
                if (commaL > 1) {
 
                    // Create all the basic comparator code
                    for(i = 0; i < commaL; i++) {
         //           for(i = 0; i < 1; i++) { 
                        if (i == 0) {
                            codeZ = brk1[0]+comma[i].trim();
                            // Separate out the different codes inside a given class
                            theNewCodeClass.put(codeZ, codeClass.get(code));
                        } else {
                            secondaryCodeNames.put(brk1[0]+comma[i].trim(), codeZ);
                        }
                    }
                    
                } else if (dashL > 1) {
                    // 
                    int min = Integer.parseInt(dash[0]);
                    int max = Integer.parseInt(dash[1]);
                    
                    // Create all the basic comparator code
                    for(i = min; i <= max; i++) {             
                        // Separate out the different codes inside a given class
     //                   theNewCodeClass.put(brk1[0]+i, codeClass.get(code));
                        if (i == min) { 
                            theNewCodeClass.put(brk1[0], codeClass.get(code));
                        } else {
                            secondaryCodeNames.put(brk1[i], brk1[0]);
                        }
                    } 
                }
            } else {
                theNewCodeClass.put(code, codeClass.get(code));
            }           
        }
        totCodeList.put("base", theNewCodeClass);
        totCodeList.put("secondary", secondaryCodeNames);
        return totCodeList;
    }    
}
