/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.matcher.comparators.base;

import com.sun.mdm.matcher.comparators.MatchComparatorException;
import com.sun.mdm.matcher.comparators.MatchComparator;
import com.sun.mdm.matcher.util.CharactersErrorsHandling;
import java.util.Locale;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import java.text.*;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;

/**
 * This algorithm comprises my own version of a generic string comparator, yet 
 * faster and less complicated than the Jaro or the Winkler/Jaro algorithms.
 * It handles transpositions, typos related to keypunch errors, characters with 
 * diacretical marks. You can tune the importance of each parameters by changing 
 * the associated coefficients
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1 $
 */
public class UnicodeSSNComparator implements MatchComparator {
    
    private Map<String, Map> params;
    // The list of parameters
    private Map<String, String> theParams;   
    
    /**
     * Initialize the parameters and data sources info.
     * @param  params  provides all the parameters associated with a given match field using this match comparator
     * @param  dataSources  provides all the data sources info. associated with a given match field using this match comparator
     * @param  dependClassList  provides the list of all the dependency classes
     */
    public void initialize(Map<String, Map> params, Map<String, Map> dataSources, Map<String, Map> dependClassList) {
        this.params = params;
    }    

    /**
     * A setter for real-time passed-in parameters
     *
     * @param  key  the key for use in a Map
     * @param  value the corresponding value for use in a Map
     */
    public void setRTParameters(String key, String value) {      
    }    
    
    /**
     * Close any related data sources streams
     */
    public void stop() {}   
    
    /**
     * Measure the degree of similarity between recA and recB using a generic
     * string comparator algorithm that accounts for accounts for insertions,
     * deletions, transpositions plus three additional enhancements (scanning errors,
     * large strings and linear dependencies of the error rate function of the
     * length)
     *
     * @param      recA   Candidate's string record.
     * @param      recB   Reference's string record.
     * @param      mEng  an instance of the Matching Engine class
     * @return     a real number that measures the degree of match [0,1]
     * @throws SbmeMatchingException an exception
     */
    public double compareFields(String recA, String recB, Map context)
    throws MatchComparatorException {
        
        // The list of parameters
        theParams = this.params.get(context.get("fieldName"));
        String language = theParams.get("language");
        
        // Define an 2D array to hold the unicode representation of the records (primary,
        // secondary and tertiary)
        List uRecA = new ArrayList();
        int[] intRecA = new int[3];        
        List uRecB = new ArrayList();
        int[] intRecB = new int[3];     
                
        RuleBasedCollator ruleB = (RuleBasedCollator) Collator.getInstance(new Locale(language));
        CollationElementIterator iter = ruleB.getCollationElementIterator(recA);

        int elem = 0;
        int i = 0;
        
        while ((elem = iter.next()) != CollationElementIterator.NULLORDER) {

            intRecA[0] = CollationElementIterator.primaryOrder(elem);
            intRecA[1] = CollationElementIterator.secondaryOrder(elem);
            intRecA[2] = CollationElementIterator.tertiaryOrder(elem);
            
            if (intRecA[0] != 32767) {        
                uRecA.add(i, intRecA);  
            }     
        }        
                    
        iter = ruleB.getCollationElementIterator(recB);                       
        elem = 0;
        i = 0;

        while ((elem = iter.next()) != CollationElementIterator.NULLORDER) {

            intRecB[0] = CollationElementIterator.primaryOrder(elem);
            intRecB[1] = CollationElementIterator.secondaryOrder(elem);
            intRecB[2] = CollationElementIterator.tertiaryOrder(elem);
            
            if (intRecB[0] != 32767) {                             
                uRecB.add(i, intRecB);  
            }     
        }      

        int lenA = uRecA.size();
        int lenB = uRecB.size();
       
        
        int fixLen;
        int[] aCh = new int[3];
        int[] bCh = new int[3];
        
        int[] aCh1 = new int[3];
        int[] bCh1 = new int[3];
        
        int aCh2;
        int bCh2;        
                
        double exactM = 0;
        int minv;
        int maxv;
        int exactCoef = 6;
        int transpCoef = 4;
        int typoCoef = 3;
        int diaCoef = 5;
        int maxWeight;

        int j;
        int numExact = 0;
        int numTypo = 0;
        int numDia = 0;
        int numTransp = 0;                               
        StringBuilder sb = null;
 
        recA = recA.toUpperCase();
        recB = recB.toUpperCase();

        // Modify the pattern so that it can handle exactly Length times 
        // any specified constraint, Length being the length of the records A and B
        if (theParams.containsKey("ssnLength")) {
            fixLen = Integer.parseInt(theParams.get("ssnLength")); 
        } else {
            fixLen = 0;
        }        

        // First test if the SSN needs to have a fixed length
        if (fixLen > 0) {

            // Make sure that both records have the same expected length
            if ((lenA != fixLen) || (lenB != fixLen)) {
                return 0.0;
            }
        }

        // Test also the type of the records (numeric or alphanumeric)
        // First, numeric
        if (theParams.get("recType").compareTo("nu") == 0) {

            // Make sure that both records are numerics
            for (i = 0; i < lenA; i++) {
                if (!Character.isDigit(recA.charAt(i))) {
                    return 0.0;
                }
            }

            for (i = 0; i < lenB; i++) {
                if (!Character.isDigit(recB.charAt(i))) {
                    return 0.0;
                }
            }

        // Second, alphanumeric
        } else if (theParams.get("recType").compareTo("an") == 0) {

            // Make sure that both records are numerics
            for (i = 0; i < lenA; i++) {
                if (!Character.isLetterOrDigit(recA.charAt(i))) {
                    return 0.0;
                }
            }

            for (i = 0; i < lenB; i++) {
                if (!Character.isLetterOrDigit(recB.charAt(i))) {
                    return 0.0;
                }
            }
        }

        if (theParams.containsKey("ssnList")) {
            String ssnList = theParams.get("ssnList");
            if(ssnList.trim().length() > 0) { 
                sb = new StringBuilder();
                //First extend the pattern to include at least min(A, B) number
                // of identical characters in the pattern
                sb.append(ssnList);
                sb.append("{").append(Math.min(lenA, lenB)).append(",}");

                Pattern pa = Pattern.compile(sb.toString());

                /* Regular expression that catches empty strings */
                Matcher matA = pa.matcher(recA);
                Matcher matB = pa.matcher(recB);

                /* test if the record is empty or null */
                if (matA.lookingAt() || matB.lookingAt()) {
                    return 0.0;
                }
            }
        }
        // Determine which field is longer
        if (lenA > lenB) {
            maxv = lenA;
            minv = lenB;
            
        } else {
            maxv = lenB;
            minv = lenA;
        }
        
        // If the smaller field, in length, is empty (blanks), return a zero weight.
        // Need review since we have already considered this case earlier
        if (minv == 0) {
            return 0.0;
        }     
        
        // First, test the corresponding character-to-character for exact, keypunch
        // errors and diacritical marks matches
        for (i = 0; i < minv; i++) {

            aCh = (int[]) uRecA.get(i);
            bCh = (int[]) uRecB.get(i);                    
           
            // Exact character-to-character match
            if (aCh[0] == bCh[0]) {
                
                if (aCh[0] != 0 && (aCh[1] == bCh[1])) {             
                    numExact++;
                    
                } else if (aCh[1] != 0 && bCh[1] == 0) {
                    numDia++; 
                       
                } else if (bCh[1] != 0 && aCh[1] == 0) {
                    numDia++;    
                }
               
            // The characters pair is in the list of Key-punch errors
            } else if (CharactersErrorsHandling.correctCharactersErrors(aCh[0], bCh[0]) == 3) {
                
                numTypo++;   
            }  
            
            // Handle transposition
            if (i < minv - 1) {
                
                aCh1 = (int[]) uRecA.get(i + 1);
                bCh1 = (int[]) uRecB.get(i + 1);    
                            
                if ((aCh[0] == bCh1[0]) && (aCh1[0] == bCh[0])) { 
                    if (aCh[0] != bCh[0]) {                         
                        numTransp++; 
                    }                                                            
                }
                
                
                // Handle cases where the characters in one of the strings is
                // expanded (zß == zss and ßz = ssz)
                if ((aCh1[2] == 2 || aCh1[2] == 3) || (bCh[2] == 2 || bCh[2] == 3)) {
                    
                    aCh2 = ((Integer) uRecA.get(i + 2)).intValue();
                    bCh2 = ((Integer) uRecB.get(i + 2)).intValue();
                                         
                    if ((aCh[0] == bCh2) && (aCh2 == bCh[1] && aCh1[0] == bCh[0])) {
                        if (aCh[0] != bCh[0]) {                         
                            numTransp++; 
                        }            
                    }                 
                } 
            }
        }                    

        // Measure the maximum weight. 
        maxWeight = maxv * exactCoef;
        
        // Measure the total weight
        exactM = numExact * exactCoef + numTypo * typoCoef + numDia * diaCoef 
                 + numTransp * transpCoef;
            
        if (exactM >= maxWeight) {
            return 1.0;
        } else {
            return exactM / maxWeight;
        }               
    }   
}
