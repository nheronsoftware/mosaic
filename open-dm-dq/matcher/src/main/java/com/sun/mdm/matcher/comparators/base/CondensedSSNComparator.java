/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.matcher.comparators.base;

import com.sun.mdm.matcher.comparators.MatchComparator;
import com.sun.mdm.matcher.comparators.MatchComparatorException;
import com.sun.mdm.matcher.configurator.MatchVariables;
import com.sun.mdm.matcher.util.CharactersErrorsHandling;
import com.sun.mdm.matcher.util.DiacriticalMarks;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import java.util.Map;

/**
 * This algorithm comprises my own version of a generic string comparator, yet 
 * faster and less complicated than the Jaro or the Winkler/Jaro algorithms.
 * It handles transpositions, typos related to keypunch errors, characters with 
 * diacretical marks. You can tune the importance of each parameters by changing 
 * the associated coefficients
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1 $
 */
public class CondensedSSNComparator implements MatchComparator {

    private Map<String, Map> params;
    // The variable that holds all the additional real-time parameters
    private Map<String, String> argumentsRT = new HashMap();     
    
    /**
     * Initialize the parameters and data sources info.
     * @param  params  provides all the parameters associated with a given match field using this match comparator
     * @param  dataSources  provides all the data sources info. associated with a given match field using this match comparator
     * @param  dependClassList  provides the list of all the dependency classes
     */
    public void initialize(Map<String, Map> params, Map<String, Map> dataSources, Map<String, Map> dependClassList) {
        this.params = params;
    }
    
    /**
     * Close any related data sources streams
     */
    public void stop() {
        argumentsRT.clear();
    }   

    /**
     * A parameters setter
     */
    public void setRTParameters(String key, String value) {
        this.argumentsRT.put(key, value);
    }         
    
    /**
     * Measure the degree of similarity between recordA and recordB using a generic
     * string comparator algorithm that accounts for accounts for insertions,
     * deletions, transpositions plus three additional enhancements (scanning errors,
     * large strings and linear dependencies of the error rate function of the
     * length)
     *
     * @param      recordA   Candidate's string record.
     * @param      recordB   Reference's string record.
     * @param      mEng  an instance of the Matching Engine class
     * @return     a real number that measures the degree of match [0,1]
     * @throws SbmeMatchingException an exception
     */
    public double compareFields(String recordA, String recordB, Map context) 
        throws MatchComparatorException {
        
        String recA = recordA.toUpperCase();
        String recB = recordB.toUpperCase();
        
        StringBuffer sb = null;
        int lenA;
        int lenB;
        int fixLen = 0;
        char aCh;
        char bCh;
        char aCh1;
        char bCh1;
                
        double exactM = 0;
        int minv;
        int maxv;
        int exactCoef = 6;
        int transpCoef = 4;
        int typoCoef = 3;
        int diaCoef = 5;
        int maxWeight;

        int i;
        int j;
        int numExact = 0;
        int numTypo = 0;
        int numDia = 0;
        int numTransp = 0;    
        
        String fieldName = argumentsRT.get("matchfield");
        if(fieldName == null) {
            fieldName = context.get("fieldName").toString();
        }
        // The list of parameters
        Map<String, String> theParams = params.get(fieldName);
        
        lenA = recA.length();
        lenB = recB.length();

 
        // Modify the pattern so that it can handle exactly Length times 
        // any specified constraint, Length being the length of the records A and B
        String ssnL = theParams.get("ssnLength");
        String recType = theParams.get("recType"); 
        String ssnList = theParams.get("ssnList");
        
        if (ssnL != null) {
            fixLen = Integer.parseInt(theParams.get("ssnLength")); 
        } else {
            fixLen = 0;
        }
        if (recType == null) {
            throw new MatchComparatorException("You need to define the RecType parameters within CondensedSSNComparator comparator");
        }
        
        // First test if the SSN needs to have a fixed length
        if (fixLen > 0) {

            // Make sure that both records have the same axpected length
            if ((lenA != fixLen) || (lenB != fixLen)) {
                return 0.0;
            }
        }


        // Test also the type of the records (numeric or alphanumeric)
        // First, numeric
         if (recType.compareTo("nu") == 0) {

            // Make sure that both records are numerics
            for (i = 0; i < lenA; i++) {
                if (!Character.isDigit(recA.charAt(i))) {
                    return 0.0;
                }
            }

            for (i = 0; i < lenB; i++) {
                if (!Character.isDigit(recB.charAt(i))) {
                    return 0.0;
                }
            }

        // Second, alphanumeric
        } else if (recType.compareTo("an") == 0) {

            // Make sure that both records are alpha numerics
            for (i = 0; i < lenA; i++) {
                if (!Character.isLetterOrDigit(recA.charAt(i))) {
                    return 0.0;
                }
            }

            for (i = 0; i < lenB; i++) {
                if (!Character.isLetterOrDigit(recB.charAt(i))) {
                    return 0.0;
                }
            }
        }

        if(ssnList != null && ssnList.trim().length() > 0) { 
            sb = new StringBuffer();
            //First extend the pattern to include at least min(A, B) number
            // of identical characters in the pattern
            sb.append(ssnList);
            sb.append("{").append(Math.min(lenA, lenB)).append(",}");

            Pattern pa = Pattern.compile(sb.toString());               

            /* Regular expression that catches empty strings */
            Matcher matA = pa.matcher(recA);
            Matcher matB = pa.matcher(recB);

            /* test if the record is empty or null */
            if (matA.lookingAt() || matB.lookingAt()) {
                return 0.0;
            }
        }        
 
        // Determine which field is longer
        if (lenA > lenB) {
            maxv = lenA;
            minv = lenB;
            
        } else {
            maxv = lenB;
            minv = lenA;
        }
        
        // If the smaller field, in length, is empty (blanks), return a zero weight.
        // Need review since we have already considered this case earlier
        if (minv == 0) {
            return 0.0;
        }     
        
        // First, test the corresponding character-to-character for exact, keypunch
        // errors and diacritical marks matches
        for (i = 0; i < minv; i++) {
            aCh = recA.charAt(i);
            bCh = recB.charAt(i);
           
            // Exact character-to-character match
            if (aCh == bCh) {
                numExact++;
               
            // The characters pair is in the list of Key-punch errors
            } else if (CharactersErrorsHandling.correctCharactersErrors(aCh, bCh) == 3) {
                
                numTypo++;
                
            // Only the first character has a diacretical mark   
            }  else if (((int) aCh > 191) && ((int) bCh < 192)) {  
                
                if(DiacriticalMarks.removeDiacriticalMark(aCh) == bCh) {
                    numDia++;
                }
                
            // Only the second character has a diacretical mark     
            }  else if (((int) bCh > 191) && ((int) aCh < 192)) { 
                
                if(DiacriticalMarks.removeDiacriticalMark(bCh) == aCh) {
                    numDia++;
                } 
            }                                                 
        }
 
        // Handle transposition
        for (i = 0; i < minv - 1; i++) { 
            aCh = recA.charAt(i);
            bCh = recB.charAt(i); 
            aCh1 = recA.charAt(i + 1);
            bCh1 = recB.charAt(i + 1);                          
            
            if ((aCh != bCh) && ((aCh == bCh1) & (aCh1 == bCh))) {                
                    numTransp++;     
            }                          
        }                                                

        // Measure the maximum weight. 
        maxWeight = maxv * exactCoef;
        
        // Measure the total weight
        exactM = numExact * exactCoef + numTypo * typoCoef + numDia * diaCoef 
                 + numTransp * transpCoef;
        
            
        if (exactM >= maxWeight) {
            return 1.0;
        } else {
            return exactM / maxWeight;
        }               
    }   
}
