/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.matcher.configurator.curveadjustor;

import com.sun.mdm.matcher.configurator.CurveAdjustor;

/**
 *
 * @author souaguenouni
 */
public class JaroAdvancedStringComparatorCurveAdjustor implements CurveAdjustor {

    private static double defaultCurv[] = {1, 0.0, 4.5, 4.5};
    private static double defaultCurvFirstN[] = {0.92, 0.75, 1.5, 3.0};
    private static double defaultCurvLastN[] = {0.96, 0.88, 3.0, 4.5};
    private static double defaultCurvNumeric[] = {0.98, 0.9, 7.5, 4.5};    
    private static double[] hf = {0.0, 0.0, 0.0, 0.0};   
    
    /**
     * 
     * Creates a new instance of JaroAdvancedStringComparatorCurveAdjustor
     */
    public double[] processCurveAdjustment(String compar, int option) {
  
        int j;
        int defSw = 0;
        double[] adjCurv = new double[4];
        
        for (j = 0; j < 4; j++) {

            // The custom values must be different from zero
            if (option != 0)  {
                adjCurv[j] = hf[j];

            } else {
                defSw++;
                // If one of the values is zero, need further consideration.
                break;
            }
        }
            
        // If at least one of the custom adjustment parameters is not
        // specified (i.e. hf[] = 0), use default values.
        if (defSw != 0) {
            // Case where we have uncertainty for first name
            if ((compar.compareTo("uf") == 0)) {

                for (j = 0; j < 4; j++) {
                    // Default adjustment values for first name
                    adjCurv[j] = defaultCurvFirstN[j];
                }

            } else if ((compar.compareTo("ul") == 0)) {
                for (j = 0; j < 4; j++) {
                    // Default adjustment values for last name
                    adjCurv[j] = defaultCurvLastN[j];
                }

            } else if ((compar.compareTo("un") == 0)) {
                for (j = 0; j < 4; j++) {

                    // Default adjustment values for NUMERIC
                    adjCurv[j] = defaultCurvNumeric[j];
                }

            } else {

                for (j = 0; j < 4; j++) {

                    // Default generic adjustment parameter values
                    adjCurv[j] = defaultCurv[j];
                }
            } 
            defSw = 0;
        }  
        return adjCurv;
    }   
}
