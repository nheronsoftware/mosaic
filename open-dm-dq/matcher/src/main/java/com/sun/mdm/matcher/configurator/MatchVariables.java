/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 2003-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * The contents of this file are subject to the terms of the Common 
 * Development and Distribution License ("CDDL")(the "License"). You 
 * may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the License at
 * https://open-dm-mi.dev.java.net/cddl.html
 * or open-dm-mi/bootstrap/legal/license.txt. See the License for the 
 * specific language governing permissions and limitations under the  
 * License.  
 *
 * When distributing the Covered Code, include this CDDL Header Notice 
 * in each file and include the License file at
 * open-dm-mi/bootstrap/legal/license.txt.
 * If applicable, add the following below this CDDL Header, with the 
 * fields enclosed by brackets [] replaced by your own identifying 
 * information: "Portions Copyrighted [year] [name of copyright owner]"
 */
package com.sun.mdm.matcher.configurator;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;
import java.util.List;

//import com.sun.mdm.matcher.util.ReadMatchConstantsValues;
import java.util.Map;

/**
 * Defines all the 'global' variables used in the matching code.
 *
 * @author Sofiane Ouaguenouni
 * @version $Revision: 1.1 $
 */
public class MatchVariables {
    
    // Parameters name
    public static final String PARAMS = "params";     
    // The dependency classes within match comparators
    public static final String DEPEND_CLASSES = "dependency-classes";
    // Class name constants
    public static final String DATA_SOURCES = "data-sources"; 
    public static final String TYPE = "type";    
    public static final String NAME = "name";    
    // 
    public static final String CURVE_ADJUST = "curve-adjust";    
    // The match field name
    public static final String MATCH_FIELD = "matchfield";     
    /**
     * A constants holding a 50 empty-character block
     */
    public static final String NULL50 = "                                                  ";  
    /** A constants holding a 50 zero-character block */
    public static final String ZERO50 = "00000000000000000000000000000000000000000000000000";    
    /** singleton instance */
    private static MatchVariables instance = null;

    /**
     * Default constructor
     */
    public MatchVariables(int numberMatchFields) {
        super();
        this.numberMatchFields = numberMatchFields;
        this.initiateData();   
    }
    
    /**
     * The input stream of the matching configuration file. 
     */
    InputStream matchConfigStream;
    
    // Get the input stream for the data file
    private static InputStream matchDataStream;
    
    /**
     *
     */
    static InputStream getMatchDataStream() {
        return matchDataStream;
    }

    // This section is dedicated to the parameters associated with pluggable match comparators in the config. file.
    private Map<String, List<String>> plugParams;
    /**
     *
     */
    public void setComparatorsParameters(Map<String, List<String>> params) {
        plugParams = params;
    }
    
    /**
     *
     */
    public void setClassNamesMap(Map<String, String> compMap) {
        this.compMap = compMap;
    }    
    
    /**
     *
     */
    private int numberMatchFields;      
    /**
     *
     */
    public double[] cWeight;
    /**
     *
     */
    public double cmpVal;    
    /**
     *
     */
    public double minD;
    /**
     *
     */
    public double maxD;    
    /**
     * Agreement weight
     */
    public double agrWeight[];
    
    /**
     * Disagreement weight
     */
    public double disWeight[];
    /**
     *
     */
    public double[] Mproba;
    /**
     *
     */
    public double[] Uproba;
    /**
     *
     */
    public double[] fracVal;
    /**
     *
     */
    public String[] nullFlag;  
    /**
     *
     */
    public int year;
    /**
     *
     */
    public int nTable;
    /**
     *
     */
    public int nCrosses;
    /**
     *
     */
    public int criticalTest;
    /**
     *
     */
    public int dw;
    /**
     *
     */
    public int dw1;
    /**
     *
     */
    public int dw2;
    /**
     *
     */
    public int dw3;
    /**
     *
     */
    public int m1;
    /**
     *
     */
    public int m2;
    /**
     *
     */
    public double fw1;
    /**
     *
     */
    public double fw2;
    /**
     *
     */
    public double fw3;
    /**
     *
     */
    public double fm1;
    /**
     *
     */
    public double fm2;

    /**
     *
     */
    public double proportion;
    /**
     *
     */
    public double degPrecision;
    /**
     *
     */
    public double nIteration;
    
    /**
     *
     */
    public int[] jIndex;
    
    /**
     *
     */
    public int eMSwitch = 0;  
    
    /**
     *
     */
    public int[] ibaso;
    
    /**
     *
     */
    public int[] dwi;
    /**
     *
     */
    public int[] tableW;
    /**
     *
     */
    public int[] tableSize;
    /**
     *
     */
    public HashMap adjCurv;
    
    /**
     *
     */
    public int[] lengthF;   
    /**
     *
     */
    public int numberItems;
    /**
     *
     */
    public int[] fieldIndex;
    /**
     *
     */
    public int[] fieldPatt;
    /**
     *
     */
    public int[] freqPatt;    
    /**
     *
     */
    public String[] comparatorType;
    /**
     *
     */
    public boolean[] trans;
    /**
     *
     */
    public boolean[] ssnConstraints;

    /**
     *
     */
    public int[] param1;
    /**
     *
     */
    public int[] param2;
    /**
     *
     */
    public double[] param3;

    /**
     *
     */
    public double[] tolerance1;
    /**
     *
     */
    public double[] tolerance2;
    /**
     *
     */
    public double[] rangeP;

    /**
     *
     */
//    public StringBuffer[] ssnList;
 
    /**
     *
     */
    public int[] ssnLength;

    /**
     *
     */
    public String[] recType;
    /**
     *
     */
//    public StringBuffer[] fieldName;
    public HashMap<Integer, String> fieldName; 
    /**
     *
     */    
    public HashMap<String, String> secondaryCodes;     
    /**
     *
     */    
    public HashMap<String, Integer> fieldNameIndex; 
    /**
     *
     */    
    public List<Map> fieldNameInfo;     
    /**
     *
     */
    public StringBuffer[] idName;
    /**
     *
     */
    public StringBuffer[] pntTable;
    /**
     *
     */
    public double[][] wgtTable;
    
    /**
     *
     */
    public int[][] adjwt;
        
    /**
     *
     */
    public int pass = 0;
    
    /**
     * Language name used in the Locale class
     */ 
    public String language;
    /**
     * The generic list of parameters
     */ 
    public Map<Integer, Map> params;    
   /**
     * The generic list of parameters
     */ 
//    public Map<Integer, Map> sources;    
    public Map<Integer, List> sources;  
   /**
     * The generic list of dependecy classes
     */ 
    public Map<Integer, Map> dependClasses;        
   /**
     * The list of comparators codes
     */     
    public Map<String, String> compMap;   
   /**
     * The index of the match field associated with a data source
     */     
    public int sourcesIndex;
   /**
     * The data source actual data
     */     
    public Map<String, Object> sourcesData;    

    /**
     * Language name used in the Locale class
     */ 
    public String[] lang = {"af", "am", "ar", "be", "bg", "bn", "ca", "cs", 
        "da", "de", "el", "en", "eo", "es", "et", "eu", "fa", "fi", "fo", 
        "fr", "ga", "gl", "gu", "gv", "he", "hi", "hr", "hu", "hy", "id", "is", 
        "it", "ja", "kl", "kn", "ko", "kw", "lt", "lv", "mk", "mr", "mt", "nb",
        "nl", "nn", "om", "pl", "ps", "pt", "ro", "ru", "sh", "sk", "sl", "so",
        "sq", "sr", "sv", "sw", "ta", "te", "th", "ti", "tr", "uk", "vi", "zh"
    };    
    
    private void initiateData() {        
        agrWeight = new double[numberMatchFields];
        disWeight = new double[numberMatchFields]; 
        Mproba = new double[numberMatchFields];
        Uproba = new double[numberMatchFields];
        fracVal = new double[numberMatchFields];
        nullFlag = new String[numberMatchFields];
        ibaso = new int[numberMatchFields];
        dwi = new int[numberMatchFields];
        lengthF = new int[numberMatchFields];
        fieldIndex = new int[numberMatchFields];
        fieldPatt = new int[numberMatchFields];
        freqPatt = new int[(int) Math.pow(2, 10)];
        comparatorType = new String[numberMatchFields];
        trans = new boolean[numberMatchFields];
        ssnConstraints = new boolean[numberMatchFields];
        param1 = new int[numberMatchFields];
        param2 = new int[numberMatchFields];
        param3 = new double[numberMatchFields];
        tolerance1 = new double[numberMatchFields];
        tolerance2 = new double[numberMatchFields];
        rangeP = new double[numberMatchFields];
//        ssnList = new StringBuffer[numberMatchFields];
        ssnLength = new int[numberMatchFields];
        recType = new String[numberMatchFields];
 //       fieldName = new StringBuffer[numberMatchFields];
        fieldName = new HashMap();
        fieldNameIndex = new HashMap();
        secondaryCodes = new HashMap();
        fieldNameInfo = new ArrayList();
        idName = new StringBuffer[numberMatchFields];
        adjwt = new int[255][255];
        
        params = new HashMap();
        sources = new HashMap();
        dependClasses = new HashMap();
        adjCurv = new HashMap();
        
        int i;
        int j;
        
        for (i = 0; i < numberMatchFields; i++) {
            comparatorType[i] = new String();
            idName[i] = new StringBuffer();
//            ssnList[i] = new StringBuffer();
            recType[i] = new String();
            ssnConstraints[i] = false;
            ssnLength[i] = 0;
            trans[i] = false;
        }
    }
        
    
   /**
     * @return the number of matching fields
     */    
    public int getNumMatchFields() {
        return numberMatchFields;  
    }        
}
